from odoo import models, fields, api
from odoo.exceptions import Warning,ValidationError,UserError

        

class ResPartnerPortalExt(models.Model):
    """This class is defined to enhance ResUsers Class to enhance portal."""

    _inherit = 'res.partner'

    gender = fields.Selection([('male', 'Male'), ('female', 'Female')],
                              string="Gender")
    birthday = fields.Date('Date of Birth')

    marital = fields.Selection(
        [('single', 'Single'),
         ('married', 'Married'),
         ('widower', 'Widower'),
         ('divorced', 'Divorced')],
        'Marital Status')
    profile_score = fields.Float("Profile Score Percentage", compute='_cmpte_profile')
    
    @api.depends('name','gender','birthday','marital','mobile','email','street','street2','city','country_id','state_id','zip','academic_ids','experience_ids','certification_ids')
    def _cmpte_profile(self):
        for rec in self:
            score = 0.0
            dct = {
                'name': 0.0,
                'gender':0.0,
                'birthday':0.0,
                'marital':0.0,
                'mobile':0.0,
                'email':0.0,
                'street':0.0,
                'street2':0.0,
                'city':0.0,
                'country_id':0.0,
                'state_id':0.0,
                'zip':0.0,
                'academic_ids':0.0,
                'experience_ids':0.0,
                'certification_ids':0.0
            }
            if rec.name and rec.name != '':
                dct.update({'name':3.0})
            else:
                dct.update({'name':0.0})
                
            if rec.gender and rec.gender != '':
                dct.update({'gender':2.0})
            else:
                dct.update({'gender':0.0})

            if rec.birthday and rec.birthday != '':
                dct.update({'birthday':2.0})
            else:
                dct.update({'birthday':0.0})

            if rec.marital and rec.marital != '':
                dct.update({'marital':2.0})
            else:
                dct.update({'marital':0.0})

            if rec.mobile and rec.mobile != '':
                dct.update({'mobile':2.0})
            else:
                dct.update({'mobile':0.0})

            if rec.email and rec.email != '':
                dct.update({'email':3.0})
            else:
                dct.update({'email':0.0})
            
            if rec.street and rec.street != '':
                dct.update({'street':2.0})
            else:
                dct.update({'street':0.0})

            if rec.street2 and rec.street2 != '':
                dct.update({'street2':1.0})
            else:
                dct.update({'street2':0.0})

            if rec.city and rec.city != '':
                dct.update({'city':2.0})
            else:
                dct.update({'city':0.0})

            if rec.country_id and rec.country_id != '':
                dct.update({'country_id':2.0})
            else:
                dct.update({'country_id':0.0})

            if rec.state_id and rec.state_id != '':
                dct.update({'state_id':2.0})
            else:
                dct.update({'state_id':0.0})

            if rec.zip and rec.zip != '':
                dct.update({'zip':2.0})
            else:
                dct.update({'zip':0.0})

            if len((rec.academic_ids)) >= 1:
                dct.update({'academic_ids':25.0})
            else:
                dct.update({'academic_ids':0.0})
            
            if len(rec.experience_ids) >= 1:
                dct.update({'experience_ids':25.0})
            else:
                dct.update({'experience_ids':0.0})

            if len(rec.certification_ids) >= 1:
                dct.update({'certification_ids':25.0})
            else:
                dct.update({'certification_ids':0.0})

            rec.profile_score = sum(dct.values())

    