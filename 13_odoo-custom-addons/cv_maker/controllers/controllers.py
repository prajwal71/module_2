import werkzeug
from odoo.http import request
from odoo import api, http, _
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID, http, _
from dateutil.relativedelta import relativedelta
import base64
import logging
import json
from odoo.tools.safe_eval import safe_eval
_logger = logging.getLogger(__name__)
from odoo.addons.portal.controllers.portal \
import CustomerPortal as CustomerPortal, pager as portal_pager
# import yaml




class InheritedCustomerPortal_ProgressBar(CustomerPortal):
    """Updated the Optional fields list to stop the unkown fields error For Gender Birthday and Marital."""
    MANDATORY_BILLING_FIELDS = ["name", "email", "street", "city", "country_id"]
    OPTIONAL_BILLING_FIELDS = [ "zipcode", "state_id", "phone", "vat","mobile", "company_name", "gender", "birthday","marital","street2",
                               "organization","location", "study_field",
                               "grade", "job_position","start_date", "description",
                               "end_date", "qualification", "certification",
                               "operation_type", "opr_id", "tr_no",
                               "is_still"]

class CreateCv(http.Controller):

    @http.route('/cvmaker/create', type='http', auth='user', website=True)
    def show_custom_webpage(self, **kw):

        countries = request.env['res.country'].sudo().search([])
        states = request.env['res.country.state'].sudo().search([])
        _logger.info("----------------STATES___________%s", states)
        partner = request.env.user.partner_id
        
        _logger.info("=================PARTNER==========%s======", partner)
        return http.request.render('cv_maker.cv_maker_webPage', {"countries": countries, "states": states, 'partner': partner})

    @http.route('/cvmaker', type="http", auth='public', website=True)
    def show_custom_options(self, **kw):

        return http.request.render('cv_maker.cv_options')



    @http.route(["/create/cv"], methods=['POST'], type="http", auth="user", website=True)
    def cvMaker(self, res_id=None, **post):

        name = post['name']
        email = post['email']
        mobile = post['mobile']
        street = post['street']
        gender = post['gender']
        marital = post['marital']
        birthday = post["birthday"]
        _logger.info(
            "------------------------------------COUNTRY---------------%s", post["country_id"])
        country_id = int(post["country_id"])
        street = post["street"]
        street2 = post["street2"]
        city = post["city"]
        state_id=int (post["state_id"])
        zip = post["zip"]
        website=post["website"]
        _logger.info("------------------------------------C---------------")
        _logger.info(
            "------------------------------------NAME---------------%s", name)
        _logger.info(
            "------------------------------------MOBILE---------------%s", mobile)
        _logger.info(
            "------------------------------------GENDER---------------%s", gender)

        update_partner = request.env.user.partner_id
        _logger.info(
            "=================UPDATE PARTNER==========%s======", update_partner)
        partner_post = update_partner.sudo().write({"name": name, "gender": gender, "email": email, "mobile": mobile, "marital": marital, "birthday": birthday,
                                                    "country_id": country_id,"state_id": state_id,  "street": street, "street2": street2, "city": city,  "zip": zip, "website": website, })
        _logger.info(
            "====================POSTED PARTNER=======%s======", partner_post)
        # Redirect to PDF 
        ret_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url') + '/report/pdf/cv_maker.page_printcv/' + str(request.env.user.partner_id.id)
        return request.redirect (ret_url)





