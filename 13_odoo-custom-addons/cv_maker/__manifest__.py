# -*- coding: utf-8 -*-
{
    'name': "CV Maker",

    'summary': """
        This module generates CV form contacts""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Nexus Incorporations",
    'website': "http://www.nexusgurus.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','website','job_portal'],

    # always loaded
    'data': [
        'views/views.xml',
        'views/templates.xml',
        'views/res_partnerext.xml',
        # 'views/thankyou.xml',
        'report/print_page.xml',
        'views/progress.xml',
        'views/options.xml',
        

    ],
    # only loaded in demonstration mode
    
}
