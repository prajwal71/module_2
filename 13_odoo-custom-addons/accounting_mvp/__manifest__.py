# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'MVP Accounting',
    'version': '13.0',
    'category': 'Accounting',
    'summary': 'MVP Accounting',
    'author': 'Nexus Incorporation',
    'description': """
The aim is to bring all the menus in single menu and make the simple application by integrating Sales, Purchase,
Accounting, Inventory into single one.
====================================================================

The following topics are covered by this module:
------------------------------------------------------
    * All menus/sub-menus in single menu.
""",
    'depends': ['account', 'sale', 'sale_management', 'purchase',
                # 'account_dynamic_reports',
            ],
    'data': [
        'views/sale_order.xml',
        'views/purchase_order.xml',
        'views/account_move.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'LGPL-3',
}
