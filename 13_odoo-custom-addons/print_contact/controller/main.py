# -*- coding: utf-8 -*-

import json
import logging
import re

from collections import OrderedDict
from werkzeug.urls import url_encode
from odoo import _, http, SUPERUSER_ID
from odoo.http import request
from odoo.addons.portal.controllers.portal import  CustomerPortal
from odoo.tools.safe_eval import safe_eval

_logger = logging.getLogger(__name__)

class CustomerPortalInherit(CustomerPortal):

    @http.route(['/my/cv/<model("res.partner"):res_id>/download/<aname>',], type='http', auth="public",
                website=True)
    def my_cv_download(self, res_id=None, aname=None, **kw):
        if res_id:
            lang = request.env.user.lang
            report_id = request.env.ref('print_contact.action_report_page')
            pdf_content, mimetype = report_id.sudo().with_context(lang=lang).render_qweb_pdf(
                res_ids=res_id.id,
            )
            pdfhttpheaders = [
                ('Content-Type', 'application/pdf'),
                ('Content-Length', len(pdf_content)),
            ]
            res = request.make_response(pdf_content, headers=pdfhttpheaders)
        else:
            res = request.render("http_routing.404")
        return res

    