import logging
_logger = logging.getLogger(__name__)
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp


class SaleOrder(models.Model):
	_inherit = 'sale.order'

	pref_date =  fields.Date(string="Preferred Date")