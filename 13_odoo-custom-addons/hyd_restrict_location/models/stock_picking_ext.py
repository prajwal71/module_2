from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, Warning
import logging
_logger = logging.getLogger(__name__)

class PickingExxt(models.Model):
    _inherit = "stock.picking"
    
    @api.model
    def create(self, vals):
        res = super(PickingExxt, self).create(vals)

        a = self.env.user.stock_location
    
     
        _logger.info('========%s=======user===id=========',vals.get('origin'))
        if vals.get('origin'):
            if 'S' in vals.get('origin'):
                _logger.info("=============sale===========order====================")
                if a:
                    res.location_id = a.id
                
        return res
        