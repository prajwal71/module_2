from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, Warning


class ResUserExt(models.Model):
    _inherit = 'res.users'
    
    stock_location = fields.Many2one(
       'stock.location', string="Allowed L0cation",
        help="""Users allowed to do operation/move from/to this location"""
             """, others users will have a popup error on transfert action""")
 
