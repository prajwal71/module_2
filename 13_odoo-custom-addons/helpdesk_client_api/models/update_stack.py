# -*- coding: utf-8 -*-

from odoo import fields, models,api, _
import requests
import base64

import xmlrpc.client

import logging

_logger = logging.getLogger(__name__)

# URL = 'http://localhost:8080'
# DB = 'Helpdesk_Server'
# USERNAME = 'admin'
# PASSWORD = 'admin'

class UpdateStack(models.Model):
    _name = 'update.stack'
    _description = 'Update records in Server'

    name = fields.Char(string="Update Type")
    state = fields.Selection([
        ('to_update', 'To Update'),
        ('updated', 'Updated')
    ], default='to_update')

    #for log note
    log_model = fields.Char('Related Document Model', index=True)
    log_body = fields.Html('Contents', default='', sanitize_style=True)
    log_message_type = fields.Selection([
        ('email', 'Email'),
        ('comment', 'Comment'),
        ('notification', 'System notification'),
        ('user_notification', 'User Specific Notification')],
        'Type'
        )
    log_subtype_id = fields.Many2one('mail.message.subtype', 'Subtype')
    log_attachment_ids = fields.Many2many(
        'ir.attachment', 'log_attachment_rel',
        'message_id', 'attachment_id',
        string='Attachments',
        help='Attachments are linked to a document through model / res_id and to the message '
             'through this field.')
    client_ticket_id = fields.Integer()

    #for channel
    ch_body = fields.Html('Contents', default='', sanitize_style=True)
    ch_subject = fields.Char('Subject')

    def update_record(self):
        if not self:
            self = self.env['update.stack'].sudo().search([])

        server = self.env['api.server.config'].sudo().search([], limit=1)
        URL = server.server_url
        DB = server.server_database
        USERNAME = server.server_username
        PASSWORD = server.server_password
        
        for rec in self:
            if rec.state == 'to_update':
                client_id = self.env['ir.config_parameter'].sudo().search([('key', '=', 'helpdesk_client_id')])

                _logger.error('-----------------client id-------------------------%s', str(client_id.value))
                _logger.error('-----------------client ticket id-------------------------%s', rec.client_ticket_id)
                try:
                    common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(URL))
                    uid = common.authenticate(DB, USERNAME, PASSWORD, {})
                    models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(URL))               
                    server_ticket_id = models.execute_kw(DB, uid, PASSWORD, 'helpdesk.ticket', 'search',
                                                            [[['client_ticket_id', '=', rec.client_ticket_id]]])
                    if rec.name == 'log/attachment':
                        attachments = []
                        if rec.log_attachment_ids:
                            for attachment in rec.log_attachment_ids:
                                attachments.append({
                                    'name': attachment.name,
                                    'type': attachment.type,
                                    'datas': attachment.datas,
                                    'res_model': 'helpdesk.ticket',
                                    'res_id': server_ticket_id[0],
                                    'res_name': attachment.res_name,
                                })
                            create_attachment = models.execute_kw(DB, uid, PASSWORD, 'ir.attachment', 'create',
                                                        [attachments])
                            _logger.info('=================%s', create_attachment)
                            
                            vals = {
                                'model': 'helpdesk.ticket',
                                'author_id': int(client_id.value),
                                'res_id': server_ticket_id[0],
                                'body': rec.log_body,
                                'attachment_ids': create_attachment,
                                'message_type': rec.log_message_type,
                                'subtype_id': rec.log_subtype_id.id,
                            }
                        else:
                            vals = {
                                'model': 'helpdesk.ticket',
                                'author_id': int(client_id.value),
                                'res_id': server_ticket_id[0],
                                'body': rec.log_body,
                                'message_type': rec.log_message_type,
                                'subtype_id': rec.log_subtype_id.id,
                            }
                        update_log = models.execute_kw(DB, uid, PASSWORD, 'mail.message', 'create',
                                                        [vals])
                        _logger.info('=======================================================%s', update_log)
                        if update_log:
                            rec.state = 'updated'

                    elif rec.name == 'channel/attachment':
                        create_attachment = []
                        attachments = []
                        server_channel_id = models.execute_kw(DB, uid, PASSWORD, 'mail.channel', 'search',
                                                        [[['client_id', '=', int(client_id.value)]]])
                        
                        if rec.log_attachment_ids:
                            for attachment in rec.log_attachment_ids:
                                attachments.append({
                                    'name': attachment.name,
                                    'type': attachment.type,
                                    'datas': attachment.datas,
                                    'res_model': 'mail.channel',
                                    'res_id': server_channel_id[0],
                                    'res_name': rec.name,
                                })
                            create_attachment = models.execute_kw(DB, uid, PASSWORD, 'ir.attachment', 'create',
                                                        [attachments])

                            notify = models.execute_kw(DB, uid, PASSWORD, 'mail.channel', 'send_message',
                                                        [server_channel_id[0], rec.ch_body, rec.ch_subject, create_attachment])                                
                            _logger.info('=======================================================%s', notify)
                            if notify:
                                rec.state = 'updated'
                        else:
                            notify = models.execute_kw(DB, uid, PASSWORD, 'mail.channel', 'send_message',
                                                        [server_channel_id[0], rec.ch_body, rec.ch_subject, create_attachment])                                        
                            _logger.info('=======================================================%s', notify)
                            if notify:
                                rec.state = 'updated'
                    
                    elif rec.name == 'confirm_ticket':
                        rec.state = 'updated'
                        ticket_id = self.env['client.ticket'].search([('id', '=', rec.client_ticket_id)])
                        if ticket_id:
                            ticket_id.action_ticket_confirm()
                            
                except:
                    pass
            else:
                rec.unlink()


