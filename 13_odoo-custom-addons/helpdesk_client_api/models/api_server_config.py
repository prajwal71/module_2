# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import logging

_logger = logging.getLogger(__name__)


class APIServerConfig(models.Model):
    _name = 'api.server.config'
    _description = 'Server API Config'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Many2one('res.partner', 'Server Partner', required=True)
    server_id = fields.Integer(related='name.id', string='Server ID', store=True)
    active = fields.Boolean(default=True)
    server_url = fields.Char('Server URL', required=True)
    server_database = fields.Char('Server Database Name', required=True)
    server_username = fields.Char('Server Username', required=True)
    server_password = fields.Char('Server Password', required=True)
