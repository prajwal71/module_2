# -*- coding: utf-8 -*-

from http import server
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_compare
from datetime import datetime
from json import dumps
import requests
import base64
import xmlrpc.client

import json
import logging

_logger = logging.getLogger(__name__)

# URL = 'http://localhost:8080'
# DB = 'Helpdesk_Server'
# USERNAME = 'admin'
# PASSWORD = 'admin'


class HelpdeskClientApi(models.Model):
    _name = 'client.ticket'
    _description = 'Helpdesk Client Ticket'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'create_date desc'

    TICKET_PRIORITY = [
        ('0', 'All'),
        ('1', 'Low priority'),
        ('2', 'High priority'),
        ('3', 'Urgent'),
    ]

    @api.model
    def default_get(self, fields):
        result = super(HelpdeskClientApi, self).default_get(fields)
        if 'stage_id' in fields and 'stage_id' not in result:  # if no stage given, deduce it from the team
            result['stage_id'] = 1
        return result

    name = fields.Char('Ticket Reference', default='/', copy=False, required=True, tracking=True)
    stage_id = fields.Many2one(
        'ticket.stage', string='Stage', readonly=False, ondelete='restrict', tracking=True, copy=False, index=True, )
    subject = fields.Char(string='Subject', required=True, index=True, track_visibility='always')
    description = fields.Html(required=True, tracking=True)
    active = fields.Boolean(default=True)
    priority = fields.Selection(TICKET_PRIORITY, string='Priority', default='0', tracking=True)
    user_id = fields.Many2one('res.users', string="Responsible", default=lambda self: self.env.user)
    reject_reason = fields.Text('Rejection Reason')
    ticket_sync = fields.Boolean(string='Sync Ticket', track_visibility='always')
    hour_taken = fields.Float(readonly=True)
    closing_comment = fields.Text(string="Closing Comment", readonly=True)
    closing_date = fields.Date()
    
    def open_chat(self):
        for rec in self:
            channel_id = self.env['mail.channel'].search([('server_channel_id', '!=', False)], limit=1)
            if channel_id:
                channel_id.sudo().message_post(
                    body=rec.display_name,
                    author_id= 2,
                    message_type="comment",
                    subtype="mail.mt_comment",
                )
            else:
                raise ValidationError(_('No Channel Found!!'))

    def action_notify(self, notif_type, message):

        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        action = self.env.ref('helpdesk_client_api.action_client_ticket_view')

        if notif_type == 'reject':
            
            a = self.env['bus.bus'].sendone(
                        (self._cr.dbname, 'res.partner', self.user_id.partner_id.id),
                        {'type': 'ticket_notification', 'title': _('Rejected!'), 'message': message, 'sticky': True, 'warning': True})
            return True

        elif notif_type == 'success':
            a = self.env['bus.bus'].sendone(
                        (self._cr.dbname, 'res.partner', self.user_id.partner_id.id),
                        {'type': 'ticket_notification', 'title': _('Congrats!'), 'message': message, 'url': base_url, 'tkt_id': self.id, 'tkt_name': self.name, 'act_id': action.id, 'sticky': True, 'warning': False})
            return True
        else:
            return True
    
    @api.model
    def create(self, vals):
        company_id = self.env['res.company'].search([('id', '=', 2)])
        list = [ s[0] for s in company_id.name.split() ]
        code = ''
        for x in list:
            code += x
        vals['name'] = vals.get('name') or '/'
        _logger.info("============test=====%s", vals['name'])
        if vals['name'].startswith('/'):
            seq = self.env['ir.sequence'].next_by_code('client.ticket') or '/'
            seq = seq.replace('Source', code)
            vals['name'] = str(seq)
        return super(HelpdeskClientApi, self).create(vals)

    def action_ticket_confirm(self):
        if self.ticket_sync is False:
            client_id = self.env['ir.config_parameter'].sudo().search([('key', '=', 'helpdesk_client_id')])
            support_package_code = self.env['ir.config_parameter'].sudo().search([('key', '=', 'support_package_code')])
            no_of_hour = self.env['ir.config_parameter'].sudo().search([('key', '=', 'number_of_hours')])
            ticket_solved = self.env['client.ticket'].sudo().search([('stage_id', '=', 3)])
            log_id = self.env['mail.message'].sudo().search([('model', '=', 'client.ticket'), ('message_type', '=', 'comment'), ('res_id', '=', self.id)])
            
            server = self.env['api.server.config'].sudo().search([], limit=1)
            URL = server.server_url
            DB = server.server_database
            USERNAME = server.server_username
            PASSWORD = server.server_password
            
            hours_consumed = 0.0
            if ticket_solved:
                for tkt in ticket_solved:
                    hours_consumed += tkt.hour_taken                

            if support_package_code:

                    if str(support_package_code.value) == 'hourly':
                        try:
                            common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(URL))
                            uid = common.authenticate(DB, USERNAME, PASSWORD, {})
                            models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(URL)) 
                            support_package = models.execute_kw(DB, uid, PASSWORD, 'accounting.support.package', 'search_read',[[['partner_id', '=', int(client_id.value)]]])
                            if support_package[0]['bal_amount'] > 20000.0:
                                a = self.env['bus.bus'].sendone(
                                    (self._cr.dbname, 'res.partner', self.user_id.partner_id.id),
                                    {'type': 'simple_notification', 'title': _('Warning!'), 'message': 'You have previous pending dues with us. Please contact support team!!', 'sticky': True, 'warning': True})                           
                            else:
                                for rec in self:
                                    vals = {
                                        'name': rec.subject,
                                            'description': rec.description,
                                            'client_ticket_id': rec.id,
                                            'ticket_sync': True,
                                            'origin': rec.name,
                                            'partner_id': int(client_id.value),
                                            'priority': rec.priority,
                                    }
                                create_ticket = models.execute_kw(DB, uid, PASSWORD, 'helpdesk.ticket', 'create',
                                                                [vals])
                                _logger.info('=================%s', create_ticket)
                                if log_id:
                                    for log in log_id:
                                        attachments = []
                                        create_attachment = []
                                        if log.attachment_ids:                                            
                                            for attachment in log.attachment_ids:
                                                attachments.append({
                                                    'name': attachment.name,
                                                    'type': attachment.type,
                                                    'datas': attachment.datas,
                                                    'res_model': 'helpdesk.ticket',
                                                    'res_id': create_ticket,
                                                    'res_name': self.name,
                                                })
                                            create_attachment = models.execute_kw(DB, uid, PASSWORD, 'ir.attachment', 'create',
                                                                        [attachments])
                                            _logger.info('=================%s', create_attachment)
                                            
                                        vals = {
                                            'model': 'helpdesk.ticket',
                                            'author_id': int(client_id.value),
                                            'res_id': create_ticket,
                                            'body': log.body,
                                            'subject': log.subject,
                                            'attachment_ids': create_attachment,
                                            'message_type': log.message_type,
                                            'subtype_id': log.subtype_id.id,
                                        }
                                        update_log = models.execute_kw(DB, uid, PASSWORD, 'mail.message', 'create',
                                                                    [vals])
                                        _logger.info('=================%s', update_log)

                                # a = self.env['bus.bus'].sendone(
                                #             (self._cr.dbname, 'res.partner', self.user_id.partner_id.id),
                                #             {'type': 'ticket_notification', 'title': _('Congrats!'), 'message': 'Your Ticket has been registered Successfully', 'sticky': True})
                        except:
                            self.write({'stage_id': 5})
                            search_stack = self.env['update.stack'].sudo().search([('name', '=', 'confirm_ticket'), ('client_ticket_id', '=', self.id)])
                            if not search_stack:
                                stack = self.env['update.stack'].sudo().create({
                                    'name': 'confirm_ticket',
                                    'state': 'to_update',
                                    'client_ticket_id': self.id,
                                })
                                _logger.error('-----------------Data Stack-------------------------%s', stack)
                            a = self.env['bus.bus'].sendone(
                                        (self._cr.dbname, 'res.partner', self.user_id.partner_id.id),
                                        {'type': 'ticket_notification', 'title': _('Warning!'), 'message': 'Your Ticket could not be registered at the moment and has been pushed in the queue!! We will notify once it has been registered in the system!! ', 'sticky': True, 'warning': True})

                    elif str(support_package_code.value) == 'amc':
                        if str(no_of_hour.value) == 'N/A' or (float(no_of_hour.value) - hours_consumed) > 0:
                            try:
                                common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(URL))
                                uid = common.authenticate(DB, USERNAME, PASSWORD, {})
                                models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(URL)) 
                                support_package = models.execute_kw(DB, uid, PASSWORD, 'accounting.support.package', 'search_read',[[['partner_id', '=', int(client_id.value)]]])
                                time_difference = datetime.today().date() - datetime.strptime(support_package[0]['amc_date'], '%Y-%m-%d').date()
                                if time_difference.days > 30 and support_package[0]['bal_amount'] > 0.0:
                                    a = self.env['bus.bus'].sendone(
                                        (self._cr.dbname, 'res.partner', self.user_id.partner_id.id),
                                        {'type': 'simple_notification', 'title': _('Warning!'), 'message': 'Your Amc package has expired for more than 30 days and you have outstanding dues with us. Please Contact Our team.', 'sticky': True, 'warning': True})
                                else:
                                    for rec in self:
                                        vals = {
                                        'name': rec.subject,
                                        'description': rec.description,
                                        'client_ticket_id': rec.id,
                                        'ticket_sync': True,
                                        'origin': rec.name,
                                        'partner_id': int(client_id.value),
                                        'priority': rec.priority,
                                        }
                                    create_ticket = models.execute_kw(DB, uid, PASSWORD, 'helpdesk.ticket', 'create',
                                                            [vals])
                                    _logger.info('=================%s', create_ticket)

                                    if log_id:
                                        for log in log_id:
                                            attachments = []
                                            create_attachment = []
                                            if log.attachment_ids:                                            
                                                for attachment in log.attachment_ids:
                                                    attachments.append({
                                                        'name': attachment.name,
                                                        'type': attachment.type,
                                                        'datas': attachment.datas,
                                                        'res_model': 'helpdesk.ticket',
                                                        'res_id': create_ticket,
                                                        'res_name': self.name,
                                                    })
                                                create_attachment = models.execute_kw(DB, uid, PASSWORD, 'ir.attachment', 'create',
                                                                            [attachments])
                                                _logger.info('=================%s', create_attachment)
                                                
                                            vals = {
                                                'model': 'helpdesk.ticket',
                                                'author_id': int(client_id.value),
                                                'res_id': create_ticket,
                                                'body': log.body,
                                                'subject': log.subject,
                                                'attachment_ids': create_attachment,
                                                'message_type': log.message_type,
                                                'subtype_id': log.subtype_id.id,
                                            }
                                            update_log = models.execute_kw(DB, uid, PASSWORD, 'mail.message', 'create',
                                                                        [vals])
                                            _logger.info('=================%s', update_log)
                                    # a = self.env['bus.bus'].sendone(
                                    #         (self._cr.dbname, 'res.partner', self.user_id.partner_id.id),
                                    #         {'type': 'ticket_notification', 'title': _('Congrats!'), 'message': 'Your Ticket has been registered Successfully', 'sticky': True})

                            except:
                                self.write({'stage_id': 5})
                                search_stack = self.env['update.stack'].sudo().search([('name', '=', 'confirm_ticket'), ('client_ticket_id', '=', self.id)])
                                if not search_stack:
                                    stack = self.env['update.stack'].sudo().create({
                                        'name': 'confirm_ticket',
                                        'state': 'to_update',
                                        'client_ticket_id': self.id,
                                    })
                                    _logger.error('-----------------Data Stack-------------------------%s', stack)
                                a = self.env['bus.bus'].sendone(
                                            (self._cr.dbname, 'res.partner', self.user_id.partner_id.id),
                                            {'type': 'ticket_notification', 'title': _('Warning!'), 'message': 'Your Ticket could not be registered at the moment and has been pushed in the queue!! We will notify once it has been registered in the system!! ', 'sticky': True, 'warning': True})
                        
                        else:
                            raise ValidationError(_('Cannot confirm ticket!! Your support hour limit has been reached!!'))
                    
                    elif str(support_package_code.value) == 'advance':
                        if (float(no_of_hour.value) - hours_consumed) > 0:
                            try:
                                common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(URL))
                                uid = common.authenticate(DB, USERNAME, PASSWORD, {})
                                models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(URL))                            
                                for rec in self:
                                    vals = {
                                        'name': rec.subject,
                                        'description': rec.description,
                                        'client_ticket_id': rec.id,
                                        'ticket_sync': True,
                                        'origin': rec.name,
                                        'partner_id': int(client_id.value),
                                        'priority': rec.priority,
                                    }
                                create_ticket = models.execute_kw(DB, uid, PASSWORD, 'helpdesk.ticket', 'create',
                                                                [vals])
                                _logger.info('=================%s', create_ticket)
                                if log_id:
                                    for log in log_id:
                                        attachments = []
                                        create_attachment = []
                                        if log.attachment_ids:                                            
                                            for attachment in log.attachment_ids:
                                                attachments.append({
                                                    'name': attachment.name,
                                                    'type': attachment.type,
                                                    'datas': attachment.datas,
                                                    'res_model': 'helpdesk.ticket',
                                                    'res_id': create_ticket,
                                                    'res_name': self.name,
                                                })
                                            create_attachment = models.execute_kw(DB, uid, PASSWORD, 'ir.attachment', 'create',
                                                                        [attachments])
                                            _logger.info('=================%s', create_attachment)
                                            
                                        vals = {
                                            'model': 'helpdesk.ticket',
                                            'author_id': int(client_id.value),
                                            'res_id': create_ticket,
                                            'body': log.body,
                                            'subject': log.subject,
                                            'attachment_ids': create_attachment,
                                            'message_type': log.message_type,
                                            'subtype_id': log.subtype_id.id,
                                        }
                                        update_log = models.execute_kw(DB, uid, PASSWORD, 'mail.message', 'create',
                                                                    [vals])
                                        _logger.info('=================%s', update_log)
                                # a = self.env['bus.bus'].sendone(
                                #             (self._cr.dbname, 'res.partner', self.user_id.partner_id.id),
                                #             {'type': 'ticket_notification', 'title': _('Congrats!'), 'message': 'Your Ticket has been registered Successfully', 'sticky': True})

                            except:
                                self.write({'stage_id': 5})
                                search_stack = self.env['update.stack'].sudo().search([('name', '=', 'confirm_ticket'), ('client_ticket_id', '=', self.id)])
                                if not search_stack:
                                    stack = self.env['update.stack'].sudo().create({
                                        'name': 'confirm_ticket',
                                        'state': 'to_update',
                                        'client_ticket_id': self.id,
                                    })
                                    _logger.error('-----------------Data Stack-------------------------%s', stack)
                                a = self.env['bus.bus'].sendone(
                                            (self._cr.dbname, 'res.partner', self.user_id.partner_id.id),
                                            {'type': 'ticket_notification', 'title': _('Warning!'), 'message': 'Your Ticket could not be registered at the moment and has been pushed in the queue!! We will notify once it has been registered in the system!! ', 'sticky': True, 'warning': True})
                        
                        else:
                            a = self.env['bus.bus'].sendone(
                                            (self._cr.dbname, 'res.partner', self.user_id.partner_id.id),
                                            {'type': 'ticket_notification', 'title': _('Warning!'), 'message': 'Your support hours has been consumed! Please Contact Customer Support Team!!', 'sticky': True, 'warning': True})
                    else:
                        raise ValidationError(_('No Support Package Set!!'))
            else:
                raise ValidationError(_('No Support Package Set!!'))

        else:
            raise UserError(_("Tickets already registered to the Server side."))

class TicketStage(models.Model):
    _name = 'ticket.stage'
    _description = 'Ticket Stage'
    _order = 'sequence'

    active = fields.Boolean(default=True)
    name = fields.Char(required=True, translate=True)
    description = fields.Text(translate=True)
    sequence = fields.Integer('Sequence', default=10)

class MailMessageExt(models.Model):
    _inherit = 'mail.message'

    @api.model
    def create(self, vals):
        res = super(MailMessageExt, self).create(vals)
        
        # for log note and attachment
        if res.model == 'client.ticket' and res.author_id.id == self.env.user.partner_id.id:
            ticket_id = self.env['client.ticket'].search([('id', '=', res.res_id)])
            client_id = self.env['ir.config_parameter'].sudo().search([('key', '=', 'helpdesk_client_id')])
            max_file_size = self.env['ir.config_parameter'].sudo().search([('key', '=', 'max_file_size_in_mb')])
            server = self.env['api.server.config'].sudo().search([], limit=1)
            server_id = server.server_id
            URL = server.server_url
            DB = server.server_database
            USERNAME = server.server_username
            PASSWORD = server.server_password

            if res.attachment_ids:
                for attach in res.attachment_ids:
                    if (attach.file_size/ 1024.0/ 1024.0) > float(max_file_size.value):
                        raise UserError(_('Cannot attach file larger than %.2f MB!!' % float(max_file_size.value)))
                       
            if ticket_id.ticket_sync:
                for rec in ticket_id:

                    try:
                        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(URL))
                        uid = common.authenticate(DB, USERNAME, PASSWORD, {})
                        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(URL))
                        attachments = []
                        server_ticket_id = models.execute_kw(DB, uid, PASSWORD, 'helpdesk.ticket', 'search',
                                                            [[['client_ticket_id', '=', rec.id]]])

                        if server_ticket_id:
                            if res.attachment_ids:
                                for attachment in res.attachment_ids:
                                    attachments.append({
                                        'name': attachment.name,
                                        'type': attachment.type,
                                        'datas': attachment.datas,
                                        'res_model': 'helpdesk.ticket',
                                        'res_id': server_ticket_id[0],
                                        'res_name': rec.name,
                                    })
                                create_attachment = models.execute_kw(DB, uid, PASSWORD, 'ir.attachment', 'create',
                                                            [attachments])
                                _logger.info('=================%s', create_attachment)
                                
                                vals = {
                                    'model': 'helpdesk.ticket',
                                    'author_id': int(client_id.value),
                                    'res_id': server_ticket_id[0],
                                    'body': res.body,
                                    'subject': res.subject,
                                    'attachment_ids': create_attachment,
                                    'message_type': res.message_type,
                                    'subtype_id': res.subtype_id.id,
                                }
                            else:
                                vals = {
                                    'model': 'helpdesk.ticket',
                                    'author_id': int(client_id.value),
                                    'res_id': server_ticket_id[0],
                                    'body': res.body,
                                    'subject': res.subject,
                                    'message_type': res.message_type,
                                    'subtype_id': res.subtype_id.id,
                                }

                            update_log = models.execute_kw(DB, uid, PASSWORD, 'mail.message', 'create',
                                                        [vals])
                            _logger.info('=================%s', update_log)
                    except:
                        if res.attachment_ids:
                                stack = self.env['update.stack'].sudo().create({
                                        'name': 'log/attachment',
                                        'state': 'to_update',
                                        'log_model': res.model,
                                        'log_body': res.body,
                                        'log_attachment_ids': res.attachment_ids,
                                        'log_message_type': res.message_type,
                                        'log_subtype_id': res.subtype_id.id,
                                        'client_ticket_id': rec.id,
                                    })
                        else:
                            stack = self.env['update.stack'].sudo().create({
                                    'name': 'log/attachment',
                                    'state': 'to_update',
                                    'log_model': res.model,
                                    'log_body': res.body,
                                    'log_message_type': res.message_type,
                                    'log_subtype_id': res.subtype_id.id,
                                    'client_ticket_id': rec.id,
                                })
                        _logger.error('-----------------Data Stack-------------------------%s', stack)

        
        # ===================== for communication through channel ====================== #
        if res.model == 'mail.channel':
            channel_id = self.env['mail.channel'].search([('id', '=', res.res_id)])
            client_id = self.env['ir.config_parameter'].sudo().search([('key', '=', 'helpdesk_client_id')])
            max_file_size = self.env['ir.config_parameter'].sudo().search([('key', '=', 'max_file_size_in_mb')])
            server = self.env['api.server.config'].sudo().search([], limit=1)
            server_id = server.server_id
            URL = server.server_url
            DB = server.server_database
            USERNAME = server.server_username
            PASSWORD = server.server_password
            if channel_id and res.author_id.id != server_id:
                if channel_id.server_channel_id:
                    for rec in channel_id:

                        if res.attachment_ids:
                            for attach in res.attachment_ids:
                                if (attach.file_size/ 1024.0/ 1024.0) > float(max_file_size.value):
                                    raise UserError(_('Cannot attach file larger than %.2f MB!!' % float(max_file_size.value)))

                        try:
                            common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(URL))
                            uid = common.authenticate(DB, USERNAME, PASSWORD, {})
                            models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(URL))
                            
                            server_channel_id = models.execute_kw(DB, uid, PASSWORD, 'mail.channel', 'search',
                                                                [[['client_id', '=', int(client_id.value)]]])
                            _logger.info('===========server_channel_id======%s', server_channel_id)

                            if server_channel_id:
                                attachments = []
                                create_attachment = []
                                if res.attachment_ids:
                                    for attachment in res.attachment_ids:
                                        attachments.append({
                                            'name': attachment.name,
                                            'type': attachment.type,
                                            'datas': attachment.datas,
                                            'res_model': 'mail.channel',
                                            'res_id': server_channel_id[0],
                                            'res_name': rec.name,
                                        })
                                    create_attachment = models.execute_kw(DB, uid, PASSWORD, 'ir.attachment', 'create',
                                                                [attachments])
                                    _logger.info('===========create_attachment======%s', create_attachment)

                                update_message = models.execute_kw(DB, uid, PASSWORD, 'mail.channel', 'send_message',
                                                            [server_channel_id[0], int(client_id.value), res.body, res.subject, create_attachment])
                                _logger.info('=========update_message========%s', update_message)
                        except:
                            if res.attachment_ids:
                                stack = self.env['update.stack'].sudo().create({
                                        'name': 'channel/attachment',
                                        'state': 'to_update',
                                        'ch_body': res.body,
                                        'ch_subject': res.subject,
                                        'log_attachment_ids': res.attachment_ids,
                                    })
                            else:
                                stack = self.env['update.stack'].sudo().create({
                                        'name': 'channel/attachment',
                                        'state': 'to_update',
                                        'ch_body': res.body,
                                        'ch_subject': res.subject,
                                    })
                            _logger.error('-----------------Data Stack-------------------------%s', stack)

                
        return res

class MailChannelExt(models.Model):
    _inherit = 'mail.channel'

    server_channel_id = fields.Integer('Server Channel ID')


    @api.model
    def send_message(self, sid, body, subject, attachments):
        channel = self.env['mail.channel'].sudo().search([('id', '=', sid)],limit=1)
        server = self.env['api.server.config'].sudo().search([], limit=1)
        server_id = server.server_id
        if channel:
            # send a message to the related user
            channel.sudo().message_post(
                body=body,
                author_id=server_id,
                subject=subject,
                attachment_ids=attachments,
                message_type="comment",
                subtype="mail.mt_comment",
            )
        return True