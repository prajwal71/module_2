# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Helpdesk Client API',
    'version': '13.0',
    'category': 'Helpdesk',
    'summary': 'Helpdesk Ticket Client API',
    'author': 'Nexus Incorporation',
    'description': """
The aim is to have a complete module to manage all client tickets and helps to communicate with server for ticket for tracking.
====================================================================

The following topics are covered by this module:
------------------------------------------------------
    * Add/remove client Tickets.
    * Track Ticket Stage.
    * Get notified about ticket.
""",
    'depends': ['base', 'mail', 'bus'],
    'data': [
        'security/helpdesk_security.xml',
        'security/ir.model.access.csv',
        'views/client_ticket_views.xml',
        'views/update_stack_view.xml',
        'views/api_server_config.xml',
        'views/assets.xml',
        'data/ir_sequence.xml',
        'data/ticket_data.xml',
        'data/cron.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'LGPL-3',
}
