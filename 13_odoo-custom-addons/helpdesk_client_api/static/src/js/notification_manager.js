odoo.define('helpdesk_client_api.NotificationManager', function (require) {
    "use strict";
    
    var MailManager = require('mail.Manager');
    const {sprintf} = require('web.utils')
    MailManager.include({
    
        //--------------------------------------------------------------------------
        // Handlers
        //--------------------------------------------------------------------------
    
        /**
         * @override
         * @param {Object} data structure depending on the type
         * @param {integer} data.id
         */
        _handlePartnerNotification: function (data) {
            if (data.type === 'ticket_notification') {
                var title = _.escape(data.title), message = _.escape(data.message), url = _.escape(data.url), tkt_id = _.escape(data.tkt_id), act_id = _.escape(data.act_id), tkt_name = _.escape(data.tkt_name);
                data.warning ? this.failure_notify(title, message, data.sticky) : this.success_notify(title, message, url, tkt_id, tkt_name, act_id, data.sticky);
            } else {
                this._super.apply(this, arguments);
            }
        },
        
        success_notify: function (title, message, url, tkt_id, tkt_name, act_id, sticky, className) {
            console.warn(title, message, url, tkt_id, tkt_name, act_id);
            var url_link = `/web#id=${_.escape(tkt_id)}&action=${_.escape(act_id)}&model=client.ticket&view_type=form`
            var links = `<a href="${_.escape(url)}${_.escape(url_link)}" target="_blank">${_.escape(tkt_name)}</a>`
            this.displayNotification({
                type: 'success',
                title: title,
                message: 'Your' + ' ' + links + ' ' + message,
                sticky: sticky,
                className: className,
                messageIsHtml: true,
            });
        },

        failure_notify: function (title, message, sticky, className) {
            console.warn(title, message);
            return this.displayNotification({
                type: 'danger',
                title: title,
                message: message,
                sticky: sticky,
                className: className,
            });
        },
    });
    
    });
    