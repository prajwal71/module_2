# -*- coding: utf-8 -*-
# import base64
# import json
# import logging
# from odoo.http import request

# from odoo import http, _, SUPERUSER_ID

# _logger = logging.getLogger(__name__)
# header = [('Content-Type', 'application/json'),
#           ('Cache-Control', 'no-store')]


# class UpdateTicket(http.Controller):
#     @http.route('/update-ticket/<int:ticket_id>/', type='json', auth="public", methods=['POST'], csrf=False)
#     def update_ticket(self, ticket_id=None, **kwargs):
#         api_key = kwargs.get('api_key') or False
#         _logger.info("======")
#         _logger.info(request.env.user)
#         if api_key:
#             if api_key == self.env['ir.config_parameter'].search([('key', '=', 'helpdesk_api_key')]).value:
#                 ticket_rec = request.env['client.ticket'].sudo().search([('id', '=', ticket_id)])
#                 if ticket_rec:
#                     ticket_rec.write(kwargs.get('vals'))
#                 else:
#                     res = {'result': 'Ticket Not Found'}
#                     return json.dumps(res)
#             else:
#                 res = {'result': 'API Key Not Found'}
#                 return json.dumps(res)
#         else:
#             res = {'result': 'API Key Not Found'}
#             return json.dumps(res)
