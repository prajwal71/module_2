
from odoo import models, fields, api


class ApplicantExam_Ext (models.Model):
    _inherit = "hr.applicant"

    profile_pic =fields.Binary(related="partner_id.image_1920", string="Applicants Profile Picture")
