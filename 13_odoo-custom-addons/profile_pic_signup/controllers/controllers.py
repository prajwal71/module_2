from os import stat
import werkzeug
from odoo.http import request
from odoo import api, http, _
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID, http, _
from dateutil.relativedelta import relativedelta
import base64
import logging
import json
from odoo.tools.safe_eval import safe_eval
_logger = logging.getLogger(__name__)
from odoo.addons.portal.controllers.portal \
import CustomerPortal as CustomerPortal
from odoo.addons.cv_maker.controllers.controllers \
import CreateCv as CreateCv
from odoo.addons.website_hr_recruitment.controllers.main \
    import WebsiteHrRecruitment as Home




class InheritedCustomerPortal_ProgressBar_ext(CustomerPortal):
    """Updated the Optional fields list to stop the unkown fields error For Gender Birthday and Marital."""
    MANDATORY_BILLING_FIELDS = ["name", "email", "street", "city", "country_id", "profile_pic"]
    OPTIONAL_BILLING_FIELDS = [ "zipcode", "state_id", "phone", "vat","mobile", "company_name", "gender", "birthday","marital","street2",
                               "organization","location", "study_field",
                               "grade", "job_position","start_date", "description",
                               "end_date", "qualification", "certification",
                               "operation_type", "opr_id", "tr_no",
                               "is_still"]

class CreateCv_ext(CreateCv):

    @http.route('/cvmaker/create', type='http', auth='user', website=True)
    def show_custom_webpage(self, **kw):

        _logger.info("=================PARTNERSSSSSSSSSSSS==========%======")
        countries = request.env['res.country'].sudo().search([])
        states = request.env['res.country.state'].sudo().search([])
        _logger.info("----------------STATES___________%s", states)
        partner = request.env.user.partner_id
        
        _logger.info("=================PARTNER==========%s======", partner)
        return http.request.render('cv_maker.cv_maker_webPage', {"countries": countries, "states": states, 'partner': partner})

    @http.route('/cvmaker', type="http", auth='public', website=True)
    def show_custom_options(self, **kw):

        return http.request.render('cv_maker.cv_options')



    @http.route(["/create/cv"], methods=['POST'], type="http", auth="user", website=True)
    def cvMaker(self, res_id=None, **post):
        profile_pic = post['profile_pic']
        name = post['name']
        email = post['email']
        mobile = post['mobile']
        street = post['street']
        gender = post['gender']
        marital = post['marital']
        birthday = post["birthday"]
        _logger.info(
            "------------------------------------COUNTRY---------------%s", post["country_id"])
        country_id = int(post["country_id"])
        street = post["street"]
        street2 = post["street2"]
        city = post["city"]
        if post['state_id']:
            state_id= int (post["state_id"])
        else :
            state_id = False
        zip = post["zip"]
        website=post["website"]
        _logger.info("------------------------------------C---------------")
        _logger.info(
            "------------------------------------NAME---------------%s", name)
        _logger.info(
            "------------------------------------MOBILE---------------%s", mobile)
        _logger.info(
            "------------------------------------GENDER---------------%s", gender)

        update_partner = request.env.user.partner_id
        _logger.info(
            "=================UPDATE PARTNER==========%s======", update_partner)
       
        partner_id=request.env.user.partner_id
        _logger.info(
            "------------------------------------PARTNER---------------%s", partner_id)
        if post.get('profile_pic',False):
                            Attachments = request.env['ir.attachment']
                            # name = post.get('profile_pic').filename      
                            files = post.get('profile_pic').stream.read()
                            


        partner_post = update_partner.sudo().write({ "name": name, "gender": gender, "email": email, "mobile": mobile, "marital": marital, "birthday": birthday,
                                                    "country_id": country_id,  "state_id":state_id, "street": street, "street2": street2, "city": city,  "zip": zip, "website": website, "image_1920":base64.encodestring(files)})
        
        

        






        # Redirect to PDF 
        ret_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url') + '/report/pdf/cv_maker.page_printcv/' + str(request.env.user.partner_id.id)
        return request.redirect (ret_url)


class WebsiteHrRecruitment_Ext(Home):  

    @http.route('/jobs/apply/<model("hr.job"):job>', type='http',
                auth="user", website=True)
    def jobs_apply(self, job, **kwargs):
        
        error = {}
        default = {}
        env = request.env(context=dict(request.env.context,
                                       show_address=True, no_tag_br=True))
        applicant_1 = request.env['hr.applicant'] \
            .search([('partner_id', '=', request.env.user.partner_id.id)],
                    limit=1, order='create_date desc')
        countries = env['res.country'].sudo().search([])
        states = env['res.country.state'].sudo().search([])

        if 'website_hr_recruitment_error' in request.session:
            error = request.session.pop('website_hr_recruitment_error')
            default = request.session.pop('website_hr_recruitment_default')
        return request.render("website_hr_recruitment.apply", {
            'applicant': applicant_1,
            'job': job,
            'error': error,
            'default': default,
            'countries': countries,
            'states': states,
            'partner': request.env['res.users'].sudo().browse(
                request.uid).partner_id,
        })

    def _get_applicant_char_fields(self):
        
        return [ 'gender', 
                 'marital','partner_mobile', 'email_from',
                'street_ht', 'street2_ht', 'city_ht', 'zip_ht',
                ]


    def _get_applicant_relational_fields(self):
        """A Helper Method to get the applicants's Relational fields."""
        return ['department_id', 'job_id', 'country_id', 'state_id_ht']

    def _get_applicant_files_fields(self):
        """A Helper Method to get the applicants's File fields."""
        return ['ufile']

    def _get_residential_address(self, kwargs):
        """A Helper Method to get the applicants's residential address."""
        address = {
            'name': kwargs.get('name'),
            'street': kwargs.get('street'),
            'street2': kwargs.get('street2') or '',
            'city': kwargs.get('city'),
            'zip': kwargs.get('zip'),
            'state_id': kwargs.get('state_id'),
            'country_id': kwargs.get('country_id'),
            'mobile': kwargs.get('partner_mobile'),
            'email': kwargs.get('email_from'),
            'customer': False,
        }
        return address

    def _format_date(self, date):
        """A Helper Method to get the formated value of the date."""
        if date:
            return datetime.strptime(date, "%m/%d/%Y").isoformat(' ')
        return False

    @http.route('/test', type='http', auth="public",
                website=True)
    def test(self, **kwargs):
        return request.render("auth_signup.signup", {})

    @http.route('/jobs/thankyou', methods=['POST'], type='http', auth="public",
                website=True)
    def jobs_thankyou(self, **kwargs):
        """A Method for rendering thankyou template after applying for the job.replace
        Args:
            kwargs: The second parameter to get the details of
            the job applicant.

        Returns:
            It renders the template & creates a record for job
            application also attaches the applicant's attachment to the object.

        """
       
        application = request.env['hr.applicant']

        env = request.env(user=SUPERUSER_ID)
        partner = env['res.partner'].sudo().browse(int(kwargs.get('partner_id')))
        job_my = env['hr.job'].sudo().search([('id', '=', int(kwargs.get('job_id')))], limit=1)
        vals = {
            'source_id': 1,
            'name': kwargs.get('name') + "'s application",
            'partner_name': kwargs.get('name') or '',
            'email_from': kwargs.get('email_from'),
            'user_id': job_my.user_id.id or False,
            'academic_ids': [(6, 0, partner.academic_ids.ids)],
            'experience_ids': [(6, 0, partner.experience_ids.ids)],
            'certification_ids': [(6, 0, partner.certification_ids.ids)],
            'country_id': partner.country_id.id,
            'partner_id': partner.id,
            
        }

       

        for field in self._get_applicant_char_fields():
            vals[field] = kwargs.get(field)

        for field in self._get_applicant_relational_fields():
            vals[field] = int(kwargs.get(field) or False)
        state = False
       
        if kwargs.get('state_id', False) != '':
            state = kwargs.get('state_id', False)
       
        vals['country_id'] = kwargs.get('country_id')
        vals['street_ht'] = kwargs.get('street')
        vals['street2_ht'] = kwargs.get('street2')
        vals['city_ht'] = kwargs.get('city')
        vals['zip_ht'] = kwargs.get('zip')
        vals['state_id_ht'] = kwargs.get('state_id')
       
        vals['marital'] = kwargs.get('marital')
        vals['gender'] = kwargs.get('gender')
        vals['birthday'] = kwargs.get('birthday')

        if kwargs.get('profile_pic',False):
                            Attachments = request.env['ir.attachment']
                            # name = post.get('profile_pic').filename      
                            files = kwargs.get('profile_pic').stream.read()
                            
      
        partner.write({
                       'country_id': kwargs.get('country_id'), 'state_id': kwargs.get('state_id'),
                       'city': kwargs.get('city'), 'street': kwargs.get('street'), 'street2': kwargs.get('street2'),
                       "image_1920":base64.encodestring(files)})


        _logger.info("=================PARTNERSSSSSSSSSSSS==========%s======",base64.encodestring(files))
        _logger.info("=========================%s",kwargs.get('email_from'))
        redundant_check = application.sudo().search([('email_from', '=', kwargs.get('email_from'))], limit=1).id
        _logger.info("=========================%s",redundant_check)
        if redundant_check:
            redundant = application.sudo().search(
                [('email_from', '=', kwargs.get('email_from')), (
                    'job_id', '=', int(kwargs.get('job_id')))], limit=1).id
            if redundant:
                applicant_1 = application.sudo().search(
                    [('partner_id', '=', request.env.user.partner_id.id)],
                    limit=1, order='create_date desc')
                applicant_1.write(vals)
                return request.render('job_portal.update_sorry', {})
        applicant_id = application.sudo().create(vals).id

        for field_name in self._get_applicant_files_fields():
            if kwargs[field_name]:
                attachment_vals = {
                    'name': kwargs[field_name].filename,
                    'res_name': vals['name'],
                    'res_model': 'hr.applicant',
                    'res_id': applicant_id,
                    'datas': base64.encodestring(kwargs[field_name].read()),
                }
                env['ir.attachment'].create(attachment_vals)
        return request.render("website_hr_recruitment.thankyou", {})




