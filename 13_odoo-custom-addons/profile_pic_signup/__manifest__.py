# -*- coding: utf-8 -*-
{
    'name': "Profile Pic in CV",

    'summary': """
        This Module enables Profile pic in CV Maker""",

    'description': """
        Long description of module's purpose
    """,

    'author': "NEXUS Incorporation",
    'website': "http://www.nexusgurus.com",

    
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr_recruitment','job_portal','cv_maker'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    # 'demo': [
    #     'demo/demo.xml',
    # ],
}
