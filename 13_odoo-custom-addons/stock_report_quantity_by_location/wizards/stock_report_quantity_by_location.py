# -*- coding: utf-8 -*-
import base64
from odoo import _, fields, models

import xlwt


class StockReportByLocationPrepare(models.TransientModel):
    _name = "stock.report.quantity.by.location.prepare"
    _description = "Stock Report Quantity By Location Prepare"

    location_ids = fields.Many2many(
        comodel_name="stock.location", string="Locations", required=True, domain=[('usage', '=', 'internal')]
    )
    with_quantity = fields.Boolean(
        string="Quantity > 0",
        default=True,
        help="Show only the products that have existing quantity on hand",
    )

    def open(self):
        self.ensure_one()
        self._compute_stock_report_by_location()
        action = {
            "type": "ir.actions.act_window",
            "view_mode": "pivot,tree",
            "name": _("Stock Report by Location"),
            "context": {
                "search_default_quantity_gt_zero": 1,
                "group_by_no_leaf": 1,
                "group_by": [],
            },
            "res_model": "stock.report.quantity.by.location",
            "domain": [("wiz_id", "=", self.id)],
        }
        return action

    def _compute_stock_report_by_location(self):
        self.ensure_one()
        recs = []
        for loc in self.location_ids:
            quant_groups = self.env["stock.quant"].read_group(
                [("location_id", "=", [loc.id])],
                ["quantity", "reserved_quantity", "product_id"],
                ["product_id"],
            )
            mapping = {}
            for quant_group in quant_groups:
                qty_on_hand = quant_group["quantity"]
                qty_reserved = quant_group["reserved_quantity"]
                qty_unreserved = qty_on_hand - qty_reserved
                qty_dict = {
                    "quantity_on_hand": qty_on_hand,
                    "quantity_reserved": qty_reserved,
                    "quantity_unreserved": qty_unreserved,
                }
                mapping.setdefault(quant_group["product_id"][0], qty_dict)
            products = self.env["product.product"].search([("type", "=", "product")])
            vals_list = []
            count = 1
            for product in products:
                qty_dict = mapping.get(product.id, {})
                qty_on_hand = qty_dict.get("quantity_on_hand", 0.0)
                qty_reserved = qty_dict.get("quantity_reserved", 0.0)
                qty_unreserved = qty_dict.get("quantity_unreserved", 0.0)
                standard_price = product.standard_price
                if count ==1:
                    st_price = product.standard_price
                    count = count + 1
                else:
                    st_price = 0
                total_value = standard_price * qty_on_hand
                if (self.with_quantity and qty_on_hand) or not self.with_quantity:
                    vals_list.append(
                        {
                            "product_id": product.id,
                            "product_category_id": product.categ_id.id,
                            "uom_id": product.uom_id.id,
                            "quantity_on_hand": qty_on_hand,
                            "quantity_reserved": qty_reserved,
                            "quantity_unreserved": qty_unreserved,
                            "standard_price": standard_price,
                            "st_price" : st_price,
                            "total_value": total_value,
                            "location_id": loc.id,
                            "wiz_id": self.id,
                            "default_code": product.default_code,
                        }
                    )
            recs = self.env["stock.report.quantity.by.location"].create(vals_list)
        return recs.ids

    def action_valuation_report(self):
        m_date_from = str(self.date_from)
        m_date_to = str(self.date_to)
        workbook = xlwt.Workbook()
        format0 = xlwt.easyxf(
            'font:height 300, bold True; pattern: pattern solid, fore_colour gray25; align: horiz center')
        format1 = xlwt.easyxf('font:bold True; pattern: pattern solid, fore_colour gray25; align: horiz left')
        format3 = xlwt.easyxf('align: horiz left')
        format4 = format3
        format5 = xlwt.easyxf('font:bold True; align: horiz left;')
        format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour tan; align: horiz left')

        sheet = workbook.add_sheet("Partner_detailed_report", cell_overwrite_ok=True)
        sheet.col(0).width = int(15 * 260)
        sheet.col(1).width = int(60 * 260)
        sheet.col(2).width = int(16 * 260)
        sheet.col(3).width = int(16 * 260)
        sheet.col(4).width = int(16 * 260)
        sheet.col(5).width = int(16 * 260)
        sheet.col(6).width = int(16 * 260)
        sheet.col(7).width = int(12 * 260)
        sheet.col(8).width = int(15 * 260)
        sheet.write_merge(0, 1, 0, 5, 'Stock Valuation Report' + " from " + str(
            self.date_from) + " to " + str(self.date_to), format0)
        sheet.write(2, 0, str("Company"), format1)
        sheet.write(2, 1, str(self.env.company.name), format1)

        sheet.write(4, 0, "Reference", format1)
        sheet.write(4, 1, "Product", format1)
        sheet.write(4, 2, "Opening Qty", format1)
        sheet.write(4, 3, "Opening Balance", format1)
        sheet.write(4, 4, "Closing Qty", format1)
        sheet.write(4, 5, "Closing Balance", format1)

        self._cr.execute(
            """SELECT product_name, default_code, SUM(opening_qty) opening_qty, SUM(opening_balance) opening_balance, 
            SUM(closing_qty) closing_qty, SUM(closing_balance) closing_balance from
            (SELECT pt.name product_name, pt.default_code, SUM(aml.quantity) opening_qty, 
            SUM(aml.balance) opening_balance, 0 closing_qty, 0 closing_balance
            FROM account_move_line aml 
            LEFT JOIN account_account acc ON aml.account_id = acc.id 
            LEFT JOIN product_product pp 
            LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id ON aml.product_id = pp.id 
            LEFT JOIN product_category pc on pc.id = pt.categ_id WHERE acc.is_stock_account = TRUE 
            AND aml.parent_state = 'posted' AND aml.date<%s GROUP BY pt.name, pt.default_code
            UNION ALL
            SELECT pt.name product_name, pt.default_code, 0 opening_qty, 0 opening_balance, 
            SUM(aml.quantity) closing_qty, SUM(aml.balance) closing_balance 
            FROM account_move_line aml 
            LEFT JOIN account_account acc ON aml.account_id = acc.id 
            LEFT JOIN product_product pp 
            LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id ON aml.product_id = pp.id 
            LEFT JOIN product_category pc on pc.id = pt.categ_id WHERE acc.is_stock_account = TRUE
            AND aml.parent_state = 'posted' AND aml.date<%s GROUP BY pt.name, pt.default_code)result 
            GROUP BY product_name, default_code""",
            (m_date_from, m_date_to,))
        result = self._cr.dictfetchall()
        row = 5
        opening_qty = 0.00
        opening_balance = 0.00
        closing_qty = 0.00
        closing_balance = 0.00
        for data in result:
            opening_quantity = float(data['opening_qty'])
            opening_amount = float(data['opening_balance'])
            closing_quantity = float(data['closing_qty'])
            closing_amount = float(data['closing_balance'])
            opening_qty = opening_qty + opening_quantity
            opening_balance = opening_balance + opening_amount
            closing_qty = closing_qty + closing_quantity
            closing_balance = closing_balance + closing_amount

            sheet.write(row, 0, data['default_code'], format3)
            sheet.write(row, 1, str(data['product_name']), format3)
            sheet.write(row, 2, str(data['opening_qty']), format3)
            sheet.write(row, 3, str(data['opening_balance']), format3)
            sheet.write(row, 4, str(data['closing_qty']), format3)
            sheet.write(row, 5, str(data['closing_balance']), format3)
            row = row + 1
        sheet.write(row + 1, 1, "Total", format1)
        sheet.write(row + 1, 2, opening_qty, format1)
        sheet.write(row + 1, 3, opening_balance, format1)
        sheet.write(row + 1, 4, closing_qty, format1)
        sheet.write(row + 1, 5, closing_balance, format1)

        path = ("/home/odoo/reports/Stock_valuation_report.xls")
        workbook.save(path)
        file = open(path, "rb")
        file_data = file.read()
        out = base64.encodebytes(file_data)
        self.write({'state': 'get', 'file_name': out, 'invoice_data': 'Stock_Valuation_Report.xls'})
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'nexus.valuation.report',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.id,
            'target': 'new',
        }


class StockReportQuantityByLocation(models.TransientModel):
    _name = "stock.report.quantity.by.location"
    _description = "Stock Report By Location"

    wiz_id = fields.Many2one(comodel_name="stock.report.quantity.by.location.prepare")
    product_id = fields.Many2one(comodel_name="product.product", required=True)
    product_category_id = fields.Many2one(
        comodel_name="product.category", string="Product Category"
    )
    location_id = fields.Many2one(comodel_name="stock.location", required=True)
    quantity_on_hand = fields.Float(string="Qty On Hand")
    quantity_reserved = fields.Float(string="Qty Reserved")
    quantity_unreserved = fields.Float(string="Qty Unreserved")
    standard_price = fields.Float(string="Standard Price")
    total_value = fields.Float(string="Total Value")
    uom_id = fields.Many2one(comodel_name="uom.uom", string="Product UoM")
    default_code = fields.Char("Internal Reference")
    st_price = fields.Float(string="Standard Price2")