# -*- coding: utf-8 -*-
{
    'name': "BFIN Advetisement",

    'summary': """
        This module Places Ads in website
        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "Nexus Incorporation",
    'website': "http://www.nexusgurus.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','website'],

    # always loaded
    'data': [
       
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    
}
