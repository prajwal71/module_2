# -*- coding: utf-8 -*-

from odoo import models, fields, api


class bfin_ads(models.Model):
    _inherit = 'website'
   
    ad_image1= fields.Binary(string="Ad Image 1")
    ad_image2= fields.Binary(string="Ad Image 2")
    ad_image3= fields.Binary(string="Ad Image 3")
    ad_image4= fields.Binary(string="Ad Image 4")
    ad_image5= fields.Binary(string="Ad Image 5")
    ad_image6= fields.Binary(string="Ad Image 6")

   
