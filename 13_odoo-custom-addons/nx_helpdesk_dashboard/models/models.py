# -*- coding: utf-8 -*-
##############################################################################


from odoo import models, fields, api
from odoo.http import request
import datetime
import logging
import xmlrpc.client
_logger = logging.getLogger(__name__)
from datetime import timedelta
from dateutil.relativedelta import relativedelta

# URL = 'http://localhost:8080'
# DB = 'Helpdesk_Server'
# USERNAME = 'admin'
# PASSWORD = 'admin'

class HelpdeskDashboard(models.Model):
    _name = 'helpdesk.dashboard'
    _description = 'Helpdesk Dashboard'

    name = fields.Char("")

    def open_chat(self):
        channel = self.env['mail.channel'].sudo().search([('server_channel_id', '!=', False)],limit=1)
        server_id = self.env['ir.config_parameter'].sudo().search([('key', '=', 'helpdesk_server_partner_id')])
        if channel:
            # send a message to the related user
            channel.sudo().message_post(
                body='',
                author_id=int(server_id),
                subject='',
                message_type="comment",
                subtype="mail.mt_comment",
            )
        return True

    @api.model
    def get_ticket_info(self, uid):
        """
        """
        usr_id = uid
        cr = self.env.cr
        tickets_lst = False
        ticket_solved_lst = False
        ticket_cancel_lst = False
        ticket_progress_lst = False
        ticket_cancel_count = 0
        ticket_progress_count = 0
        ticket_solved_count = 0
        ticket_count = 0
        hours_consumed = 0.0
        hours_consumed_this_month = 0.0
        hour_invoiced = 0.0
        remaining_to_invoice = 0.0
        ticket_data = []
        remaining_hrs =  'N/A'
        service_package = 'N/A'

        server = self.env['api.server.config'].sudo().search([], limit=1)
        URL = server.server_url
        DB = server.server_database
        USERNAME = server.server_username
        PASSWORD = server.server_password

        try:
            client_id = self.env['ir.config_parameter'].sudo().get_param('helpdesk_client_id')
            common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(URL))
            uid = common.authenticate(DB, USERNAME, PASSWORD, {})
            models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(URL)) 
            support_package = models.execute_kw(DB, uid, PASSWORD, 'accounting.support.package', 'search_read',[[['partner_id', '=', int(client_id)]]])
            if support_package[0]['hour_invoiced']:
                hour_invoiced = support_package[0]['hour_invoiced']
        except:
            pass
        
        user_id = self.env['res.users'].sudo().search_read([('id', '=', usr_id)], limit=1)
        company_id = self.env['res.company'].sudo().search([('id', '=', 1)], limit=1)
        tickets = self.env['client.ticket'].sudo().search([],order="id desc")
        ticket_progress = self.env['client.ticket'].sudo().search([('stage_id', '=', 2)])
        ticket_cancel = self.env['client.ticket'].sudo().search([('stage_id', '=', 4)])
        ticket_solved = self.env['client.ticket'].sudo().search([('stage_id', '=', 3)])
        remain_hrs = self.env['ir.config_parameter'].sudo().get_param('number_of_hours')        
        service_pkg = self.env['ir.config_parameter'].sudo().get_param('support_package_code')
        database_create = self.env['ir.config_parameter'].sudo().get_param('database.create_date')
        database_expire = self.env['ir.config_parameter'].sudo().get_param('database.expiration_date')
        if remain_hrs and str(remain_hrs) != 'N/A':
            remaining_hrs = float(remain_hrs)
        if service_pkg:
            if str(service_pkg) == 'hourly':
                service_package = 'Hourly Support'
            elif str(service_pkg) == 'amc':
                service_package = 'AMC Based Support'
            elif str(service_pkg) == 'advance':
                service_package = 'Advanced Support'
            else:
                service_package = 'N/A'
        if tickets:
            tickets_lst = [ids.id for ids in tickets]
            ticket_count = len(tickets)
        if ticket_progress:
            ticket_progress_lst = [ids.id for ids in ticket_progress]
            ticket_progress_count = len(ticket_progress)
        if ticket_cancel:
            ticket_cancel_lst = [ids.id for ids in ticket_cancel ]
            ticket_cancel_count = len(ticket_cancel)
        if ticket_solved:
            ticket_solved_lst = [ids.id for ids in ticket_solved]
            ticket_solved_count = len(ticket_solved)
            for tkt in ticket_solved:
                if tkt.closing_date and tkt.closing_date.year == fields.Date.today().year and tkt.closing_date.month == fields.Date.today().month:
                    hours_consumed_this_month += tkt.hour_taken
                hours_consumed += tkt.hour_taken
            if str(remain_hrs) != 'N/A':
                remaining_hrs = remaining_hrs - hours_consumed

        if hours_consumed >= hour_invoiced:
            remaining_to_invoice = hours_consumed - hour_invoiced

        if tickets:
            for data in tickets:
                ticket_data.append(
                    {
                        'name': data.name,
                        'subject': data.subject,
                        'user_id': data.user_id.name,
                        'priority': data.priority,
                        'stage_id': data.stage_id.name,
                        'hour_taken': data.hour_taken,
                        'id': data.id,
                    })
        if user_id:            
            data = {
                'remaining_hrs':remaining_hrs,
                'service_package': service_package,
                'tickets_lst': tickets_lst,
                'ticket_progress_lst':ticket_progress_lst,
                'ticket_cancel_lst':ticket_cancel_lst,
                'ticket_solved_lst':ticket_solved_lst,
                'ticket_data': ticket_data,
                'ticket_cancel_count':ticket_cancel_count,
                'ticket_progress_count':ticket_progress_count,
                'ticket_solved_count':ticket_solved_count,
                'ticket_count': ticket_count,
                'company_name': company_id.name,
                'company_img': company_id.logo,
                'database_create': str(database_create),
                'database_expire': str(database_expire),
                'hours_consumed': hours_consumed,
                'hours_consumed_this_month': hours_consumed_this_month,
                'hour_invoiced': hour_invoiced,
                'remaining_to_invoice': remaining_to_invoice,
            }
            user_id[0].update(data)
        return user_id
