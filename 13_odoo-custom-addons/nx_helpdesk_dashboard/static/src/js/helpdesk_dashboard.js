odoo.define('nx_helpdesk_dashboard.dashboard', function (require) {
    "use strict";
    
    var core = require('web.core');
    var framework = require('web.framework');
    var session = require('web.session');
    var ajax = require('web.ajax');
    var AbstractAction = require('web.AbstractAction');
    var QWeb = core.qweb;
    
    var _t = core._t;
    var _lt = core._lt;
    var user = session.uid;
    var HelpdeskDashboardView = AbstractAction.extend({
        events: {
            'click .cancel-det': 'action_cancel',
            'click .progress-det': 'action_progress',
            'click .solved-det': 'action_solved',
            'click .total-det': 'action_total_ticket',
            'click .create-det': 'action_create_ticket',
            'click .open-chat': 'action_open_chat',
        },
        init: function(parent, context) {
            this._super(parent, context);
            var ticket_data = [];
            var self = this;
            if (context.tag == 'helpdesk_dashboard_main') {
                self._rpc({
                    model: 'helpdesk.dashboard',
                    method: 'get_ticket_info',
                    args: [user],
                }, []).then(function(result){
                    self.ticket_data = result[0]
                    console.log("oooooooooooo");
                    console.log(result[0]);
                    self.render();
                    self.href = window.location.href;
                });
            }
        },
        willStart: function() {
             return $.when(ajax.loadLibs(this), this._super());
        },
        start: function() {
            var self = this;
            return this._super();
        },
        render: function() {
            var super_render = this._super;
            var self = this;
            var helpdesk_dashboard = QWeb.render('helpdesk_dashboard_main', {
                widget: self,
            });
            $( ".o_control_panel" ).addClass( "o_hidden" );
            $(helpdesk_dashboard).prependTo(self.$el);
            // self.graph();
            self.previewTable();
            return helpdesk_dashboard
        },
        reload: function () {
                window.location.href = this.href;
        },

        action_cancel: function(event) {
            var self = this;
            event.stopPropagation();
            event.preventDefault();
           console.log(self.ticket_data.ticket_cancel_lst);
            this.do_action({
                name: _t("Tickets"),
                type: 'ir.actions.act_window',
                res_model: 'client.ticket',
                view_mode: 'tree,form',
                view_type: 'form',
                views: [[false, 'list'],[false, 'form']],
               
               domain: [["id","in", self.ticket_data.ticket_cancel_lst]],
                target: 'current'
            },{on_reverse_breadcrumb: function(){ return self.reload();}})
        },
        action_progress: function(event) {
            var self = this;
            event.stopPropagation();
            event.preventDefault();
            this.do_action({
                name: _t("Tickets"),
                type: 'ir.actions.act_window',
                res_model: 'client.ticket',
                view_mode: 'tree,form',
                view_type: 'form',
                views: [[false, 'list'],[false, 'form']],
                domain: [["id","in", self.ticket_data.ticket_progress_lst]],
                target: 'current'
            },{on_reverse_breadcrumb: function(){ return self.reload();}})
        },
        action_solved: function(event) {
            var self = this;
            event.stopPropagation();
            event.preventDefault();
            this.do_action({
                name: _t("Tickets"),
                type: 'ir.actions.act_window',
                res_model: 'client.ticket',
                view_mode: 'tree,form',
                view_type: 'form',
                views: [[false, 'list'],[false, 'form']],
                domain: [["id","in", self.ticket_data.ticket_solved_lst]],
                target: 'current'
            },{on_reverse_breadcrumb: function(){ return self.reload();}})
        },

        action_total_ticket: function(event) {
            var self = this;
            event.stopPropagation();
            event.preventDefault();
            this.do_action({
                name: _t("Tickets"),
                type: 'ir.actions.act_window',
                res_model: 'client.ticket',
                view_mode: 'tree,form',
                view_type: 'form',
                views: [[false, 'list'],[false, 'form']],
                domain: [["id","in", self.ticket_data.tickets_lst]],
                target: 'current'
            },{on_reverse_breadcrumb: function(){ return self.reload();}})
        },

        action_create_ticket: function(event) {
            var self = this;
            event.stopPropagation();
            event.preventDefault();
            this.do_action({
                name: _t("Tickets"),
                type: 'ir.actions.act_window',
                res_model: 'client.ticket',
                view_mode: 'form',
                view_type: 'form',
                views: [[false, 'form']],
                domain: [],
                target: 'current'
            },{on_reverse_breadcrumb: function(){ return self.reload();}})
        },

        action_open_chat: function(e) {
            var self = this;
            return self._rpc({
                model: "helpdesk.dashboard",
                method: "open_chat",
                args: [[]],
              }).then(function(data) {
                self.resPrev = data;
              });
          },
        

        // action_ticket: function(e) {
        //     var self = this;
        //     $(e.currentTarget).find('span').attr('id')
        //     var line_id = parseInt($(e.currentTarget).find('span').attr('id'));
        //     console.log("clicked====");
        //     console.log(parseInt($(e.currentTarget).find('span').attr('id')));
        //     e.stopPropagation();
        //     e.preventDefault();
        //     this.do_action({
        //         name: _t("Tickets"),
        //         type: 'ir.actions.act_window',
        //         res_model: 'client.ticket',
        //         view_mode: 'form',
        //         view_type: 'form',
        //         views: [[false, 'form']],
        //         res_id: line_id,
        //         target: 'current'
        //     },{on_reverse_breadcrumb: function(){ return self.reload();}})
        // },

        previewTable: function() {
    
            $('#ticket_details').DataTable( {
                "ordering" : true,
                "columnDefs": [
                    { "targets": [1,2] ,class:"wrapok ticket-det"},
                ],
                order: [0, 'desc'],
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                pageLength: 10,
            } );
        },
    
    });
    core.action_registry.add('helpdesk_dashboard_main', HelpdeskDashboardView);
    return HelpdeskDashboardView
    });
    
