# -*- coding: utf-8 -*-
##############################################################################

{
    'name': "Helpdesk Dashboard",

    'summary': """
        Dashboard For management of ticket and update.
        """,

    'description': """
        Dashboard which includes ticket details, total remaining support hours charts,
        menus and count of ticket details.
    """,

    'author': "Sagar Jayswal",
    'website': "https://www.sagarcs.com",
    'category': "Helpdesk",
    'version': "13.0.1.0.0",
    'depends': [
        'base',
        'helpdesk_client_api',
        'mail'
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/helpdesk_dashboard.xml',
    ],
    'qweb': [
        "static/src/xml/helpdesk_dashboard.xml",
    ],
    'license': "AGPL-3",
    'installable': True,
    'application': True,
}
