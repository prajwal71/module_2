About
============
Hey! Are you looking for Attractive, Customizable, Futuristic, Responsive, Clean theme for your corporate website in odoo? Here it is! In this theme you will get 8 different pre-designed theme style or it will customise as required. This theme builds with designs and various features like Mega menu, cookie notice, news bar, Snippet builder, Website Comming Soon as well as is a Modern & custom odoo theme. It will design with bootstrap so it will be suitable for kinds of the corporate websites. In this theme, all features will be suitable for the multi-website in odoo. It is a fully responsive theme and looks equally stunning on all kinds of screens and devices. Corpomate theme is a clear and feature stuffed theme for the odoo website.


Installation
============
1) Copy module files to addon folder.
2) Restart odoo service (sudo service odoo-server restart).
3) Go to your odoo instance and open apps (make sure to activate debug mode).
4) click on update app list.
5) search module name and hit install button. or Goto Website Config setting and Choose theme.

Extra Addons Download Link
=====================================
https://drive.google.com/file/d/1KxedfOkuGKA0cFq1sijaLZqGT23psCbh/view?usp=sharing

Any Problem with module?
=====================================
Please create your ticket here https://softhealer.com/support

Softhealer Technologies Doubt/Inquiry/Sales/Customization Team
=====================================
Skype: live:softhealertechnologies
What's app: +917984575681
E-Mail: support@softhealer.com
Website: https://softhealer.com

