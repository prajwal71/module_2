13.0.1 (Date : 24 Dec 2019)
----------------------------

Initial Release

13.0.2 (Date : 6 APRIL 2019)
----------------------------

- THEME 6 IMPLEMENTED


13.0.3 (Date : 10 APRIL 2019)
----------------------------

- THEME 7 IMPLEMENTED

13.0.4 (Date : 22 May 2020)
----------------------------

- Mega Menu Implemented.


13.0.5 (Date : 29 May 2020)
----------------------------
- Theme 8,9 Implemented.
- HOME PAGES AND NEW THEME 8,9








developer code ref:
======================

page.is_homepage

sequence (website menu)

is_homepage

<field name="sequence" type="int">10</field>

<record id="menu_home" model="website.menu">
<field name="name">Home</field>
<field name="url">/</field>
<field name="parent_id" ref="website.main_menu"/>
<field name="sequence" type="int">10</field>
</record>

home ==> 1
Mega menu ==> 2
about ==> 3
services ==> 4
team ==> 5
contact ==> 6
faq ==> 7
privacy policy ==> 8
t and c ==> 9


1 July 2020
=============
v13c final and done at


3 July 2020
=============
=>= Thumbnail added and all things done.
==> blog in demo page and mega menu still remains.




20 Aug 2020
=============
==> Dynamic preparation for theme 9


21 Aug 2020
=============
==> Dynamic Things for theme 9 done.
==> ready made page remains.


28 Aug 2020
=============
==> Dynamic Things for theme 10, 11 done.


31 Aug 2020
=============
==> blog dynamic for theme 10,11 done


13.0.5 (Date: 14 Dec 2020) 
=============
==> all 13 free addons added built in.
==> header logo size bug fixed.
==> js preloader overflow css changed.
==> 8 button style added in link dialog.
==> 1 button snippet added.


Date: 23 Dec 2020
=============
==> theme 13 design done.
==> pending: dynamic pending.
==> Header, Footer, Copyright, Subscribe, Topbar section Design

13.0.6 (Date: 24 Dec 2020) 
====================
- Theme 13 (all done)

 





13.0.7 (Date: 16 Jan 2021)
=============
==> [NEW] theme 14 (dynamic done)
==> PENDING: design currection.


13.0.8 (Date: 20 Jan 2021)
=============
==> [NEW] theme 14 done
==> PENDING: testing



13.0.9 (Date: 22 March 2021)
=============
==> Snippet Builder now supports jquery (assets based)



13.0.10 (Date: 24 March 2021)
=============
==> Theme Wise Font Family Added.


13.0.11 (Date: 17th June 2021)
=============
==> Merge module "sh_web_push_notifications"








23-01-2021
theme14 update