
# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.
{
    "name": "Corpomate Theme - Multipurpose Corporate Theme",
    "author": "Softhealer Technologies",
    "website": "https://www.softhealer.com",
    "support": "suppport@softhealer.com",
    'category': 'Theme/Corporate',
    "summary": "Corporate Theme Construction Theme Medical Theme Finance Theme Business Theme SaaS Theme Digital Mobile App Web App Creative Corporate Theme Multipurpose Theme Pet Care salon Industry Laboratory agriculture NGO fitness Law transportation Odoo",


    "description": """Hey! Are you looking for Attractive, Customizable, Futuristic, Responsive, Clean theme for your corporate website in odoo? Here it is! In this theme you will get 12 different pre-designed theme style or it will customise as required. This theme builds with designs and various features like Mega menu, cookie notice, news bar, Website Comming Soon as well as is a Modern & custom odoo theme. It will design with bootstrap so it will be suitable for kinds of the corporate websites. In this theme, all features will be suitable for the multi-website in odoo. It is a fully responsive theme and looks equally stunning on all kinds of screens and devices. Corpomate theme is a clear and feature stuffed theme for the odoo website.""",
    "version": "13.0.13",
    "depends": [

        "portal",
        "theme_common",
        "website_crm",
        "mass_mailing",
        "website_blog",
        "website_hr_recruitment"


    ],
    "application": True,
    "data": [
        "security/security.xml",
        "security/ir.model.access.csv",
        "views/assets.xml",
        "views/theme_corpomate_templates.xml",
        "views/customize_modal.xml",

        # DYNAMIC BLOG SNIPPETS
        "views/blog_slider/blog_slider_s.xml",
        "views/blog_slider/blog_slider_s_item.xml",


        "views/s_button.xml",
        "views/snippets/theme_13_s.xml",
        "views/snippets/theme_14_s.xml",
        "views/snippets/theme_15_s.xml",
        "views/snippets/theme_16_s.xml",
        "views/snippets/theme_17_s.xml",
        "views/snippets.xml",
        "views/sh_corpomate_theme_item.xml",
        "views/testimonial_view.xml",
        "views/our_partners_view.xml",



        # # DEMO PAGES
        # # THEME 1
        # "views/pages/mega_menu/page_mega_menu_1.xml",
        # "views/pages/about_us/page_about_us_1.xml",
        # "views/pages/contact_us/page_contact_us_1.xml",
        # "views/pages/faq/page_faq_1.xml",
        # "views/pages/home/page_home_1.xml",
        # "views/pages/our_team/page_our_team_1.xml",
        # "views/pages/privacy_policy/privacy_policy_1.xml",
        # "views/pages/service/page_service_1.xml",
        # "views/pages/term_and_condition/page_terms_and_conditions_1.xml",


        # # THEME 2
        # "views/pages/mega_menu/page_mega_menu_2.xml",
        # "views/pages/about_us/page_about_us_2.xml",
        # "views/pages/contact_us/page_contact_us_2.xml",
        # "views/pages/faq/page_faq_2.xml",
        # "views/pages/home/page_home_2.xml",
        # "views/pages/our_team/page_our_team_2.xml",
        # "views/pages/privacy_policy/privacy_policy_2.xml",
        # "views/pages/service/page_service_2.xml",
        # "views/pages/term_and_condition/page_terms_and_conditions_2.xml",

        # # THEME 3
        # "views/pages/mega_menu/page_mega_menu_3.xml",
        # "views/pages/about_us/page_about_us_3.xml",
        # "views/pages/contact_us/page_contact_us_3.xml",
        # "views/pages/faq/page_faq_3.xml",
        # "views/pages/home/page_home_3.xml",
        # "views/pages/our_team/page_our_team_3.xml",
        # "views/pages/privacy_policy/privacy_policy_3.xml",
        # "views/pages/service/page_service_3.xml",
        # "views/pages/term_and_condition/page_terms_and_conditions_3.xml",

        # # THEME 4
        # "views/pages/mega_menu/page_mega_menu_4.xml",
        # "views/pages/about_us/page_about_us_4.xml",
        # "views/pages/contact_us/page_contact_us_4.xml",
        # "views/pages/faq/page_faq_4.xml",
        # "views/pages/home/page_home_4.xml",
        # "views/pages/our_team/page_our_team_4.xml",
        # "views/pages/privacy_policy/privacy_policy_4.xml",
        # "views/pages/service/page_service_4.xml",
        # "views/pages/term_and_condition/page_terms_and_conditions_4.xml",

        # # THEME 5
        # "views/pages/mega_menu/page_mega_menu_5.xml",
        # "views/pages/about_us/page_about_us_5.xml",
        # "views/pages/contact_us/page_contact_us_5.xml",
        # "views/pages/faq/page_faq_5.xml",
        # "views/pages/home/page_home_5.xml",
        # "views/pages/our_team/page_our_team_5.xml",
        # "views/pages/privacy_policy/privacy_policy_5.xml",
        # "views/pages/service/page_service_5.xml",
        # "views/pages/term_and_condition/page_terms_and_conditions_5.xml",

        # # THEME 6
        # "views/pages/mega_menu/page_mega_menu_6.xml",
        # "views/pages/about_us/page_about_us_6.xml",
        # "views/pages/contact_us/page_contact_us_6.xml",
        # "views/pages/faq/page_faq_6.xml",
        # "views/pages/home/page_home_6.xml",
        # "views/pages/our_team/page_our_team_6.xml",
        # "views/pages/privacy_policy/privacy_policy_6.xml",
        # "views/pages/service/page_service_6.xml",
        # "views/pages/term_and_condition/page_terms_and_conditions_6.xml",

        # # THEME 7
        # "views/pages/mega_menu/page_mega_menu_7.xml",
        # "views/pages/about_us/page_about_us_7.xml",
        # "views/pages/contact_us/page_contact_us_7.xml",
        # "views/pages/faq/page_faq_7.xml",
        # "views/pages/home/page_home_7.xml",
        # "views/pages/our_team/page_our_team_7.xml",
        # "views/pages/privacy_policy/privacy_policy_7.xml",
        # "views/pages/service/page_service_7.xml",
        # "views/pages/term_and_condition/page_terms_and_conditions_7.xml",


        # # THEME 8
        # "views/pages/mega_menu/page_mega_menu_8.xml",
        # "views/pages/about_us/page_about_us_8.xml",
        # "views/pages/home/page_home_8.xml",
        # "views/pages/service/page_service_8.xml",
        # "views/pages/contact_us/page_contact_us_8.xml",
        # "views/pages/our_team/page_our_team_8.xml",
        # "views/pages/faq/page_faq_8.xml",
        # "views/pages/term_and_condition/page_terms_and_conditions_8.xml",
        # "views/pages/privacy_policy/privacy_policy_8.xml",


        # # THEME 9
        # "views/pages/mega_menu/page_mega_menu_9.xml",
        # "views/pages/about_us/page_about_us_9.xml",
        # "views/pages/home/page_home_9.xml",
        # "views/pages/service/page_service_9.xml",
        # "views/pages/contact_us/page_contact_us_9.xml",
        # "views/pages/our_team/page_our_team_9.xml",
        # "views/pages/faq/page_faq_9.xml",
        # "views/pages/term_and_condition/page_terms_and_conditions_9.xml",
        # "views/pages/privacy_policy/privacy_policy_9.xml",


        # # THEME 10
        # "views/pages/mega_menu/page_mega_menu_10.xml",
        # "views/pages/about_us/page_about_us_10.xml",
        # "views/pages/home/page_home_10.xml",
        # "views/pages/service/page_service_10.xml",
        # "views/pages/contact_us/page_contact_us_10.xml",
        # "views/pages/our_team/page_our_team_10.xml",
        # "views/pages/faq/page_faq_10.xml",
        # "views/pages/term_and_condition/page_terms_and_conditions_10.xml",
        # "views/pages/privacy_policy/privacy_policy_10.xml",


        # # THEME 11
        # "views/pages/mega_menu/page_mega_menu_11.xml",
        # "views/pages/about_us/page_about_us_11.xml",
        # "views/pages/home/page_home_11.xml",
        # "views/pages/service/page_service_11.xml",
        # "views/pages/contact_us/page_contact_us_11.xml",
        # #                 "views/pages/our_team/page_our_team_11.xml",
        # "views/pages/faq/page_faq_11.xml",
        # "views/pages/term_and_condition/page_terms_and_conditions_11.xml",
        # "views/pages/privacy_policy/privacy_policy_11.xml",


        # # THEME 12
        # "views/pages/mega_menu/page_mega_menu_12.xml",
        # "views/pages/about_us/page_about_us_12.xml",
        # "views/pages/home/page_home_12.xml",
        # "views/pages/service/page_service_12.xml",
        # "views/pages/contact_us/page_contact_us_12.xml",
        # "views/pages/our_team/page_our_team_12.xml",
        # "views/pages/faq/page_faq_12.xml",
        # "views/pages/term_and_condition/page_terms_and_conditions_12.xml",
        # "views/pages/privacy_policy/privacy_policy_12.xml",
        # "views/pages/project/project_12.xml",


        # # THEME 13
        # "views/pages/about_us/page_about_us_13.xml",
        # "views/pages/home/page_home_13.xml",
        # "views/pages/service/page_service_13.xml",
        # "views/pages/contact_us/page_contact_us_13.xml",
        # "views/pages/our_team/page_our_team_13.xml",

        # # THEME 14
        # "views/pages/about_us/page_about_us_14.xml",
        # "views/pages/home/page_home_14.xml",
        # "views/pages/service/page_service_14.xml",
        # "views/pages/contact_us/page_contact_us_14.xml",
        # "views/pages/our_team/page_our_team_14.xml",

        # # THEME 15
        # "views/pages/about_us/page_about_us_15.xml",
        # "views/pages/home/page_home_15.xml",
        # "views/pages/service/page_service_15.xml",
        # "views/pages/contact_us/page_contact_us_15.xml",

        # # THEME 16
        # "views/pages/about_us/page_about_us_16.xml",
        # "views/pages/home/page_home_16.xml",
        # "views/pages/service/page_service_16.xml",
        # "views/pages/contact_us/page_contact_us_16.xml",

        #  #THEME 17
        # "views/pages/about_us/page_about_us_17.xml",
        # "views/pages/home/page_home_17.xml", 
        # "views/pages/service/page_service_17.xml",       
        # "views/pages/contact_us/page_contact_us_17.xml",
        # "views/pages/project/project_17.xml",       
        # "views/pages/gallery/page_gallery_17.xml",

        #Bottom Navigation Bar
        "data/demo_data/bottom_nav_bar.xml",
        "views/bottom_nav_bar/res_language.xml",
        "views/bottom_nav_bar/bottom_nav_bar.xml",
        "views/res_config_settings.xml",
        "views/bottom_nav_bar/sh_language_selector_tmpl.xml",
        "views/bottom_nav_bar/bottom_nav_bar_tmpl_style.xml",
        "views/bottom_nav_bar/bottom_nav_bar_tmpl.xml",

        # MENU
        # "data/website_menu_1.xml",
        # "data/website_menu_2.xml",
        # "data/website_menu_3.xml",
        # "data/website_menu_4.xml",
        # "data/website_menu_5.xml",
        # "data/website_menu_6.xml",
        # "data/website_menu_7.xml",
        # "data/website_menu_8.xml",
        # "data/website_menu_9.xml",
        # "data/website_menu_10.xml",
        # "data/website_menu_11.xml",
        # "data/website_menu_12.xml",
        # "data/website_menu_13.xml",
        # "data/website_menu_14.xml",
        # "data/website_menu_15.xml",
        # "data/website_menu_16.xml",
        # "data/website_menu_17.xml",

        # Mega Menu
        "views/mega_menu/website_menu.xml",
        "views/mega_menu/menu_templates.xml",
        "views/mega_menu/mega_menu_snippets.xml",
        "views/blog_slider/slider.xml",
        "data/demo_data/corpomate_demo.xml",
        "data/demo_data/blog_demo.xml",

        # Cookie Notice
        'extra_addons/sh_cookie_notice/views/website_view.xml',
        'extra_addons/sh_cookie_notice/views/res_config_settings_view.xml',
        'extra_addons/sh_cookie_notice/views/cookie_notice_template.xml',

        # Notice Board
        "extra_addons/sh_notice_board/views/notice_board_view.xml",
        "extra_addons/sh_notice_board/views/assets_frontend.xml",
        "extra_addons/sh_notice_board/views/snippets.xml",

        # HTML Snippet
        "extra_addons/sh_html_snippet/views/html_snippet.xml",
        "extra_addons/sh_html_snippet/views/assets.xml",

        # Pwa Frontend
        "extra_addons/sh_pwa_frontend/data/pwa_configuraion_data.xml",
        "extra_addons/sh_pwa_frontend/views/views.xml",
        "extra_addons/sh_pwa_frontend/views/pwa_configuration_view.xml",

        # Snippet Advance
        "extra_addons/sh_snippet_adv/views/web_editor.xml",
        "extra_addons/sh_snippet_adv/views/assets_wysiwyg.xml",

        # Website Popup
        "extra_addons/sh_website_popup/views/popup.xml",
        "extra_addons/sh_website_popup/views/res_config_settings_view.xml",
        "extra_addons/sh_website_popup/views/website_view.xml",

        # Website Portfolio

        "extra_addons/sh_website_portfolio/views/website_backend_portfolio.xml",
        "extra_addons/sh_website_portfolio/views/sh_website_portfolio_template.xml",
        "extra_addons/sh_website_portfolio/data/sh_website_portfolio_data.xml",

        # Website Preloader
        "extra_addons/sh_website_preloader/views/res_config_settings_view.xml",

        # Website Quote
        "extra_addons/sh_website_quote/views/res_config_settings_view.xml",
        "extra_addons/sh_website_quote/views/website_view.xml",
        "extra_addons/sh_website_quote/views/website_quote_template.xml",

        # Website Whatsapp
        "extra_addons/sh_website_wtsapp/data/whatsapp_data.xml",
        "extra_addons/sh_website_wtsapp/views/whatsapp.xml",
        "extra_addons/sh_website_wtsapp/views/res_config_settings_view.xml",
        "extra_addons/sh_website_wtsapp/views/website_view.xml",

        #                 #website_maintainance
        "extra_addons/website_maintainance/views/website_templates.xml",
        "extra_addons/website_maintainance/views/res_config.xml",
        "extra_addons/website_maintainance/views/website_view.xml",

        #                 #Website Coming Soon

        "extra_addons/sh_website_coming_soon/views/website_view.xml",
        "extra_addons/sh_website_coming_soon/views/assets_frontend.xml",
        "extra_addons/sh_website_coming_soon/views/res_config_settings_view.xml",
        "extra_addons/sh_website_coming_soon/views/template.xml",

        # snippet builder
        "extra_addons/sh_snippet_builder/views/snippet_builder_view.xml",
        "extra_addons/sh_snippet_builder/views/snippet_template.xml",
        "extra_addons/sh_snippet_builder/data/snippet_data.xml",
        "extra_addons/sh_snippet_builder/views/assets.xml",

        # sh_web_push_notifications
        "extra_addons/sh_web_push_notifications/views/send_notifications.xml",
        "extra_addons/sh_web_push_notifications/views/views.xml",
        "extra_addons/sh_web_push_notifications/views/web_push_notification.xml",

    ],
    "images": [
        "static/description/splash-screen.gif",
        "static/description/splash-screen_screenshot.gif",

    ],
    "qweb": ["static/src/xml/website.editor.xml"],
    "auto_install": False,
    "installable": True,
    "price": 120,
    "currency": "EUR",
}
