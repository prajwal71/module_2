About
============
Don’t settle for a simple "Coming Soon" template when you could use one of these to get customers talking!here is a creative, modern, and vibrant “coming soon” template, and you can also customize the template like text, background image, etc,

User Guide
============
Blog: https://softhealer.com/blog/odoo-2/post/coming-soon-235

Installation
============
1) Copy module files to addon folder.
2) Restart odoo service (sudo service odoo-server restart).
3) Go to your odoo instance and open apps (make sure to activate debug mode).
4) click on update app list.
5) search module name and hit install button.

Any Problem with module?
=====================================
Please create your ticket here https://softhealer.com/support

Softhealer Technologies Doubt/Inquiry/Sales/Customization Team
=====================================
Skype: live:softhealertechnologies
What's app: +917984575681
E-Mail: support@softhealer.com
Website: https://softhealer.com
