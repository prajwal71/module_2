odoo.define("sh_corpomate_theme.sh_corpomate_bottom_navbar", function (require) {
    "use strict";

    var publicWidget = require("web.public.widget");
    var animations = require('website.content.snippets.animation');


    publicWidget.registry.sh_corpomate_bottom_navbar = animations.Animation.extend({
        selector: "#wrapwrap",
        disabledInEditableMode: true,

        effects: [{
            startEvents: 'scroll',
            update: '_add_remove_top_to_bottom_navbar',
        }],



        /**
         * @constructor
         */
        init: function () {
            this._super(...arguments);
            var self = this;
            this.amount_scrolled = 300;


        },



        //--------------------------------------------------------------------------
        /**
         * Called when the window is scrolled
         *
         * @private
         * @param {integer} scroll
         */
        _add_remove_top_to_bottom_navbar: function (scroll) {
            var self = this;
            var navbar = $(document).find('.js_cls_bottom_nav_bar_wrapper');
            
            if (navbar.length > 0 && scroll + 812 + 100 <= $("body")[0].scrollHeight) {
                navbar.fadeIn('slow');
            } else if (navbar.length > 0) {
                navbar.fadeOut('slow');
            }

        
        },


    });
    $(document).find('.js_cls_sh_corpomate_theme_flag_btn').click(function () {
        $(document).find(".sh_bottom_nav  #sh_corpomate_lang_dropdown_area").slideToggle('slow')
        
    });
});


