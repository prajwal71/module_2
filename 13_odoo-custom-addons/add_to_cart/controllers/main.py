
# -*- coding: utf-8 -*-
import werkzeug

from odoo import SUPERUSER_ID
from odoo import http

from odoo.http import request
from odoo.tools.translate import _
from werkzeug.exceptions import Forbidden, NotFound
# from odoo.addons.website.models.website import slug
from odoo.addons.http_routing.models.ir_http import slug, unslug
from odoo import api, fields, models, tools, http, release, registry
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo import http
import json
from odoo.addons.website.controllers.main import QueryURL
from odoo.addons.website.models.ir_http import sitemap_qs2dom
import logging 
_logger = logging.getLogger(__name__)
PPG = 20 # Products Per Page
PPR = 4  # Products Per Row


class TableCompute(object):

    def __init__(self):
        self.table = {}

    def _check_place(self, posx, posy, sizex, sizey, ppr):
        res = True
        for y in range(sizey):
            for x in range(sizex):
                if posx + x >= ppr:
                    res = False
                    break
                row = self.table.setdefault(posy + y, {})
                if row.setdefault(posx + x) is not None:
                    res = False
                    break
            for x in range(ppr):
                self.table[posy + y].setdefault(x, None)
        return res

    def process(self, products, ppg=20, ppr=4):
        # Compute products positions on the grid
        minpos = 0
        index = 0
        maxy = 0
        x = 0
        for p in products:
            x = min(max(p.website_size_x, 1), ppr)
            y = min(max(p.website_size_y, 1), ppr)
            if index >= ppg:
                x = y = 1

            pos = minpos
            while not self._check_place(pos % ppr, pos // ppr, x, y, ppr):
                pos += 1
            # if 21st products (index 20) and the last line is full (ppr products in it), break
            # (pos + 1.0) / ppr is the line where the product would be inserted
            # maxy is the number of existing lines
            # + 1.0 is because pos begins at 0, thus pos 20 is actually the 21st block
            # and to force python to not round the division operation
            if index >= ppg and ((pos + 1.0) // ppr) > maxy:
                break

            if x == 1 and y == 1:   # simple heuristic for CPU optimization
                minpos = pos // ppr

            for y2 in range(y):
                for x2 in range(x):
                    self.table[(pos // ppr) + y2][(pos % ppr) + x2] = False
            self.table[pos // ppr][pos % ppr] = {
                'product': p, 'x': x, 'y': y,
                'class': " ".join(x.html_class for x in p.website_style_ids if x.html_class)
            }
            if index <= ppg:
                maxy = max(maxy, y + (pos // ppr))
            index += 1

        # Format table according to HTML needs
        rows = sorted(self.table.items())
        rows = [r[1] for r in rows]
        for col in range(len(rows)):
            cols = sorted(rows[col].items())
            x += len(cols)
            rows[col] = [r[1] for r in cols if r[1]]

        return rows



class WebsiteSaleEXt(WebsiteSale):

    def sitemap_shop1(env, rule, qs):
        if not qs or qs.lower() in '/shop':
            yield {'loc': '/build-pc'}

        Category = env['product.public.category']
        dom = sitemap_qs2dom(qs, '/build-pc/category', Category._rec_name)
        dom += env['website'].get_current_website().website_domain()
        for cat in Category.search(dom):
            loc = '/build-pc/category/%s' % slug(cat)
            if not qs or qs.lower() in loc:
                yield {'loc': loc}


    @http.route([
        '''/build-pc''',
        '''/build-pc/page/<int:page>''',
        '''/build-pc/category/<model("product.public.category"):category>''',
        '''/build-pc/category/<model("product.public.category"):category>/page/<int:page>'''
    ], type='http', auth="public", website=True, sitemap=sitemap_shop1)
    def pc(self, page=0, category=None, search='', ppg=False, **post):
        add_qty = int(post.get('add_qty', 1))
        Category = request.env['product.public.category']
        if category:
            category = Category.search([('id', '=', int(category))], limit=1)
            if not category or not category.can_access_from_current_website():
                raise NotFound()
        else:
            category = Category

        if ppg:
            try:
                ppg = int(ppg)
                post['ppg'] = ppg
            except ValueError:
                ppg = False
        if not ppg:
            ppg = request.env['website'].get_current_website().shop_ppg or 20

        ppr = request.env['website'].get_current_website().shop_ppr or 4

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [[int(x) for x in v.split("-")] for v in attrib_list if v]
        attributes_ids = {v[0] for v in attrib_values}
        attrib_set = {v[1] for v in attrib_values}

        domain = self._get_search_domain(search, category, attrib_values)

        keep = QueryURL('/build-pc', category=category and int(category), search=search, attrib=attrib_list, order=post.get('order'))

        pricelist_context, pricelist = self._get_pricelist_context()

        request.context = dict(request.context, pricelist=pricelist.id, partner=request.env.user.partner_id)
        # request.context.update({
        #         'pc':True,
        #         })


        # context = request.env.context.copy()
        # context.update({'pc': True})
        # request.env.context = context


        _logger.info("=======post===     %s",post)

        url = "/build-pc"
        if search:
            post["search"] = search
        if attrib_list:
            post['attrib'] = attrib_list

        Product = request.env['product.template'].with_context(bin_size=True)

        search_product = Product.search(domain, order=self._get_search_order(post))
        website_domain = request.website.website_domain()
        categs_domain = [('parent_id', '=', False),('is_pc','=',True)] + website_domain
        if search:
            search_categories = Category.search([('product_tmpl_ids', 'in', search_product.ids)] + website_domain).parents_and_self
            categs_domain.append(('id', 'in', search_categories.ids))
        else:
            search_categories = Category
        categs = Category.search(categs_domain)

        if category:
            url = "/build-pc/category/%s" % slug(category)

        product_count = len(search_product)
        pager = request.website.pager(url=url, total=product_count, page=page, step=ppg, scope=7, url_args=post)
        offset = pager['offset']
        products = search_product[offset: offset + ppg]

        ProductAttribute = request.env['product.attribute']
        if products:
            # get all products without limit
            attributes = ProductAttribute.search([('product_tmpl_ids', 'in', search_product.ids)])
        else:
            attributes = ProductAttribute.browse(attributes_ids)

        layout_mode = request.session.get('website_sale_shop_layout_mode')
        if not layout_mode:
            if request.website.viewref('website_sale.products_list_view').active:
                layout_mode = 'list'
            else:
                layout_mode = 'grid'
        layout_mode = 'list'

        values = {
            'search': search,
            'category': category,
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'pager': pager,
            'pricelist': pricelist,
            'add_qty': add_qty,
            'products': products,
            'search_count': product_count,  # common for all searchbox
            'bins': TableCompute().process(products, ppg, ppr),
            'ppg': ppg,
            'ppr': ppr,
            'categories': categs,
            'attributes': False,
            'keep': keep,
            'search_categories_ids': search_categories.ids,
            'layout_mode': layout_mode,
            'pc':True
        }
        if category:
            values['main_object'] = category
        return request.render("website_sale.products", values)


    @http.route(['/shop/cart/update_json'], type='json', auth="public", methods=['POST'], website=True, csrf=False)
    def cart_update_json(self, product_id, line_id=None, add_qty=None, set_qty=None,url=None, display=True):
        order = request.website.sale_get_order(force_create=1)
        _logger.info("==========normal add to cart   ")
        if order.state != 'draft':
            request.website.sale_reset()
            return {}
        if url:
            value = order._cart_update(product_id=product_id, line_id=line_id, add_qty=add_qty, set_qty=set_qty,url=url)
        else :
            value = order._cart_update(product_id=product_id, line_id=line_id, add_qty=add_qty, set_qty=set_qty)

        if not order.cart_quantity:
            request.website.sale_reset()
            return value

        order = request.website.sale_get_order()
        value['cart_quantity'] = order.cart_quantity

        if not display:
            return value

        value['website_sale.cart_lines'] = request.env['ir.ui.view'].render_template("website_sale.cart_lines", {
            'website_sale_order': order,
            'date': fields.Date.today(),
            'suggested_products': order._cart_accessories()
        })
        value['website_sale.short_cart_summary'] = request.env['ir.ui.view'].render_template("website_sale.short_cart_summary", {
            'website_sale_order': order,
        })
        value['website_sale.total'] = request.env['ir.ui.view'].render_template("website_sale.total", {
            'website_sale_order': order,
        })
        _logger.info("========my own====values====")
        return value

    
class MyController(http.Controller):
    
    # FOR THE DEFAULT (NON-INHERITED) "ADD TO CART" METHODS
    # ADDING TRY-EXCEPT FOR EMPTY VALUE "ADD TO CART" ACTION BY USER
    @http.route(['/shop/cart/update'], type='http', auth="public", methods=['POST'], website=True)
    def cart_update(self, product_id, add_qty=1, set_qty=0,url=None, **kw):
        cr, uid, context = request.cr, request.uid, request.context
        try:
            add_qty = float(add_qty)
        except:
            if request.httprequest.headers and request.httprequest.headers.get('Referer'):
                return request.redirect(str(request.httprequest.headers.get('Referer')))
            return request.redirect('/shop')
        request.website.sale_get_order(force_create=1)._cart_update(product_id=int(product_id), add_qty=add_qty, set_qty=float(set_qty))
        a = request.httprequest.host_url
        b = http.request.httprequest
        c = request.httprequest.url
        _logger.info("====%s         %s     ===========kw====%s",url,context,kw)
        if kw.get('pc'):
             return request.redirect("/build-pc")

        if url:
            if '/build-pc' in url:
                return request.redirect("/build-pc")
            else:
                return request.redirect("/shop/cart")
        else:
            return request.redirect("/shop/cart")

    @http.route(['/shop/cart/update_no_redirect'], type='http', auth="public", methods=['GET'], website=True)
    def cart_update_no_redirect(self, product_id, add_qty=1, set_qty=0, **kw):
        # HANDLING INVALID "add_qty" VALUES
        
        try:
            add_qty = float(add_qty)
        except ValueError:
            #this.do_warn(_t("The following fields are invalid:"), warnings.join(''));
            return None
        if add_qty <= 0.0:
            return None
        cr, uid, context = request.cr, request.uid, request.context
        request.website.sale_get_order(force_create=1)._cart_update(product_id=int(product_id), add_qty=add_qty, set_qty=float(set_qty))
        if request.httprequest.headers and request.httprequest.headers.get('Referer'):
            return request.redirect(str(request.httprequest.headers.get('Referer')))
        return request.redirect('/shop')
    

    
    @http.route(['/shop/cart/update_json_shop_to_qty'], type='json', auth="public", methods=['POST'], website=True)
    def update_shop_to_cart_qty_json(self, product_id, add_qty=None, set_qty=None, display=True,url=None):
        if set_qty:
            try:
                set_qty = float(set_qty)
            except ValueError:
                return None
            if set_qty <= 0.0:
                return None
        order = request.website.sale_get_order(force_create=1)
        if order:
            value = {}
            so_line_obj = request.registry['sale.order.line']
            #so_line_obj = request.session.model('sale.order.line')
            cr, uid, context = request.cr, request.uid, request.context
            if 1:#try:
                product_id = int(product_id)
            #except ValueError:
            #    return None
            so_line_ids = so_line_obj.search_read(cr, SUPERUSER_ID, [('order_id', '=', order.id), ('product_id', '=', product_id)], ['id'], context=context)
            if so_line_ids:
                if url:
                    value = order._cart_update(product_id=product_id, line_id=so_line_ids[0]['id'], add_qty=add_qty, set_qty=set_qty,url=url)
                else:
                    value = order._cart_update(product_id=product_id, line_id=so_line_ids[0]['id'], add_qty=add_qty, set_qty=set_qty)
            if not display:
                return None
            value['cart_quantity'] = order.cart_quantity
            value['quantity'] = set_qty
            value['website_sale.total'] = request.website._render("website_sale.total", {
                    'website_sale_order': request.website.sale_get_order()
                })
            return value
        return None
    
    @http.route(['/shop/get_cart_qty_for_selected_variant'], type='http', auth="public", website=True)
    def get_cart_qty_for_selected_variant(self, product_id=False):
        data ={}
        if product_id.isdigit():
            product_uom_qty = 0.0
            data["selected_product_variant_uom_qty"] = False
            data["product_id"] = int(product_id)
            order = request.website.sale_get_order(force_create=1)
            if order:
                so_line_obj = request.registry['sale.order.line']
                cr, uid, context = request.cr, request.uid, request.context
                so_line_qtys = so_line_obj.search_read(cr, SUPERUSER_ID, [('order_id', '=', order.id), ('product_id', '=', data["product_id"])], ['product_uom_qty'], context=context)
                for so_line_qty in so_line_qtys:
                    product_uom_qty += so_line_qty['product_uom_qty']
                if product_uom_qty:
                    data["selected_product_variant_uom_qty"] = product_uom_qty
        return  json.dumps(data)


    # IF IN CASE, THE QUOTATION REFERRED BY SESSION OBJECT IN BELOW METHOD IS DELETED AND WE REFRESH THE PAGE,
    # NILESH WANT THAT THE ERROR SHOULD NOT SHOWN INSTEAD WE SHOUDL BE REDIRECTED TO THE "/SHOP" PAGE
    # @http.route(['/shop/confirmation'], type='http', auth="public", website=True)
    # def payment_confirmation(self, **post):
    #     """ End of checkout process controller. Confirmation is basically seing
    #     the status of a sale.order. State at this point :

    #      - should not have any context / session info: clean them
    #      - take a sale.order id, because we request a sale.order and are not
    #        session dependant anymore
    #     """
    #     cr, uid, context = request.cr, request.uid, request.context
    #     sale_order_id = request.session.get('sale_last_order_id')
    #     if sale_order_id:
    #         order = request.registry['sale.order'].browse(cr, SUPERUSER_ID, sale_order_id, context=context)
    #     else:
    #         return request.redirect('/shop')
    #     try:
    #         # IF STATE IS CANCELLED, REDIRECT TO "/SHOP" 
    #         # IF THE QUOTATION IS DELETED, THIS WILL GENERATE AN ERROR AND WILL INTURN REDIRECT TO "/SHOP"
    #         if order.state == u'cancel':
    #             return request.redirect('/shop')
    #         return request.website.render("website_sale.confirmation", {'order': order})
    #     except:
    #         # ON EXCEPTION, MEANS SOMETHING IS NOT GOOD, WE REDIRECT TO "/SHOP"
    #         return request.redirect('/shop')
        
        
