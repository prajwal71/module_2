# See LICENSE file for full copyright and licensing details.
from datetime import *
import re
from odoo import api, fields, models
from odoo.exceptions import ValidationError
import json
import logging
_logger = logging.getLogger(__name__)

class HrJobLevel(models.Model):
    _name = "hr.job.level"
    _description = "Job Level"

    name = fields.Char('Job Type')

class HrJobSector(models.Model):
    _name = "hr.job.sector"
    _description = "Job Sector"

    name = fields.Char('Sector')


class HrJob(models.Model):
    _inherit = 'hr.job'

    @api.model
    def _default_address_id(self):
        return self.env.user.partner_id

    address_id = fields.Many2one(
        'res.partner', "Job Location", default=_default_address_id,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]",
        help="Address where employees are working", required=True)
    
    job_sector_id = fields.Many2one('hr.job.sector', "Sector", tracking=True,required=True)
    job_views_count = fields.Integer("Website View Count")
    user_id = fields.Many2one('res.users', "Responsible", tracking=True,required=True,default=lambda self: self.env.user)
    job_section_id = fields.Many2one('custom.job.section', string='Job Section',required=True)
    job_type = fields.Selection([('fulltime', 'Full Time'),('parttime', 'PartTime')], default='fulltime', string="Job Type",track_visibility='onchange')
    closing_date = fields.Date('Expiry Date',required=True)
    job_level_id = fields.Many2one('hr.job.level', string='Job Level',required=True)
    offered_salary_type = fields.Selection([('nego', 'Negotiable'),('range', 'Range'),('fixed', 'Fixed')], default='nego', string="Offered Salary Type",track_visibility='onchange')
    min_salary = fields.Float('Min')
    max_salary = fields.Float('Max')
    fix_salary = fields.Float('Fixed')
    

    study_field = fields.Selection(
        [('slc', 'SLC/SEE'),
         ('plus', '+2'),
         ('bachelor', 'Bachelor'),
         ('master', 'Master'),
         ('Phd', 'Doctorate')],
        'Level',required=True)
    # education_level = fields.Selection([('school', 'School'),('inter', 'Intermediate'),('bachelors', 'Bachelors'),('masters', 'Master'),('phd', 'PHD')], default='school', string="Education Level",track_visibility='onchange')
    experience_req = fields.Float('Experience Required (Yrs)',required=True)
    skill_ids = fields.Many2many('hr.my.skills',string='Skills Required',required=True)
    
    
    
    
    # benefits_ids = fields.One2many('hr.job.benefits', 'job_benefits_id',
                                #    string='Benefits')
    # job_requirement_ids = fields.One2many('hr.job.requirement',
                                        #   'job_requirement_id',
                                        #   string='Requirements')
    # job_by_area = fields.Char('Jobs by Functional Area')
    
    # notify_email = fields.Char('Application Notify Email')
    # location_ids = fields.One2many('hr.job.location', 'job_location_id',
                                #    string='Location')
    # des = fields.Text(string="Short Description About Company",compute ='get_details')
    def get_jb_details(self):
        for rec in self:
            res = []
            if rec.user_id:
                if rec.user_id.req_id:
                    mobile = ""
                    phone = ""
                    if rec.user_id.partner_id.mobile:
                        mobile += rec.user_id.partner_id.mobile 
                    if rec.user_id.partner_id.phone:
                        phone += rec.user_id.partner_id.phone
                    res.append({
                    'des': rec.user_id.req_id.description,
                        'lnk' : rec.user_id.req_id.get_logo_url(),
                        'req_mail' : rec.user_id.partner_id.email,
                        'req_website' : rec.user_id.partner_id.website,
                        'req_name' : rec.user_id.partner_id.name,
                        'req_phone' : phone  + " ," + mobile,
                        })
                    _logger.info("------desc---%s",res)
            return res
    def get_jb_url(self):
        for rec in self:
            btx = self.env['ir.config_parameter'].get_param('web.base.url')
            return btx + '/jobs/detail/' + str(rec.id)

    @api.constrains('closing_date')
    def validate_dates(self):
        if not self.env.context.get('website_id'):
            today = date.today()
            if today >= self.closing_date:
                raise ValidationError("Closing date should be greater than"
                                      " Current Date.")

class HrMySkills(models.Model):
    _name = "hr.my.skills"
    _description = "Job Skills"

    name = fields.Char('Skill Name')
    
# class HrJobBenefits(models.Model):
#     _name = "hr.job.benefits"
#     _description = "Job Benefits"

#     name = fields.Char('Benefit')
#     job_benefits_id = fields.Many2one('hr.job', string="Job")


# class HrJobRequirement(models.Model):
#     _name = "hr.job.requirement"
#     _description = "Job Requirement"

#     name = fields.Char('Requirement')
#     job_requirement_id = fields.Many2one('hr.job', string="Job")


# class HrJobLocation(models.Model):
#     _name = "hr.job.location"
#     _description = "Job Location"

#     name = fields.Char('Location')
#     job_location_id = fields.Many2one('hr.job', string="Job")
