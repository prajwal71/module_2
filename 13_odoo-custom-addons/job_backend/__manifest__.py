{
    "name" : "Job Backend",
    "version" : "13.0.0.1",
    "category" : "Website",
    "depends" : ['website','portal','mail','website_hr_recruitment','product'],
    "author": "Sagar",
    'summary': 'This apps helps to do Backend Config Of Job Portal',
    "description": """ Job Backend""",
    "website" : "www.sagarcs.com",
    "price": 50,
    "currency": 'EUR',
    "data": [
        'data/data.xml',
        'views/templates.xml',
        'views/wizard.xml',
        'data/email.xml',
        'views/views.xml',
    ],
    'qweb': [
    ],
    "auto_install": False,
    "installable": True,
  
}

