# -*- coding: utf-8 -*-

from odoo.http import request
from odoo import http, _
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from dateutil.relativedelta import relativedelta
import base64
import logging
import json
_logger = logging.getLogger(__name__)
# import yaml
import werkzeug

class RegisterEmployerJobBackend(http.Controller):

    @http.route(['/register-employer'],type='http', auth="public", website=True)
    def register_employer_job_backend(self, page=1, **kwargs):
        # if request.env.user.partner_id != request.env.ref('base.public_partner'):
        #     name=request.env.user.partner_id.name
        #     email=request.env.user.partner_id.email
        #     mobile=request.env.user.partner_id.mobile
        # _logger.info('-----------------TEST: - %s - This is happening-------------',email)
        country_id = request.env['res.country'].sudo().search([('name','=', 'Nepal')],limit=1)
        # sectors=request.env['hr.job.sector'].sudo().search([])
        if country_id:
            states=request.env['res.country.state'].sudo().search([('country_id','=',country_id.id)])
        else:
            states=request.env['res.country.state'].sudo().search([])
        # return request.render("job_backend.register_employer", {'sectors':sectors,'states':states})
        return request.render("job_backend.register_employer", {'states':states})


    @http.route(['/register-employer-sbt'], type='http', auth="public", website=True)
    def check_employer_submission(self, page=1, **post):
        # partner_id=request.env.user.partner_id.id
        name = post['name']
        phone = post['phone_number']
        description = post['description']
        email = post['email']
        city =  post['city']
        # sector_id = post['sector_id']      
        street = post['address']
        attachment = post['attachment']
        state_id = post['state_id']
        country_id = request.env['res.country'].sudo().search([('name','=', 'Nepal')],limit=1)
       

        employer_obj = request.env['employer.base'].sudo().search([('cname','=',name),('email','=',email)])
        if not employer_obj:  
            files = post.get('attachment').stream.read()
            k_obj =  employer_obj.sudo().create({
            'cname': name,
            'street': street,
            'email': email,
            'phone':phone,
            # 'sector_id': sector_id,
            'state_id': state_id,
            'country_id': country_id.id,
            'city':city,
            'description':description,
            'image_1920' : base64.encodestring(files),
            
            })
            if k_obj:
                if post.get('attachment',False):
                    Attachments = request.env['ir.attachment']
                    name = post.get('attachment').filename      
                    files = post.get('attachment').stream.read()
                    attachment_id = Attachments.sudo().create({
                        'name':name,
                        # 'datas_fname':name,
                        'type': 'binary',   
                        'res_model': 'employer.base',
                        'res_id':k_obj.id,
                        'datas': base64.encodestring(files),
                    }) 
                if post.get('attachment1',False):
                    Attachments = request.env['ir.attachment']
                    name = post.get('attachment1').filename      
                    files = post.get('attachment1').stream.read()
                    attachment_id = Attachments.sudo().create({
                        'name':name,
                        # 'datas_fname':name,
                        'type': 'binary',   
                        'res_model': 'employer.base',
                        'res_id':k_obj.id,
                        'datas': base64.encodestring(files),
                    }) 
                if post.get('attachment2',False):
                    Attachments = request.env['ir.attachment']
                    name = post.get('attachment2').filename      
                    files = post.get('attachment2').stream.read()
                    attachment_id = Attachments.sudo().create({
                        'name':name,
                        # 'datas_fname':name,
                        'type': 'binary',   
                        'res_model': 'employer.base',
                        'res_id':k_obj.id,
                        'datas': base64.encodestring(files),
                    }) 
                if post.get('attachment3',False):
                    Attachments = request.env['ir.attachment']
                    name = post.get('attachment3').filename      
                    files = post.get('attachment3').stream.read()
                    attachment_id = Attachments.sudo().create({
                        'name':name,
                        # 'datas_fname':name,
                        'type': 'binary',   
                        'res_model': 'employer.base',
                        'res_id':k_obj.id,
                        'datas': base64.encodestring(files),
                    }) 
                    k_obj.send_notif_email_job_backend()
                    return request.render("job_backend.success_job_backend")
            else:
                return request.render("job_backend.failed_job_backend")
        else:
                return request.render("job_backend.failed_job_backend")
        
        
   

