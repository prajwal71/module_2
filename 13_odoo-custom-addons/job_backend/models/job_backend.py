# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
import requests
import re
import json
from odoo.tools.image import image_data_uri
from odoo.exceptions import UserError


class ResUsers(models.Model):
    _inherit = 'res.users'
    is_member = fields.Boolean(string="Is Member",default=False)
    req_id = fields.Many2one('employer.base','Employer Source')

class CustomJobSection(models.Model):
    _name = "custom.job.section"
    _description = "Job Section"

    name = fields.Char('Name',required=True)
    jtpye = fields.Selection([('hot', 'Hot Jobs'), ('feature','Featured Jobs'),('top', 'Top Jobs'), ('newspaper', 'Newspaper Jobs')], default='hot', string="Type")

    picture = fields.Binary('1st Ad Image  (720x120)')
    picture1 = fields.Binary('2nd Ad Image (720x120)')
    picture2 = fields.Binary('3rd Ad Image (720x120)')
    picture3 = fields.Binary('4th Ad Image (720x120)')
    picture4 = fields.Binary('5th Ad Image (300x300)')
    picture5 = fields.Binary('6th Ad Image (300x600)')





class EmployerBase(models.Model):
    _name = "employer.base"
    _description = "Employer Base Data"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char('Name',required=True, copy=False, readonly=True, states={'draft': [('readonly', False)]}, index=True, default=lambda self: _('New'))
    cname = fields.Char('Company Name',required=True,states={'draft': [('readonly', False)]})
    user_id = fields.Many2one('res.users',string="User ID",readonly=True)
    partner_id = fields.Many2one('res.partner',related='user_id.partner_id',string='Related Partner')
    state = fields.Selection([('draft', 'Draft'), ('under_review','Under Review'),('approved', 'Approve'),('refuse','Refused')], readonly=True, default='draft', copy=False, string="Status",track_visibility='onchange')
    user_status = fields.Boolean("ID Status",readonly=True,states={'draft': [('readonly', False)]})
    image_1920 = fields.Binary('Logo',attachment=True,required=True)
    street = fields.Char(states={'draft': [('readonly', False)]})
    description = fields.Text(string="Short Description About Company")
    # zip = fields.Char(change_default=True)
    category_id = fields.Many2one('hr.department',string='Job Category',states={'draft': [('readonly', False)]})
    # sector_id = fields.Many2one('hr.job.sector',string='Job Sector',states={'draft': [('readonly', False)]})
    city = fields.Char(states={'draft': [('readonly', False)]})
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict', domain="[('country_id', '=', country_id)]",states={'draft': [('readonly', False)]})
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict',states={'draft': [('readonly', False)]})
    phone = fields.Char(states={'draft': [('readonly', False)]})
    email = fields.Char(states={'draft': [('readonly', False)]})
    website = fields.Char('Website Link')
    product_id = fields.Many2one('product.product',string='Membership Package')
    membership_start_date = fields.Date('Membershio Start Date', default=fields.date.today(),track_visibility='onchange',states={'draft': [('readonly', False)]})
    membership_end_date = fields.Date('Membership End Date',track_visibility='onchange',states={'draft': [('readonly', False)]})
    vat = fields.Char(string='Tax ID', index=True, help="The Tax Identification Number. Complete it if the contact is subjected to government taxes. Used in some legal statements.",states={'draft': [('readonly', False)]})
    reason = fields.Char('Refuse Reason',states={'draft': [('readonly', False)]})


    def send_notif_email_job_backend(self):
        activity_data = {
                'res_id': self.id,
                'res_model_id': self.env['ir.model']._get(self._name).id,
                'activity_type_id': 2,
                'user_id': 2,
                'date_deadline': fields.datetime.now(),
                'summary': "New Employer Signup " + str(self.name),
            }
        self.env['mail.activity'].sudo().create(activity_data)
        template = self.env.ref('job_backend.email_template_job_backend_success_notif')
        if template:
            self.env['mail.template'].browse(template.id).send_mail(self.id)
        emp_obj = self.env['employer.base'].search([('id','=',self.id)])
        if emp_obj:
            dat = {
                'name': self.cname,
                'email': self.email,
                'phone': self.phone,
                'website':  self.website,
                'vat':  self.vat,
                'street':  self.street,
                # 'street2':  self.street2,
                'company_type': 'company',
                'city':  self.city,
                'state_id':  self.state_id.id,
                'country_id':  self.country_id.id,
            }    
            cont_obj = self.env['res.partner'].search([('name','=',self.cname),('email','=',self.email)],limit=1)
            # role = self.env['role.management'].search([('name','=','Membership')],limit=1)
            if not cont_obj:
                cont_obj = self.env['res.partner'].create(dat)
            employer_group = self.env.ref('job_backend.job_portal_employer_group')
            
            if employer_group:
                user_old_obj = self.env['res.users'].search([('login','=',emp_obj.partner_id.email)],limit=1)
                if not user_old_obj:
                    user_obj = self.env['res.users'].create(
                        {
                            'name':emp_obj.cname,
                            'login': emp_obj.email,
                            'partner_id': cont_obj.id,
                            # 'sel_groups_1_8_9': '1',
                            'company_ids': [(4, self.env.company.id)],
                            'company_id': self.env.company.id,
                            # 'role_id': role.id,
                            'is_member':True,
                            'req_id':emp_obj.id,
                        })
                    if user_obj:
                        employer_group.write({'users': [(4, user_obj.id)]})
                        emp_obj.user_id = user_obj.id
                        emp_obj.user_status = True
                        # emp_obj.state = 'approved'
                else:
                    emp_obj.user_id = user_old_obj.id
                    emp_obj.user_status = True
                    # emp_obj.state = 'approved'
                    user_old_obj.partner_id.update(dat)
                    employer_group.write({'users': [(4, user_old_obj.id)]})
                    user_old_obj.action_reset_password()
            else:
                raise UserError(_("Group Doesnot exist"))
                
    def job_backend_cancel(self,reason):
        self.write({'state':'refuse','reason':reason})
        template = self.env.ref('job_backend.email_template_job_backend_failed')
        if template:
            self.env['mail.template'].browse(template.id).send_mail(self.id)
        
    def job_backend_approve(self):
        if self.state in ('draft','under_review'):
            if not self.membership_start_date or not self.membership_end_date or not self.product_id:
                raise UserError(_("Please Start Date/ End Date/ Package"))
        else:
            raise UserError(_("Its not in Draft or Under Review"))
        if self.user_id:
            self.state = 'approved'
            template = self.env.ref('job_backend.email_template_job_backend')
            if template:
                self.env['mail.template'].browse(template.id).send_mail(self.id)
        else:
            raise UserError(_('Please check User details again'))

        
    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('employer.base.seq') or _('New')
        result = super(EmployerBase, self).create(vals)
        return result        

    def unlink(self):
        if self.filtered(lambda x: x.state in ('draft','under_review', 'approved','refuse')):
            raise UserError(_('You can not delete submitted Form.'))
        return super(EmployerBase, self).unlink()
    def get_logo_url(self):
        for rec in self:
            rr =  image_data_uri(rec.image_1920)
            return rr
           
class CancelEmployerRequest(models.TransientModel):
    _name = "cancel.employer.request"
    
    reason = fields.Char('Cancel Reason',required=True)

    def action_cancel_employer(self):
        context = dict(self._context or {})
        active_id = context.get('active_id', False)
        if active_id:
            cur_obj = self.env['employer.base'].browse(active_id)
            cur_obj.job_backend_cancel(self.reason)
           
# class Website(models.Model):
#     _inherit = 'website'

#     # def get_country_list(self):            
#     #     country_ids=self.env['res.country'].search([])
#     #     return country_ids
        
#     def get_state_list(self):            
#         state_ids=self.env['res.country.state'].search([])
#         return state_ids
        
    
