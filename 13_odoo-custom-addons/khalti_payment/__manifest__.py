# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Khalti Payment Acquirer',
    'category': 'Accounting',
    'summary': 'Payment Acquirer: Khalti Implementation',
    'version': '1.0',
    'author': 'Nexus Incorporation',
    'description': """Khalti Payment Acquirer""",
    'depends': ['payment'],
    'application': True,
    'data': [
        'views/payment_views.xml',
        'views/payment_khalti_templates.xml',
        'data/payment_acquirer_data.xml',
    ],
    'post_init_hook': 'create_missing_journal_for_acquirers',
    # 'images': ['static/description/banner.png'],
}
