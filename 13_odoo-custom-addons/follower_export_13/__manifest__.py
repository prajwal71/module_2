# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Followers Export13',
    'version' : '1.1',
    'summary': 'followers export import',
    'description': """

====================

    """,
    'author': 'Nexus',
    'depends' : ['base','web','mail'],
    'data': [
            'views/view.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
