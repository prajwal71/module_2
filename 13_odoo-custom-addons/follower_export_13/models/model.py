# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.

from odoo import fields, models,api,_
import logging
_logger = logging.getLogger(__name__)


class IrAttachmentExt(models.Model):
    _inherit = 'mail.followers'
    
    external_id_custom = fields.Char('Custom External ID')

    
    def find_external(self):
        for rec in self:
            if rec.res_model and rec.res_id:
                rec_res_data = self.env['ir.model.data'].search([('model','=',rec.res_model),('res_id','=',rec.res_id)],limit=1)
                if rec_res_data:
                    rec.external_id_custom = rec_res_data.name

    