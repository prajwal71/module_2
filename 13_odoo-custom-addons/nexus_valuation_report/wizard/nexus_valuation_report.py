# -*- coding: utf-8 -*-
import base64
import logging
from datetime import date

import xlwt

from odoo import models, fields

_logger = logging.getLogger(__name__)


class PartnerDetailedReporting(models.TransientModel):
    _name = "nexus.valuation.report"

    invoice_data = fields.Char('Name', )
    file_name = fields.Binary('Nexus Valuation Report', readonly=True)
    state = fields.Selection([('choose', 'choose'), ('get', 'get')],
                             default='choose')
    date_from = fields.Date('Start Date', required='True', default=date.today())
    date_to = fields.Date('End date', required='True', default=date.today())

    def action_valuation_report(self):
        m_date_from = str(self.date_from)
        m_date_to = str(self.date_to)
        workbook = xlwt.Workbook()
        format0 = xlwt.easyxf(
            'font:height 300, bold True; pattern: pattern solid, fore_colour gray25; align: horiz center')
        format1 = xlwt.easyxf('font:bold True; pattern: pattern solid, fore_colour gray25; align: horiz left')
        format3 = xlwt.easyxf('align: horiz left')
        format4 = format3
        format5 = xlwt.easyxf('font:bold True; align: horiz left;')
        format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour tan; align: horiz left')

        sheet = workbook.add_sheet("Partner_detailed_report", cell_overwrite_ok=True)
        sheet.col(0).width = int(25 * 260)
        sheet.col(1).width = int(15 * 260)
        sheet.col(2).width = int(20 * 260)
        sheet.col(3).width = int(15 * 260)
        sheet.col(4).width = int(20 * 260)
        sheet.col(5).width = int(14 * 260)
        sheet.col(6).width = int(14 * 260)
        sheet.col(7).width = int(12 * 260)
        sheet.col(8).width = int(15 * 260)
        sheet.write_merge(0, 1, 0, 6, 'Stock Valuation Report' + " from " + str(
            self.date_from) + " to " + str(self.date_to), format0)
        sheet.write(2, 0, str("Company"), format1)
        sheet.write(2, 1, str(self.env.company.name), format1)

        sheet.write(4, 0, "Reference", format1)
        sheet.write(4, 1, "Product", format1)
        sheet.write(4, 2, "Opening Qty", format1)
        sheet.write(4, 3, "Opening Balance", format1)
        sheet.write(4, 4, "Closing Qty", format1)
        sheet.write(4, 5, "Closing Balance", format1)
        # sheet.write(4, 3, "Particulars", format1)
        # sheet.write(4, 4, "Debit", format1)
        # sheet.write(4, 5, "Credit", format1)
        # sheet.write(4, 6, "Balance", format1)

        self._cr.execute(
            """SELECT pt.name product_name, pt.default_code ref, SUM(aml.quantity) opening_qty, SUM(aml.balance) opening_balance 
            FROM account_move_line aml 
            LEFT JOIN account_account acc ON aml.account_id = acc.id 
            LEFT JOIN product_product pp 
            LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id ON aml.product_id = pp.id 
            LEFT JOIN product_category pc on pc.id = pt.categ_id WHERE acc.is_stock_account = TRUE 
            AND aml.parent_state = 'posted' AND aml.date<%s GROUP BY pt.name, pt.default_code""",
            (m_date_from,))
        result = self._cr.dictfetchall()
        row = 5
        opening_qty = 0.00
        opening_balance = 0.00
        for data in result:
            quantity = float(data['opening_qty'])
            amount = float(data['opening_balance'])
            opening_qty = opening_qty + quantity
            opening_balance = opening_balance + amount

            sheet.write(row, 0, str(data['ref']), format3)
            sheet.write(row, 1, str(data['product_name']), format3)
            sheet.write(row, 2, str(data['opening_qty']), format3)
            sheet.write(row, 3, str(data['opening_balance']), format3)
            row = row + 1
        # sheet.write(row + 1, 0, "Total", format4)
        # sheet.write(row + 1, 1, opening_qty, format4)
        # sheet.write(row + 1, 2, opening_balance, format4)

        self._cr.execute(
            """SELECT pt.name product_name, pt.default_code ref, SUM(aml.quantity) closing_qty, SUM(aml.balance) closing_balance 
            FROM account_move_line aml 
            LEFT JOIN account_account acc ON aml.account_id = acc.id 
            LEFT JOIN product_product pp 
            LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id ON aml.product_id = pp.id 
            LEFT JOIN product_category pc on pc.id = pt.categ_id WHERE acc.is_stock_account = TRUE
            AND aml.parent_state = 'posted' AND aml.date>=%s GROUP BY pt.name, pt.default_code""",
            (m_date_to,))
        result1 = self._cr.dictfetchall()
        row = 5
        closing_qty = 0.00
        closing_balance = 0.00
        for data1 in result1:
            col_quantity = float(data1['closing_qty'])
            col_amount = float(data1['closing_balance'])
            closing_qty = closing_qty + col_quantity
            closing_balance = closing_balance + col_amount

            sheet.write(row, 0, str(data1['ref']), format3)
            sheet.write(row, 1, str(data1['product_name']), format3)
            sheet.write(row, 4, str(data1['closing_qty']), format3)
            sheet.write(row, 5, str(data1['closing_balance']), format3)
            row = row + 1
        # sheet.write(row + 1, 1, closing_qty, format4)
        # sheet.write(row + 1, 2, closing_balance, format4)

        path = ("/home/odoo/reports/Stock_valuation_report.xls")
        workbook.save(path)
        file = open(path, "rb")
        file_data = file.read()
        out = base64.encodebytes(file_data)
        self.write({'state': 'get', 'file_name': out, 'invoice_data': 'Stock_Valuation_Report.xls'})
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'nexus.valuation.report',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.id,
            'target': 'new',
        }
