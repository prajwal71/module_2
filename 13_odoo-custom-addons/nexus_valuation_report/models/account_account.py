# -*- coding: utf-8 -*-
import logging

from odoo import models, fields

_logger = logging.getLogger(__name__)


class AccountAccountInherit(models.Model):
    _inherit = 'account.account'

    is_stock_account = fields.Boolean("Stock Account")
