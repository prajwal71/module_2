# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Nexus Valuation Report',
    'version': '13.0',
    'category': 'Accounting',
    'summary': 'Valuation Report',
    'author': 'Nexus Incorporation',
    'description': """
The aim is to have a valuation report and also opening and closing balance.
""",
    'depends': ['account'],
    'data': [
        'security/ir.model.access.csv',
        'views/valuation_report.xml',
        'views/account_account_views.xml',
        'wizard/nexus_valuation_report.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'LGPL-3',
}
