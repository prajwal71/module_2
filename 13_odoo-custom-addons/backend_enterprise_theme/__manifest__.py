# -*- coding: utf-8 -*-


{
    'name': 'Backend Enterprise Theme',
    'category': 'Hidden',
    'version': '1.0',
    'description':
        """
Enterprise like view
        """,
        'author':"Nexus",
        'website':"nexuserp.com",
    'depends': ['web'],
    'installable': True,
    'auto_install': False,
    'data': [
        'views/backend_theme_templates.xml',
    ],
    'qweb': [
        "static/src/xml/*.xml",
    ],
   
}
