# -*- coding: utf-8 -*-
{
    'name': "Exam Details",

    'summary': """
        This module print Exam Card """,

    'description': """
        Long description of module's purpose
    """,

    'author': "Nexus Incorporated",
    'website': "http://www.nexusgurus.com",

    
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr_recruitment','job_portal'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',       
        'reports/report.xml',
        'reports/print_exam_details.xml'
    ],
    # only loaded in demonstration mode
    
}
