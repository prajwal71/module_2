
from odoo import models, fields, api


class ApplicantExam(models.Model):
    _inherit = "hr.applicant"

    exam_center=fields.Char(string="Exam Center")
    exam_date_time=fields.Datetime(string="Exam Date and Time")
    exam_roll_no=fields.Char(string="Exam Roll Number")

