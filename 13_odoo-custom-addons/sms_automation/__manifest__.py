# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Sparrow Sms Automation',
    'version' : '13.0.1.0.0',
    'summary': 'Sms bulk send and message default setting',
    'author': 'Nexus Incorporation',
    'depends' : ['contacts','sale','stock','base'],
    'data': [
        'security/ir.model.access.csv',
        'views/sms_automation_view.xml',
        'views/templates.xml',
        'views/view_integration_sms_sale.xml',
        'views/res_config_settings.xml',
        'wizard/sms_send_wizard.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
