import logging
import urllib
import html2text
from odoo import http, models, fields, api, tools, _
from odoo.http import request

_logger = logging.getLogger(__name__)

class ConvertMyHtmlText(object):

    def convert_html_to_text(result_txt):
        capt = b'%s' % (result_txt)
        convert_byte_to_str = capt.decode('utf-8')
        return html2text.html2text(convert_byte_to_str)

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def send_sms_step_one(self):

        record = self.with_context(proforma=True)
        dominio = http.request.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url_preview = self.get_portal_url()
        format_url = '{}{}'.format(dominio, url_preview)

        result_txt = request.env['ir.ui.view'].render_template("sms_automation.template_sms_quote", {
            'doc_ids': self.ids,
            'doc_model': 'sale.order',
            'docs': record,
        })
        result_link = request.env['ir.ui.view'].render_template("sms_automation.template_sms_quote_link", {
            'doc_ids': self.ids,
            'doc_model': 'sale.order',
            'docs': record,
        })

        message_txt = ConvertMyHtmlText.convert_html_to_text(result_txt)
        message_link = ConvertMyHtmlText.convert_html_to_text(result_link)
        message_with_link = str(message_link).format(link =format_url)

        return {'type': 'ir.actions.act_window',
                'name': _('Send SMS'),
                'res_model': 'send.sparrow.sms',
                'target': 'new',
                'view_mode': 'form',
                # 'view_type': 'form',
                'context': {'default_partner_id': self.partner_id.id, 'message_txt': message_txt, 'message_link': message_with_link,'default_type_sms': 'sale_sms','default_source_sms': self.name},
                }

class AccountMoves(models.Model):
    _inherit = 'account.move'

    def send_sms_step_one(self):

        record = self.with_context(proforma=True)
        dominio = http.request.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url_preview = self.get_portal_url()
        format_url = '{}{}'.format(dominio, url_preview)

        result_txt = request.env['ir.ui.view'].render_template("sms_automation.template_sms_invoice", {
            'doc_ids': self.ids,
            'doc_model': 'sale.order',
            'docs': record,
        })
        result_link = request.env['ir.ui.view'].render_template("sms_automation.template_sms_invoice_link", {
            'doc_ids': self.ids,
            'doc_model': 'sale.order',
            'docs': record,
        })

        message_txt = ConvertMyHtmlText.convert_html_to_text(result_txt)
        message_link = ConvertMyHtmlText.convert_html_to_text(result_link)
        message_with_link = str(message_link).format(link =format_url)

        return {'type': 'ir.actions.act_window',
                'name': _('Send SMS'),
                'res_model': 'send.sparrow.sms',
                'target': 'new',
                'view_mode': 'form',
                # 'view_type': 'form',
                'context': {'default_partner_id': self.partner_id.id, 'message_txt': message_txt, 'message_link': message_with_link,'default_type_sms': 'invoice_sms','default_source_sms': self.name},
                }

class StockPickig(models.Model):
    _inherit = 'stock.picking'

    def send_sms_step_one(self):

        record = self.with_context(proforma=True)

        result_txt = request.env['ir.ui.view'].render_template("sms_automation.template_sms_stock_picking", {
            'doc_ids': self.ids,
            'doc_model': 'sale.order',
            'docs': record,
        })

        message_txt = ConvertMyHtmlText.convert_html_to_text(result_txt)

        return {'type': 'ir.actions.act_window',
                'name': _('Send SMS'),
                'res_model': 'send.sparrow.sms',
                'target': 'new',
                'view_mode': 'form',
                # 'view_type': 'form',
                'context': {'default_partner_id': self.partner_id.id, 'message_txt': message_txt, 'format_invisible': True,'default_type_sms': 'delivery_sms','default_source_sms': self.name},
                }