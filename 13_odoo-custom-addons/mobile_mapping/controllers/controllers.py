# -*- coding: utf-8 -*-
# from odoo import http


# class MobileMapping(http.Controller):
#     @http.route('/mobile_mapping/mobile_mapping/', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('/mobile_mapping/mobile_mapping/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('mobile_mapping.listing', {
#             'root': '/mobile_mapping/mobile_mapping',
#             'objects': http.request.env['mobile_mapping.mobile_mapping'].search([]),
#         })

#     @http.route('/mobile_mapping/mobile_mapping/objects/<model("mobile_mapping.mobile_mapping"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('mobile_mapping.object', {
#             'object': obj
#         })
from odoo.addons.portal.controllers.portal import CustomerPortal

class CustomerPortalext(CustomerPortal):
    MANDATORY_BILLING_FIELDS = ["name", "email", "street", "city", "country_id"]
    OPTIONAL_BILLING_FIELDS = ["zipcode","state_id","vat","company_name","phone","mobile","job_position","location","description","start_date","end_date","operation_type","opr_id","tr_no","organization","certification","grade",
    "qualification","study_field"]

    def _prepare_portal_layout_values(self):
        values=super(CustomerPortalext, self)._prepare_portal_layout_values()
        return values