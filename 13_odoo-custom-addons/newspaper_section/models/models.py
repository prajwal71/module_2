# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CustomJobSectionExtended(models.Model):
    _inherit = "custom.job.section"
    
    
    jtpye = fields.Selection([('hot', 'Hot Jobs'), ('feature','Featured Jobs'),('top', 'Top Jobs'), ('newspaper', 'Newspaper Jobs')], default='hot', string="Type")
