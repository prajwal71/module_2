import json
from odoo import http
from odoo.http import request
from datetime import datetime
from odoo.tools.safe_eval import safe_eval
import json
from odoo import http
import base64
from io import BytesIO

class Main(http.Controller):

    @http.route('/firebase-messaging-sw.js', type='http', auth="public")
    def sw_http(self):
        if request.env.company and request.env.company.enable_web_push_notification:
            config_obj = request.env.company.config_details
            
            js = """
            this.addEventListener('install', function(e) {
             e.waitUntil(
               caches.open('video-store').then(function(cache) {
                 return cache.addAll([
                     '/sh_corpomate_theme/static/src/js/index_1.js'
                 ]);
               })
             );
            });
            
            this.addEventListener('fetch', function(e) {
              e.respondWith(
                caches.match(e.request).then(function(response) {
                  return response || fetch(e.request);
                })
              );
            });
    
            importScripts('https://www.gstatic.com/firebasejs/8.4.2/firebase-app.js');
            importScripts('https://www.gstatic.com/firebasejs/8.4.2/firebase-messaging.js');
            var firebaseConfig =
            """+ config_obj +""" ;
            firebase.initializeApp(firebaseConfig);
    
            const messaging = firebase.messaging();
    
            messaging.setBackgroundMessageHandler(function(payload) {
            const notificationTitle = "Background Message Title";
            const notificationOptions = {
                body: payload.notification.body,
            };
            return self.registration.showNotification(
                notificationTitle,
                notificationOptions,
            );
            });
    
            """
            return http.request.make_response(js, [('Content-Type', 'text/javascript')])
        else:
            js = """
            this.addEventListener('install', function(e) {
             e.waitUntil(
               caches.open('video-store').then(function(cache) {
                 return cache.addAll([
                     '/sh_corpomate_theme/static/src/js/index_1.js'
                 ]);
               })
             );
            });
            
            this.addEventListener('fetch', function(e) {
              e.respondWith(
                caches.match(e.request).then(function(response) {
                  return response || fetch(e.request);
                })
              );
            });
    
           
            """
            
            return http.request.make_response(js, [('Content-Type', 'text/javascript')])