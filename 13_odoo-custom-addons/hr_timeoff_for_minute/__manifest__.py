# -*- coding: utf-8 -*-
{
    'name': "HR Time off for minute",

    'summary': """
        time off of a employee even for a minute""",

    'description': """
        time off of a employee even for a minute
    """,

    'author': "Nexus Incoporation",
    'website': "http://nexusgurus.com",
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['hr','hr_holidays'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/views.xml',
        'views/view.xml',
        
    ],
    # only loaded in demonstration mode
    'demo': [
       
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
