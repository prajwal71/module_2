
{
    'name': 'No create Inventory',
    'version': '13.0',
    'category': 'Accounting',
    'summary': 'THis module avoid creation of partner and product from Inventory',
    'author': 'Nexus Incorporation',
    'description': """
                    THis module avoid creation of partner and product from Inventory

""",
    'depends': ['stock'
            ],
    'data': [
        'views/stock.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'LGPL-3',
}
