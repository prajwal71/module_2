# -*- coding: utf-8 -*-

from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)

class ResPartnerExt(models.Model):
    _inherit = 'res.partner'

    @api.model
    def create(self,vals):
        # _logger.info("=======================contact===-%s",self.env.company)
        if self.env.company:
            vals['company_id'] = self.env.company.id
        res=super(ResPartnerExt,self).create(vals)
        return res
