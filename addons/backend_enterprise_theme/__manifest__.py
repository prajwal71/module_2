# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Backend Enterprise Theme',
    'category': 'Hidden',
    'version': '1.0',
    'description': """
Odoo Enterprise Web Client.
===========================

This module modifies the web addon to provide Enterprise design and responsiveness.
        """,
    'depends': ['web'],
    # 'auto_install': True,
    'data': [
        'views/partner_view.xml',
        'views/webclient_templates.xml',
    ],
    'assets': {
        'web._assets_primary_variables': [
            ('after', 'web/static/src/scss/primary_variables.scss', 'backend_enterprise_theme/static/src/**/**/*.variables.scss'),
            ('before', 'web/static/src/scss/primary_variables.scss', 'backend_enterprise_theme/static/src/scss/primary_variables.scss'),
        ],
        'web._assets_secondary_variables': [
            ('before', 'web/static/src/scss/secondary_variables.scss', 'backend_enterprise_theme/static/src/scss/secondary_variables.scss'),
        ],
        'web._assets_backend_helpers': [
            ('before', 'web/static/src/scss/bootstrap_overridden.scss', 'backend_enterprise_theme/static/src/scss/bootstrap_overridden.scss'),
        ],
        'web.assets_common': [
            'backend_enterprise_theme/static/src/webclient/home_menu/home_menu_background.scss',
            'backend_enterprise_theme/static/src/webclient/navbar/navbar.scss',
        ],
        'web.assets_frontend': [
            'backend_enterprise_theme/static/src/webclient/home_menu/home_menu_background.scss',
            'backend_enterprise_theme/static/src/webclient/navbar/navbar.scss',
        ],
        'web.assets_backend': [
            ('replace', 'web/static/src/legacy/scss/fields_extra.scss', 'backend_enterprise_theme/static/src/legacy/scss/fields.scss'),
            ('replace', 'web/static/src/legacy/scss/form_view_extra.scss', 'backend_enterprise_theme/static/src/legacy/scss/form_view.scss'),
            ('replace', 'web/static/src/legacy/scss/list_view_extra.scss', 'backend_enterprise_theme/static/src/legacy/scss/list_view.scss'),

            'backend_enterprise_theme/static/src/legacy/scss/dropdown.scss',
            'backend_enterprise_theme/static/src/legacy/scss/control_panel_mobile.scss',
            'backend_enterprise_theme/static/src/legacy/scss/kanban_view.scss',
            'backend_enterprise_theme/static/src/legacy/scss/touch_device.scss',
            'backend_enterprise_theme/static/src/legacy/scss/form_view_mobile.scss',
            'backend_enterprise_theme/static/src/legacy/scss/modal_mobile.scss',
            'backend_enterprise_theme/static/src/legacy/scss/promote_studio.scss',
            'backend_enterprise_theme/static/src/webclient/**/*.scss',
            ('remove', 'backend_enterprise_theme/static/src/webclient/home_menu/home_menu_background.scss'), # already in _assets_common_styles
            ('remove', 'backend_enterprise_theme/static/src/webclient/navbar/navbar.scss'), # already in _assets_common_styles
            'backend_enterprise_theme/static/src/views/**/*.scss',

            # Allows events to be added to the ListRenderer before it is extended.
            # for more info, see: https://github.com/odoo/enterprise/pull/30169#pullrequestreview-1064657223
            ('prepend', 'backend_enterprise_theme/static/src/legacy/js/views/list/list_renderer_mobile.js'),

            'backend_enterprise_theme/static/src/legacy/js/apps.js',

            'backend_enterprise_theme/static/src/core/**/*',
            'backend_enterprise_theme/static/src/webclient/**/*.js',
            'backend_enterprise_theme/static/src/webclient/**/*.xml',
            'backend_enterprise_theme/static/src/views/**/*.js',
            'backend_enterprise_theme/static/src/views/**/*.xml',

            'backend_enterprise_theme/static/src/legacy/**/*.js',
            'backend_enterprise_theme/static/src/legacy/**/*.xml',

            # Don't include dark mode files in light mode
            ('remove', 'backend_enterprise_theme/static/src/**/**/*.dark.scss'),
        ],
        'web.assets_backend_prod_only': [
            ('replace', 'web/static/src/main.js', 'backend_enterprise_theme/static/src/main.js'),
        ],
        # ========= Dark Mode =========
        "web.dark_mode_variables": [
            # web._assets_primary_variables
            ('before', 'backend_enterprise_theme/static/src/scss/primary_variables.scss', 'backend_enterprise_theme/static/src/scss/primary_variables.dark.scss'),
            ('before', 'backend_enterprise_theme/static/src/**/**/*.variables.scss', 'backend_enterprise_theme/static/src/**/**/*.variables.dark.scss'),
            # web._assets_secondary_variables
            ('before', 'backend_enterprise_theme/static/src/scss/secondary_variables.scss', 'backend_enterprise_theme/static/src/scss/secondary_variables.dark.scss'),
        ],
        "web.dark_mode_assets_common": [
            ('include', 'web.dark_mode_variables'),
        ],
        "web.dark_mode_assets_backend": [
            ('include', 'web.dark_mode_variables'),
            # web._assets_backend_helpers
            ('before', 'backend_enterprise_theme/static/src/scss/bootstrap_overridden.scss', 'backend_enterprise_theme/static/src/scss/bootstrap_overridden.dark.scss'),
            ('after', 'web/static/lib/bootstrap/scss/_functions.scss', 'backend_enterprise_theme/static/src/scss/bs_functions_overridden.dark.scss'),
            # assets_backend
            'backend_enterprise_theme/static/src/**/**/*.dark.scss',
        ],
        'web.tests_assets': [
            'backend_enterprise_theme/static/tests/*.js',
        ],
        'web.qunit_suite_tests': [
            'backend_enterprise_theme/static/tests/views/**/*.js',
            'backend_enterprise_theme/static/tests/webclient/**/*.js',

            'backend_enterprise_theme/static/tests/legacy/views/list_tests.js',
            'backend_enterprise_theme/static/tests/legacy/barcodes_tests.js',
        ],
        'web.qunit_mobile_suite_tests': [
            'backend_enterprise_theme/static/tests/views/disable_patch.js',
            'backend_enterprise_theme/static/tests/mobile/**/*.js',

            'backend_enterprise_theme/static/tests/legacy/action_manager_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/control_panel_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/form_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/relational_fields_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/views/basic/basic_render_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/views/kanban_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/views/list_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/components/action_menus_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/barcodes_mobile_tests.js',
        ],
    },
    # 'license': 'OEEL-1',
}
