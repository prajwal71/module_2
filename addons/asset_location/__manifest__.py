# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2020 CodUP (<http://codup.com>).
#
##############################################################################

{
    'name': 'Assets Location',
    'version': '1.00',
    'summary': 'Asset Location for Location Type',
    'description': """
It gives asset location for location type in odoo inventory location.
===========================
Support following feature:
     """,
    'author': 'Nexus Incorporation',
    'website': 'http://nexusgurus.com',
    'category': 'Industries',
    'sequence': 0,
    'depends': ['stock'],
    # 'demo': ['asset_demo.xml'],
    # 'data': [
    #     'security/asset_security.xml',
    #     'security/ir.model.access.csv',
    #     'asset_view.xml',
    #     'asset_data.xml',
    #     'stock_data.xml',
    #     'views/asset.xml',
    # ],
    'installable': True,
    'application': True,
}
