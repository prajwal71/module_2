.. _changelog:

Changelog
=========

`Version 1.0 (29-11-2019)`
-------------------------
- First release for Odoo 13.0.
