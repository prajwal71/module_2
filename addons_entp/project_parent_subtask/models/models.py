# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import timedelta

from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.tools.safe_eval import safe_eval


class ProjectParentsubtask(models.Model):
    _inherit = 'project.task'
   
    # def updatesubtask(self):

    #     if self.project_id:
    #         child_task = self.env['project.task'].search([('parent_id','=',self.id)])
    #         print("===============================")
    #         print(child_task)

    #         for rec in child_task:
    #             rec.project_id = self.project_id.id
                # rec.write({'project_id':self.project_id.id})


    
    def write(self, vals):
        now = fields.Datetime.now()
        # subtask: force some parent values, if needed
        if 'parent_id' in vals and vals['parent_id']:
            vals.update(self._subtask_values_from_parent(vals['parent_id']))
        # stage change: update date_last_stage_update
        if 'stage_id' in vals:
            vals.update(self.update_date_end(vals['stage_id']))
            vals['date_last_stage_update'] = now
            # reset kanban state when changing stage
            if 'kanban_state' not in vals:
                vals['kanban_state'] = 'normal'
        # user_id change: update date_assign
        if vals.get('user_id') and 'date_assign' not in vals:
            vals['date_assign'] = now

        result = super(ProjectParentsubtask, self).write(vals)
        # rating on stage
        if 'stage_id' in vals and vals.get('stage_id'):
            self.filtered(lambda x: x.project_id.rating_status == 'stage')._send_task_rating_mail(force_send=True)
        # subtask: update subtask according to parent values
        subtask_values_to_write = self._convert_to_write(vals)
        if subtask_values_to_write:
            subtasks = self.filtered(lambda task: not task.parent_id).mapped('child_ids')
            for task in subtasks:
                if task:
                    task.write(subtask_values_to_write)

        if self.project_id:
            child_task = self.env['project.task'].search([('parent_id','=',self.id)])
            for rec in child_task:
                rec.project_id = self.project_id.id

        return result














