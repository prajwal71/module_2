from odoo import models, api, _
from datetime import date, timedelta, datetime
from odoo.http import request
from odoo.exceptions import UserError
import time


# class DayBookPdfReport(models.AbstractModel):

#     _name = 'report.account_day_book.day_book_report_template'

#     def _get_account_move_entry(self, accounts, form_data):
#         cr = self.env.cr
#         MoveLine = self.env['account.move.line']
#         tables, where_clause, where_params = MoveLine._query_get()
#         wheres = [""]
#         if where_clause.strip():
#             wheres.append(where_clause.strip())
#         if form_data['target_move'] == 'posted':
#             target_move = "AND m.state = 'posted'"
#         else:
#             target_move = ''
#         sql = ('''
#                 SELECT l.id AS lid, acc.name as accname, l.account_id AS account_id, l.date AS ldate, j.code AS lcode, l.currency_id, 
#                 l.amount_currency, l.ref AS lref, l.name AS lname, COALESCE(l.debit,0) AS debit, COALESCE(l.credit,0) AS credit, 
#                 COALESCE(SUM(l.debit),0) - COALESCE(SUM(l.credit), 0) AS balance,
#                 m.name AS move_name, c.symbol AS currency_code, p.name AS partner_name
#                 FROM account_move_line l
#                 JOIN account_move m ON (l.move_id=m.id)
#                 LEFT JOIN res_currency c ON (l.currency_id=c.id)
#                 LEFT JOIN res_partner p ON (l.partner_id=p.id)
#                 JOIN account_journal j ON (l.journal_id=j.id)
#                 JOIN account_account acc ON (l.account_id = acc.id) 
#                 WHERE l.account_id IN %s AND l.journal_id IN %s '''+target_move+''' AND l.date BETWEEN %s AND %s
#                 GROUP BY l.id, l.account_id, l.date,
#                      j.code, l.currency_id, l.amount_currency, l.ref, l.name, m.name, c.symbol, p.name , acc.name
#                      ORDER BY l.date DESC
#         ''')
#         params = (tuple(accounts.ids), tuple(form_data['journal_ids']), form_data['date_from'], form_data['date_to'])
#         cr.execute(sql, params)
#         data = cr.dictfetchall()
#         print("=========Niraj=======")
#         print(data)
#         return data

    # @api.model
    # def _get_report_values(self, docids, data=None):
    #     if not data.get('form') or not self.env.context.get('active_model'):
    #         raise UserError(_("Form content is missing, this report cannot be printed."))

    #     self.model = self.env.context.get('active_model')
    #     docs = self.env[self.model].browse(self.env.context.get('active_ids', []))
    #     form_data = data['form']
    #     codes = []
    #     if data['form'].get('journal_ids', False):
    #         codes = [journal.code for journal in self.env['account.journal'].search([('id', 'in', data['form']['journal_ids'])])]
    #     active_acc = data['form']['journal_ids']
    #     accounts = self.env['account.account'].search([('id', 'in', active_acc)]) if data['form']['journal_ids'] else self.env['account.account'].search([])
    #     accounts_res = self.with_context(data['form'].get('used_context',{}))._get_account_move_entry(accounts, form_data)
    #     return {
    #         'doc_ids': docids,
    #         'doc_model': self.model,
    #         'data': data['form'],
    #         'docs': docs,
    #         'time': time,
    #         'Accounts': accounts_res,
    #         'print_journal': codes,
    #     }
        

class PartnerDetailPdfReport(models.AbstractModel):

    _name = 'report.account_day_book.partner_detail_report_template'


    def _get_account_move_entry(self, accounts, form_data,flag):

    
        m_date_from = str(form_data['date_from'])
        m_date_to = str(form_data['date_to'])
        partner_id = form_data['partner_id']
        print(form_data)

   

        self._cr.execute("""select ai.id,ai.date_invoice as ldate,pt.name,ail.quantity,ail.price_unit,ail.price_subtotal,ai.number,ai.amount_untaxed,ai.amount_tax,ai.amount_total,ai.partner_id from account_invoice_line ail 
                                left join account_invoice ai on ail.invoice_id = ai.id  left join product_product p on p.id = ail.product_id left join product_template pt on pt.id = p.product_tmpl_id where ai.state in ('open','paid') and ai.type='out_invoice' and ai.company_id = %s and ai.date_invoice>=%s  
                                 and ai.date_invoice<=%s and ai.partner_id =%s order by ai.id""", (self.env.user.company_id.id,m_date_from,m_date_to,partner_id[0],))
        result =  self._cr.dictfetchall()
        
        cr = self.env.cr


        sql = ('''
                        SELECT l.id AS lid, acc.name as accname,acc.code as acccode, l.account_id AS account_id, l.date AS ldate, j.code AS lcode, j.name AS jname, l.currency_id, 
                        l.amount_currency, l.ref AS lref, l.name AS lname, COALESCE(l.debit,0) AS debit, COALESCE(l.credit,0) AS credit, 
                        COALESCE(SUM(l.debit),0) - COALESCE(SUM(l.credit), 0) AS balance,
                        m.name AS move_name, c.symbol AS currency_code, p.name AS partner_name,l.create_uid as cid
                        FROM account_move_line l
                        JOIN account_move m ON (l.move_id=m.id)
                        LEFT JOIN res_currency c ON (l.currency_id=c.id)
                        LEFT JOIN res_partner p ON (l.partner_id=p.id)
                        JOIN account_journal j ON (l.journal_id=j.id)
                        JOIN account_account acc ON (l.account_id = acc.id) 
                        WHERE  l.journal_id IN %s AND l.partner_id = %s AND m.state = 'posted' AND l.date BETWEEN %s AND %s
                        GROUP BY l.id, l.account_id, l.date,
                             j.code, j.name, l.currency_id, l.amount_currency, l.ref, l.name, m.name, c.symbol, p.name , acc.name, acc.code
                             ORDER BY j.code DESC
                ''')
        # kl = []
        journal_ids = form_data['journal_ids']

       
        params = (tuple(journal_ids),partner_id[0], m_date_from, m_date_to)

        cr.execute(sql, params)
        result_pay = cr.dictfetchall()
        print("================Datas=======================")
      
        datas1 = result + result_pay
        datas1.sort(key=lambda item:item['ldate'])
        print("================datas1========")
        print(datas1)
        tot_dict = {}
        # print(data)
        lst = []
        flag11 = 0
        tmp = " "
        tmp1 = " "
        tmpd = " "
        tot_receivable = 0.00
        total_paid = 0.00
        total_received = 0.00
        
        for data in datas1:
            k = [" "]*10
            j = ["."]*10
            if 'id' in data:
                flag11 = 1
                if data['quantity'] != 0: 
                    if tmp == data['number']:
                                k[0] = " "
                                k[1] = " "
                    else:
                        
                        lst.append(j)
                        if data['amount_total'] != 0:
                            tot_receivable += data['amount_total']
                        k[0] = data['ldate']
                        k[1] = data['number']
                        data['amount_untaxed'] = float(data['amount_untaxed'])
                        data['amount_untaxed'] = '{:,.2f}'.format(data['amount_untaxed'])
                        k[6] = data['amount_untaxed']
                        data['amount_total'] = float(data['amount_total'])
                        data['amount_total'] = '{:,.2f}'.format(data['amount_total'])
                        k[8] = data['amount_total']
                        data['amount_tax'] = float(data['amount_tax'])
                        data['amount_tax'] = '{:,.2f}'.format(data['amount_tax'])
                        k[7] = data['amount_tax']
                        
                    act_unt = ((float(data['price_unit'])*100)/113)
                    k[2] = data['name']
                    data['quantity'] = float(data['quantity'])
                    data['quantity'] = '{:,.2f}'.format(data['quantity'])
                    k[3] = data['quantity']
                    # k[4] = ("%.2f" % act_unt)
                    mn = ("%.2f" % act_unt)
                    mn = float(mn)
                    k[4] = '{:,.2f}'.format(mn)
                    data['price_subtotal'] = float(data['price_subtotal'])
                    data['price_subtotal'] = '{:,.2f}'.format(data['price_subtotal'])
                    k[5] = data['price_subtotal']
                    
                    tmp = data['number']
                    tmp1 = data['amount_total']
                    # j[0] = "."
                    # lst.append(j)
                    
            
        # print(lst)

            elif 'lid' in data:
                        if tmpd == data['move_name']:
                            flag11 = 2
                            # k[0] = "_"
                            # k[1] = " "
                            # j[0] = "."
                            # lst.append(j)
                        else:
                            flag11 = 1
                          
                            lst.append(j)
                            k[0] = data['ldate']
                            k[1] = data['move_name'] + "\n" + "(" + data['jname'] + ")"
                            
                        if data['accname'] == 'Account Receivable' and data['credit'] != 0 and data['debit'] == 0:
                            total_received += data['credit']
                        if data['accname'] == 'Account Payable' and data['credit'] == 0 and data['debit'] != 0:
                            total_paid += data['debit']
                        
                        if data['accname'] == 'Account Receivable' or  data['accname'] == 'Account Payable':
                            data['debit'] = float(data['debit'])
                            data['debit'] = '{:,.2f}'.format(data['debit'])
                            k[8] = data['debit']
                            print("=======NIRAJ=====")
                            print(k[8])
                            data['credit'] = float(data['credit'])
                            data['credit'] = '{:,.2f}'.format(data['credit'])
                            k[9] = data['credit']
                            print("=======NIRAJKARNA=====")
                            print(k[9])
                        tmpd = data['move_name']
                        # j[0] = "."
                        # lst.append(j)
            if flag11 == 1:
                lst.append(k)
        print(lst)
        tot_dict['tot_receivable'] = tot_receivable
        tot_dict['total_received'] = total_received
        tot_dict['total_paid'] = total_paid
        tot_dict['partner'] = partner_id
        tot_dict['date_from'] = m_date_from
        tot_dict['date_to'] = m_date_to
        if flag == 0:
            return lst
        elif flag == 1:
            return tot_dict
    @api.model
    def _get_report_values(self, docids, data=None):
        if not data.get('form') or not self.env.context.get('active_model'):
            raise UserError(_("Form content is missing, this report cannot be printed."))

        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_ids', []))
        form_data = data['form']
        codes = []
        if data['form'].get('journal_ids', False):
            codes = [journal.code for journal in self.env['account.journal'].search([('id', 'in', data['form']['journal_ids'])])]
        active_acc = data['form']['journal_ids']
        accounts = self.env['account.journal'].search([('id', 'in', active_acc)]) if data['form']['journal_ids'] else self.env['account.account'].search([])
        accounts_res = self.with_context(data['form'].get('used_context',{}))._get_account_move_entry(accounts, form_data,flag=0)
        print("==============yo==========")
        print(accounts_res)
        # print(accounts_res)
        
        account_res1 = self.with_context(data['form'].get('used_context',{}))._get_account_move_entry(accounts, form_data,flag=1)
        print("========================Acconity=============")
        print(account_res1)
        return {
            'doc_ids': docids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'time': time,
            'Accounts': accounts_res,
            'Totals' : account_res1,
            'print_journal': codes,
        }

