# -*- coding: utf-8 -*-
import xlwt
import base64
import calendar
from io import StringIO
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning
from datetime import  timedelta,date,time
import datetime
from odoo.http import request
import re
class PartnerDetailedReporting(models.TransientModel):
    _name = "partner.detailed.reporting"
    
    invoice_data = fields.Char('Name',)
    file_name = fields.Binary('Partner Detailed Report', readonly=True)
    state = fields.Selection([('choose', 'choose'), ('get', 'get')],
                             default='choose')
    journal_ids = fields.Many2many('account.journal', string='Journals', required=True,
                                   default=lambda self: self.env['account.journal'].search([('type','in',('bank','cash','general')),('company_id','=',self.env.user.company_id.id)]))
    account_ids = fields.Many2many('account.account', string='Sales Accounts', required=True,
                                    default=lambda self: self.env['account.account'].search([("name","ilike","Product Sales"),('company_id','=',self.env.user.company_id.id)]))
    partner_id = fields.Many2one('res.partner',string='Partner')
   
    date_from = fields.Date('Start Date',required='True',default=date.today())
    date_to = fields.Date('End date',required='True',default=date.today())


    def _build_contexts(self, data):
        result = {}
        result['journal_ids'] = 'journal_ids' in data['form'] and data['form']['journal_ids'] or False
        result['state'] = 'state' in data['form'] and data['form']['state'] or ''
        result['date_from'] = data['form']['date_from'] or False
        result['date_to'] = data['form']['date_to'] or False
        result['partner_id'] = data['form']['partner_id'] or False
        result['strict_range'] = True if result['date_from'] else False
        return result

    # @api.multi
    def check_reports(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['date_from', 'date_to', 'journal_ids', 'partner_id', 'invoice_data'
                                   ])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang') or 'en_US')
        print (data)
   
        return self.env.ref('account_day_book.partner_detailed_pdf_report').report_action(self, data=data)

    # @api.multi
    def action_partner_detailed_report(self):
        # mon_name = datetime.strptime(self.date_from, '%Y-%m-%d %H:%M:%S')
        # mon_name = mon_name.strftime("%B")
        # tm = datetime.time(23, 59, 59)
        # dt1 = datetime.datetime.strptime(self.date_from, "%Y-%m-%d")
        # dt2 = datetime.datetime.strptime(self.date_to, "%Y-%m-%d").date()
        m_date_from = str(self.date_from)
        m_date_to = str(self.date_to)
        workbook = xlwt.Workbook()
        format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour green;align: horiz center')
        format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
        format3 = xlwt.easyxf('align: horiz left')
        format4 = format3
        format5 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour aqua;align: horiz left')
        format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour tan;align: horiz left')

        sheet = workbook.add_sheet("Partner_detailed_report",cell_overwrite_ok=True)
        sheet.col(0).width = int(12*260)
        sheet.col(1).width = int(15*260)    
        sheet.col(2).width = int(22*260)    
        sheet.col(3).width = int(8*260) 
        sheet.col(4).width = int(12*260)   
        sheet.col(5).width = int(12*260)
        sheet.col(6).width = int(13*260)
        sheet.col(7).width = int(12*260)
        sheet.col(8).width = int(15*260)
        sheet.col(9).width = int(12*260)   
        sheet.col(10).width = int(12*260)
        sheet.col(11).width = int(12*260)   
        sheet.write_merge(0, 1, 0, 4, 'Partner Detailed Report',format0)
        sheet.write(2, 0, str("Date" +":"), format1)
        sheet.write(2, 1, str(self.date_from), format1)
        # sheet.write(2, 2, str("Partner"), format1)
        sheet.write(2, 2, str(self.partner_id.name), format1)
        sheet.write(2, 4, str("Company"), format1)
        sheet.write(2, 5, str(self.env.user.company_id.name), format1)

        sheet.write(3, 0, "Date", format1)
        sheet.write(3, 1, "Ref. Number", format1)
        # sheet.write(3, 2, "JRNL", format1)
        sheet.write(3, 2, "Product", format1)
        sheet.write(3, 3, "Qty", format1)
        sheet.write(3, 4, "Unit Price", format1)
        sheet.write(3, 5, "Subtotal", format1)
        sheet.write(3, 6, "Untaxed Amt", format1)
        sheet.write(3, 7, "Total VAT", format1)
        sheet.write(3, 8, "Debit", format1)
        sheet.write(3, 9, "Credit", format1)
        # sheet.write(3, 5, str(m_date_from), format1)
        # sheet.write(3, 6, str(m_date_to), format1)
        # if self.doctor_id:
            # curdoc_id = self.doctor_id.id
        ac = []
        for ids in self.account_ids:
            ac.append(ids.id)
        self._cr.execute("""select am.id,am.invoice_date as ldate,pt.name as product,aml.quantity,aml.price_unit,aml.price_subtotal,am.name as number,am.amount_untaxed,am.amount_tax,am.amount_total,am.partner_id from account_move_line aml 
                                left join account_move am on aml.move_id = am.id  left join product_product p on p.id = aml.product_id left join product_template pt on pt.id = p.product_tmpl_id where am.state ='posted' and 
                                am.type IN ('out_invoice','in_invoice') and am.company_id= %s and aml.account_id IN %s and am.invoice_date BETWEEN %s AND %s  and am.partner_id =%s order by am.id""", (self.env.user.company_id.id,tuple(ac),m_date_from,m_date_to,self.partner_id.id,))
        result =  self._cr.dictfetchall()
        cr = self.env.cr
        # MoveLine = self.env['account.move.line']
        # tables, where_clause, where_params = MoveLine._query_get()
        # wheres = [""]
        # if where_clause.strip():
        #     wheres.append(where_clause.strip())
        # if form_data['target_move'] == 'posted':
        #     target_move = "AND m.state = 'posted'"
        # else:
        #     target_move = ''

        sql = ('''
                        SELECT l.id AS lid, acc.name as accname,acc.code as acccode, l.account_id AS account_id, l.date AS ldate, j.code AS lcode, l.currency_id, 
                        l.amount_currency, l.ref AS lref, l.name AS lname, COALESCE(l.debit,0) AS debit, COALESCE(l.credit,0) AS credit, 
                        COALESCE(SUM(l.debit),0) - COALESCE(SUM(l.credit), 0) AS balance,
                        m.name AS move_name, c.symbol AS currency_code, p.name AS partner_name,l.create_uid as cid
                        FROM account_move_line l
                        JOIN account_move m ON (l.move_id=m.id)
                        LEFT JOIN res_currency c ON (l.currency_id=c.id)
                        LEFT JOIN res_partner p ON (l.partner_id=p.id)
                        JOIN account_journal j ON (l.journal_id=j.id)
                        JOIN account_account acc ON (l.account_id = acc.id) 
                        WHERE j.type in ('cash','bank','general') AND m.type in('entry','out_receipt','in_receipt')
                        AND l.journal_id IN %s AND l.partner_id = %s AND m.state = 'posted' AND l.date BETWEEN %s AND %s
                        GROUP BY l.id, l.account_id, l.date,
                        j.code, l.currency_id, l.amount_currency, l.ref, l.name, m.name, c.symbol, p.name , acc.name, acc.code
                        ORDER BY j.code DESC
                ''')
        kl = []
        for ids in self.journal_ids:
            kl.append(ids.id)
        params = (tuple(kl),self.partner_id.id, m_date_from, m_date_to)
        cr.execute(sql, params)
        result_pay = cr.dictfetchall()
        # return data
        print("================Excel=======================")
        print(result_pay)
        final_result = result + result_pay
        print("================Excel=======================")
        print(result)
        print(final_result)
        final_result.sort(key=lambda item:item['ldate'])   
        
        row_number = 4
        col_number = 0
        tmp =  " "
        tmp1 = " "
        tmpd = " "
        # tot_payable = 0.00
        tot_receivable = 0.00
        # tax_payable = 0.00
        # tax_receivable = 0.00
        total_paid = 0.00
        total_received = 0.00
        # total = 0.00
        for data in final_result:
            if 'id' in data:
                if data['quantity'] != 0: 
                    if tmp == data['number']:
                        sheet.write(row_number, col_number , " ",
                                format4)
                        sheet.write(row_number, col_number + 1, " ",
                                format4)
                        # sheet.write(row_number, col_number + 2, " ",
                        #         format4)
                        # sheet.write(row_number, col_number + 3, " ",
                        #         format4)
                    else:
                        row_number += 1
                        sheet.write(row_number, col_number, str(data['ldate']), format4)
                        sheet.write(row_number, col_number + 1, data['number'],format4)
                        # row_number += 1
                        # sheet.write(row_number, col_number+7, str("Account Receivable"), format4)
                        sheet.write(row_number, col_number+6, str(data['amount_untaxed']), format5)
                        sheet.write(row_number, col_number+8, str(data['amount_total']), format5)
                        sheet.write(row_number, col_number+7, str(data['amount_tax']), format5)
                        if data['amount_total'] != 0:
                            tot_receivable += data['amount_total']
                        # row_number += 1
                        
                        # sheet.write(row_number, col_number+6, str("Account Receivable"), format4)
                        # sheet.write(row_number, col_number+7, str(data['amount_total']), format4)
                        # row_number += 1
                    # if tmp1 == data['amount_total']:
                    #     # sheet.write(row_number, col_number+6, str(data['date_invoice']), format4)
                    #     sheet.write(row_number, col_number+6 , " ",
                    #             format4)
                    #     sheet.write(row_number, col_number + 7, " ",
                    #             format4)
                    # else:
                        
                    # if data['amount_total'] != 0:
                    #     tot_receivable += data['amount_total']
                    act_unt = ((float(data['price_unit'])*100)/113)
                    sheet.write(row_number, col_number + 2, data['product'],format4)
                    sheet.write(row_number, col_number + 3, data['quantity'],format4)
                    sheet.write(row_number, col_number + 4,"%.2f" % act_unt ,format4)
                    sheet.write(row_number, col_number + 5, data['price_subtotal'], format4)
                    # sheet.write(row_number, col_number + 6, data[''], format5)
                    tmp = data['number']
                    tmp1 = data['amount_total']
                    row_number += 1
            
            
            
            elif 'lid' in data:
                if tmpd == data['move_name']:
                    sheet.write(row_number, col_number , " ",
                            format4)
                    sheet.write(row_number, col_number + 1, " ",
                            format4)
                else:
                    row_number += 1
                    sheet.write(row_number, col_number, str(data['ldate']), format4)
                    sheet.write(row_number, col_number + 1, data['move_name'],
                            format4)
                    
                    # sheet.write(row_number, col_number + 2, data['lcode'], format4)
                    # sheet.write(row_number, col_number + 3, data['partner_name'],
                    #             format4)
                   
                # sheet.write(row_number, col_number + 3, lines['lref'],format4)
                
                
                
                if data['accname'] == 'Account Receivable' and data['credit'] != 0 and data['debit'] == 0:
                    total_received += data['credit']
                if data['accname'] == 'Account Payable' and data['credit'] == 0 and data['debit'] != 0:
                    total_paid += data['debit']
                # if lines['accname'] == 'Account Payable' and lines['debit'] == 0 and lines['credit'] != 0:
                #     tot_payable += lines['credit']
                # if lines['accname'] == 'Account Payable' and lines['credit'] == 0 and lines['debit'] != 0:
                #     total_paid += lines['debit']



                # sheet.write(row_number, col_number + 7, data['acccode'] + ' ' + data['accname'],
                #             format4)
                
                # sheet.write(row_number, col_number + 5, data['lname'],
                #             format4)
                if data['accname'] == 'Account Receivable' or  data['accname'] == 'Account Payable':
                    sheet.write(row_number, col_number + 8, data['debit'], format6)
                    sheet.write(row_number, col_number + 9, data['credit'], format6)
                tmpd = data['move_name']
                row_number += 1

       
        sheet.write(row_number+1, col_number + 8, "Total Receivable", format1)
        sheet.write(row_number+2, col_number + 8, "Total Received", format1)
        sheet.write(row_number+3, col_number + 8, "Total Paid", format1)
        sheet.write(row_number+1, col_number + 9, str(tot_receivable), format1)
        sheet.write(row_number+2, col_number + 9,  str(total_received), format1)
        sheet.write(row_number+3, col_number + 9,  str(total_paid), format1)
                # sheet.write(row_number, col_number + 2, lines[''], format4)
                # sheet.write(row_number, col_number + 3, lines['name'],
                            # format4)
                # sheet.write(row_number, col_number + 8, str(k.login),
                            # format4)
            # if data['disc_typ'] == 'percentage':
            #     if data['type'] == 'out_refund':
            #         sub_total = (-1)*((float(data['price_subtotal']) - (float(data['price_subtotal'])*float(data['disc_p']))/100))
            #     if data['type'] == 'out_invoice':
            #         sub_total = (1)*((float(data['price_subtotal']) - (float(data['price_subtotal'])*float(data['disc_p']))/100))
            #     total = sub_total+total
            #     # total = float(data['price_subtotal'])+total
            # else:
            #     if data['type'] == 'out_refund':
            #         sub_total = (-1)*float(data['price_subtotal']) 
            #     if data['type'] == 'out_invoice':
            #         sub_total = (1)*float(data['price_subtotal']) 
            #     total = sub_total+total
            # sheet.write(row, 0, str(data['pat_name']), format3)
            
            # tmp_id = int(data['product_tmpl_id'])
            # tmp_obj = self.env['product.template'].search([('id','=',tmp_id)],limit=1)
            # cate_obj = self.env['product.category'].search([('id','=',tmp_obj.categ_id.id)],limit=1)
            # sheet.write(row, 1, str(data['number']), format3)
            # sheet.write(row, 2, str(data['dat']), format3)
            # sheet.write(row, 3, str(tmp_obj.name), format3)
            # sheet.write(row, 4, str(cate_obj.name), format3)
            # sheet.write(row, 5, str(data['quantity']), format3)
            # sheet.write(row, 6, str(sub_total), format3)
            # slpr_id = self.env['res.users'].search([('id','=',data['usr'])])
            # if slpr_id:
            #     sheet.write(row, 7, str(slpr_id.login), format3)
            # row = row+1
        # sheet.write(row+1, 5,"Total", format3)
        # sheet.write(row+1, 6, total, format3)
        # path = ("/mnt/extra-addons/sagar_ext/reports/Partner_detailed_report.xls")
        path = ("/home/odoo/reports/Partner_detailed_report.xls")
        workbook.save(path)
        file = open(path, "rb")
        file_data = file.read()
        out = base64.encodestring(file_data)
        self.write({'state': 'get', 'file_name': out, 'invoice_data':'Partner_detailed_report.xls'})
        return {
               'type': 'ir.actions.act_window',
               'res_model': 'partner.detailed.reporting',
               'view_mode': 'form',
               'view_type': 'form',
               'res_id': self.id,
               'target': 'new',
            }                

    
    



                  
