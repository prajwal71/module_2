import json
import re
import uuid
from functools import partial
import requests
from lxml import etree
from dateutil.relativedelta import relativedelta
from werkzeug.urls import url_encode
from datetime import datetime,date
from odoo import api, exceptions, fields, models, _
from odoo.tools import float_is_zero, float_compare, pycompat
from odoo.tools.misc import formatLang

from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

from odoo.addons import decimal_precision as dp
import logging


class AccountMoveInherited(models.Model):
    _inherit = 'account.move'


    @api.model
    def run_sql_my(self, qry):
        self._cr.execute(qry)
        # _res = self._cr.fetchall()
        # return _res

    
    copy_count = fields.Integer(default=0,string='Print Count', help="used for invoice printcount",store=True)
    bill_post = fields.Boolean(string='Sync state',track_visibility='always',default=False)
    bill_data = fields.Char(string='Bill Data',track_visibility='always')
    last_printed = fields.Datetime(string='Last Printed', default=lambda self: fields.datetime.now(),track_visibility='always')
    # customer_pan = fields.Char(string='PAN',readonly=True)



    @api.model
    def post_invoices_ird(self):
        inv_objs = self.env['account.move'].search([('state', '=', 'posted'),('type', 'in',('out_invoice','out_refund')),('bill_post' , '=', False)])
        if inv_objs:
            for invoice in inv_objs:
                comp_obj = self.env['res.company'].search([('id','=',invoice.company_id.id)],limit=1)
                if comp_obj.ird_integ == True:
                    headers = {'charset': 'utf-8'}
                    # d2 =  date(2019, 7, 17)
                    # d3 =  date(2020, 7, 16)
                    if comp_obj.fy_start < invoice.invoice_date < comp_obj.fy_end:
                        fy_yr = comp_obj.fy_prefix
                        if invoice.type == 'out_invoice':
                            url = 'http://103.1.92.174:9050/api/bill'
                            data = {
                                    "username" : str(comp_obj.ird_user),
                                    "password" : str(comp_obj.ird_password),
                                    "seller_pan" : str(invoice.company_id.vat),
                                    "buyer_pan" : str(invoice.partner_id.vat),
                                    "buyer_name" : str(invoice.partner_id.name),
                                    "fiscal_year" : fy_yr,
                                    "invoice_number":str(invoice.name),
                                    "invoice_date":str(invoice.invoice_date),
                                    "total_sales":str(invoice.amount_total),
                                    "taxable_sales_vat":str(invoice.amount_untaxed),
                                    "vat":str(invoice.amount_tax),
                                    "excisable_amount":"0",
                                    "excise":"0",
                                    "taxable_sales_hst":"0",
                                    "hst":"0",
                                    "amount_for_esf":"0",
                                    "esf":"0",
                                    "export_sales":"0",
                                    "tax_exempted_sales":"0",
                                    "isrealtime":"true",
                                    "datetimeclient" : str(datetime.now())
                            }
                            response = requests.post(url, json=data, headers=headers)
                            invoice.bill_data = json.dumps(data) + str(response)
                            invoice.bill_post = True
                        elif invoice.type == 'out_refund':
                            url = 'http://103.1.92.174:9050/api/billreturn'
                            data = {
                                    "username" : str(comp_obj.ird_user),
                                    "password" : str(comp_obj.ird_password),
                                    "seller_pan" : str(invoice.company_id.vat),
                                    "buyer_pan" : str(invoice.partner_id.vat),
                                    "buyer_name" : str(invoice.partner_id.name),
                                    "fiscal_year" : fy_yr,
                                    "ref_invoice_number":str(invoice.ref),
                                    "credit_note_number":str(invoice.name),
                                    "credit_note_date":str(invoice.invoice_date),
                                    "reason_for_return":str(invoice.ref),
                                    "total_sales":str(invoice.amount_total),
                                    "taxable_sales_vat":str(invoice.amount_untaxed),
                                    "vat":str(invoice.amount_tax),
                                    "excisable_amount":"0",
                                    "excise":"0",
                                    "taxable_sales_hst":"0",
                                    "hst":"0",
                                    "amount_for_esf":"0",
                                    "esf":"0",
                                    "export_sales":"0",
                                    "tax_exempted_sales":"0",
                                    "isrealtime":"true",
                                    "datetimeclient" : str(datetime.now())
                            }
                            response = requests.post(url, json=data, headers=headers)
                            invoice.bill_data = json.dumps(data) + str(response)
                            invoice.bill_post = True
                            print(response)
                    else:
                        raise UserError(_('Invalid date i.e out of current Fiscal Year.'))
                else:
                    print("IRD Disabled in this company")   
        else:
            print("All bills posted to IRD")

    
    @api.depends('amount_total')
    def get_amount_in_words(self):
        amount_in_words = self.currency_id.amount_to_text(self.amount_total)
        return amount_in_words

    # @api.onchange('partner_id')
    # def onchange_partner_value(self):
    #     if self.partner_id:
    #         if self.type in ('out_invoice','out_refund','in_invoice','in_refund','out_receipt','in_receipt'):
    #             self.customer_pan = self.partner_id.vat
    
    
    

class ResCompanyInherited(models.Model):
    _inherit = 'res.company'

    ird_integ = fields.Boolean('Connect to IRD',default=False)
    ird_user = fields.Char('IRD Username')
    ird_password =  fields.Char('IRD Password')
    fy_start = fields.Date('Fiscal Year Start')
    fy_end = fields.Date('Fiscal Year End')
    fy_prefix = fields.Char('FY prefix')

class ResPartnerInherited(models.Model):
    _inherit = 'res.partner'

    
    @api.onchange('vat')
    def _valid_pan(self):
        if self.vat:
            if((len(self.vat) == 0 or len(self.vat) == 9) and self.vat[0:].isdigit() == True):
                pass
                #print("ok")
            else:
                    #print("Invalid")(self):
                raise ValidationError(_('Invalid Pan Number'))