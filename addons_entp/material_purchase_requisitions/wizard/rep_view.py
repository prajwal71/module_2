# -*- coding: utf-8 -*-
import xlwt
import base64
import calendar
from io import StringIO
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning
from datetime import datetime
from odoo.http import request

class InvoiceReport(models.Model):
    _name = "req.reporting"
    
##    start_date = fields.Date(string='Start Date', required=True, default=datetime.today().replace(day=1))
##    end_date = fields.Date(string="End Date", required=True, default=datetime.now().replace(day = calendar.monthrange(datetime.now().year, datetime.now().month)[1]))
##    invoice_state = fields.Selection([
##            ('open', 'Open'),
##            ('paid', 'Paid'),
##        ], string='Status', default='open', required=True)
##    partner_select = fields.Selection([
##            ('customer','Customer'),
##            ('vendor', 'Vendor'),
##            ('both', 'Both'),
##        ], string='Partner', default='customer', required=True)
    invoice_data = fields.Char('Name',)
    file_name = fields.Binary('Employee Excel Report', readonly=True)
    state = fields.Selection([('choose', 'choose'), ('get', 'get')],
                             default='choose')
    prod_id = fields.Many2one('product.product','Product')
    p_id = fields.Many2one('hr.employee','Employee')
    x_id = fields.Integer('myids')
    y_id = fields.Integer('myid')
##
##    _sql_constraints = [
##            ('check','CHECK((start_date <= end_date))',"End date must be greater then start date")  
##    ]
##    @api.onchange('p_id')
##    def action_my_change(self):
##      self.x_id = self.p_id
    #@api.multi
    def action_invoice_report(self):
        uid = request.session.uid
        cr = self.env.cr
        workbook = xlwt.Workbook()
        format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour gray25;align: horiz center')
        format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
        format2 = xlwt.easyxf('font:bold True;align: horiz left')
        format3 = xlwt.easyxf('align: horiz left')
        format4 = xlwt.easyxf('align: horiz right')
        format5 = xlwt.easyxf('font:bold True;align: horiz right')
        format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz right')
        format7 = xlwt.easyxf('font:bold True;borders:top thick;align: horiz right')
        format8 = xlwt.easyxf('font:bold True;borders:top thick;pattern: pattern solid, fore_colour gray25;align: horiz left')
##        invoice_number = rec.number.split('/')
        sheet = workbook.add_sheet("Employee_report",cell_overwrite_ok=True)
        sheet.col(0).width = int(30*260)
        sheet.col(1).width = int(30*260)    
        sheet.col(2).width = int(18*260)    
        sheet.col(3).width = int(18*260) 
        sheet.col(4).width = int(33*260)   
        sheet.col(5).width = int(15*260)
        sheet.col(6).width = int(18*260)   
        
        sheet.write(2, 0, "Material", format1)
        sheet.write(2, 1, "Quantity", format1)
        sheet.write(2, 2, "Requisition Type", format1)
        #sheet.write(0, 2, "Deadline", format1)
#        self.x_id = self.p_id
        x_id = self.p_id.id
        # query = """select e.name as o_name,a.name o_names from account_invoice_line e
        #             inner join res_partner a on  (e.partner_id = a.id and a.id= %s)"""
        # self.env.cr.execute(query)
        self.env.cr.execute('select e.name as employee,l.description as material,l.qty as quantity ,l.requisition_type as type from material_purchase_requisition_line l inner join hr_employee e on l.emp_id = e.id and l.emp_id = %s', (x_id,))
        my_data3 = self.env.cr.dictfetchall()
        crm_label_sl1 = []      
        crm_dataset_sl1 = []
        row = 3
        for data in my_data3:
             sheet.write_merge(0, 1, 0, 4, 'Employee : ' + data['employee'] , format0)
             sheet.write(row, 0, (data['material']), format3)
             sheet.write(row, 1, (data['quantity']), format3)  
             sheet.write(row, 2, (data['type']), format3)           
##             sheet.write(row, 2, (data['o_deadline']), format4)
##             crm_label_sl1.append(data['o_name'])
##             crm_dataset_sl1.append(data['o_deadline'])
             row = row+1
     
        path = ("/home/odoo/Reports/Employee_Material_Report.xls")
        #filename = ('Deadline report'+ '.xls')
        workbook.save(path)
        file = open(path, "rb")
        file_data = file.read()
        out = base64.encodestring(file_data)
        self.write({'state': 'get', 'file_name': out, 'invoice_data':'Employee_Material_Report.xls'})
        return {
               'type': 'ir.actions.act_window',
               'res_model': 'req.reporting',
               'view_mode': 'form',
               'view_type': 'form',
               'res_id': self.id,
               'target': 'new',
              
            }                      
        
        #else:
         #   raise Warning("Currently No Invoice/Bills For This Data!!")
    
   
    #@api.multi
    def action_invoice_report1(self):
        uid = request.session.uid
        cr = self.env.cr
        workbook = xlwt.Workbook()
        format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour gray25;align: horiz center')
        format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
        format2 = xlwt.easyxf('font:bold True;align: horiz left')
        format3 = xlwt.easyxf('align: horiz left')
        format4 = xlwt.easyxf('align: horiz right')
        format5 = xlwt.easyxf('font:bold True;align: horiz right')
        format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz right')
        format7 = xlwt.easyxf('font:bold True;borders:top thick;align: horiz right')
        format8 = xlwt.easyxf('font:bold True;borders:top thick;pattern: pattern solid, fore_colour gray25;align: horiz left')
##        invoice_number = rec.number.split('/')
        sheet = workbook.add_sheet("Employee_report",cell_overwrite_ok=True)
        sheet.col(0).width = int(30*260)
        sheet.col(1).width = int(30*260)    
        sheet.col(2).width = int(18*260)    
        sheet.col(3).width = int(18*260) 
        sheet.col(4).width = int(33*260)   
        sheet.col(5).width = int(15*260)
        sheet.col(6).width = int(18*260)   
        
        sheet.write(2, 0, "Material", format1)
        sheet.write(2, 1, "Quantity", format1)
        sheet.write(2, 2, "Requisition Type", format1)
        #sheet.write(0, 2, "Deadline", format1)
#        self.x_id = self.p_id
        y_id = self.prod_id.id
        # query = """select e.name as o_name,a.name o_names from account_invoice_line e
        #             inner join res_partner a on  (e.partner_id = a.id and a.id= %s)"""
        # self.env.cr.execute(query)
        self.env.cr.execute('select e.name as employee,l.description as material,l.qty as quantity ,l.requisition_type as type from material_purchase_requisition_line l inner join hr_employee e on e.id = l.emp_id and l.product_id = %s', (y_id,))
        my_data3 = self.env.cr.dictfetchall()
        crm_label_sl1 = []      
        crm_dataset_sl1 = []
        row = 3
        for data in my_data3:
             sheet.write_merge(0, 1, 0, 4, 'Material : ' + data['material'] , format0)
             sheet.write(row, 0, (data['employee']), format3)
             sheet.write(row, 1, (data['quantity']), format3)  
             sheet.write(row, 2, (data['type']), format3)           
##             sheet.write(row, 2, (data['o_deadline']), format4)
##             crm_label_sl1.append(data['o_name'])
##             crm_dataset_sl1.append(data['o_deadline'])
             row = row+1
     
        path = ("/home/odoo/Reports/Material_Report.xls")
        #filename = ('Deadline report'+ '.xls')
        workbook.save(path)
        file = open(path, "rb")
        file_data = file.read()
        out = base64.encodestring(file_data)
        self.write({'state': 'get', 'file_name': out, 'invoice_data':'Material_Report.xls'})
        return {
               'type': 'ir.actions.act_window',
               'res_model': 'req.reporting',
               'view_mode': 'form',
               'view_type': 'form',
               'res_id': self.id,
               'target': 'new',
              
            }    
    #@api.multi
    def action_invoice_report2(self):
        uid = request.session.uid
        cr = self.env.cr
        workbook = xlwt.Workbook()
        format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour gray25;align: horiz center')
        format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
        format2 = xlwt.easyxf('font:bold True;align: horiz left')
        format3 = xlwt.easyxf('align: horiz left')
        format4 = xlwt.easyxf('align: horiz right')
        format5 = xlwt.easyxf('font:bold True;align: horiz right')
        format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz right')
        format7 = xlwt.easyxf('font:bold True;borders:top thick;align: horiz right')
        format8 = xlwt.easyxf('font:bold True;borders:top thick;pattern: pattern solid, fore_colour gray25;align: horiz left')
##        invoice_number = rec.number.split('/')
        sheet = workbook.add_sheet("Employee_report",cell_overwrite_ok=True)
        sheet.col(0).width = int(30*260)
        sheet.col(1).width = int(30*260)
        sheet.col(2).width = int(18*260)
        sheet.col(3).width = int(18*260)
        sheet.col(4).width = int(33*260)
        sheet.col(5).width = int(15*260)
        sheet.col(6).width = int(18*260)

        sheet.write(2, 0, "Employee", format1)
        sheet.write(2, 1, "Material", format1)
        sheet.write(2, 2, "Quantity", format1)
        sheet.write(2, 3, "Requisition Type", format1)
        #sheet.write(0, 2, "Deadline", format1)
#        self.x_id = self.p_id
        #x_id = self.p_id.id
        # query = """select e.name as o_name,a.name o_names from account_invoice_line e
        #             inner join res_partner a on  (e.partner_id = a.id and a.id= %s)"""
        # self.env.cr.execute(query)
        self.env.cr.execute('select e.name as employee,l.description as material,l.qty as quantity ,l.requisition_type as type from material_purchase_requisition_line l inner join hr_employee e on l.emp_id = e.id order by employee')
        my_data3 = self.env.cr.dictfetchall()
        crm_label_sl1 = []
        crm_dataset_sl1 = []
        row = 3
        for data in my_data3:
             sheet.write_merge(0, 1, 0, 4, "All Employee Report" , format0)
             sheet.write(row, 0, (data['employee']), format3)
             sheet.write(row, 1, (data['material']), format3)
             sheet.write(row, 2, (data['quantity']), format3)
             sheet.write(row, 3, (data['type']), format3)
##             sheet.write(row, 2, (data['o_deadline']), format4)
##             crm_label_sl1.append(data['o_name'])
##             crm_dataset_sl1.append(data['o_deadline'])
             row = row+1

        path = ("/home/odoo/Reports/All_Employee_Material_Report.xls")
        #filename = ('Deadline report'+ '.xls')
        workbook.save(path)
        file = open(path, "rb")
        file_data = file.read()
        out = base64.encodestring(file_data)
        self.write({'state': 'get', 'file_name': out, 'invoice_data':'All_Employee_Material_Report.xls'})
        return {
               'type': 'ir.actions.act_window',
               'res_model': 'req.reporting',
               'view_mode': 'form',
               'view_type': 'form',
               'res_id': self.id,
               'target': 'new',

            }



                  

