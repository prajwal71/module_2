# -*- coding: utf-8 -*-
import xlwt
import base64
import calendar
from io import StringIO
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning
from datetime import datetime

class InvoiceReport(models.TransientModel):
    _name = "invoice.book.report"
    
    start_date = fields.Date(string='Start Date', required=True, default=datetime.today().replace(day=1))
    end_date = fields.Date(string="End Date", required=True, default=datetime.now().replace(day = calendar.monthrange(datetime.now().year, datetime.now().month)[1]))
    # invoice_state = fields.Selection([
    #         ('open', 'Open'),
    #         ('paid', 'Paid'),
    #     ], string='Status', default='open', required=True)
    invoice_type = fields.Selection([
            ('out_invoice', 'Sales'),
            ('out_refund', 'Sales Return'),
            ('in_invoice','Purchase'),

        ], string='Type', default='out_invoice', required=True)
    # partner_select = fields.Selection([
    #         ('customer','Customer'),
    #         ('vendor', 'Vendor'),
    #         ('both', 'Both'),
    #     ], string='Partner', default='customer', required=True)
    invoice_data = fields.Char('Name',)
    file_name = fields.Binary('Invoice Excel Report', readonly=True)
    state = fields.Selection([('choose', 'choose'), ('get', 'get')],
                             default='choose')

    _sql_constraints = [('check','CHECK((start_date <= end_date))',"End date must be greater then start date")]

    # @api.multi
    def action_invoice_report(self):
        workbook = xlwt.Workbook()
        
        
        if self.invoice_type == 'out_invoice':
            invoice = self.env['account.move'].search([('invoice_date', '>=', self.start_date), ('invoice_date', '<=', self.end_date), 
                                                    ('state', '=', 'posted'),('type', '=','out_invoice')])
            sheet = workbook.add_sheet('Sale001')
            sheet.col(0).width = int(30*260)
            sheet.col(1).width = int(30*260)    
            sheet.col(2).width = int(18*260)    
            sheet.col(3).width = int(18*260) 
            sheet.col(4).width = int(33*260)   
            sheet.col(5).width = int(23*260)
            sheet.col(6).width = int(18*260)
            format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour gray25;align: horiz center')
            format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
            format2 = xlwt.easyxf('font:bold True;align: horiz left')
            format3 = xlwt.easyxf('align: horiz left')
            format4 = xlwt.easyxf('align: horiz right')
            format5 = xlwt.easyxf('font:bold True;align: horiz right')
            format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz right')
            format7 = xlwt.easyxf('font:bold True;borders:top thick;align: horiz right')
            format8 = xlwt.easyxf('font:bold True;borders:top thick;pattern: pattern solid, fore_colour gray25;align: horiz left')

            sheet.write_merge(0, 2, 0, 9, 'Sales Book ' , format0)
            
        elif self.invoice_type == 'out_refund':
            invoice = self.env['account.move'].search([('invoice_date', '>=', self.start_date), ('invoice_date', '<=', self.end_date), 
                                                    ('state', '=', 'posted'),('type', '=','out_refund')])
            
            sheet = workbook.add_sheet('Salereturn001')
            sheet.col(0).width = int(30*260)
            sheet.col(1).width = int(30*260)    
            sheet.col(2).width = int(18*260)    
            sheet.col(3).width = int(18*260) 
            sheet.col(4).width = int(33*260)   
            sheet.col(5).width = int(23*260)
            sheet.col(6).width = int(18*260)
            format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour gray25;align: horiz center')
            format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
            format2 = xlwt.easyxf('font:bold True;align: horiz left')
            format3 = xlwt.easyxf('align: horiz left')
            format4 = xlwt.easyxf('align: horiz right')
            format5 = xlwt.easyxf('font:bold True;align: horiz right')
            format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz right')
            format7 = xlwt.easyxf('font:bold True;borders:top thick;align: horiz right')
            format8 = xlwt.easyxf('font:bold True;borders:top thick;pattern: pattern solid, fore_colour gray25;align: horiz left')

            sheet.write_merge(0, 2, 0, 9, 'Sales Return' , format0)

        else:
            invoice = self.env['account.move'].search([('invoice_date', '>=', self.start_date), ('invoice_date', '<=', self.end_date), 
                                                    ('state', '=', 'posted'),('type', 'in',('in_invoice','in_refund'))])
            sheet = workbook.add_sheet('Purchase001')
            sheet.col(0).width = int(30*260)
            sheet.col(1).width = int(30*260)    
            sheet.col(2).width = int(18*260)    
            sheet.col(3).width = int(18*260) 
            sheet.col(4).width = int(33*260)   
            sheet.col(5).width = int(23*260)
            sheet.col(6).width = int(18*260)
            format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour gray25;align: horiz center')
            format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
            format2 = xlwt.easyxf('font:bold True;align: horiz left')
            format3 = xlwt.easyxf('align: horiz left')
            format4 = xlwt.easyxf('align: horiz right')
            format5 = xlwt.easyxf('font:bold True;align: horiz right')
            format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz right')
            format7 = xlwt.easyxf('font:bold True;borders:top thick;align: horiz right')
            format8 = xlwt.easyxf('font:bold True;borders:top thick;pattern: pattern solid, fore_colour gray25;align: horiz left')

            sheet.write_merge(0, 2, 0, 9, 'Purchase Book ' , format0)

        record = []
        if invoice:
            for rec in invoice:
                record.append(rec)
                # if self.partner_select == "customer" and rec.partner_id.customer == True and rec.partner_id.supplier == False:
                #     record.append(rec)
                # elif self.partner_select == "vendor" and rec.partner_id.supplier == True and rec.partner_id.customer == False:
                #     record.append(rec)
                # elif self.partner_select == "both" and rec.partner_id.customer == True and rec.partner_id.supplier == True:
                    # record.append(rec)
            file = StringIO()        
            self.record= record.sort(key=lambda p: (p.invoice_date, p.name))
            final_value = {}
            # data1 = {}
            # data1['start'] = str(self.start_date)
            # data1['end'] = str(self.end_date)
            
            timestamp = str(datetime.today())
            row=16
            if self.invoice_type == 'out_invoice' or self.invoice_type == 'out_refund':
                sheet.write(5, 0, "Seller Name:", format1)
                sheet.write(5, 1, str(rec.company_id.name), format2)
                sheet.write(6, 0, "Seller PAN:", format1)
                sheet.write(6, 1, str(rec.company_id.vat), format2)
                sheet.write(7, 0, "Seller Address:", format1)
                sheet.write(7, 1, str(rec.company_id.street), format2)
                sheet.write(8, 0, "Duration of sales", format1)
                sheet.write(8, 1, str(self.start_date), format2)
                sheet.write(8, 2, 'to', format2)
                sheet.write(8, 3, str(self.end_date), format2)
                sheet.write(10, 1, 'Report generated at:', format2)
                sheet.write(10, 2, timestamp, format2)
                
                
                sheet.write(15, 0, 'Date', format1)
                sheet.write(15, 1, 'Bill No', format1)
                sheet.write(15, 2, 'Buyer Name', format1)
                sheet.write(15, 3, 'Buyer PAN No.', format1)

                sheet.write(15, 4, 'Total Sales', format6)
                sheet.write(15, 5, 'Non-Taxable Sales', format6)
                sheet.write(15, 6, 'Export Sales', format6)
                sheet.write(15, 7, 'Discount', format6)
                sheet.write(15, 8, 'Amount', format6)
                sheet.write(15, 9, 'Tax(Rs)', format6)
                row1 = 16
                if record:
                    for rec in record:
                        sheet.write(row, 0, str(rec.invoice_date), format3)
                        sheet.write(row, 1, str(rec.name), format3)
                        sheet.write(row, 2, str(rec.partner_id.name), format3)
                        sheet.write(row, 3, str(rec.partner_id.vat), format3)
                        row=row+1
                    #     invoice_lines = []
                    #     for lines in rec.invoice_line_ids:
                    #         product = {
                                
                    #             'description'    : lines.product_id.name,
                    #             'quantity'       : lines.quantity,
                    #             'price_unit'     : lines.price_unit,
                    #             'price_subtotal' : lines.price_each_subtotal 
                    #         }
                    #         if lines.invoice_line_tax_ids:
                    #             taxes = []
                    #             for tax_id in lines.invoice_line_tax_ids:
                    #                 taxes.append(tax_id.name)
                    #             product['invoice_line_tax_ids'] = taxes
                    #         invoice_lines.append(product)
                        final_value['partner_id'] = rec.partner_id.name
                        final_value['pan_number'] = rec.partner_id.vat
                        final_value['address_buyer'] = rec.partner_id.street
                        final_value['company_id'] = rec.company_id.name
                        final_value['pan_number_seller'] = rec.company_id.vat
                        final_value['address_seller'] = rec.company_id.street
                        final_value['date_invoice'] = rec.invoice_date
                        # final_value['date_due'] = rec.date_due
                        final_value['number'] = rec.name
                        final_value['currency_id'] = rec.currency_id
                        # final_value['state'] = dict(self.env['account.invoice'].fields_get(allfields=['state'])['state']['selection'])[rec.state]
                        # final_value['payment_term_id'] = rec.payment_term_id.name
                        # final_value['origin'] = rec.origin
                        final_value['amount_untaxed'] = rec.amount_untaxed
                        # final_value['amount_subtotal_1'] = rec.amount_actual
                        final_value['amount_discount'] = "0.00"
                        final_value['amount_tax'] = rec.amount_tax
                        final_value['amount_total'] = rec.amount_total
                        # final_value['amnt_in_words'] = rec.get_amount_in_words()
                        sheet.write(row1, 4, str(final_value['currency_id'].symbol) + str(final_value['amount_total']), format4)
                        sheet.write(row1, 5, '0.0', format4)
                        sheet.write(row1, 6, '0.0', format4)
                        sheet.write(row1, 7, str(final_value['currency_id'].symbol) + str(final_value['amount_discount']), format4)
                        sheet.write(row1, 8, str(final_value['currency_id'].symbol) + str(final_value['amount_untaxed']), format4)
                        sheet.write(row1, 9, str(final_value['currency_id'].symbol) + str(final_value['amount_tax']), format4)
                        row1=row1+1
            else:
                sheet.write(5, 0, "Buyer Name:", format1)
                sheet.write(5, 1, str(rec.company_id.name), format2)
                sheet.write(6, 0, "Buyer PAN:", format1)
                sheet.write(6, 1, str(rec.company_id.vat), format2)
                sheet.write(7, 0, "Buyer Address:", format1)
                sheet.write(7, 1, str(rec.company_id.street), format2)
                sheet.write(8, 0, "Duration of sales", format1)
                sheet.write(8, 1, str(self.start_date), format2)
                sheet.write(8, 2, 'to', format2)
                sheet.write(8, 3, str(self.end_date), format2)
                sheet.write(10, 1, 'Report generated at:', format2)
                sheet.write(10, 2, timestamp, format2)
                
                
                sheet.write(15, 0, 'Date', format1)
                sheet.write(15, 1, 'Bill No', format1)
                sheet.write(15, 2, 'Seller Name', format1)
                sheet.write(15, 3, 'Seller PAN No.', format1)

                sheet.write(15, 4, 'Total Sales', format6)
                sheet.write(15, 5, 'Non-Taxable Sales', format6)
                sheet.write(15, 6, 'Export Sales', format6)
                sheet.write(15, 7, 'Discount', format6)
                sheet.write(15, 8, 'Amount', format6)
                sheet.write(15, 9, 'Tax(Rs)', format6)
                row1 = 16
                if record:
                    for rec in record:
                        sheet.write(row, 0, str(rec.invoice_date), format3)
                        sheet.write(row, 1, str(rec.name), format3)
                        sheet.write(row, 2, str(rec.partner_id.name), format3)
                        sheet.write(row, 3, str(rec.partner_id.vat), format3)
                        row=row+1
                    #     invoice_lines = []
                    #     for lines in rec.invoice_line_ids:
                    #         product = {
                                
                    #             'description'    : lines.product_id.name,
                    #             'quantity'       : lines.quantity,
                    #             'price_unit'     : lines.price_unit,
                    #             'price_subtotal' : lines.price_each_subtotal 
                    #         }
                    #         if lines.invoice_line_tax_ids:
                    #             taxes = []
                    #             for tax_id in lines.invoice_line_tax_ids:
                    #                 taxes.append(tax_id.name)
                    #             product['invoice_line_tax_ids'] = taxes
                    #         invoice_lines.append(product)
                        final_value['partner_id'] = rec.partner_id.name
                        final_value['pan_number'] = rec.partner_id.vat
                        final_value['address_buyer'] = rec.partner_id.street
                        final_value['company_id'] = rec.company_id.name
                        final_value['pan_number_seller'] = rec.company_id.vat
                        final_value['address_seller'] = rec.company_id.street
                        final_value['date_invoice'] = rec.invoice_date
                        # final_value['date_due'] = rec.date_due
                        final_value['number'] = rec.name
                        final_value['currency_id'] = rec.currency_id
                        # final_value['state'] = dict(self.env['account.invoice'].fields_get(allfields=['state'])['state']['selection'])[rec.state]
                        # final_value['payment_term_id'] = rec.payment_term_id.name
                        # final_value['origin'] = rec.origin
                        final_value['amount_untaxed'] = rec.amount_untaxed
                        # final_value['amount_subtotal_1'] = rec.amount_actual
                        final_value['amount_discount'] = "0.00"
                        final_value['amount_tax'] = rec.amount_tax
                        final_value['amount_total'] = rec.amount_total
                        # final_value['amnt_in_words'] = rec.get_amount_in_words()
                        sheet.write(row1, 4, str(final_value['currency_id'].symbol) + str(final_value['amount_total']), format4)
                        sheet.write(row1, 5, '0.0', format4)
                        sheet.write(row1, 6, '0.0', format4)
                        sheet.write(row1, 7, str(final_value['currency_id'].symbol) + str(final_value['amount_discount']), format4)
                        sheet.write(row1, 8, str(final_value['currency_id'].symbol) + str(final_value['amount_untaxed']), format4)
                        sheet.write(row1, 9, str(final_value['currency_id'].symbol) + str(final_value['amount_tax']), format4)
                        row1=row1+1
            # else:
            #     raise Warning("Currently No Invoice/Bills For This Data!!")
            path = ("/mnt/extra-addons/Sales_Book.xls")
            workbook.save(path)
            file = open(path, "rb")
            file_data = file.read()
            out = base64.encodestring(file_data)
            self.write({'state': 'get', 'file_name': out, 'invoice_data':'Sales_Book.xls'})
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'invoice.book.report',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': self.id,
                'target': 'new',
                }                
        else:
            raise Warning("Currently No Invoice/Bills For This Data!!")
