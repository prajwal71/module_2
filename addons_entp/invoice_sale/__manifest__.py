# -*- coding: utf-8 -*-
{
    'name': 'Sales/Purchase Book',
    'version': '13.0',
    'category': 'Accounts',
    'summary': ''' Prints Sales/Purchase Book Excel Report based on start date,end date,customer,vendor,both and you can also select
        whether the invoice state is open or paid.''',
    'author': 'Sagar',
    'license': "OPL-1",
    'depends': [
        'base',
        'account',
        'sale_management',
        'purchase'
    ],
    'data': [
        'wizard/invoice_sale.xml',
        
    ],
    'demo': [],  
    'auto_install': False,
    'installable': True,
    'application': True
}
