# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'PDC Mgmt',
    'version' : '12.0.1',
    'summary': 'This app helps you to create payments for PDC',
    'description': "PDC",
    'category': 'Accounting',
    'author': 'SAGAR JAYSWAL',
    'website': 'http://www.sagarcs.com',
    'license': 'AGPL-3',
    'data': ['views/views.xml',
    'data/data.xml',
    'views/cancel_wizard.xml',
    # 'views/set_target.xml'
    ],
    'depends': ['base','account','sale'],
    'installable': True,
    'application': False,
    'auto_install': False,
    
}
