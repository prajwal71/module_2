# -*- coding: utf-8 -*-
# 

from odoo import fields, models, api, _
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError
class LocManagement(models.Model):
    _name = 'loc.management'
    _description = 'LC Management'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'create_date desc'
    # @api.model
    # def create(self, values):
    #         values['name'] = self.env['ir.sequence'].next_by_code('loc.management')
    
    name = fields.Char('Reference',default=lambda self: self.env['ir.sequence'].next_by_code('loc.management'),copy=False, required=True,readonly=True)
    purchase_id = fields.Many2one('purchase.order','Purchase Order',required=True)
    group_id = fields.Many2one('procurement.group','Purchase Order')
    picking_ids = fields.Many2one('stock.picking','Delivery')
    landed_cost_id = fields.Many2one('stock.landed.cost','Landed cost',domain="[('picking_ids', '=', picking_ids)]")
    date = fields.Datetime('Date',required=True)
    # insurance
    insc_vendor_id = fields.Many2one('res.partner','Insurer')
    policy = fields.Many2one('lc.policy','Insurance Policy')
    # insc_amount = fields.Float('Insurance Amount')
    insc_documents = fields.Many2many('ir.attachment', 'doc_attach_relship', 'doc_id5', 'attach_id5', string="Insurance Documents",
                                         help='You can attach the copy of your document', copy=False)
    # insc_documents = fields.Binary('Insurance Documents')
    # pi
    seller_id = fields.Many2one('res.partner','Seller')
    buyer_id = fields.Many2one('res.partner','Buyer')
    date_pi = fields.Date('PI Date')
    pi_no = fields.Char('PI No.')
    pi_amount = fields.Float('PI Amount')
    pi_amount_usd = fields.Float('PI Amount(USD/AED)')
    excg_rate = fields.Float('Exchange Rate')
    performa_invoice = fields.Many2many('ir.attachment', 'doc_pro_relship', 'doc_id1', 'attach_id4', string="Proforma Invoice",
                                         help='You can attach the copy of your document', copy=False)
    # performa_invoice = fields.Binary('Proforma Invoice')
    flag = fields.Boolean('Flag',default=False)
    flag1 = fields.Boolean('Flag1',default=False)
    flag2 = fields.Boolean('Flag2',default=False)
    #LC Details & Bank Details
    alarm_ids = fields.Many2many('calendar.alarm', 'lc_event_rel', string='Reminders', ondelete="restrict", copy=False)
    bank_lc_charge = fields.Float('Bank Charge')
    bank_issue_charge = fields.Float('Issue Charge')
    swift_charge = fields.Float('Swift Charge I')
    # swift_charge_sc = fields.Float('Swift Charge II')
    lc_no = fields.Char('LC No')
    date_lc = fields.Date('LC Date')
    lc_amount = fields.Float('LC Amount')
    lc_terms = fields.Many2one('lc.term','LC Terms')
    lc_documents = fields.Many2many('ir.attachment', 'doc_lc_relship', 'doc_id7', 'attach_id7', string="LC Document",
                                         help='You can attach the copy of your document', copy=False)
    buyer_bank_id = fields.Many2one('res.bank','Buyer Bank')
    buyer_acc_no = fields.Many2one('res.partner.bank','Buyer Account No')
    seller_bank_id = fields.Many2one('res.bank','Seller Bank')
    seller_acc_no = fields.Many2one('res.partner.bank','Seller Account No')
    
    #Forwarder & Custom Details
    fr_vendor_id = fields.Many2one('res.partner','Forwarder')
    bol_no  = fields.Char('Bill Of Lading')
    bol_doc = fields.Binary('BOL Document')
    bol_doc_name = fields.Char('BOL Document Name')
    
    trn_vendor_id = fields.Many2one('res.partner','Transporter')
    date_lc = fields.Date('LC Date')
    vec_number = fields.Char('Vehicle Number')
    # bill_ref = 
    date_pp = fields.Date('Pragyapan Date')
    pragyapan_no = fields.Char('Pragyapan No')
    tax_appliable = fields.Float('Import Duty')
    tax_appliable_addnl = fields.Float('Additional Import Duty')
    custom_ecs = fields.Float('Custom & Clearance (ECS)')
    tax_csf = fields.Float('CSF')
    tax_value_details = fields.Float('Value Details')



    state = fields.Selection([('draft','Draft'),('pi','PI'),('approved','Approved by Bank'),('in_transit','In Transit'),('port','At Custom'),('custom','Departed'),('at_warehouse','At Warehouse'),('tr','TR Booked'),('l_c','Costing'),('closed','Closed & Settled')], default='draft', string='State',track_visibility='onchange')
    custom_documents = fields.Many2many('ir.attachment', 'doc_attach_custom', 'doc_id6', 'attach_id6', string="Custom Documents",
                                         help='You can attach the copy of your document', copy=False)
    
    import_documents = fields.Many2many('ir.attachment', 'doc_import_reln', 'doc_id_im_6', 'attach_id_im_6', string="Import Documents",
                                         help='You can attach the copy of your document', copy=False)
    # accounting
    margin_amount_f = fields.Float('5% Margin Amt')
    margin_amount_t = fields.Float('10% Margin Amt')
    nrb_chq_no = fields.Char('NRB Cheque No')
    # nrb_chq_amount = fields.Float('NRB Checque Amnt')
    currency_id =  fields.Many2one(
        'res.currency', 'Currency',
        required=True)
    account_journal_id = fields.Many2one('account.journal', 'Account Journal',
        required=True)
    account_move_id_aa = fields.Many2one(
        'account.move', 'JE (5%)',
        copy=False, readonly=True)
    account_move_id_bb = fields.Many2one('account.move', 'JE (5% Refund)',copy=False, readonly=True)
    account_move_id_cc = fields.Many2one(
        'account.move', 'JE (10%)',
        copy=False, readonly=True)
    account_move_id_dd = fields.Many2one(
        'account.move', 'JE (10% Refund)',
        copy=False, readonly=True)
    account_move_id_ee = fields.Many2one(
        'account.move', 'JE Bank Charge',
        copy=False, readonly=True)
    account_id_dr = fields.Many2one('account.account', 'Debit Account',required=True, domain=[('deprecated', '=', False)],states={'approved': [('readonly', True)]})
    account_id_cr = fields.Many2one('account.account', 'Credit Account',required=True, domain=[('deprecated', '=', False)],states={'approved': [('readonly', True)]})
    move_bnk_issue_id = fields.Many2one(
        'account.move', 'Issue Charge',
        copy=False, readonly=True)
    move_bnk_sft_id = fields.Many2one(
        'account.move', 'Swift Charge',
        copy=False, readonly=True)
    # @api.multi
    def change_state_pi(self):
        if (self.pi_amount != False and self.performa_invoice):
            self.update({'state': 'pi'})
        else:
            raise UserError(_('Please Enter PI Amount and document .')) 
    @api.onchange('pi_amount')
    def change_pi_amnt(self):
        if self.pi_amount:
            self.margin_amount_f = (0.05*self.pi_amount)
            self.margin_amount_t = (0.10*self.pi_amount)
            self.bank_lc_charge =  (0.0001*self.pi_amount)
    @api.onchange('pi_amount_usd','excg_rate')
    def change_pi_ambank_lc_chargent_usd(self):
        if self.pi_amount_usd:
            self.pi_amount = (self.pi_amount_usd*self.excg_rate)
            
    #Posting journal entries
    @api.onchange('purchase_id')
    def change_po(self):
        if self.purchase_id:
            self.group_id = self.purchase_id.group_id.id
            self.seller_id = self.purchase_id.partner_id.id
            self.buyer_id = self.purchase_id.company_id.partner_id.id

            # self.margin_amount_t = (0.10*self.pi_amount)
    # @api.multi
    def action_approve_done(self):
        if (self.pi_amount != False and self.performa_invoice and self.lc_no != False): 
            for rec in self:
                debit = credit = rec.currency_id.compute(rec.margin_amount_f, rec.currency_id) 
                debit_bc = credit_bc = rec.currency_id.compute(rec.bank_lc_charge, rec.currency_id) 
                debit_issue = credit_issue = rec.currency_id.compute(rec.bank_issue_charge, rec.currency_id)          
                # if rec.state == 'draft':
                #     raise UserError(_("Only a Submitted payment can be posted. Trying to post a payment in state %s.") % rec.state)
        
                # sequence_code = 'hr.advance.sequence'
                # rec.name = self.env['ir.sequence'].with_context(ir_sequence_date=rec.payment_date).next_by_code(sequence_code)
                cal_obj = self.env['calendar.event']
                cal_obj.create(
                {   'name':self.name ,
                    'start': datetime.now()+relativedelta(days=21),
                    'stop': datetime.now()+relativedelta(days=21),
                    'alarm_ids':[(6,0, self.alarm_ids.ids)]}
                )
                self.flag2 = True
                # move = {
                #     'name': '/',
                #     'journal_id': rec.account_journal_id.id,
                #     'date': date.today(),
        
                #              'line_ids': [(0, 0, {
                #             'name': '5% margin TRF',
                #             'debit': debit,
                #             'account_id': rec.account_id_dr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         }), (0, 0, {
                #             'name':  '5% margin TRF',
                #             'credit': credit,
                #             'account_id': rec.account_id_cr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         })]
                # }
                # move_bnk_lc = {
                #     'name': '/',
                #     'journal_id': rec.account_journal_id.id,
                #     'date': date.today(),
        
                #     'line_ids': [(0, 0, {
                #             'name': 'Bank Charge',
                #             'debit': debit_bc,
                #             'account_id': rec.account_id_dr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         }), (0, 0, {
                #             'name':  'Bank Charge',
                #             'credit': credit_bc,
                #             'account_id': rec.account_id_cr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         })]
                # }
                # move_id = self.env['account.move'].create(move)
                # move_id.post() 

                # move_bnk_lc_id = self.env['account.move'].create(move_bnk_lc)
                # move_bnk_lc_id.post() 

                # move_bnk_issue = {
                #     'name': '/',
                #     'journal_id': rec.account_journal_id.id,
                #     'date': date.today(),
        
                #     'line_ids': [(0, 0, {
                #             'name': 'Issue Charge',
                #             'debit': debit_issue,
                #             'account_id': rec.account_id_dr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         }), (0, 0, {
                #             'name':  'Issue Charge',
                #             'credit': debit_issue,
                #             'account_id': rec.account_id_cr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         })]
                # }
                # move_bnk_iss_id = self.env['account.move'].create(move_bnk_issue)
                # move_bnk_iss_id.post() 
                # # self.account_move_id_aa = move_id.id  
                # return rec.write({'state': 'approved', 'account_move_id_aa': move_id.id,'account_move_id_ee':move_bnk_lc_id.id,'move_bnk_issue_id':move_bnk_iss_id.id})
                return rec.write({'state': 'approved'})                  
                
        else:
            raise UserError(_('Please Bank Details and LC No.'))
    # @api.multi
    def action_ship_done(self):
        if self.fr_vendor_id: 
            self.update({'state': 'in_transit'})
            # lst = []
            # for rec in self:
            #     lst.append(rec.alarm_ids.id)
            
            cal_obj = self.env['calendar.event']
            cal_obj.create(
                {
                    'name':self.name ,
                    'start': datetime.now()+relativedelta(days=90),
                    'stop': datetime.now()+relativedelta(days=90),
                    'alarm_ids':[(6,0, self.alarm_ids.ids)]
                                    }
            )
            self.flag = True
            # self = self.with_context(show_message=True) 
        else:
            raise UserError(_('Please Enter Forwarder Details'))
    # @api.multi
    def action_port_done(self):
        
        # self = self.with_context(show_message=False)
        if (self.bol_no != False): 
            for rec in self:
                debit = credit = rec.currency_id.compute(rec.margin_amount_t, rec.currency_id)           
                # move = {
                #     'name': '/',
                #     'journal_id': rec.account_journal_id.id,
                #     'date': date.today(),
        
                #     'line_ids': [(0, 0, {
                #             'name': '10% margin TRF',
                #             'debit': debit,
                #             'account_id': rec.account_id_dr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         }), (0, 0, {
                #             'name':  '10% margin TRF',
                #             'credit': credit,
                #             'account_id': rec.account_id_cr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         })]
                # }
                # move_id = self.env['account.move'].create(move)
                # move_id.post() 
                # # self.account_move_id_aa = move_id.id  
                # return rec.write({'state': 'port', 'account_move_id_cc': move_id.id,'flag':False })
                return rec.write({'state': 'port'})
        else:
            raise UserError(_('Please Enter Bill Of Ladding Details.'))
    # @api.multi
    def action_custom_done(self):
        if (self.pragyapan_no != False ): 
        # if (self.pi_amount != False and self.performa_invoice != False): 
            for rec in self:
                debit = credit = rec.currency_id.compute(rec.margin_amount_t, rec.currency_id)           
                # if rec.state == 'draft':
                #     raise UserError(_("Only a Submitted payment can be posted. Trying to post a payment in state %s.") % rec.state)
        
                # sequence_code = 'hr.advance.sequence'
                # rec.name = self.env['ir.sequence'].with_context(ir_sequence_date=rec.payment_date).next_by_code(sequence_code)
                
                # move = {
                #     'name': '/',
                #     'journal_id': rec.account_journal_id.id,
                #     'date': date.today(),
        
                #     'line_ids': [(0, 0, {
                #             'name': '10% margin Refund(NRB Cheque)',
                #             'debit': debit,
                #             'account_id': rec.account_id_cr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         }), (0, 0, {
                #             'name':  '10% margin Refund(NRB Cheque)',
                #             'credit': credit,
                #             'account_id': rec.account_id_dr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         })]
                # }
                # move_id = self.env['account.move'].create(move)
                # move_id.post() 
                # # self.account_move_id_aa = move_id.id  
                # return rec.write({'state': 'custom', 'account_move_id_dd': move_id.id})
                return rec.write({'state': 'custom'})
        else:
            raise UserError(_('Please Provide Pragayapan No..'))
    # @api.multi
    def action_warehouse_done(self):
        if self.picking_ids and self.trn_vendor_id != False: 
            self.update({'state': 'at_warehouse'})
        else:
            raise UserError(_('Please Delivery Reference No and Transport Details.'))   
    # @api.multi
    def action_book_tr(self):
        # lst = []
        # for rec in self:
        #     lst.append(rec.alarm_ids.id)
        # set remainder and email
            # self.update({'state': 'tr'})
        cal_obj = self.env['calendar.event']
        cal_obj.create(
                {   'name':self.name ,
                    'start': datetime.now()+relativedelta(days=30),
                    'stop': datetime.now()+relativedelta(days=30),
                    'alarm_ids':[(6,0, self.alarm_ids.ids)]}
            )
        # self = self.with_context(show_message=True)
        self.flag1 = True
        for rec in self:
                debit = credit = rec.currency_id.compute(rec.margin_amount_f, rec.currency_id)
                # move = {
                #     'name': '/',
                #     'journal_id': rec.account_journal_id.id,
                #     'date': date.today(),
        
                #     'line_ids': [(0, 0, {
                #             'name': '5% margin Refund',
                #             'debit': debit,
                #             'account_id': rec.account_id_cr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         }), (0, 0, {
                #             'name':  '5% margin Refund',
                #             'credit': credit,
                #             'account_id': rec.account_id_dr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         })]
                # }
                # move_id = self.env['account.move'].create(move)
                # move_id.post() 
                
                # move_swift = {
                #     'name': '/',
                #     'journal_id': rec.account_journal_id.id,
                #     'date': date.today(),
        
                #     'line_ids': [(0, 0, {
                #             'name': 'Swift Charge',
                #             'debit': debit,
                #             'account_id': rec.account_id_cr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         }), (0, 0, {
                #             'name':  'Swift Charge',
                #             'credit': credit,
                #             'account_id': rec.account_id_dr.id,
                #             # 'partner_id': rec.employee_id.user_id.partner_id.id,
                #         })]
                # }
                # move_sft_id = self.env['account.move'].create(move_swift)
                # move_sft_id.post() 
                # # self.account_move_id_aa = move_id.id  
                # return rec.write({'state': 'tr', 'account_move_id_bb': move_id.id,'move_bnk_sft_id':move_sft_id.id})
                return rec.write({'state': 'tr'})
        # else:
        #     raise UserError(_('Please Provide Custom & Delivery details .'))
    # @api.multi
    def action_landed_cost(self):
        lst = []
        self.flag1 = False
        for rec in self:
            lst.append(rec.picking_ids.id)
        My_lc = self.env['stock.landed.cost']
        My_lc.create(
            {
            'date': date.today(),
            'account_journal_id': self.account_journal_id.id,
            'picking_ids': [(6, 0, lst)], 
            'purchase_id' : self.purchase_id.id,
            })
        latest_rec = self.env['stock.landed.cost'].search([], limit=1, order='create_date desc')
        if latest_rec:
            self.update({'state': 'l_c'})
            return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'stock.landed.cost',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'res_id': int(latest_rec),
            }
    # @api.multi
    def action_close_done(self):
        if self.landed_cost_id: 
            self.update({'state': 'closed'})
        else:
            raise UserError(_('Please enter Landed Cost Reference No.'))    
    
class LcTerms(models.Model):
    _name = "lc.term"
    _description = "LC Terms"

    name = fields.Char('Name')
    types = fields.Char('Type')
    desc = fields.Text('Description')

class LcPolicy(models.Model):
    _name = "lc.policy"
    _description = "LC Policy"

    name = fields.Char('Name')
    types = fields.Char('Type')
    desc = fields.Text('Description')

class LcAttchment(models.Model):
    _inherit = 'ir.attachment'

    doc_attach_relship = fields.Many2many('loc.management', 'insc_documents', 'attach_id5', 'doc_id5',
                                      string="Insurance Document", invisible=1)
    doc_pro_relship = fields.Many2many('loc.management', 'performa_invoice', 'attach_id4', 'doc_id1',
                                      string="Insurance Document", invisible=1)
    doc_attach_custom = fields.Many2many('loc.management', 'custom_documents', 'attach_id6', 'doc_id6',
                                      string="Custom Document", invisible=1)
    doc_lc_relship = fields.Many2many('loc.management', 'lc_documents', 'attach_id7', 'doc_id7',
                                      string="LC Document", invisible=1)
    doc_lc_relship = fields.Many2many('loc.management', 'lc_documents', 'attach_id7', 'doc_id7',
                                      string="LC Document", invisible=1)
    doc_import_reln = fields.Many2many('loc.management', 'import_documents', 'attach_id7', 'doc_id7',
                                      string="LC Document", invisible=1)

class LcRemainders(models.Model):
    _inherit = 'calendar.alarm'

    lc_event_rel  = fields.Many2many('loc.management', 'alarm_ids', 
                                      string="Reminders", invisible=1)

