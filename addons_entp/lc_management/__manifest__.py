{
    "name" : "Letter of Credit Management",
    "version" : "13.0.0.1",
    "category" : "Accounting",
    "depends" : ['base','purchase','sale','stock','account','stock_landed_costs'],
    "author": "Sagar",
    'summary': 'This apps helps state of LC consignment',
    "description": """
    """,
    "website" : "www.sagarcs.com",
    "price": 100,
    "currency": 'EUR',
    "data": [
        'data/data.xml',
        'views/loc_view.xml',
        'security/lc_security.xml'
        # 'views/loc_view_2.xml',
       
    ],
    'qweb': [
],
    "auto_install": False,
    "installable": True,
    "images":["static/description/Banner.png"],
}

