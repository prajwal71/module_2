# -*- coding: utf-8 -*-
{
    'name':"Map View",
    'summary':"Defines the map view for odoo enterprise",
    'description':"Allows the viewing of records on a map",
    'category': 'Hidden',
    'version':'1.0',
    'depends':['web', 'base_setup'],
    'data':[
        "views/res_config_settings.xml",
        "views/res_partner_views.xml",
    ],
    'auto_install': True,
    'license': 'LGPL-3',
    'assets': {
        'web.assets_backend': [
            'map_view/static/src/js/map_controller.js',
            'map_view/static/src/js/map_model.js',
            'map_view/static/src/js/map_renderer.js',
            'map_view/static/src/js/map_view.js',
            'map_view/static/lib/leaflet/leaflet.css',
            'map_view/static/src/scss/map_view.scss',
        ],
        'web.qunit_suite_tests': [
            'map_view/static/tests/**/*',
        ],
        'web.assets_qweb': [
            'map_view/static/src/xml/**/*',
        ],
    }
}
