# coding: utf-8

import base64
import json
import binascii
from collections import OrderedDict
import hashlib
import hmac
import logging
from itertools import chain

from werkzeug import urls
import datetime
from dateutil import relativedelta
from odoo import api, fields, models, tools, _
# from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.exceptions import ValidationError
# from odoo.addons.payment_hbl.controllers.main import HblController
from odoo.tools.pycompat import to_text

_logger = logging.getLogger(__name__)



class AcquirerHbl(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('hbl', 'HBL')])
    hbl_merchant_account_id = fields.Char('Merchant Account ID(MID)', required_if_provider='hbl', groups='base.group_user')
    hbl_merchant_name = fields.Char('Merchant Name', required_if_provider='hbl', groups='base.group_user')
    hbl_secret_key = fields.Char('Secret Key', required_if_provider='hbl', groups='base.group_user')

    @api.model
    def _hbl_convert_amount(self, amount):
        """
            Hbl requires the amount to be padded.
        """
        paymentAmount = (amount*100)
        paymentAmount = int(paymentAmount)
        payAmount = ('000000000000%s' % str(paymentAmount))[-12:]
        return payAmount

    @api.model
    def _get_hbl_urls(self, environment):
        """ Hbl URLs:  """
        if environment == 'prod':
            return {
                'hbl_form_url': 'https://hblpgw.2c2p.com/HBLPGW/Payment/Payment/Payment',
            }
        else:
            return {
                'hbl_form_url': 'https://uat3ds.2c2p.com/HBLPGW/Payment/Payment/Payment',
            }


    def _hbl_generate_hash(self,invoiceNo,amount,currencyCode,nonSecure):
        """ 
            Generate hash value for hbl.
        """
        signatureString = self.hbl_merchant_account_id+invoiceNo+amount+currencyCode+nonSecure
        key_bytes= bytes(self.hbl_secret_key, 'utf-8') # Commonly 'latin-1' or 'utf-8'
        data_bytes = bytes(signatureString, 'utf-8')
        hashval = hmac.new(key_bytes, data_bytes, hashlib.sha256).hexdigest()

        return hashval
    
    def _hbl_generate_hash_check(self,data):
        """ 
            Generate hash value for hbl.
        """
        signatureString = data
        key_bytes= bytes(self.hbl_secret_key, 'utf-8') # Commonly 'latin-1' or 'utf-8'
        data_bytes = bytes(signatureString, 'utf-8')
        hashval = hmac.new(key_bytes, data_bytes, hashlib.sha256).hexdigest()

        return hashval

    def hbl_form_generate_values(self, values):
        base_url = self.get_base_url()
        # tmp
     
        amount = self._hbl_convert_amount(values['amount'])
        values.update({
            'paymentGatewayID': self.hbl_merchant_account_id,
            'amount': amount,
            'currencyCode': "524",
            'invoiceNo': values['reference'],
            'productDesc': values['reference'],
            'nonSecure': "Y",
            'hashValue': self._hbl_generate_hash(values['reference'],amount,"524","Y"),
            'return_url': "https://sbfurniturenepal.com/payment/hbl/return/" 
            
            # 'resURL': urls.url_join(base_url, AdyenController._return_url),
            # 'merchantReturnData': json.dumps({'return_url': '%s' % values.pop('return_url')}) if values.get('return_url', '') else False,
            # 'shopperEmail': values.get('partner_email') or values.get('billing_partner_email') or '',
        })
        return values

    def hbl_get_form_action_url(self):
        self.ensure_one()
        environment = 'prod' if self.state == 'enabled' else 'test'
        return self._get_hbl_urls(environment)['hbl_form_url']


class TxHbl(models.Model):
    _inherit = 'payment.transaction'

    @api.model
    def _hbl_convert_amount(self, amount):
        """
            Hbl requires the amount to be padded.
        """
        paymentAmount = (amount*100)
        paymentAmount = int(paymentAmount)
        payAmount = ('000000000000%s' % str(paymentAmount))[-12:]
        return payAmount

    @api.model
    def _get_hbl_urls(self, environment):
        """ Hbl URLs:  """
        if environment == 'prod':
            return {
                'hbl_form_url': 'https://hblpgw.2c2p.com/HBLPGW/Payment/Payment/Payment',
            }
        else:
            return {
                'hbl_form_url': 'https://uat3ds.2c2p.com/HBLPGW/Payment/Payment/Payment',
            }


    def _hbl_generate_hash(self,invoiceNo,amount,currencyCode,nonSecure):
        """ 
            Generate hash value for hbl.
        """
        signatureString = self.acquirer_id.hbl_merchant_account_id+invoiceNo+amount+currencyCode+nonSecure
        key_bytes= bytes(self.acquirer_id.hbl_secret_key, 'utf-8') # Commonly 'latin-1' or 'utf-8'
        data_bytes = bytes(signatureString, 'utf-8')
        hashval = hmac.new(key_bytes, data_bytes, hashlib.sha256).hexdigest()

        return hashval
    
    def _hbl_generate_hash_check(self,data):
        """ 
            Generate hash value for hbl.
        """
        signatureString = data
        key_bytes= bytes(self.hbl_secret_key, 'utf-8') # Commonly 'latin-1' or 'utf-8'
        data_bytes = bytes(signatureString, 'utf-8')
        hashval = hmac.new(key_bytes, data_bytes, hashlib.sha256).hexdigest()

        return hashval

    def hbl_form_generate_values(self, values):
        base_url = self.get_base_url()
        # tmp
     
        amount = self._hbl_convert_amount(values['amount'])
        values.update({
            'paymentGatewayID': self.hbl_merchant_account_id,
            'amount': amount,
            'currencyCode': "524",
            'invoiceNo': values['reference'],
            'productDesc': values['reference'],
            'nonSecure': "Y",
            'hashValue': self._hbl_generate_hash(values['reference'],amount,"524","Y"),
            'return_url': "https://sbfurniturenepal.com/payment/hbl/return/" 
            # 'resURL': urls.url_join(base_url, AdyenController._return_url),
            # 'merchantReturnData': json.dumps({'return_url': '%s' % values.pop('return_url')}) if values.get('return_url', '') else False,
            # 'shopperEmail': values.get('partner_email') or values.get('billing_partner_email') or '',
        })
        return values

    def hbl_get_form_action_url(self):
        self.ensure_one()
        environment = 'prod' if self.state == 'enabled' else 'test'
        return self._get_hbl_urls(environment)['hbl_form_url']


    # --------------------
    # FORM RELATED METHODS
    # --------------------

    def _get_specific_rendering_values(self, processing_values):
        """ Override of payment to return Paypal-specific rendering values.

        Note: self.ensure_one() from `_get_processing_values`

        :param dict processing_values: The generic and specific processing values of the transaction
        :return: The dict of acquirer-specific processing values
        :rtype: dict
        """
        res = super()._get_specific_rendering_values(processing_values)
        if self.provider != 'hbl':
            return res

        if self.acquirer_id.state  == 'enabled':
            api_url =  "https://hblpgw.2c2p.com/HBLPGW/Payment/Payment/Payment"
        else:
            api_url =  "https://uat3ds.2c2p.com/HBLPGW/Payment/Payment/Payment"
        base_url = self.acquirer_id.get_base_url()
        # partner_first_name, partner_last_name = payment_utils.split_partner_name(self.partner_name)
        # notify_url = self.acquirer_id.paypal_use_ipn \
        #   
        #            and urls.url_join(base_url, PaypalController._notify_url)
        amount = self._hbl_convert_amount(self.amount)
        return {
            # 'address1': self.partner_address,
            # 'amount': float_repr(float_round(self.amount, 2) * 100, 0),
            # # 'business': self.acquirer_id.paypal_email_account,
            # 'city': self.partner_city,
            # 'country': self.partner_country_id.code,
            # 'currency_code': self.currency_id.name,
            # 'email': self.partner_email,
            # 'first_name': self.partner_name,
            # 'handling': self.fees,
            # 'item_name': f"{self.company_id.name}: {self.reference}",
            # 'item_number': self.reference,
            
            # 'lc': self.partner_lang,
            # # 'notify_url': notify_url,
            # # 'return_url': urls.url_join(base_url, PaypalController._return_url),
            # 'state': self.partner_state_id.name,
            # 'zip_code': self.partner_zip,
            # 'acquirer': self.acquirer_id,
            # 'publicKey':self.acquirer_id.khalti_key_public,
            # 'productIdentity':self.reference,
            # 'productName': self.partner_name,
            # 'api_url': api_url,
             'paymentGatewayID': self.acquirer_id.hbl_merchant_account_id,
            'amount': amount,
            'currencyCode': "524",
            'invoiceNo': self.reference,
            'productDesc': self.reference,
            'nonSecure': "Y",
            'hashValue': self._hbl_generate_hash(self.reference,amount,"524","Y"),
            'return_url': "https://sbfurniturenepal.com/payment/hbl/return/" ,
            'api_url': api_url,
        }

    @api.model
    def _hbl_form_get_tx_from_data(self, data):
        paymentGatewayID, respCode, fraudCode, Pan, Amount, invoiceNo, tranRef, approvalCode, Eci, hashValue, Status, dateTime  = data.get('paymentGatewayID'), data.get('respCode'), data.get('fraudCode'), data.get('Pan'), data.get('Amount'),data.get('invoiceNo'), data.get('tranRef'), data.get('approvalCode'), data.get('Eci'),data.get('hashValue'),data.get('Status'),data.get('dateTime')
        if not paymentGatewayID or not respCode or not hashValue or not Status or not approvalCode:
            error_msg = _('Hbl: received data with missing reference (%s) or missing pspReference (%s)') % (reference, pspReference)
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        # find tx -> @TDENOTE use pspReference ?
        tx = self.env['payment.transaction'].search([('reference', '=', invoiceNo)])
        if not tx or len(tx) > 1:
            error_msg = _('Hbl: received data for reference %s') % (invoiceNo)
            if not tx:
                error_msg += _('; no order found')
            else:
                error_msg += _('; multiple order found')
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        # verify shasign
        sigString = paymentGatewayID +  respCode + fraudCode + Pan + Amount +  invoiceNo + tranRef + approvalCode + Eci +  dateTime + Status 
        shasign_check = tx.acquirer_id._hbl_generate_hash_check(sigString)
        if shasign_check == hashValue:
            error_msg = _('Hbl: invalid hashValue, received %s, computed %s') % (data.get('hashValue'), shasign_check)
            _logger.warning(error_msg)
            raise ValidationError(error_msg)

        return tx

    # def _adyen_form_get_invalid_parameters(self, data):
    #     invalid_parameters = []

    #     # reference at acquirer: pspReference
    #     if self.acquirer_reference and data.get('pspReference') != self.acquirer_reference:
    #         invalid_parameters.append(('pspReference', data.get('pspReference'), self.acquirer_reference))
    #     # seller
    #     if data.get('skinCode') != self.acquirer_id.adyen_skin_code:
    #         invalid_parameters.append(('skinCode', data.get('skinCode'), self.acquirer_id.adyen_skin_code))
    #     # result
    #     if not data.get('authResult'):
    #         invalid_parameters.append(('authResult', data.get('authResult'), 'something'))

        # return invalid_parameters

    def _hbl_form_validate(self, data):
        resp = data.get('respCode')
        # approval = data.get('approvalCode')
        status = data.get('Status')
        if resp == '00' and status == 'AP':
            self.write({'acquirer_reference': data.get('tranRef')})
            self._set_transaction_done()
            return True
        elif resp != '00' and status == 'PE':
            self.write({'acquirer_reference': data.get('tranRef')})
            self._set_transaction_pending()
            return True
        else:
            error = _('Hbl: feedback error')
            _logger.info(error)
            self.write({'state_message': error})
            self._set_transaction_cancel()
            return False
class AccountPaymentMethod(models.Model):
    _inherit = 'account.payment.method'

    @api.model
    def _get_payment_method_information(self):
        res = super()._get_payment_method_information()
        res['hbl'] = {'mode': 'unique', 'domain': [('type', '=', 'bank')]}
        return res
