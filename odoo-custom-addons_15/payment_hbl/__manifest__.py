# -*- coding: utf-8 -*-

{
    'name': 'Hbl Payment Acquirer',
    'category': 'Accounting/Payment',
    'summary': 'Payment Acquirer: HBL Implementation',
    'version': '1.0',
    'description': """HBL Payment Acquirer""",
    'depends': ['payment'],
    'data': [
        'views/payment_hbl_templates.xml',
        'data/payment_acquirer_data.xml',
        'views/payment_views.xml',
        
        
    ],
    'installable': True,
    # 'post_init_hook': 'create_missing_journal_for_acquirers',
    # 'uninstall_hook': 'uninstall_hook',
}
