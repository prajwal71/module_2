odoo.define('nexus_website_city.portal', function(require) {
    "use strict";
   
 
    var core = require('web.core');
    var ajax = require('web.ajax');
    
    var _t = core._t;
    
    // COPY TO CLIPBOARD
    
      $(document).ready(function() {
        if (document.getElementById("statess")){
        console.log("====onload")
        // var rpc = require('web.rpc');

        // rpc.query({

        //     model: 'deltatech_website_city.portal',

        //     method: 'search_session_and_subscription'

        // }).then(function (data) {

        //     console.log(data);

        // });
        // ========for by default=========Distict=====load======
        var st = 	document.getElementById("statess").value
        var dt = document.getElementById("address_district").value
        ajax.jsonRpc("/states", 'call',{'statess':st}).then(function (result){    
          var selectDist1 = $("select[name='address_district']");
          
          
          var selected1 = selectDist1.data("value");
    
          
            $("select[name='address_district']").parent("div").hide();
            selectDist1.html("");
                  _.each(result.districts, function (z) {
                      var opt = $("<option>")
                      
                      .text(z[1])
                      .attr("value", z[0])
                      
                      .attr("selected", z[0] === selected1);
                       
                      selectDist1.append(opt);
                    
                  });
                  $("select[name='address_district']").parent("div").show();
                  var selDist = document.getElementById("address_district");
                  selDist.value = Number(dt);
                 
 
       
    });

       // ========for by default=========local==bodies=====load======
       var dis = 	document.getElementById("address_district").value
       var lc = document.getElementById("address_local").value
       ajax.jsonRpc("/district", 'call',{'districts':dis}).then(function (result){    
        var selectLoc1 = $("select[name='address_local']");
        
        var selected2 = selectLoc1.data("value");
  
          $("select[name='address_local']").parent("div").hide();
          selectLoc1.html("");
                _.each(result.locals, function (w) {
                    var opt = $("<option>")
                    
                    .text(w[1])
                    .attr("value", w[0])
                    
                    .attr("selected", w[0] === selected2);
                     
                    selectLoc1.append(opt);
                    // console.log(x)
                });
                $("select[name='address_local']").parent("div").show();
                var selLoc = document.getElementById("address_local");
                selLoc.value = Number(lc);
            

     
  });
   
}



    // ============onchange=========state=====
        $('#statess').on('change',function(){
        
        console.log("onchange")
         var statess = 	document.getElementById("statess").value
         
            ajax.jsonRpc("/states", 'call',{'statess':statess}).then(function (result){    
                var selectDist = $("select[name='address_district']");
                var store;
                var selected = selectDist.data("value");
          
                  $("select[name='address_district']").parent("div").hide();
                  selectDist.html("");
                        _.each(result.districts, function (x) {
                            var opt = $("<option>")
                            
                            .text(x[1])
                            .attr("value", x[0])
                            
                            .attr("selected", x[0] === selected);
                             
                            selectDist.append(opt);
                            store = x[0];
                            // console.log(x)
                        });
                        $("select[name='address_district']").parent("div").show();
                        var selDist = document.getElementById("address_district");
                        selDist.value = store
                        var districts = 	document.getElementById("address_district").value
                        console.log(districts)
                        console.log("district")
                        
                          ajax.jsonRpc("/district", 'call',{'districts':districts}).then(function (result){    
                              var selectLoc = $("select[name='address_local']");
                              
                              var selected = selectLoc.data("value");
                        
                                $("select[name='address_local']").parent("div").hide();
                                selectLoc.html("");
                                      _.each(result.locals, function (y) {
                                          var opt = $("<option>")
                                          
                                          .text(y[1])
                                          .attr("value", y[0])
                                          
                                          .attr("selected", y[0] === selected);
                                            
                                          selectLoc.append(opt);
                                          // console.log(x)
                                      });
                                      $("select[name='address_local']").parent("div").show();
                                  
                      
                            
                        });
                    
       
             
          });

          var districts = 	document.getElementById("address_district").value
          console.log(districts)
          console.log("district")
          
             ajax.jsonRpc("/district", 'call',{'districts':districts}).then(function (result){    
                 var selectLoc = $("select[name='address_local']");
                 
                 var selected = selectLoc.data("value");
           
                   $("select[name='address_local']").parent("div").hide();
                   selectLoc.html("");
                         _.each(result.locals, function (y) {
                             var opt = $("<option>")
                             
                             .text(y[1])
                             .attr("value", y[0])
                             
                             .attr("selected", y[0] === selected);
                              
                             selectLoc.append(opt);
                             // console.log(x)
                         });
                         $("select[name='address_local']").parent("div").show();
                     
        
              
           });
            
    
        });



        // ==========onchange=======district==============
        $('#address_district').on('change',function(){
        
        console.log("district change") 
          var districts = 	document.getElementById("address_district").value
          
             ajax.jsonRpc("/district", 'call',{'districts':districts}).then(function (result){    
                 var selectLoc = $("select[name='address_local']");
                 
                 var selected = selectLoc.data("value");
           
                   $("select[name='address_local']").parent("div").hide();
                   selectLoc.html("");
                         _.each(result.locals, function (y) {
                             var opt = $("<option>")
                             
                             .text(y[1])
                             .attr("value", y[0])
                             
                             .attr("selected", y[0] === selected);
                              
                             selectLoc.append(opt);
                             // console.log(x)
                         });
                         $("select[name='address_local']").parent("div").show();
                     
        
              
           });
             
     
         });
      });
    
    
    
    
    });
    
  