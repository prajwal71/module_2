# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class TalentHUnt(models.Model):
    _name = 'talent.hunt'
    # _inherit = ['mail.thread','mail.activity.mixin']

    name = fields.Char()
    address = fields.Char()
    mobile = fields.Char()
    email = fields.Char()
    college_name = fields.Char("College Name ")
    grade = fields.Selection([
        ('11','Grade 11'),('12','Grade 12'),('other','Other')
        ],String='Grade')
    additional_grade = fields.Char('Other Grade')
    faculty = fields.Selection([
        ('management','Management'),('science','Science'),('other','Other')
        ],String='Faculty')
    additional_faculty = fields.Char('Other Faculty')
    category_talent = fields.Selection([
        ('singing','Singing'),('dancing','Dancing'),('other','Other'),
        ('comedy','Comedy'),('tiktok','Tiktok'),('poem','Poem'),('art','Art')
        ],String='Category of Talent')
    additional_category_talent = fields.Char('Other Category of Talent')
    talent_file = fields.Binary()

    
    