from odoo.http import request
from odoo import http, _
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from dateutil.relativedelta import relativedelta
import base64
import logging
import json
_logger = logging.getLogger(__name__)
# import yaml
import werkzeug

class TalentHUntWebsite(http.Controller):
    @http.route('/talent-hunt',  type='http', auth="public", website=True)
    def talentHuntForm(self, **kw):
        return request.render("talent_hunt.talent_hunt_form", {})



    @http.route('/submit/talent/hunt',  type='http', auth="public", website=True)
    def talentHuntFormSubmit(self, **post):
        vals = {}
        vals['name'] = post.get('name')
        vals['address'] = post.get('address')
        vals['mobile'] = post.get('mobile')
        vals['email'] = post.get('email')
        vals['college_name'] = post.get('college_name')
        vals['grade'] = post.get('grade')
        vals['additional_grade'] = post.get('additional_grade')
        vals['faculty'] = post.get('faculty')
        vals['additional_faculty'] = post.get('additional_faculty')
        vals['category_talent'] = post.get('category_talent')
        vals['additional_category_talent'] = post.get('additional_category_talent')
        if post.get('talent_file',False):
            files = post.get('talent_file').stream.read()
            vals['talent_file'] = base64.encodestring(files)
        talent_record = request.env['talent.hunt'].sudo().create(vals)
        return request.render("talent_hunt.sucess_page_talent_hunt", {})

