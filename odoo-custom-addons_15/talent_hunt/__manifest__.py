# -*- coding: utf-8 -*-
{
    'name': "Talent HUnt",

    'summary': """
        """,

    'description': """
    """,

    'author': "Subheta Tech",
    'website': "http://shubetatech.com/",
    'category': 'Customizations',
    'version': '15.0.0.1',
    'depends': ['crm','website','mail'],
     "assets": {
        "web.assets_frontend": ["/talent_hunt/static/src/js/main.js"
       
        ],
    },
    'data': [
       'security/ir.model.access.csv',
        'views/views.xml',
        'views/template.xml'
    ],
}
