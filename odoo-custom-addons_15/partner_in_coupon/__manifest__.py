{
    'name': 'Used By In Couppon',
    'version': '1.0',
    'summary': 'Detail of customer that has used Couppon.',
    'description' : 'Detail of customer that has used Couppon.',
    'category': 'companies',
    'author': 'Nexus',
    'depends': ['coupon','sale_coupon'],
    'data': ['views/view.xml'],
    'installable': True,
    'auto_install': False,

}
