from odoo import api, fields, models


class CouponExt(models.Model):
    _inherit = "coupon.coupon"

    used_partner = fields.Many2one(related='sales_order_id.partner_id',string='Used By')
    used_date = fields.Datetime(related='sales_order_id.create_date',string='Used Date')
    used_email = fields.Char(related='sales_order_id.partner_id.email',string='Used Email')
