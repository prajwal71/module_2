# -*- coding: utf-8 -*-

{
    'name': 'EMI in Website',
    'version': '12.0.1.1.0',
    'summary': """ EMI on Website""",
    'description': 'Buy now is added in the E-commerce for the purpose of quick checkout',
    'category': 'Website',
    'author': 'Nexus Inc',
    'company': 'Nexus Inc',
    'maintainer': 'Nexus Inc',
    'website': "https://www.nexusgurus.com",
    'depends': ['base','website_sale','website','web'],
     'assets': {
        'web.assets_frontend': ['emi_odoo/static/src/js/emi_sale.js']
     },
    'data': ['views/emi.xml',
            #  'views/emi_sale.xml',
             'views/preload_v13.xml',],
    'demo': [],
    'images': [],
    'license': 'LGPL-3',
    'installable': True,
    'auto_install': False,
}
