{
    'name': 'Sale order Unit Price Change',
    'version': '1.0',
    'summary': 'This modules calculate sub total price accurately.',
    'description' : 'This modules calculate sub total price accurately.',
    'category': 'companies',
    'author': 'Nexus',
    'depends': ['sale'],
    'data': [''],
    'installable': True,
    'auto_install': False,

}
