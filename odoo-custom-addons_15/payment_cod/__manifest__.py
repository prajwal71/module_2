# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'COD Payment Acquirer',
    'version': '2.0',
    'category': 'Hidden',
    'description': """
This module is for CASH ON DELIVERY
""",
    'depends': ['payment'],
    'data': [
        'views/payment_templates.xml',
        'views/payment_cod_templates.xml',
        'data/payment_acquirer_data.xml',
    ],
    'uninstall_hook': 'uninstall_hook',
    'assets': {
        'web.assets_frontend': [
            'payment_cod/static/src/js/**/*',
        ],
    },
    'license': 'LGPL-3',
}
