# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class ProductTemplate(models.Model):
    _inherit = "product.template"

    is_equipment = fields.Boolean(string="Is equipment?")
    default_equipment_category_id = fields.Many2one('maintenance.equipment.category', string="Default Equipment Category")
    equipment_assign_to	 = fields.Selection( [('department', 'Department'), ('employee', 'Employee'), ('other', 'Other')],
        string='Used By', default='employee')
    maintenance_team_id = fields.Many2one('maintenance.team', string="Maintenance Team")
    technician_user_id =fields.Many2one('res.users', string="Technician")
    model = fields.Char(string="Model")
    equipment_ids = fields.One2many('maintenance.equipment', 'product_tmpl_id', help="The Equipments of this product")
    equipment_count = fields.Integer(string='Equipments', compute='_compute_equipment_count')

    @api.depends("equipment_ids")
    def _compute_equipment_count(self):
        """
        Calculate the number of equipment linked for each product
        :return:
        """
        for product in self:
            product.equipment_count = len(product.equipment_ids)


    def action_view_equipment(self):
        '''
        This function returns an action that display equipments linked with this product,
         It can either be a in a list or in a form view, if there is only one equipment to show
        '''
        action = self.env.ref('maintenance.hr_equipment_action').read()[0]
        equipments = self.mapped('equipment_ids')
        if len(equipments) > 1:
            action['domain'] = [('id', 'in', equipments.ids)]
        elif equipments:
            action['views'] = [(self.env.ref('maintenance.hr_equipment_view_form').id, 'form')]
            action['res_id'] = equipments.id
        return action


    @api.onchange('is_equipment')
    def _onchange_is_equipment(self):
        """
        Set product tracking to serial if the Is equipment is changed and equal True, if it changed to False
        then set tracking to none
        :return:
        """
        if self.is_equipment:
            self.tracking = 'serial'
        else:
            self.tracking = 'none'

    @api.onchange('default_equipment_category_id')
    def _onchange_default_equipment_category_id(self):
        """
        Set technician to the equipment category responsible
        :return:
        """
        if self.default_equipment_category_id.technician_user_id:
            self.technician_user_id = self.default_equipment_category_id.technician_user_id.id



class ProductProduct(models.Model):
    _inherit = "product.product"

    @api.depends("equipment_ids")
    def _compute_equipment_count(self):
        """
        Calculate the number of equipment linked for each product
        :return:
        """
        for product in self:
            product.equipment_count = len(product.equipment_ids)


    def action_view_equipment(self):
        '''
        This function returns an action that display equipments linked with this product,
         It can either be a in a list or in a form view, if there is only one equipment to show
        '''
        action = self.env.ref('maintenance.hr_equipment_action').read()[0]
        equipments = self.mapped('equipment_ids')
        if len(equipments) > 1:
            action['domain'] = [('id', 'in', equipments.ids)]
        elif equipments:
            action['views'] = [(self.env.ref('maintenance.hr_equipment_view_form').id, 'form')]
            action['res_id'] = equipments.id
        return action


    @api.onchange('is_equipment')
    def _onchange_is_equipment(self):
        """
        Set product tracking to serial if the Is equipment is changed and equal True, if it changed to False
        then set tracking to none
        :return:
        """
        if self.is_equipment:
            self.tracking = 'serial'
        else:
            self.tracking = 'none'

    @api.onchange('default_equipment_category_id')
    def _onchange_default_equipment_category_id(self):
        """
        Set technician to the equipment category responsible
        :return:
        """
        if self.default_equipment_category_id.technician_user_id:
            self.technician_user_id = self.default_equipment_category_id.technician_user_id.id