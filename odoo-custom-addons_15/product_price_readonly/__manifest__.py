
{
    'name': 'Product Price Readonly',
    'version': '13.0.0.0',
    'author': 'Nexus',
    'summary': 'Product Price Readonly',
    'description': """Product price readonly for a specific user in Sales and Invoice.""",
    'category': 'Base',
    'website': 'https://www.iwesabe.com/',
    'license': 'AGPL-3',
    'depends': ['product'],
    'data': [
        'data/ir_module_category_data.xml',
        'security/security.xml',
        'views/product_template.xml',
    ],
    'qweb': [],
    'images': ['static/description/iWesabe-Apps-Product-Unit-Price-Readonly.png'],
    'installable': True,
    'application': True,
    'auto_install': False,
}
