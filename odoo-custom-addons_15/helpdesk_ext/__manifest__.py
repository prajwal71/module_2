# -*- coding: utf-8 -*-
{
    'name': "Helpdesk Ext",

    'summary': """
        This module is developed to update the bom cost price in product variant cost price""",

    'description': """
        The calculated BOM cost price is added to the product variant cost price
    """,

    'author': "Niraj Karna",
    'website': "http://www.sagar.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['helpdesk','helpdesk_stock'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/sequence.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/report.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}