from datetime import timedelta

from odoo import api, fields, models, _
from odoo.addons.base.models.res_partner import WARNING_MESSAGE, WARNING_HELP
from odoo.tools.float_utils import float_round
import logging 
_logger = logging.getLogger(__name__)

class BatteryType(models.Model):
    _name = 'battery.type'

    name = fields.Char('Battery Type')


class BatteryType(models.Model):
    _name = 'vehicle.type'

    name = fields.Char('Vehicle Type')

class JobTicketLine(models.Model):
    _name = 'job.ticket.line'

    particular = fields.Selection([('bc', 'Before Charging'), ('ac', 'After Charging'), ('idle', 'After 24 Hs idle')],default='bc')
    description = fields.Char("Description", default="Gravity")
    voltage = fields.Char("Voltage")
    load = fields.Char("Load Test")
    cell1 = fields.Char("Cell 1")
    cell2 = fields.Char("Cell 2")
    cell3 = fields.Char("Cell 3")
    cell4 = fields.Char("Cell 4")
    cell5 = fields.Char("Cell 5")
    cell6 = fields.Char("Cell 6")
    job_ticket_id = fields.Many2one('helpdesk.ticket')

class HelpdeskTicketPrint(models.Model):
   
    _inherit = 'helpdesk.ticket'

    def print_ticket(self):
        return self.env.ref('helpdesk_ext.report_helpdesk_ticket_print').report_action(self)

    seq_name = fields.Char(string="Job Card ID", readonly=True, required=True, copy=False, default='New') 
    is_job_card = fields.Boolean("Is Job Card")
    dealer_id = fields.Many2one("res.partner",string="Dealers/Retailers Name")
    battery_sn = fields.Char("Battery SN")
    srf_no = fields.Char("S.R.F. No")
    battery_type_id = fields.Many2one("battery.type",string="Battery Type")
    battey_remarks = fields.Html("Remarks")
    
    b_out_date = fields.Date("Out Date")
    b_in_date = fields.Date("In Date")
    b_ok = fields.Char("Battey OK")

    vehicle_no = fields.Char("Vehicle No/UPS Voltage")
    vehicle_type_id = fields.Many2one("vehicle.type",string="Vehicle Type")


    free_replacement =  fields.Char("Free Replacement")
    reject_remarks = fields.Html("Rejected Reason")

    old_b_sn = fields.Char("Old Battery S.N.")
    new_b_sn = fields.Char("New Battery S.N.")
    dealer_sales_date = fields.Date("Dealer Sales Date")
    job_card_date = fields.Date("Date")
    company_sales_date = fields.Date("Company Sales Date")

    sb_no = fields.Char("S.B. No.")
    sr_no = fields.Char("S.R. No.")

    bv_type = fields.Selection(
        [('4wheel', '4 Wheeler'), ('2wheel', '2 Wheeler'), ('ericksha', 'Ericksha'),
         ('inverter', 'Inverter'), ('solar', 'Solar')])

    job_ticket_line = fields.One2many('job.ticket.line', 'job_ticket_id')


    @api.model
    def create(self, vals):
        if vals.get('is_job_card'):
            if vals.get('seq_name', 'New') == 'New':
                vals['seq_name'] = self.env['ir.sequence'].next_by_code(
                    'job.card.sequence') or 'New'
        result = super(HelpdeskTicketPrint, self).create(vals)
        return result


