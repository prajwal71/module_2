# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Create Different Bill and INvoice for same Partner',
    'version' : '1.1',
    'summary': 'To create different bill for same partner',
    'description': """
.
    """,
    'author': 'Nexus Incoporation',
    'depends' : ['sale'],
    'data': [
          'security/res_groups.xml',
        'security/ir.model.access.csv',
        'wizard/view.xml',
        'views/view.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
