# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
from datetime import datetime, timedelta
from itertools import groupby
import json
import logging
_logger = logging.getLogger(__name__)
from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import AccessError, UserError, ValidationError
from odoo.osv import expression
from odoo.tools import float_is_zero, html_keep_url, is_html_empty


class SaleOrderCustomEXt(models.Model):
    _inherit = "sale.order"
    
 
    migrat = fields.Boolean(
        string='Migration',default=False
    )
    
    
    def _create_invoices(self, grouped=False, final=False, date=None):
        """
        Create the invoice associated to the SO.
        :param grouped: if True, invoices are grouped by SO id. If False, invoices are grouped by
                        (partner_invoice_id, currency)
        :param final: if True, refunds will be generated if necessary
        :returns: list of created invoices
        """
        # _logger.info("========1234======ENTRYYYYYYY=======")
        # res = super(SaleOrderCustomEXt,self)._create_invoices(grouped=False, final=False, date=None)
        # for rec in self:
        #     check = rec.migrat
        #     if check==False:
        #         _logger.info("========1234======new=======")
        #         return res
            
        
        
        _logger.info("========%s======new=======",grouped)
        if not self.env['account.move'].check_access_rights('create', False):
            try:
                self.check_access_rights('write')
                self.check_access_rule('write')
            except AccessError:
                return self.env['account.move']

        # 1) Create invoices.
        invoice_vals_list = []
        invoice_item_sequence = 0 # Incremental sequencing to keep the lines order on the invoice.
        for order in self:
            order = order.with_company(order.company_id)
            current_section_vals = None
            down_payments = order.env['sale.order.line']

            invoice_vals = order._prepare_invoice()
            invoiceable_lines = order._get_invoiceable_lines(final)

            if not any(not line.display_type for line in invoiceable_lines):
                continue

            invoice_line_vals = []
            down_payment_section_added = False
            for line in invoiceable_lines:
                if not down_payment_section_added and line.is_downpayment:
                    # Create a dedicated section for the down payments
                    # (put at the end of the invoiceable_lines)
                    invoice_line_vals.append(
                        (0, 0, order._prepare_down_payment_section_line(
                            sequence=invoice_item_sequence,
                        )),
                    )
                    down_payment_section_added = True
                    invoice_item_sequence += 1
                invoice_line_vals.append(
                    (0, 0, line._prepare_invoice_line(
                        sequence=invoice_item_sequence,
                    )),
                )
                invoice_item_sequence += 1

            invoice_vals['invoice_line_ids'] += invoice_line_vals
            invoice_vals_list.append(invoice_vals)

        if not invoice_vals_list:
            raise self._nothing_to_invoice_error()

        # 2) Manage 'grouped' parameter: group by (partner_id, currency_id).
        for rec in self:
            _logger.info("========bool====bool====%s=======",rec.migrat)
            if rec.migrat==False:
            
                if not grouped:
                    new_invoice_vals_list = []
                    invoice_grouping_keys = self._get_invoice_grouping_keys()
                    invoice_vals_list = sorted(
                        invoice_vals_list,
                        key=lambda x: [
                            x.get(grouping_key) for grouping_key in invoice_grouping_keys
                        ]
                    )
                    for grouping_keys, invoices in groupby(invoice_vals_list, key=lambda x: [x.get(grouping_key) for grouping_key in invoice_grouping_keys]):
                        origins = set()
                        payment_refs = set()
                        refs = set()
                        ref_invoice_vals = None
                        for invoice_vals in invoices:
                            if not ref_invoice_vals:
                                ref_invoice_vals = invoice_vals
                            else:
                                ref_invoice_vals['invoice_line_ids'] += invoice_vals['invoice_line_ids']
                            origins.add(invoice_vals['invoice_origin'])
                            payment_refs.add(invoice_vals['payment_reference'])
                            refs.add(invoice_vals['ref'])
                        ref_invoice_vals.update({
                            'ref': ', '.join(refs)[:2000],
                            'invoice_origin': ', '.join(origins),
                            'payment_reference': len(payment_refs) == 1 and payment_refs.pop() or False,
                        })
                        new_invoice_vals_list.append(ref_invoice_vals)
                    invoice_vals_list = new_invoice_vals_list

            # 3) Create invoices.

            # As part of the invoice creation, we make sure the sequence of multiple SO do not interfere
            # in a single invoice. Example:
            # SO 1:
            # - Section A (sequence: 10)
            # - Product A (sequence: 11)
            # SO 2:
            # - Section B (sequence: 10)
            # - Product B (sequence: 11)
            #
            # If SO 1 & 2 are grouped in the same invoice, the result will be:
            # - Section A (sequence: 10)
            # - Section B (sequence: 10)
            # - Product A (sequence: 11)
            # - Product B (sequence: 11)
            #
            # Resequencing should be safe, however we resequence only if there are less invoices than
            # orders, meaning a grouping might have been done. This could also mean that only a part
            # of the selected SO are invoiceable, but resequencing in this case shouldn't be an issue.
        for rec in self:
            rec.migrat=False
            
        if len(invoice_vals_list) < len(self):
            SaleOrderLine = self.env['sale.order.line']
            for invoice in invoice_vals_list:
                sequence = 1
                for line in invoice['invoice_line_ids']:
                    line[2]['sequence'] = SaleOrderLine._get_invoice_line_sequence(new=sequence, old=line[2]['sequence'])
                    sequence += 1

        # Manage the creation of invoices in sudo because a salesperson must be able to generate an invoice from a
        # sale order without "billing" access rights. However, he should not be able to create an invoice from scratch.
        moves = self.env['account.move'].sudo().with_context(default_move_type='out_invoice').create(invoice_vals_list)

        # 4) Some moves might actually be refunds: convert them if the total amount is negative
        # We do this after the moves have been created since we need taxes, etc. to know if the total
        # is actually negative or not
        if final:
            moves.sudo().filtered(lambda m: m.amount_total < 0).action_switch_invoice_into_refund_credit_note()
        for move in moves:
            move.message_post_with_view('mail.message_origin_link',
                values={'self': move, 'origin': move.line_ids.mapped('sale_line_ids.order_id')},
                subtype_id=self.env.ref('mail.mt_note').id
            )
            
        
        return moves



class PurchaseOrderCustomEXt(models.Model):
    _inherit = "purchase.order"
    


    def action_create_invoice_seperate(self):
        """Create the invoice associated to the PO.
        """
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')

        # 1) Prepare invoice vals and clean-up the section lines
        invoice_vals_list = []
        for order in self:
            if order.invoice_status != 'to invoice':
                continue

            order = order.with_company(order.company_id)
            pending_section = None
            # Invoice values.
            invoice_vals = order._prepare_invoice()
            # Invoice line values (keep only necessary sections).
            for line in order.order_line:
                if line.display_type == 'line_section':
                    pending_section = line
                    continue
                if not float_is_zero(line.qty_to_invoice, precision_digits=precision):
                    if pending_section:
                        invoice_vals['invoice_line_ids'].append((0, 0, pending_section._prepare_account_move_line()))
                        pending_section = None
                    invoice_vals['invoice_line_ids'].append((0, 0, line._prepare_account_move_line()))
            invoice_vals_list.append(invoice_vals)

        if not invoice_vals_list:
            raise UserError(_('There is no invoiceable line. If a product has a control policy based on received quantity, please make sure that a quantity has been received.'))

        # 2) group by (company_id, partner_id, currency_id) for batch creation
        # new_invoice_vals_list = []
        # for grouping_keys, invoices in groupby(invoice_vals_list, key=lambda x: (x.get('company_id'), x.get('partner_id'), x.get('currency_id'))):
        #     origins = set()
        #     payment_refs = set()
        #     refs = set()
        #     ref_invoice_vals = None
        #     for invoice_vals in invoices:
        #         if not ref_invoice_vals:
        #             ref_invoice_vals = invoice_vals
        #         else:
        #             ref_invoice_vals['invoice_line_ids'] += invoice_vals['invoice_line_ids']
        #         origins.add(invoice_vals['invoice_origin'])
        #         payment_refs.add(invoice_vals['payment_reference'])
        #         refs.add(invoice_vals['ref'])
        #     ref_invoice_vals.update({
        #         'ref': ', '.join(refs)[:2000],
        #         'invoice_origin': ', '.join(origins),
        #         'payment_reference': len(payment_refs) == 1 and payment_refs.pop() or False,
        #     })
        #     new_invoice_vals_list.append(ref_invoice_vals)
        # invoice_vals_list = new_invoice_vals_list

        # 3) Create invoices.
        moves = self.env['account.move']
        AccountMove = self.env['account.move'].with_context(default_move_type='in_invoice')
        for vals in invoice_vals_list:
            moves = AccountMove.with_company(vals['company_id']).create(vals)

        # 4) Some moves might actually be refunds: convert them if the total amount is negative
        # We do this after the moves have been created since we need taxes, etc. to know if the total
        # is actually negative or not
        moves.filtered(lambda m: m.currency_id.round(m.amount_total) < 0).action_switch_invoice_into_refund_credit_note()

        return self.action_view_invoice(moves)
