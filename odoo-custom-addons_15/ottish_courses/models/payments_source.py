from odoo import api, fields, models


class PaymentSourceLine(models.Model):
    _name = "payment.source.line"
    _description = "Payment details"


    product_course_id = fields.Many2one('product.product',string="Course")
    name = fields.Many2one("customer.payment.source",string="Payment Source")
    amount = fields.Float("Course Price")
    amount_total = fields.Float("Course Price After Discount")
    discounted_amount = fields.Float(string="Discount (in Amount)")
    transaction_id = fields.Char("Transaction ID")
    crm_lead = fields.Many2one("crm.lead")


class PaymentSourceLine(models.Model):
    _name = "customer.payment.source"
    _description = "Payment details"


    name = fields.Char(string="Payment Source")
