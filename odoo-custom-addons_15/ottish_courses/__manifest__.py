# -*- encoding: utf-8 -*-
{
    'name': 'Ottish Courses',
    'version': '15.0.0.0.0',
    'category': 'sales',

    'author': '10 Orbits pvt. ltd.',
    'license': 'AGPL-3',
    'depends': ['sale_management','crm','hr','product'],
    'data': [
        'security/ir.model.access.csv',
        'security/rec_filter_crm.xml',
        'views/product_template.xml',
        'views/crm_lead.xml',
        'views/hr_employee.xml',
        'views/payment_source.xml'
    ],
    'installable': True,
    'sequence': -10,
    'application': False,
}

