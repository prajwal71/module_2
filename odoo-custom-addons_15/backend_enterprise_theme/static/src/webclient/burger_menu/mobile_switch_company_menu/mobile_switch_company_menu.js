/** @odoo-module **/
import { SwitchCompanyMenu } from "@web/webclient/switch_company_menu/switch_company_menu";

export class MobileSwitchCompanyMenu extends SwitchCompanyMenu {}
MobileSwitchCompanyMenu.template = "backend_enterprise_theme.MobileSwitchCompanyMenu";
