# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Web Enterprise',
    'category': 'Hidden',
    'version': '1.0',
    'description': """
Odoo Enterprise Web Client.
===========================

This module modifies the web addon to provide Enterprise design and responsiveness.
        """,
    'depends': ['web'],
    'auto_install': True,
    'data': [
        'views/partner_view.xml',
        'views/webclient_templates.xml',
    ],
    'assets': {
        'web.assets_qweb': [
            'backend_enterprise_theme/static/src/**/*.xml',
        ],
        'web._assets_primary_variables': [
            ('prepend', 'backend_enterprise_theme/static/src/legacy/scss/primary_variables.scss'),
        ],
        'web._assets_secondary_variables': [
            'backend_enterprise_theme/static/src/legacy/scss/secondary_variables.scss',
        ],
        'web._assets_backend_helpers': [
            'backend_enterprise_theme/static/src/legacy/scss/bootstrap_overridden.scss',
        ],
        'web._assets_common_styles': [
            ('replace', 'web/static/src/legacy/scss/ui_extra.scss', 'backend_enterprise_theme/static/src/legacy/scss/ui.scss'),

            'backend_enterprise_theme/static/fonts/fonts.scss',
        ],
        'web.assets_backend': [
            ('replace', 'web/static/src/webclient/webclient_extra.scss', 'backend_enterprise_theme/static/src/webclient/webclient.scss'),
            ('replace', 'web/static/src/webclient/webclient_layout.scss', 'backend_enterprise_theme/static/src/webclient/webclient_layout.scss'),

            ('replace', 'web/static/src/legacy/scss/dropdown_extra.scss', 'backend_enterprise_theme/static/src/legacy/scss/fields.scss'),
            ('replace', 'web/static/src/legacy/scss/fields_extra.scss', 'backend_enterprise_theme/static/src/legacy/scss/form_view.scss'),
            ('replace', 'web/static/src/legacy/scss/form_view_extra.scss', 'backend_enterprise_theme/static/src/legacy/scss/list_view.scss'),
            ('replace', 'web/static/src/legacy/scss/list_view_extra.scss', 'backend_enterprise_theme/static/src/legacy/scss/search_view.scss'),
            ('replace', 'web/static/src/search/search_panel/search_view_extra.scss', 'backend_enterprise_theme/static/src/legacy/scss/dropdown.scss'),

            'backend_enterprise_theme/static/src/legacy/scss/base_settings_mobile.scss',
            'backend_enterprise_theme/static/src/legacy/scss/search_panel_mobile.scss',
            'backend_enterprise_theme/static/src/legacy/scss/menu_search.scss',
            'backend_enterprise_theme/static/src/legacy/scss/control_panel_layout.scss',
            'backend_enterprise_theme/static/src/legacy/scss/control_panel_mobile.scss',
            'backend_enterprise_theme/static/src/legacy/scss/kanban_view.scss',
            'backend_enterprise_theme/static/src/legacy/scss/touch_device.scss',
            'backend_enterprise_theme/static/src/legacy/scss/snackbar.scss',
            'backend_enterprise_theme/static/src/legacy/scss/swipe_item_mixin.scss',
            'backend_enterprise_theme/static/src/legacy/scss/form_view_mobile.scss',
            'backend_enterprise_theme/static/src/legacy/scss/kanban_view_mobile.scss',
            'backend_enterprise_theme/static/src/legacy/scss/modal_mobile.scss',
            'backend_enterprise_theme/static/src/legacy/scss/promote_studio.scss',
            'backend_enterprise_theme/static/src/legacy/scss/web_calendar_mobile.scss',
            'backend_enterprise_theme/static/src/legacy/scss/barcodes_mobile.scss',
            'backend_enterprise_theme/static/src/legacy/scss/pivot_view_mobile.scss',
            'backend_enterprise_theme/static/src/search/**/*.scss',
            'backend_enterprise_theme/static/src/webclient/**/*.scss',
            'backend_enterprise_theme/static/src/views/**/*.scss',

            ('replace', 'web/static/src/legacy/js/fields/upgrade_fields.js', 'backend_enterprise_theme/static/src/legacy/js/apps.js'),

            'backend_enterprise_theme/static/src/search/**/*.js',
            'backend_enterprise_theme/static/src/webclient/**/*.js',
            'backend_enterprise_theme/static/src/views/**/*.js',

            'backend_enterprise_theme/static/src/legacy/**/*.js',
            ("remove", "backend_enterprise_theme/static/src/legacy/js/views/pivot_renderer.js"),
        ],
        "web.assets_backend_legacy_lazy": [
            "backend_enterprise_theme/static/src/legacy/js/views/pivot_renderer.js",
        ],
        'web.assets_backend_prod_only': [
            ('replace', 'web/static/src/main.js', 'backend_enterprise_theme/static/src/main.js'),
        ],
        'web.tests_assets': [
            'backend_enterprise_theme/static/tests/*.js',
        ],
        'web.qunit_suite_tests': [
            ('remove', 'web/static/tests/legacy/fields/upgrade_fields_tests.js'),

            'backend_enterprise_theme/static/tests/webclient/**/*.js',

            'backend_enterprise_theme/static/tests/legacy/upgrade_fields_tests.js',
            'backend_enterprise_theme/static/tests/legacy/views/list_tests.js',
            'backend_enterprise_theme/static/tests/legacy/barcodes_tests.js',
        ],
        'web.qunit_mobile_suite_tests': [
            'backend_enterprise_theme/static/tests/mobile/**/*.js',

            'backend_enterprise_theme/static/tests/legacy/action_manager_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/control_panel_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/form_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/relational_fields_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/views/basic/basic_render_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/views/calendar_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/views/kanban_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/views/list_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/base_settings_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/components/action_menus_mobile_tests.js',
            'backend_enterprise_theme/static/tests/legacy/barcodes_mobile_tests.js',
        ],
    },
    # 'license': 'OEEL-1',
}
