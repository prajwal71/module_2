# -*- coding: utf-8 -*-
{
    'name': "Nexus Calendar Minutes of Meeting",

    'summary': """
        Nexus Calendar MOM .It helps to keep the agenda of meeting in calender , minutes of meeting """,


    'author': "Nexus Incorporation",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','calendar'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
    ],
   
}
