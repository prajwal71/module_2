import json
import logging
from werkzeug.exceptions import Forbidden, NotFound
from werkzeug.urls import url_decode, url_encode, url_parse

from odoo import fields, http, SUPERUSER_ID, tools, _
from odoo.fields import Command
from odoo.http import request
from odoo.addons.base.models.ir_qweb_fields import nl2br
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.payment.controllers import portal as payment_portal
from odoo.addons.payment.controllers.post_processing import PaymentPostProcessing
from odoo.addons.website.controllers.main import QueryURL
from odoo.addons.website.models.ir_http import sitemap_qs2dom
from odoo.exceptions import AccessError, MissingError, ValidationError
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.osv import expression
from odoo.tools.json import scriptsafe as json_scriptsafe
_logger = logging.getLogger(__name__)

class WebsiteSaleCustomExt(WebsiteSale):


    @http.route(['/shop/confirmation'], type='http', auth="public", website=True, sitemap=False)
    def shop_payment_confirmation(self, **post):
        res = super(WebsiteSaleCustomExt,self).shop_payment_confirmation(**post)
        k = [request.env.company.sparrow_sms_partner.id]
        sale_order_id_new = request.session.get('sale_last_order_id')
        _logger.info("=============partner=======%s==",k)
        if sale_order_id_new:
            order_new = request.env['sale.order'].sudo().browse(sale_order_id_new)
            SMSobj = request.env['sms.automation'].sudo().create({
                    'recepient_ids' :k,
                    'subject': 'Website Sale Order Has Been Confirmed',
                    'message': "Sale order: "+order_new.name +" has been confirmed. Please follow up",
                    'source_sms': 'Sale Order Has Been Confirmed',
                    'type_sms': 'sale_sms',
                    })
            try:
                SMSobj.action_send_sms()
            except:
                pass

        _logger.info("=============partner=======%s==",k)
        return res