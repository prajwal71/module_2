# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
from odoo import fields, models,api,_
import requests
import re
import json
from odoo.exceptions import ValidationError
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

class SmsAutomation(models.Model):
    _name = 'sms.automation'
    _description = 'Sms Automation'
    _inherit = 'mail.thread'
    _rec_name = 'subject'

    recepient_ids = fields.Many2many('res.partner')
    subject = fields.Char('Subject')
    message = fields.Text('Message')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'Sent'),('cancel', 'Cancel')], default="draft")
    recepient_count = fields.Integer(string="Recepient Count",compute='_compute_count',readonly=True,store=True)
    source_sms = fields.Char('Origin',readonly=True)
    type_sms = fields.Selection([
        ('mass_sms', 'Mass Sms'),
        ('sale_sms', 'Sale Sms'),('invoice_sms','Invoice SMS'),('delivery_sms', 'Delivery Sms'),('helpdesk_sms', 'Helpdesk Sms'),('others', 'Others')],string="Sms Type", default="mass_sms",readonly=True)
    response = fields.Char('Response',track_visibility = 'always')

    @api.depends('recepient_ids')
    def _compute_count (self):
        for record in self:
            record.recepient_count = len(record.recepient_ids)

    def action_send_sms(self):
        sparrow_sms_active = self.env.company.sparrow_sms_active
        sparrow_sms_api_key = self.env.company.sparrow_sms_api_key
        sparrow_sms_from  = self.env.company.sparrow_sms_from

        _logger.info("################Sparrow SMS#############")
        _logger.info(sparrow_sms_active)
        _logger.info("################Sparrow SMS#############")
        _logger.info(sparrow_sms_api_key)
        _logger.info("################Sparrow SMS#############")
        _logger.info(sparrow_sms_from)


        if sparrow_sms_active and sparrow_sms_api_key and sparrow_sms_from:
            k = []
            for partner in self.recepient_ids:
                _logger.info("################Numbers#############")
                _logger.info(partner.mobile)
                s = partner.mobile
                s = str(s)
                s = s.replace(' ','')
                s = s.replace('-','')
                mobile = s[-10:]
                # _logger.info("################mobile#############")
                # _logger.info(mobile)
                ntc = re.compile("[9][8][46][0-9]{7}")
                ncell = re.compile("[9][8][0-2][0-9]{7}")
                ntc_pst = re.compile("[9][8][5][0-9]{7}")
                smartcell = re.compile("[9][6][0-9]{8}|[9][8][8][0-9]{7}")
                if ncell.match(mobile):
                    k.append(mobile)
                elif ntc.match(mobile):
                    k.append(mobile)
                elif ntc_pst.match(mobile):
                    k.append(mobile)
                elif smartcell.match(mobile):
                    k.append(mobile)
                else:
                     _logger.info("################INvalid Numbers#############")
                     _logger.info(mobile)
            _logger.info("################full List#############")
            _logger.info(k)
            def divide_chunks(l, n):
                for i in range(0, len(l), n): 
                    yield l[i:i + n]
            k_sp = list(divide_chunks(k,250))
            _logger.info("################List#############")
            _logger.info(k_sp)
            
            for i in range(0, len(k_sp)):
                numbers = ""
                numbers = ','.join(k_sp[i])
                _logger.info("################Numbers#############")
                _logger.info(k_sp[i])
                _logger.info("################Total Numbers#############")
                _logger.info(len(k_sp))
             
                r = requests.get(
                    "http://api.sparrowsms.com/v2/sms/",
                    params={
                            'token' : sparrow_sms_api_key,
                            'from'  : sparrow_sms_from,
                            'to'    : numbers,
                            'text'  : self.message
                        })
                status_code = r.status_code
                _logger.info('#######status_code######')
                _logger.info(status_code)
                response = r.text
                self.response = r.text
                _logger.info(response)
                response_json = r.json()
                _logger.info(response_json)

                json_data = json.loads(response)
                _logger.info('#######Json Data#########',json_data)
                if json_data['response_code'] == 200:
                   
                    _logger.info("Sucessfully Sent Sparrow Sms")
                    self.state = 'sent'
                   
                else:
                    raise UserError(_("No More Credit Message Was not Delivered Or Check Phone number"))
            return True
        else:
            raise UserError(_("Please activate SMS and enter API KEY from General Settings."))

    def action_cancel(self):
        self.state = 'cancel'

# class SaleOrderInherited(models.Model):
#     _inherit = 'sale.order'


    # def action_confirm(self):
    #     if self._get_forbidden_state_confirm() & set(self.mapped('state')):
    #         raise UserError(_(
    #             'It is not allowed to confirm an order in the following states: %s'
    #         ) % (', '.join(self._get_forbidden_state_confirm())))

    #     for order in self.filtered(lambda order: order.partner_id not in order.message_partner_ids):
    #         order.message_subscribe([order.partner_id.id])
    #     self.write({
    #         'state': 'sale',
    #         'date_order': fields.Datetime.now()
    #     })

    #     # Context key 'default_name' is sometimes propagated up to here.
    #     # We don't need it and it creates issues in the creation of linked records.
    #     context = self._context.copy()
    #     context.pop('default_name', None)

    #     self.with_context(context)._action_confirm()
    #     if self.env.user.has_group('sale.group_auto_done_setting'):
    #         self.action_done()

    #     k =[self.partner_id.id]
    #     SMSobj =self.env['sms.automation'].create({
    #         'recepient_ids' :k,
    #         'subject': "Sales Order",
    #         'message': "Your Sales order with Ref " + self.name + " has been placed sucessfully and will be soon Dispatched." ,
    #         'source_sms': self.name,
    #         'type_sms': 'sale_sms'
    #     })
    #     SMSobj.action_send_sms()
    #     return True

