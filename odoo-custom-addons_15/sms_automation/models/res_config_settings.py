# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ResCompanyInherited(models.Model):
    _inherit = "res.company"

    sparrow_sms_active  = fields.Boolean(string='Activate Sparrow sms api Key', help="Activate Sparrow SMS API Key")
    sparrow_sms_api_key  = fields.Char(string='Sparrow sms api Key', help="Sparrow SMS API Key")
    sparrow_sms_from  = fields.Char(string='Sparrow sms Sender Name', help="Sparrow SMS Sender Name")
    # sparrow_sms_no = fields.Char(string="Phone Number")
    sparrow_sms_partner = fields.Many2one('res.partner',string="SMS Receiving Partner")
class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    sparrow_sms_active  = fields.Boolean(string='Activate Sparrow sms api Key',related='company_id.sparrow_sms_active', help="Activate Sparrow SMS API Key",readonly=False)
    sparrow_sms_api_key  = fields.Char(string='Sparrow sms api Key',related='company_id.sparrow_sms_api_key',help="Sparrow SMS API Key",readonly=False)
    sparrow_sms_from  = fields.Char(string='Sparrow sms Sender Name',related='company_id.sparrow_sms_from', help="Sparrow SMS Sender Name",readonly=False)
    sparrow_sms_partner = fields.Many2one(related='company_id.sparrow_sms_partner',string="SMS Receiving Partner",readonly=False)

    

    # @api.model
    # def get_values(self, fields=None):
	#     res = super(ResConfigSettings, self).get_values()
    #     IrDefault = self.env['ir.default'].sudo()
       
	
	