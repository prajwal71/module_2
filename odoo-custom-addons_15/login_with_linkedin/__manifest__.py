
{
    'name': 'Login With Linkedin',
    'summary': """User can login by signin with linkedin""",
    'version': '15.0.1.0.0',
    'description': """User can login by signin with linkedin""",
    'author': 'Nexus',
    'company': 'Nexus',
    'website': 'https://www.nexusgurus.com',
    'category': 'Tools',
    'depends': ['base', 'auth_oauth'],
    'license': 'AGPL-3',
    'data': [
       
         'views/oauth_auth.xml',
    ],
    'images': ['static/description/icon.png'],
    'installable': True,
    'auto_install': False,
}

