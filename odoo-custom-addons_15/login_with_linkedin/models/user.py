import json
import pprint
import base64
import logging
_logger = logging.getLogger(__name__)
import requests
import werkzeug.http
from odoo.http import request
from odoo import registry as registry_get
from odoo import api, fields, models,SUPERUSER_ID
from odoo.exceptions import AccessDenied, UserError
from odoo.addons.auth_signup.models.res_users import SignupError

from odoo.addons import base
base.models.res_users.USER_PRIVATE_FIELDS.append('oauth_access_token')

class AuthOAuthProviderExt(models.Model):
    _inherit = 'auth.oauth.provider'
    
    secret_key = fields.Char(string='Secret Key')


class ResUsersInherit(models.Model):
    _inherit = 'res.users'

    def get_profile(self,access_token):
        _logger.info("=========last==")
        URL = "https://api.linkedin.com/v2/me"
        # URL = "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))"
        
        headers = {'Content-Type': 'application/x-www-form-urlencoded','Authorization':'Bearer {}'.format(access_token),'X-Restli-Protocol-Version':'2.0.0'}
        response = requests.get(url=URL, headers=headers)
        _logger.info("====dats===%s",response.json())
        print(response.json())
        return response.json()
    
    def get_email(self,access_token):
        _logger.info("=========last==")
        URL = "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))"
        
        headers = {'Content-Type': 'application/x-www-form-urlencoded','Authorization':'Bearer {}'.format(access_token),'X-Restli-Protocol-Version':'2.0.0'}
        response = requests.get(url=URL, headers=headers)
        _logger.info("====dats===%s",response.json())
        print(response.json())
        return response.json()

    def get_pic(self,access_token):
        _logger.info("=========last==")
        URL = "https://api.linkedin.com/v2/me?projection=(id,profilePicture(displayImage~digitalmediaAsset:playableStreams))"
        headers = {'Content-Type': 'application/x-www-form-urlencoded','Authorization':'Bearer {}'.format(access_token),'X-Restli-Protocol-Version':'2.0.0'}
        response = requests.get(url=URL, headers=headers)
        data = response.json()
        
        profile_pic = data.get('profilePicture')
        if profile_pic:
            _logger.info("=================poriflce  check     %s",profile_pic)
            _logger.info(
                "post-processing values for acquirer with id :===========%s",
                pprint.pformat(profile_pic)
            ) 
            img = profile_pic.get('displayImage~')
            _logger.info(
                "post-processing values for acquirer with id :==img=========%s",
                pprint.pformat(img)
            ) 
            # _logger.info("+===================profile===pic====%s======%s",profile_pic,type(profile_pic))
            elements = img.get('elements')
            _logger.info(
                "post-processing values for acquirer with id :==elements=========%s",
                pprint.pformat(elements)
            ) 
            
            elements_first = elements[0]
            _logger.info(
                "post-processing values for acquirer with id :==elements_first=========%s",
                pprint.pformat(elements_first)
            ) 
            identifiers = elements_first.get('identifiers')
            _logger.info(
                "post-processing values for acquirer with id :==identifiers=========%s",
                pprint.pformat(identifiers)
            ) 
            identifiers_first = identifiers[0]
            _logger.info(
                "post-processing values for acquirer with id :==identifiers_first=========%s",
                pprint.pformat(identifiers_first)
            ) 
            pic_url = identifiers_first.get('identifier')
            _logger.info(
                "post-processing values for acquirer with id :==pic_url=========%s",
                pprint.pformat(pic_url)
            ) 
            return identifiers_first
        else:
            return False


    def _auth_oauth_rpc_linkedin(self, endpoint, access_token,oauth_provider):
        headers = {'Content-Type': 'application/x-www-form-urlencoded', 'User-Agent': 'OAuth gem v0.4.4'}
        AUTH_CODE = access_token
        ACCESS_TOKEN_URL = 'https://www.linkedin.com/oauth/v2/accessToken'
        client_id= str(oauth_provider.client_id)
        client_secret= str(oauth_provider.secret_key)
        # redirect_uri = 'https://vp.nexusgurus.com/auth_oauth/signin'
        # changed==============================
        redirect_url = request.httprequest.url_root + 'auth_oauth/signin'
        PARAM = {'grant_type': 'authorization_code',
        'code': AUTH_CODE,
        'redirect_uri': redirect_url,
        'client_id': client_id,
        'client_secret': client_secret}
        response = requests.post(ACCESS_TOKEN_URL, data=PARAM, headers=headers, timeout=600)
        response = response.json()
        _logger.info("+==============response=======%s======",response)
        return response

    @api.model
    def _auth_oauth_validate(self, provider, access_token):
        """ return the validation data corresponding to the access token """
        oauth_provider = self.env['auth.oauth.provider'].browse(provider)
        if oauth_provider.auth_endpoint == 'https://www.linkedin.com/oauth/v2/authorization':
            validation = self._auth_oauth_rpc_linkedin(oauth_provider.validation_endpoint, access_token,oauth_provider)
            if validation.get("error"):
                raise Exception(validation['error'])
            access_token = validation['access_token']  
            if oauth_provider.data_endpoint:
                data = self.get_profile(access_token)
                # _logger.info("+==============data=======%s======",data)
                validation.update(data)
                data1 = self.get_email(access_token)
                # _logger.info("+==============data=======%s======",data)
                validation.update(data1)
                data2 = self.get_pic(access_token)
                if data2:
                    _logger.info("+======none==check========data=======%s======",data2)
                    validation.update(data2)
        
            _logger.info("====subject==%s===========",validation)
        
            subject = next(filter(None, [
                validation.pop(key, None)
                for key in [
                    'sub', # standard
                    'id', # google v1 userinfo, facebook opengraph
                    'user_id', # google tokeninfo, odoo (tokeninfo)
                ]
            ]), None)
            
            if not subject:
                raise AccessDenied('Missing subject identity')
            validation['user_id'] = subject

            return validation
        else:
            validation = self._auth_oauth_rpc(oauth_provider.validation_endpoint, access_token)
            if validation.get("error"):
                raise Exception(validation['error'])
            if oauth_provider.data_endpoint:
                data = self._auth_oauth_rpc(oauth_provider.data_endpoint, access_token)
                validation.update(data)
            return validation


    def _create_sinup_detail(self, provider, vals):
        oauth_uid = vals['oauth_uid']
        # login = vals.get('email') or vals.get('name')  # for twitter name is login and google explicit pass by contorller
        # email = vals.get('email', '%s_user_%s' % (provider.name, oauth_uid))
        email = vals.get('email')
        login = vals.get('email')
        name = vals.get('name', email)
        _logger.info("======%s===11=====%s=======%s========%s======",oauth_uid,login,email,name)
        return dict(vals,
                    name=name,
                    login=login,
                    email=email,
                    oauth_provider_id=provider.id,
                    active=True
                    )
    @api.model
    def _auth_oauth_user(self, provider, user_detail):
        oauth_uid = user_detail['oauth_uid']
        try:
            oauth_user = self.search([("oauth_uid", "=", oauth_uid), ('oauth_provider_id', '=', provider.id)])
            if not oauth_user:
                raise AccessDenied()
            assert len(oauth_user) == 1
            oauth_user.write({'oauth_access_token': user_detail['oauth_access_token']})
            return oauth_user.login
        except AccessDenied:
            if self.env.context.get('no_user_creation'):
                return None
            _, login, _ = self.signup(user_detail)
            return login


    @api.model
    def _singup_user(self, provider_id, vals):
        _logger.info("===========i was called i was shocked========%s",vals)
        
        provider = self.env['auth.oauth.provider'].sudo().browse(provider_id)
        
        user_detail = self._create_sinup_detail(provider, vals)
        _logger.info("+=====user_detail                   %s==========",user_detail)
        login = self._auth_oauth_user(provider, user_detail)
        
        return login


    def fetch_image(self, url):
        if not url:
            return False
        response = requests.get(url)
        image_base64 = False
        if 'image/' in response.headers['Content-Type']:
            image_base64 = base64.b64encode(response.content)
        return image_base64
        
    @api.model
    def auth_oauth(self, provider, params):
        oauth_provider = self.env['auth.oauth.provider'].browse(provider)
        if oauth_provider.auth_endpoint == 'https://www.linkedin.com/oauth/v2/authorization':
            access_token = params.get('code')
            validation = self._auth_oauth_validate(provider, access_token)

            elements = validation.get('elements')
            _logger.info("+=====elements                   %s==========",elements)
            elements_first = elements[0]
            _logger.info("+=====elements_first                   %s==========",elements_first)
            elements_handle = elements_first.get('handle~') 
        
            email = elements_handle.get('emailAddress')
            _logger.info("+=====email                   %s==========",email)
            image_url = validation.get('identifier')
            _logger.info("+=====image                   %s==========",image_url)
            image_1920 = self.fetch_image(image_url)
            vals = {
                'oauth_access_token': access_token,
                'name': validation.get('localizedFirstName') + ' '+ validation.get('localizedLastName'),
                'oauth_uid': validation.get('user_id') ,
                'email': email,
                'image_1920': image_1920,
                
            }
            provider_id = provider
            db = request.session['db']
            
            registry = registry_get(db)
            login = self._singup_user(provider_id, vals)
            return (self.env.cr.dbname, login, access_token)

        else:
            access_token = params.get('access_token')
            validation = self._auth_oauth_validate(provider, access_token)
            # required check
            if not validation.get('user_id'):
                # Workaround: facebook does not send 'user_id' in Open Graph Api
                if validation.get('id'):
                    validation['user_id'] = validation['id']
                else:
                    raise AccessDenied()

            # retrieve and sign in user
            login = self._auth_oauth_signin(provider, validation, params)
            if not login:
                raise AccessDenied()
            # return user credentials
            return (self.env.cr.dbname, login, access_token)


