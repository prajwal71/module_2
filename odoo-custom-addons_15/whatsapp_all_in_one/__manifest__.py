# -*- coding: utf-8 -*-
{
    'name': 'All In One WhatsApp Odoo Integration',
    'version': '13.0.1.0.1',
    'category': 'Tools',
    'author': 'Ascents Entrepreneurs',
    'license': 'OPL-1',
    'summary': 'All in One WhatsApp Integration with Odoo',
    'description': """
This module can be used to send messages to WhatsApp
----------------------------------------------------
Send Messages via WhatsApp
WhatsApp All in One Module
""",
    'depends': ['base', 'base_setup', 'contacts', 'account', 'sale_management', 'purchase', 'delivery', 'website_sale'],
     "assets": {
        "web.assets_backend":[
                              
                                 'whatsapp_all_in_one/static/src/js/many2many_tags_mobile.js',
                                  'whatsapp_all_in_one/static/src/js/refresh_qr_code.js',     
                             ],
         'web.assets_qweb': [
                                'whatsapp_all_in_one/static/src/xml/mobile_widget.xml',
                                'whatsapp_all_in_one/static/src/pos_whatsapp.xml/',

            
        ],

     },
    'data': [
        'security/ir.model.access.csv',
        'data/whatsapp_cron.xml',
        'wizard/send_wp_msg_views.xml',
        'views/res_partner_views.xml',
        'views/res_config_settings_views.xml',
        'views/account_inovice_form_wa_inherited.xml',
        'views/account_payment_form_wa_inherited.xml',
        # 'views/pos_config_views.xml',
        'views/purchase_order_form_wa_inherited.xml',
        'views/sale_order_form_wa_inherited.xml',
        'views/stock_picking_form_wa_inherited.xml',
        'views/templates.xml',
    ],
 
    'external_dependencies': {'python': ['phonenumbers', 'selenium']},
    'images': ['static/description/main_screenshot.gif'],
    'installable': True,
    'auto_install': False,
    'application': True,
    'sequence': 1,
    'currency': 'EUR',
    'price': 30,

}
