from odoo import models, fields, api, _


class PackageUpdate(models.TransientModel):
    _name = 'package.update'
    _description = 'Package Update'

    def get_data(self):
        if self.env.context.get('active_model') and self.env.context.get('active_id'):
            package_record_id = self.env[self.env.context.get('active_model')].browse(
                self.env.context.get('active_id'))
            if package_record_id:
                if self.env.context.get('package_update'):
                    package_record_id.write({'state': 'updated'})
                for package in package_record_id:
                    package.action_update_client()
        return