# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, Warning
import xmlrpc.client
import logging

_logger = logging.getLogger(__name__)

class accounting_support_package(models.Model):
    _name = 'accounting.support.package'
    _description = 'Accounting and Support Package'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    
    name = fields.Char('Client Name', required=True, tracking=True)
    partner_id = fields.Many2one('res.partner', string="Customer", required=True, tracking=True)
    client_id = fields.Integer(related='partner_id.id', string='Client ID', store=True)
    active = fields.Boolean(default=True)
    product_id = fields.Many2one('product.product', domain=[('default_code', 'ilike', 'support')], required=True, tracking=True)
    package_code = fields.Selection([
                    ('hourly', 'Hourly Support'),
                    ('amc', 'AMC based Support'),
                    ('advance', 'Advanced Support')
                ], tracking=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('updated', 'Updated'),
        ('cancel', 'Cancel')
    ], default="draft", tracking=True)
    number_of_hours = fields.Float('Number Of Hours', tracking=True)
    amc_date = fields.Date('AMC Date')
    bal_amount = fields.Float('Balance', compute="default_payment_method_id")
    
    @api.depends("partner_id")
    def default_payment_method_id(self):
        date = fields.Date.today()
        recv_amount = payb_amount = bal_amount = 0.00
        total = 0
        obj = self.env['account.move.line'].search([('partner_id', '=', self.partner_id.id)])
        for i in obj:
            if i.move_id.state == "posted":
                if i.balance != 0:
                    if i.account_id.reconcile == True:
                        if i.account_id.internal_type in ['receivable']:
                            print("i.partner_id.name", i.balance)
                            recv_amount = recv_amount + i.balance
                        elif i.account_id.internal_type in ['payable']:
                            payb_amount = payb_amount + i.balance
        bal_amount = recv_amount + payb_amount
        self.bal_amount = bal_amount



    @api.onchange('product_id')
    def onchange_product(self):
        for rec in self:
            if rec.product_id:
                if rec.product_id.default_code:
                    if 'hourly' in rec.product_id.default_code.lower():
                        rec.package_code = 'hourly'
                    elif 'amc' in rec.product_id.default_code.lower():
                        rec.package_code = 'amc'
                    elif 'advance' in rec.product_id.default_code.lower():
                        rec.package_code = 'advance'
            if rec.number_of_hours:
                rec.number_of_hours = 0.0

    def action_cancel(self):
        for rec in self:
            rec.state = 'cancel'

    def action_draft(self):
        for rec in self:
            rec.state = 'draft'

    def action_update(self):
        return {
            'name': _('Confirmation'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': self.env.ref('accounting_support_package.wizard_package_update_form_view').id,
            'res_model': 'package.update',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'package_update': True}
        } 


    def action_update_client(self):
        for rec in self:
            client_api_rec = self.env['api.client.config'].search(
                    [('partner_id', '=', rec.client_id)], limit=1)
            if client_api_rec:
                if client_api_rec.active:
                    url = client_api_rec.client_url
                    db = client_api_rec.client_database
                    username = client_api_rec.client_username
                    password = client_api_rec.client_password

                    try:
                        
                        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
                        uid = common.authenticate(db, username, password, {})
                        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

                        package_code_key = models.execute_kw(db, uid, password, 'ir.config_parameter', 'search',
                                                        [[['key', '=', 'support_package_code']]])
                        _logger.info('============================%s', package_code_key)
                        if package_code_key:
                            package_code_key = models.execute_kw(db, uid, password, 'ir.config_parameter', 'unlink',
                                                        [[package_code_key[0]]])
                        package_code_value = models.execute_kw(db, uid, password, 'ir.config_parameter', 'set_param',
                                                    ['support_package_code', rec.package_code])
                        _logger.info('============================%s', package_code_value)
                        if rec.package_code == 'hourly':
                            no_of_hour_key = models.execute_kw(db, uid, password, 'ir.config_parameter', 'search',
                                                        [[['key', '=', 'number_of_hours']]])
                            if no_of_hour_key:
                                no_of_hour_key = models.execute_kw(db, uid, password, 'ir.config_parameter', 'unlink',
                                                        [[no_of_hour_key[0]]])
                            no_of_hour_value = models.execute_kw(db, uid, password, 'ir.config_parameter', 'set_param',
                                                    ['number_of_hours', 'N/A'])
                            _logger.info('============================%s', no_of_hour_value)
                        else:
                            no_of_hour_key = models.execute_kw(db, uid, password, 'ir.config_parameter', 'search',
                                                        [[['key', '=', 'number_of_hours']]])
                            if no_of_hour_key:
                                no_of_hour_key = models.execute_kw(db, uid, password, 'ir.config_parameter', 'unlink',
                                                        [[no_of_hour_key[0]]])
                            no_of_hour_value = models.execute_kw(db, uid, password, 'ir.config_parameter', 'set_param',
                                                    ['number_of_hours', rec.number_of_hours])
                            _logger.info('============================%s', no_of_hour_value)

                    except:
                        if xmlrpc.client.ProtocolError:
                            raise ValidationError(_('Protocol error occurred'))
                        elif xmlrpc.client.Fault:
                            raise Warning(_('A fault occurred'))
                else:
                    raise Warning(_('Client API is inactive!'))
            else:
                raise Warning(_('No configuration for client %s found!!', rec.client_id))

