# -*- coding: utf-8 -*-
{
    'name': "Accounting Support Package",
    'version': '15.0',
    'summary': """
        Accounting Support Package""",

    'description': """
        Accounting Support Package
    """,

    'author': "Nexus Incorporation",
    'website': "http://nexusgurus.com",
    'category': 'Helpdesk',
    'version': '0.1',
    'depends': ['helpdesk_server_api'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'wizard/package_update.xml'
    ],
}
