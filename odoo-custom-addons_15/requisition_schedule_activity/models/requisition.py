# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
import logging
_logger = logging.getLogger(__name__)


class PurchaseRequistion(models.Model):
    _inherit = 'material.purchase.requisition'

    
    def requisition_confirm(self):
        res = super(PurchaseRequistion, self).requisition_confirm()
        if self.employee_id.parent_id.user_id:
            create_vals = {
                'activity_type_id': '4',
                'summary': 'Requisition Approval',
                'automated': True,
                'date_deadline': fields.Date.today(),
                'res_model_id': self.env['ir.model'].search([('model', '=', 'material.purchase.requisition')]).id,
                'res_id': self.id,
                'user_id': self.employee_id.parent_id.user_id.id
            }
            self.env['mail.activity'].create(create_vals)
        return res
    
    
    def manager_approve(self):
        res = super(PurchaseRequistion, self).manager_approve()
        self.activity_feedback(['mail.mail_activity_data_todo'])
        if self.employee_id.requisition_admin:
            create_vals = {
                'activity_type_id': '4',
                'summary': 'Requisition Approval',
                'automated': True,
                'date_deadline': fields.Date.today(),
                'res_model_id': self.env['ir.model'].search([('model', '=', 'material.purchase.requisition')]).id,
                'res_id': self.id,
                'user_id': self.employee_id.requisition_admin.id
            }
            self.env['mail.activity'].create(create_vals)
        return res

    def user_approve(self):
        res = super(PurchaseRequistion, self).user_approve()
        self.activity_feedback(['mail.mail_activity_data_todo'])
        return res



class HrEmployee(models.AbstractModel):
    _inherit = 'hr.employee.base'

    requisition_admin = fields.Many2one('res.users', 'Requisition Admin', domain="[('share', '=', False)]")
