# -*- coding: utf-8 -*-
{
    'name': "Requisition Schedule Activity",

    'summary': """
        Requisition Schedule Activity """,

    'description': """
       Requisition Schedule Activity.
    """,

    'author': "Nexus Incorporation",
    'website': "http://nexusgurus.com",

    'category': 'Requisition',
    'version': '0.1',

    'depends': ['material_purchase_requisitions'],

    'data': [
        'views/hr_employee_view.xml'
    ],

}
