# -*- coding: utf-8 -*-
{
    "name": "Custom Fields: Core",
    "version": "14.0.1.0.2",
    "category": "Extra Tools",
    "author": "faOtools",
    "website": "https://faotools.com/apps/14.0/custom-fields-core-484",
    "license": "Other proprietary",
    "application": True,
    "installable": True,
    "auto_install": False,
    'bootstrap': True,
    
    "depends": [
        "web_editor"
    ],
    'assets': {
        'website.assets_frontend': [
            
            'business_appointment_website/static/src/css/website_business_appointments.css',
            'business_appointment_website/static/src/js/slots.js',
             'business_appointment_website/static/src/js/frontend.js',
            
        ],
        
    },
    "data": [
        "security/ir.model.access.csv",
        "data/data.xml",
        # "views/view.xml"
    ],
    "qweb": [
        
    ],
    "js": [
        
    ],
    "demo": [
        
    ],
    "external_dependencies": {},
    "summary": "The technical core to add new fields for Odoo documents without any special knowledge",
    "description": """
For the full details look at static/description/index.html

* Features * 




#odootools_proprietary

    """,
    "images": [
        "static/description/main.png"
    ],
    "price": "28.0",
    "currency": "EUR",
    "live_test_url": "https://faotools.com/my/tickets/newticket?&url_app_id=108&ticket_version=14.0&url_type_id=3",
}