# -*- coding: utf-8 -*-
{
	"name" : "Product Warranty Registration and Claim",
	"version" : "13.0.1.1",
	"category" : "Industries",
	"depends" : ['base','website','portal','product','helpdesk','stock','mail'],
	"author": "Nexus Incorporation",
	'summary': 'Apps for product Warranty with serial number and claim history',
	"description": """
		Apps for product Warranty with serial number and claim history
	""",
	"website" : "htpps://www.nexusgurus.com",
	"price": 100,
	"currency": 'EUR',
	"data": [
		'security/warranty_security.xml',
		'security/ir.model.access.csv',
		'report/warranty_receipt.xml',
		'report/warranty_receipt_menu.xml',
		'data/cron.xml',
		'data/data.xml',
		'data/warranty_reg_email_data.xml',
		'views/my_warranty.xml',
		'views/warranty_view.xml',
		'views/warranty_claim_menu.xml',
		# 'views/warranty_sale_view.xml',
	],
	'qweb': [
	],
	"auto_install": False,
	"installable": True,
	
	
}

