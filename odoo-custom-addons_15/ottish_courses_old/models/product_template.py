from odoo import api, fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    # course_teachers = fields.Many2many('res.partner','course_teacher_rel', string="Teachers")
    language = fields.Selection([('english', 'English'), ('nepali', 'Nepali')], default='english', string="Language")
    teacher1 = fields.Char(string="Teacher 1")
    teacher2 = fields.Char(string="Teacher 2")
    teacher3 = fields.Char(string="Teacher 3")

    no_vacancy = fields.Integer("No. of Vacancy")

    exam_date = fields.Date(string="Examination Date")

    course_type = fields.Selection([('recorded', 'Recorded Class'), ('live', 'Live Class')], default='recorded', string="Class Type")
