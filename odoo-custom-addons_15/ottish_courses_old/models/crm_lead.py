from odoo import api, fields, models
import logging
_logger = logging.getLogger(__name__)

class ProductTemplate(models.Model):
    _inherit = 'crm.lead'

    
    transaction_ids = fields.One2many("payment.source.line" ,'crm_lead',string="Transactions")
    discounted_revenue = fields.Float(string="Discounted Price", readonly=True)
    
    
        

    @api.onchange('transaction_ids')
    def calculate_amount_set(self):
        for rec in self:
            tot = without_discount = 0.0
            if rec.transaction_ids:
                for res in rec.transaction_ids:
                    if res.product_course_id:
                        # rec.product_ids = [4, [res.product_course_id.id]]
                        _logger.info("===============%s",res.product_course_id.id)
                        # rec.write({'product_ids': [(4, res.product_course_id.id)] }) 
                        
                        res.amount = res.product_course_id.list_price
                        if res.discounted_amount:
                            res.amount_total = res.amount - res.discounted_amount
                        else:
                            res.amount_total = res.amount
                        tot += res.amount_total
                        without_discount += res.amount
                rec.discounted_revenue = tot
                rec.expected_revenue = without_discount
                
            else:
                rec.discounted_revenue = 0.0
                rec.expected_revenue = 0.0
                




            

