# Copyright 2014-2015 Grupo ESOC <www.grupoesoc.es>
# Copyright 2017-Apertoso N.V. (<http://www.apertoso.be>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models


class ResPartner(models.Model):
    _inherit = "res.partner"
    _name = 'res.partner'

    facebook=fields.Char(string='Facebook')
    twitter=fields.Char(string='Twitter')
    linkedin=fields.Char(string='LinkedIn')
    snapchat=fields.Char(string='Snapchat')
    tiktok=fields.Char(string='Tiktok')
    youtube=fields.Char(string='Youtube')
    instagram=fields.Char(string='Instagram')
    pinterest=fields.Char(string='Pinterest')