
{
    "name": "Contact Social Media Links",
    "summary": "Add social media links in the contact",
    "version": "13.0.1.0.0",
    "category": "Customer Relationship Management",
    "website": "https://nexusgurus.com",
    "author": "https://nexusgurus.com",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "auto_install": False,
    "depends": ["contacts"],
    "data": ["views/res_partner.xml"],
}
