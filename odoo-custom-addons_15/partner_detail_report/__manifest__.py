
# -*- coding: utf-8 -*-
{
    'name': 'Partner Detailed Report',
    'version': '13.0.1.0.1',
    'summary': """Partner Detailed Report""",
    'description': """Partner Detailed Report""",
    'category': 'Accounting',
    'author': 'Sagar',
    'company': 'Sagar',
    'maintainer': 'Sagar',
    'depends': ['base', 'account', 'report_xlsx', 'web'],
    'website': 'https://www.sagarcs.com',
    'data': [
        'wizard/rep_account_account.xml',
        'wizard/xlsx_partner_rep.xml',
        'reports/day_book_report.xml',
        # 'reports/day_book_template.xml',
        'reports/partner_detail_template.xml'
    ],
    'qweb': [],
    'images': ['static/description/banner.jpg'],
    'license': 'OPL-1',
    'installable': True,
    'auto_install': False,
    'application': False,
}
