# -*- coding: utf-8 -*-
from odoo import fields, models
from werkzeug import urls
from werkzeug.exceptions import NotFound, Forbidden

from odoo import http, _
from odoo.http import request

class InheritResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    show_whatsapp_icon = fields.Boolean(string="Show Whatsapp Icon", related='website_id.whatsapp_icon_show',
                                        readonly=False)
    whatsapp_no = fields.Char(string="WhatsApp Number", related='website_id.whatsapp_number', readonly=False)


class InheritWebsiteModule(models.Model):
    _inherit = "website"

    whatsapp_number = fields.Char(string='Whatsapp Number')
    whatsapp_icon_show = fields.Boolean(string='Show WhatsApp Icon')

	

	# wa_api_url = 'https://api.whatsapp.com/send?phone=%s&text=%s' % (self.phone,message)
    def redirect_whatsapp_url(self):

        message = request.httprequest.url_root[:-1] + (hasattr(request, 'rerouting') and request.rerouting[0] or request.httprequest.path)
        # message = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        """ Returns WhatsApp Redirect URL. """
        # wa_url = "https://wa.me/" + str(self.whatsapp_number)
        wa_url = 'https://api.whatsapp.com/send?phone=%s&text=%s' % (self.whatsapp_number,message)

        return wa_url

    def redirect_whatsapp_url_chat(self):

        """ Returns WhatsApp Redirect URL. """
        wa_url = "https://wa.me/" + str(self.whatsapp_number)


        return wa_url
