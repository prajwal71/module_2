# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo.http import request
from odoo import http, _
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from dateutil.relativedelta import relativedelta
import base64
import logging
import json
_logger = logging.getLogger(__name__)
# import yaml
import werkzeug

class XlabPromHt(http.Controller):

    @http.route(['/register-product'], type='http', auth="user", website=True)
    def auth_product_xlab(self, page=1, **kwargs):
        _logger.info("--reached to controller--")
        if request.env.user.partner_id:
            name=request.env.user.partner_id.name
            email=request.env.user.partner_id.email
            mobile=request.env.user.partner_id.mobile
        _logger.info('-----------------TEST: - %s - This is happening-------------',email)
        products=request.env['product.product'].sudo().search([('under_warranty','=',True)])
        return request.render("xlab_kasper.xlab_prom", {'email': email,'name':name,'mobile':mobile,'products':products})

    @http.route(['/check-xlab-promo-details'], type='http', auth="user", website=True)
    def check_promo_code_xlab(self, page=1, **post):
        _logger.info("=====posttt===%s",post)
        partner_id=request.env.user.partner_id.id
        if not post:
            return request.render('http_routing.404')
        name = post['name']
        phone = post['phone_number']
        email = post['email']
        purchase_date = post['purchase_date']
        product_id = post['product_id']
        serial_no = post['serial_no']

        attachment = post['attachment']
        accept1 = post['accept1']
        customer_obj = request.env['res.partner'].sudo().search([('id','=',partner_id)])
        prom_obj = request.env['xlab.kasper.prom']
        ksp = False
        if not customer_obj:
            k =  customer_obj.sudo().create({
            'name': name,
            'email': email,
            'mobile':phone,
            })
            
            
            serial_obj = request.env['stock.production.lot'].sudo().search([('product_id','=',int(product_id)),('name','=', serial_no),('company_id','=', 2)],limit=1,order='create_date desc')
            _logger.info("=====dddd===%s",serial_obj) 
            if serial_obj:
                ksp_exist = request.env['xlab.kasper.prom'].sudo().search([('lot_id.name','=', serial_no)])
                warranty_obj_exist = request.env['product.warranty'].sudo().search([('product_serial_id','=', serial_obj.id)])
                if warranty_obj_exist:
                    return request.render("xlab_kasper.promo_code_failed_xlab_exist")
                if not ksp_exist:
                    ksp = prom_obj.sudo().create({
                    'lot_id': serial_obj.id,
                    'partner_id': customer_obj.id,
                    })
                    if ksp:
                        if post.get('attachment',False):
                            Attachments = request.env['ir.attachment']
                            name = post.get('attachment').filename      
                            files = post.get('attachment').stream.read()
                            # rec_id = request.env['product.warranty'].search([], limit=1, order='create_date desc')
                            # attachment = files.read() 
                            attachment_id = Attachments.sudo().create({
                                'name':name,
                                # 'datas_fname':name,
                                'type': 'binary',   
                                'res_model': 'xlab.kasper.prom',
                                'res_id':ksp.id,
                                'datas': base64.encodebytes(files),
                            }) 
                if not warranty_obj_exist:
                    warranty_obj = request.env['product.warranty'].sudo().create({
                    'product_serial_id': serial_obj.id,
                    'partner_id': customer_obj.id,
                    'purchase_date': purchase_date,
                    'accept1': True,
                    })
                    if warranty_obj:
                        if post.get('attachment',False):
                            Attachments = request.env['ir.attachment']
                            name = post.get('attachment').filename      
                            files = post.get('attachment').stream.read()
                            # rec_id = request.env['product.warranty'].search([], limit=1, order='create_date desc')
                            # attachment = files.read() 
                            w_attachment_id = Attachments.sudo().create({
                                'name':name,
                                # 'datas_fname':name,
                                'type': 'binary',   
                                'res_model': 'product.warranty',
                                'res_id':warranty_obj.id,
                                'datas': base64.encodebytes(files),
                            }) 
                        warranty_obj.send_notif_email_warranty()
            else:
                return request.render("xlab_kasper.promo_code_failed_xlab")
        else:
            customer_obj.write({
            #    'email': email,
                'mobile':phone,})
            serial_obj = request.env['stock.production.lot'].sudo().search([('product_id','=',int(product_id)),('name','=', serial_no)],limit=1,order='create_date desc')
            _logger.info("=====dddd===%s",serial_obj) 
            if serial_obj:
                ksp_exist = request.env['xlab.kasper.prom'].sudo().search([('lot_id.name','=', serial_no)])
                warranty_obj_exist = request.env['product.warranty'].sudo().search([('product_serial_id','=', serial_obj.id)])
                if warranty_obj_exist:                                       
                    return request.render("xlab_kasper.promo_code_failed_xlab_exist")
                if not ksp_exist:
                    ksp = prom_obj.sudo().create({
                    'lot_id': serial_obj.id,
                    'partner_id': customer_obj.id,
                    })
                    if ksp:
                        if post.get('attachment',False):
                            Attachments = request.env['ir.attachment']
                            name = post.get('attachment').filename      
                            files = post.get('attachment').stream.read()
                            # rec_id = request.env['product.warranty'].search([], limit=1, order='create_date desc')
                            # attachment = files.read() 
                            attachment_id = Attachments.sudo().create({
                                'name':name,
                                # 'datas_fname':name,
                                'type': 'binary',   
                                'res_model': 'xlab.kasper.prom',
                                'res_id':ksp.id,
                                'datas': base64.encodebytes(files),
                            }) 
                if not warranty_obj_exist:
                    warranty_obj = request.env['product.warranty'].sudo().create({
                    'product_serial_id': serial_obj.id,
                    'partner_id': customer_obj.id,
                    'purchase_date': purchase_date,
                    'accept1': True,
                    })
                    if warranty_obj:
                        if post.get('attachment',False):
                            Attachments = request.env['ir.attachment']
                            name = post.get('attachment').filename      
                            files = post.get('attachment').stream.read()
                            # rec_id = request.env['product.warranty'].search([], limit=1, order='create_date desc')
                            # attachment = files.read() 
                            w_attachment_id = Attachments.sudo().create({
                                'name':name,
                                # 'datas_fname':name,
                                'type': 'binary',   
                                'res_model': 'product.warranty',
                                'res_id':warranty_obj.id,
                                'datas': base64.encodebytes(files),
                            })
                        warranty_obj.send_notif_email_warranty()
            else:
                return request.render("xlab_kasper.promo_code_failed_xlab")
        if ksp:
            ksp.send_notif_email_xlab()
        return request.render("xlab_kasper.promo_code_success_xlab")
        
        
   

