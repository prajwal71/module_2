{
    "name" : "Xlab Kaspersky Promotion",
    "version" : "13.0.0.1",
    "category" : "Website",
    "depends" : ['website','portal','product','stock','warranty_registration'],
    "author": "Sagar",
    'summary': 'This apps helps to do kaspersky campaign',
    "description": """
    """,
    "website" : "www.sagarcs.com",
    "price": 50,
    "currency": 'EUR',
    "assets": {
        'web.assets_frontend': [
                                    
            ('xlab_kasper/static/src/js/custom.js'),('xlab_kasper/static/src/js/product_search.js')],

    },
    "data": [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/xlab_serial_tmpl.xml',
        'views/product_brand_view.xml',
        'data/email_xlab.xml',
        'views/temp_view.xml',
    ],
    'qweb': [
    ],
    "auto_install": False,
    "installable": True,
  
}

