from odoo import http, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.http import request
from werkzeug.exceptions import NotFound
from odoo.addons.website_hr_recruitment.controllers.main import WebsiteHrRecruitment
import logging

_logger = logging.getLogger(__name__)
class WebsiteHrRecruitmenteXt(WebsiteHrRecruitment):

    @http.route('''/jobs/apply/<model("hr.job"):job>''', type='http', auth="public", website=True, sitemap=True)
    def jobs_apply(self, job, **kwargs):
        # error = {}
        # default = {}
        res = super(WebsiteHrRecruitmenteXt,self).jobs_apply(job, **kwargs)
        nep_horoscope = request.env['horoscope'].sudo().search([])
        blood_group = request.env['blood.group'].sudo().search([])
        _logger.info("==================%s======heroscope",nep_horoscope)
        # if 'website_hr_recruitment_error' in request.session:
        #     error = request.session.pop('website_hr_recruitment_error')
        #     default = request.session.pop('website_hr_recruitment_default')
        # return request.render("website_hr_recruitment.apply", {
        #     'job': job,
        #     'error': error,
        #     'default': default,
        #     'blood_group':blood_group,
        #     'nep_horoscope':nep_horoscope,

        # })

        res.qcontext.update({'nep_horoscope':nep_horoscope,'blood_group':blood_group})
        return res
