# -*- coding: utf-8 -*-
{
    'name': "blood_group_and_horoscope",
    'summary': """
        Added Nepali Horoscope and Blood Group in Applicant""",
    'description': """
        Added Nepali Horoscope and Blood Group in Applicant
    """,
    'author': "Nexus Incorporation",
    'website': "http://nexusgurus.com",
    'category': 'Applicant',
    'version': '0.1',
    'depends': ['base', 'hr_recruitment', 'hr'],
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
       
        'views/blood_group.xml',
        'views/horoscope.xml',
         'views/template.xml',
    ],
}
