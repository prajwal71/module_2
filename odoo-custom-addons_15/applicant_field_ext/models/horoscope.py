# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class NepaliHoroscope(models.Model):
    _name = 'horoscope'
    _rec_name = 'horoscope'

    horoscope = fields.Char()


class BloodGroup(models.Model):
    _name = 'blood.group'
    _rec_name = 'blood_group'

    blood_group = fields.Char()



