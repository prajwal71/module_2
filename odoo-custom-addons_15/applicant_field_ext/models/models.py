# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)


class ApplicantFieldExt(models.Model):
    _inherit = 'hr.applicant'

    nep_horoscope = fields.Many2one('horoscope', string="Nepali Horoscope")

    blood_group = fields.Many2one('blood.group', string="Blood Group")

    def create_employee_from_applicant(self):
        result = super(ApplicantFieldExt, self).create_employee_from_applicant()
        for rec in self:
            if rec.nep_horoscope or rec.blood_group:
                EmpObj = self.env['hr.employee'].search([('private_email', '=', self.email_from)])
                EmpObj.write({
                    'nep_horoscope': rec.nep_horoscope,
                    'blood_group': rec.blood_group
                })
        return result


class EmployeeFieldExt(models.AbstractModel):
    _inherit = 'hr.employee.base'

    nep_horoscope = fields.Many2one('horoscope', string="Nepali Horoscope")

    blood_group = fields.Many2one('blood.group', string="Blood Group")






