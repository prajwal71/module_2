# -*- coding: utf-8 -*-
#################################################################################
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
#################################################################################
{
  "name"                 :  "Nexus Website Ticket",
  "summary"              :  "The module helps to fetch value in website ticket of Nexus.",
  "category"             :  "Website",
  "version"              :  "13.0.1.0.0",
  "sequence"             :  1,
  "author"               :  "Nexus",
  "description"          :  """Helpdesk Website""",
  "depends"              :  [
                             'website_helpdesk_form',
                            ],
  "data"                 :  [
                            #  'views/template.xml',
                            ],
  "images"               :  ['static/description/Banner.png'],
  "application"          :  True,
  "installable"          :  True,
}
