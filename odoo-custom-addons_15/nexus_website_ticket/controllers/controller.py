# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import http
from odoo.http import request
from odoo.addons.website.controllers import form


class WebsiteForm(form.WebsiteForm):

    @http.route(['/customer-care'], method="post", type='http', auth='public', website=True, csrf=False)
    def nexus_ticket_request(self, **post):
        """
        Browses all maintenance teams and equipments in backend and returns them to the web page
        """
        default_values = {}
        if request.env.user.partner_id != request.env.ref('base.public_partner'):
            default_values['name'] = request.env.user.partner_id.name
            default_values['email'] = request.env.user.partner_id.email
        return request.render("website.customer-care", {'default_values': default_values})