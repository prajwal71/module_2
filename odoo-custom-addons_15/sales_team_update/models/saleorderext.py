# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.

from odoo import fields, models,api,_
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta

import logging
_logger = logging.getLogger(__name__)

class SaleOrderExt(models.Model):
    _inherit = 'sale.order'

    @api.onchange('user_id')
    def onchange_user_id(self):
        if self.partner_id:
            if self.partner_id.team_id:
                  self.team_id = self.partner_id.team_id.id
                  _logger.info("=============================")



        else: 
            _logger.info("=========else========")
            if self.user_id:
                self.team_id = self.env['crm.team'].with_context(
                    default_team_id=self.team_id.id
                )._get_default_team_id(user_id=self.user_id.id)