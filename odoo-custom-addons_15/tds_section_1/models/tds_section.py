
from pkg_resources import require
from odoo import models, api,fields,_
import logging
import json
from odoo.exceptions import AccessError, UserError
_logger = logging.getLogger(__name__)

class TdsSection(models.Model):
    _name = 'tds.section'

    
    name = fields.Char(string="Revenue Headings",required=True)
    tds_type = fields.Many2one('tds.type',string="TDS Type",required=True)
    revenue_code = fields.Char(string="Revenue code")
 
    
    tds_rates = fields.Many2many('tds.rate',string="TDS Rate")

    # @api.depends('name', 'tds_rates')
    def name_get(self):
        _logger.info("=============my==self")
      
        b=[]
        c = []
        
        for rec in self:
            c=[]
            if rec.name and rec.tds_rates:
                for a in rec.tds_rates:
                    c.append(a.name)
                
                d = [x.replace("'", "") for x in c]
                b.append((rec.id, rec.name + str(d)))
            else:
                b.append((rec.id, rec.name))
            # b = rec.name + rec.revenue_code
            
        return b

 

 

class TdsType(models.Model):
    _name ='tds.type'

    # name = fields.Selection([('19','19'),('24','24'),('27','27'),
    #                         ('28','28'),('31','31'),('33','33'),('20','20')],string="TDS Type")

    name = fields.Char("TDS Type",required=True)


class TdsRate(models.Model):
    _name = 'tds.rate'

  
    name = fields.Char(string="TDS Rtae (in %)")
   