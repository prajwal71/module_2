{
    "name" : "Check Product Authorization",
    "version" : "11.0.0.1",
    "category" : "Website",
    "depends" : ['website','portal','website_sale','product_brand','product'],
    "assets":
        {
       'web.assets_frontend':
           [
            #    'web_serial_check/static/src/css/custom.css',
               'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css','https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js',
            'https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css',
            'web_serial_check/static/src/js/custom.js',
            'web_serial_check/static/src/css/profile_css.css',
            'web_serial_check/static/src/css/gallery-grid.css',
            'https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js',
            
            
            ],
        },
    "author": "Sagar/Nexus",
    'summary': 'This apps helps users to check authenticity of their product',
    "description": """
    """,
    "website" : "www.sagarcs.com",
    "price": 150,
    "currency": 'EUR',
    "data": [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/auth_product_serial.xml',
        'views/product_brand_view.xml',
        'views/temp_view.xml',
    ],
    'qweb': [
    ],
    "auto_install": False,
    "installable": True,
    "images":["static/description/Banner.png"],
}

