$( document ).ready(function() {
var input = document.querySelector("#mobile");
var pwd = document.querySelector("#password");
var cpwd = document.querySelector("#confirm_password");
// initialise plugin
if (input != null){
var iti =  window.intlTelInput(input, {
  // allowDropdown: false,
  // autoHideDialCode: false,
  // autoPlaceholder: "off",
  // dropdownContainer: document.body,
  // excludeCountries: ["us"],
  // formatOnDisplay: false,
  geoIpLookup: function(callback) {
    $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      var countryCode = (resp && resp.country) ? resp.country : "";
      callback(countryCode);
    });
  },
  // hiddenInput: "full_number",
  initialCountry: "np",
  // localizedCountries: { 'de': 'Deutschland' },
  // nationalMode: false,
  onlyCountries: ['np'],
  // placeholderNumberType: "MOBILE",
  // preferredCountries: ['cn', 'jp'],
  // separateDialCode: true,
  utilsScript: "/signup_valid/static/src/intl-tel-input-master/build/js/utils.js",
});

// function submit() {
//   var iti = window.intlTelInputGlobals.getInstance(input);
// var k = iti.isValidNumber();
// alert(k);
// }

var elementCustom = document.getElementById('custum');
var errorMsg = document.querySelector("#error-msg"),
validMsg = document.querySelector("#valid-msg");
var errorMsgPass = document.querySelector("#error-msg-pass"),
validMsgPass = document.querySelector("#valid-msg-pass");

// here, the index maps to the error code returned from getValidationError - see readme
var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
var countryData = iti.getSelectedCountryData();
input.addEventListener("countrychange", function() {
    countryData = iti.getSelectedCountryData();
    var k = countryData['iso2'];
  // if (k == 'us'){
  //   $("#country_id").val(233);
  // }
  if(k == 'np'){
    $("#country_id").val(167);
  }
  console.log(countryData);
});


var reset = function() {
input.classList.remove("error");
errorMsg.innerHTML = "";
errorMsg.classList.add("hide");
validMsg.classList.add("hide");
errorMsgPass.classList.add("hide");
validMsgPass.classList.add("hide");
};



// on blur: validate
input.addEventListener('blur', function() {

  reset();
  if (input.value.trim()) {
  if (iti.isValidNumber()) {
    if (pwd.value == cpwd.value ){
      if (!(elementCustom.disabled)) {
      validMsg.classList.remove("hide");
      $('#sgn_up').removeAttr("disabled");
      $(".alert-warning").alert('close');
      }

  }
  else{
    $('#sgn_up').attr("disabled", true);
  }
  } else {
    input.classList.add("error");
    var errorCode = iti.getValidationError();
    console.log('err');
    console.log(errorCode);
    if (errorCode < 0 || errorCode > 4)
    {
      errorCode = 0;
    }
    errorMsg.innerHTML = errorMap[errorCode];
    errorMsg.classList.remove("hide");
    $('#sgn_up').attr("disabled", true);


  }
  }
});

// on mouse out validate
input.addEventListener('mouseout', function() {

  reset();
  if (input.value.trim()) {
  if (iti.isValidNumber()) {
    if (pwd.value == cpwd.value ){
      if (!(elementCustom.disabled)) {
      validMsg.classList.remove("hide");
      $('#sgn_up').removeAttr("disabled");
      $(".alert-warning").alert('close');
      }

  }
  else{
    $('#sgn_up').attr("disabled", true);
  }
  } else {
    input.classList.add("error");
    var errorCode = iti.getValidationError();
    console.log('err');
    console.log(errorCode);
    if (errorCode < 0 || errorCode > 4)
    {
      errorCode = 0;
    }
    errorMsg.innerHTML = errorMap[errorCode];
    errorMsg.classList.remove("hide");
    $('#sgn_up').attr("disabled", true);


  }
  }
});

pwd.addEventListener('blur', function() {
  reset();
  if (input.value.trim()) {
  if (iti.isValidNumber()) {
    if (pwd.value == cpwd.value ){
      if (!(elementCustom.disabled)) {
      // validMsg.classList.remove("hide");
      $('#sgn_up').removeAttr("disabled");
      $(".alert-warning").alert('close');
      }

  }
  else{
    $('#sgn_up').attr("disabled", true);
  }
  } else {
    input.classList.add("error");
    var errorCode = iti.getValidationError();
    console.log('err');
    console.log(errorCode);
    if (errorCode < 0 || errorCode > 4)
    {
      errorCode = 0;
    }
    errorMsg.innerHTML = errorMap[errorCode];
    errorMsg.classList.remove("hide");
    $('#sgn_up').attr("disabled", true);
  
  
  }
  }
  });
 
  cpwd.addEventListener('keyup', function() {
    reset();
    if (pwd.value != cpwd.value ){
      errorMsgPass.classList.remove("hide");
      $(".alert-warning").alert('close');
      // $('#sgn_up').before('<div class="alert alert-warning">Confirm Password doesnot Match.</div>');
    }
    else{
      validMsgPass.classList.remove("hide")
      // errorMsgPass.classList.remove("hide");
      $(".alert-warning").alert('close');
    }

    if (input.value.trim()) {
      if (iti.isValidNumber()) {
        if (pwd.value == cpwd.value ){
          if (!(elementCustom.disabled)) {
          validMsg.classList.remove("hide");
          $('#sgn_up').removeAttr("disabled");
          $(".alert-warning").alert('close');
          }

      }
      else{
        $('#sgn_up').attr("disabled", true);
        $(".alert-warning").alert('close');
        // $('#sgn_up').before('<div class="alert alert-warning">Confirm Password doesnot Match.</div>');

      }
    } 
      else {
        input.classList.add("error");
        var errorCode = iti.getValidationError();
        console.log('err');
        console.log(errorCode);
        if (errorCode < 0 || errorCode > 4)
        {
          errorCode = 0;
        }
        errorMsg.innerHTML = errorMap[errorCode];
        errorMsg.classList.remove("hide");
        $('#sgn_up').attr("disabled", true);
      
      
    }
    }
    });



// on keyup / change flag: reset
input.addEventListener('change', reset);
input.addEventListener('keyup', reset);
}
});
