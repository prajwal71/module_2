import re
from odoo import models, api, fields, _
from odoo.exceptions import AccessDenied, AccessError, UserError, ValidationError
from odoo.http import request
import json
import logging
_logger = logging.getLogger(__name__)


class ResUserExt(models.Model):
    _inherit = "res.users"

    # mobiles = fields.Char(store=True, related="partner_id.mobile")
    # mobile = fields.Char()
    # _sql_constraints = [
    #     ('mobile_key', 'UNIQUE (mobile)',
    #      'You can not have two users with the same mobile number !')
    # ]

    @api.model
    def _get_login_domain(self, login):
        _logger.info('............LOGIN...................%s..............LOGIN', login)
        return ['|', ('login', '=', login), ('mobile', '=', '+977' + login)]