# -*- coding: utf-8 -*-

from email.policy import default
from odoo import models, fields, api
import random as r
import logging
_logger = logging.getLogger(__name__)

class SignUpDetailVerify(models.Model):
    _name = 'signup.detail.verify'
    _description = 'Signup Email Verify'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    def _default_website(self):
        return self.env['website'].search([('company_id', '=', self.env.company.id)], limit=1)

    
    email = fields.Char(string='Email',required=True,readonly=True)
    
    vcode = fields.Integer(string='Verification Code',readonly=True)
    company_id = fields.Many2one('res.company', string='Company', required=True,
        default=lambda self: self.env.company,readonly=True)
    website_id = fields.Many2one('website', string="website",
                                 default=_default_website, ondelete='cascade',readonly=True)
    user_id = fields.Many2one('res.users', string='Registered User',readonly=True)
    is_done = fields.Boolean(string='Is Verify Done?',default=False,readonly=True)
    def otpgen(self):
        otp=""
        for i in range(6):
            otp+=str(r.randint(1,9))
        return otp
 
    def send_otp_email(self):
        for rec in self:
            if rec.email:
                _logger.info("=====%s",self.env.user)
                
                c_otp = rec.otpgen()
                _logger.info("==testotp===%s",c_otp)
                rec.vcode = c_otp
                _logger.info("==testotp=22==%s",rec.vcode)
                template = self.env.ref('signup_valid.email_template_signup_otp')
                if template:
                    a = self.env['mail.template'].browse(template.id).send_mail(self.id,force_send=True)
                    _logger.info("+========================test===============%s",a)
