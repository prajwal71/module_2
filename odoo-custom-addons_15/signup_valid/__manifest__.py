# -*- coding: utf-8 -*-
{
    'name': "Signup Validation",

    'summary': """
        This Module helps to verify email before Signup""",

    'description': """
               This Module helps to verify email before Signup""",


    'author': "Nexus Incorporation",
    'website': "http://www.nexusgurus.com",

    'category': 'Website',
    'version': '13.0.0.1',

    'depends': ['website','auth_signup','base','mail'],
     'bootstrap': True,
     'assets': {
        'web.assets_frontend': [
          'signup_valid/static/src/css/custom.css',
           'signup_valid/static/src/intl_tel_input_master/build/css/intlTelInput.css',
           'signup_valid/static/src/js/signup_valid.js',
           'signup_valid/static/src/js/ph_no.js',
           'signup_valid/static/src/js/check_duplicate_phn.js',
           'signup_valid/static/src/intl_tel_input_master/build/js/**/*',
          
        ],
    },

    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/views.xml',
        'views/templates.xml',
    ],    
}
