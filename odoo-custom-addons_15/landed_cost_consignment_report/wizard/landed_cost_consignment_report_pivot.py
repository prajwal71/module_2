from odoo import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)


class LandedCostPivot(models.Model):
    _name = "landed.cost.report.pivot"

    date = fields.Date(string='Date',readonly=True)
    invoice_ref = fields.Char(string='Invoice Ref',readonly=True)
    vendor_name = fields.Char(string="Vendor Name",readonly=True)
    fc_value = fields.Float(string="FC Value",readonly=True)
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True)
    rate = fields.Float(string="Exchange Rate",readonly=True)
    qty = fields.Float(string="Quantity",readonly=True)
    basic_amt = fields.Float(string="Purchase Amount",readonly=True)
    discount = fields.Float(string="Discount",readonly=True)
    
    custom_clearing = fields.Float(string="Custom Clearing",readonly=True)
    custom_duty = fields.Float(string="Custom Duty",readonly=True)
    freight_india = fields.Float(string="Freight India",readonly=True)
    insurance = fields.Float(string="Insurance",readonly=True)
    lc_expense = fields.Float(string="LC Expense",readonly=True)
    load_unload = fields.Float(string="Load/Unload",readonly=True)
    other_expenses = fields.Float(string="Other Expenses",readonly=True)
    sea_freight = fields.Float(string="Sea Freight",readonly=True)
    service_charge = fields.Float(string="Service Charge",readonly=True)
    tt_expense = fields.Float(string="TT Expense",readonly=True)
    freight_nepal = fields.Float(string="Freight Nepal",readonly=True)
    invoice_grand_total = fields.Float(string="Total",readonly=True)
    landed_cost_total = fields.Float(string="Landed Cost Total",readonly=True)

class LandedCostPivot(models.Model):
    _name = "landed.cost.report.pivot.detail"

    date = fields.Date(string='Date',readonly=True)
    invoice_ref = fields.Char(string='Invoice Ref',readonly=True)
    partner_id = fields.Many2one('res.partner',string="Vendor Name",readonly=True)
    purchase_name = fields.Char(string="Purchase Name",readonly=True)
    fc_value = fields.Float(string="FC Value",readonly=True)
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True)
    rate = fields.Float(string="Exchange Rate",readonly=True)
    unit_price = fields.Float(string="Unit Price",readonly=True)
    qty = fields.Float(string="Quantity",readonly=True)
    basic_amt = fields.Float(string="Purchase Amount",readonly=True)
    discount = fields.Float(string="Discount",readonly=True)
    product_id = fields.Many2one('product.product',string="Product",readonly=True)
    custom_clearing = fields.Float(string="Custom Clearing",readonly=True)
    custom_duty = fields.Float(string="Custom Duty",readonly=True)
    freight_india = fields.Float(string="Freight India",readonly=True)
    insurance = fields.Float(string="Insurance",readonly=True)
    lc_expense = fields.Float(string="LC Expense",readonly=True)
    load_unload = fields.Float(string="Load/Unload",readonly=True)
    other_expenses = fields.Float(string="Other Expenses",readonly=True)
    sea_freight = fields.Float(string="Sea Freight",readonly=True)
    service_charge = fields.Float(string="Service Charge",readonly=True)
    tt_expense = fields.Float(string="TT Expense",readonly=True)
    freight_nepal = fields.Float(string="Freight Nepal",readonly=True)
    invoice_grand_total = fields.Float(string="Total",readonly=True)
    landed_cost_total = fields.Float(string="Landed Cost Total",readonly=True)
    additional_landed_cost =fields.Float(string="Additional Cost")


    
class ProductInherit(models.Model):
    _inherit = "product.product"

    lc_f_name = fields.Many2one('ir.model.fields',string="LC Pivot Name",domain=[('model_id.model','=','landed.cost.report.pivot')])
    lc_seq = fields.Integer(string="Lc seq")    