# -*- coding: utf-8 -*-
import xlwt
import base64
import calendar
from io import StringIO
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning
from datetime import datetime

import logging
_logger = logging.getLogger(__name__)



class LandedCostConsignmentReport(models.TransientModel):
    _name = "landed.cost.consigment.report"
    
    start_date = fields.Date(string='Start Date', required=True, default=datetime.today().replace(day=1))
    end_date = fields.Date(string="End Date", required=True, default=datetime.now().replace(day = calendar.monthrange(datetime.now().year, datetime.now().month)[1]))
    
    landedcost_data = fields.Char('Name',)
    file_name = fields.Binary('Landed Cost Excel Report', readonly=True)
    state = fields.Selection([('choose', 'choose'), ('get', 'get')],
                             default='choose')
    partner_id = fields.Many2many('res.partner', string='Vendor')
    origin = fields.Many2many('purchase.order', string='Source Document')
    
    _sql_constraints = [('check','CHECK((start_date <= end_date))',"End date must be greater then start date")]

    # @api.onchange('partner_id')
    # def _onchange_partner_id(self):
    #     if self.partner_id:
    #         purchase_list = []
    #         obj = self.env['purchase.order'].search([('partner_id', 'in', self.partner_id.ids)])
    #         if obj:
    #             for lst in obj:
    #                 purchase_list.append(lst.id)
    #             self.origin = [(6,0,purchase_list)]
    #     else:
    #         self.origin = False
    
    def open_lc_table(self):
        report_search = self.env['landed.cost.report.pivot'].search([])
        if report_search:
            report_search.unlink()
        tree_view_id = self.env.ref('landed_cost_consignment_report.view_landed_consignment_report_pivot_tree').id
        form_view_id = self.env.ref('landed_cost_consignment_report.view_landed_consignment_report_pivot_form').id
        # graph_view_id = self.env.ref('agent_aged_pivot_report.view_aged_report_graph').id
        pivot_view_id = self.env.ref('landed_cost_consignment_report.view_landed_consignment_report_pivot_pivot').id
        
        
        ldata = self.env['stock.landed.cost'].search([('date', '>=', self.start_date), ('date', '<=', self.end_date), 
                                                ('state', '=', 'done')])
        record = []
        if ldata:
            for rec in ldata:
                record.append(rec)
            self.record= record.sort(key=lambda p: (p.date, p.name))
           
            landed_product_rset = self.env['product.product'].search([('landed_cost_ok','=',True)],order="lc_seq asc")
            landed_cost_product_lst = []
            for landed_product in landed_product_rset:                    
                landed_cost_product_lst.append(landed_product.id)
            
            if record:
                for rec in record:
                    invoice_qty_count = 0

                    invoice_grand_total = 0
                    landed_cost_total = 0
                    r1 = self.env['landed.cost.report.pivot'].create({
                        'date':rec.date,
                    })
                    picking_rset = self.env['stock.picking'].search([('id','in',rec.picking_ids.ids)],limit=1)
                    if picking_rset:
                        if picking_rset.origin:
                            purchase_rset = self.env['purchase.order'].search([('name','=',picking_rset.origin)],limit=1)
                            
                            if purchase_rset:
                                r1.write({
                                    'invoice_ref':purchase_rset.partner_ref,
                                    'vendor_name':purchase_rset.partner_id.name,
                                    'fc_value':purchase_rset.amount_total,
                                    'currency_id':purchase_rset.currency_id.id,
                                })
                                
                                invoice_rset = self.env['account.move'].search([('id','in',purchase_rset.invoice_ids.ids),('state','=','posted'),('type','=','in_invoice')],limit=1)
                                if invoice_rset:
                                    r1.write({
                                    'invoice_ref':invoice_rset.ref,
                                     })

                                    currency_rset = self.env['res.currency'].search([('id','=',invoice_rset.currency_id.id)],limit=1)
                                    if currency_rset:
                                        if self.env.company.currency_id == invoice_rset.currency_id:
                                            exchange_rate = 1
                                            r1.write({
                                                    'rate':exchange_rate,
                                                })
                                            # _logger.info("==================here===there========%s",exchange_rate)
                                        else:
                                            currency_rate_rset = self.env['res.currency.rate'].search([('name','=',invoice_rset.date)],limit=1)
                                            if currency_rate_rset:
                                                exchange_rate = round(1/currency_rate_rset.rate,2)
                                                r1.write({
                                                    'rate':exchange_rate,
                                                })
                                        for line_rset in invoice_rset.invoice_line_ids:
                                            invoice_qty_count += line_rset.quantity
                                        r1.write({
                                            'qty':invoice_qty_count,
                                        })
                                        
                                        basic_amount = round(exchange_rate * invoice_rset.amount_total,2)
                                        
                                        r1.write({
                                            'basic_amt':basic_amount,
                                            'discount': 0.0,
                                        })
                                        invoice_grand_total += basic_amount

                                         
                            col=8
                            # lc_fld_list = []

                            for lc in landed_cost_product_lst:
                                sum_lc_pdt = 0
                                ln1 = self.env['stock.landed.cost.lines'].search([('cost_id','=',rec.id),('product_id','=',lc)])     
                                if ln1:
                                    for ln in ln1:
                                        sum_lc_pdt += ln.price_unit
                                    r1.write({
                                            str(ln1[0].product_id.lc_f_name.name):sum_lc_pdt,
                                        }) 
                    invoice_grand_total += rec.amount_total
                    landed_cost_total += rec.amount_total
                    r1.write({
                                    'invoice_grand_total': invoice_grand_total ,
                                    'landed_cost_total': landed_cost_total
                                })   
                                  
                                   

        action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (pivot_view_id, 'pivot'),
                    (tree_view_id, 'tree'), 
                    (form_view_id, 'form'),
                   
                ],
                'view_mode': 'tree,form',
                'name': _('LC Consignment Report'),
                'res_model': 'landed.cost.report.pivot',
                # 'search_view_id': search_view_ref and search_view_ref.id,
                'context': {'group_by': ['vendor_name']}
            }
        return action

    

    def action_landed_cost_consignment_report(self):
        workbook = xlwt.Workbook()
        
        if self.partner_id and not self.origin:
            ldata = self.env['stock.landed.cost'].search([('date', '>=', self.start_date), ('date', '<=', self.end_date), ('picking_ids.partner_id', 'in', self.partner_id.ids),
                                                    ('state', '=', 'done')])
            _logger.info('=================%s', ldata)

        elif self.origin and not self.partner_id:
            origin_list = []
            obj = self.env['purchase.order'].search([('id', 'in', self.origin.ids)])
            if obj:
                for lst in obj:
                    origin_list.append(lst.name)
            _logger.info('==================%s', origin_list)
            ldata = self.env['stock.landed.cost'].search([('date', '>=', self.start_date), ('date', '<=', self.end_date), ('picking_ids.origin', 'in', origin_list),
                                                    ('state', '=', 'done')])
        elif self.origin and self.partner_id:
            origin_list = []
            obj = self.env['purchase.order'].search([('id', 'in', self.origin.ids)])
            if obj:
                for lst in obj:
                    origin_list.append(lst.name)
            _logger.info('==================%s', origin_list)
            ldata = self.env['stock.landed.cost'].search([('date', '>=', self.start_date), ('date', '<=', self.end_date), ('picking_ids.origin', 'in', origin_list), ('picking_ids.partner_id', 'in', self.partner_id.ids),
                                                    ('state', '=', 'done')])
        else:
            ldata = self.env['stock.landed.cost'].search([('date', '>=', self.start_date), ('date', '<=', self.end_date),
                                                    ('state', '=', 'done')])
        
        sheet = workbook.add_sheet('LandedCost001',cell_overwrite_ok=True)
        sheet.col(0).width = int(15*260)
        sheet.col(1).width = int(15*260)    
        sheet.col(2).width = int(30*260)    
        sheet.col(3).width = int(15*260) 
        sheet.col(4).width = int(15*260)   
        sheet.col(5).width = int(15*260)
        sheet.col(6).width = int(15*260)
        sheet.col(7).width = int(15*260)
        sheet.col(8).width = int(15*260)
        sheet.col(9).width = int(15*260)
        sheet.col(10).width = int(15*260)
        sheet.col(11).width = int(15*260)
        sheet.col(12).width = int(15*260)
        sheet.col(13).width = int(15*260)
        sheet.col(14).width = int(15*260)
        sheet.col(15).width = int(15*260)
        sheet.col(16).width = int(15*260)
        sheet.col(17).width = int(15*260)
        sheet.col(18).width = int(15*260)
        sheet.col(19).width = int(15*260)
        sheet.col(20).width = int(15*260)
        sheet.col(21).width = int(15*260)
        sheet.col(22).width = int(15*260)
        sheet.col(23).width = int(15*260)
        sheet.col(24).width = int(15*260)



        format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour gray25;align: horiz center')
        format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
        format11 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour yellow;align: horiz left')

        format2 = xlwt.easyxf('font:bold True;align: horiz left')
        format3 = xlwt.easyxf('align: horiz left')
        format4 = xlwt.easyxf('align: horiz right')
        format5 = xlwt.easyxf('font:bold True;align: horiz right')
        format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz right')
        format7 = xlwt.easyxf('font:bold True;borders:top thick;align: horiz right')
        format8 = xlwt.easyxf('font:bold True;borders:top thick;pattern: pattern solid, fore_colour gray25;align: horiz left')

        sheet.write_merge(0, 2, 0, 9, 'Landed Cost Report' , format0)
      

        record = []
        if ldata:
            for rec in ldata:
                record.append(rec)
              
            file = StringIO()        
            self.record= record.sort(key=lambda p: (p.date, p.name))
            final_value = {}
           
            
            timestamp = str(datetime.today())
            row=16
            sheet.write(5, 0, "Company:", format1)
            sheet.write(5, 1, str(rec.company_id.name), format2)
            sheet.write(6, 0, 'Report generated at:', format1)
            sheet.write(6, 1, timestamp, format2)
            # sheet.write(7, 0, "Seller Address:", format1)
            # sheet.write(7, 1, str(rec.company_id.street), format2)
            # sheet.write(8, 0, "Duration of sales", format1)
            # sheet.write(8, 1, str(self.start_date), format2)
            # sheet.write(8, 2, 'to', format2)
            # sheet.write(8, 3, str(self.end_date), format2)
            # sheet.write(10, 1, 'Report generated at:', format2)
            # sheet.write(10, 2, timestamp, format2)
            
            sheet.write(15, 0, 'Date', format1)
            sheet.write(15, 1, 'Invoice', format1)
            sheet.write(15, 2, 'Vendor Name', format1)
            sheet.write(15, 3, 'FC Value', format1)
            sheet.write(15, 4, 'Currency', format1)
            sheet.write(15, 5, 'Exchange Rate', format1)
            sheet.write(15, 6, 'Qty', format1)
            sheet.write(15, 7, 'Purchase Amount', format1)
            sheet.write(15, 8, 'Discount', format1)
            col=8
            landed_product_rset = self.env['product.product'].search([('landed_cost_ok','=',True)],order="lc_seq asc")
            landed_cost_product_lst = []
            for landed_product in landed_product_rset:                    
                sheet.write(15, col, landed_product.name, format11)
                col += 1
                landed_cost_product_lst.append(landed_product.id)

            if record:
                fc_value_grand_total = 0
                invoice_qty_grand_total = 0
                basic_amount_grand_total = 0
                landed_cost_lst =[]
                final_invoice_grand_total = 0
                for rec in record:
                    invoice_qty_count = 0

                    invoice_grand_total = 0
                    landed_cost_grand_total = 0
                    sheet.write(row, 0, str(rec.date), format3)
                    picking_rset = self.env['stock.picking'].search([('id','in',rec.picking_ids.ids)],limit=1)
                    if picking_rset:
                        if picking_rset.origin:
                            purchase_rset = self.env['purchase.order'].search([('name','=',picking_rset.origin)],limit=1)
                            
                            if purchase_rset:
                                sheet.write(row, 2, str(purchase_rset.partner_id.name), format3)
                                sheet.write(row, 3, str(purchase_rset.amount_total), format3)
                                fc_value_grand_total += purchase_rset.amount_total

                                sheet.write(row, 4, str(purchase_rset.currency_id.name), format3)
                                invoice_rset = self.env['account.move'].search([('id','in',purchase_rset.invoice_ids.ids),('state','=','posted'),('type','=','in_invoice')],limit=1)
                                if invoice_rset:
                                    currency_rset = self.env['res.currency'].search([('id','=',invoice_rset.currency_id.id)],limit=1)
                                    if currency_rset:
                                        # _logger.info("===========================ccccccc=============%s",self.env.company.currency_id)
                                        if self.env.company.currency_id == invoice_rset.currency_id:
                                            exchange_rate = 1
                                            # _logger.info("==================here===there========%s",exchange_rate)
                                        else:

                                            currency_rate_rset = self.env['res.currency.rate'].search([('name','=',invoice_rset.date)],limit=1)
                                            if currency_rate_rset:
                                                exchange_rate = round(1/currency_rate_rset.rate,2)
                                                sheet.write(row, 5, str(exchange_rate), format3)
                                           
                                        for line_rset in invoice_rset.invoice_line_ids:
                                            invoice_qty_count += line_rset.quantity
                                        sheet.write(row, 1, str(line_rset.ref), format3)
                                        
                                        sheet.write(row, 6, str(invoice_qty_count), format3)
                                        invoice_qty_grand_total += invoice_qty_count
                                        basic_amount = round(exchange_rate * invoice_rset.amount_total,2)
                                        basic_amount_grand_total += basic_amount
                                        sheet.write(row, 7, str(basic_amount), format3)
                                        # sheet.write(row, 8, invoice_rset.amount_discount, format3)
                                        invoice_grand_total += basic_amount
                            col=8
                            for lc in landed_cost_product_lst:
                                sum_lc_pdt = 0
                                ln1 = self.env['stock.landed.cost.lines'].search([('cost_id','=',rec.id),('product_id','=',lc)])     
                                if ln1:
                                    for ln in ln1:
                                        sum_lc_pdt += ln.price_unit        
                                    sheet.write(row, col, sum_lc_pdt, format3)
                                    landed_cost_lst.append({'col':col,'sum_lc_pdt':sum_lc_pdt})
                                    col +=1
                                else:
                                    landed_cost_lst.append({'col':col,'sum_lc_pdt':sum_lc_pdt})
                                    col +=1
                                    # landed_cost_grand_total += sum_lc_pdt
                                   


                    invoice_grand_total += rec.amount_total
                    sheet.write(15, col, "Total Landed Cost", format1)
                    sheet.write(row, col, invoice_grand_total, format3)
                    final_invoice_grand_total += invoice_grand_total
                    row = row + 1
                row1 = row + 1
                
                sheet.write(row1, 2, 'Grand Total', format1)
                sheet.write(row1, 3,fc_value_grand_total , format3)
                sheet.write(row1, 6,invoice_qty_grand_total , format3)
                sheet.write(row1, 7,basic_amount_grand_total , format3)
                sheet.write(row1, col,final_invoice_grand_total , format3)
                # _logger.info("====================llllllllllllst===========%s",landed_cost_lst)
                def sum_by_common_key(input_list, index_key='col'):
                    output_dict = {}
                    for d in input_list:
                        index = d[index_key]
                        if index not in output_dict:
                            output_dict[index] = {}
                        for k, v in d.items():
                            if k not in output_dict[index]:
                                output_dict[index][k] = v
                            elif k != index_key:
                                output_dict[index][k] += v
                    return output_dict.values()
                fnl_lst = sum_by_common_key(landed_cost_lst)
                # _logger.info("====================lst===========%s",fnl_lst)
                for dict_item in fnl_lst:
                    # _logger.info("=====================dict===============%s",dict_item)
                    # _logger.info("====================rowcol===========%s%s",row1,dict_item['col'])
                    sheet.write(row1, dict_item['col'],str(dict_item['sum_lc_pdt']) , format3)

                    
               
                    
            
            path = ("/home/odoo/Reports/Landed_Cost_Report.xls")
            workbook.save(path)
            file = open(path, "rb")
            file_data = file.read()
            out = base64.encodestring(file_data)
            self.write({'state': 'get', 'file_name': out, 'landedcost_data':'Landed_Cost_Report.xls'})
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'landed.cost.consigment.report',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': self.id,
                'target': 'new',
                }                
        else:
            raise Warning("Currently No LandedCost For This Data!!")


class LandedCostConsignmentDetailReport(models.TransientModel):
    _name = "landed.cost.consigment.detail.report"
    
    start_date = fields.Date(string='Start Date', required=True, default=datetime.today().replace(day=1))
    end_date = fields.Date(string="End Date", required=True, default=datetime.now().replace(day = calendar.monthrange(datetime.now().year, datetime.now().month)[1]))
    
    landedcost_detail_data = fields.Char('Name',)
    file_name = fields.Binary('Landed Cost Detail Excel Report', readonly=True)
    state = fields.Selection([('choose', 'choose'), ('get', 'get')],
                             default='choose')
    partner_id = fields.Many2many('res.partner', string='Vendor')
    origin = fields.Many2many('purchase.order', string='Source Document')

    _sql_constraints = [('check','CHECK((start_date <= end_date))',"End date must be greater then start date")]

    # @api.onchange('partner_id')
    # def _onchange_partner_id(self):
    #     purchase_list = []
    #     obj = self.env['purchase.order'].search([('partner_id', 'in', self.partner_id.ids)])
    #     if obj:
    #         for lst in obj:
    #             purchase_list.append(lst.id)
    #         self.origin = [(6,0,purchase_list)]

    # def action_landed_cost_consignment_detail_report(self):
    #     workbook = xlwt.Workbook()
        
        
    #     ldata = self.env['stock.landed.cost'].search([('date', '>=', self.start_date), ('date', '<=', self.end_date), 
    #                                             ('state', '=', 'done')])
    #     sheet = workbook.add_sheet('LandedCost001',cell_overwrite_ok=True)
    #     sheet.col(0).width = int(15*260)
    #     sheet.col(1).width = int(15*260)    
    #     sheet.col(2).width = int(30*260)    
    #     sheet.col(3).width = int(15*260) 
    #     sheet.col(4).width = int(15*260)   
    #     sheet.col(5).width = int(15*260)
    #     sheet.col(6).width = int(15*260)
    #     sheet.col(7).width = int(15*260)
    #     sheet.col(8).width = int(15*260)
    #     sheet.col(9).width = int(15*260)
    #     sheet.col(10).width = int(15*260)
    #     sheet.col(11).width = int(15*260)
    #     sheet.col(12).width = int(15*260)
    #     sheet.col(13).width = int(15*260)
    #     sheet.col(14).width = int(15*260)
    #     sheet.col(15).width = int(15*260)
    #     sheet.col(16).width = int(15*260)
    #     sheet.col(17).width = int(15*260)
    #     sheet.col(18).width = int(15*260)
    #     sheet.col(19).width = int(15*260)
    #     sheet.col(20).width = int(15*260)
    #     sheet.col(21).width = int(15*260)
    #     sheet.col(22).width = int(15*260)
    #     sheet.col(23).width = int(15*260)
    #     sheet.col(24).width = int(15*260)



    #     format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour gray25;align: horiz center')
    #     format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
    #     format11 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour yellow;align: horiz left')

    #     format2 = xlwt.easyxf('font:bold True;align: horiz left')
    #     format3 = xlwt.easyxf('align: horiz left')
    #     format4 = xlwt.easyxf('align: horiz right')
    #     format5 = xlwt.easyxf('font:bold True;align: horiz right')
    #     format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz right')
    #     format7 = xlwt.easyxf('font:bold True;borders:top thick;align: horiz right')
    #     format8 = xlwt.easyxf('font:bold True;borders:top thick;pattern: pattern solid, fore_colour gray25;align: horiz left')

    #     sheet.write_merge(0, 2, 0, 9, 'Landed Cost Report' , format0)
      

    #     record = []
    #     if ldata:
    #         for rec in ldata:
    #             record.append(rec)
              
    #         file = StringIO()        
    #         self.record= record.sort(key=lambda p: (p.date, p.name))
    #         final_value = {}
           
            
    #         timestamp = str(datetime.today())
    #         row=16
    #         p_row = row + 1
    #         sheet.write(5, 0, "Company:", format1)
    #         sheet.write(5, 1, str(rec.company_id.name), format2)
    #         sheet.write(6, 0, 'Report generated at:', format1)
    #         sheet.write(6, 1, timestamp, format2)
    #         # sheet.write(7, 0, "Seller Address:", format1)
    #         # sheet.write(7, 1, str(rec.company_id.street), format2)
    #         # sheet.write(8, 0, "Duration of sales", format1)
    #         # sheet.write(8, 1, str(self.start_date), format2)
    #         # sheet.write(8, 2, 'to', format2)
    #         # sheet.write(8, 3, str(self.end_date), format2)
    #         # sheet.write(10, 1, 'Report generated at:', format2)
    #         # sheet.write(10, 2, timestamp, format2)
            
    #         sheet.write(15, 0, 'Date', format1)
    #         sheet.write(15, 1, 'Invoice', format1)
    #         sheet.write(15, 2, 'Vendor Name', format1)
    #         sheet.write(15, 3, 'Product Name', format1)
    #         sheet.write(15, 4, 'FC Value', format1)
    #         sheet.write(15, 5, 'Currency', format1)
    #         # sheet.write(15, 5, 'Exchange Rate', format1)
    #         sheet.write(15, 6, 'Qty', format1)
    #         sheet.write(15, 7, 'Rate', format1)

    #         sheet.write(15, 8, 'Purchase Amount', format1)
    #         sheet.write(15, 9, 'Discount', format1)
    #         col=8
    #         landed_product_rset = self.env['product.product'].search([('landed_cost_ok','=',True)])
    #         landed_cost_product_lst = []
    #         for landed_product in landed_product_rset:                    
    #             sheet.write(15, col, landed_product.name, format11)
    #             col += 1
    #             landed_cost_product_lst.append(landed_product.id)

    #         if record:
    #             fc_value_grand_total = 0
    #             invoice_qty_grand_total = 0
    #             basic_amount_grand_total = 0
    #             landed_cost_lst =[]
    #             final_invoice_grand_total = 0
    #             for rec in record:
    #                 invoice_qty_count = 0

    #                 invoice_grand_total = 0
    #                 landed_cost_grand_total = 0
    #                 sheet.write(row, 0, str(rec.date), format3)
    #                 picking_rset = self.env['stock.picking'].search([('id','in',rec.picking_ids.ids)],limit=1)
    #                 if picking_rset:
    #                     if picking_rset.origin:
    #                         purchase_rset = self.env['purchase.order'].search([('name','=',picking_rset.origin)],limit=1)
                            
    #                         if purchase_rset:
    #                             sheet.write(row, 1, str(purchase_rset.partner_ref), format3)
    #                             sheet.write(row, 2, str(purchase_rset.partner_id.name), format3)

    #                             sheet.write(row, 4, str(purchase_rset.amount_total), format3)
    #                             fc_value_grand_total += purchase_rset.amount_total

    #                             sheet.write(row, 5, str(purchase_rset.currency_id.name), format3)
    #                             invoice_rset = self.env['account.move'].search([('id','in',purchase_rset.invoice_ids.ids),('state','=','posted'),('type','=','in_invoice')],limit=1)
    #                             if invoice_rset:
    #                                 currency_rset = self.env['res.currency'].search([('id','=',invoice_rset.currency_id.id)],limit=1)
    #                                 if currency_rset:
    #                                     # _logger.info("===========================ccccccc=============%s",self.env.company.currency_id)
    #                                     if self.env.company.currency_id == invoice_rset.currency_id:
    #                                         exchange_rate = 1
    #                                         # _logger.info("==================here===there========%s",exchange_rate)
    #                                     else:

    #                                         currency_rate_rset = self.env['res.currency.rate'].search([('name','=',invoice_rset.date)],limit=1)
    #                                         if currency_rate_rset:
    #                                             exchange_rate = round(1/currency_rate_rset.rate,2)
                                           
    #                                 for line_rset in invoice_rset.invoice_line_ids:
    #                                     invoice_qty_count += line_rset.quantity
    #                                 sheet.write(row, 8, str(invoice_qty_count), format3)
    #                                 basic_amount = round(exchange_rate * invoice_rset.amount_total,2)

    #                                 invoice_qty_grand_total += invoice_qty_count
    #                                 basic_amount_grand_total += basic_amount
    #                                 sheet.write(p_row, 9, str(basic_amount), format3)
    #                                 # sheet.write(row, 8, invoice_rset.amount_discount, format3)
    #                                 invoice_grand_total += basic_amount


    #                             purchase_qty_grand_total = 0
    #                             basic_amount_grand_total = 0 
    #                             custom_lst = []
    #                             # custom_line_lst = []
    #                             # fnll_list = []
    #                             # n_row = p_row
    #                             for purchase_line_rset in purchase_rset.order_line:
    #                                 l_col = 8

    #                                 sheet.write(p_row, 0, str(purchase_line_rset.product_id.default_code), format3)

    #                                 sheet.write(p_row, 3, str(purchase_line_rset.product_id.name), format3)
                                    
    #                                 sheet.write(p_row, 6, str(purchase_line_rset.product_qty), format3)
    #                                 sheet.write(p_row, 7, purchase_line_rset.price_unit, format3)
    #                                 basic_amount_line = round(purchase_line_rset.product_qty * purchase_line_rset.price_unit,2)
    #                                 sheet.write(p_row, 8,basic_amount_line , format3)
    #                                 purchase_qty_grand_total += purchase_line_rset.product_qty
    #                                 basic_amount_grand_total += basic_amount_line
    #                                 sheet.write(row, 6,purchase_qty_grand_total , format3)
    #                                 sheet.write(row, 7,basic_amount_grand_total , format3)
    #                                 for eval_line in rec.valuation_adjustment_lines:
    #                                     if purchase_line_rset.product_id.id == eval_line.product_id.id:
                                            
    #                                         custom_lst.append({
    #                                                    'lc_product':  eval_line.cost_line_id.product_id.id,
    #                                                    'additional_cost': eval_line.additional_landed_cost,
                                                        
    #                                             })
                                    
    #                                 for ldc in landed_cost_product_lst:

    #                                     for dec in custom_lst:
                                            
    #                                         # _logger.info("========a======a==========a==========a%s====%s",dec['lc_product'],ldc)
                                            
    #                                         if dec['lc_product'] == ldc:
    #                                             # _logger.info("========a======a==========a==========a%s====%s",dec['lc_product'],dec['additional_cost'])
    #                                             sheet.write(p_row, l_col,dec['additional_cost'] , format3)
    #                                     l_col += 1
                                        

    #                                         # sheet.write(p_row, l_col,custom_lst , format3)
                                            
    #                                         # fnll_list.append({
    #                                         #        'product_id':custom_lst['product_id'][0],
    #                                         #        'lc_product': custom_line_lst['lc_product'][0],
    #                                         #        'additional_cost': custom_line_lst['additional_cost'][0]

    #                                         # })

    #                                         # _logger.info("=========================sdfsdfs==========444========%s",custom_lst)
    #                                         # _logger.info("=========================sdfsdfs==========4447777========%s",custom_line_lst)
                                            

                                    
    #                             # _logger.info("------------------------sdfsdfsdfsdf----------------------%s",fnll_list)

                                    


                                    



    #                                 p_row += 1

                                    

    #                         col=8
    #                         col2=col
    #                         # val_row = row + 1
    #                         final_list = []
    #                         for lc in landed_cost_product_lst:
    #                             sum_lc_pdt = 0
    #                             ln1 = self.env['stock.landed.cost.lines'].search([('cost_id','=',rec.id),('product_id','=',lc)])     
    #                             if ln1:
    #                                 for ln in ln1:
    #                                     sum_lc_pdt += ln.price_unit

                                        
    #                                 sheet.write(row, col, sum_lc_pdt, format3)

    #                                 landed_cost_lst.append({'col':col,'sum_lc_pdt':sum_lc_pdt})
    #                                 col +=1
    #                             else:
    #                                 landed_cost_lst.append({'col':col,'sum_lc_pdt':sum_lc_pdt})
    #                                 col +=1
    #                                 # landed_cost_grand_total += sum_lc_pdt
                                
                                     

                                

                                







    #                 invoice_grand_total += rec.amount_total
    #                 sheet.write(15, col, "Total Landed Cost", format1)
    #                 sheet.write(row, col, invoice_grand_total, format3)
    #                 final_invoice_grand_total += invoice_grand_total
    #                 row = p_row + 1

                    
    #             row1 = row + 1
                
    #             sheet.write(row1, 2, 'Grand Total', format1)
    #             sheet.write(row1, 3,fc_value_grand_total , format3)
    #             sheet.write(row1, 6,invoice_qty_grand_total , format3)
    #             sheet.write(row1, 7,basic_amount_grand_total , format3)
    #             sheet.write(row1, col,final_invoice_grand_total , format3)
    #             # .info("====================llllllllllllst===========%s",landed_cost_lst)
    #             def sum_by_common_key(input_list, index_key='col'):
    #                 output_dict = {}
    #                 for d in input_list:
    #                     index = d[index_key]
    #                     if index not in output_dict:
    #                         output_dict[index] = {}
    #                     for k, v in d.items():
    #                         if k not in output_dict[index]:
    #                             output_dict[index][k] = v
    #                         elif k != index_key:
    #                             output_dict[index][k] += v
    #                 return output_dict.values()
    #             fnl_lst = sum_by_common_key(landed_cost_lst)
    #             # _logger.info("====================lst===========%s",fnl_lst)
    #             for dict_item in fnl_lst:
    #                 # _logger.info("=====================dict===============%s",dict_item)
    #                 # _logger.info("====================rowcol===========%s%s",row1,dict_item['col'])
    #                 sheet.write(row1, dict_item['col'],str(dict_item['sum_lc_pdt']) , format3)


    #         path = ("/home/odoo/Reports/Landed_Cost_Report_Detail.xls")
    #         workbook.save(path)
    #         file = open(path, "rb")
    #         file_data = file.read()
    #         out = base64.encodestring(file_data)
    #         self.write({'state': 'get', 'file_name': out, 'landedcost_detail_data':'Landed_Cost_Report_Detail.xls'})
    #         return {
    #             'type': 'ir.actions.act_window',
    #             'res_model': 'landed.cost.consigment.detail.report',
    #             'view_mode': 'form',
    #             'view_type': 'form',
    #             'res_id': self.id,
    #             'target': 'new',
    #             }                
    #     else:
    #         raise Warning("Currently No LandedCost For This Data!!")

    def action_landed_cost_consignment_detail_report(self):
        workbook = xlwt.Workbook()        
        
        if self.partner_id and not self.origin:
            ldata = self.env['stock.landed.cost'].search([('date', '>=', self.start_date), ('date', '<=', self.end_date), ('picking_ids.partner_id', 'in', self.partner_id.ids),
                                                    ('state', '=', 'done')])
            _logger.info('=================%s', ldata)

        elif self.origin and not self.partner_id:
            origin_list = []
            obj = self.env['purchase.order'].search([('id', 'in', self.origin.ids)])
            if obj:
                for lst in obj:
                    origin_list.append(lst.name)
            _logger.info('==================%s', origin_list)
            ldata = self.env['stock.landed.cost'].search([('date', '>=', self.start_date), ('date', '<=', self.end_date), ('picking_ids.origin', 'in', origin_list),
                                                    ('state', '=', 'done')])
        elif self.origin and self.partner_id:
            origin_list = []
            obj = self.env['purchase.order'].search([('id', 'in', self.origin.ids)])
            if obj:
                for lst in obj:
                    origin_list.append(lst.name)
            _logger.info('==================%s', origin_list)
            ldata = self.env['stock.landed.cost'].search([('date', '>=', self.start_date), ('date', '<=', self.end_date), ('picking_ids.origin', 'in', origin_list), ('picking_ids.partner_id', 'in', self.partner_id.ids),
                                                    ('state', '=', 'done')])

        else:
            ldata = self.env['stock.landed.cost'].search([('date', '>=', self.start_date), ('date', '<=', self.end_date),
                                                    ('state', '=', 'done')])

        sheet = workbook.add_sheet('LandedCost001',cell_overwrite_ok=True)
        sheet.col(0).width = int(15*260)
        sheet.col(1).width = int(15*260)    
        sheet.col(2).width = int(30*260)    
        sheet.col(3).width = int(15*260) 
        sheet.col(4).width = int(15*260)   
        sheet.col(5).width = int(15*260)
        sheet.col(6).width = int(15*260)
        sheet.col(7).width = int(15*260)
        sheet.col(8).width = int(15*260)
        sheet.col(9).width = int(15*260)
        sheet.col(10).width = int(15*260)
        sheet.col(11).width = int(15*260)
        sheet.col(12).width = int(15*260)
        sheet.col(13).width = int(15*260)
        sheet.col(14).width = int(15*260)
        sheet.col(15).width = int(15*260)
        sheet.col(16).width = int(15*260)
        sheet.col(17).width = int(15*260)
        sheet.col(18).width = int(15*260)
        sheet.col(19).width = int(15*260)
        sheet.col(20).width = int(15*260)
        sheet.col(21).width = int(15*260)
        sheet.col(22).width = int(15*260)
        sheet.col(23).width = int(15*260)
        sheet.col(24).width = int(15*260)



        format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour gray25;align: horiz center')
        format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
        format11 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour yellow;align: horiz left')

        format2 = xlwt.easyxf('font:bold True;align: horiz left')
        format3 = xlwt.easyxf('align: horiz left')
        format4 = xlwt.easyxf('align: horiz right')
        format5 = xlwt.easyxf('font:bold True;align: horiz right')
        format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz right')
        format7 = xlwt.easyxf('font:bold True;borders:top thick;align: horiz right')
        format8 = xlwt.easyxf('font:bold True;borders:top thick;pattern: pattern solid, fore_colour gray25;align: horiz left')

        sheet.write_merge(0, 2, 0, 9, 'Landed Cost Report Product Wise' , format0)
      

        record = []
        if ldata:
            for rec in ldata:
                record.append(rec)
              
            file = StringIO()        
            self.record= record.sort(key=lambda p: (p.date, p.name))
            final_value = {}
           
            
            timestamp = str(datetime.today())
            row=16
            sheet.write(5, 0, "Company:", format1)
            sheet.write(5, 1, str(rec.company_id.name), format2)
            sheet.write(6, 0, 'Report generated at:', format1)
            sheet.write(6, 1, timestamp, format2)
           
            sheet.write(15, 0, 'Date', format1)
            sheet.write(15, 1, 'Invoice', format1)
            sheet.write(15, 2, 'Vendor Name', format1)
            sheet.write(15, 3, 'Product Name', format1)
            # sheet.write(15, 4, 'FC Value', format1)
            sheet.write(15, 4, 'Currency', format1)
            sheet.write(15, 5, 'Qty', format1)
            sheet.write(15, 6, 'Exchange Rate', format1)
            sheet.write(15, 7, 'Unit Rate', format1)
            sheet.write(15, 8, 'Purchase Amount', format1)
            sheet.write(15, 9, 'Local Rate', format1)
            col=10
            landed_product_rset = self.env['product.product'].search([('landed_cost_ok','=',True)],order="lc_seq asc")
            landed_cost_product_lst = []
            for landed_product in landed_product_rset:                    
                sheet.write(15, col, landed_product.name, format11)
                col += 1
                landed_cost_product_lst.append(landed_product.id)
            if record:
                
                for rec in record:
                    lc_line_total = []
                    
                    sheet.write(row, 0, str(rec.date), format3)
                    picking_rset = self.env['stock.picking'].search([('id','in',rec.picking_ids.ids)],limit=1)
                    if picking_rset:
                        if picking_rset.origin:
                            purchase_rset = self.env['purchase.order'].search([('name','=',picking_rset.origin)],limit=1)
                            
                            if purchase_rset:
                                
                                product_lc_qty_total = 0
                                # product_lc_basic_amt_total = 0
                                basic_amount_local_line_total = 0
                                product_lc_basic_amt_total = 0
                                product_lc_grand_total = 0
                                product_cost_per_unit_grand_total = 0
                                product_lc_local_amt_total = 0
                                invoice_rset = self.env['account.move'].search([('id','in',purchase_rset.invoice_ids.ids),('state','=','posted'),('type','=','in_invoice')],limit=1)
                                if invoice_rset:
                                    currency_rset = self.env['res.currency'].search([('id','=',invoice_rset.currency_id.id)],limit=1)
                                    if currency_rset:
                                        # _logger.info("===========================ccccccc=============%s",self.env.company.currency_id)
                                        if self.env.company.currency_id == invoice_rset.currency_id:
                                            exchange_rate = 1
                                        else:
                                            currency_rate_rset = self.env['res.currency.rate'].search([('name','=',invoice_rset.date)],limit=1)
                                            if currency_rate_rset:
                                                exchange_rate = round(1/currency_rate_rset.rate,2)
                                sheet.write(row, 1, str(invoice_rset.ref), format3)
                                sheet.write(row, 2, str(invoice_rset.partner_id.name), format3)
                                row += 1

                                for line_rset in invoice_rset.invoice_line_ids:
                                    product_lc_total = 0

                                    _logger.info("======================1==========%s",product_lc_total)
                                    sheet.write(row, 3, str(line_rset.product_id.name), format3)
                                    sheet.write(row, 5, str(line_rset.quantity), format3)
                                    product_lc_qty_total += line_rset.quantity
                                    sheet.write(row, 6, str(exchange_rate), format3)
                                    basic_amount_line = round(line_rset.quantity * line_rset.price_unit,2)

                                    basic_local_total = round(exchange_rate * basic_amount_line,2)
                                    product_lc_basic_amt_total += basic_amount_line
                                    product_lc_total += basic_local_total
                                    _logger.info("======================2==========%s",product_lc_total)

                                    sheet.write(row, 7,line_rset.price_unit, format3)
                                    sheet.write(row, 8,basic_amount_line , format3)
                                    basic_amount_local_line = round(exchange_rate * basic_amount_line,2)
                                    product_lc_local_amt_total += basic_amount_local_line
                                    sheet.write(row, 9,basic_local_total , format3)
                                    basic_amount_local_line_total += basic_local_total
                                    for eval_line in rec.valuation_adjustment_lines:
                                        custom_lst = []
                                        if line_rset.product_id.id == eval_line.product_id.id:   
                                            custom_lst.append({
                                                        'lc_product':  eval_line.cost_line_id.product_id.id,
                                                        'additional_cost': eval_line.additional_landed_cost,
                                                        
                                                })
                                        col1=10
                                        for ldc in landed_cost_product_lst:
                                            _logger.info("======================3==========%s",product_lc_total)
                                            
                                            for dec in custom_lst:
                                                if dec['lc_product'] == ldc:
                                                    sheet.write(row,col1,dec['additional_cost'] , format3)
                                                    product_lc_total += dec['additional_cost']
                                                    _logger.info("======================4==========%s",product_lc_total)

                                            col1 += 1
                                        sheet.write(15, col1, "Total Landed Cost", format1)
                                        sheet.write(row, col1, product_lc_total, format1)
                                        sheet.write(15, col1 + 1, "Cost Per Unit", format1)
                                        if line_rset.quantity == 0:
                                            sheet.write(row, col1 + 1, product_lc_total, format1)
                                        else:
                                            sheet.write(row, col1 + 1, round(product_lc_total/line_rset.quantity, 2), format1)

                                    product_lc_grand_total += product_lc_total
                                    if line_rset.quantity == 0:
                                        product_cost_per_unit_grand_total += product_lc_total
                                    else:
                                        product_cost_per_unit_grand_total += round(product_lc_total/line_rset.quantity,2)




                                









                                    row += 1
                    # sheet.write(row, 4, str(invoice_rset.amount_total), format3)
                    sheet.write(row, 4, str(invoice_rset.currency_id.name), format3)
                    # sheet.write(row, 4, str(invoice_rset.amount_total), format3)
                    sheet.write(row, 5, product_lc_qty_total, format1)
                    sheet.write(row, 8, product_lc_basic_amt_total, format1)
                    sheet.write(row, 9, product_lc_local_amt_total, format1)
                    sheet.write(row, col1, product_lc_grand_total, format1)
                    sheet.write(row, col1+1, product_cost_per_unit_grand_total, format1)
                    col2 =10
                    for lc in landed_cost_product_lst:
                        sum_lc_pdt = 0
                        ln1 = self.env['stock.landed.cost.lines'].search([('cost_id','=',rec.id),('product_id','=',lc)])     
                        if ln1:
                            for ln in ln1:
                                sum_lc_pdt += ln.price_unit

                                
                            sheet.write(row, col2, sum_lc_pdt, format3)

                            lc_line_total.append({'col':col2,'sum_lc_pdt':sum_lc_pdt})
                            col2 +=1
                        else:
                            lc_line_total.append({'col':col2,'sum_lc_pdt':sum_lc_pdt})
                            col2 +=1
                    _logger.info("==========================c=========%s",lc_line_total)
                    def sum_by_common_key(input_list, index_key='col'):
                        output_dict = {}
                        for d in input_list:
                            index = d[index_key]
                            if index not in output_dict:
                                output_dict[index] = {}
                            for k, v in d.items():
                                if k not in output_dict[index]:
                                    output_dict[index][k] = v
                                elif k != index_key:
                                    output_dict[index][k] += v
                        return output_dict.values()
                    fnl_lst = sum_by_common_key(lc_line_total)
                    for dict_item in fnl_lst:
                        sheet.write(row, dict_item['col'],str(dict_item['sum_lc_pdt']) , format1)
                    row += 1
                

                                    





            

            path = ("/home/odoo/Reports/Landed_Cost_Report_Detail.xls")
            workbook.save(path)
            file = open(path, "rb")
            file_data = file.read()
            out = base64.encodestring(file_data)
            self.write({'state': 'get', 'file_name': out, 'landedcost_detail_data':'Landed_Cost_Report_Detail.xls'})
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'landed.cost.consigment.detail.report',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': self.id,
                'target': 'new',
                }                
        else:
            raise Warning("Currently No LandedCost For This Data!!")

    def open_lc_detail_table(self):
        report_search = self.env['landed.cost.report.pivot'].search([])
        if report_search:
            report_search.unlink()
        tree_view_id = self.env.ref('landed_cost_consignment_report.view_landed_consignment_report_pivot_detail_tree').id
        form_view_id = self.env.ref('landed_cost_consignment_report.view_landed_consignment_report_pivot_detail_form').id
        # graph_view_id = self.env.ref('agent_aged_pivot_report.view_aged_report_graph').id
        pivot_view_id = self.env.ref('landed_cost_consignment_report.view_landed_consignment_report_pivot_pivot_detail').id
        
        
        ldata = self.env['stock.landed.cost'].search([('date', '>=', self.start_date), ('date', '<=', self.end_date), 
                                                ('state', '=', 'done')])
        record = []
        if ldata:
            for rec in ldata:
                record.append(rec)
            self.record= record.sort(key=lambda p: (p.date, p.name))
           
            landed_product_rset = self.env['product.product'].search([('landed_cost_ok','=',True)],order="lc_seq asc")
            landed_cost_product_lst = []
            for landed_product in landed_product_rset:                    
                landed_cost_product_lst.append(landed_product.id)
            
            for rec in record:
                    lc_line_total = []
                    r1 = self.env['landed.cost.report.pivot.detail'].create({
                        'date':rec.date,
                    })
                    
                    picking_rset = self.env['stock.picking'].search([('id','in',rec.picking_ids.ids)],limit=1)
                    if picking_rset:
                        if picking_rset.origin:
                            purchase_rset = self.env['purchase.order'].search([('name','=',picking_rset.origin)],limit=1)
                            
                            if purchase_rset:
                                custom_lst = []
                                product_lc_total = 0
                                product_lc_qty_total = 0
                                # product_lc_basic_amt_total = 0
                                basic_amount_local_line_total = 0
                                product_lc_basic_amt_total = 0
                                product_lc_grand_total = 0
                                product_lc_local_amt_total = 0
                                invoice_rset = self.env['account.move'].search([('id','in',purchase_rset.invoice_ids.ids),('state','=','posted'),('type','=','in_invoice')],limit=1)
                                if invoice_rset:
                                    currency_rset = self.env['res.currency'].search([('id','=',invoice_rset.currency_id.id)],limit=1)
                                    if currency_rset:
                                        # _logger.info("===========================ccccccc=============%s",self.env.company.currency_id)
                                        if self.env.company.currency_id == invoice_rset.currency_id:
                                            exchange_rate = 1
                                        else:
                                            currency_rate_rset = self.env['res.currency.rate'].search([('name','=',invoice_rset.date)],limit=1)
                                            if currency_rate_rset:
                                                exchange_rate = round(1/currency_rate_rset.rate,2)
                                r1.update({
                                    'invoice_ref': invoice_rset.ref,
                                    'partner_id': invoice_rset.partner_id.id
                                })

                                # sheet.write(row, 2, str(invoice_rset.partner_id.name), format3)
                                # row += 1

                                for line_rset in invoice_rset.invoice_line_ids:
                                    r1.update({
                                    'product_id': line_rset.product_id.id,
                                    'qty': line_rset.quantity,
                                    'rate': exchange_rate
                                        })  
                                    # sheet.write(row, 3, str(line_rset.product_id.name), format3)
                                    # sheet.write(row, 5, str(line_rset.quantity), format3)
                                    product_lc_qty_total += line_rset.quantity
                                    # sheet.write(row, 6, str(exchange_rate), format3)
                                    basic_amount_line = round(line_rset.quantity * line_rset.price_unit,2)

                                    basic_local_total = round(exchange_rate * line_rset.price_unit,2)
                                    product_lc_basic_amt_total += basic_amount_line
                                    product_lc_total += basic_amount_line
                                    r1.update({
                                    'unit_price': line_rset.price_unit,
                                        }) 
                                    # sheet.write(row, 7,line_rset.price_unit, format3)
                                    # sheet.write(row, 8,basic_amount_line , format3)
                                    # basic_amount_local_line = round(exchange_rate * line_rset.price_unit,2)
                                    # product_lc_local_amt_total += basic_amount_local_line
                                    # sheet.write(row, 9,basic_local_total , format3)
                                    # basic_amount_local_line_total += basic_local_total
                                    for eval_line in rec.valuation_adjustment_lines:
                                        if line_rset.product_id.id == eval_line.product_id.id:   
                                            custom_lst.append({
                                                        'lc_product':  eval_line.cost_line_id.product_id.id,
                                                        'additional_cost': eval_line.additional_landed_cost,
                                                        
                                                })
                                    col1=10
                                    for ldc in landed_cost_product_lst:
                                        
                                        for dec in custom_lst:
                                            if dec['lc_product'] == ldc:
                                                r1.update({
                                                'additional_landed_cost': dec['additional_cost'],
                                                    }) 
                                                # sheet.write(row,col1,dec['additional_cost'] , format3)
                                                product_lc_total += dec['additional_cost']
                                        col1 += 1
                                    # sheet.write(15, col1, "Total Landed Cost", format1)
                                    # sheet.write(row, col1, product_lc_total, format1)
                                    # product_lc_grand_total += product_lc_total


                                    # row += 1
                    # sheet.write(row, 4, str(invoice_rset.amount_total), format3)
                    r1.update({
                                                'currency_id': invoice_rset.currency_id.id,
                                                    })
                    # sheet.write(row, 4, str(invoice_rset.currency_id.name), format3)
                    # sheet.write(row, 4, str(invoice_rset.amount_total), format3)
                    # sheet.write(row, 5, product_lc_qty_total, format1)
                    # sheet.write(row, 8, product_lc_basic_amt_total, format1)
                    # sheet.write(row, 9, product_lc_local_amt_total, format1)
                    # sheet.write(row, 17, product_lc_grand_total, format1)
                    # col2 =10
                    
                                  
                                   

        action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (pivot_view_id, 'pivot'),
                    (tree_view_id, 'tree'), 
                    (form_view_id, 'form'),
                   
                ],
                'view_mode': 'tree,form',
                'name': _('LC Consignment Report Detail'),
                'res_model': 'landed.cost.report.pivot.detail',
                'context': {'group_by': ['product_id']}
            }
        return action