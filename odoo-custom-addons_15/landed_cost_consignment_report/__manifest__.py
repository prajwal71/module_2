# -*- coding: utf-8 -*-
{
    'name': 'Landed Cost Consignment Report',
    'version': '13.0',
    'category': 'Accounts',
    'summary': ''' Landed Cost Consignment Report''',
    'author': 'Nexus Incorporation Pvt Ltd.',
    'license': "OPL-1",
    'depends': [
        'base',
        'account',
        'purchase',
        'stock_landed_costs','product',
    ],
    'data': [
        'security/ir.model.access.csv',
        'wizard/landed_cost_report_view.xml',
        'wizard/landed_cost_report_pivot_view.xml',
        'wizard/landed_cost_report_pivot_view_detail.xml',
        
    ],
    'demo': [],  
    'auto_install': False,
    'installable': True,
    'application': True
}
