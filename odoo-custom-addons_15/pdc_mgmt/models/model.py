from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning
from datetime import date, datetime
import logging
_logger = logging.getLogger(__name__)

class AccountPDCPayment(models.Model):
    _name = "account.mypdc.payment"
    _description = 'PDC Payments'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    @api.model
    def create(self, vals):
        if 'company_id' in vals:
            seq = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code(
                'account.mypdc.payment') or '/'
            vals['name'] = seq
        else:
            seq = self.env['ir.sequence'].next_by_code('account.mypdc.payment') or '/'
            vals['name'] = seq
        return super(AccountPDCPayment, self).create(vals)

    def unlink(self):
        if self.state != 'draft':
            raise UserError(_("You cannot delete a payment that is already posted."))

    name = fields.Char('Name', readonly=True)
    # cheque_reference = fields.Char(copy=False)
    effective_date = fields.Date('Effective Date', help='Effective date of PDC', copy=False, default=False,
                                 required=True, track_visibility='onchange')
    date = fields.Datetime('Entry Date', help=' date of PDC', copy=False, default=datetime.today(), required=True)
    bank_references = fields.Char(string='Bank Refrence', copy=False)
    cheque_references = fields.Char(string='Cheque Refrence', required=True, copy=False)
    is_pdc_cheque = fields.Boolean('PDC Cheque', default=True)
    payment_type = fields.Selection([('outbound', 'Send Money'), ('inbound', 'Receive Money')], string='Payment Type',
                                    default='inbound', required=True)
    partner_type = fields.Selection([('customer', 'Customer'), ('supplier', 'Vendor')])
    partner_id = fields.Many2one('res.partner', string='Partner')
    user_id_collector = fields.Many2one('res.users', string='Collected By')
    user_id_encasher = fields.Many2one('res.users', string='Encashed by')
    amount = fields.Monetary(string='Payment Amount', required=True)
    currency_id = fields.Many2one('res.currency', string='Currency', required=True,
                                  default=lambda self: self.env.user.company_id.currency_id.id)
    ref = fields.Char(string='Memo')
    b_cnt = fields.Integer(string='Bounce Count', default=0, readonly=True)
    account_move_id_pdc = fields.Many2one('account.move', 'PDC Entry', copy=False, readonly=True,
                                          track_visibility='onchange')
    reason_cancel = fields.Many2one('cancel.reason', track_visibility='onchange')
    journal_id = fields.Many2one('account.journal', string='Payment Journal', required=True,
                                 domain=[('type', 'in', ('bank', 'cash'))])
    account_id_dr = fields.Many2one('account.account', 'Debit PDC Account', required=True,
                                    domain=[('deprecated', '=', False)])
    account_id_cr = fields.Many2one('account.account', 'Credit Partner Account', readonly=True,
                                    domain=[('deprecated', '=', False)])
    account_payment_ref = fields.Many2one('account.payment', 'Payment Refrence', readonly=True)
    company_id = fields.Many2one('res.company', related='journal_id.company_id', string='Company', store=True,
                                 readonly=True)
    state = fields.Selection([('draft', 'Draft'), ('posted', 'Posted'), ('encash', 'Encashed'), ('cancel', 'Cancel')],
                             readonly=True, default='draft', copy=False, string="Status", track_visibility='onchange')

    @api.depends('partner_id')
    def onchange_partner_id(self):
        if self.partner_id.property_account_receivable_id:
            self.account_id_cr = self.partner_id.property_account_receivable_id.id
        else:
            raise Warning(_('Please Configure Receivable id of partner'))

    def action_create_pdc_payment(self):
        for rec in self:
            if self.partner_id:
                self.account_id_cr = self.partner_id.property_account_receivable_id.id
            else:
                raise Warning(_('Please Configure Receivable id of partner'))

            debit = credit = rec.currency_id.compute(rec.amount, rec.currency_id)
            move = {
                # 'name': self.name or '/',
                'journal_id': rec.journal_id.id,
                'date': rec.date,

                'line_ids': [(0, 0, {
                    'name': 'Payment',
                    'debit': debit,
                    'account_id': rec.account_id_dr.id,
                    'partner_id': rec.partner_id.id,
                }),
                             (0, 0, {
                                 'name': 'Customer Cheque',
                                 'credit': credit,
                                 'account_id': rec.account_id_cr.id,
                                 'partner_id': rec.partner_id.id,
                             })]
            }
            move_id = self.env['account.move'].create(move)
            move_id.action_post()
            s_obj = self.env['mail.activity']
            self.env.cr.execute(" SELECT id FROM ir_model WHERE model ='account.mypdc.payment';")
            fetch = self.env.cr.fetchall()
            s_obj.create(
                {
                    'activity_type_id': 4,
                    'res_id': self.id,
                    'res_model_id': str(fetch)[2:-3],
                    'date_deadline': self.effective_date,
                    'user_id': self.env.user.id,
                }
            )
            # self.account_move_id_aa = move_id.id  
            return rec.write({'account_move_id_pdc': move_id.id, 'state': 'posted'})

    def approve_pdc_payment(self):
        # payment_vals = {}
        if not self.account_payment_ref:
            payment_vals = {}
            payment_vals['journal_id'] = self.journal_id.id
            payment_vals['partner_type'] = self.partner_type
            payment_vals['ref'] = self.ref or self.name
            payment_vals['payment_type'] = self.payment_type
            payment_vals['payment_method_id'] = 1
            payment_vals['currency_id'] = self.env.user.company_id.currency_id.id
            payment_vals['amount'] = self.amount
            payment_vals['date'] = self.effective_date
            payment_vals['partner_id'] = self.partner_id.id
            payment_vals['account_id_pdc'] = self.account_id_dr.id
            payment_vals['is_pdc_cheque'] = self.is_pdc_cheque
            payment_vals['cheque_references'] = self.cheque_references
            payment = self.env['account.payment']
            payment_s = payment.create(payment_vals)
            payment_s.action_post()
            return self.write({'account_payment_ref': payment_s.id, 'state': 'encash'})
        elif self.account_payment_ref:
            payment_vals = {}
            pay_move = self.env['account.payment'].search([('id', '=', self.account_payment_ref.id)], limit=1)
            payment_vals['journal_id'] = self.journal_id.id
            payment_vals['partner_type'] = self.partner_type
            payment_vals['ref'] = self.ref or self.name
            payment_vals['payment_type'] = self.payment_type
            payment_vals['payment_method_id'] = 1
            payment_vals['currency_id'] = self.env.user.company_id.currency_id.id
            payment_vals['amount'] = self.amount
            payment_vals['date'] = self.effective_date
            payment_vals['partner_id'] = self.partner_id.id
            payment_vals['account_id_pdc'] = self.account_id_dr.id
            payment_vals['is_pdc_cheque'] = self.is_pdc_cheque
            payment_vals['cheque_references'] = self.cheque_references
            payment_s = pay_move.update(payment_vals)
            _logger.info("===========================%s===========",pay_move)
            _logger.info("===========================%s===========",payment_s)
            return self.write({'account_payment_ref': pay_move.id, 'state': 'encash'})

    def approve_pdc_reset(self):
        return self.write({'state': 'draft'})


class AccountPaymentInherited(models.Model):
    _inherit = "account.payment"

    # @api.depends('invoice_ids', 'payment_type', 'partner_type', 'partner_id', 'is_pdc_cheque')
    # def _compute_destination_account_id(self):
    #     self.destination_account_id = False
    #     for payment in self:
    #         if payment.invoice_ids:
    #             payment.destination_account_id = payment.invoice_ids[0].mapped(
    #                 'line_ids.account_id').filtered(
    #                 lambda account: account.user_type_id.type in ('receivable', 'payable'))[0]
    #         elif payment.payment_type == 'transfer':
    #             if not payment.company_id.transfer_account_id.id:
    #                 raise UserError(
    #                     _('There is no Transfer Account defined in the accounting settings. Please define one to be able to confirm this transfer.'))
    #             payment.destination_account_id = payment.company_id.transfer_account_id.id
    #         elif payment.partner_id:
    #             if payment.partner_type == 'customer':
    #                 if payment.is_pdc_cheque == True:
    #                     payment.destination_account_id = payment.account_id_pdc.id
    #                 else:
    #                     payment.destination_account_id = payment.partner_id.property_account_receivable_id.id
    #             else:
    #                 payment.destination_account_id = payment.partner_id.property_account_payable_id.id
    #         elif payment.partner_type == 'customer':
    #             default_account = self.env['ir.property'].get('property_account_receivable_id', 'res.partner')
    #             payment.destination_account_id = default_account.id
    #         elif payment.partner_type == 'supplier':
    #             default_account = self.env['ir.property'].get('property_account_payable_id', 'res.partner')
    #             payment.destination_account_id = default_account.id

    # @api.depends('journal_id', 'partner_id', 'partner_type', 'is_internal_transfer', 'is_pdc_cheque')
    # def _compute_destination_account_id(self):
    #     self.destination_account_id = False
    #     for pay in self:
    #         if pay.is_internal_transfer:
    #             pay.destination_account_id = pay.journal_id.company_id.transfer_account_id
    #         elif pay.partner_type == 'customer':
    #             # Receive money from invoice or send money to refund it.
    #             if pay.partner_id:
    #                 if pay.is_pdc_cheque == True:
    #                     pay.destination_account_id = pay.partner_id.with_company(
    #                         pay.company_id).account_id_pdc
    #                 else:
    #                     pay.destination_account_id = pay.partner_id.with_company(
    #                         pay.company_id).property_account_receivable_id
    #             else:
    #                 pay.destination_account_id = self.env['account.account'].search([
    #                     ('company_id', '=', pay.company_id.id),
    #                     ('internal_type', '=', 'receivable'),
    #                     ('deprecated', '=', False),
    #                 ], limit=1)
    #         elif pay.partner_type == 'supplier':
    #             # Send money to pay a bill or receive money to refund it.
    #             if pay.partner_id:
    #                 pay.destination_account_id = pay.partner_id.with_company(pay.company_id).property_account_payable_id
    #             else:
    #                 pay.destination_account_id = self.env['account.account'].search([
    #                     ('company_id', '=', pay.company_id.id),
    #                     ('internal_type', '=', 'payable'),
    #                     ('deprecated', '=', False),
    #                 ], limit=1)

    account_id_pdc = fields.Many2one('account.account', 'PDC Account', readonly=True,
                                     domain=[('deprecated', '=', False)])
    is_pdc_cheque = fields.Boolean('PDC Cheque', default=False)
    cheque_references = fields.Char(string='Cheque Refrence', copy=False)


class AccountPDCCancel(models.TransientModel):
    _name = "pdc.cancel.wizard"
    _description = 'Cancel PDC Wizard'
    reason_cancel = fields.Many2one('cancel.reason', required=True, track_visibility='onchange')

    # @api.multi
    def action_cancel_pdc(self):
        context = dict(self._context or {})
        active_id = context.get('active_id', False)
        if active_id:
            pdc_payment = self.env['account.mypdc.payment'].browse(active_id)
            inv_move = self.env['account.move'].search([('id', '=', pdc_payment.account_move_id_pdc.id)], limit=1)
            inv_move.button_cancel()
            pdc_payment.write({'reason_cancel': self.reason_cancel.id, 'state': 'cancel'})


class AccountPDCCancelEncashed(models.TransientModel):
    _name = "pdc.cancel.wizard.encashed"
    _description = 'Cancel PDC Wizard Encashed'
    reason_cancel = fields.Many2one('cancel.reason', required=True, track_visibility='onchange')

    def action_cancel_pdc_encashed(self):
        context = dict(self._context or {})
        active_id = context.get('active_id', False)
        if active_id:
            pdc_payment = self.env['account.mypdc.payment'].browse(active_id)
            inv_move = self.env['account.move'].search([('id', '=', pdc_payment.account_move_id_pdc.id)], limit=1)
            inv_move.button_cancel()
            pay_move = self.env['account.payment'].search([('id', '=', pdc_payment.account_payment_ref.id)], limit=1)
            pay_move.action_cancel()
            pay_move.action_draft()
            pdc_payment.write({'reason_cancel': self.reason_cancel.id, 'state': 'cancel'})


class AccountPDCBounce(models.TransientModel):
    _name = "pdc.bounce.wizard"
    _description = 'PDC Bounce Wizard'
    effective_date = fields.Date('New Effective Date', help='Effective date of PDC', copy=False, default=False,
                                 required=True, track_visibility='onchange')

    def action_bounce_pdc(self):
        context = dict(self._context or {})
        active_id = context.get('active_id', False)
        if active_id:
            pdc_payment = self.env['account.mypdc.payment'].browse(active_id)
            pdc_payment.b_cnt += 1
            pdc_payment.write({'effective_date': self.effective_date})


class CancelReasonInherit(models.Model):
    _name = 'cancel.reason'
    _description = 'Cancel Reason'

    name = fields.Char('Reason Name')
