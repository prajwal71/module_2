# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'PDC Mgmt',
    'version': '15.0.1',
    'summary': 'This app helps you to create payments for PDC',
    'description': "PDC",
    'category': 'Accounting',
    'author': 'Nexus Incorporation',
    'website': 'http://www.nexusgurus.com',
    'license': 'AGPL-3',
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/cancel_wizard.xml',
        'views/views.xml',
    ],
    'depends': ['base', 'account', 'sale'],
    'installable': True,
    'application': False,
    'auto_install': False,

}
