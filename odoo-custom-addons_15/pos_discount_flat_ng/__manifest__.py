# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'POS Flat Discount',
    'version': '15.0',
    'category': 'Point of Sale',
    'sequence': 7,
    'summary': 'Point of Sale ',
    'description': """ This module allows the Flat Discount on Order lines. """,
    'depends': ['point_of_sale'],
      'assets': {
   
        'point_of_sale.assets': [
            'pos_discount_flat_ng/static/src/js/discount_flat.js'],
          'web.assets_qweb': [
             'pos_discount_flat_ng/static/src/xml/discount_flat_templates.xml'],
      },
    'data': [
        # 'views/pos_discount_flat_views.xml',
        # 'views/pos_discount_flat_templates.xml'
    ],
    # 'qweb': [
    #     'static/src/xml/discount_flat_templates.xml',
    # ],
    'installable': True,
}
