odoo.define('pos_discount_flat_ng.pos_discount_flat_ng', function (require) {
"use strict";

var core = require('web.core');
var screens = require('point_of_sale.ReceiptScreen');

var _t = core._t;
console.log("i am ")

var DiscountFlatButton = screens.ActionButtonWidget.extend({
    template: 'DiscountFlatButton',
    button_click: function(){
        var self = this;
        this.gui.show_popup('number',{
            'title': _t('Discount Percentage'),
            // 'value': this.pos.config.discount_pc,
            'confirm': function(val) {
                val = Math.round(Math.max(0,Math.min(100,val)));
                self.apply_discount(val);
            },
        });
    },
    apply_discount: function(pc) {
        var order    = this.pos.get_order();
        var lines    = order.get_orderlines();
        // var product  = this.pos.db.get_product_by_id(this.pos.config.discount_product_id[0]);
        // if (product === undefined) {
        //     this.gui.show_popup('error', {
        //         title : _t("No discount product found"),
        //         body  : _t("The discount product seems misconfigured. Make sure it is flagged as 'Can be Sold' and 'Available in Point of Sale'."),
        //     });
        //     return;
        // }

        // Remove existing discounts
        var i = 0;
        while ( i < lines.length ) {
            // if (lines[i].get_product() === product) {
            //     order.remove_orderline(lines[i]);
            // } else {
            //     i++;
            // }
            lines[i].set_discount(pc)
            i++;
        }
    },
});

screens.define_action_button({
    'name': 'discount',
    'widget': DiscountFlatButton,
    
});

return {
    DiscountFlatButton: DiscountFlatButton,
}

});
