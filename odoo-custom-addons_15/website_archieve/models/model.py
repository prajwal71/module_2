from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class WebsiteExtCustom(models.Model):
    _inherit= "website"

    active = fields.Boolean(string="Active",default=True)

