# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class ResPartner(models.Model):
    _inherit = "res.partner"

    name_ar = fields.Char(string="Arabic Name", required=False, )
    vend_old_id = fields.Integer(string="Old ID", required=False,)
    cust_old_id = fields.Integer(string="Old ID", required=False, )

class Categ(models.Model):
    _inherit = "product.category"

    name_ar = fields.Char(string="Arabic Name", required=False, )
    old_id = fields.Integer(string="Old ID", required=False,)
    old_parent_id = fields.Char(string="Old ID", required=False, )

class Product(models.Model):
    _inherit = "product.template"

    old_id = fields.Integer(string="Old ID", required=False,)

