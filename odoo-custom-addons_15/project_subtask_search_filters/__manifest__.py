# -*- coding: utf-8 -*-
# Copyright 2018 Nova Code (http://www.novacode.nl)
# See LICENSE file for full copyright and licensing details.
{
    'name': 'Project SubTask - Search Filters',
    'summary': 'It add domain parent id in project task',
    'category': 'Project Management',
    'version': '1.0',
    'author': 'Sagar',
    'website': '',
    'license': '',
    'depends': [
        'project'
    ],
    'data': [
        'views/project_task_views.xml'
    ]
}
