# -*- coding: utf-8 -*-
# Powered by Kanak Infosystems LLP.
# © 2020 Kanak Infosystems LLP. (<https://www.kanakinfosystems.com>).

{
    "name": "Account Activate by email at signup",
    "summary": "Email confirmation to activation link",
    "version": "1.0",
    "category": "Tools",
    'license': 'OPL-1',
    "author": "Kanak Infosystems LLP.",
    'website': 'https://www.kanakinfosystems.com',
    'images': ['static/description/banner.jpg'],
    "depends": ["website", "auth_signup"],
    "data": [
        "data/auth_signup_data.xml",
        "views/signup_templates.xml",
    ],
    'installable': True,
    'price': 30,
    'currency': 'EUR',
}
