# See LICENSE file for full copyright and licensing details.

"""
This is controller file to enahnce Website Recruitment portal of Odoo.
.
"""
import base64
import werkzeug.utils
from datetime import datetime

import odoo.addons.website_sale.controllers.main
from odoo import SUPERUSER_ID, http, _
from odoo.addons.portal.controllers.portal \
    import CustomerPortal as CustomerPortal, pager as portal_pager
from odoo.addons.website_hr_recruitment.controllers.main \
    import WebsiteHrRecruitment as Home
from odoo.http import request


class InheritedCustomerPortal(CustomerPortal):
    """Updated the Optional fields list to stop the unkown fields error."""

    def _prepare_portal_layout_values(self):
        values = super(InheritedCustomerPortal, self)._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        warranty_obj = request.env['product.warranty']
        claim_obj = request.env['warranty.claim']
        warranty_count = warranty_obj.sudo().search_count(
                    [('partner_id', '=', partner.id)])
        claim_count = claim_obj.sudo().search_count(
                    [('partner_id', '=', partner.id)])
        values.update({
            'warranty_count': warranty_count,
            'claim_count': claim_count,
        })
        return values

    @http.route(['/my/warranty', '/my/warranty/page/<int:page>'],
                type='http', auth="user", website=True)
    def portal_my_warranty(self, page=1, sortby=None, **kw):
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        warranty_obj = request.env['product.warranty']
        domain =  [('partner_id', '=', partner.id)]
        searchbar_sortings = {
            'warran': {'label': _('My Warranty'), 'order': 'id'},
            
        }

        # default sortby order
        if not sortby:
            sortby = 'warran'
        sort_order = searchbar_sortings[sortby]['order']

        archive_groups = self._get_archive_groups('product.warranty', domain)

        # count for pager
        warranty_count = warranty_obj.search_count(domain)
        # make pager
        pager = portal_pager(
            url="/my/warranty",
            url_args={'sortby': sortby},
            total=warranty_count,
            page=page,
            step=self._items_per_page
        )
        # search the count to display, according to the pager data
        warranty = warranty_obj.search(domain, order=sort_order,
                                            limit=self._items_per_page,
                                            offset=pager['offset'])
        request.session['my_leads_history'] = warranty.ids[:100]

        values.update({
            'warranty': warranty.sudo(),
            'pager': pager,
            'page_name': 'Warranty',
            'archive_groups': archive_groups,
            'default_url': '/my/warranty',
            'searchbar_sortings': searchbar_sortings,
            'sortby': sortby,
        })
        return request.render("warranty_registration.portal_my_warranty", values)

    @http.route(['/my/warranty/<int:warranty_id>'], type='http',
                auth="user", website=True)
    def portal_warranty_page(self, warranty_id, **kw):
        warranty1 = request.env['product.warranty'].sudo().search(
            [('id', '=', warranty_id)])
        values = {
            'partner_id': request.env.user.partner_id.id,
            'warranty1': warranty1,
            'page_name': 'Warranty',
        }
        return request.render('warranty_registration.portal_warranty', values)



    @http.route(['/my/claim', '/my/claim/page/<int:page>'],
                type='http', auth="user", website=True)
    def portal_my_claim(self, page=1, sortby=None, **kw):
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        claim_obj = request.env['warranty.claim']
        domain =  [('partner_id', '=', partner.id)]
        searchbar_sortings = {
            'claimso': {'label': _('My Claims'), 'order': 'id'},
            
        }

        # default sortby order
        if not sortby:
            sortby = 'claimso'
        sort_order = searchbar_sortings[sortby]['order']

        archive_groups = self._get_archive_groups('warranty.claim', domain)

        # count for pager
        claim_count = claim_obj.search_count(domain)
        # make pager
        pager = portal_pager(
            url="/my/claim",
            url_args={'sortby': sortby},
            total=claim_count,
            page=page,
            step=self._items_per_page
        )
        # search the count to display, according to the pager data
        claims = claim_obj.search(domain, order=sort_order,
                                            limit=self._items_per_page,
                                            offset=pager['offset'])
        request.session['my_leads_history'] = claims.ids[:100]

        values.update({
            'claims': claims.sudo(),
            'pager': pager,
            'page_name': 'Claim',
            'archive_groups': archive_groups,
            'default_url': '/my/claim',
            'searchbar_sortings': searchbar_sortings,
            'sortby': sortby,
        })
        return request.render("warranty_registration.portal_my_claim", values)

    @http.route(['/my/claim/<int:claim_id>'], type='http',
                auth="user", website=True)
    def portal_claim_page(self, claim_id, **kw):
        claim = request.env['warranty.claim'].sudo().search(
            [('id', '=', claim_id)])
        values = {
            'partner_id': request.env.user.partner_id.id,
            'claim': claim,
            'page_name': 'Claim',
        }
        return request.render('warranty_registration.portal_claim', values)

    
    @http.route(['/reg-claim/<int:waren_id>'], type='http',
                auth="user", website=True)
    def reg_warranty_claim(self, waren_id, **kw):
        # if waren_id:
        warranty_1 = request.env['product.warranty'].sudo().search(
            [('id', '=', waren_id)])
        if warranty_1:
            values = {
                # 'partner_id': request.env.user.partner_id.id,
                'warranty_1': warranty_1,
                'page_name': 'Register Claim',
            }
        return request.render('warranty_registration.register_warranty_claim', values)

    
    @http.route(['/submit-claim'], type='http', auth="public", website=True)
    def check_claim_details_xlab(self, page=1, **post):
        
        # pro_code = post['promo_code_id']
        product_id = post['product_id']
        product_serial_id = post['product_serial_id']
        warranty_id = post['warranty_id']
        description = post['description']
        # serial_no = post['serial_no']
        # res_code = post['res_code']
        # street = post['address']
        # attachment = post['attachment']
        
        if warranty_id:
            warranty_obj = request.env['product.warranty'].sudo().search([('id','=',warranty_id)])
            if warranty_obj:
                claim_reg_obj = request.env['warranty.claim'].sudo().create(
                    {
                        'description':description,
                        'warranty_id': warranty_obj.id,
                    }
                )
            else:
                return request.render("warranty_registration.warranty_not_found")
        if claim_reg_obj:
            claim_reg_obj.send_notif_email_claim()
            return request.render("warranty_registration.success_claim_register")
        else:
            return request.render("warranty_registration.warranty_not_found")
