{
    'name': 'Salary Rule archive',
    'version': '1.0',
    'summary': 'archives the salary rules.',
    'description' : 'This modules archives the company.',
    'category': 'companies',
    'author': 'Nexus',
    'depends': ['hr_payroll_community'],
    'data': ['views/salary_archive.xml'],
    'installable': True,
    'auto_install': False,

}
