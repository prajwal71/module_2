from odoo import api, fields, models


class SalaryStructuresExt(models.Model):
    _inherit = "hr.payroll.structure"

    active = fields.Boolean(default=True)


class SalaryRuleCategoriesExt(models.Model):
    _inherit = "hr.salary.rule.category"

    active = fields.Boolean(default=True)

class SalaryRuleExt(models.Model):
    _inherit = "hr.salary.rule"

    active = fields.Boolean(default=True)

