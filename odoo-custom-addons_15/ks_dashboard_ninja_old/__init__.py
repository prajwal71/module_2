# -*- coding: utf-8 -*-

from . import models
from . import controllers
from . import common_lib
from odoo.addons.ks_dashboard_ninja.models.ks_dashboard_ninja import KsDashboardNinjaBoard
import logging
_logger = logging.getLogger(__name__)
from odoo.api import Environment, SUPERUSER_ID

import base64
def uninstall_hook(cr, registry):
    env = Environment(cr, SUPERUSER_ID, {})
    for rec in env['ks_dashboard_ninja.board'].search([]):
        rec.ks_dashboard_client_action_id.unlink()
        rec.ks_dashboard_menu_id.unlink()

def pre_init_check(cr):
    from odoo.service import common
    from odoo.exceptions import Warning
    import json 
    env = Environment(cr, SUPERUSER_ID, {})
    f = open('/opt/odoo15/odoo-custom-addons/ks_dashboard_ninja/inventory.json') 
    # encode = base64.b64encode(f)
    # data = base64.b64decode(encode)
    data = json.load(f) 
    self = env['ks_dashboard_ninja.board'].search([])
    _logger.info("=========cr=====%s",self)

    KsDashboardNinjaBoard.ks_import_dashboard(data)

    # version_info = common.exp_version()
    # server_serie = version_info.get('server_serie')
    # if server_serie != '13.0':
    #     raise Warning('Module support Odoo series 13.0 found {}.'.format(server_serie))
    return True
