# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': "Project Ext",
    'summary': """Bridge module for project and enterprise""",
    'description': """
Bridge module for project and enterprise
    """,
    'category': 'Services/Project',
    'version': '1.0',
    'depends': ['project', 'map_view', 'gantt_view', 'backend_enterprise_theme'],
    'data': [
        'views/res_config_settings_views.xml',
        'views/project_task_views.xml',
        'views/project_sharing_templates.xml',
    ],
    'auto_install': True,
    # 'license': 'OEEL-1',
    'assets': {
        'web.assets_backend': [
            'project_ext/static/src/js/**/*',
            'project_ext/static/src/scss/**/*',
            'project_ext/static/src/project_control_panel/**/*',
        ],
        'web.assets_qweb': [
            'project_ext/static/src/**/*.xml',
        ],
        'web.qunit_suite_tests': [
            'project_ext/static/tests/**/*',
        ],
        'project.webclient': [
            ('remove', 'backend_enterprise_theme/static/src/legacy/legacy_service_provider.js'),
            ('remove', 'backend_enterprise_theme/static/src/webclient/home_menu/*'),
            ('remove', 'project/static/src/project_sharing/main.js'),
            'project_ext/static/src/project_sharing/**/*',
        ],
    }
}
