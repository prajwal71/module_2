# Copyright 2019 O4SB - Graeme Gellatly
# Copyright 2019 Tecnativa - Ernesto Tejeda
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
import babel
import copy
import logging
import re

from lxml import html as htmltree

from lxml import html
from markupsafe import Markup
from werkzeug import urls

from odoo import _, api, fields, models, tools
from odoo.addons.base.models.qweb import QWebCodeFound
from odoo.exceptions import UserError, AccessError
from odoo.tools import is_html_empty, safe_eval
from odoo.tools.rendering_tools import convert_inline_template_to_qweb, parse_inline_template, render_inline_template, template_env_globals

_logger = logging.getLogger(__name__)
class MailTemplateEX(models.AbstractModel):
    _inherit = 'mail.render.mixin'

    @api.model
    def _replace_local_links(self, html, base_url=None):
        """ Replace local links by absolute links. It is required in various
        cases, for example when sending emails on chatter or sending mass
        mailings. It replaces

         * href of links (mailto will not match the regex)
         * src of images (base64 hardcoded data will not match the regex)
         * styling using url like background-image: url

        It is done using regex because it is shorten than using an html parser
        to create a potentially complex soupe and hope to have a result that
        has not been harmed.
        """
        if not html:
            return html

        wrapper = Markup if isinstance(html, Markup) else str
        html = tools.ustr(html)
        if isinstance(html, Markup):
            wrapper = Markup

        def _sub_relative2absolute(match):
            # compute here to do it only if really necessary + cache will ensure it is done only once
            # if not base_url
            if not _sub_relative2absolute.base_url:
                _sub_relative2absolute.base_url = self.env["ir.config_parameter"].sudo().get_param("web.base.url")
            return match.group(1) + urls.url_join(_sub_relative2absolute.base_url, match.group(2))

        _sub_relative2absolute.base_url = base_url
        html = re.sub(r"""(<img(?=\s)[^>]*\ssrc=")(/[^/][^"]+)""", _sub_relative2absolute, html)
        html = re.sub(r"""(<a(?=\s)[^>]*\shref=")(/[^/][^"]+)""", _sub_relative2absolute, html)
        html = re.sub(re.compile(
            r"""( # Group 1: element up to url in style
                <[^>]+\bstyle=" # Element with a style attribute
                [^"]+\burl\( # Style attribute contains "url(" style
                (?:&\#34;|'|&quot;)?) # url style may start with (escaped) quote: capture it
            ( # Group 2: url itself
                /(?:[^'")]|(?!&\#34;))+ # stop at the first closing quote
        )""", re.VERBOSE), _sub_relative2absolute, html)


# =========================================
# ADDED FOR REMOVING POWERED BY ODOO AND USING ODOO
# ===========================================================
# ===========================================


        using_word = _("using")
        odoo_word = _("Odoo")
        _logger.info("=======values====%s===========222222222222222222",html)
        
        html = re.sub(using_word + "(.*)[\r\n]*(.*)>" + odoo_word + r"</a>", "", html)
        powered_by = _("Powered by")
        _logger.info("=======PRAJWAL====2===========222222222222222222")
        if powered_by not in html:
           return wrapper(html)
        root = htmltree.fromstring(html)
        powered_by_elements = root.xpath("//*[text()[contains(.,'%s')]]" % powered_by)
        for elem in powered_by_elements:
            # make sure it isn't a spurious powered by
            if any(
                [
                    "www.odoo.com" in child.get("href", "")
                    for child in elem.getchildren()
                ]
            ):
                for child in elem.getchildren():
                    elem.remove(child)
                elem.text = None
        html = htmltree.tostring(root).decode("utf-8")

        return wrapper(html)