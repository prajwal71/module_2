# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from operator import itemgetter

from markupsafe import Markup

from odoo import http
from odoo.exceptions import AccessError, MissingError, UserError
from odoo.http import request
from odoo.tools.translate import _
from odoo.tools import groupby as groupbyelem
from odoo.addons.portal.controllers import portal
from odoo.addons.portal.controllers.portal import pager as portal_pager
from odoo.osv.expression import OR


class CustomerPortal(portal.CustomerPortal):



    def _prepare_home_portal_values(self, counters):
        values = super()._prepare_home_portal_values(counters)
        if 'application_count' in counters:
            values['application_count'] = (
                request.env['crm.lead'].search_count([('partner_id','=',request.env.user.partner_id.id)])
                if request.env['crm.lead'].check_access_rights('read', raise_exception=False)
                else 0
            )
        return values

    def _application_get_page_view_values(self, application, access_token, **kwargs):
        values = {
            'page_name': 'My Application',
            'application': application,
        }
        return self._get_page_view_values(application, access_token, values, 'my_application_history', False, **kwargs)

    @http.route(['/my/application', '/my/application/page/<int:page>'], type='http', auth="user", website=True)
    def my_application(self, page=1, date_begin=None, date_end=None, sortby=None, filterby='all', search=None, groupby='none', search_in='content', **kw):
        values = self._prepare_portal_layout_values()
        # default sort by value
        if not sortby:
            sortby = 'date'
     

        # pager
        domain =[('partner_id','=',request.env.user.partner_id.id)]
        application_count = request.env['crm.lead'].search_count(domain)
        pager = portal_pager(
            url="/my/application",
            # url_args={'date_begin': date_begin, 'date_end': date_end, 'sortby': sortby, 'search_in': search_in, 'search': search, 'groupby': groupby, 'filterby': filterby},
            total=application_count,
            page=page,
            step=self._items_per_page
        )

        applications = request.env['crm.lead'].search(domain,limit=self._items_per_page, offset=pager['offset'])
        request.session['my_application_history'] = applications.ids[:100]

        # if groupby == 'stage':
        #     grouped_tickets = [request.env['helpdesk.ticket'].concat(*g) for k, g in groupbyelem(tickets, itemgetter('stage_id'))]
        # else:
        #     grouped_tickets = [tickets]

        values.update({
            # 'date': date_begin,
            'applications': applications,
            'page_name': 'My Application',
            'default_url': '/my/application',
            'pager': pager,
            # 'sortby': sortby,
            # 'groupby': groupby,
            # 'search_in': search_in,
            # 'search': search,
            # 'filterby': filterby,
        })
        return request.render("website_crm_show.portal_application_view", values)

    @http.route([
        "/application/detail/<int:application_id>",
        "/application/detail/<int:application_id>/<access_token>",
    ], type='http', auth="public", website=True)
    def application_followup(self, application_id=None, access_token=None, **kw):
        try:
            application_sudo = self._document_check_access('crm.lead', application_id, access_token)
        except (AccessError, MissingError):
            return request.redirect('/my')

        values = self._application_get_page_view_values(application_sudo, access_token, **kw)
        return request.render("website_crm_show.application_followup", values)


