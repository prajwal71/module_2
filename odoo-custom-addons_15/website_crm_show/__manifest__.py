{
    'name': 'My Application on Website',
    'version': '1.0',
    'summary': '.',
    'description' : 'This modules helps to see lead from website.',
    'category': 'companies',
    'author': 'Shubheta Tech',
    'depends': ['crm','portal','website'],
    'data': [
        'security/ir.model.access.csv',
        'views/view.xml'],
    'installable': True,
    'auto_install': False,
    "assets": {
        "web.assets_frontend": [
            'website_crm_show/static/src/js/custom.js',
            'website_crm_show/static/src/js/main.js'
       
        ],
    },

}
