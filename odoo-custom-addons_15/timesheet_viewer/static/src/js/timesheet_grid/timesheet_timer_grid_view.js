odoo.define('timesheet_viewer.TimerGridView', function (require) {
    "use strict";

    const viewRegistry = require('web.view_registry');
    const WebGridView = require('grid_template.GridView');
    const TimerGridController = require('timesheet_viewer.TimerGridController');
    const TimerGridModel = require('timesheet_viewer.TimerGridModel');
    const GridRenderer = require('timesheet_viewer.TimerGridRenderer');
    const TimesheetConfigQRCodeMixin = require('timesheet_viewer.TimesheetConfigQRCodeMixin');
    const { onMounted, onPatched } = owl.hooks;

    class TimerGridRenderer extends GridRenderer {
        constructor() {
            super(...arguments);
            onMounted(() => this._bindPlayStoreIcon());
            onPatched(() => this._bindPlayStoreIcon());
        }
    }

    // QRCode mixin to bind event on play store icon
    Object.assign(TimerGridRenderer.prototype, TimesheetConfigQRCodeMixin);

    const TimerGridView = WebGridView.extend({
        config: Object.assign({}, WebGridView.prototype.config, {
            Model: TimerGridModel,
            Controller: TimerGridController,
            Renderer: TimerGridRenderer
        })
    });

    viewRegistry.add('timesheet_timer_grid', TimerGridView);

    return TimerGridView;
});
