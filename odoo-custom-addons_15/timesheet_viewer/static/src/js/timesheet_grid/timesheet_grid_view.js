odoo.define('timesheet_viewer.GridView', function (require) {
    "use strict";

    const viewRegistry = require('web.view_registry');
    const WebGridView = require('grid_template.GridView');
    const TimesheetConfigQRCodeMixin = require('timesheet_viewer.TimesheetConfigQRCodeMixin');
    const TimesheetGridController = require('timesheet_viewer.GridController');
    const TimesheetGridModel = require('timesheet_viewer.GridModel');
    const GridRenderer = require('timesheet_viewer.GridRenderer');
    const { onMounted, onPatched } = owl.hooks;

    class TimesheetGridRenderer extends GridRenderer {
        constructor() {
            super(...arguments);
            onMounted(() => this._bindPlayStoreIcon());
            onPatched(() => this._bindPlayStoreIcon());
        }
    }

    // QRCode mixin to bind event on play store icon
    Object.assign(TimesheetGridRenderer.prototype, TimesheetConfigQRCodeMixin);

    // JS class to avoid grouping by date
    const TimesheetGridView = WebGridView.extend({
        config: Object.assign({}, WebGridView.prototype.config, {
            Model: TimesheetGridModel,
            Controller: TimesheetGridController,
            Renderer: TimesheetGridRenderer
        })
    });

    viewRegistry.add('timesheet_viewer', TimesheetGridView);

    return TimesheetGridView;
});
