from odoo import fields, http, _
from odoo.exceptions import AccessError, MissingError, ValidationError
from odoo.fields import Command
from odoo.http import request
import logging
# from odoo.addons.payment.controllers import portal as payment_portal
# from odoo.addons.payment import utils as payment_utils
# from odoo.addons.portal.controllers.mail import _message_post_helper
# from odoo.addons.portal.controllers import portal
from odoo.addons.portal.controllers.portal import pager as portal_pager, get_records_pager
from odoo.addons.sale.controllers.portal import CustomerPortal
_logger = logging.getLogger(__name__)

class CustomerPortalEXt(CustomerPortal):

    def _prepare_home_portal_values(self, counters):
        values = super()._prepare_home_portal_values(counters)
        partner = request.env.user.partner_id

        DeliveryOrder = request.env['stock.picking']
        SaleOrder = request.env['sale.order']
        if 'quotation_count' in counters:
            values['quotation_count'] = SaleOrder.search_count([
                ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
                ('state', 'in', ['sent', 'cancel'])
            ]) if SaleOrder.check_access_rights('read', raise_exception=False) else 0
        if 'order_count' in counters:
            values['order_count'] = SaleOrder.search_count([
                ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
                ('state', 'in', ['sale', 'done'])
            ]) if SaleOrder.check_access_rights('read', raise_exception=False) else 0

        if 'delivery_count' in counters:
            values['delivery_count'] = DeliveryOrder.search_count([
                ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
            ])

        return values


    @http.route(['/my/delivery', '/my/delivery/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_delivery(self, page=1, date_begin=None, date_end=None, sortby=None, **kw):
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        DeliveryOrder = request.env['stock.picking']

        domain = [
            ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id])
            # ('state', 'in', ['assigned','confirmed','done'])
        ]

        searchbar_sortings = {
            'date': {'label': _('Order Date'), 'order': 'scheduled_date desc'},
            'name': {'label': _('Reference'), 'order': 'name'},
            'stage': {'label': _('Stage'), 'order': 'state'},
        }
        # default sortby order
        if not sortby:
            sortby = 'date'
        sort_order = searchbar_sortings[sortby]['order']

        if date_begin and date_end:
            domain += [('create_date', '>', date_begin), ('create_date', '<=', date_end)]

        # count for pager
        delivery_count = DeliveryOrder.search_count(domain)
        _logger.info("==============nisraj karnaj===========%s",order_count)
        # pager
        pager = portal_pager(
            url="/my/delivery",
            url_args={'date_begin': date_begin, 'date_end': date_end, 'sortby': sortby},
            total=delivery_count,
            page=page,
            step=self._items_per_page
        )
        # content according to pager
        orders = DeliveryOrder.search(domain, order=sort_order, limit=self._items_per_page, offset=pager['offset'])
        request.session['my_delivery_history'] = orders.ids[:100]
    
        # order_sudo = request.env['stock.picking']

        values.update({
            'date': date_begin,
            # 'delivery_order': order_sudo,
            'orders': orders.sudo(),
            'page_name': 'delivery',
            'pager': pager,
            'default_url': '/my/delivery',
            'searchbar_sortings': searchbar_sortings,
            'sortby': sortby,
        })
        return request.render("portal_delivery.portal_delivery_orders", values)
