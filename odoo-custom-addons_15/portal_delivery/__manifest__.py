# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Portal Delivery ',
    'version' : '1.1',
    'summary': 'This module help to see the delivery of portal for portal',
    'description': """

    """,
    'author': 'Nexus Incoporation ',
    'depends' : ['sale','portal'],
    'data': [
        # 'security/backdate_security.xml'
        'views/templete.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
