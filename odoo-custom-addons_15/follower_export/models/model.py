# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.

from email.policy import default
from odoo import fields, models,api,_
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta
import logging
_logger = logging.getLogger(__name__)


class IrAttachmentExt(models.Model):
    _inherit = 'mail.followers'
    
    # res_model = fields.Char('Resource Model', help="The database object this attachment will be attached to.")
    external_id_custom = fields.Char('Custom External ID')
    mig_status = fields.Selection([('none', 'Not Required'),('waiting', 'Waiting'),('done', 'Migrated')],'Migration Status',compute='find_mig_status',store=True)
    is_migrated = fields.Boolean(default=False,store=True)
    res_model = fields.Char(
        'Related Document Model Name', required=False, index=True)

    @api.depends('external_id_custom','is_migrated')
    def find_mig_status(self):
        _logger.info("=================================running)")
        for rec in self:
            if rec.external_id_custom and not rec.is_migrated:
                rec.mig_status = 'waiting'
            elif rec.external_id_custom and rec.is_migrated:
                rec.mig_status = 'done'
            else:
                rec.mig_status = 'none'
