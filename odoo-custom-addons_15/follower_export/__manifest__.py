# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Followers Export',
    'version' : '1.1',
    'summary': 'Followers export import',
    'description': """

    """,
    'author': 'Nexus',
    'depends' : ['mail'],
    'data': [
             'views/view.xml'
        ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
