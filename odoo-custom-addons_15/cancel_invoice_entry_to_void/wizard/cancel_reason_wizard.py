from odoo import fields, models, api
import logging
_logger = logging.getLogger(__name__)


class Salewizard(models.TransientModel):
    _name="invoice.cancel.reason"
    

    cancel_reason=fields.Html(string="Reason",required=True)

   
    
    
    def cancel_reason_to_note(self):
        self.env['account.move'].browse(self._context.get("active_ids")).update({'narration':self.cancel_reason})
        data={
            'form':self.read()[0],
        }
        id1 = self._context.get("active_ids")
        id = id1[0]
        _logger.info("=============id==============%s",id)
        self.env['account.move'].cancel_to_void_after_wizard(id)
        # return True
   