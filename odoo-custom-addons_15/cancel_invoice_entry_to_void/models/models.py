# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import api,fields,models,_
import logging
_logger = logging.getLogger(__name__)

class InvoiceInherit(models.Model):
    _inherit = "account.move"

    state = fields.Selection(selection_add=[('voided', 'Voided')],ondelete={'voided': 'cascade'}
                             )

    def cancel_to_void_entry(self):
        
        return {
                'type':'ir.actions.act_window',
                'res_model':'invoice.cancel.reason',
                'view_mode':'form',
                'target':'new'

               }
    def cancel_to_void_after_wizard(self,id):
        _logger.info("=========================111===")
        movObj = self.env['account.move'].search([('state','=','posted')])
        current_id = id
        void_customer_id = self.env.ref('cancel_invoice_entry_to_void.void_customer_from_invoice').id
        void_product_id = self.env.ref('cancel_invoice_entry_to_void.void_product_from_invoice').id
        void_product_name = self.env['product.product'].search([('id','=',void_product_id)]).name

        for rec in movObj:
            if rec.id == current_id:
                # rec.button_draft()
                rec.write({
                    'partner_id': void_customer_id,
                    'state': 'voided'
                })
                
                lineObj = self.env['account.move.line'].search([('move_id','=',rec.id)])
                for dec in lineObj.with_context(check_move_validity=False):
                    dec.write({
                        'product_id': void_product_id,
                        'name': void_product_name,
                        'quantity': 0,
                        'price_unit': 0,
                        'debit': 0,
                        'credit': 0
                        
                        
                    })
            # if rec.invoice_origin:
            #     saleobj = self.env['sale.order'].search([('name','=',rec.invoice_origin)])
            #     for sec in saleobj.picking_ids:
                    

                   

                
                # rec.button_cancel()
                # rec.button_draft()
            