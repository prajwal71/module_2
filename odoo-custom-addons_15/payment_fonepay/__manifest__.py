# -*- coding: utf-8 -*-

{
    'name': 'Fonepay Payment',
    'category': 'eCommerce',
    'summary': 'Payment Acquirer:Fonepay Implementation',
    'version': '1',
    'license': 'AGPL-3',
    'author': 'Nexus Incorporation',
    'website': 'https://nexusgurus.com/',
    'description': """fonepay Payment Acquirer""",
    'depends': ['payment','website'],
    'data': [
        'views/payment_views.xml',
        'views/payment_fonepay_templates.xml',
         'data/payment_acquirer_data.xml',
    ],
    'images': ['static/description/icon.png'],
    'installable': True,
    'application': True,
     'uninstall_hook': 'uninstall_hook',
    'license': 'LGPL-3',
    # 'post_init_hook': 'create_missing_journal_for_acquirers',
}
