# -*- coding: utf-8 -*-
import logging
import pprint
import werkzeug

from odoo import http
from odoo.http import request
# from odoo.addons.payment.controllers.portal import PaymentProcessing
from odoo.addons.payment.controllers.post_processing import PaymentPostProcessing
_logger = logging.getLogger(__name__)


class FonepayController(http.Controller):
    
    @http.route(['/payment/fonepay/return/'], type='http', auth='public', website=True,csrf=False)
    def fonepay_valid_tx(self, **post):
        """ Verify a payment transaction"""
        reference = post.get('PRN')
        success = post.get('RC')
        _logger.info("====%s=====refer===============cntroller=====",reference)
        _logger.info("====%s=====post===============cntroller=====",post)
        
        # TX = request.env['payment.transaction']
        # tx = None
        
        # data = post
        # _logger.info('Fonepay:  %s', pprint.pformat(post))
        # payment_id = kwargs.get('payment_id')
        # reference = data.get('UID')
        # if reference:
        # response = request.env['payment.transaction'].sudo()._fonepay_verify_tx(post)
            # if response:
                # datas = {}
                # datas.update(data)
                # datas.update(response)
                # response['reference'] = reference 
        
        # _logger.info('Fonepay: entering form_feedback with post data %s', pprint.pformat(post))
        # request.env['payment.transaction'].sudo()._get_tx_from_feedback_data('fonepay',post)
        # # request.env['payment.transaction'].sudo().form_feedback(post, 'fonepay')   
        # return_url = post.get('RU') or '/'
        # return werkzeug.utils.redirect('/payment/process')
        
        # response = request.env['payment.transaction'].sudo()._fonepay_form_get_tx_from_data(post)
        # # response = request.env['payment.transaction'].sudo()._fonepay_verify_tx(post)
        # if response:
        #     _logger.info("====%s=====response===============cntroller=====",response)
        #     _logger.info("========new====new=====prajwal")
        #     if post.get('UID'):
        #         # response['reference'] = reference 
        #         _logger.info('khalti: entering form_feedback with post data %s', pprint.pformat(response))
        #         _logger.info("========new==111==new=====prajwal")
        #         tx = request.env['payment.transaction'].sudo()._handle_feedback_data('fonepay',response)
        #         val = request.env['payment.transaction'].sudo()._fonepay_form_validate_custom(response,tx.id)
        #         _logger.info("========new===222=new=====prajwal")
        if success =='successful':
            # tx = request.env['payment.transaction'].sudo()._handle_feedback_data('fonepay',post)
            tx = request.env['payment.transaction'].sudo()._fonepay_form_get_tx_from_data(post)
            _logger.info("========tx================")
            val = request.env['payment.transaction'].sudo()._fonepay_form_validate_custom(post,tx.id)
            _logger.info("========tx========22========")
            return request.redirect('/shop/confirmation')
        else :
            # return request.redirect('/contactus')
            return request.render("payment_fonepay.payment_fail")
            

        


  