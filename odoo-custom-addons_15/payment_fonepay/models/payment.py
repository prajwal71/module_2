# *- encoding: utf-8 -*-

import logging
from tokenize import Double
import requests
import pprint
import json
from werkzeug import urls
import hashlib
import hmac
from odoo import api, fields, models, _
from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.exceptions import UserError
from odoo.tools.safe_eval import safe_eval
from odoo.tools.float_utils import float_round
import xml.etree.ElementTree as ET
import datetime
# from xml.etree import ElementTree
_logger = logging.getLogger(__name__)



class PaymentAcquirerFonepay(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('fonepay', 'Fonepay')], ondelete={'fonepay': 'set default'})
    
    merchant_code = fields.Char(required_if_provider='fonepay', groups='base.group_user')
    fonepay_secret_key = fields.Char(required_if_provider='fonepay', groups='base.group_user')
    

    @api.model
    def _get_fonepay_urls(self, environment):
        _logger.info("'''''''''=================last")
        """ Fonepay URLs"""
        if environment == 'prod':
            return {'fonepay_form_url':'https://clientapi.fonepay.com/api/merchantRequest'}
        else :
            return {'fonepay_form_url':'https://dev-clientapi.fonepay.com/api/merchantRequest'}

    def _fonepay_generate_sign(self, data):
        _logger.info("=========second====")
        sign = data['PID']+','+data['MD']+','+data['PRN']+','+float(data['AMT'])+','+data['CRN']+','+data['DT']+','+data['R1']+','+data['R2']+','+data['RU']
        _logger.info('Sign  %s', pprint.pformat(sign))
        shasign = hmac.new(self.fonepay_secret_key.encode('utf-8'), sign.encode('utf-8'), hashlib.sha512).hexdigest()
        return shasign

    def fonepay_form_generate_values(self, tx_values):
        _logger.info("============first==")
        self.ensure_one()
        _return_url = '/payment/fonepay/return'
        _cancel_url = '/payment/fonepay/return'
        base_url = self.get_base_url()
        now = datetime.datetime.now()
        date_string = now.strftime('%m/%d/%Y')
        fonepay_tx_values = dict(tx_values)
        temp_fonepay_tx_values = {
            'AMT': tx_values['amount'],  # Mandatory
            'CRN': 'NPR',  # Mandatory anyway
            'MD': 'P',  # Any info of the partner is not mandatory
            'DT': date_string,
            'R1': tx_values['reference'],
            'R2': 'N/A',
            'PRN': tx_values['reference'],
            'PID': self.merchant_code,
            'RU':  urls.url_join(base_url, _return_url),
            
        }
        temp_fonepay_tx_values['DV'] = self._fonepay_generate_sign(temp_fonepay_tx_values)
        fonepay_tx_values.update(temp_fonepay_tx_values)
        # fonepay_tx_values.update({'ADD_RETURNDATA': temp_fonepay_tx_values.pop('return_url', '') or ''})
        return fonepay_tx_values
    
    def fonepay_get_form_action_url(self):
        _logger.info("===========third=============================")
        self.ensure_one()
        # data= self.fonepay_form_generate_values()
        environment = 'prod' if self.state == 'enabled' else 'test'
        return self._get_fonepay_urls(environment)['fonepay_form_url']
    
    def _get_default_payment_method_id(self):
        self.ensure_one()
        if self.provider != 'fonepay':
            return super()._get_default_payment_method_id()
        return self.env.ref('payment_fonepay.payment_method_fonepay').id

    


class PaymentTransactionFonepay(models.Model):
    _inherit = 'payment.transaction'
#     'AMT': '23.0',
#  'BC': 'N/A',
#  'BID': '0000',
#  'INI': 'N/A',
#  'PID': 'VQTQ',
#  'PRN': 'S00003-1',
#  'RU': 'https://clientapi.fonepay.com/api/merchantRequest/verificationMerchant',
#  'UID': '3990529'} 

    def _fonepay_generate_sign(self, data):
        _logger.info("=========second====")
        scret=self.acquirer_id.fonepay_secret_key
        # self.acquirer_id.merchant_code
        sign = data['PID']+','+data['MD']+','+data['PRN']+','+str(float(data['AMT']))+','+data['CRN']+','+data['DT']+','+data['R1']+','+data['R2']+','+data['RU']
        _logger.info('Sign  %s', pprint.pformat(sign))
        shasign = hmac.new(scret.encode('utf-8'), sign.encode('utf-8'), hashlib.sha512).hexdigest()
        return shasign

    def _get_specific_rendering_values(self, processing_values):
        """ Override of payment to return Paypal-specific rendering values.

        Note: self.ensure_one() from `_get_processing_values`

        :param dict processing_values: The generic and specific processing values of the transaction
        :return: The dict of acquirer-specific processing values
        :rtype: dict
        """
        res = super()._get_specific_rendering_values(processing_values)
        if self.provider != 'fonepay':
            return res
        _logger.info("=====%s========1=======functiom",processing_values)
        if self.acquirer_id.state  == 'enabled':
            api_url = 'https://clientapi.fonepay.com/api/merchantRequest' 
        else:
            api_url = 'https://dev-clientapi.fonepay.com/api/merchantRequest' 
        base_url = self.acquirer_id.get_base_url()
        # partner_first_name, partner_last_name = payment_utils.split_partner_name(self.partner_name)
        # notify_url = self.acquirer_id.paypal_use_ipn \
        #              and urls.url_join(base_url, PaypalController._notify_url)
        now = datetime.datetime.now()
        date_string = now.strftime('%m/%d/%Y')
        _return_url = '/payment/fonepay/return'
        _cancel_url = '/payment/fonepay/return'
        base_url = self.get_base_url()
        _logger.info("==============%s=====id===",self.acquirer_id.fonepay_secret_key)
        fonepay_tx_values = dict()
        temp_fonepay_tx_values = {
            'address1': self.partner_address,
            'amount': self.amount,
            # 'business': self.acquirer_id.paypal_email_account,
            'city': self.partner_city,
            'country': self.partner_country_id.code,
            'currency_code': self.currency_id.name,
            'email': self.partner_email,
            'first_name': self.partner_name,
            'handling': self.fees,
            'item_name': f"{self.company_id.name}: {self.reference}",
            'item_number': self.reference,
            
            'lc': self.partner_lang,
            # 'notify_url': notify_url,
            # 'return_url': urls.url_join(base_url, PaypalController._return_url),
            'state': self.partner_state_id.name,
            # 'AMT': self.amount,
            'zip_code': self.partner_zip,
            'acquirer': self.acquirer_id,
            'api_url': api_url,
            'AMT': self.amount,  # Mandatory
            'CRN': 'NPR',  # Mandatory anyway
            'MD': 'P',  # Any info of the partner is not mandatory
            'DT': date_string,
            'R1': self.reference,
            'R2': 'N/A',
            'PRN': self.reference,
            'PID': self.acquirer_id.merchant_code,
            'RU':  urls.url_join(base_url, _return_url),
        }
        temp_fonepay_tx_values['DV'] = self._fonepay_generate_sign(temp_fonepay_tx_values)
        fonepay_tx_values.update(temp_fonepay_tx_values)
        # fonepay_tx_values.update({'ADD_RETURNDATA': temp_fonepay_tx_values.pop('return_url', '') or ''})
        return fonepay_tx_values
    @api.model
    def _get_tx_from_feedback_data(self, provider, data):
        """ Override of payment to find the transaction based on Paypal data.

        :param str provider: The provider of the acquirer that handled the transaction
        :param dict data: The feedback data sent by the provider
        :return: The transaction if found
        :rtype: recordset of `payment.transaction`
        :raise: ValidationError if the data match no transaction
        """
        _logger.info("=============2=======functiom")
        tx = super()._get_tx_from_feedback_data(provider, data)
       
        return tx
    
    def _process_feedback_data(self, data):
        """ Override of payment to process the transaction based on Paypal data.

        Note: self.ensure_one()

        :param dict data: The feedback data sent by the provider
        :return: None
        :raise: ValidationError if inconsistent data were received
        """
        _logger.info("=============3=======functiom")
        super()._process_feedback_data(data)
        # if self.provider != 'paypal':
        return

    @api.model
    def _fonepay_form_get_tx_from_data(self, data):
        """ Given a data dict coming from fonepay, verify it and find the related
        transaction record. """
        # origin_data = dict(data)
        # data = normalize_keys_upper(data)
        _logger.info("=========%s====4=======functiom",data)
        amt, bc, bid,ini,pid,prn,ru,uid = data.get('P_AMT'), data.get('BC'), data.get('BID'),data.get('INI'),data.get('PID'),data.get('PRN'),data.get('RU'),data.get('UID')
        _logger.info("+===============new+++++++prajwal===")
        if not amt or not bc or not uid or not prn:
            error_msg = _('Fonepay: received data with missing reference (%s) or pay_id (%s) or shasign (%s)') % (amt, prn, uid)
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        tx = self.search([('reference', '=', prn)])
        if not tx or len(tx) > 1:
            error_msg = _('Fonepay: received data for reference %s') % (prn)
            if not tx:
                error_msg += _('; no order found')
            else:
                error_msg += _('; multiple order found')
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        # verify shasign
        # shasign_check = tx.acquirer_id._buckaroo_generate_digital_sign('out', origin_data)
        # if shasign_check.upper() != shasign.upper():
            # error_msg = _('Buckaroo: invalid shasign, received %s, computed %s, for data %s') % (shasign, shasign_check, data)
            # _logger.info(error_msg)
            # raise ValidationError(error_msg)

        return tx


    def _fonepay_verify_tx(self, data):
        _logger.info("=============5=======functiom")
        acquirer_obj = self.env['payment.acquirer'].search([('provider','=','fonepay')])
        _logger.info('_fonepay_verify_tx: Sending values to URL %s',  pprint.pformat(acquirer_obj))
        if acquirer_obj.state == 'enabled':
            api_url = 'https://merchantapi.fonepay.com/api' 
        else:
            api_url = 'https://dev-merchantapi.fonepay.com/api' 
        datas= {
                    'PRN': data.get('PRN'),
                    'PID': data.get('PID'),
                    'BID': data.get('BID'),
                    'UID': data.get('UID'),
                    'AMT': data.get('AMT'),
        }

        sign = datas['PID']+','+str(data['AMT'])+','+data['PRN']+','+data['BID']+','+data['UID']
        
        shasign = hmac.new(acquirer_obj.fonepay_secret_key.encode('utf-8'), sign.encode('utf-8'), hashlib.sha512).hexdigest()
        datas['DV'] = shasign
        _logger.info('_fonepay_verify_tx: Values received:\n%s', pprint.pformat(datas))
        r = requests.get(api_url,datas)
        resp = str(r.text)
        _logger.info('_fonepay_response: Values received:\n%s', pprint.pformat(r.text))
        responseXml = ET.fromstring(resp)
        response = responseXml.find('success')
        msg = responseXml.find('message')
        return {'status':response.text,
                'msg': msg.text,
                'UID':data.get('UID')}


    def _fonepay_form_validate(self,data):
        _logger.info("=============8=======functiom")
        res = self._fonepay_verify_tx(data)
        status = res['status']
        _logger.info("PRAJWAL ===== ENTER=====")
        # amount = tree["data"]["amount"]
        # currency = tree["data"]["currency"]
        if status == 'true':
            self.write({
                'date': fields.datetime.now(),
                'acquirer_reference': res.get('UID'),
                'state_message': res.get('msg')
            })
            self._set_transaction_done()
            # self.execute_callback()
            # if self.payment_token_id:
            #     self.payment_token_id.verified = True
            # return True
        else:
            
            _logger.info("PRAJWAL ===== ENTER=====")
            error = res.get('msg')
            _logger.warn(error)
            self.sudo().write({
                'state_message': error,
                'acquirer_reference':res.get('UID'),
                'date': fields.datetime.now(),
            })
            self._set_transaction_cancel()
            return False
    def _fonepay_form_validate_custom(self,data,record):
        _logger.info("======================%s===data======%s==",data,type(data))
        status = data['RC']
        if status == "successful":
            tran = self.env['payment.transaction'].search([('id','=',record)])
            _logger.info('Validated fonepay payment for tx %s: set as done' % (tran.reference))
            
            tran = self.env['payment.transaction'].search([('id','=',record)])
            tran.write({'acquirer_reference': data.get('UID')})
            tran._set_done()
            sales_orders = tran.mapped('sale_order_ids').filtered(lambda so: so.state in ('draft', 'sent'))
            # for tx in tran:
            #     tx._check_amount_and_confirm_order()
            # sales_orders._send_order_confirmation_mail()
            _logger.info("==============%s==sale===oder",sales_orders)
            # sales_orders.action_confirm()
            for s in sales_orders:
                _logger.info("====sss==========%s==sale===oder",sales_orders)
                s.action_confirm()
                tran._invoice_sale_orders()

            # 
            
            tran._create_payment()
            # sales_orders.action_confirm()
            # tran._finalize_post_processing()
            return True
        else:
            error = 'Received unrecognized status for fonepay payment %s: %s, set as error' % (tran.reference, status)
            _logger.info(error)
            self.write({'acquirer_reference': data, 'state_message': data})
            self._set_transaction_cancel()
            return False
    def error_msg(self):
        raise UserError(
                        _(
                            "Sorry Due to Server Error Your payment was not registered Please Try Again. "
                          
                        )
                    )

class AccountPaymentMethod(models.Model):
    _inherit = 'account.payment.method'

    @api.model
    def _get_payment_method_information(self):
        res = super()._get_payment_method_information()
        res['fonepay'] = {'mode': 'unique', 'domain': [('type', '=', 'bank')]}
        return res


# paypal_type = fields.Char(
#         string="PayPal Transaction Type", help="This has no use in Odoo except for debugging.")

    

#     @api.model
#     def _get_tx_from_feedback_data(self, provider, data):
#         """ Override of payment to find the transaction based on Paypal data.

#         :param str provider: The provider of the acquirer that handled the transaction
#         :param dict data: The feedback data sent by the provider
#         :return: The transaction if found
#         :rtype: recordset of `payment.transaction`
#         :raise: ValidationError if the data match no transaction
#         """
#         tx = super()._get_tx_from_feedback_data(provider, data)
#         if provider != 'paypal':
#             return tx

#         reference = data.get('item_number')
#         tx = self.search([('reference', '=', reference), ('provider', '=', 'paypal')])
#         if not tx:
#             raise ValidationError(
#                 "PayPal: " + _("No transaction found matching reference %s.", reference)
#             )
#         return tx

#     def _process_feedback_data(self, data):
#         """ Override of payment to process the transaction based on Paypal data.

#         Note: self.ensure_one()

#         :param dict data: The feedback data sent by the provider
#         :return: None
#         :raise: ValidationError if inconsistent data were received
#         """
#         super()._process_feedback_data(data)
#         if self.provider != 'paypal':
#             return

#         txn_id = data.get('txn_id')
#         txn_type = data.get('txn_type')
#         if not all((txn_id, txn_type)):
#             raise ValidationError(
#                 "PayPal: " + _(
#                     "Missing value for txn_id (%(txn_id)s) or txn_type (%(txn_type)s).",
#                     txn_id=txn_id, txn_type=txn_type
#                 )
#             )
#         self.acquirer_reference = txn_id
#         self.paypal_type = txn_type

#         payment_status = data.get('payment_status')

#         if payment_status in PAYMENT_STATUS_MAPPING['pending'] + PAYMENT_STATUS_MAPPING['done'] \
#             and not (self.acquirer_id.paypal_pdt_token and self.acquirer_id.paypal_seller_account):
#             # If a payment is made on an account waiting for configuration, send a reminder email
#             self.acquirer_id._paypal_send_configuration_reminder()

#         if payment_status in PAYMENT_STATUS_MAPPING['pending']:
#             self._set_pending(state_message=data.get('pending_reason'))
#         elif payment_status in PAYMENT_STATUS_MAPPING['done']:
#             self._set_done()
#         elif payment_status in PAYMENT_STATUS_MAPPING['cancel']:
#             self._set_canceled()
#         else:
#             _logger.info("received data with invalid payment status: %s", payment_status)
#             self._set_error(
#                 "PayPal: " + _("Received data with invalid payment status: %s", payment_status)
#             )
