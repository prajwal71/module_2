from odoo import api, fields, models


class archive_model(models.Model):
    _inherit = "ir.attachment"

    active = fields.Boolean(default=True)
