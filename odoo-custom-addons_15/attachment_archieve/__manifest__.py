{
    'name': 'attachment archive',
    'version': '1.0',
    'summary': 'archives the attachment.',
    'description' : 'This modules archives the attachment.',
    'category': 'companies',
    'author': 'Nexus',
    'depends': ['base'],
    'data': ['views/model_archive.xml'],
    'installable': True,
    'auto_install': False,

}
