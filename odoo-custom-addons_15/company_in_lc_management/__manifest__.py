{
    'name': 'Comapny Field in LC Management',
    'version': '1.0',
    'summary': 'This modules add company in Lc.',
    'description' : 'This modules add company in Lc.',
    'category': 'companies',
    'author': 'Nexus',
    'depends': ['lc_management'],
    'data': ['views/view.xml'],
    'installable': True,
    'auto_install': False,

}
