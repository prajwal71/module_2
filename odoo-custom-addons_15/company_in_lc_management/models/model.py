from odoo import api, fields, models


class LC(models.Model):
    _inherit = "loc.management"

    company_id =  fields.Many2one('res.company',string="Company")
