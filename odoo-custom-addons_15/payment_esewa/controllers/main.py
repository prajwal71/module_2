# -*- coding: utf-8 -*-
import logging
import pprint
import werkzeug

from odoo import http
from odoo.http import request
# from odoo.addons.payment.controllers.portal import PaymentProcessing
from odoo.addons.payment.controllers.post_processing import PaymentPostProcessing
_logger = logging.getLogger(__name__)


class FonepayController(http.Controller):
    
    @http.route(['/payment/fonepay/return/'], type='http', auth='public', csrf=False)
    def fonepay_valid_tx(self, **post):
        """ Verify a payment transaction"""
        # TX = request.env['payment.transaction']
        # tx = None
        
        # data = post
        # _logger.info('Fonepay:  %s', pprint.pformat(post))
        # payment_id = kwargs.get('payment_id')
        # reference = data.get('UID')
        # if reference:
        # response = request.env['payment.transaction'].sudo()._fonepay_verify_tx(post)
            # if response:
                # datas = {}
                # datas.update(data)
                # datas.update(response)
                # response['reference'] = reference 
        
        _logger.info('Fonepay: entering form_feedback with post data %s', pprint.pformat(post))
        request.env['payment.transaction'].sudo()._get_tx_from_feedback_data('fonepay',post)
        # request.env['payment.transaction'].sudo().form_feedback(post, 'fonepay')   
        return_url = post.get('RU') or '/'
        return werkzeug.utils.redirect('/payment/process')