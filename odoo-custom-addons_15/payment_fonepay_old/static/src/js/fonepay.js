odoo.define('payment_fonepay.fonepay', function(require) {
    "use strict";
    console.log("Niraj  111 testing");
    var ajax = require('web.ajax');
    var core = require('web.core');
    var _t = core._t;
    var qweb = core.qweb;
    ajax.loadXML('/payment_fonepay/static/src/xml/payment_fonepay_templates.xml', qweb);

    require('web.dom_ready');
    
    if (!$('.o_payment_form').length) {
        return $.Deferred().reject("DOM doesn't contain '.o_payment_form'");
    }

    // var observer = new MutationObserver(function(mutations, observer) {
    //     for(var i=0; i<mutations.length; ++i) {
    //         for(var j=0; j<mutations[i].addedNodes.length; ++j) {
    //             if(mutations[i].addedNodes[j].tagName.toLowerCase() === "form" && mutations[i].addedNodes[j].getAttribute('provider') == 'khalti') {
    //                 display_khalti_form($(mutations[i].addedNodes[j]));
    //             }
    //         }
    //     }
    // });

    function fonepay_show_error(msg) {
        var wizard = $(qweb.render('fonepay.error', {'msg': msg || _t('Payment error')}));
        wizard.appendTo($('body')).modal({'keyboard': true});
    };

    function display_fonepay_form(provider_form) {
        // Open Checkout with further options
        console.log("i m price");
        console.log(document.getElementById("amount").value);
        var payment_form = $('.o_payment_form');
        if(!payment_form.find('i').length)
        {
            payment_form.append('<i class="fa fa-spinner fa-spin"/>');
            payment_form.attr('disabled','disabled');
        }

        var get_input_value = function (name) {
            return provider_form.find('input[name="' + name + '"]').val();
        }
        var primaryColor = getComputedStyle(document.body).getPropertyValue('--primary');
        console.log("i m price");
        console.log(document.getElementById("amount").value);
        var options = {
            "publicKey": document.getElementById("publicKey").value,
            "amount": parseFloat(document.getElementById("amount").value),
            "productName": document.getElementById("productName"),
            "productIdentity": document.getElementById("productIdentity"),
            "publicKey": document.getElementById("publicKey").value,
            "amount": parseFloat(document.getElementById("amount").value),
            "productName": document.getElementById("productName"),
            "productIdentity": document.getElementById("productIdentity"),
            "productUrl" : " ",
            "eventHandler": {
                onSuccess (payload) {
                    if (payload.idx) {
                        $.post('/payment/fonepay/return/',{
                            payment_id: payload.idx,
                            amount:payload.amount,
                            token:payload.token,
                            productIdentity:payload.product_identity,
                        }).done(function (data) {
                            window.location.href = data;
                        }).fail(function (data) {
                            fonepay_show_error(data && data.data && data.data.message);
                        });
                    }
                    // hit merchant api for initiating verfication
                    console.log(payload);
                },
                onError (error) {
                    console.log(error);
                },
                onClose () {
                    location.reload();
                    console.log('widget is closing');
                }
            }
            
        }
        var checkout = new fonepayCheckout(options);
        checkout.show({amount: parseFloat(document.getElementById('amount').value)});
    };

    $.getScript("https://dev-clientapi.fonepay.com/api/merchantRequest/verificationMerchant", function (data, textStatus, jqxhr) {
        // observer.observe(document.body, {childList: true});
        display_fonepay_form($('form[provider="fonepay"]'));
    });
});