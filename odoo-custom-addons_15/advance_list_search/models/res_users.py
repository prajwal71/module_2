from odoo import models, fields, Command


class ResUsers(models.Model):
    _inherit = 'res.users'

    advanced_list_search_view_ids = fields.Many2many('ir.ui.view')

    def toggle_advanced_list_search_view_id(self, view_id):
        self.ensure_one()
        if view_id not in self.advanced_list_search_view_ids.ids:
            self.advanced_list_search_view_ids = [Command.link(view_id)]
        else:
            self.advanced_list_search_view_ids = [Command.unlink(view_id)]
        return {
            'advanced_list_search': view_id in self.advanced_list_search_view_ids.ids,
        }
