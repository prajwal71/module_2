/** @odoo-module **/

import Domain from 'web.Domain';
import {
    FLOAT_FIELDS,
    NUMERIC_FIELDS,
    TEXT_FIELDS,
    SIMPLE_FIELDS,
} from './main';

const { Component } = owl;
const { useRef, useState } = owl.hooks;

class AdvancedSearchSimpleField extends Component {
    constructor(...args) {
        super(...args);

        this.inputRef = useRef('input');

        this.searchQuery = '';
        let filter = Object.values(this.props.searchModelState.filters)
            .find(f => f.type === 'filter' && !!f.advancedSearch && f.fieldName === this.props.field.name);
        if (filter) {
            let searchQuery = this.props.searchModelState.query
                .find(e => e.filterId === filter.id);
            if (searchQuery && filter.domain) {
                let domainArray = Domain.prototype.stringToArray(filter.domain);
                this.searchQuery = domainArray[0][2];
            }
        }

        this.state= useState({
            showClearBtn: !!this.searchQuery,
        });

        this.inputType = this.isNumericField() ? 'number' : 'text';
        this.domainOperator = this.isNumericField() ? '=' : 'ilike';
    }

    isNumericField() {
        return NUMERIC_FIELDS.includes(this.props.field.type);
    }

    onClickSearchButton(event) {
        this.onSearch();
    }

    onInput(event) {
        this.state.showClearBtn = !!this.inputRef.el.value;
    }

    onKeydownInput(event) {
        switch (event.key) {
            case 'Enter':
                this.onSearch();
                break;
        }
    }

    onClickClearBtn(event) {
        this.inputRef.el.value = '';
        this.state.showClearBtn = false;
        this.onSearch();
    }

    onSearch() {
        let value = this.inputRef.el.value.trim();
        if (value === this.searchQuery) {
            return;
        }
        if (this.isNumericField()) {
            if (FLOAT_FIELDS.includes(this.props.field.type)) {
                value = parseFloat(value);
            } else {
                value = parseInt(value);
            }
        }
        this.env.bus.trigger('advanced-search', {
            fieldType: this.props.field.type,
            fieldName: this.props.field.name,
            operator: this.domainOperator,
            value,
            fieldString: this.props.field.string,
        });
    }
}

AdvancedSearchSimpleField.props = {
    field: {
        type: Object,
        validate: prop => SIMPLE_FIELDS.includes(prop.type),
    },
    searchModelState: Object,
}

AdvancedSearchSimpleField.template = 'advanced_search_simple_field';

export {
    AdvancedSearchSimpleField,
}
