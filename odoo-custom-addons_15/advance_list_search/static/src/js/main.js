/** @odoo-module **/

const ADVANCED_LIST_SEARCH_CLASS = 'advanced_list_search';
const TEXT_FIELDS = ['char', 'text', 'html'];
const FLOAT_FIELDS = ['integer', 'float', 'monetary']
const NUMERIC_FIELDS = ['integer', ...FLOAT_FIELDS]
const SIMPLE_FIELDS = [...TEXT_FIELDS, ...NUMERIC_FIELDS];
const SELECTION_FIELD = 'selection';
const RELATION_FIELDS = ['many2one', 'many2many'];
const stickyHeader = {
	NONE: 'none',
	ALWAYS: 'always',
	ADVANCED_SEARCH : 'advanced_search',
}

function checkAdvancedListSearchClass(attrs) {
	if (attrs.class) {
		let classList = attrs.class.split(' ');
		return classList.includes(ADVANCED_LIST_SEARCH_CLASS);
	}
	return false;
}

export {
	TEXT_FIELDS,
	FLOAT_FIELDS,
	NUMERIC_FIELDS,
	SIMPLE_FIELDS,
	SELECTION_FIELD,
	RELATION_FIELDS,
	stickyHeader,
	checkAdvancedListSearchClass,
}
