/** @odoo-module **/

import ListRenderer from 'web.ListRenderer';
import { AdvancedSearchSimpleField } from './advanced_search_simple_field';
import { AdvancedSearchSelectionField } from './advanced_search_selection_field';
import { AdvancedSearchRelationField } from './advanced_search_relation_field';
import {
	stickyHeader,
	SIMPLE_FIELDS,
	SELECTION_FIELD,
	RELATION_FIELDS,
} from './main';
import 'web.EditableListRenderer';
import { ComponentWrapper } from 'web.OwlCompatibility';

ListRenderer.include({
	_renderView() {
		return this._super(...arguments).then(() => {
			if (this.state.count === 0 && !this.el.querySelector('table')) {
				let table = document.createElement('table');
				table.className = 'o_list_table table table-sm table-hover table-striped';
				table.appendChild(this._renderHeader()[0]);
				let tableWrapper = Object.assign(document.createElement('div'), {
					className: 'table-responsive',
				});
				tableWrapper.appendChild(table);
				this.el.appendChild(tableWrapper);
			}

			if (this.withStickyHeader() && this._shouldRenderOptionalColumnsDropdown()) {
                this.el.querySelector('div.o_optional_columns')?.remove();
                this.el.querySelector('thead').appendChild(this._renderOptionalColumnsDropdown()[0]);
            }
		});
	},

	_renderHeader: function (node) {
		let $thead = this._super(...arguments);
		let thead = $thead[0];
		if (this.withAdvancedSearch()) {
			thead.classList.add('advanced_list_search_thead');
			let tr = document.createElement('tr');

			if (this.hasSelectors) {
				tr.appendChild(document.createElement('th'));
			}

			for (let node of this.columns) {
				
				if (node.tag !== 'field') {
					continue;
				}
				let th = document.createElement('th');
				th.classList.add('advanced_search_th');
				tr.appendChild(th);
				let fieldName = node.attrs.name;
				let field = this.state.fields[fieldName];

				if (!field.searchable) {
					continue;
				}

				let widget;
				if (SIMPLE_FIELDS.includes(field.type)) {
					widget = AdvancedSearchSimpleField;
				} else if (SELECTION_FIELD === field.type) {
					widget = AdvancedSearchSelectionField;
				} else if (RELATION_FIELDS.includes(field.type)) {
					widget = AdvancedSearchRelationField;
				}

				if (widget) {
					this.mountAdvancedSearchWidget({
						widget,
						target: th,
						field,
					});
				}
			}

			thead.appendChild(tr);
		}

		if (this.withStickyHeader()) {
			thead.classList.add('advanced_list_search_thead_sticky');
			if (this._shouldRenderOptionalColumnsDropdown()) {
				let optionalToggleEl = document.createElement('i');
				optionalToggleEl.className = 'o_optional_columns_dropdown_toggle fa fa-ellipsis-v';
				thead.appendChild(optionalToggleEl);
			}
		}

		return $thead;
	},

	mountAdvancedSearchWidget({widget, target, field}) {
		let componentWrapper = new ComponentWrapper(this, widget, {
			field,
			searchModelState: this.searchModelState,
		});
		componentWrapper.mount(target, {position: 'first-child'})
			.catch((e) => {
				console.log(e);
			});
	},

	setRowMode: function (recordID, mode) {
		let res = this._super(...arguments);
		if (this.withAdvancedSearch() && this.isEditable() && this.currentRow !== null) {
			this.currentRow = this.currentRow - 1;
		}
		return res;
	},

	_renderBody: function () {
		let res = this._super(...arguments);
		if (this.withAdvancedSearch() && this.isEditable() && this.currentRow !== null) {
			this.currentRow = this.currentRow - 1;
		}
		return res;
	},

	_selectCell: function (rowIndex, fieldIndex, options) {
		if (this.withAdvancedSearch() && this.isEditable() && rowIndex >= 0) {
			if (rowIndex === 0) {
				return;
			}
			rowIndex = rowIndex - 1;
		}
		return this._super(rowIndex, fieldIndex, options);
	},

	withAdvancedSearch: function () {
		return this.advancedSearchByCode || this.advancedSearchByUser;
	},

	withStickyHeader: function () {
		return this.advancedSearchStickyHeader === stickyHeader.ALWAYS ||
			(this.advancedSearchStickyHeader === stickyHeader.ADVANCED_SEARCH && this.withAdvancedSearch());
	}
});
