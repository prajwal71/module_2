/** @odoo-module **/

import ControlPanelModelExtension from 'web/static/src/js/control_panel/control_panel_model_extension.js';
import { patch } from 'web.utils';
import Domain from 'web.Domain';
import {
    SIMPLE_FIELDS
} from "./main";

const domainOperatorDescriptions = {
    'ilike': 'contains',
    '=': 'equals',
};

patch(ControlPanelModelExtension.prototype, 'advanced_list_search/static/src/control_panel_model_extension.js', {
    get(property, ...args) {
        if (property === 'state') {
            return this.state;
        }
        return this._super(...arguments);
    },

    _getFilters(predicate) {
        let filters = this._super(...arguments);
        if (predicate && predicate.toString().includes('filter')) {
            filters = filters.filter(f => !f.advancedSearch);
        }
        return filters;
    },

    _getFacets() {
        let facets = this._super();
        if (this.viewInfo?.viewType === 'list') {
            let advancedSearchFiltersGroupIds = Object.values(this.state.filters)
                .filter(f => !!f.advancedSearch && f.type === 'filter' && !!f.groupId)
                .map(f => f.groupId);
            facets = facets.filter(f => !advancedSearchFiltersGroupIds.includes(f.groupId));
        }
        return facets;
    },

    setViewInfo(viewInfo) {
        this.viewInfo = viewInfo;
    },

    async createNewFavorite(preFilter) {
        this._super(...arguments);
        this.env.bus.trigger('advanced-update-view', this.viewInfo);
    },

    addAdvancedFilterQuery({ fieldType, fieldName, operator, value, fieldString, valueDisplayName }) {
        let filter;
        if (SIMPLE_FIELDS.includes(fieldType)) {
            filter = Object.values(this.state.filters)
                .find(f => f.type === 'filter' && !!f.advancedSearch && f.fieldName === fieldName);
        }
        if (!filter) {
            let existFieldFilter = Object.values(this.state.filters)
                .find(f => f.type === 'filter' && !!f.advancedSearch && f.fieldName === fieldName);
            let groupId;
            if (existFieldFilter) {
                groupId = existFieldFilter.groupId;
            } else {
                groupId = this.state.nextGroupId;
                this.state.nextGroupId++;
            }
            filter = {
                groupId,
                id: this.state.nextId,
                type: 'filter',
                fieldName,
                fieldType,
                advancedSearch: true,
            }
            if (valueDisplayName) {
                filter.valueDisplayName = valueDisplayName;
            }
            this.state.filters[this.state.nextId] = filter;
            this.state.nextId++;
        }
        let queryElement = this.state.query.find(f => f.filterId === filter.id);
        let domain = Domain.prototype.arrayToString([[fieldName, operator, value]]);
        if (!queryElement) {
            filter.domain = domain;
            let filedDescription = fieldString[0].toUpperCase() + fieldString.slice(1).toLowerCase();
            let operatorDescription = domainOperatorDescriptions[operator];
            let valueDescription = valueDisplayName || value;
            filter.description = `${filedDescription} ${operatorDescription} "${valueDescription}"`;
            this.state.query.push({
                groupId: filter.groupId,
                filterId: filter.id,
            });
        } else {
            if (!value) {
                this.state.query = this.state.query.filter(f => f.filterId !== filter.id);
                delete this.state.filters[filter.id];
            } else {
                filter.domain = domain;
            }
        }
    },

    removeAdvancedSearchFilter(filterId) {
        if (this.state.filters[filterId]) {
            delete this.state.filters[filterId];
            this.state.query = this.state.query.filter(f => f.filterId !== filterId);
        }
    },

    removeAdvancedSearchFilterGroup(groupIds) {
        let filterIds = Object.values(this.state.filters)
            .filter(f => groupIds.includes(f.groupId) && !!f.advancedSearch).map(e => e.id);
        for (let id of filterIds) {
            delete this.state.filters[id];
        }
        this.state.query = this.state.query.filter(e => !filterIds.includes(e.filterId));
    },

    clearAdvancedSearchFilters() {
        let advancedSearchFilterIds = Object.values(this.state.filters)
            .filter(f => f.advancedSearch).map(f => f.id);
        this.state.query = this.state.query.filter(f => !advancedSearchFilterIds.includes(f.filterId));
    },
});
