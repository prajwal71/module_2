

from odoo import models, fields, api,_
import odoo.addons.decimal_precision as dp
from odoo.tools import format_amount

class ProductProduct(models.Model):
    _inherit = 'product.product'

    # Column Section
    standard_price_tax_included = fields.Char(
        compute='_compute_standard_price_tax_included',
        string='Cost Price Tax Included',store=True,
        digits=dp.get_precision('Product Price'),
        help="Cost Price of the product, All Tax Included:\n"
        "This field will be computed with the 'Cost Price', taking into"
        " account Sale Taxes setting.")

    @api.depends('standard_price','taxes_id')
    def _compute_standard_price_tax_included(self):
        for record in self:
            if record.taxes_id:
                amount = record.standard_price*1.13
                currency = record.currency_id
                joined = []
                joined.append(_('%s Incl. Taxes', format_amount(self.env, amount, currency)))
                record.standard_price_tax_included = f"(= {', '.join(joined)})"
            else:
                record.standard_price_tax_included = " "
class ProductTemplate(models.Model):
    _inherit = 'product.template'

    # Column Section
    standard_price_tax_included = fields.Char(
        compute='_compute_standard_price_tax_included',
        string='Cost Price Tax Included',store=True,
        help="Cost Price of the product, All Tax Included:\n"
        "This field will be computed with the 'Cost Price', taking into"
        " account Sale Taxes setting.")

    # @api.depends('standard_price','taxes_id')
    # def _compute_standard_price_tax_included(self):
    #     for product in self:
    #         if product.taxes_id:
    #             product.standard_price_tax_included = product.standard_price*1.13
    #         else:
    #             product.standard_price_tax_included = product.standard_price
    @api.depends('standard_price','taxes_id')
    def _compute_standard_price_tax_included(self):
        for record in self:
            if record.taxes_id:
                amount = record.standard_price*1.13
                currency = record.currency_id
                joined = []
                joined.append(_('%s Incl. Taxes', format_amount(self.env, amount, currency)))
                record.standard_price_tax_included = f"(= {', '.join(joined)})"
            else:
                record.standard_price_tax_included = " "
