
{
    'name': 'Product - Cost Price Tax Included',
    'version': '12.0.1.0.1',
    'category': 'Product',
    'summary': 'Brings a Cost Price Field Tax Included on Product Model',
    'author': 'Nexus',
    'website': 'https://nexusgurus.com',
    'depends': [
        'product',
    ],
    'data': [
        'views/view_product_product.xml',
    ],
}
