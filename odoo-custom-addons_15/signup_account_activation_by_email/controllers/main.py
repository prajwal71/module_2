# -*- coding: utf-8 -*-
# Powered by Kanak Infosystems LLP.
# © 2020 Kanak Infosystems LLP. (<https://www.kanakinfosystems.com>).

import logging
import werkzeug

from odoo import http, _
from odoo.addons.auth_signup.controllers.main import AuthSignupHome
from odoo.http import request
from odoo.addons.web.controllers.main import Home

_logger = logging.getLogger(__name__)


# class Home(Home):
#     @http.route()
#     def web_login(self, *args, **kw):
#         res = super(HomeExt, self).web_login(*args, **kw)
#         if kw.get('login'):
#             user_id = request.env['res.users'].sudo().search([('login', '=', kw.get('login')), ('active', '=', False)])
#             if user_id:
#                 request.params['login_success'] = False
#                 values = request.params.copy()
#                 values['error'] = _("please activate your account first")
#                 values.update({
#                     'signup_enabled': 'True',
#                     'reset_password_enabled': 'True'
#                 })
#                 response = request.render('web.login', values)
#                 response.headers['X-Frame-Options'] = 'DENY'
#                 return response
#         return super(Home, self).web_login(*args, **kw)
class HomeExt(Home):
    @http.route()
    def web_login(self, *args, **kw):
        res = super(HomeExt, self).web_login(*args, **kw)
        if kw.get('login'):
            user_id = request.env['res.users'].sudo().search([('login', '=', kw.get('login')), ('active', '=', False)])
            _logger.info("======logined====")
            if user_id:
                _logger.info("=====11=logined====")
                request.params['login_success'] = False
                values = request.params.copy()
                error = _("please activate your account first")
                values.update({
                    'signup_enabled': 'True',
                    'reset_password_enabled': 'True'
                })
                res.qcontext.update({'error':error})
                res.qcontext.update({'signup_enabled':True})
                res.qcontext.update({'reset_password_enabled':True})
                _logger.info("====================%s=========res===value",res.qcontext)
        return res


class SignupVerifyEmail(AuthSignupHome):
    @http.route()
    def web_auth_signup(self, *args, **kw):
        return self.passwordless_signup(http.request.params)

    def passwordless_signup(self, values):
        qcontext = self.get_auth_signup_qcontext()

        if not values.get("login"):
            return http.request.render("auth_signup.signup", qcontext)

        if not (values.get("login") or values.get("password") or values.get("confirm_password")):
            qcontext["error"] = _("Required field are missing.")
            return http.request.render("auth_signup.signup", qcontext)
        # Check field values are valid
        if not values.get("login", ""):
            qcontext["error"] = _("That does not seem to be an email address.")
            return http.request.render("auth_signup.signup", qcontext)
        elif values.get("password") != values.get("confirm_password"):
            qcontext["error"] = _("Password and Confirm Password does't match.")
            return http.request.render("auth_signup.signup", qcontext)
        elif not values.get("email"):
            values["email"] = values.get("login")
        if not qcontext.get('error') and values.get("login"):
            inactive_users = request.env['res.users'].sudo().search(
                    [('login', '=', qcontext.get('login')), ('active', '=', False)], limit=1)
            if inactive_users:
                inactive_users.write({
                    'password': values.get('password')
                    })
                inactive_users.with_context(create_user=True).action_account_active()
                welcome_msg = """Thank you for your registration. \
                    An E-Mail has been send to you, kindly authenticate your email address."""
                qcontext["message"] = _(welcome_msg)
                return http.request.render("auth_signup.reset_password", qcontext)

        sudo_users = (http.request.env["res.users"].with_context(create_user=True).sudo())

        try:
            values.pop('redirect')
            values.pop('token')
            values.pop('confirm_password')
            if values.get('g-recaptcha-response'):
                values.pop('g-recaptcha-response')
            if values.get('db'):
                values.pop('db')
            company = request.env.company
            _logger.info("============%s=======company",company)
            company_id = company.id
            values.update({'company_id': company_id})
            values['company_ids'] = [(6, 0, [company_id])]
            sudo_users.activate_signup(values, qcontext.get("token"))
            sudo_users.account_active(values.get("login"))
        except Exception as error:
            _logger.exception(error)
            http.request.env.cr.rollback()
            if request.env["res.users"].sudo().search([("login", "=", qcontext.get("login"))]):
                qcontext["error"] = _("Another user is already registered using this email address.")
                return http.request.render("auth_signup.signup", qcontext)
            else:
                qcontext["error"] = _("Something went wrong, please try again later.")
                return http.request.render("auth_signup.signup", qcontext)

        welcome_msg = """Thank you for your registration. \
            An E-Mail has been send to you, kindly authenticate your email address."""
        qcontext["message"] = _(welcome_msg)
        return http.request.render("auth_signup.reset_password", qcontext)

    @http.route('/web/activate', type='http', auth='public', website=True)
    def web_signup_account_active(self, *args, **kw):
        qcontext = self.get_auth_signup_qcontext()

        if not qcontext.get('token'):
            raise werkzeug.exceptions.NotFound()

        if 'error' not in qcontext and request.httprequest.method == 'GET':
            try:
                User = request.env['res.users'].sudo().search(
                    [('login', '=', qcontext.get('login')), ('active', '=', False)])
                if User.partner_id.signup_token == qcontext['token']:
                    User.partner_id.signup_token = False
                    User.partner_id.signup_type = False
                    User.partner_id.signup_expiration = False
                    User.active = True

                    qcontext["message"] = _("Your account activated.")
                    return werkzeug.utils.redirect('/web/login')
                else:
                    qcontext['error'] = _("Invalid or expired token number.")
            except Exception as e:
                qcontext['error'] = _(e.message)

            return http.request.render("auth_signup.signup", qcontext)
