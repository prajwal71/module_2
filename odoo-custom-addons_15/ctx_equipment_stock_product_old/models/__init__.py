# -*- coding: utf-8 -*-

from . import product_template
from . import maintenance_equipment
from . import stock_production_lot
from . import stock_move_line
from . import stock_picking
from . import stock_quant
