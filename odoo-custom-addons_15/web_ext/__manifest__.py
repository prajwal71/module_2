# -*- coding: utf-8 -*-
{
    'name': "web_ext",
    # 'category': 'Hidden',
    'version': '1.0',
    'description':
        """
Odoo Dashboard View.
========================

This module defines the Dashboard view, a new type of reporting view. This view
can embed graph and/or pivot views, and displays aggregate values.
        """,
    'author': 'Nexus',
    'depends': ['web'],
    'data': [
        # 'views/assets.xml',
        # 'views/dashboard.rng',
    ],
    "assets": {
      
        "web.qunit_suite":['web_ext/static/tests/dashboard_tests.js'],
        "web.assets_backend":['web_ext/static/src/js/dashboard_view.js','web_ext/static/src/js/dashboard_renderer.js','web_ext/static/src/js/dashboard_model.js','web_ext/static/src/js/dashboard_controller.js',
                                'web_ext/static/src/scss/dashboard_view.scss','web_ext/static/tests/dashboard_tests.js'],
        'web.assets_qweb': ['web_ext/static/src/xml/dashboard.xml'],
    },
    'qweb': [
        "static/src/xml/dashboard.xml",
    ],
}
