# -*- coding: utf-8 -*-
from odoo import fields, models


VIEW_TYPES = [
    ('tree', 'Tree'),
    ('form', 'Form'),
    ('graph', 'Graph'),
    ('pivot', 'Pivot'),
    ('calendar', 'Calendar'),
    ('gantt', 'Gantt'),
    ('kanban', 'Kanban'),
    ('dashboard','Dashboard'),

]

class ActWindowView(models.Model):
    _inherit = 'ir.actions.act_window.view'

    # view_mode = fields.Selection(selection_add=[('dashboard', "Dashboard")],ondelete={'tree': 'set default'})
    view_mode = fields.Selection(VIEW_TYPES, string='View Type', required=True)