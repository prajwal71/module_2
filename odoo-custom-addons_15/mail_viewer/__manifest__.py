# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.backend_enterprise_theme

{
    'name': 'Mail Enterprise',
    'category': 'Productivity/Discuss',
    'depends': ['mail', 'web_resp'],
    'description': """
Bridge module for mail and enterprise
=====================================

Display a preview of the last chatter attachment in the form view for large
screen devices.
""",
    'auto_install': True,
    # 'license': 'OEEL-1',
    'assets': {
        'mail.assets_discuss_public': [
            'backend_enterprise_theme/static/src/webclient/webclient.scss',
            'mail_viewer/static/src/components/*/*',
            'mail_viewer/static/src/models/*/*.js',
            'web_resp/static/src/js/core/mixins.js',
            'web_resp/static/src/js/core/session.js',
            'web_resp/static/src/js/services/core.js',
        ],
        'web.assets_backend': [
            'mail_viewer/static/src/components/*/*.js',
            'mail_viewer/static/src/models/*/*.js',
            'mail_viewer/static/src/js/attachment_viewer.js',
            'mail_viewer/static/src/scss/mail_enterprise.scss',
            'mail_viewer/static/src/components/*/*.scss',
            'mail_viewer/static/src/scss/mail_enterprise_mobile.scss',
            'mail_viewer/static/src/widgets/*/*.js',
            'mail_viewer/static/src/widgets/*/*.scss',
        ],
        'web.assets_tests': [
            'mail_viewer/static/tests/tours/**/*',
        ],
        'web.qunit_suite_tests': [
            'mail_viewer/static/src/components/*/tests/*.js',
            'mail_viewer/static/src/widgets/*/tests/*.js',
            'mail_viewer/static/tests/attachment_preview_tests.js',
        ],
        'web.assets_qweb': [
            'mail_viewer/static/src/components/*/*.xml',
            'mail_viewer/static/src/xml/mail_enterprise.xml',
        ],
    }
}
