About
============
This module displays the "verified purchase" label on the website that indicates that reviewing person is confirmed and the services that were received. You can display verified purchase label state wise in the sale order(sale order & locked).

Installation
============
1) Copy module files to addon folder.
2) Restart odoo service (sudo service odoo-server restart).
3) Go to your odoo instance and open apps (make sure to activate debug mode).
4) click on update app list.
5) search module name and hit install button.

Any Problem with module?
=====================================
Please create your ticket here https://softhealer.com/support

Softhealer Technologies Doubt/Inquiry/Sales/Customization Team
=====================================
Skype: live:softhealertechnologies
What's app: +917984575681
E-Mail: support@softhealer.com
Website: https://softhealer.com
