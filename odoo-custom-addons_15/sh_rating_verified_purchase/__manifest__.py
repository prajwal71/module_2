# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.
{
    "name" : "Verified Purchase Label",
    "author" : "Softhealer Technologies",
    "website": "http://www.softhealer.com",
    "support": "support@softhealer.com",
    "category": "Website",
    "license": "OPL-1", 
    "summary": "Product Verified Label, Ratings Verified, Product Purchase Verified, Product Review Verify, Verify Product Label, Product Purchsae Verify Label, Verified Purchase Product Label Odoo",
    "description": """This module displays the "verified purchase" label on the website that indicates that reviewing person is confirmed and the services that were received. You can display verified purchase label state wise in the sale order(sale order & locked).""",

    "version":"14.0.1",
    "depends" : [
                    "portal",
                    "website_sale",
                ],
    "application" : True,
    "data" : [

        # "views/assets_frontend.xml",
        "views/res_config_settings.xml",

            ],
    
 
    'assets':
        {
            'web_editor.assets_frontend':
                ['sh_rating_verified_purchase/static/src/js/portal_chatter.js'],

    
        },    

 
    
    "images": ["static/description/background.png", ],
    "auto_install":False,
    "installable" : True,
    "price": 30,
    "currency": "EUR"
}
