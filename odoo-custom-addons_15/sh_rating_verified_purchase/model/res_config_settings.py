# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.

from odoo import fields, models


class Website(models.Model):
    _inherit = "website"
    
    sh_verified_purchase_msg = fields.Char('Label',translate=True)
    sh_verified_purchase_state = fields.Selection([
        ('sale','Sale Order'),
        ('done','Locked'),
        ],
        string='Sale order state would be',
        default="sale")

class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"
    
    sh_verified_purchase_msg = fields.Char(
        related="website_id.sh_verified_purchase_msg",
        string='Label',
        readonly=False)
    
    sh_verified_purchase_state = fields.Selection(
        string="Sale order state would be",
        related="website_id.sh_verified_purchase_state",
        readonly=False)