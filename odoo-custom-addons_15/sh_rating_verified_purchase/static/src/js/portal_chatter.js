odoo.define('sh_rating_verified_purchase.portal.chatter', function (require) {
'use strict';
console.log("Helolo")
var core = require('web.core');
var portalChatter = require('portal.chatter');
var utils = require('web.utils');
var time = require('web.time');

var _t = core._t;
var PortalChatter = portalChatter.PortalChatter;
var qweb = core.qweb;
var STAR_RATING_RATIO = 2; // conversion factor from the star (1-5) to the db rating range (1-10)

var ajax = require('web.ajax');


/**
 * PortalChatter
 *
 * Extends Frontend Chatter to handle rating
 */
PortalChatter.include({	
    xmlDependencies: (PortalChatter.prototype.xmlDependencies || [])
        .concat([
            '/sh_rating_verified_purchase/static/src/xml/portal_chatter.xml'
        ]),
});


});
