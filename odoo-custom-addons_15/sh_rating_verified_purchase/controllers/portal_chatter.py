# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import http
from odoo.http import request

from odoo.addons.portal.controllers.mail import PortalChatter


class PortalChatter(PortalChatter):

    @http.route()
    def portal_chatter_init(self, res_model, res_id, domain=False, limit=False, **kwargs):
        result = super(PortalChatter, self).portal_chatter_init(res_model, res_id, domain=domain, limit=limit, **kwargs)
        # get the rating statistics about the record            
        
        if request.website and res_id and res_model == 'product.template':            
            updated_messages = []
            if result and result.get("messages",False):
                messages = result.get("messages")
                sale_order_state = 'sale'
                if request.website.sh_verified_purchase_state == 'done':
                    sale_order_state = 'done'
                for message in messages:
                    if message.get('message_type','') == 'comment' and message.get('author_id',False):
                        author_id = False
                       
                        try:
                            author_id = message.get('author_id')[0]
                        except:
                            pass
                        
                        if not author_id:
                            continue
                        request._cr.execute('''
                           select sol.id from sale_order_line as sol 
                        JOIN sale_order as so ON so.id = sol.order_id
                        JOIN res_partner as cust ON so.partner_id = cust.id
                        JOIN product_product as product ON sol.product_id = product.id
                        JOIN product_template as template ON product.product_tmpl_id = template.id
                        where so.state = '%s' and so.partner_id = %s and template.id = %s ;
                        ''' % (sale_order_state,author_id, res_id))
                        if request._cr.fetchone():
                            message["sh_verified_purchase_msg"] = request.website.sh_verified_purchase_msg or ''
                        updated_messages.append(message)
            result["messages"] = updated_messages
        return result
