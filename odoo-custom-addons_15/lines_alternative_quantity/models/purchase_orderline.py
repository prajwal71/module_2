from odoo import api, fields, models,exceptions
import logging
_logger = logging.getLogger(__name__)
class PurchaseOrderAlternateQty(models.Model):
    _inherit = "purchase.order"

    alternate_qty_check = fields.Boolean(string="Alternate Qty Activate",default=False)
  
    
    def action_view_invoice(self, invoices=False):
        result = super(PurchaseOrderAlternateQty, self).action_view_invoice(invoices)
        # result['context']['default_alternate_qty_check'] =self.alternate_qty_check
        result.update({'default_alternate_qty_check': self.alternate_qty_check})
        return result
        
class PurchaseOrderLineAlternateQty(models.Model):
    _inherit = "purchase.order.line"

    alternate_qty = fields.Float(string="Aternate Qty")

    
    def _prepare_account_move_line(self, move=False):
        res = super()._prepare_account_move_line(move)
        res.update({'alternate_qty': self.alternate_qty})
        return res    
        
      

class PurchaseReport(models.Model):
    _inherit = "purchase.report"

    alternate_qty = fields.Float(string='Alternate Qty',readonly=True,store=True)

    def _select(self):
        return super(PurchaseReport, self)._select() + ", l.alternate_qty as alternate_qty"

    def _group_by(self):
        return super(PurchaseReport, self)._group_by() + ", l.alternate_qty"    