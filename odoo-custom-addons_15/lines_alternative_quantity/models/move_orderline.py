from odoo import api, fields, models,exceptions,_
import logging
_logger = logging.getLogger(__name__)
from odoo import tools

class AccountMoveAlternateQty(models.Model):
    _inherit = "account.move"

    alternate_qty_check = fields.Boolean(string="Alternate Qty Activate",default=False)

    
    
        
class AccountMoveLineAlternateQty(models.Model):
    _inherit = "account.move.line"

    alternate_qty = fields.Float(compute="get_bill_line_id",string="Aternate Qty",store=True)


    @api.depends('purchase_line_id.alternate_qty','sale_line_ids')
    def get_bill_line_id(self):
        for rec in self:
            
            PurLineObj = self.env['purchase.order.line'].search([('id','=',rec.purchase_line_id.id)])
            if PurLineObj:
                rec.alternate_qty = PurLineObj.alternate_qty
            else:
                SaleLineObj = self.env['sale.order.line'].search([('id','in',rec.sale_line_ids.ids)])
                rec.alternate_qty = SaleLineObj.alternate_qty
   
        
    

class AccountInvoiceReport(models.Model):
    _inherit = "account.invoice.report"

    alternate_qty = fields.Float(string='Alternate Qty',readonly=True)
    partner_city = fields.Char(string='City',readonly=True)

    def _select(self):
        return super(AccountInvoiceReport, self)._select() + ", line.alternate_qty as alternate_qty  , partner.city as partner_city"

    def _group_by(self):
        return super(AccountInvoiceReport, self)._group_by() + ", line.alternate_qty , partner_city"    



   