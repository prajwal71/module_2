from odoo import api, fields, models,exceptions
import logging
_logger = logging.getLogger(__name__)
class SaleOrderAlternateQty(models.Model):
    _inherit = "sale.order"

    alternate_qty_check = fields.Boolean(string="Alternate Qty Activate",default=False)

    def _prepare_invoice(self):
        qty_check_result = super(SaleOrderAlternateQty, self)._prepare_invoice()
        qty_check_result['alternate_qty_check'] = self.alternate_qty_check
        return qty_check_result
    


        
class SaleOrderLineAlternateQty(models.Model):
    _inherit = "sale.order.line"

    alternate_qty = fields.Float(string="Aternate Qty")


  


class SaleReport(models.Model):
    _inherit = "sale.report"

    alternate_qty = fields.Float(string='Alternate Qty',readonly=True,store=True)
    partner_city = fields.Char(string='City',readonly=True)


    def _query(self, with_clause='', fields={}, groupby='', from_clause=''):
        fields['alternate_qty'] = ", l.alternate_qty as alternate_qty"
        fields['partner_city'] = ", partner.city as partner_city"

        groupby += ', l.alternate_qty'
        groupby += ', partner.city'
        return super(SaleReport, self)._query(with_clause, fields, groupby, from_clause)
      
