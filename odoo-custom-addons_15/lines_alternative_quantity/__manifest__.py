# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Lines Alternative Quantity',
    'version' : '1.0',
    'author':'Manoj Khadka',
    'category': 'Sales',
    'maintainer': 'Manoj Khadka',
    'summary': """auto sales workflow""",
    'description': """

        auto sales workflow

    """,
    'website': 'https://www.sagarcs.com/',
    'license': 'LGPL-3',
    'support':'info@sagarcs.com',
    'depends' : ['sale','stock','purchase'],
    'data': [
        'views/sale_orderline.xml',
        'views/purchase_orderline.xml',
        'views/move_orderline.xml',
    ],
    
    'installable': True,
    'application': True,
    'auto_install': False,
    

}
