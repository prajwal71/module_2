from datetime import datetime, timedelta
from itertools import groupby
import json
import logging
_logger = logging.getLogger(__name__)
from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import AccessError, UserError, ValidationError
from odoo.osv import expression
from odoo.tools import float_is_zero, html_keep_url, is_html_empty

class SaleOrderExt(models.Model):
    _inherit = "sale.order"
    
    def _send_order_confirmation_mail(self):
        if self.env.su:
            _logger.info("===============%s======superuser",SUPERUSER_ID)
            # sending mail in sudo was meant for it being sent from superuser
            self = self.with_user(2)
        # grp = self.env.user
        # if self.env.user and self.env.user.has_group('base.group_user'):
        #     self = self.with_user(self.env.user.id)
        template_id = self._find_mail_template(force_confirmation_template=True)
        if template_id:
            for order in self:
                order.with_context(force_send=True).message_post_with_template(template_id, composition_mode='comment', email_layout_xmlid="mail.mail_notification_paynow")