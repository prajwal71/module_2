# -*- coding: utf-8 -*-
#################################################################################
#
# Copyright (c) 2018-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>:wink:
# See LICENSE file for full copyright and licensing details.
#################################################################################
from odoo import http, _
import logging
from odoo.http import request
from odoo.exceptions import UserError
from odoo.addons.web.controllers.main import ensure_db, Home
import logging
_logger = logging.getLogger(__name__)
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.portal.controllers.portal import CustomerPortal



from odoo.addons.auth_signup.models.res_users import SignupError
import werkzeug
from odoo.addons.portal.controllers.portal import pager as portal_pager, CustomerPortal

_logger = logging.getLogger(__name__)

class AuthSignupHome(Home):
	def do_signup(self, qcontext):
		""" Shared helper that creates a res.partner out of a token """
		
		values = { key: qcontext.get(key) for key in ('login', 'name', 'password', 'mobile') }
		_logger.info("...<<<<<<<<<>>>>>>>>>>>>>>>>>>>>..%s",values)
		if not values:
			raise UserError(_("The form was not properly filled in."))
		if values.get('password') != qcontext.get('confirm_password'):
			raise UserError(_("Passwords do not match; please retype them."))
		supported_lang_codes = [code for code, _ in request.env['res.lang'].get_installed()]
		lang = request.context.get('lang', '').split('_')[0]
		if lang in supported_lang_codes:
			values['lang'] = lang
		self._signup_with_values(qcontext.get('token'), values)
		request.env.cr.commit()


class CustomerPortalCity(CustomerPortal):
    MANDATORY_BILLING_FIELDS = ["name", "email", "mobile","street"]
    OPTIONAL_BILLING_FIELDS = ["zipcode","city_id","country_id","state_id","company_name","vat"]
	
	# def _prepare_portal_layout_values(self):
	# 	values = super(CustomerPortalCity, self)._prepare_portal_layout_values()		
	# 	states = request.env['res.country.state'].sudo().search([])
	# 	cities = request.env['res.city'].sudo().search([])
	# 	values['cities'] = cities

		# return values

class WebsiteSaleCity(WebsiteSale):

	def values_postprocess(self, order, mode, values, errors, error_msg):
		new_values, errors, error_msg = super(WebsiteSaleCity, self).values_postprocess(
			order, mode, values, errors, error_msg
		)
		
		new_values["mobile"] = values.get("mobile")


		# if new_values["city_id"]:
		# 	city = request.env["res.city"].browse(int(values.get("city_id")))
		# 	if city:
		# 		new_values["city"] = city.name
		return new_values, errors, error_msg

	def _get_mandatory_fields_billing(self, country_id=False):
		req = ["name", "email","street","mobile"]

		return req

	def _get_mandatory_fields_shipping(self, country_id=False):
		req = ["name", "email","street","mobile"]
		
		return req


	
	
