# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Login Redirect to My Home page',
    'version' : '1.1',
    'summary': '',
    'description': """

====================

    """,
    'author': 'Nexus',
    'depends' : ['base','auth_oauth'],
    # 'data': [
    #          'views/view.xml'
    #     ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
