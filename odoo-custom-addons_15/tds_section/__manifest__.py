# -*- coding: utf-8 -*-
{
    'name': 'TDS Section',
    'author': 'Nexus',
    'category': 'account',
    'license': 'AGPL-3',
    'website': 'www.nexusgurus.com.com',
    'version': '13.0.1.0.1',
    'summary': '''
               This module help to create TDS section
            ''',
    'description': '''
                   
                ''',
    'depends': ['account'],
    'data': [

        'security/ir.model.access.csv',
        'data/demo.xml',
        'views/view.xml',
        'views/account.xml',

    ],

    'installable': True,
    'auto_install': False,
    'application': False
}
