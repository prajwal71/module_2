# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mobile',
    'category': 'Hidden',
    'summary': 'Odoo Mobile Core module',
    'version': '1.0',
    'description': """
        This module provides the core of the Odoo Mobile App.
        """,
    'depends': [
        'backend_enterprise_theme',
    ],
    'data': [
        'views/views.xml',
    ],
    'assets': {
        'web.assets_qweb': [
            'web_resp/static/src/xml/**/*',
        ],
        'web.assets_backend': [
            'web_resp/static/src/**/*',
        ],
        'web.tests_assets': [
            'web_resp/static/tests/helpers/**/*',
        ],
        'web.qunit_mobile_suite_tests': [
            'web_resp/static/tests/*_mobile_tests.js',
        ],
    },
    'installable': True,
    'auto_install': True,
    # 'license': 'OEEL-1',
}
