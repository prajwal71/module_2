# *- encoding: utf-8 -*-

import logging
import requests
import pprint
import json
from werkzeug import urls
import hashlib
import hmac
from odoo import api, fields, models, _
from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.exceptions import UserError
from odoo.tools.safe_eval import safe_eval
from odoo.tools.float_utils import float_round
import xml.etree.ElementTree as ET
import datetime
# from xml.etree import ElementTree
_logger = logging.getLogger(__name__)



class PaymentAcquirerFonepay(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('fonepay', 'Fonepay')])
    
    merchant_code = fields.Char(required_if_provider='fonepay', groups='base.group_user')
    fonepay_secret_key = fields.Char(required_if_provider='fonepay', groups='base.group_user')
    

    @api.model
    def _get_fonepay_urls(self, environment):
        """ Fonepay URLs"""
        if environment == 'prod':
            return {'fonepay_form_url':'https://clientapi.fonepay.com/api/merchantRequest'}
        else :
            return {'fonepay_form_url':'https://dev-clientapi.fonepay.com/api/merchantRequest'}

    def _fonepay_generate_sign(self, data):
        sign = data['PID']+','+data['MD']+','+data['PRN']+','+str(data['AMT'])+','+data['CRN']+','+data['DT']+','+data['R1']+','+data['R2']+','+data['RU']
        _logger.info('Sign  %s', pprint.pformat(sign))
        shasign = hmac.new(self.fonepay_secret_key.encode('utf-8'), sign.encode('utf-8'), hashlib.sha512).hexdigest()
        return shasign

    def fonepay_form_generate_values(self, tx_values):
        self.ensure_one()
        _return_url = '/payment/fonepay/return'
        _cancel_url = '/payment/fonepay/return'
        base_url = self.get_base_url()
        now = datetime.datetime.now()
        date_string = now.strftime('%m/%d/%Y')
        fonepay_tx_values = dict(tx_values)
        temp_fonepay_tx_values = {
            'AMT': tx_values['amount'],  # Mandatory
            'CRN': 'NPR',  # Mandatory anyway
            'MD': 'P',  # Any info of the partner is not mandatory
            'DT': date_string,
            'R1': tx_values['reference'],
            'R2': 'N/A',
            'PRN': tx_values['reference'],
            'PID': self.merchant_code,
            'RU':  urls.url_join(base_url, _return_url),
            
        }
        temp_fonepay_tx_values['DV'] = self._fonepay_generate_sign(temp_fonepay_tx_values)
        fonepay_tx_values.update(temp_fonepay_tx_values)
        fonepay_tx_values.update({'ADD_RETURNDATA': temp_fonepay_tx_values.pop('return_url', '') or ''})
        return fonepay_tx_values
    
    def fonepay_get_form_action_url(self):
        self.ensure_one()
        # data= self.fonepay_form_generate_values()
        environment = 'prod' if self.state == 'enabled' else 'test'
        return self._get_fonepay_urls(environment)['fonepay_form_url']


class PaymentTransactionFonepay(models.Model):
    _inherit = 'payment.transaction'
#     'AMT': '23.0',
#  'BC': 'N/A',
#  'BID': '0000',
#  'INI': 'N/A',
#  'PID': 'VQTQ',
#  'PRN': 'S00003-1',
#  'RU': 'https://clientapi.fonepay.com/api/merchantRequest/verificationMerchant',
#  'UID': '3990529'} 


    @api.model
    def _fonepay_form_get_tx_from_data(self, data):
        """ Given a data dict coming from fonepay, verify it and find the related
        transaction record. """
        # origin_data = dict(data)
        # data = normalize_keys_upper(data)
        amt, bc, bid,ini,pid,prn,ru,uid = data.get('AMT'), data.get('BC'), data.get('BID'),data.get('INI'),data.get('PID'),data.get('PRN'),data.get('RU'),data.get('UID')
        if not amt or not bc or not uid or not prn:
            error_msg = _('Fonepay: received data with missing reference (%s) or pay_id (%s) or shasign (%s)') % (amt, prn, uid)
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        tx = self.search([('reference', '=', prn)])
        if not tx or len(tx) > 1:
            error_msg = _('Fonepay: received data for reference %s') % (prn)
            if not tx:
                error_msg += _('; no order found')
            else:
                error_msg += _('; multiple order found')
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        # verify shasign
        # shasign_check = tx.acquirer_id._buckaroo_generate_digital_sign('out', origin_data)
        # if shasign_check.upper() != shasign.upper():
            # error_msg = _('Buckaroo: invalid shasign, received %s, computed %s, for data %s') % (shasign, shasign_check, data)
            # _logger.info(error_msg)
            # raise ValidationError(error_msg)

        return tx


    def _fonepay_verify_tx(self, data):
        acquirer_obj = self.env['payment.acquirer'].search([('provider','=','fonepay')])
        _logger.info('_fonepay_verify_tx: Sending values to URL %s',  pprint.pformat(acquirer_obj))
        if acquirer_obj.state == 'enabled':
            api_url = 'https://clientapi.fonepay.com/api/merchantRequest/verificationMerchant' 
        else:
            api_url = 'https://dev-clientapi.fonepay.com/api/merchantRequest/verificationMerchant' 
        datas= {
                    'PRN': data.get('PRN'),
                    'PID': data.get('PID'),
                    'BID': data.get('BID'),
                    'UID': data.get('UID'),
                    'AMT': data.get('AMT'),
        }
        sign = datas['PID']+','+str(data['AMT'])+','+data['PRN']+','+data['BID']+','+data['UID']
        
        shasign = hmac.new(acquirer_obj.fonepay_secret_key.encode('utf-8'), sign.encode('utf-8'), hashlib.sha512).hexdigest()
        datas['DV'] = shasign
        _logger.info('_fonepay_verify_tx: Values received:\n%s', pprint.pformat(datas))
        r = requests.get(api_url,datas)
        resp = str(r.text)
        _logger.info('_fonepay_response: Values received:\n%s', pprint.pformat(r.text))
        responseXml = ET.fromstring(resp)
        response = responseXml.find('success')
        msg = responseXml.find('message')
        return {'status':response.text,
                'msg': msg.text,
                'UID':data.get('UID')}


    def _fonepay_form_validate(self,data):
        res = self._fonepay_verify_tx(data)
        status = res['status']
        # amount = tree["data"]["amount"]
        # currency = tree["data"]["currency"]
        if status == 'true':
            self.write({
                'date': fields.datetime.now(),
                'acquirer_reference': res.get('UID'),
                'state_message': res.get('msg')
            })
            self._set_transaction_done()
            # self.execute_callback()
            # if self.payment_token_id:
            #     self.payment_token_id.verified = True
            # return True
        else:
            error = res.get('msg')
            _logger.warn(error)
            self.sudo().write({
                'state_message': error,
                'acquirer_reference':res.get('UID'),
                'date': fields.datetime.now(),
            })
            self._set_transaction_cancel()
            return False

