# -*- coding: utf-8 -*-
{
	"name" : "Product Warranty Registration From Invoice",
	"version" : "13.0.1.1",
	"category" : "Industries",
	"depends" : ['base','product','account','sale','mail','stock'],
	"author": "Nexus Incorporation",
	'summary': 'Apps for product Warranty with serial number',
	"description": """
	""",
	"website" : "htpps://www.nexusgurus.com",
	"price": 100,
	"currency": 'EUR',
	"data": [
		'security/ir.model.access.csv',
		'report/warranty_receipt.xml',
		'report/warranty_receipt_menu.xml',
	
		'data/data.xml',
		'data/warranty_reg_email_data.xml',
		# 'views/my_warranty.xml',
		'views/warranty_view.xml',
		'views/invoice.xml',

	],
	'qweb': [
	],
	"auto_install": False,
	"installable": True,
	
	
}

