# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from datetime import datetime
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID
import logging
_logger = logging.getLogger(__name__)

class ProductTemplateCustom(models.Model):
	_inherit = 'product.template'
	
	under_warranty = fields.Boolean('Under Warranty',default=False)

class AccountMoveCustom(models.Model):
	_inherit="account.move"

	warranty_id = fields.Many2one('product.warranty.invoice',string="Warranty")
	is_warranty = fields.Boolean(string='Is Warranty',default=False,readonly=True)

	def create_warranty(self):
		for rec in self:
			if rec:
				if rec.move_type:
					if rec.move_type == 'out_invoice':
						lot_values = rec._get_invoiced_lot_values()
						if lot_values:
							for lot in lot_values:
								vals={}
								if rec.invoice_origin:
									sale_order = self.env['sale.order'].search([('name','=',rec.invoice_origin)])
									if sale_order:
										if sale_order.picking_ids:
											for picking in sale_order.picking_ids:
												if picking.date_done:
													vals['warranty_create_date'] = picking.date_done.date()
													vals['warranty_end_date'] = picking.date_done.date() + relativedelta(years=1)
													break


								vals['partner_id'] = rec.partner_id.id
								vals['email'] = rec.partner_id.email
								vals['phone'] = rec.partner_id.phone
								vals['mobile'] = rec.partner_id.mobile
								lot_record = self.env['stock.production.lot'].search([('id','=',lot['lot_id'])])
								product = lot_record.product_id.id
								product_record = lot_record.product_id
								_logger.info("=======%s=========%s==lot['lot_name']=====",product,lot['lot_id'])
								
								vals['invoice_id'] = rec.id
								if product_record.tracking == 'serial' and lot['lot_id'] and product and product_record.under_warranty:
									rec.is_warranty = True

									if not rec.warranty_id:
										vals_line = {}
										vals_line['product_serial_id'] = lot['lot_id']
										vals_line['product_id'] = product
										warranty_id = self.env['product.warranty.invoice'].create(vals)
										if warranty_id:
											rec.warranty_id = warranty_id.id
											vals_line['product_warranty_id'] = warranty_id.id
											warranty_line_id = self.env['product.warranty.line'].create(vals_line)
									else :
										vals_line = {}
										vals_line['product_serial_id'] = lot['lot_id']
										vals_line['product_id'] = product
										vals_line['product_warranty_id'] = rec.warranty_id.id
										warranty_line_id = self.env['product.warranty.line'].create(vals_line)

								else:
									pass


						else:
							# raise ValidationError(_('You Cannot Create Warranty Until Transfer is Created. Reason is Warranty Start Date Come from Transfer'))
							pass


class WarrantyLine(models.Model):
	_name = 'product.warranty.line'
	_inherit = ['mail.thread', 'mail.activity.mixin']
	_rec_name = "product_serial_id"

	product_warranty_id = fields.Many2one('product.warranty.invoice')
	product_serial_id = fields.Many2one('stock.production.lot',"Serial No", required=False,track_visibility='onchange')
	product_id = fields.Many2one('product.product',related='product_serial_id.product_id',string='Product', domain="[('under_warranty', '=', True)]",readonly=False)
	state = fields.Selection([('draft','Under Approval'),('approved','Approved'),('cancel','Canceled')],related='product_warranty_id.state',track_visibility='onchange' ,string='State',readonly=True)
	partner_id = fields.Many2one('res.partner',related='product_warranty_id.partner_id',string="Partner")
	warranty_create_date = fields.Date('Warranty Start Date',related='product_warranty_id.warranty_create_date')
	warranty_end_date = fields.Date('Warranty End Date',related='product_warranty_id.warranty_end_date')

#Warranty Registration Master
class WarrantyDetails(models.Model):
	_name = 'product.warranty.invoice'
	_order = 'id desc'
	_description = "Product Warranty"
	_inherit = ['mail.thread', 'mail.activity.mixin']


	name = fields.Char('Name',required=True, copy=False, readonly=True, states={'draft': [('readonly', False)]}, index=True, default=lambda self: _('New'))
	partner_id = fields.Many2one('res.partner', 'Customer', required=True,track_visibility='onchange',states={'draft': [('readonly', False)]})
	warranty_line_id = fields.One2many('product.warranty.line','product_warranty_id')
	phone = fields.Char('Phone',states={'draft': [('readonly', False)]})
	mobile= fields.Char('Mobile',states={'draft': [('readonly', False)]})
	email = fields.Char('Email',states={'draft': [('readonly', False)]})
	warranty_create_date = fields.Date('Warranty Start Date', default=fields.date.today(),track_visibility='onchange',states={'draft': [('readonly', False)]})
	warranty_end_date = fields.Date('Warranty End Date',track_visibility='onchange',states={'draft': [('readonly', False)]})
	invoice_id = fields.Many2one('account.move',string='Invoice')

	state = fields.Selection([('draft','Under Approval'),('approved','Approved'),('cancel','Canceled')], default='draft',copy=False,track_visibility='onchange' ,string='State',readonly=True)
	company_id = fields.Many2one('res.company', 'Company',default=lambda self: self.env.company.id,required=True,states={'draft': [('readonly', False)]})
 
	@api.onchange('partner_id')
	def customer_details(self):
		if self.partner_id:
			self.update({'phone': self.partner_id.phone, 'email': self.partner_id.email, 'mobile': self.partner_id.mobile})

	def action_confirm(self):
		for rec in self:
			if not rec.warranty_create_date or not rec.warranty_end_date:
				raise ValidationError(_('Please Enter the Start and End date Properly.'))
			elif rec.warranty_create_date > rec.warranty_end_date:
				raise ValidationError(_('Start Date Cannot Be greater than End Date.'))
			else:
				rec.write({'state':'approved'})
				# rec.product_serial_id.warranty_id = self.id
		template = self.env.ref('warranty_registration_from_invoice.email_template_warranty_registration')
		self.env['mail.template'].browse(template.id).send_mail(self.id,force_send=True,)

	def action_cancel(self):
		for rec in self:
			
			rec.write({'state':'cancel'})
			# template = self.env.ref('warranty_registration.email_template_warranty_registration_cancel')
			# self.env['mail.template'].browse(template.id).send_mail(rec.id)
	
	def action_set_to_draft(self):
		for rec in self:
			rec.write({'state':'draft'})
	
	

	@api.model
	def create(self, vals):
		vals['name'] = self.env['ir.sequence'].next_by_code('warranty.serial') or 'New'		
		result = super(WarrantyDetails, self).create(vals)

		return result

	def unlink(self):
		if self.filtered(lambda x: x.state in ('in_progress', 'cancel')):
			raise UserError(_('You can not delete a confirmed Warrany. Please Cancel and reset to Draft.'))
		return super(WarrantyDetails, self).unlink()



