{
    'name': 'model archive',
    'version': '1.0',
    'summary': 'archives the company.',
    'description' : 'This modules archives the company.',
    'category': 'companies',
    'author': 'Subhas',
    'depends': ['base'],
    'data': ['views/model_archive.xml'],
    'installable': True,
    'auto_install': False,

}
