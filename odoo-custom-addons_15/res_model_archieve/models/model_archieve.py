from odoo import api, fields, models


class archive_model(models.Model):
    _inherit = "ir.model"

    active = fields.Boolean(default=True)
