from odoo import api, fields, models,_
from odoo.exceptions import AccessError, UserError, ValidationError

class VisitorDetails(models.Model):
    _inherit = 'fo.visitor'


    @api.model
    def create(self,vals_list):
        if not vals_list.get('phone') and not vals_list.get('email'):
            raise UserError(_('Please Fill the Phone Or Email.'))
        return super(VisitorDetails, self).create(vals_list)
    
    
    @api.constrains('email', 'phone')
    def _check_description(self):
        for record in self:
            if not record.phone and not record.email:
                raise ValidationError("Please Fill the Phone Or Email.")