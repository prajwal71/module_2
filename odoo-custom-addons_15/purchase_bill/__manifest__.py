# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Create Different Bill for same partner',
    'version' : '1.1',
    'summary': 'To create different bill for same partner',
    'description': """
.
    """,
    'author': 'Nexus Incoporation',
    'depends' : ['purchase'],
    'data': [
          'security/res_groups.xml',
        'views/view.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
