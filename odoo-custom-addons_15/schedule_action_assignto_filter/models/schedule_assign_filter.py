# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.

from odoo import fields, models,api,_
import logging
_logger = logging.getLogger(__name__)
from odoo.exceptions import UserError, ValidationError
from dateutil.relativedelta import relativedelta


class MailActivityAssignInherit(models.Model):
    _inherit = 'mail.activity'
    

    extra_assign_to = fields.Many2many('res.users',string="Extra Assign Follower")
    flag = fields.Boolean(default=False)
    
    
    
    
    def action_close_dialog(self):
        user_list = []
        for rec in self:
            # res = super().action_close_dialog()
            context = dict(rec._context or {})
            active_id = context.get('default_res_id', False)
            _logger.info("==============================%s",active_id)
            active_model = context.get('default_res_model',False)
            _logger.info("==============================%s",active_model)    
            if active_id:
                task_obj = self.env[active_model].browse(active_id)
                _logger.info("==============================%s",task_obj)
                if task_obj:
                    ResObj = self.env['res.users'].search([('id','in',rec.extra_assign_to.ids)])
                    for dec in ResObj:
                        if dec.id not in rec.user_id.ids:
                            user_list.append(dec.id)
                    for user in user_list:
                        create_vals = {
                            'activity_type_id': rec.activity_type_id.id,
                            'summary': rec.summary,
                            'automated': True,
                            'note': rec.note,
                            'date_deadline': rec.date_deadline,
                            'res_model_id': self.env['ir.model'].search([('model', '=', active_model)]).id,
                            'res_id': active_id,
                            'user_id': user
                        }

                        # create_vals.update(act_values)
                        # self.env['mail.thread'].message_subscribe(partner_list)      
                        # self.env[rec.res_model].browse(rec.res_id).message_subscribe(partner_ids=[dec.id])
                        self.env['mail.activity'].create(create_vals)
        return {'type': 'ir.actions.act_window_close'}

        # return res                                
    


    # @api.constrains('extra_assign_to')
    # def _check_exist_assign_member_to_schedule(self):
    #     for rec in self:
    #         exist_assign_member_list = []
    #         exist_assign_member_list.append(rec.user_id)
    #         #   exist_assign_member_list.append(rec.)
    #         if rec.user_id.id in exist_assign_member_list:
    #             raise ValidationError(_('Select Extra User other than assign to user.'))
    #         exist_assign_member_list.append(rec.user_id.id)   
    
    @api.onchange('activity_type_id')
    def _onchange_activity_type_id(self):
        if self.activity_type_id:
            self.summary = self.activity_type_id.summary
            # Date.context_today is correct because date_deadline is a Date and is meant to be
            # expressed in user TZ
            base = fields.Date.context_today(self)
            if self.activity_type_id.delay_from == 'previous_activity' and 'activity_previous_deadline' in self.env.context:
                base = fields.Date.from_string(self.env.context.get('activity_previous_deadline'))
            self.date_deadline = base + relativedelta(**{self.activity_type_id.delay_unit: self.activity_type_id.delay_count})
            self.note = self.activity_type_id.default_note

class MailChannelPartnerInherit(models.Model):
    _inherit = 'mail.channel'

    @api.constrains('channel_last_seen_partner_ids')
    def _check_exist_member_in_line(self):
      for rec in self:
          exist_member_list = []
          for line in rec.channel_last_seen_partner_ids:
             if line.partner_id.id in exist_member_list:
                PartObj = self.env['res.partner'].search([('id','=',line.partner_id.id)])
                raise ValidationError(_('Member should be one per line.Please remove dublicate member %s'% PartObj.name))
             exist_member_list.append(line.partner_id.id)
          
    domain_filter_partners = fields.Many2many('res.partner',compute="get_channel_last_seen_partner_ids_domain_change")
    def get_channel_last_seen_partner_ids_domain_change(self):
        user_partner_ids = []
        resObj = self.env["res.users"].search([('share','=',False)])

        if resObj:
            for user_partner_id in resObj:
                user_partner_ids.append(user_partner_id.partner_id.id)
                # if user_partner_id.partner_id.id in self.domain_filter_partners.ids:
                #     raise ValidationError(_('Member should be one per line.'))
            self.write({
                                'domain_filter_partners' : [(6,0,user_partner_ids)]
                            })
        
        # return {'domain': {'partner_id': [('id', 'in', self.domain_filter_partners)]}}
        # return res
