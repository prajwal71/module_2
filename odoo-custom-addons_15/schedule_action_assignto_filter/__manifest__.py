# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Schedule action Assigned to Filter',
    'version' : '1.1',
    'summary': '''
    1.internal user is only shown in the field of assigned to,project_manager
    ,projec task,hr job responsible,hr recruitment responsible etc.public portal user are removed
    2.portal public member restriction except internal user
    3.multiple schedule assignment 

    ''',
    
    'author': 'Nexus Incorporation',
    'depends' : ['mail','project','hr','hr_recruitment'],
    'data': [
        
        'views/schedule_assign_filter_views.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
