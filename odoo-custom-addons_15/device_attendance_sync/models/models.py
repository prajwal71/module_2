# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, SUPERUSER_ID
import datetime
from datetime import datetime
from odoo.addons.hr_pyzk.controllers import controller as c
import logging
_logger = logging.getLogger(__name__)

class HrAttendanceExt(models.Model):
    _inherit = 'hr.attendance'

    flag = fields.Boolean(default=False)
    check = fields.Boolean(default=False)

    @api.model
    def create(self, vals):
        _logger.info('======================+%s', vals)
        if not vals.get('flag'):
            if 'str' in str(type(vals.get('check_in'))):
                d1 = vals.get('check_in')
                d2 = vals.get('check_in')
            else:
                d1 = datetime.strftime(vals.get('check_in'), "%Y-%m-%d 00:00:00")
                d2 = datetime.strftime(vals.get('check_in'), "%Y-%m-%d 23:59:59")
                
            DeviceAttendance = self.env['device.attendances']
            device = self.env['devices'].sudo().search([],limit=1)
            device_attendance_id = DeviceAttendance.sudo().search([('employee_id', '=', vals.get('employee_id')),
                                                                ('device_datetime', '>=', d1),('device_datetime', '<=', d2),
                                                                ('device_punch', '=', 'checkin')], limit=1)
            if not device_attendance_id:
                dev_user = self.env['device.users'].sudo().search([('employee_id','=',vals.get('employee_id'))],limit=1)
                if dev_user:
                    emp_id = dev_user.employee_id
                    if emp_id:
                        shift_id = emp_id.resource_calendar_id.id
                    DeviceAttendance.sudo().create({
                                    'device_user_id': dev_user.id,
                                    'flag': True,
                                    'device_datetime': vals.get('check_in'),
                                    'channel': 'man',
                                    'employee_id': emp_id.id,
                                    'state': 'nonapp',
                                    'attendance_state': 'notlogged',
                                    'device_punch': 'checkin',
                                    'device_id': device.id,
                                    'shift_id': shift_id,
                                })        
        return super(HrAttendanceExt, self).create(vals)

    # @api.model
    def write(self, vals):
        for rec in self:
            if vals.get('address'):
                Atnd = self.env['device.attendances'].sudo().search([('employee_id', '=', rec.employee_id.id), ('device_datetime', '=', rec.check_in), ('device_punch', '=', 'checkin'), ('flag','=', True)], limit=1)
                if Atnd:
                    Atnd.sudo().write({
                        'address': vals.get('address'),
                    })
            if vals.get('check_out') and vals.get('check'):
                DeviceAttendance = self.env['device.attendances']
                device = self.env['devices'].sudo().search([],limit=1)
                dev_user = self.env['device.users'].sudo().search([('employee_id','=',rec.employee_id.id)],limit=1)
                if dev_user:
                    emp_id = dev_user.employee_id
                    if emp_id:
                        shift_id = emp_id.resource_calendar_id.id
                    DeviceAttendance.sudo().create({
                                    'device_user_id': dev_user.id,
                                    'flag': True,
                                    'device_datetime': vals.get('check_out'),
                                    'channel': 'man',
                                    'address': rec.address,
                                    'employee_id': emp_id.id,
                                    'state': 'nonapp',
                                    'attendance_state': 'notlogged',
                                    'device_punch': 'checkout',
                                    'device_id': device.id,
                                    'shift_id': shift_id,
                                })        
        return super(HrAttendanceExt, self).write(vals)

class DeviceAttendanceExt(models.Model):
    _inherit = 'device.attendances'

    flag = fields.Boolean(default=False)
    address = fields.Text('Login Location', readonly=True)

    @api.model
    def create(self, vals):
        if not vals.get('flag'):            
            HrAttendance = self.env['hr.attendance']
            if 'str' in str(type(vals.get('device_datetime'))):
                date_time = datetime.strptime(vals.get('device_datetime'), "%Y-%m-%d %H:%M:%S")
                date_time = datetime.strftime(date_time, "%Y-%m-%d 00:00:00")
            else:
                date_time = datetime.strftime(vals.get('device_datetime'), "%Y-%m-%d 00:00:00")
            device_attendance_id = HrAttendance.sudo().search([('employee_id', '=', vals.get('employee_id')),
                                                                ('check_in', '>=', date_time)], limit=1)
            if not device_attendance_id:
                if vals.get('device_punch') == 'checkin':
                    HrAttendance.sudo().create({
                                    'employee_id': vals.get('employee_id'),
                                    'flag': True,
                                    'check_in': vals.get('device_datetime')
                                })
            else:
                if vals.get('device_punch') == 'checkout':
                    device_attendance_id.sudo().write({
                                    'check_out': vals.get('device_datetime'),
                                    'check': True,
                                })      
        return super(DeviceAttendanceExt, self).create(vals)

