# -*- coding: utf-8 -*-
{
    'name': "device_attendance_sync",

    'summary': """
        Sync Device Attendance and Attendance module.""",

    'description': """
        Sync Device Attendance and Attendance module.
    """,

    'author': "Nexus Incorporation",
    'license': 'LGPL-3',
    'website': "http://nexusgurus.com",
    'category': 'Human Resources',
    'version': '15.0.0.1',
    'depends': ['hr_pyzk', 'hr_attendance'],
    'data': [
        'views/views.xml'
    ]
}
