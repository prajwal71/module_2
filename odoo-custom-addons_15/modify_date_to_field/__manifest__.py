# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'update date',
    'version' : '1.1',
    'summary': 'modify date in hr leave module in tree view',
    'description': """
.
    """,
    'author': 'Nexus Incoporation',
    'depends' : ['hr_holidays'],
    'data': [
        # 'security/backdate_security.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
