from odoo import api, fields, models


class ProductPricelistItem(models.Model):
    _inherit = "product.pricelist.item"

    product_public_categ_id = fields.Many2one(
        comodel_name='product.public.category',
        string='Product Public Category',
        ondelete='cascade')

    applied_on = fields.Selection(selection_add=[
        ('21_product_public_categ', 'Product Public Category')],
        ondelete={'21_product_public_categ': 'cascade'})

    @api.onchange('applied_on')
    def _onchange_applied_on(self):
        """ clean fields based on applied on. """
        if self.applied_on != '0_product_variant':
            self.product_id = False
        if self.applied_on != '1_product':
            self.product_tmpl_id = False
        if self.applied_on != '2_product_category':
            self.categ_id = False
        if self.applied_on != '21_product_public_categ':
            self.product_public_categ_id = False
       

