from odoo import api, fields, models
import logging

_logger = logging.getLogger(__name__)



class ApplicantInherit(models.Model):

    _inherit = 'hr.applicant'   
                
    def create_employee_from_applicant(self):
        result = super(ApplicantInherit, self).create_employee_from_applicant()
        if self.partner_id:
            PartObj = self.env['res.partner'].browse(self.partner_id.id)
            tags_ids = PartObj.category_id.ids
            catObj = self.env['res.partner.category'].search([('name','ilike',"Applicant")],limit=1)
            catObjj = self.env['res.partner.category'].search([('name','ilike',"Sagar Employee")],limit=1)
            for dec in tags_ids:
                if dec == catObj.id:
                    PartObj.write({'category_id': [( 3, dec)]})
            if catObjj:
                PartObj.write({'category_id': [( 4, catObjj.id)]})
        return result
            
        
    def website_form_input_filter(self, request, values):
        if 'partner_name' in values:
            values.setdefault('name', '%s\'s Application' % values['partner_name'])
            _logger.info("=============================%s==========",values)
            catObj = self.env['res.partner.category'].sudo().search([('name','=',"Applicant")],limit=1)
           
            if catObj:
                resObj = self.env['res.partner'].sudo().create({
                    'name': values['partner_name'],
                    'mobile': values['partner_phone'],
                    'email': values['email_from'],
                    'category_id': catObj.ids
                })
            else:
                resObj = self.env['res.partner'].sudo().create({
                    'name': values['partner_name'],
                    'mobile': values['partner_phone'],
                    'email': values['email_from'],
                })             
            values.setdefault('partner_id',resObj.id)
            
        return values
    
    def write(self, vals):
        res = super().write(vals)
        if self.partner_id and self.partner_name:
            self.partner_id.name = self.partner_name
        if self.partner_id and self.partner_mobile and not self.partner_id.mobile:
            self.partner_id.mobile = self.partner_mobile
        return res



class VisitorDetailsInherit(models.Model):
    _inherit = 'fo.visitor'

    contact_tags = fields.Many2many('res.partner.category',string="contact tags")

    def create_fcontact(self):
        li_obj = self.env['res.partner']

        if self.contact_tags:
            li_obj.create({
                            'name':self.name,
                            'email':self.email,
                            'phone':self.phone,
                            'street':self.street,
                            'street2':self.street2,
                            'city':self.city,
                            'state_id':self.state_id.id,
                            'country_id':self.country_id.id,
                            'zip':self.zip,
                            'parent_id':self.company_info.id,
                            'category_id': self.contact_tags
                        })
        else:
            li_obj.create({
                        'name':self.name,
                        'email':self.email,
                        'phone':self.phone,
                        'street':self.street,
                        'street2':self.street2,
                        'city':self.city,
                        'state_id':self.state_id.id,
                        'country_id':self.country_id.id,
                        'zip':self.zip,
                        'parent_id':self.company_info.id,
                    })

        self.bool_field = True


class HrEmployeeInherit(models.Model):
    _inherit = 'hr.employee'

    is_ex_employee = fields.Boolean(default=False,compute='get_active_status_info',store=True)

   

    @api.depends('active')
    def get_active_status_info(self):
        for rec in self:
            if rec.active == False:
                if rec.address_home_id:
                    PartObj = self.env['res.partner'].browse(rec.address_home_id.id)

                    tags_ids = PartObj.category_id.ids
                    catObj = self.env['res.partner.category'].search([('name','ilike',"Sagar Employee")],limit=1)
                    catObjj = self.env['res.partner.category'].search([('name','ilike',"Ex-Employee")],limit=1)

                    for dec in tags_ids:
                        if dec == catObj.id:
                            PartObj.write({'category_id': [( 3, dec)]})
                    if catObjj:
                        PartObj.write({'category_id': [( 4, catObjj.id)]})

                