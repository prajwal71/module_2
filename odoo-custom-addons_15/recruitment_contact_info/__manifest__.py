# -*- coding: utf-8 -*-

{
    'name': 'Recruitment Contact Set',
    'version': '13.0.1.0.0',
    'category': 'Recruitment',
    'summary': "Recruitment Contact Set",
    'author': 'Manoj Khadka',

    'depends': ['base','hr','hr_recruitment','website_hr_recruitment','front_office_management'
            
                ],
    'data': [
        'views/partner_views.xml'
    ],
    'license': 'AGPL-3',
    'application': True,
    'installable': True,
    'auto_install': False,
}
