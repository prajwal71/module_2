# ©  2008-2021 Deltatech
# See README.rst file on addons root folder for license details


from odoo import fields, models
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.website.controllers.main import QueryURL

class category(models.Model):
    _inherit = 'website'

    login_background = fields.Binary(string="Login Page Background")

    def get_all_categories_url(self, category):
        url = "/shop"
        if len(category)>0:
            url = "/shop/category/%s" % slug(category)
        return url

    def get_all_categories(self):
        keep = QueryURL("/", category=0)
        return {
            'keep': keep
        }

    def get_all_brand_url(self, attrib):
        url = "/shop"
        if len(attrib)>0:
            url = "/shop/attrib/%s" % slug(attrib)
        return url

    def get_all_brand(self):
        keep = QueryURL("/", attrib=0)
        return {
            'keep': keep
        }