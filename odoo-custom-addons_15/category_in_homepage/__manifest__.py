# ©  2008-2021 Deltatech
#              Dorin Hongu <dhongu(@)gmail(.)com
# See README.rst file on addons root folder for license details
{
    "name": "Product Category In Homepage",
    "category": "Website/Website",
    "summary": "City extension",
    "version": "15.0.1.0.0",
    "author": "Nexus",
    "license": "LGPL-3",
    "website": "https://www.terrabit.ro",
    "depends": ["portal", "website_sale","website"],
    "data": [
        "views/homepage.xml"
        # "views/portal.xml",
        # "views/checkout.xml",
        #     "views/assets.xml"
    ],
    "images": ['static/description/Banner.png'],
    "installable": True,
    "development_status": "Mature",
    "maintainers": ["dhongu"],
    "assets": {
       
    },
}
