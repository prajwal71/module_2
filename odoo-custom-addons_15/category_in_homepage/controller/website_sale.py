# ©  2008-2021 Deltatech
#              Dorin Hongu <dhongu(@)gmail(.)com
# See README.rst file on addons root folder for license details

from odoo.http import request
from odoo import fields, http, SUPERUSER_ID, tools, _
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.http import route, request
from odoo.exceptions import AccessError, MissingError, ValidationError
from werkzeug.exceptions import Forbidden, NotFound
import logging
_logger = logging.getLogger(__name__)
from odoo.addons.http_routing.models.ir_http import slug

class WebsiteSaleCity(WebsiteSale):
    def get_all_categories(self, category, cate_list=[], url=False, key=1):
        try:
            id = int(category)
            category = request.env['product.public.category'].browse(id)
        except ValueError:
            category = category
        cate_list.append({"name": category.name, "url": url, "key": key})
        if len(category.parent_id)>0:
            url = "/shop/category/%s" % slug(category.parent_id)
            self.get_all_categories(category.parent_id, cate_list, url, key+1)
        return sorted(cate_list, key = lambda i: i.get('key') if i.get('key') else True, reverse=True)


    def get_all_brand(self, attrib, attrib_list=[], url=False, key=1):
        try:
            id = int(attrib)
            attrib = request.env['product.attribute'].browse(id)
        except ValueError:
            attrib = attrib
        attrib_list.append({"name": attrib.name})
        if len(attrib)>0:
            # https://platformshub.com.np/shop?search=&attrib=1-893
            url = "/shop/?search=&attrib=%s"% attrib.id
            self.get_all_brand(attrib.name, attrib_list, url, key+1)
        return sorted(attrib_list, key = lambda i: i.get('key') if i.get('key') else True, reverse=True)


  
