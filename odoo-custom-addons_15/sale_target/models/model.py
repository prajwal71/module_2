
from email.policy import default
from odoo import fields, models,api,_
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta
import logging
_logger = logging.getLogger(__name__)


class DailyRecord(models.Model):
    _name ='daily.record'

    date = fields.Date('Date')
    saleperson = fields.Many2one('res.users')
    gap = fields.Float("Gap Value",default=0.0)
    target = fields.Float("Target",default=0.0)
    acheive = fields.Float("Acheive",default=0.0)

class WeeklyRecord(models.Model):
    _name ='weekly.record'

    date = fields.Date('Date')
    saleperson = fields.Many2one('res.users')
    gap = fields.Float("Gap Value",default=0.0)
    target = fields.Float("Target",default=0.0)
    acheive = fields.Float("Acheive",default=0.0)

class MonthlyRecord(models.Model):
    _name ='monthly.record'

    date = fields.Date('Date')
    saleperson = fields.Many2one('res.users')
    gap = fields.Float("Gap Value",default=0.0)
    target = fields.Float("Target",default=0.0)
    acheive = fields.Float("Acheive",default=0.0)

class YearlyRecord(models.Model):
    _name ='yearly.record'

    date = fields.Date('Date')
    saleperson = fields.Many2one('res.users')
    gap = fields.Float("Gap Value",default=0.0)
    target = fields.Float("Target",default=0.0)
    acheive = fields.Float("Acheive",default=0.0)

class IssueSaleperson(models.Model):
    _name ='issue.saleperson'

    saleperson = fields.Many2one('res.users')
    date = fields.Datetime()
    comment = fields.Char()


class KsNinjaExt(models.Model):
    _inherit = 'ks_dashboard_ninja.item'
  
    is_daily = fields.Boolean(default=False,store=True)
    is_yesterday = fields.Boolean(default=False,store=True)
    is_weekly = fields.Boolean(default=False,store=True)
    is_monthly = fields.Boolean(default=False,store=True)
    is_yearly = fields.Boolean(default=False,store=True)
    gap = fields.Float("Gap Value",default=0.0)
    standard_target = fields.Float("Uniform Target",default=0.0)
    saleperson = fields.Many2one('res.users')
    responsible_person = fields.Many2many('res.users')
    temp = fields.Float('Temporary value')
    temp_name = fields.Char('Temp')
# 2am
    @api.model
    def _set_yesterday(self):
        _logger.info("---1---%s----",self)
        records = self.search([])
        for rec in records:
            # if rec:
            _logger.info("---1---234----")
            if rec.is_yesterday:
                _logger.info("---1---2----")
                record = self.env['ks_dashboard_ninja.item'].search([('saleperson','=',rec.saleperson.id),('is_daily','=',True)],limit=1)
                if record:
                    _logger.info("---3-------")
                    rec.ks_standard_goal_value = record.ks_standard_goal_value
                    rec.gap = record.ks_standard_goal_value - rec.ks_record_count
                    # rec.ks_standard_goal_value = record.ks_standard_goal_value
                    record.ks_standard_goal_value = record.standard_target + rec.gap
                    rec.temp = record.ks_standard_goal_value
                    date = fields.Datetime.now().date() - timedelta(days=1)
                    vals={}
                    rec.temp_name = rec.saleperson.name
                    user = rec.saleperson
                    vals['date'] = date
                    vals['saleperson'] = rec.saleperson.id
                    vals['target'] = rec.ks_standard_goal_value
                    vals['gap'] = rec.gap
                    vals['acheive'] = rec.ks_record_count
                    self.env['daily.record'].create(vals)
                    template = self.env.ref('sale_target.daily_sale_targe', raise_if_not_found=False)
                    template_values = {
                        # 'email_from':self.env.company.name + self.env.company.email,
                        'email_from' : self.env.company.name + '{{user.company_id.email}}',
                        'email_to': rec.saleperson.login,
                        'subject':'Daily Sale Target Report of ' + rec.saleperson.name,
                       'email_cc':str([user.login for user in rec.responsible_person]).replace('[', '').replace(']', '').replace("'", ''),
                        # 'email_cc': False,
                        'auto_delete': True,
                        # 'partner_to': False,
                        # 'scheduled_date': False,
                        'lang':user.partner_id.lang,
                        # 'body_html':'Your Yesterday Target was ' + str(rec.ks_standard_goal_value) + '. You Have achieve ' + str(rec.ks_record_count) +'and Today target is ' + str(record.ks_standard_goal_value)
                        }
                    template.write(template_values)
                    
                    template.send_mail(rec.id, force_send=True, raise_exception=True)
                    # template.send_mail(user.id, force_send=True, raise_exception=True)

                else:
                    vals={}
                    sale_person = self.env['res.users'].search([('id','=',rec.saleperson.id)])
                    vals['saleperson'] = rec.saleperson
                    vals['date'] = datetime.now()
                    vals['comment'] = 'unable to set Yesterday target of ' +  sale_person.name + datetime.now()
                    backlog = self.env['issue.saleperson'].create(vals)

            else:
                pass

     
    
            
                  # 11pm
    @api.model
    def _set_weekly(self):
        records = self.search([])
        for rec in records:
            _logger.info("=========weekly")
            if rec.is_weekly:
                # record = self.env['ks_dashboard_ninja.item'].search([('saleperson','=',rec.saleperson),('is_yesterday','=',True)],limit=1)
                # if record:
                vals={}
                user_id = rec.saleperson
                email_target = rec.ks_standard_goal_value
                rec.temp = rec.ks_standard_goal_value
                vals['target'] = rec.ks_standard_goal_value
                rec.gap = rec.ks_standard_goal_value - rec.ks_record_count
                rec.ks_standard_goal_value = rec.standard_target + rec.gap
                date = fields.Datetime.now().date()
                
                vals['date'] = date
                vals['saleperson'] = rec.saleperson.id
                rec.temp_name = rec.saleperson.name  
                _logger.info("=================%s==rec.temp_name===",rec.temp_name)
                vals['gap'] = rec.gap
                vals['acheive'] = rec.ks_record_count
                self.env['weekly.record'].create(vals)
                template = self.env.ref('sale_target.weekly_sale_targe', raise_if_not_found=False)
                template_values = {
                    # 'email_from':self.env.company.name + self.env.company.email,
                    'email_from' : self.env.company.name + '{{user.company_id.email}}',
                    'email_to': rec.saleperson.login,
                    'subject':'Weekly Sale Target Report of ' + rec.saleperson.name,
                    # 'email_cc': [x.login for x in rec.responsible_person],
                    'email_cc':str([user.login for user in rec.responsible_person]).replace('[', '').replace(']', '').replace("'", ''),
                    'auto_delete': False,
                    # 'partner_to': False,
                    # 'scheduled_date': False,
                    'lang':user_id.partner_id.lang,
                    # 'body_html':'Your Previous Week Target was ' + str(email_target) + '. You Have achieve ' + str(rec.ks_record_count) +'and This Week target is ' + str(rec.ks_standard_goal_value)
                    }
                template.write(template_values)
                
                template.send_mail(rec.id, force_send=True, raise_exception=True)

                # else:
                #     vals={}
                #     sale_person = self.env['res.users'].search([('id','=',rec.saleperson)])
                #     vals['saleperson'] = rec.saleperson
                #     vals['date'] = datetime.now()
                #     vals['comment'] = 'unable to set Weekly target of ' +  sale_person.name + datetime.now()
                #     backlog = self.env['issue.saleperson'].create(vals)

            else:
                pass
    #    11pm 
    @api.model
    def _set_monthly(self):
        records = self.search([])
        for rec in records:
            _logger.info("==========monthly")
            if rec.is_monthly:
                vals={}
                user = rec.saleperson
                email_target = rec.ks_standard_goal_value
                rec.temp = rec.ks_standard_goal_value
                vals['target'] = rec.ks_standard_goal_value
                rec.gap = rec.ks_standard_goal_value - rec.ks_record_count
                rec.ks_standard_goal_value = rec.standard_target + rec.gap
                date = fields.Datetime.now().date()
                rec.temp_name = rec.saleperson.name
                vals['date'] = date
                vals['saleperson'] = rec.saleperson.id
                
                vals['gap'] = rec.gap
                vals['acheive'] = rec.ks_record_count
                self.env['monthly.record'].create(vals)
                template = self.env.ref('sale_target.monthly_sale_targe', raise_if_not_found=False)
                template_values = {
                    # 'email_from':self.env.company.name + self.env.company.email,
                    'email_from' : self.env.company.name + '{{user.company_id.email}}',
                    'email_to': rec.saleperson.login,
                    'subject':'Monthly Sale Target Report of ' + rec.saleperson.name,
                    'email_cc':str([user.login for user in rec.responsible_person]).replace('[', '').replace(']', '').replace("'", ''),
                    # 'email_cc': False,
                    'auto_delete': True,
                    # 'partner_to': False,
                    # 'scheduled_date': False,
                    'lang':user.partner_id.lang,
                    # 'body_html':'Your Previous Month Target was ' + str(email_target) + '. You Have achieve ' + str(rec.ks_record_count) +'and This Month target is ' + str(rec.ks_standard_goal_value)
                    }
                template.write(template_values)
                
                template.send_mail(rec.id, force_send=True, raise_exception=True)
                # template.send_mail(user.id, force_send=True, raise_exception=True)
            else:
                pass

# 11pm

    @api.model
    def _set_yearly(self):
        records = self.search([])
        for rec in records:
            _logger.info("==========yearly")
            if rec.is_yearly:
                vals={}
                user = rec.saleperson
                email_target = rec.ks_standard_goal_value
                rec.temp = rec.ks_standard_goal_value
                vals['target'] = rec.ks_standard_goal_value
                # record = self.env['ks_dashboard_ninja.item'].search([('saleperson','=',rec.saleperson),('is_yesterday','=',True)],limit=1)
                # if record:
                rec.gap = rec.ks_standard_goal_value - rec.ks_record_count
                rec.ks_standard_goal_value = rec.standard_target + rec.gap  
                date = fields.Datetime.now().date()
                
                vals['date'] = date
                vals['saleperson'] = rec.saleperson.id
                rec.temp_name = rec.saleperson.name
                vals['gap'] = rec.gap
                vals['acheive'] = rec.ks_record_count
                self.env['daily.record'].create(vals)
                template = self.env.ref('sale_target.yearly_sale_targe', raise_if_not_found=False)
                template_values = {
                    # 'email_from':self.env.company.name + self.env.company.email,
                    'email_from' : self.env.company.name + '{{user.company_id.email}}',
                    'email_to': rec.saleperson.login,
                    'subject':'Monthly Sale Target Report of ' + rec.saleperson.name,
                    # 'email_cc': False,
                    'email_cc':str([user.login for user in rec.responsible_person]).replace('[', '').replace(']', '').replace("'", ''),
                    'auto_delete': True,
                    # 'partner_to': False,
                    # 'scheduled_date': False,
                    'lang':user.partner_id.lang,
                    # 'body_html':'Your Previous Year Target was ' + str(email_target) + '. You Have achieve ' + str(rec.ks_record_count) +'and This Year target is ' + str(rec.ks_standard_goal_value)
                    }
                template.write(template_values)
                template.send_mail(rec.id, force_send=True, raise_exception=True)
                # template.send_mail(user.id, force_send=True, raise_exception=True)
            else:
                pass

