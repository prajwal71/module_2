# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Sale Target',
    'version' : '1.1',
    'summary': 'Sale Target  ',
    'description': """

====================

    """,
    'author': 'Nexus',
    'depends' : ['base','ks_dashboard_ninja'],
    'data': [
             'security/groups.xml',
             'security/ir.model.access.csv',
             'data/email.xml',
             'data/ir_cron.xml',
             'views/view.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
