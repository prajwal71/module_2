# -*- coding: utf-8 -*-


import re
from odoo import models, api, fields, _
from odoo.exceptions import AccessDenied, AccessError, UserError, ValidationError
from odoo.http import request
import json
import logging
_logger = logging.getLogger(__name__)


class ResUser(models.Model):
    _inherit = "res.users"

    mobile = fields.Char(store=True, related="partner_id.mobile")

    _sql_constraints = [
        ('mobile_key', 'UNIQUE (mobile)',
         'You can not have two users with the same mobile number !')
    ]

    @api.model_create_multi
    def create(self, vals_list):
        users = super(ResUser, self.with_context(
            default_customer=False)).create(vals_list)

        for user in users:
            if any('mobile' in d for d in vals_list[0]):
                user.mobile = vals_list[0]['mobile']
                user.partner_id.mobile = vals_list[0]['mobile']
                _logger.info(
                    "................ DEF CREATE..............%s...............................", user.mobile)
        return users

    def reset_password(self, login):
        """ retrieve the user corresponding to login (login or email),
                and reset their password
        """
        users = self.search(
            ['|', ('login', '=', login), ('mobile', '=', login)])
        if not users:
            users = self.search(
                ['|', ('email', '=', login), ('mobile', '=', login)])
        if len(users) != 1:
            raise Exception(_('Reset password: invalid username or email'))
        return users.action_reset_password()

    @api.model
    def _get_login_domain(self, login):
        a=login
        login=a.replace('-','')
        _logger.info('............LOGIN...................%s..............LOGIN', login)
        return ['|', ('login', '=', login), ('mobile', '=', login)]


class ResPartner(models.Model):

    _inherit = "res.partner"

    @api.model
    def signup_retrieve_info(self, token):
        partner = self._signup_retrieve_partner(token, raise_exception=True)
        res = {'db': self.env.cr.dbname}
        if partner.signup_valid:
            res['token'] = token
            res['name'] = partner.name
        if partner.user_ids:
            res['login'] = partner.user_ids[0].login
            res['mobile'] = partner.user_ids[0].partner_id.mobile
        else:
            res['email'] = res['login'] = partner.email or ''
            res['mobile'] = partner.user_ids[0].partner_id.mobile or ''
        return res


class ResPartner(models.Model):

    _inherit = "res.partner"

    mobile = fields.Char()

    _sql_constraints = [
        ('mobile_key', 'UNIQUE (mobile)',
         'You can not have two users with the same mobile number !')
    ]

    @api.onchange('mobile', 'country_id', 'company_id')
    def _onchange_mobile_validation(self):
        _logger.info(
            ">>>>>>>>>>>>>>>>>>>>>>>>%s<<<<<<<<<<<<<<<<<<<<<<<<<", self)

    @api.model_create_multi
    def create(self, vals_list):
        if self.env.context.get('import_file'):
            self._check_import_consistency(vals_list)
        for vals in vals_list:
            if vals.get('website'):
                vals['website'] = self._clean_website(vals['website'])
            if vals.get('parent_id'):
                vals['company_name'] = False
            if vals.get('mobile'):
                vals['mobile'] = '+977' + vals['mobile']

        partners = super(ResPartner, self).create(vals_list)
        _logger.info(
            ">>>>>>>>>>>>>>>>>>>>>PARTNER>>>%s<<<<<<<PARTNER<<<<<<<<<<<<<<<<<<", partners)
        if self.env.context.get('_partners_skip_fields_sync'):
            return partners

        for partner, vals in zip(partners, vals_list):
            partner._fields_sync(vals)
            partner._handle_first_contact_creation()
        return partners

    def write(self, vals):
        if vals.get('active') is False:
            # DLE: It should not be necessary to modify this to make work the ORM. The problem was just the recompute
            # of partner.user_ids when you create a new user for this partner, see test test_70_archive_internal_partners
            # You modified it in a previous commit, see original commit of this:
            # https://github.com/odoo/odoo/commit/9d7226371730e73c296bcc68eb1f856f82b0b4ed
            #
            # RCO: when creating a user for partner, the user is automatically added in partner.user_ids.
            # This is wrong if the user is not active, as partner.user_ids only returns active users.
            # Hence this temporary hack until the ORM updates inverse fields correctly.
            self.invalidate_cache(['user_ids'], self._ids)
            for partner in self:
                if partner.active and partner.user_ids:
                    raise ValidationError(
                        _('You cannot archive a contact linked to a portal or internal user.'))
        # res.partner must only allow to set the company_id of a partner if it
        # is the same as the company of all users that inherit from this partner
        # (this is to allow the code from res_users to write to the partner!) or
        # if setting the company_id to False (this is compatible with any user
        # company)
        if vals.get('website'):
            vals['website'] = self._clean_website(vals['website'])
        if vals.get('parent_id'):
            vals['company_name'] = False        
        if 'company_id' in vals:
            company_id = vals['company_id']
            for partner in self:
                if company_id and partner.user_ids:
                    company = self.env['res.company'].browse(company_id)
                    companies = set(user.company_id for user in partner.user_ids)
                    if len(companies) > 1 or company not in companies:
                        raise UserError(
                            ("The selected company is not compatible with the companies of the related user(s)"))
                if partner.child_ids:
                    partner.child_ids.write({'company_id': company_id})
        if vals.get('mobile'):
            pattern_number = "+977"
            if pattern_number in vals['mobile']:
                vals['mobile'] = vals['mobile']
            else:
                vals['mobile'] = '+977'+vals['mobile']
        result = True
        # To write in SUPERUSER on field is_company and avoid access rights problems.
        if 'is_company' in vals and self.user_has_groups('base.group_partner_manager') and not self.env.su:
            result = super(ResPartner, self.sudo()).write({'is_company': vals.get('is_company')})
            del vals['is_company']
        result = result and super(ResPartner, self).write(vals)
        for partner in self:
            if any(u.has_group('base.group_user') for u in partner.user_ids if u != self.env.user):
                self.env['res.users'].check_access_rights('write')
            partner._fields_sync(vals)
        return result