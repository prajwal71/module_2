$(document).ready(function () {
  var input = document.querySelector("#mobile");


  // initialise plugin
  if (input != null) {
    var iti = window.intlTelInput(input, {
      geoIpLookup: function (callback) {
        $.get("https://ipinfo.io", function () {}, "jsonp").always(function (
          resp
        ) {
          var countryCode = resp && resp.country ? resp.country : "";
          callback(countryCode);
        });
      },

      initialCountry: "np",

      onlyCountries: ["np"],

      utilsScript:
        "/mobile_number_signup_app/static/src/intl-tel-input-master/build/js/utils.js",
    });

    var errorMsg = document.querySelector("#error-msg"),
      validMsg = document.querySelector("#valid-msg");

    var errorMap = [
      "Invalid number",
      "Invalid country code",
      "Too short",
      "Too long",
      "Invalid number",
    ];
    var countryData = iti.getSelectedCountryData();

    var reset = function () {
      input.classList.remove("error");
      errorMsg.innerHTML = "";
      errorMsg.classList.add("hide");
      validMsg.classList.add("hide");
    };

    // on blur: validate
    input.addEventListener("blur", function () {
      reset();
      if (input.value.trim()) {
        if (iti.isValidNumber()) {
          validMsg.classList.remove("hide");
          document.getElementById("sgn_up").disabled = false;
        } else {
          input.classList.add("error");

          document.getElementById("sgn_up").disabled = true;
          var errorCode = iti.getValidationError();
          console.log("err");
          console.log(errorCode);
          if (errorCode < 0 || errorCode > 4) {
            errorCode = 0;
          }
          errorMsg.innerHTML = errorMap[errorCode];
          errorMsg.classList.remove("hide");
        }
      }
    });
    input.addEventListener("mouseout", function () {
      reset();
      if (input.value.trim()) {
        if (iti.isValidNumber()) {
          validMsg.classList.remove("hide");

          document.getElementById("sgn_up").disabled = false;
        } else {
          input.classList.add("error");

          document.getElementById("sgn_up").disabled = true;
          var errorCode = iti.getValidationError();
          console.log("err");
          console.log(errorCode);
          if (errorCode < 0 || errorCode > 4) {
            errorCode = 0;
          }
          errorMsg.innerHTML = errorMap[errorCode];
          errorMsg.classList.remove("hide");
        }
      }
    });
    
    
    var sign_up = document.getElementById("confirm_password");
      sign_up.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
         event.preventDefault();
         document.getElementById("sgn_up").click();
        }
      });

    // on keyup / change flag: reset
    input.addEventListener("change", reset);
    input.addEventListener("keyup", reset);
  }
});
