# -*- coding: utf-8 -*-

{
	'name' : 'Sign Up With Mobile Number (Nexus)',
	'author': "Nexus Incorporations",
	'version' : '13.0.1.0',
	# 'live_test_url':'https://youtu.be/MYTL6XSAtTk',
	'category' : 'Website',
	'website': "http://www.nexusgurus.com",
    
	'summary' : 'Sign up option with Mobile Number option on Signup login with mobile number Signup mobile number with Password Reset Mobile Number Option in Login Mobile Number Option in sign up Mobile Number Option in Reset Password  sign up with mobile number sign up',
	'description' : """
		This module helps to add mobile number options in login screen, signup screen and Password reset Screen.
	""",
	'depends' : ['base','auth_signup','auth_oauth', 'portal'],
    "license" : "OPL-1",
	'data' : [
		'views/template.xml',
	],
	'qweb' : [],
	'demo' : [],
	'installable' : True,
	'auto_install' : False,
	'price': 20,
	'currency': "EUR",
}
