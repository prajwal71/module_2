import odoo
import odoo.modules.registry
import json
import logging
from odoo.tools.translate import _
from odoo import http
from odoo.addons.auth_signup.models.res_users import SignupError
from odoo.exceptions import UserError
from odoo.addons.auth_signup.controllers.main import AuthSignupHome
from odoo.http import request
_logger = logging.getLogger(__name__)


class LoginHome(AuthSignupHome):

    @http.route('/web/signup', type='http', auth='public', website=True, sitemap=False)
    def web_auth_signup(self, *args, **kw):
        qcontext = self.get_auth_signup_qcontext()

        if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            raise werkzeug.exceptions.NotFound()

        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                mob = kw['mobile']
                qcontext['mobile'] = '+977' + mob
                pattern_dash = "-"
                if pattern_dash in mob:
                    dash=mob.replace('-','')
                    qcontext['mobile']='+977' + dash
                else:
                    qcontext['mobile'] = '+977' + mob

                self.do_signup(qcontext)
                # Send an account creation confirmation email
                if qcontext.get('token'):
                    user_sudo = request.env['res.users'].sudo().search(
                        [('login', '=', qcontext.get('login'))])
                    template = request.env.ref(
                        'auth_signup.mail_template_user_signup_account_created', raise_if_not_found=False)
                    if user_sudo and template:
                        template.sudo().with_context(
                            lang=user_sudo.lang,
                            auth_login=werkzeug.url_encode(
                                {'auth_login': user_sudo.email}),
                        ).send_mail(user_sudo.id, force_send=True)
                return self.web_login(*args, **kw)
            except UserError as e:
                qcontext['error'] = e.name or e.value
            except (SignupError, AssertionError) as e:
                if request.env["res.users"].sudo().search([("login", "=", qcontext.get("login"))]):
                    qcontext["error"] = _(
                        "Another user is already registered using this email address.")
                elif request.env["res.users"].sudo().search([("mobile", "=", qcontext.get("mobile"))]):
                    qcontext["error"] = _(
                        "Another user is already registered using this Mobile Number.")
                elif request.env["res.partner"].sudo().search([("mobile", "=", qcontext.get("mobile"))]):
                    qcontext["error"] = _(
                        "Another user is already registered using this Mobile Number.")
                else:
                    _logger.error("%s", e)
                    qcontext['error'] = _("Could not create a new account.")

        response = request.render('auth_signup.signup', qcontext)
        response.headers['X-Frame-Options'] = 'DENY'
        return response

    def do_signup(self, qcontext):
        """ Shared helper that creates a res.partner out of a token """
        # qcontext[mobile]

        values = {key: qcontext.get(key) for key in (
            'login', 'name', 'password', 'mobile')}
        _logger.info("////////////////////////////////////////%s",
                     values['mobile'])
        if not values:
            raise UserError(_("The form was not properly filled in."))
        if values.get('password') != qcontext.get('confirm_password'):
            raise UserError(_("Passwords do not match; please retype them."))
        supported_lang_codes = [code for code,
                                _ in request.env['res.lang'].get_installed()]
        lang = request.context.get('lang', '').split('_')[0]
        if lang in supported_lang_codes:
            values['lang'] = lang
        self._signup_with_values(qcontext.get('token'), values)
        request.env.cr.commit()
