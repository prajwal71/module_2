from odoo import http
from odoo.http import request
from odoo.tools.translate import _
import logging
_logger = logging.getLogger(__name__)


class Preferreddate(http.Controller):
    
    
    @http.route("/payment/checkout/date", type='json', auth="public", methods=['POST'], website=True)
    def setreferalErnings(self, **kw):
        sale_order = request.env.user.partner_id.last_website_so_id
        
        sale_order.pref_date = kw['pref_date']
        return True