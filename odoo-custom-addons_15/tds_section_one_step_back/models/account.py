from contextlib import nullcontext
from odoo import models, api,fields,_
import logging
_logger = logging.getLogger(__name__)
import json
from odoo.exceptions import AccessError, UserError

class AccountAccountExt(models.Model):
    _inherit = 'account.account'

    is_tds_account = fields.Boolean('Is TDS Account',default=False)



class AccountExt(models.Model):
    _inherit = 'account.move'

    revenue_heading = fields.Many2one('tds.section',string='Revenue Headings')
    tds_rate = fields.Many2one('tds.rate',string="TDS Rate")
    tds_amount = fields.Float("TDS Amount",compute="_calculate_tds",store=True)
    
    tds_account = fields.Many2one('account.account',string="TDS Account",domain="[('is_tds_account','=',True)]")
    total_amount_after_tds = fields.Float("Total Amount After TDS",compute="_calculate_tds",store=True)
    is_tds_posted = fields.Boolean('Posted TDS',default=False)
    is_tds_bill = fields.Boolean('Is TDS',default=False)
    tds_label = fields.Char("TDS label")

    
    @api.onchange('tds_label')
    def change_label(self):
        if self.move_type == 'in_invoice' or self.move_type == 'out_invoice':
            if self.is_tds_bill:
                if self.is_tds_posted:
                    for line in self.line_ids:
                        if line.credit:
                            # _logger.info("======line   value====%s",line._origin)
                            a = line._origin.id
                            _logger.info("======line   value====%s",a)
                            # record = self.env['account.move.line'].search([('id','=',a.id)])
                            record = self.env['account.move.line'].browse(a)
                            _logger.info("=======browse=====     =====%s",record)
                            if record:
                                record.name = self.tds_label


    @api.onchange('tds_rate')
    def check_correct_rate(self):
        _logger.info("====%s=========checking==========",self.revenue_heading.tds_rates)
        for rec in self:
            if rec.tds_rate:
                if rec.revenue_heading:
                    b=[]
                    for a in rec.revenue_heading.tds_rates:
                        b.append(a.name)
                    if rec.tds_rate.name in b:
                        _logger.info("=======================checked ===pass====data===match")
                    else :
                        rec.tds_rate = nullcontext

                        raise UserError(_('The TDS Rate you have selected is not in this Revenue Headings.'
                        'The rate for this section are %s')%b)

              
    @api.depends('tds_rate','amount_total','amount_untaxed')
    def _calculate_tds(self):
        _logger.info("================calculating")
        for self_obj in self:
            if self_obj.move_type == 'in_invoice' or self_obj.move_type == 'out_invoice':
                if self_obj.is_tds_bill:
                    if self_obj.tds_rate:
                        if self_obj.amount_untaxed:
                            _logger.info("================%s===tds",self_obj.tds_rate.name)
                            _logger.info("================%s===untax",self_obj.amount_untaxed)
                            _logger.info("================%s==tax",self_obj.amount_total)
                            self_obj.tds_amount = (float(self_obj.tds_rate.name)/100) * self_obj.amount_untaxed
                            self_obj.total_amount_after_tds = self_obj.amount_total - self_obj.tds_amount
                        else:
                            self_obj.tds_amount = 0
                            self_obj.total_amount_after_tds = 0
                    else:
                        self_obj.tds_amount = 0
                        self_obj.total_amount_after_tds = 0


                        
    # ====bill==============tds===
    def get_tds_move_line_bill(self, move_id, account):
        return {
            'move_id':self,
            'name' :self.tds_label,
            'exclude_from_invoice_tab':True,
            'account_id': account,
            'credit': round(self.tds_amount,2),
           
        }

    # ===========invoice======tds===entry
    def get_tds_move_line(self, move_id, account):
        return {
            'move_id':self,
            'name' :self.tds_label,
            'exclude_from_invoice_tab':True,
            'account_id': account,
            'debit': round(self.tds_amount,2),
           
        }
    # ============bill===========payable===entry
    def get_payable_move_line(self, move_id, account):
        for rec in self.line_ids:
            if rec.credit:
                return {
                    'move_id':self,
                    'id':rec.id,
                    'partner_id':self.partner_id.commercial_partner_id.id,
                    'exclude_from_invoice_tab':True,
                    'name':self.tds_label,
                    'account_id': account,
                    'credit': round(rec.credit - self.tds_amount,2)
                
                
                }
    # ==========invoice=========receivable=====entry
    def get_receievable_move_line_invoice(self, move_id, account):
        for rec in self.line_ids:
            if rec.debit:
                return {
                    'move_id':self,
                    'id':rec.id,
                    'partner_id':self.partner_id.commercial_partner_id.id,
                    'exclude_from_invoice_tab':True,
                    'name':self.tds_label,
                    'account_id': account,
                    'debit': round(rec.debit - self.tds_amount,2)
                
                
               }

    # =========bill=========as===whole===entry==combine
    def get_move_vals(self, debit_line, credit_line):
        return {
            'id':self.id,
            'line_ids': [(3,debit_line['id'] , debit_line),
                        (0, 0, credit_line),
                        (0, 0, debit_line),
                        ]
        }
    
    # ===============invoice===as==a===whole===entry==combine
    def get_move_vals_invoice(self, debit_line, credit_line):
        return {
            'id':self.id,
            'line_ids': [(3,debit_line['id'] , debit_line),
                        (0, 0, credit_line),
                        (0, 0, debit_line),
                        ]
        }

    
    
    def invoice_line(self, move_id):
            return {
                
                'move_id':self,
                'id':self.id,
                'name':'prajwal',
                # 'exclude_from_invoice_tab':True,
                # 'partner_id': self.partner_id.id,
                # 'name':'TDS Section',
                'account_id': self.tds_account.id,
                # 'credit': round(rec.credit,2),
            
            
            }
    # ======delete===tds===entry===bill==
    def delete_tds_move_line(self, move_id, account):
        for rec in self.line_ids:
            if rec.account_id == self.tds_account:
                if rec.credit == round(self.tds_amount,2) or rec.debit == round(self.tds_amount,2):
                    _logger.info("===============account===name===%s",rec.name)
                    if rec.credit:
                        _logger.info("-=============delete==========tds=====%s",rec.credit)
                        return {
                            
                            'move_id':self,
                            'id':rec.id,
                            'exclude_from_invoice_tab':True,
                            # 'partner_id': self.partner_id.id,
                            # 'name':'TDS Section',
                            'account_id': account,
                            'credit': round(rec.credit,2),
                        
                        
                        }
                    if rec.debit:
                        _logger.info("====================delete====tds======debit==%s",rec.credit)
                        return {
                            'move_id':self,
                            'id':rec.id,
                            'exclude_from_invoice_tab':True,
                            # 'partner_id': self.partner_id.id,
                            # 'name':'TDS Section',
                            'account_id': account,
                            'credit': round(rec.credit,2),
                        
                        
                        }
    # ======delete===tds===entry===invoice==
    def delete_tds_move_line_invoice(self, move_id, account):
        for rec in self.line_ids:
            if rec.account_id == self.tds_account:
                if rec.debit == round(self.tds_amount,2) or rec.credit == round(self.tds_amount,2):
                    _logger.info("===============account===name===%s",rec.name)
                    if rec.debit:
                        _logger.info("-=============delete==========tds=====%s",rec.debit)
                        return {
                            
                            'move_id':self,
                            'id':rec.id,
                            'exclude_from_invoice_tab':True,
                            # 'partner_id': self.partner_id.id,
                            # 'name':'TDS Section',
                            'account_id': account,
                            'debit': round(rec.debit,2),
                        
                        
                        }
                    if rec.credit:
                        _logger.info("====================delete====tds======debit==%s",rec.credit)
                        return {
                            'move_id':self,
                            'id':rec.id,
                            'exclude_from_invoice_tab':True,
                            # 'partner_id': self.partner_id.id,
                            # 'name':'TDS Section',
                            'account_id': account,
                            'debit': round(rec.debit,2),
                          }
                        
                        
    # ======payable==entry==delete==bill==               
    def delete_payable_line(self, move_id, account):
        for rec in self.line_ids:
            
            if rec.account_id == self.tds_account:
                _logger.info("===          111                 ===========")
                _logger.info("===          222   %s                 ===========",rec.credit)
                if rec.credit == round(self.tds_amount,2) or rec.debit == round(self.tds_amount,2):
                    _logger.info("===          222   %s                 ===========",rec.credit)
                    if rec.credit:
                    
                        vals = rec.credit
                    if rec.debit:
                        vals = rec.credit
        for rec in self.line_ids:
            _logger.info("==%s==i==am=delt==here",self.line_ids)
            
            if rec.account_id != self.tds_account:
                _logger.info("==============2=====step")
                if rec.name == self.tds_label:
                    # _logger.info("==================i===enter========payable=====delete")
                    if rec.credit:
                        _logger.info("====================delete====payable======credit==%s",rec.credit)
                        return {
                            'move_id':self,
                            'id':rec.id,
                            'exclude_from_invoice_tab':True,
                            'partner_id':self.partner_id.commercial_partner_id.id,
                            # 'partner_id':self.partner_id.commercial_partner_id.id,
                            # 'name':'TDS Section',
                            'account_id': account,
                            'credit': round(rec.credit + vals,2),
                        
                        
                        }
                    if rec.debit:
                        _logger.info("====================delete====payable======debit==%s",rec.debit)
                        return {
                            'move_id':self,
                            'id':rec.id,
                            'exclude_from_invoice_tab':True,
                            # 'partner_id': self.partner_id.id,
                            'partner_id':self.partner_id.commercial_partner_id.id,
                            # 'name':'TDS Section',
                            'account_id': account,
                            'credit': round(rec.debit - vals,2),
                        
                        
                        }


    # ======payable==entry==delete==invoice==               
    def delete_payable_line_invoice(self, move_id, account):
        for rec in self.line_ids:
            
            if rec.account_id == self.tds_account:
                _logger.info("===          111                 ===========")
                _logger.info("===          222   %s                 ===========",rec.debit)
                if rec.debit == round(self.tds_amount,2) or rec.credit == round(self.tds_amount,2):
                    _logger.info("===          222   %s                 ===========",rec.debit)
                    if rec.debit:
                    
                        vals = rec.debit
                    if rec.credit:
                        vals = rec.credit
        for rec in self.line_ids:
            _logger.info("==%s==i==am=delt==here",self.line_ids)
            
            if rec.account_id != self.tds_account:
                _logger.info("=======================vals===%s",vals)
                if rec.account_id == self.partner_id.property_account_receivable_id:
                    _logger.info("=========invoice==oayable=========delette==%s==",rec.debit)
                    # _logger.info("==================i===enter========payable=====delete")
                    if rec.debit:
                        _logger.info("====================delete====payable======credit==%s",rec.debit)
                        return {
                            'move_id':self,
                            'id':rec.id,
                            'exclude_from_invoice_tab':True,
                            'partner_id':self.partner_id.commercial_partner_id.id,
                            # 'partner_id':self.partner_id.commercial_partner_id.id,
                            # 'name':'TDS Section',
                            'account_id': account,
                            'debit': round(rec.debit + vals,2),
                        
                        
                        }
                    if rec.credit:
                        _logger.info("====================delete====payable======debit==%s",rec.debit)
                        return {
                            'move_id':self,
                            'id':rec.id,
                            'exclude_from_invoice_tab':True,
                            # 'partner_id': self.partner_id.id,
                            'partner_id':self.partner_id.commercial_partner_id.id,
                            # 'name':'TDS Section',
                            'account_id': account,
                            'debit': round(rec.credit - vals,2),
                        }
                        
    # ===========delete==all==entry===bill===                  
    def delete_move_vals(self, debit_line, credit_line):
        _logger.info("+=%s=====id===%s===========+++%s",debit_line,credit_line)
        return {
            'id':self.id,
            'line_ids': [(3,debit_line['id'] , debit_line),
                         (3, credit_line['id'], credit_line),
                         (0, 0, debit_line),
                         ]
        }
    
    # ===========delete==all==entry===invoice===                  
    def delete_move_vals_invoice(self, debit_line, credit_line):
        _logger.info("+=%s=====id===%s===========+++%s",debit_line,credit_line)
        return {
            'id':self.id,
            'line_ids': [(3,debit_line['id'] , debit_line),
                         (3, credit_line['id'], credit_line),
                         (0, 0, debit_line),
                         ]
        }

    def delete_invoice(self,invoice):
        return {
            'id':self.id,
            'line_ids': [(0, 0, invoice),
                            # (0, 0, invoice),
                            # (3,invoice['id'] , invoice),
                            ]
        }

    # @api.onchange('is_tds_bill')
    # def isTdsBill(self):
    #     if self.move_type == 'in_invoice':
    #         if not self.is_tds_bill:
    #             if  self.is_tds_posted:
    #                 tds = self.delete_tds_move_line(self.id, self.tds_account.id)
    #                 payabale =self.delete_payable_line(self.id, self.partner_id.property_account_payable_id.id)
    #                 move_vals_line = self.delete_move_vals(payabale, tds)
    #                 move_id = self.write(move_vals_line)
    #                 self._origin.is_tds_posted = False


    # =====delete==entry==of==tds===for===bill==
    def delete_tds(self):
        _logger.info("=========================second===function")
        if self.move_type == 'in_invoice':
            if self.is_tds_bill:
                if self.is_tds_posted:
                    tds = self.delete_tds_move_line(self.id, self.tds_account.id)
                    payabale =self.delete_payable_line(self.id, self.partner_id.property_account_payable_id.id)
                    move_vals_line = self.delete_move_vals(payabale, tds)
                    move_id = self.write(move_vals_line)
                    self.is_tds_posted = False 

    # =====delete==entry==of==tds===for===invoice==
    def delete_tds_invoice(self):
        _logger.info("=========================second===function")
        if self.move_type == 'out_invoice':
            if self.is_tds_bill:
                if self.is_tds_posted:
                    tds = self.delete_tds_move_line_invoice(self.id, self.tds_account.id)
                    receivable =self.delete_payable_line_invoice(self.id, self.partner_id.property_account_receivable_id.id)
                    move_vals_line = self.delete_move_vals_invoice(receivable, tds)
                    move_id = self.write(move_vals_line)
                    self.is_tds_posted = False 


    def delete_inv(self):
        _logger.info("=========================second===function")
        if self.move_type == 'in_invoice':
            if self.is_tds_bill:
                # tds = self.delete_tds_move_line(self.id, self.tds_account.id)
                # payabale =self.delete_payable_line(self.id, self.partner_id.property_account_payable_id.id)
                invoice = self.invoice_line(self.id)
                move_vals_line = self.delete_invoice(invoice)
                move_id = self.write(move_vals_line)
            


    def calculate_tds(self):
        if self.move_type == 'in_invoice':
            if self.is_tds_bill:
                if self.revenue_heading:
                    if self.tds_rate:
                        if self.tds_label:
                            if self.tds_account:
                                if self.partner_id:
                                    if not self.is_tds_posted:
                                        if self.tds_amount:   
                                            move_line_vals_debit = {}
                                            move_line_vals_credit = {}
                                            move_line_vals_debit = self.get_payable_move_line(self.id, self.partner_id.property_account_payable_id.id)
                                            move_line_vals_credit = self.get_tds_move_line_bill(self.id, self.tds_account.id)
                                            _logger.info("====%s===",move_line_vals_credit)
                                            # create move and post it
                                            move_vals = self.get_move_vals(move_line_vals_debit, move_line_vals_credit)
                                            _logger.info("====================%s===move==vals",move_vals)
                                            move_id = self.write(move_vals)
                                            self.is_tds_posted = True  
                                            _logger.info("====================test=====%s",self.is_tds_posted) 
                                        

                                            _logger.info("========done==================")
                                    # else :
                                    #     _logger.info("=========else===")
                        

                            
                                    #     if self.tds_amount >0:
                                    #         _logger.info("=======amount===2================")
                                    #         test = False
                                    #         for rec in self.line_ids:
                                    #             if rec.name == self.revenue_heading.name:
                                    #                 if rec.credit:
                                    #                     if rec.credit>0 and rec.account_id == self.tds_account:
                                    #                         if rec.credit != self.tds_amount:
                                    #                             test = True
                                    #                             vals = rec.credit

                                    #                             _logger.info("========11=========i===found")
                                    #                             rec.credit = round(self.tds_amount,2)
                                    #         for rec1 in self.line_ids:
                                    #             if rec1.name == self.revenue_heading.name:
                                    #                 if rec1.credit:
                                    #                     if rec1.credit>0 and rec1.account_id != self.tds_account:
                                    #                         if test and vals:
                                    #                             _logger.info("======22====%s===========i===found",rec.credit)
                                    #                             rec.credit = round(rec.credit - self.tds_amount +vals,2)
                                    

                                    #     else:
                                    #         tds = self.delete_tds_move_line(self.id, self.tds_account.id)
                                    #         payabale =self.delete_payable_line(self.id, self.partner_id.property_account_payable_id.id)
                                    #         move_vals_line = self.delete_move_vals(payabale, tds)
                                    #         move_id = self.write(move_vals_line)
                                    #         self.is_tds_posted = False
                                
                                else:
                                            raise UserError(_("Please Select Partner"))
                            else :
                                raise UserError(_("Please Select the TDS Account."
                                    "If you dont want to calculate TDS Please untick the 'Is TDS bill' field"))
                        else :
                            raise UserError(_("Please Enter the TDS label."
                                ))

                    else:
                        raise UserError(_("Please Select the TDS Rate."
                                "If you dont want to calculate TDS Please untick the 'Is TDS bill' field"))
                else:
                        raise UserError(_("Please Select the TDS Revenue Heading."
                                "If you dont want to calculate TDS Please untick the 'Is TDS bill' field"))

    
    def calculate_tds_invoice(self):
        if self.move_type == 'out_invoice':
            if self.is_tds_bill:
                if self.revenue_heading:
                    if self.tds_rate:
                        if self.tds_label:
                            if self.tds_account:
                                if self.partner_id:
                                    if not self.is_tds_posted:
                                        if self.tds_amount:   
                                            move_line_vals_debit = {}
                                            move_line_vals_credit = {}
                                            move_line_vals_debit = self.get_receievable_move_line_invoice(self.id, self.partner_id.property_account_receivable_id.id)
                                            move_line_vals_credit = self.get_tds_move_line(self.id, self.tds_account.id)
                                            _logger.info("====%s===",move_line_vals_credit)
                                            # create move and post it
                                            move_vals = self.get_move_vals_invoice(move_line_vals_debit, move_line_vals_credit)
                                            _logger.info("====================%s===move==vals",move_vals)
                                            move_id = self.write(move_vals)
                                            self.is_tds_posted = True  
                                            _logger.info("====================test=====%s",self.is_tds_posted) 
                                        

                                            _logger.info("========done==================")
                                    # else :
                                    #     _logger.info("=========else===")
                        

                            
                                    #     if self.tds_amount >0:
                                    #         _logger.info("=======amount===2================")
                                    #         test = False
                                    #         for rec in self.line_ids:
                                    #             if rec.name == self.revenue_heading.name:
                                    #                 if rec.credit:
                                    #                     if rec.credit>0 and rec.account_id == self.tds_account:
                                    #                         if rec.credit != self.tds_amount:
                                    #                             test = True
                                    #                             vals = rec.credit

                                    #                             _logger.info("========11=========i===found")
                                    #                             rec.credit = round(self.tds_amount,2)
                                    #         for rec1 in self.line_ids:
                                    #             if rec1.name == self.revenue_heading.name:
                                    #                 if rec1.credit:
                                    #                     if rec1.credit>0 and rec1.account_id != self.tds_account:
                                    #                         if test and vals:
                                    #                             _logger.info("======22====%s===========i===found",rec.credit)
                                    #                             rec.credit = round(rec.credit - self.tds_amount +vals,2)
                                    

                                    #     else:
                                    #         tds = self.delete_tds_move_line(self.id, self.tds_account.id)
                                    #         payabale =self.delete_payable_line(self.id, self.partner_id.property_account_payable_id.id)
                                    #         move_vals_line = self.delete_move_vals(payabale, tds)
                                    #         move_id = self.write(move_vals_line)
                                    #         self.is_tds_posted = False
                                
                                else:
                                            raise UserError(_("Please Select Partner"))
                            else :
                                raise UserError(_("Please Select the TDS Account."
                                    "If you dont want to calculate TDS Please untick the 'Is TDS bill' field"))
                        else :
                            raise UserError(_("Please Enter the TDS label."
                                ))

                    else:
                        raise UserError(_("Please Select the TDS Rate."
                                "If you dont want to calculate TDS Please untick the 'Is TDS bill' field"))
                else:
                        raise UserError(_("Please Select the TDS Revenue Heading."
                                "If you dont want to calculate TDS Please untick the 'Is TDS bill' field"))

    @api.model
    def action_post(self):
        for rec in self:
            if rec.move_type == 'in_invoice':

                rec.calculate_tds()
            if rec.move_type == 'out_invoice':
                rec.calculate_tds_invoice()
        res = super(AccountExt,self).action_post()
        
        return res
    
    
    def button_draft(self):
        _logger.info("====================we=====are===calling")
        res = super(AccountExt,self).button_draft()
        for rec in self:
            if rec.move_type == 'in_invoice':
                rec.delete_tds()
            if rec.move_type == 'out_invoice':
                rec.delete_tds_invoice()
        return res





        
    # @api.onchange('invoice_line_ids')
    # def generate_tds_journal(self):
    #     _logger.info('========================onchange===')
    #     for obj in self:
    #         if obj.move_type == 'in_invoice':
    #                 # _logger.info("===========vendor====%s===========",self)
    #                 if obj.state != 'posted':
    #                     _logger.info("=======2222===============1111=======")
    #                     if not obj.is_tds_posted:
    #                         pass
    #                         # _logger.info("=======3333======%s=========1111=======",self.is_tds_posted)
    #                         # if obj.tds_amount:
    #                         #     _logger.info("======================1111=======")
    #                         #     if obj.tds_amount>0:
    #                         #         _logger.info("=======amount===================")
    #                         #         if obj.tds_account:
    #                         #             if obj._origin.id:
    #                         #                 _logger.info("====account===========journal=====%s",obj.line_ids)
    #                         #                 move = self.env['account.move']
    #                         #                 move_line_vals_debit = {}
    #                         #                 move_line_vals_credit = {}
    #                         #                 move_line_vals_debit = obj.get_debit_move_line(obj._origin.id, obj.partner_id.property_account_payable_id.id)
    #                         #                 move_line_vals_credit = obj.get_credit_move_line(obj._origin.id, obj.tds_account.id)
    #                         #                 _logger.info("====%s===",move_line_vals_credit)
    #                         #                 # create move and post it
    #                         #                 move_vals = obj.get_move_vals(move_line_vals_debit, move_line_vals_credit)
    #                         #                 _logger.info("====================%s===move==vals",move_vals)
    #                         #                 move_id = obj.write(move_vals)
    #                         #                 obj._origin.is_tds_posted = True  
    #                         #                 _logger.info("====================test=====%s",obj.is_tds_posted) 
    #                         #                 # for line in rec.invoice_line_ids:
    #                         #                 #     if line.account_id.id == rec.partner_id.property_account_payable_id.id  and  line.price_unit == rec.tds_amount:
    #                         #                 #         line.unlink()
    #                         #                 #     if line.account_id.id == rec.tds_account.id  and  line.price_unit == rec.tds_amount:
    #                         #                 #         line.unlink()

    #                         #                 _logger.info("========done==================")
    #                     else :
    #                         _logger.info("=========else===")
    #                         # if self.tds_amount:
    #                         #     _logger.info("======else ===222")

                
    #                         if obj.tds_amount > 0:
    #                             _logger.info("=======amount===2================")
    #                             test = False
    #                             for rec in obj.line_ids:
    #                                 if rec.name == 'TDS Section':
    #                                     if rec.credit:
    #                                         if rec.credit>0 and rec.account_id == obj.tds_account:
    #                                             if rec.credit != obj.tds_amount:
    #                                                 test = True
    #                                                 vals = rec.credit

    #                                                 _logger.info("========11=========i===found")
    #                                                 rec.credit = obj.tds_amount
    #                             for rec in obj.line_ids:
    #                                 if rec.name == 'TDS Section':
    #                                     _logger.info("==else00======11=========i===found")
    #                                     if rec.credit:
    #                                         _logger.info("===else1112=====11=========i===found")
    #                                         if rec.credit>0 and rec.account_id != obj.tds_account:
    #                                             _logger.info("====wewe====11=========i===found")
    #                                             if test and vals:
    #                                                 _logger.info("======22====%s===========i===found",rec.credit)
    #                                                 rec.credit = rec.credit - obj.tds_amount +vals
    #                         else:
    #                                 tds = self.delete_tds_move_line(self.id, self.tds_account.id)
    #                                 payabale =self.delete_payable_line(self.id, self.partner_id.property_account_payable_id.id)
    #                                 move_vals_line = self.delete_move_vals(payabale, tds)
    #                                 move_id = self.write(move_vals_line)
    #                                 self.is_tds_posted = False
                                                
                                            


    #     for ob in self:
    #         if ob.move_type == 'in_invoice':
    #                 # _logger.info("===========vendor====%s===========",self)
    #             if ob.state != 'posted':
    #                 if ob.is_tds_posted:
    #                     if not ob.tds_account:
    #                         tds = ob.delete_tds_move_line(self._origin, self.tds_account.id)
    #                         payabale =ob.delete_payable_line(self._origin, self.partner_id.property_account_payable_id.id)
    #                         move_vals_line = ob.delete_move_vals(payabale, tds)
    #                         move_id = ob.write(move_vals_line)
    #                         ob._origin.is_tds_posted = False


    

    # def get_credit_move_line(self, move_id, account):
    #     return {
    #         'move_id':self,
    #         # 'partner_id': self.partner_id.id,
    #         'name' :'TDS Section',
    #         'exclude_from_invoice_tab':True,
    #         'account_id': account,
    #         'credit': self.tds_amount,
           
    #     }

    # def get_debit_move_line(self, move_id, account):
    #     for rec in self._origin.line_ids:
    #         _logger.info("==%s==i==am===here",self._origin.line_ids)
    #         if rec.credit:
    #             _logger.info("====i==am===here")
    #             return {
    #                 'move_id':self,
    #                 'id':rec.id,
    #                 'exclude_from_invoice_tab':True,
    #                 # 'partner_id': self.partner_id.id,
    #                 'name':'TDS Section',
    #                 'account_id': account,
    #                 'credit': rec.credit - self.tds_amount,
                
                
    #             }
    # def delete_tds_move_line(self, move_id, account):
    #     for rec in self._origin.line_ids:
    #         _logger.info("==%s=               tds=i==am=delt==here",rec)
    #         if rec.credit:
    #             _logger.info("======%s======1==========step",rec)
    #             if rec.credit == self._origin.tds_amount:
    #                 _logger.info("==========tds====line====%s",rec.credit)
    #                 return {
    #                     'move_id':self,
    #                     'id':rec.id,
    #                     'exclude_from_invoice_tab':True,
    #                     # 'partner_id': self.partner_id.id,
    #                     'name':'TDS Section',
    #                     'account_id': account,
    #                     'credit': rec.credit,
                    
                    
    #                 }
    # def delete_payable_line(self, move_id, account):
    #     for rec in self._origin.line_ids:
    #         _logger.info("==%s==i==am=delt==here",self.line_ids)
    #         if rec.credit:
    #             _logger.info("==============2=====step")
    #             if rec.credit != self._origin.tds_amount:
    #                 _logger.info("==========payabale===line====%s",rec.credit)
                    
    #                 return {
    #                     'move_id':self,
    #                     'id':rec.id,
    #                     'exclude_from_invoice_tab':True,
    #                     # 'partner_id': self.partner_id.id,
    #                     'name':'TDS Section',
    #                     'account_id': account,
    #                     'credit': rec.credit + self._origin.tds_amount,
                    
                    
    #                 }

    # def get_move_vals(self, debit_line, credit_line):
    #     _logger.info("+======id==============+++%s",self.id)
    #     return {
    #         'id':self.id,
    #         'line_ids': [(3,debit_line['id'] , debit_line),
    #                      (0, 0, credit_line),
    #                      (0, 0, debit_line),
    #                      ]
    #     }
    # def delete_move_vals(self, debit_line, credit_line):
    #     _logger.info("+=%s=====id===%s===========+++%s",debit_line,credit_line)
    #     return {
    #         'id':self.id,
    #         'line_ids': [(3,debit_line['id'] , debit_line),
    #                      (3, credit_line['id'], credit_line),
    #                      (0, 0, debit_line),
    #                      ]
    #     }
    
  

   

    # @api.onchange('tds_account')
    # def change_journal_entry(self):
    #     _logger.info('========================onchange===')
    #     if self.move_type == 'in_invoice':
    #             _logger.info("===========vendor===============")
    #             if self.state != 'posted':
    #                 _logger.info("=======2222===============1111=======")
    #                 if not self.is_tds_posted:
    #                     _logger.info("=======3333======%s=========1111=======",self.is_tds_posted)
    #                     if self.tds_amount:
    #                         _logger.info("======================1111=======")
    #                         if self.tds_amount>0:
    #                             _logger.info("=======amount=====neww==============")
    #                             if self.tds_account:
    #                                 if self._origin.id:
    #                                     _logger.info("====account===========journal=====%s",self.line_ids)
    #                                     move = self.env['account.move']
    #                                     move_line_vals_debit = {}
    #                                     move_line_vals_credit = {}
    #                                     move_line_vals_debit = self.get_debit_move_line(self._origin, self.partner_id.property_account_payable_id.id)
    #                                     move_line_vals_credit = self.get_credit_move_line(self._origin, self.tds_account.id)
    #                                     _logger.info("====%s===",move_line_vals_credit)
    #                                     # create move and post it
    #                                     move_vals = self.get_move_vals(move_line_vals_debit, move_line_vals_credit)
    #                                     _logger.info("====================%s===move==vals",move_vals)
    #                                     move_id = self.write(move_vals)
                                    
    #                                     self._origin.is_tds_posted = True  
    #                                     _logger.info("====================test=====%s",self.is_tds_posted) 
    #                                     # for line in rec.invoice_line_ids:
    #                                     #     if line.account_id.id == rec.partner_id.property_account_payable_id.id  and  line.price_unit == rec.tds_amount:
    #                                     #         line.unlink()
    #                                     #     if line.account_id.id == rec.tds_account.id  and  line.price_unit == rec.tds_amount:
    #                                     #         line.unlink()

    #                                     _logger.info("========done==================")
    #                 else :
    #                     _logger.info("=========else===")
    #                     if self.tds_account:
    #                         for rec in self.line_ids:
    #                             if rec.name == 'TDS Section':
    #                                 if rec.credit:

    #                                     rec.account_id = self.tds_account
                                    

    #                     # if self.tds_amount:
    #                     #     _logger.info("======else ===222")

    #     for ob in self:
    #         if ob.move_type == 'in_invoice':
    #                 # _logger.info("===========vendor====%s===========",self)
    #             if ob.state != 'posted':
    #                 if ob.is_tds_posted:
    #                     if not ob.tds_account:
    #                         tds = ob.delete_tds_move_line(self._origin, self.tds_account.id)
    #                         payabale =ob.delete_payable_line(self._origin, self.partner_id.property_account_payable_id.id)
    #                         move_vals_line = ob.delete_move_vals(payabale, tds)
    #                         move_id = ob.write(move_vals_line)
    #                         ob._origin.is_tds_posted = False

            
                       




                                        
                            

                                            
                            







   
    
    

    