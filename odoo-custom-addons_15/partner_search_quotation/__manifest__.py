# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Partner Mobile Number Search',
    'version' : '1.1',
    'summary': 'search partner from mobile number in quoation and invoice',
    'description': """

====================

    """,
    'author': 'Nexus',
    'depends' : ['base','sale'],
    'data': [
             'views/view.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
