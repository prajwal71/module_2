# ©  2008-2021 Deltatech
#              Dorin Hongu <dhongu(@)gmail(.)com
# See README.rst file on addons root folder for license details

import base64
from odoo import fields as odoo_fields, http, tools, _, SUPERUSER_ID
from odoo.http import request
from odoo.http import content_disposition, Controller, request, route
import logging
_logger = logging.getLogger(__name__)

from odoo.addons.portal.controllers.portal import CustomerPortal


class CustomerPortalCity(CustomerPortal):
    MANDATORY_BILLING_FIELDS = ["name", "email", "mobile","country_id","state_id","city_id","street"]
    OPTIONAL_BILLING_FIELDS = ["zipcode","image_1920"]

    def _prepare_portal_layout_values(self):
        values = super(CustomerPortalCity, self)._prepare_portal_layout_values()
        # cities = request.env['res.city'].sudo().search([])
        # values['cities'] = cities
       
        values["statess"] = request.env['res.country.state'].sudo().search([])
        values["city"] = values['statess'].city_ids
        # values['zip'] = request.env['res.city'].sudo().search([]).zipcode
        # values["zipcode"] = request.env['res.city'].sudo().search([])
        # values["address_state"] = request.env['res.nepal.state'].sudo().search([])
        _logger.info("==========%s============check==search",values["city"])
        return values

    # @http.route(
    #     ['/shop/state_infos/<model("res.country.state"):state>'],
    #     type="json",
    #     auth="public",
    #     methods=["POST"],
    #     website=True,
    # )
    # def country_infos(self, state, mode, **kw):
    #     return dict(
    #         cities=[(st.id, st.name, st.zipcode or "") for st in state.get_website_sale_cities(mode=mode)],
    #     )

    @route(['/my/account'], type='http', auth='user', website=True)
    def account(self, redirect=None, **post):
        _logger.info("============i===was====new")
        if 'image_1920' in post:
            image_1920 = post.get('image_1920')
            if image_1920:
                image_1920 = image_1920.read()
                image_1920 = base64.b64encode(image_1920)
                request.env.user.partner_id.sudo().write({
                    'image_1920': image_1920
                })
            post.pop('image_1920')
        if 'clear_avatar' in post:
            request.env.user.partner_id.sudo().write({
                'image_1920': False
            })
            post.pop('clear_avatar')
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        values.update({
            'error': {},
            'error_message': [],
        })

        if post and request.httprequest.method == 'POST':
            error, error_message = self.details_form_validate(post)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)
            if not error:
                
              
                values = {key: post[key] for key in self.MANDATORY_BILLING_FIELDS}
                values.update({key: post[key] for key in self.OPTIONAL_BILLING_FIELDS if key in post})
                for field in set(['country_id','state_id','city_id','zipcode']) & set(values.keys()):
                    try:
                        values[field] = int(values[field])
                    except:
                        values[field] = False
                values.update({'zip': values.pop('zipcode', '')})
                # values.update({'mobile': values.pop('mobile', '')})
                # values.update({'street_name': values.pop('street_name', '')})
                # values.update({'ward_no': values.pop('ward_no', '')})
                partner.sudo().write(values)
                if redirect:
                    return request.redirect(redirect)
                return request.redirect('/my/home')

        countries = request.env['res.country'].sudo().search([])
        statess = request.env['res.country.state'].sudo().search([])
        # statess = request.env['res.nepal.state'].sudo().search([])
        city = statess.city_ids
        # localss = request.env['res.address.local'].sudo().search([])

        values.update({
            'partner': partner,
            'countries': countries,
            'state_id': statess,
            'has_check_vat': hasattr(request.env['res.partner'], 'check_vat'),
            'redirect': redirect,
            'page_name': 'my_details',
            'city_id':city,

            # 'address_local':localss
        })

        response = request.render("portal.portal_my_details", values)
        response.headers['X-Frame-Options'] = 'DENY'
        return response

    @http.route("/country", type='json', auth="public", methods=['POST'], website=True)
    def countrychange(self, **kw):
        a=kw['cys']
        _logger.info("=======country  %s=============onchange",a)
        s = request.env['res.country.state'].sudo().search([])
        # city = s.city_ids
        statess = request.env['res.country.state'].sudo().search([('country_id.id','=',a)])
        _logger.info("=======state  %s=============onchange",statess)

        # city = statess.city_ids
        return dict(
            statess=[(st.id, st.name) for st in statess]
        )

    @http.route("/states", type='json', auth="public", methods=['POST'], website=True)
    def stateChange(self, **kw):
        _logger.info("+===========================new prajwal   ")
        a=kw['statess']
        _logger.info("===========%s===state",a)
        # _logger.info("====================onchange")
        s = request.env['res.country.state'].sudo().search([])
        city = s.city_ids
        statess = request.env['res.country.state'].sudo().search([('id','=',a)])
        city = statess.city_ids
        _logger.info("===========%s===city",city)
        return dict(
            city=[(ct.id, ct.name,ct.zipcode) for ct in city]
        )

    @http.route("/city", type='json', auth="public", methods=['POST'], website=True)
    def cityChange(self, **kw):
        _logger.info("+===========================new prajwal   ")
        a=kw['city1']
        _logger.info("===========%s===city",a)
        # _logger.info("====================onchange")
        # s = request.env['res.country.state'].sudo().search([])
        # city = s.city_ids
        city = request.env['res.city'].sudo().search([('id','=',a)],limit=1)
        zipcode = city.zipcode
        # _logger.info("===========%s===city",city)
        return zipcode
    

    @http.route("/check/email/checkout", type='json', auth="public", methods=['POST'], website=True)
    def checkemail(self, **kw):
        # sale_order = request.env.user.partner_id.last_website_so_id
        
        # sale_order.pref_date = kw['pref_date']
        email = kw['email']
        res_partner = request.env['res.partner'].sudo().search([('email','=',email)])
        # res_login = request.env['res.users'].sudo().search([('login','=',email)])
        res_login = request.env.user
        res_login_id = res_login.id
        _logger.info("==================%s===parnter",res_login)
        if res_login_id != 4:
            return False
        else :
            if res_partner:
                _logger.info("==================match===parnter")
                return True
            else:
                return False

    