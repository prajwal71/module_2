odoo.define("nexus_website_city.portal", function (require) {
  "use strict";

  var core = require("web.core");
  var ajax = require("web.ajax");

  var _t = core._t;
  $(document).ready(function () {
    if (document.getElementById("statess")) {
      // ========for by default=========Distict=====load======
      var cy = document.getElementById("cy").value;
      console.log(cy);
      var st = document.getElementById("statess").value;
      console.log(st);
      var statestore;
      var count = 0;
      ajax.jsonRpc("/country", "call", { cys: cy }).then(function (result) {
        // console.log(result);
        var selectState1 = $("select[name='state_id']");
        // console.log(selectState1);

        var selectes1 = selectState1.data("value");
        console.log(selectState1);

        $("select[name='state_id']").parent("div").hide();
        selectState1.html("");
        _.each(result.statess, function (z) {
          if (count == 0) {
            var opt = $("<option>")
              .text(z[1])
              .attr("value", z[0])

              .attr("selected", z[0] === selectes1);
            statestore = z[0];

            selectState1.append(opt);
            count = 2;
          } else {
            var opt = $("<option>")
              .text(z[1])
              .attr("value", z[0])

              .attr("selected", z[0] === selectes1);
            // statestore = z[0];

            selectState1.append(opt);
            // count=2;
          }
        });
        // console.log("hello bipin poudel");
        $("select[name='state_id']").parent("div").show();
        // console.log("hello bipin poudel part2");
        // var selDist = document.getElementById("citys");
        // selDist.value = Number(dt);
        var selst = document.getElementById("statess");

        // selst.value = Number(statestore);
        if (st) {
          selst.value = Number(st);
        } else {
          selst.value = Number(statestore);
        }
        var state = document.getElementById("statess").value;
        var ct = document.getElementById("citys").value;
        var zp = document.getElementById("zipcode").value;
        // console.log("BIPIN")
        var storecity;
        var countcity = 0;
        var storezip;
        // var countzip = 0;
        ajax
          .jsonRpc("/states", "call", { statess: state })
          .then(function (result) {
            var selectCity1 = $("select[name='city_id']");
            // console.log(selectCity1)

            var selectec1 = selectCity1.data("value");

            $("select[name='city_id']").parent("div").hide();
            selectCity1.html("");
            _.each(result.city, function (z) {
              if (countcity == 0) {
                var opt = $("<option>")
                  .text(z[1])
                  .attr("value", z[0])

                  .attr("selected", z[0] === selectec1);
                storecity = z[0];
                countcity = 2;
                storezip = z[2];

                selectCity1.append(opt);
              } else {
                var opt = $("<option>")
                  .text(z[1])
                  .attr("value", z[0])

                  .attr("selected", z[0] === selectec1);
                selectCity1.append(opt);
              }
            });
            $("select[name='city_id']").parent("div").show();
            var selCity = document.getElementById("citys");
            var selZip1 = document.getElementById("zipcode");
            if (ct) {
              selCity.value = Number(ct);
            } else {
              selCity.value = Number(storecity);
            }

            if (zp) {
              selZip1.value = zp;
            } else {
              selZip1.value = storezip;
            }
          });
      });
    }

    $("#statess").on("change", function () {
      console.log("onchange");
      var statess = document.getElementById("statess").value;
      var storecitychange;
      var countcitychange = 0;
      var storezipchange;
      ajax
        .jsonRpc("/states", "call", { statess: statess })
        .then(function (result) {
          var selectDist = $("select[name='city_id']");
          var store;
          var selected = selectDist.data("value");

          $("select[name='city_id']").parent("div").hide();
          selectDist.html("");
          _.each(result.city, function (x) {
            if (countcitychange == 0) {
              var opt = $("<option>")
                .text(x[1])
                .attr("value", x[0])

                .attr("selected", x[0] === selected);

              selectDist.append(opt);
              store = x[0];
              storecitychange = x[0];
              storezipchange = x[2];
              countcitychange = 2;
            } else {
              var opt = $("<option>")
                .text(x[1])
                .attr("value", x[0])

                .attr("selected", x[0] === selected);

              selectDist.append(opt);
              store = x[0];
            }
          });
          $("select[name='city_id']").parent("div").show();
          var selDist = document.getElementById("citys");
          selDist.value = storecitychange;
          var selZip = document.getElementById("zipcode");
          selZip.value = storezipchange;
        });
    });

    $("#citys").on("change", function () {
      // console.log("onchange");
      console.log("city change");
      var city1 = document.getElementById("citys").value;
      // var storecitychange;
      // var countcitychange = 0;
      // var storezipchange;
      ajax
        .jsonRpc("/city", "call", { city1: city1 })
        .then(function (result) {
          // console.log(result);
          var selectzip = document.getElementById("zipcode");
          // console.log(selectzip);
          selectzip.value = result;
        });
    });

  });
});
