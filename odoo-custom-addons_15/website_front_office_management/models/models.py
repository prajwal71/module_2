# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.http import request
import werkzeug.urls
import werkzeug.utils
from werkzeug.exceptions import BadRequest
import json
import logging
_logger = logging.getLogger(__name__)


class VisitorCustom(models.Model):
    _inherit = 'fo.visitor'

    virtual_user_id = fields.Char('Virtual User')
    number_of_visit = fields.Integer('Number of Visit')
    number_of_visit_words = fields.Char('Number of Visit')
    creation_type = fields.Selection([
        ('manual','manual'),('Linkedin','Linkedin'),('Facebook','Facebook'),('Google','Google')
        
        ],default='manual')
    previous_creation_type = fields.Selection([
        ('manual','manual'),('Linkedin','Linkedin'),('Facebook','Facebook'),('Google','Google')
        
        ])

class WebsiteCustom(models.Model):
    _inherit='website'

    # linkedin start  ====
    enable_linkedin = fields.Boolean(default=False)
    linked_secret_key = fields.Char(string='Linkedin Secret Key')
    linkedin_client_id = fields.Char(string='Linkedin Client ID')  # Our identifier
    linkedin_auth_endpoint = fields.Char(string='Linkedin Authentication URL', required=False)  # OAuth provider URL to authenticate users
    linkedin_scope = fields.Char()  # OAUth user data desired to access
    linkedin_validation_endpoint = fields.Char(string='Linkedin Validation URL', required=False)  # OAuth provider URL to validate tokens
    data_endpoint = fields.Char(string='Data URL')

    # -----field for google
    enable_google = fields.Boolean(default=False)
    google_client_id = fields.Char(string='Google Client ID')  # Our identifier
    google_auth_endpoint = fields.Char(string='Google Authentication URL', required=False)  # OAuth provider URL to authenticate users
    google_scope = fields.Char()  # OAUth user data desired to access
    google_validation_endpoint = fields.Char(string='Google Validation URL', required=False)  # OAuth provider URL to validate tokens
    google_data_endpoint = fields.Char(string='Google Data URL')

    # -----field for facebook
    enable_facebook = fields.Boolean(default=False)
    facebook_client_id = fields.Char(string='Facebook Client ID')  # Our identifier
    facebook_auth_endpoint = fields.Char(string='Facebook Authentication URL', required=False)  # OAuth provider URL to authenticate users
    facebook_scope = fields.Char()  # OAUth user data desired to access
    facebook_validation_endpoint = fields.Char(string='Facebook Validation URL', required=False)  # OAuth provider URL to validate tokens
    facebook_data_endpoint = fields.Char(string='Facebook Data URL')
    
     # linkedin start  ====
    
    def get_linkedin_param(self):
        
        company = self.env.company
        website = company.website_id
        for rec in website:
            redirect ='/welcome-visitor'
            return_url = request.httprequest.url_root + 'create_visitor_linkedin/signin'
            # _logger.info("========%s=============1 return_url",return_url)
            state = dict(
                d=request.session.db,
                # p=provider['id'],
                r=werkzeug.urls.url_quote_plus(redirect),
            )
            token = request.params.get('token')
            if token:
                state['t'] = token
            param={}
            params = dict(
                response_type='code',
                client_id= rec.linkedin_client_id,
                redirect_uri=return_url,
                scope=rec.linkedin_scope,
                state=json.dumps(state),
            )
            

            provider = "%s?%s" % (rec.linkedin_auth_endpoint, werkzeug.urls.url_encode(params))
            # _logger.info("==============%s=====provider======",provider)
            return provider

            # end of linkedin ======

            # ======start of google----
    
    def get_google_param(self):
        
        company = self.env.company
        website = company.website_id
        for rec in website:
            redirect ='/welcome-visitor'
            return_url = request.httprequest.url_root + 'create_visitor_google/signin'
            _logger.info("========%s=============1 return_url",return_url)
            state = dict(
                d=request.session.db,
                # p=provider['id'],
                r=werkzeug.urls.url_quote_plus(redirect),
            )
            token = request.params.get('token')
            if token:
                state['t'] = token
            param={}
            params = dict(
                response_type='token',
                client_id= rec.google_client_id,
                redirect_uri=return_url,
                scope=rec.google_scope,
                state=json.dumps(state),
            )
            

            provider = "%s?%s" % (rec.google_auth_endpoint, werkzeug.urls.url_encode(params))
            _logger.info("==============%s=====provider======",provider)
            return provider
    
    def get_facebook_param(self):
        
        company = self.env.company
        website = company.website_id
        for rec in website:
            redirect ='/welcome-visitor'
            return_url = request.httprequest.url_root + 'create_visitor_facebook/signin'
            _logger.info("========%s=============1 return_url",return_url)
            state = dict(
                d=request.session.db,
                # p=provider['id'],
                r=werkzeug.urls.url_quote_plus(redirect),
            )
            token = request.params.get('token')
            if token:
                state['t'] = token
            param={}
            params = dict(
                response_type='token',
                client_id= rec.facebook_client_id,
                redirect_uri=return_url,
                scope=rec.facebook_scope,
                state=json.dumps(state),
            )
            

            provider = "%s?%s" % (rec.facebook_auth_endpoint, werkzeug.urls.url_encode(params))
            _logger.info("==============%s=====provider======",provider)
            return provider
    



    
    
