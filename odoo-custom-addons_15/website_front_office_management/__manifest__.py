
{
    'name': "Website Front Office Management",

    'summary': """
        create visitor from website""",

    'description': """
       create visitor from website
    """,

    'author': "Nexus",
    'website': "http://www.nexusgurus.com",
    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['website','front_office_management'],


    'data': [

        'views/templates.xml',
        'views/view.xml',
       
    ],

    
}
