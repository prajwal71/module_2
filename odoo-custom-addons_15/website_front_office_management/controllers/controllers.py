  # -*- coding: utf-8 -*-
from odoo import http,tools,_
from odoo.http import request
import functools
import json
import base64
import pprint
import requests
from num2words import num2words
from werkzeug.exceptions import BadRequest
import logging
_logger = logging.getLogger(__name__)

def fragment_to_query_string(func):
    @functools.wraps(func)
    def wrapper(self, *a, **kw):
        kw.pop('debug', False)
        if not kw:
            return """<html><head><script>
                var l = window.location;
                var q = l.hash.substring(1);
                var r = l.pathname + l.search;
                if(q.length !== 0) {
                    var s = l.search ? (l.search === '?' ? '' : '&') : '?';
                    r = l.pathname + l.search + s + q;
                }
                if (r == l.pathname) {
                    r = '/';
                }
                window.location = r;
            </script></head><body></body></html>"""
        return func(self, *a, **kw)
    return wrapper

class VisitorForm(http.Controller):


    # linkedin start from here===============

    def get_profile_front_office(self,access_token):
        _logger.info("=========last==")
        URL = "https://api.linkedin.com/v2/me"

        
        headers = {'Content-Type': 'application/x-www-form-urlencoded','Authorization':'Bearer {}'.format(access_token),'X-Restli-Protocol-Version':'2.0.0'}
        response = requests.get(url=URL, headers=headers)
        _logger.info("====dats===%s",response.json())
        print(response.json())
        return response.json()

    def get_email_front_office(self,access_token):
        _logger.info("=========last==")
        URL = "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))"
        
        headers = {'Content-Type': 'application/x-www-form-urlencoded','Authorization':'Bearer {}'.format(access_token),'X-Restli-Protocol-Version':'2.0.0'}
        response = requests.get(url=URL, headers=headers)
        _logger.info("====dats===%s",response.json())
        print(response.json())
        return response.json()
    
    def get_pic_front_office(self,access_token):
        _logger.info("=========last==")
        URL = "https://api.linkedin.com/v2/me?projection=(id,profilePicture(displayImage~digitalmediaAsset:playableStreams))"
        headers = {'Content-Type': 'application/x-www-form-urlencoded','Authorization':'Bearer {}'.format(access_token),'X-Restli-Protocol-Version':'2.0.0'}
        response = requests.get(url=URL, headers=headers)
        data = response.json()
        
        profile_pic = data.get('profilePicture')
        if profile_pic:
            _logger.info("=================poriflce  check     %s",profile_pic)
            _logger.info(
                "post-processing values for acquirer with id :===========%s",
                pprint.pformat(profile_pic)
            ) 
            img = profile_pic.get('displayImage~')
            _logger.info(
                "post-processing values for acquirer with id :==img=========%s",
                pprint.pformat(img)
            ) 
            # _logger.info("+===================profile===pic====%s======%s",profile_pic,type(profile_pic))
            elements = img.get('elements')
            _logger.info(
                "post-processing values for acquirer with id :==elements=========%s",
                pprint.pformat(elements)
            ) 
            
            elements_first = elements[0]
            _logger.info(
                "post-processing values for acquirer with id :==elements_first=========%s",
                pprint.pformat(elements_first)
            ) 
            identifiers = elements_first.get('identifiers')
            _logger.info(
                "post-processing values for acquirer with id :==identifiers=========%s",
                pprint.pformat(identifiers)
            ) 
            identifiers_first = identifiers[0]
            _logger.info(
                "post-processing values for acquirer with id :==identifiers_first=========%s",
                pprint.pformat(identifiers_first)
            ) 
            pic_url = identifiers_first.get('identifier')
            _logger.info(
                "post-processing values for acquirer with id :==pic_url=========%s",
                pprint.pformat(pic_url)
            ) 
            return identifiers_first
        else:
            return False

    def fetch_image_front_office(self, url):
        if not url:
            return False
        response = requests.get(url)
        image_base64 = False
        if 'image/' in response.headers['Content-Type']:
            image_base64 = base64.b64encode(response.content)
        return image_base64

    @http.route('/visitor-registration',  type='http', auth="public", website=True)
    def render_visitor_form(self, **kw):
        return request.render("website_front_office_management.visitor_signup", {})

    # @http.route('/create-visitor',  type='http', auth="public", website=True)
    # def render_visitor_form_data(self, **post):
    #     vals = {}
    #     vals['name'] = post.get('name')
        
    #     vals['phone'] = post.get('phone')
    #     vals['email'] = post.get('email')
    #     email = post.get('email')
    #     exist_visitor_email = request.env['fo.visitor'].sudo().search([('email','=',email)],limit=1)
    #     if exist_visitor_email:
    #         exist_visitor_email.sudo().write({'number_of_visit':exist_visitor_email.number_of_visit + 1})
    #         exist_visitor_email.sudo().write({'number_of_visit_words':num2words(exist_visitor_email.number_of_visit, lang='en_GB', to='ordinal')})
    #         exist_visitor_email.sudo().write({'previous_creation_type':exist_visitor_email.creation_type})
    #         exist_visitor_email.sudo().write({'creation_type':'manual'})
    #         # return request.redirect('/contactus')
    #         return request.render("website_front_office_management.welcome_visitor", {'datas':exist_visitor_email})
    #     else:
    #         vals['number_of_visit'] = 1

    #     return request.render("website_front_office_management.visitor_signup", {})

    
    @http.route('/create_visitor_linkedin/signin', type='http', auth='public',website=True)
    @fragment_to_query_string
    def create_visitor_from_linkedin(self, **kw):
        if kw.get('code'):
            state = kw['state']
            a = json.dumps(kw)
            b = json.loads(a)
            state = eval(state)
        else:
            state = json.loads(kw['state'])

        dbname = state['d']
        if not http.db_filter([dbname]):
            return BadRequest()

        headers = {'Content-Type': 'application/x-www-form-urlencoded', 'User-Agent': 'OAuth gem v0.4.4'}
        access_token = kw.get('code')
        AUTH_CODE = access_token
        websites = request.env['website'].sudo().search([])
        _logger.info("==websites===========%s======",websites)
        for website in websites:
            if website.enable_linkedin:
                if website.linkedin_client_id:
                    linkedin_client_id = website.linkedin_client_id
                if website.linked_secret_key:
                    linked_secret_key = website.linked_secret_key

        ACCESS_TOKEN_URL = 'https://www.linkedin.com/oauth/v2/accessToken'
        client_id= str(linkedin_client_id)
        client_secret= str(linked_secret_key)

        redirect_url = request.httprequest.url_root + 'create_visitor_linkedin/signin'
        PARAM = {'grant_type': 'authorization_code',
        'code': AUTH_CODE,
        'redirect_uri': redirect_url,
        'client_id': client_id,
        'client_secret': client_secret}
        response = requests.post(ACCESS_TOKEN_URL, data=PARAM, headers=headers, timeout=600)
        response = response.json()
        validation = response
        _logger.info("====validation               %s   gap  ==========response  %s=====",validation,response)
        access_token = validation['access_token'] 
        data = self.get_profile_front_office(access_token)
        validation.update(data)
        data1 = self.get_email_front_office(access_token)
        validation.update(data1)
        data2 = self.get_pic_front_office(access_token)
        if data2:
            validation.update(data2)
        elements = validation.get('elements')
        elements_first = elements[0]
        elements_handle = elements_first.get('handle~') 
        email = elements_handle.get('emailAddress')
        image_url = validation.get('identifier')
        image_1920 = self.fetch_image_front_office(image_url)
        #   _logger.info("=========validation===========%s=========",validation)
        vals = {
            # 'oauth_access_token': access_token,
            'name': validation.get('localizedFirstName') + ' '+ validation.get('localizedLastName'),
            'virtual_user_id': str(validation.get('id')) ,
            'email': email,
            'visitor_image': image_1920,
            
        }
        if validation.get('localizedFirstName') and email:
            exist_id = str(validation.get('user_id'))
            exist_visitor_user_id = request.env['fo.visitor'].sudo().search([('virtual_user_id','=',exist_id)],limit=1)
            exist_visitor_email = request.env['fo.visitor'].sudo().search([('email','=',email)],limit=1)
            if not exist_visitor_user_id:
                if not exist_visitor_email:
                    visitor_obj = request.env['fo.visitor'].sudo().create(vals)
                    
                    if visitor_obj:
                        visitor_obj.sudo().write({'number_of_visit':1})
                        visitor_obj.sudo().write({'number_of_visit_words':'First'})
                        visitor_obj.sudo().write({'creation_type':'Linkedin'})
                    #   visitor_obj.sudo().write({'number_of_visit':1})
                        # request.redirect('/contactus')
                        # return request.render("website_front_office_management.visitor_signup", {})
                        return request.render("website_front_office_management.welcome_visitor", {'datas':visitor_obj})
                else:
                    exist_visitor_email.sudo().write({'number_of_visit':exist_visitor_email.number_of_visit + 1})
                    exist_visitor_email.sudo().write({'number_of_visit_words':num2words(exist_visitor_email.number_of_visit, lang='en_GB', to='ordinal')})
                    exist_visitor_email.sudo().write({'previous_creation_type':exist_visitor_email.creation_type})
                    exist_visitor_email.sudo().write({'creation_type':'Linkedin'})
                    # return request.redirect('/contactus')
                    return request.render("website_front_office_management.welcome_visitor", {'datas':exist_visitor_email})

            else:
                exist_visitor_user_id.sudo().write({'number_of_visit':exist_visitor_user_id.number_of_visit + 1})
                exist_visitor_user_id.sudo().write({'number_of_visit_words':num2words(exist_visitor_user_id.number_of_visit, lang='en_GB', to='ordinal')})
                exist_visitor_user_id.sudo().write({'previous_creation_type':exist_visitor_user_id.creation_type})
                exist_visitor_user_id.sudo().write({'creation_type':'Linkedin'})
                # display time of visit=================
                # return request.redirect('/contactus')
                return request.render("website_front_office_management.welcome_visitor", {'datas':exist_visitor_user_id})
        else:
        # ------no email part=----
            pass


# ==========end of linkedin===

   
    def _auth_oauth_rpc_google(self, endpoint, access_token):
        return requests.get(endpoint, params={'access_token': access_token}).json()

    @http.route('/create_visitor_google/signin', type='http', auth='public',website=True)
    @fragment_to_query_string
    def create_visitor_from_google(self, **kw):
        
      
      
        if kw.get('code'):
            state = kw['state']
            a = json.dumps(kw)
            b = json.loads(a)
            state = eval(state)
        else:
            state = json.loads(kw['state'])

        dbname = state['d']
        if not http.db_filter([dbname]):
            return BadRequest()
        _logger.info("================%s============kw====",kw)
        headers = {'Content-Type': 'application/x-www-form-urlencoded', 'User-Agent': 'OAuth gem v0.4.4'}
        access_token = kw.get('access_token')
        _logger.info("================%s============access_token====",access_token)
       
        websites = request.env['website'].sudo().search([])
        _logger.info("==websites===========%s======",websites)
        for website in websites:
            if website.enable_google:
                
                if website.google_validation_endpoint:
                    google_validation_endpoint = website.google_validation_endpoint
                if website.google_data_endpoint:
                    google_data_endpoint = website.google_data_endpoint

        validation = self._auth_oauth_rpc_google(google_validation_endpoint, access_token)
        data = self._auth_oauth_rpc_google(google_data_endpoint, access_token)
        validation.update(data)
        _logger.info("================%s============validation====",validation)
        
        
       
        email = validation.get('email')
        image_url = validation.get('picture')
        image_1920 = self.fetch_image_front_office(image_url)
        #   _logger.info("=========validation===========%s=========",validation)
        vals = {
            # 'oauth_access_token': access_token,
            'name': validation.get('name'),
            'virtual_user_id': str(validation.get('user_id')) ,
            'email': email,
            'visitor_image': image_1920,
            
        }
        if validation.get('name') and email:
            exist_id = str(validation.get('user_id'))
            exist_visitor_user_id = request.env['fo.visitor'].sudo().search([('virtual_user_id','=',exist_id)],limit=1)
            exist_visitor_email = request.env['fo.visitor'].sudo().search([('email','=',email)],limit=1)
            if not exist_visitor_user_id:
                if not exist_visitor_email:
                    visitor_obj = request.env['fo.visitor'].sudo().create(vals)
                
                    if visitor_obj:
                        visitor_obj.sudo().write({'number_of_visit':1})
                        visitor_obj.sudo().write({'number_of_visit_words':'First'})
                        visitor_obj.sudo().write({'creation_type':'Google'})
                        
                        # request.redirect('/contactus')
                        # return request.render("website_front_office_management.visitor_signup", {})
                        # return request.redirect('/contactus')
                        return request.render("website_front_office_management.welcome_visitor", {'datas':visitor_obj})
                else:
                    exist_visitor_email.sudo().write({'number_of_visit':exist_visitor_email.number_of_visit + 1})
                    exist_visitor_email.sudo().write({'number_of_visit_words':num2words(exist_visitor_email.number_of_visit, lang='en_GB', to='ordinal')})
                    exist_visitor_email.sudo().write({'previous_creation_type':exist_visitor_email.creation_type})
                    exist_visitor_email.sudo().write({'creation_type':'Google'})
                    # return request.redirect('/contactus')
                    return request.render("website_front_office_management.welcome_visitor", {'datas':exist_visitor_email})

            else:
                exist_visitor_user_id.sudo().write({'number_of_visit':exist_visitor_user_id.number_of_visit + 1})
                exist_visitor_user_id.sudo().write({'number_of_visit_words':num2words(exist_visitor_user_id.number_of_visit, lang='en_GB', to='ordinal')})
                exist_visitor_user_id.sudo().write({'previous_creation_type':exist_visitor_user_id.creation_type})
                exist_visitor_user_id.sudo().write({'creation_type':'Google'})
                # display time of visit=================
                # return request.redirect('/contactus')
                return request.render("website_front_office_management.welcome_visitor", {'datas':exist_visitor_user_id})
        else:
            # no email msg---
            pass

    def _auth_oauth_rpc_facebook(self, endpoint, access_token):
        return requests.get(endpoint, params={'access_token': access_token}).json()

    @http.route('/create_visitor_facebook/signin', type='http', auth='public',website=True)
    @fragment_to_query_string
    def create_visitor_from_facebook(self, **kw):
        
      
      
        if kw.get('code'):
            state = kw['state']
            a = json.dumps(kw)
            b = json.loads(a)
            state = eval(state)
        else:
            state = json.loads(kw['state'])

        dbname = state['d']
        if not http.db_filter([dbname]):
            return BadRequest()
        _logger.info("================%s============kw====",kw)
        headers = {'Content-Type': 'application/x-www-form-urlencoded', 'User-Agent': 'OAuth gem v0.4.4'}
        access_token = kw.get('access_token')
        # _logger.info("================%s============access_token====",access_token)
       
        websites = request.env['website'].sudo().search([])
        # _logger.info("==websites===========%s======",websites)
        for website in websites:
            if website.enable_facebook:
                
                if website.facebook_validation_endpoint:
                    facebook_validation_endpoint = website.facebook_validation_endpoint
                if website.facebook_data_endpoint:
                    facebook_data_endpoint = website.facebook_data_endpoint

        validation = self._auth_oauth_rpc_facebook(facebook_validation_endpoint, access_token)
        data = self._auth_oauth_rpc_facebook(facebook_data_endpoint, access_token)
        validation.update(data)
        _logger.info("================%s============validation====",validation)
        
        
       
        email = validation.get('email')
        image_url = validation.get('picture')
        image_1920 = self.fetch_image_front_office(image_url)
        #   _logger.info("=========validation===========%s=========",validation)
        vals = {
            # 'oauth_access_token': access_token,
            'name': validation.get('name'),
            'virtual_user_id': str(validation.get('id')) ,
            'email': email,
            'visitor_image': image_1920,
            
        }
        if validation.get('name') and email:
            exist_id = str(validation.get('user_id'))
            exist_visitor_user_id = request.env['fo.visitor'].sudo().search([('virtual_user_id','=',exist_id)],limit=1)
            exist_visitor_email = request.env['fo.visitor'].sudo().search([('email','=',email)],limit=1)
            if not exist_visitor_user_id:
                if not exist_visitor_email:
                    visitor_obj = request.env['fo.visitor'].sudo().create(vals)
                
                    if visitor_obj:
                        visitor_obj.sudo().write({'number_of_visit':1})
                        visitor_obj.sudo().write({'number_of_visit_words':'First'})
                        visitor_obj.sudo().write({'creation_type':'Facebook'})
                        # return request.redirect('/contactus')
                        return request.render("website_front_office_management.welcome_visitor", {'datas':visitor_obj})
                else:
                    exist_visitor_email.sudo().write({'number_of_visit':exist_visitor_email.number_of_visit + 1})
                    exist_visitor_email.sudo().write({'number_of_visit_words':num2words(exist_visitor_email.number_of_visit, lang='en_GB', to='ordinal')})
                    exist_visitor_email.sudo().write({'previous_creation_type':exist_visitor_email.creation_type})
                    exist_visitor_email.sudo().write({'creation_type':'Facebook'})
                    # return request.redirect('/contactus')
                    return request.render("website_front_office_management.welcome_visitor", {'datas':exist_visitor_email})

            else:
                exist_visitor_user_id.sudo().write({'number_of_visit':exist_visitor_user_id.number_of_visit + 1})
                exist_visitor_user_id.sudo().write({'number_of_visit_words':num2words(exist_visitor_user_id.number_of_visit, lang='en_GB', to='ordinal')})
                exist_visitor_user_id.sudo().write({'previous_creation_type':exist_visitor_user_id.creation_type})
                exist_visitor_user_id.sudo().write({'creation_type':'Facebook'})
                # display time of visit=================
                # return request.redirect('/contactus')
                return request.render("website_front_office_management.welcome_visitor", {'datas':exist_visitor_user_id})
        else:
            # no email msg----
            pass
