from odoo import http, _
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from dateutil.relativedelta import relativedelta
import base64
import logging
import json
import random as r
_logger = logging.getLogger(__name__)
# import yaml
import werkzeug
from odoo.http import request
from odoo.addons.website_event.controllers.main import WebsiteEventController
from odoo.addons.website_event_sale.controllers.main import WebsiteEventSaleController


class WebsiteEventControllerExt(WebsiteEventController):
    
    @http.route(['''/event/<model("event.event"):event>/registration/confirm'''], type='http', auth="public", methods=['POST'], website=True)
    def registration_confirm(self, event, **post):
        # _logger.info("========attendees_sudo======%s============",post)
       

        registrations = self._process_attendees_form(event, post)
        attendees_sudo = self._create_attendees_from_registration_post(event, registrations).id
        att = int(attendees_sudo)
        record = request.env['event.registration'].sudo().search([('id','=',att)])
        if record:
            record.write({'manager_name':record.name})
            record.write({'manager_email':record.email})
            if record.phone:
                record.write({'manager_contact_no':record.phone})

        # attach = self.create_attach(attendees_sudo,post)

        
        return request.render('website_event_ext.attach', {'event':event.id,'attendees_sudo':attendees_sudo})
   
    @http.route("/attachment", type='http', auth="public", methods=['POST'], website=True)
    def create_attach(self,**post):
        # _logger.info('===========%s======self',manager)
        if post.get('attendees_sudo',False):
            att = int (post['attendees_sudo'])
            record = request.env['event.registration'].sudo().search([('id','=',att)])
            otp=""
            for i in range(6):
                otp+=str(r.randint(1,9))
            ticket_code = "QC-"+record.team_name +"-"+otp
            record.write({'team_name':ticket_code})
            

            if post.get('event',False):
                event = int (post['event'])
        # if attendees_sudo:
        #     _logger.info("========attendees_sudo======%s============",post)
        #     if post.get('pl1',False):
        #             Attachments = request.env['ir.attachment']
        #             name = post.get('pl1').filename      
        #             files = post.get('pl1').stream.read()
        #             # w_attachment_id = Attachments.sudo().create({
        #             #     'name':'Player 1',
        #             #     'type': 'binary',   
        #             #     'res_model': 'event.registration',
        #             #     'res_id':att,
        #             #     'datas': base64.encodestring(files),
        #             # }) 
        #             record.write({'pl1_image':base64.encodebytes(files)})
        #     if post.get('pl2',False):
        #             Attachments = request.env['ir.attachment']
        #             name = post.get('pl2').filename      
        #             files = post.get('pl2').stream.read()
        #             # w_attachment_id = Attachments.sudo().create({
        #             #    'name':'Player 2',
        #             #     'type': 'binary',   
        #             #     'res_model': 'event.registration',
        #             #    'res_id':att,
        #             #     'datas': base64.encodestring(files),
        #             # }) 
        #             record.write({'pl2_image':base64.encodebytes(files)})

            
        #     if post.get('pl3',False):
        #             Attachments = request.env['ir.attachment']
        #             name = post.get('pl3').filename      
        #             files = post.get('pl3').stream.read()
        #             # w_attachment_id = Attachments.sudo().create({
        #             #   'name':'Player 3',
        #             #     'type': 'binary',   
        #             #     'res_model': 'event.registration',
        #             #     'res_id':att,
        #             #     'datas': base64.encodestring(files),
        #             # }) 
        #             record.write({'pl3_image':base64.encodebytes(files)})
                
        #     if post.get('pl4',False):
        #             Attachments = request.env['ir.attachment']
        #             name = post.get('pl4').filename      
        #             files = post.get('pl4').stream.read()
        #             # w_attachment_id = Attachments.sudo().create({
        #             #    'name':'Player 4',
        #             #     'type': 'binary',   
        #             #     'res_model': 'event.registration',
        #             #    'res_id':att,
        #             #     'datas': base64.encodestring(files),
        #             # }) 
        #             record.write({'pl4_image':base64.encodebytes(files)})

        #     if post.get('pl5',False):
        #             Attachments = request.env['ir.attachment']
        #             name = post.get('pl5').filename      
        #             files = post.get('pl5').stream.read()
        #             # w_attachment_id = Attachments.sudo().create({
        #             #     'name':'Player 5',
        #             #     'type': 'binary',   
        #             #     'res_model': 'event.registration',
        #             #    'res_id':att,
        #             #     'datas': base64.encodestring(files),
        #             # }) 
        #             record.write({'pl5_image':base64.encodebytes(files)})

            if post.get('manager',False):
                    Attachments = request.env['ir.attachment']
                    name =post.get('manager').filename      
                    files = post.get('manager').stream.read()
                    # attachment_id = Attachments.sudo().create({
                    #     'name':'Manager',
                    #     'type': 'binary',   
                    #     'res_model': 'event.registration',
                    #         'res_id':att,
                    #     'datas': base64.encodestring(files),
                    # }) 
                    record.write({'manager_image':base64.encodebytes(files)})

            if post.get('team',False):
                    Attachments = request.env['ir.attachment']
                    name =post.get('team').filename      
                    files = post.get('team').stream.read()
                    # attachment_id = Attachments.sudo().create({
                    #     'name':'Manager',
                    #     'type': 'binary',   
                    #     'res_model': 'event.registration',
                    #         'res_id':att,
                    #     'datas': base64.encodestring(files),
                    # }) 
                    record.write({'team_logo':base64.encodebytes(files)})
                    # _logger.info("=========================%s=====w_attachment_id=====",attachment_id)
        if att:
            order = request.website.sale_get_order(force_create=False)
            if order.amount_total:
                return request.redirect("/shop/checkout")
            # free tickets -> order with amount = 0: auto-confirm, no checkout
            elif order:
                order.action_confirm()  # tde notsure: email sending ?
                request.website.sale_reset()
        return request.redirect(('/event/%s/registration/success?' % event) + werkzeug.urls.url_encode({'registration_ids': ",".join([str(att)])}))

# class WebsiteEventSaleControllerExt(WebsiteEventSaleController):
