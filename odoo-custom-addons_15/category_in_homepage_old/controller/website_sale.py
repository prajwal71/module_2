# ©  2008-2021 Deltatech
#              Dorin Hongu <dhongu(@)gmail(.)com
# See README.rst file on addons root folder for license details

from odoo.http import request
from odoo import fields, http, SUPERUSER_ID, tools, _
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.http import route, request
from odoo.exceptions import AccessError, MissingError, ValidationError
from werkzeug.exceptions import Forbidden, NotFound
import logging
_logger = logging.getLogger(__name__)
from odoo.addons.http_routing.models.ir_http import slug

class WebsiteSaleCity(WebsiteSale):
    def get_all_categories(self, category, cate_list=[], url=False, key=1):
        try:
            id = int(category)
            category = request.env['product.public.category'].browse(id)
        except ValueError:
            category = category
        cate_list.append({"name": category.name, "url": url, "key": key})
        if len(category.parent_id)>0:
            url = "/shop/category/%s" % slug(category.parent_id)
            self.get_all_categories(category.parent_id, cate_list, url, key+1)
        return sorted(cate_list, key = lambda i: i.get('key') if i.get('key') else True, reverse=True)

    
    # def get_price_filer_domain(self, min="", max=""):
    #     res = {}
    #     ICPSudo = request.env['ir.default'].sudo()
    #     min_limit = ICPSudo.get('xtremo.res.config.settings', 'website_min_price_filter') or 0
    #     max_limit = ICPSudo.get('xtremo.res.config.settings', 'website_max_price_filter') or 100000
    #     res["min_limit"] = min_limit
    #     res["max_limit"] = max_limit
    #     res["min-price"] = min if min and int(min) >= 0 else min_limit
    #     res["max-price"] = max if max else max_limit
    #     url = request.httprequest.full_path
    #     prev_1 = "min_price="+str(min)
    #     prev_2 = "max_price="+str(max)
    #     if (prev_1 in url) and (prev_2 in url):
    #         url = url.replace(prev_1, "min_price=xtremo-lower-val")
    #         url = url.replace(prev_2, "max_price=xtremo-higher-val")
    #     elif ("?" in url) and (str(min) not in url) and (str(max) not in url):
    #         url = "{}&min_price={}&max_price={}".format(url, "xtremo-lower-val", "xtremo-higher-val")
    #     else:
    #         url = "{}?min_price={}&max_price={}".format(url, "xtremo-lower-val", "xtremo-higher-val")
    #     res['filter-url'] = url
    #     return res

    # def _get_search_domain(self, search, category, attrib_values):
    #     domain = super(WebsiteSale, self)._get_search_domain(search, category, attrib_values)
    #     min_price = request.httprequest.args.get('min_price')
    #     max_price = request.httprequest.args.get('max_price')
    #     if min_price and max_price:
    #         domain.append(('lst_price','>=',min_price))
    #         domain.append(('lst_price','<=',max_price))
    #     return domain

    # @route(['/xtremo/get-feature'], type="json", website=True, auth="public")
    # def get_feature(self, ref=False, **post):
    #     template = False
    #     t_name = False
    #     xt_products = False
    #     if ref == "feature":
    #         t_name = "theme_xtremo.xtremo_home_page_feature"
    #         xt_products = request.website.get_all_featured_products()
    #     elif ref == "category":
    #         t_name = "theme_xtremo.xtremo_home_page_category"
    #         xt_products = request.website.get_all_category_products()
    #     elif ref == "top_sale":
    #         t_name = "theme_xtremo.xtremo_home_page_top_sales"
    #         xt_products = request.website.get_all_top_sale_products()
    #     elif ref == "top_rated":
    #         t_name = "theme_xtremo.xtremo_home_page_top_rated"
    #         xt_products = request.website.get_all_top_listed_products()
    #     if t_name:
    #         template = request.env['ir.ui.view'].render_template(t_name, {"xt_products": xt_products})
    #     return template

    # @route(['/xtremo/get-category-feature'], type="json", website=True, auth="public")
    # def get_category_feature(self, ref=False, **post):
    #     template = request.env['ir.ui.view'].render_template('theme_xtremo.xtremo_banner_with_category_item', {})
    #     return template

  
