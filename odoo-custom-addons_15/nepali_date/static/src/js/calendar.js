odoo.define('nepali_date.config', function(require) {
    "use strict"; 
    var rpc = require('web.rpc')
    var date_mode = 'bs'
    var date_format = 'MM dd, yyyy'
    // var report_date_mode = 'both'

    var mode = rpc.query({
              model: 'ir.config_parameter',
              method: 'get_param',
              args: ['nepali_date.default_datepicker']
          }).then(function (res) {
              date_mode = res;
          });
 
    var format = rpc.query({
              model: 'ir.config_parameter',
              method: 'get_param',
              args: ['nepali_date.date_format']
          }).then(function (res) {
              date_format = res;
              console.log('hello : ', date_format)
          });

    // var date_mode_query = rpc.query({
    //           model: 'ir.config_parameter',
    //           method: 'get_param',
    //           args: ['nepali_date.report.date_mode']
    //       }).then(function (res) {
    //           report_date_mode = res;
    //       });
    var config = {
      'date_mode': 'bs',
      'date_format': date_format,
    //   'report_date_mode': report_date_mode,
    };

    return config;
});