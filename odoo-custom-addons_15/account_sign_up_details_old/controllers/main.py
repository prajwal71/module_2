# -*- coding: utf-8 -*-
#################################################################################
#
# Copyright (c) 2018-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>:wink:
# See LICENSE file for full copyright and licensing details.
#################################################################################
from odoo import http, _
import logging
from odoo.http import request
from odoo.exceptions import UserError
from odoo.addons.web.controllers.main import ensure_db, Home
import logging
_logger = logging.getLogger(__name__)
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.portal.controllers.portal import CustomerPortal



from odoo.addons.auth_signup.models.res_users import SignupError
import werkzeug
from odoo.addons.portal.controllers.portal import pager as portal_pager, CustomerPortal

_logger = logging.getLogger(__name__)

class AuthSignupHome(Home):
	def do_signup(self, qcontext):
		""" Shared helper that creates a res.partner out of a token """
		
		values = { key: qcontext.get(key) for key in ('login', 'name', 'password', 'mobile') }
		_logger.info("...<<<<<<<<<>>>>>>>>>>>>>>>>>>>>..%s",values)
		if not values:
			raise UserError(_("The form was not properly filled in."))
		if values.get('password') != qcontext.get('confirm_password'):
			raise UserError(_("Passwords do not match; please retype them."))
		supported_lang_codes = [code for code, _ in request.env['res.lang'].get_installed()]
		lang = request.context.get('lang', '').split('_')[0]
		if lang in supported_lang_codes:
			values['lang'] = lang
		self._signup_with_values(qcontext.get('token'), values)
		request.env.cr.commit()

	@http.route('/web/signup', type='http', auth='public', website=True, sitemap=False)
	def web_auth_signup(self, *args, **kw):

		# res = super(AuthSignupHome,self).web_auth_signup(*args, **kw)
		# _logger.info("=======login====")
		# qcontext = self.get_auth_signup_qcontext()
		# if 'error' not in qcontext and request.httprequest.method == 'POST':
		# 	mbl = kw['mobile']
		# 	_logger.info("====22===login====")
		
		# 	res.qcontext.update({'mobile':mbl})
		# return res
	
		qcontext = self.get_auth_signup_qcontext()
       
	

		if not qcontext.get('token') and not qcontext.get('signup_enabled'):
			raise werkzeug.exceptions.NotFound()

		if 'error' not in qcontext and request.httprequest.method == 'POST':
			try:
				mbl = kw['mobile']
				# cnty = kw['country_id']
				# qcontext['mobile'] = mbl
				qcontext['mobile'] = mbl
				# qcontext['country_id'] = cnty
				_logger.info("==============prajwla2====%s============",mbl)
				self.do_signup(qcontext)
				# Send an account creation confirmation email
				if qcontext.get('token'):
					user_sudo = request.env['res.users'].sudo().search([('login', '=', qcontext.get('login'))])
					template = request.env.ref('auth_signup.mail_template_user_signup_account_created',
												raise_if_not_found=False)
					if user_sudo and template:
						template.sudo().with_context(
							lang=user_sudo.lang,
							auth_login=werkzeug.url_encode({'auth_login': user_sudo.email}),
						).send_mail(user_sudo.id, force_send=True)
				return self.web_login(*args, **kw)
			except UserError as e:
				_logger.error("-----actual 1 signup error-----%s", e)
				qcontext['error'] = e.name or e.value
			except (SignupError, AssertionError) as e:
				if request.env["res.users"].sudo().search([("login", "=", qcontext.get("login"))]):
					qcontext["error"] = _("Another user is already registered using this email address.")
				else:
					_logger.error("-----actual 2 signup error-----%s", e)
					qcontext['error'] = _("Could not create a new account.")

		response = request.render('auth_signup.signup', qcontext)
		response.headers['X-Frame-Options'] = 'DENY'
		return response


class CustomerPortalCity(CustomerPortal):
    MANDATORY_BILLING_FIELDS = ["name", "email", "mobile","street"]
    OPTIONAL_BILLING_FIELDS = ["zipcode","country_id","state_id","company_name","vat"]

    def _prepare_portal_layout_values(self):
        values = super(CustomerPortalCity, self)._prepare_portal_layout_values()
        # cities = request.env['res.city'].sudo().search([])
        # values['cities'] = cities
        
        return values

class WebsiteSaleCity(WebsiteSale):

	def values_postprocess(self, order, mode, values, errors, error_msg):
		new_values, errors, error_msg = super(WebsiteSaleCity, self).values_postprocess(
			order, mode, values, errors, error_msg
		)
		
		new_values["mobile"] = values.get("mobile")


		# if new_values["city_id"]:
		#     city = request.env["res.city"].browse(int(values.get("city_id")))
		#     if city:
		#         new_values["city"] = city.name
		return new_values, errors, error_msg
	def _get_mandatory_fields_billing(self, country_id=False):
		req = ["name", "email","street","mobile"]

		return req

	def _get_mandatory_fields_shipping(self, country_id=False):
		req = ["name", "email","street","mobile"]
		
		return req


	
	
