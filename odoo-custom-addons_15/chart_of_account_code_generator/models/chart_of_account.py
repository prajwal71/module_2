from odoo import models, api,fields,_
import logging
from odoo.exceptions import AccessError, UserError
_logger = logging.getLogger(__name__)

class ChartAccount(models.Model):
    _inherit = 'account.account'

    code = fields.Char(size=64,readonly=True, required=False, index=True, tracking=True)

    @api.model 
    def create(self,vals):
        comapny_name = self.env.company
        comapny_chart = comapny_name.prefix_chart
        if not comapny_chart:
            raise UserError(_("Please Set Prefix For This Company"))
        user_type = vals.get('user_type_id')
        account_record = self.env['account.account.type'].search([('id','=',user_type)])
        account_type_prefix = account_record.prefix
        if not account_type_prefix:
            raise UserError(_("Please Set Prefix For Account Type"))
        internal_type = account_record.name
        sq_cod = account_record.seq
        if int(sq_cod)<10:
            vals['code'] = str(account_type_prefix) + '-' + str(comapny_chart) +'-' +'000'+str(sq_cod)
        if int(sq_cod)>9 and int(sq_cod)<100:
            vals['code'] = str(account_type_prefix) + '-' + str(comapny_chart) +'-' +'00'+str(sq_cod)
        if int(sq_cod)>99 and int(sq_cod)<1000:
            vals['code'] = str(account_type_prefix) + '-' + str(comapny_chart) +'-' +'0'+str(sq_cod)
        if int(sq_cod)>999 and int(sq_cod)<10000:
            vals['code'] = str(account_type_prefix) + '-' + str(comapny_chart) +'-' +str(sq_cod)
        sq_cod_convert = int(sq_cod) + 1
        account_record.seq = str(sq_cod_convert)
        _logger.info("=============new====sequence=====%s",account_record.seq)
        # if internal_type == 'Receivable':
        #     vals['code'] = str(comapny_chart) +'-' + str(account_type_prefix) + '-' + self.env['ir.sequence'].next_by_code(
        #         'Receivable.sequence')
        # elif internal_type == 'Payable':
        #     vals['code'] = str(comapny_chart) +'-' + str(account_type_prefix) + '-' + self.env['ir.sequence'].next_by_code(
        #         'Payable.sequence')
        # elif internal_type == 'Bank and Cash':
        #     vals['code'] = str(comapny_chart) +'-' + str(account_type_prefix) + '-' + self.env['ir.sequence'].next_by_code(
        #         'Bank.Cash.sequence')
        # elif internal_type == 'Receivable':
        #     vals['code'] = str(comapny_chart) +'-' + str(account_type_prefix) + '-' + self.env['ir.sequence'].next_by_code(
        #         'Receivable.sequence')
        # elif internal_type == 'Receivable':
        #     vals['code'] = str(comapny_chart) +'-' + str(account_type_prefix) + '-' + self.env['ir.sequence'].next_by_code(
        #         'Receivable.sequence')

        # elif internal_type == 'Receivable':
        #     vals['code'] = str(comapny_chart) +'-' + str(account_type_prefix) + '-' + self.env['ir.sequence'].next_by_code(
        #         'Receivable.sequence')
        # elif internal_type == 'Receivable':
        #     vals['code'] = str(comapny_chart) +'-' + str(account_type_prefix) + '-' + self.env['ir.sequence'].next_by_code(
        #         'Receivable.sequence')
        # elif internal_type == 'Receivable':
        #     vals['code'] = str(comapny_chart) +'-' + str(account_type_prefix) + '-' + self.env['ir.sequence'].next_by_code(
        #         'Receivable.sequence')
        # elif internal_type == 'Receivable':
        #     vals['code'] = str(comapny_chart) +'-' + str(account_type_prefix) + '-' + self.env['ir.sequence'].next_by_code(
        #         'Receivable.sequence')
        # elif internal_type == 'Receivable':
        #     vals['code'] = str(comapny_chart) +'-' + str(account_type_prefix) + '-' + self.env['ir.sequence'].next_by_code(
        #         'Receivable.sequence')
        # elif internal_type == 'Receivable':
        #     vals['code'] = str(comapny_chart) +'-' + str(account_type_prefix) + '-' + self.env['ir.sequence'].next_by_code(
        #         'Receivable.sequence')
        # elif internal_type == 'Receivable':
        #     vals['code'] = str(comapny_chart) +'-' + str(account_type_prefix) + '-' + self.env['ir.sequence'].next_by_code(
        #         'Receivable.sequence')
        
        res = super(ChartAccount,self).create(vals)
        return res


class ChartAccountExt(models.Model):
    _inherit = 'account.account.type'

    prefix =fields.Char(string="Prefix")
    seq=fields.Char(string="Sequence value",default="1")