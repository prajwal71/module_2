
from odoo import models, api,fields


class ResCompany(models.Model):
    _inherit = 'res.company'

    prefix_chart = fields.Char(string="Chart of Account Prefix")
