# -*- coding: utf-8 -*-
{
    'name': 'Chart of Account code Generator',
    'author': 'Nexus',
    'category': 'account',
    'license': 'AGPL-3',
    'website': 'www.nexusgurus.com.com',
    'version': '13.0.1.0.1',
    'summary': '''
               This module helps to generate sequence (code) number based on type
            ''',
    'description': '''
                   
                ''',
    'depends': ['base','account','odoo_account_type_menu'],
    'data': [
        'data/sequence.xml',
        'views/view.xml',
        'views/chart_view.xml'
    ],
   
    'installable': True,
    'auto_install': False,
    'application': False
}
