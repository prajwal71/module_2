odoo.define('khalti_payment.khalti', function(require) {
    "use strict";
    console.log("Niraj testing");
    var ajax = require('web.ajax');
    var core = require('web.core');
    var _t = core._t;
    var qweb = core.qweb;
    ajax.loadXML('/khalti_payment/static/src/xml/payment_khalti_templates.xml', qweb);

    require('web.dom_ready');
    
    if (!$('.o_payment_form').length) {
        return $.Deferred().reject("DOM doesn't contain '.o_payment_form'");
    }

    // var observer = new MutationObserver(function(mutations, observer) {
    //     for(var i=0; i<mutations.length; ++i) {
    //         for(var j=0; j<mutations[i].addedNodes.length; ++j) {
    //             if(mutations[i].addedNodes[j].tagName.toLowerCase() === "form" && mutations[i].addedNodes[j].getAttribute('provider') == 'khalti') {
    //                 display_khalti_form($(mutations[i].addedNodes[j]));
    //             }
    //         }
    //     }
    // });

    function khalti_show_error(msg) {
        var wizard = $(qweb.render('khalti.error', {'msg': msg || _t('Payment error')}));
        wizard.appendTo($('body')).modal({'keyboard': true});
    };

    function display_khalti_form(provider_form) {
        // Open Checkout with further options
        var payment_form = $('.o_payment_form');
        if(!payment_form.find('i').length)
        {
            payment_form.append('<i class="fa fa-spinner fa-spin"/>');
            payment_form.attr('disabled','disabled');
        }

        var get_input_value = function (name) {
            return provider_form.find('input[name="' + name + '"]').val();
        }
        var primaryColor = getComputedStyle(document.body).getPropertyValue('--primary');
        var options = {
            "publicKey": document.getElementById("publicKey").value,
            "amount": parseFloat(document.getElementById("amount").value),
            "productName": document.getElementById("productName").value,
            "productIdentity": document.getElementById("productIdentity").value,
            "productUrl" : "http://bookexample.com/",
            "eventHandler": {
                onSuccess (payload) {
                    if (payload.idx) {
                        $.post('/payment/khalti',{
                            payment_id: payload.idx,
                            amount:payload.amount,
                            token:payload.token,
                            productIdentity:payload.product_identity,
                        }).done(function (data) {
                            window.location.href = data;
                        }).fail(function (data) {
                            khalti_show_error(data && data.data && data.data.message);
                        });
                    }
                    // hit merchant api for initiating verfication
                    console.log(payload);
                },
                onError (error) {
                    console.log(error);
                },
                onClose () {
                    location.reload();
                    console.log('widget is closing');
                }
            }
            
        }
        var checkout = new KhaltiCheckout(options);
        checkout.show({amount: parseFloat(document.getElementById('amount').value)});
    };

    $.getScript("https://khalti.com/static/khalti-checkout.js", function (data, textStatus, jqxhr) {
        // observer.observe(document.body, {childList: true});
        display_khalti_form($('form[provider="khalti"]'));
    });
});