import logging
from tokenize import Double
import requests
import pprint
import json
from werkzeug import urls
import hashlib
import hmac
from odoo import api, fields, models, _
from odoo.tools.float_utils import float_compare, float_repr, float_round
from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.exceptions import UserError
from odoo.tools.safe_eval import safe_eval
from odoo.tools.float_utils import float_round
import xml.etree.ElementTree as ET
import datetime
# from xml.etree import ElementTree
_logger = logging.getLogger(__name__)


class PaymentAcquirer(models.Model):
    _inherit = 'payment.acquirer'

    # provider = fields.Selection(selection_add=[('khalti', 'Khalti')])
    provider = fields.Selection(selection_add=[('khalti', 'Khalti')], ondelete={'khalti': 'set default'})
    # khalti_key_id = fields.Char(string='Key ID', required_if_provider='khalti', groups='base.group_user')
    khalti_key_public = fields.Char(string='Key Public', required_if_provider='khalti', groups='base.group_user')
    khalti_key_secret = fields.Char(string='Key Secret', required_if_provider='khalti', groups='base.group_user')

    def khalti_form_generate_values(self, values):
        self.ensure_one()
        currency = self.env['res.currency'].sudo().browse(values['currency_id'])
        if currency != self.env.ref('base.NPR'):
            raise ValidationError(_('Currency not supported by Khalti'))
        values.update({
            'publicKey': self.khalti_key_public,
            'amount': float_repr(float_round(values.get('amount'), 2) * 100, 0),
            'productName': values.get('partner_name'),
            'productIdentity': values.get('reference'),
            # 'email': values.get('partner_email'),
            # 'order_id': values.get('reference'),
        })
        return values


class PaymentTransaction(models.Model):
    _inherit = 'payment.transaction'

    def _get_specific_rendering_values(self, processing_values):
        """ Override of payment to return Paypal-specific rendering values.

        Note: self.ensure_one() from `_get_processing_values`

        :param dict processing_values: The generic and specific processing values of the transaction
        :return: The dict of acquirer-specific processing values
        :rtype: dict
        """
        res = super()._get_specific_rendering_values(processing_values)
        if self.provider != 'khalti':
            return res

        if self.acquirer_id.state  == 'enabled':
            api_url =  "https://khalti.com/api/v2/payment/verify/"
        else:
            api_url =  "https://khalti.com/api/v2/payment/verify/"
        base_url = self.acquirer_id.get_base_url()
        # partner_first_name, partner_last_name = payment_utils.split_partner_name(self.partner_name)
        # notify_url = self.acquirer_id.paypal_use_ipn \
        #              and urls.url_join(base_url, PaypalController._notify_url)
        return {
            'address1': self.partner_address,
            'amount': float_repr(float_round(self.amount, 2) * 100, 0),
            # 'business': self.acquirer_id.paypal_email_account,
            'city': self.partner_city,
            'country': self.partner_country_id.code,
            'currency_code': self.currency_id.name,
            'email': self.partner_email,
            'first_name': self.partner_name,
            'handling': self.fees,
            'item_name': f"{self.company_id.name}: {self.reference}",
            'item_number': self.reference,
            
            'lc': self.partner_lang,
            # 'notify_url': notify_url,
            # 'return_url': urls.url_join(base_url, PaypalController._return_url),
            'state': self.partner_state_id.name,
            'zip_code': self.partner_zip,
            'acquirer': self.acquirer_id,
            'publicKey':self.acquirer_id.khalti_key_public,
            'productIdentity':self.reference,
            'productName': self.partner_name,
            'api_url': api_url,
        }
    # def _send_payment_request(self):
    #     """ Override of payment to simulate a payment request.

    #     Note: self.ensure_one()

    #     :return: None
    #     """
    #     super()._send_payment_request()
    #     if self.provider != 'khalti':
    #         return

    #     # The payment request response would normally transit through the controller but in the end,
    #     # all that interests us is the reference. To avoid making a localhost request, we bypass the
    #     # controller and handle the fake feedback data directly.
    #     self._handle_feedback_data('khalti', {'reference': self.reference})
   
    def _process_feedback_data(self, data):
        """ Override of payment to process the transaction based on dummy data.

        Note: self.ensure_one()

        :param dict data: The dummy feedback data
        :return: None
        :raise: ValidationError if inconsistent data were received
        """
        super()._process_feedback_data(data)
        if self.provider != "khalti":
            return

        # self._set_done()  # Dummy transactions are always successful
        if self.tokenize:
            token = self.env['payment.token'].create({
              'address1': self.partner_address,
            'amount': float_repr(float_round(self.amount, 2) * 100, 0),
            # 'business': self.acquirer_id.paypal_email_account,
            'city': self.partner_city,
            'country': self.partner_country_id.code,
            'currency_code': self.currency_id.name,
            'email': self.partner_email,
            'first_name': self.partner_name,
            'handling': self.fees,
            'item_name': f"{self.company_id.name}: {self.reference}",
            'item_number': self.reference,
            
            'lc': self.partner_lang,
            # 'notify_url': notify_url,
            # 'return_url': urls.url_join(base_url, PaypalController._return_url),
            'state': self.partner_state_id.name,
            'zip_code': self.partner_zip,
            'acquirer': self.acquirer_id,
            'publicKey':self.acquirer_id.khalti_key_public,
            'productIdentity':self.reference,
            'productName': self.partner_name,
            
                'verified': True,
            })
            self.token_id = token.id
            
    @api.model
    def _get_tx_from_feedback_data(self, provider, data):
        """ Override of payment to find the transaction based on Paypal data.

        :param str provider: The provider of the acquirer that handled the transaction
        :param dict data: The feedback data sent by the provider
        :return: The transaction if found
        :rtype: recordset of `payment.transaction`
        :raise: ValidationError if the data match no transaction
        """
        tx = super()._get_tx_from_feedback_data(provider, data)
        if provider != 'khalti':
            return tx

        reference = data.get('reference')
        tx = self.search([('reference', '=', reference), ('provider', '=', 'khalti')])
        if not tx:
            raise ValidationError(
                "khalti: " + _("No transaction found matching reference %s.", reference)
            )
        return tx
        # if tx:
        #     valid = self._khalti_form_validate(data)
        #     return valid
    
    # def _process_feedback_data(self, data):
    #     """ Override of payment to process the transaction based on Paypal data.

    #     Note: self.ensure_one()

    #     :param dict data: The feedback data sent by the provider
    #     :return: None
    #     :raise: ValidationError if inconsistent data were received
    #     """
    #     super()._process_feedback_data(data)
    #     # if self.provider != 'paypal':
    #     return
        
    @api.model
    def _create_khalti_capture(self, data):
        _logger.info("+=======================model=========")
        payment_acquirer = self.env['payment.acquirer'].search([('provider', '=', 'khalti')], limit=1)
        payment_url = "https://khalti.com/api/v2/payment/verify/"

        payload = {
                    "token": data.get('token'),
                    "amount": data.get('amount')
                }
        headers = {
        "Authorization": "Key %s" % (payment_acquirer.khalti_key_secret)
        }
        try:
            payment_response = requests.post(payment_url, payload, headers = headers)
            payment_response = payment_response.json()
            _logger.info("+====%s=======================model=========",payment_response)
        except Exception as e:
            raise e
        
        return payment_response

    @api.model
    def _khalti_form_get_tx_from_data(self, data):
        _logger.info(data)
        _logger.info("+==========_khalti_form_get_tx_from_data========model=========")
        reference, txn_id = data.get('reference'), data.get('idx')
        if not txn_id:
            error_msg = _('Khalti: received data with missing reference (%s) or txn_id (%s)') % (reference, txn_id)
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        txs = self.env['payment.transaction'].search([('reference', '=', reference)])
        if not txs or len(txs) > 1:
            error_msg = _('Khalti: received data for reference %s') % (reference)
            if not txs:
                error_msg += '; no order found'
            else:
                error_msg += '; multiple order found'
            _logger.info(error_msg)
            raise ValidationError(error_msg)
        return txs[0]

    def _khalti_form_get_invalid_parameters(self, data):
        _logger.info("+==========_khalti_form_get_invalid_parameters========model=========")
        invalid_parameters = []
        if float_compare(data.get('amount', '0.0') / 100, self.amount, precision_digits=2) != 0:
            invalid_parameters.append(('amount', data.get('amount'), '%.2f' % self.amount))
        return invalid_parameters

    def _khalti_form_validate(self, data):
        _logger.info("+==========_khalti_form_validate========model=========")
        status = data.get('state',{}).get('name')
        if status == "Completed":
            _logger.info('Validated Khalti payment for tx %s: set as done' % (self.reference))
            self.write({'acquirer_reference': data.get('idx')})
            self._set_done()
            return True
        # if status == 'authorized':
        #     _logger.info('Validated Razorpay payment for tx %s: set as authorized' % (self.reference))
        #     self.write({'acquirer_reference': data.get('id')})
        #     self._set_transaction_authorized()
        #     return True
        else:
            error = 'Received unrecognized status for Khalti payment %s: %s, set as error' % (self.reference, status)
            _logger.info(error)
            self.write({'acquirer_reference': data, 'state_message': data})
            self._set_transaction_cancel()
            return False

    def _khalti_form_validate_custom(self,data,record):
        status = data.get('state',{}).get('name')
        if status == "Completed":
            _logger.info('Validated Khalti payment for tx %s: set as done' % (self.reference))
            
            tran = self.env['payment.transaction'].search([('id','=',record)])
            tran.write({'acquirer_reference': data.get('idx')})
            tran._set_done()
            return True
        else :
            return False

class AccountPaymentMethod(models.Model):
    _inherit = 'account.payment.method'

    @api.model
    def _get_payment_method_information(self):
        res = super()._get_payment_method_information()
        res['khalti'] = {'mode': 'unique', 'domain': [('type', '=', 'bank')]}
        return res