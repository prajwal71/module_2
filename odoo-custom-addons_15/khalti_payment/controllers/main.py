# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import pprint

from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)


class KhaltiController(http.Controller):

   
    @http.route(['/payment/khalti'], type='http', auth='public', csrf=False)
    def khalti_capture(self, **kwargs):
        payment_id = kwargs.get('payment_id')
        reference = kwargs.get('productIdentity')
        # request.env['payment.transaction'].sudo()._get_tx_from_feedback_data('khalti',kwargs)
        if payment_id:
            response = request.env['payment.transaction'].sudo()._create_khalti_capture(kwargs)
            _logger.info("====%s=====response===============cntroller=====",response)
            _logger.info("========new====new=====prajwal")
            if response.get('idx'):
                response['reference'] = reference 
                _logger.info('khalti: entering form_feedback with post data %s', pprint.pformat(response))
                _logger.info("========new====new=====prajwal")
                tx = request.env['payment.transaction'].sudo()._handle_feedback_data('khalti',response)
                return request.redirect('/contactus')
                # if tx:
                #     request.render("website_sale.confirmation", {})
                # if tx:
                #     record = tx.id
                #     valid = request.env['payment.transaction'].sudo()._khalti_form_validate_custom(response,record)
                #     if valid:
                #         # request.render("website_sale.confirmation", {})
                #         return ''
                        # return request.redirect('/payment/status')
                        # return request.redirect('/contactus')
