# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import http
from odoo.http import request
from odoo.addons.website.controllers import form
import logging
import base64
import json
import html2text
_logger = logging.getLogger(__name__)


class ConvertMyHtmlText(object):

    def convert_html_to_text(result_txt):
        _logger.info("'===============================%s=======res======",result_txt)
        capt = b'%s' % (result_txt)
        convert_byte_to_str = capt.decode('utf-8')
        return html2text.html2text(convert_byte_to_str)

class WebsiteForm(form.WebsiteForm):


    # @http.route('/website_form/<string:model_name>', type='http', auth="public", methods=['POST'], website=True)
    # def website_form(self, model_name, **kwargs):
    #     if request.params.get('partner_email'):
    #         Partner = request.env['res.partner'].sudo().search([('email', '=', kwargs.get('partner_email'))], limit=1)
    #         if Partner:
    #             request.params['partner_id'] = Partner.id
    #     return super(WebsiteForm, self).website_form(model_name, **kwargs)

    @http.route(['/internal-support-ticket'], type='http', auth="public", website=True)
    def website_helpnesk_form(self, **kwargs):
        default_values = {}
        if request.env.user.partner_id != request.env.ref('base.public_partner'):
            default_values['name'] = request.env.user.partner_id.name
            default_values['email'] = request.env.user.partner_id.email
        return request.render("website_helpdesk_form_ext.internal-support-ticket", {'default_values': default_values})

    # @http.route(['/create-ticket'], method="post", type='http', auth='user', website=True, csrf=False)
    # def request(self, **post):
    #     """
    #     Browses all maintenance teams and equipments in backend and returns them to the web page
    #     """
    #     default_values = {}
    #     teams = request.env['helpdesk.team'].sudo().search([])
    #     if request.env.user.partner_id != request.env.ref('base.public_partner'):
    #         default_values['name'] = request.env.user.partner_id.name
    #         default_values['email'] = request.env.user.partner_id.email
    #     return request.render("website.create-ticket", {'default_values': default_values,'teams':teams})

    
    @http.route(['/ticket/submitted'], method="post", type='http', auth='user', website=True, csrf=False)
    def post_ticket(self, **post):
        vals={}
        vals['partner_name'] = post['partner_name']
        vals['partner_email'] = post['partner_email']
        team_id = post['team_id']
        _logger.info("==================%s=============value",post['team_id'])
        team = request.env['helpdesk.team'].sudo().search([('id','=',team_id)])
        _logger.info("==================%s=============value",post['add'])
        vals['team_id'] =team.id
        vals['name']= post['name']
        if post.get('priority',False):
            vals['priority'] = post['priority']
        if post.get('description',False):
            details = post['description']
            details = details.encode()
            details = ConvertMyHtmlText.convert_html_to_text(details)
            vals['details'] = details
        # if post['Attachment']:
        #     attachment = post['Attachment']


        create = request.env['helpdesk.ticket'].create(vals)
        if create:
            if post.get('attachment',False):
                            Attachments = request.env['ir.attachment']
                            name = post.get('attachment').filename      
                            files = post.get('attachment').stream.read()
                            # rec_id = request.env['product.warranty'].search([], limit=1, order='create_date desc')
                            # attachment = files.read() 
                            attachment_id = Attachments.sudo().create({
                                'name':name,
                                # 'datas_fname':name,
                                'type': 'binary',   
                                'res_model': 'helpdesk.ticket',
                                'res_id':create.id,
                                'datas': base64.encodestring(files),
                            })
        return request.render("website_helpdesk_form.ticket_submited")
        # create.write{'teamd_id.id':1}

        # attachment = post['Attachment']
