# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from lxml import etree


from odoo import api, fields, models
from odoo.addons.http_routing.models.ir_http import slug


class HelpdeskTicket(models.Model):
    _inherit = ['helpdesk.ticket']

    # details=fields.Char(string="Website Helpdesk")


