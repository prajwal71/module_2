from odoo import models, fields, api
import logging
logger = logging.getLogger(__name__)

class StockCustom(models.Model):
    _inherit = 'stock.move'

    def _should_bypass_reservation(self, forced_location=False):
        # self.ensure_one()
        location = forced_location or self.location_id
        return location.should_bypass_reservation() or self.product_id.type != 'product'