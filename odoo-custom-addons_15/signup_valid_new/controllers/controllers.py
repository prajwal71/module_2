# -*- coding: utf-8 -*-

from odoo.http import request
from odoo import http, _
import logging
import json
_logger = logging.getLogger(__name__)
from odoo.addons.web.controllers.main import ensure_db, Home
from odoo.exceptions import UserError
_logger = logging.getLogger(__name__)
from odoo.addons.auth_signup.models.res_users import SignupError
import werkzeug
from odoo.addons.portal.controllers.portal import pager as portal_pager, CustomerPortal



class AuthSignupHomeExt(Home):
    
    

    def do_signup(self, qcontext):
        """ Shared helper that creates a res.partner out of a token """
        
        values = { key: qcontext.get(key) for key in ('login', 'name', 'password', 'mobile','country_id') }
        _logger.info("=======================prajwal=========%s",values)
        
        if qcontext.get('country_id') is not None:
            values.update({'country_id': 167})
        _logger.info("=======================%s=====",values)
        if not values:
            raise UserError(_("The form was not properly filled in."))
        _logger.info('=====cc===%s',qcontext)
        email = values.get('login')
        company_id = request.env.user.company_id.id
        website_id = request.website.id
        user_already_exist = request.env['res.users'].sudo().search([('login', '=', email),('company_id','=',company_id)])
        if not user_already_exist:
            domain = [('email', '=', email),('company_id','=',company_id),('website_id','=',website_id)]
            signup_obj = request.env['signup.detail.verify'].sudo().search(domain,order='id desc')
            if signup_obj:
                if not signup_obj.is_done:
                    raise UserError(_("Your email is not verified. Please try again."))
            else:
                raise UserError(_("Your email is not verified. Please try again."))
        
        if values.get('password') != qcontext.get('confirm_password'):
            raise UserError(_("Passwords do not match; please retype them."))
        supported_lang_codes = [code for code, _ in request.env['res.lang'].get_installed()]
        lang = request.context.get('lang', '').split('_')[0]
        if lang in supported_lang_codes:
            values['lang'] = lang
        self._signup_with_values(qcontext.get('token'), values)
        request.env.cr.commit()
    
    @http.route('/web/signup', type='http', auth='public', website=True, sitemap=False)
    def web_auth_signup(self, *args, **kw):
        qcontext = self.get_auth_signup_qcontext()
       
        qcontext['countries'] = request.env['res.country'].sudo().search([])

        if not qcontext.get('token') and not qcontext.get('signup_enabled'):
            raise werkzeug.exceptions.NotFound()

        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                mbl = kw['mobile']
                # cnty = kw['country_id']
                qcontext['mobile'] = mbl
                # qcontext['country_id'] = cnty
                _logger.info("==============prajwla2====%s============",mbl)
                self.do_signup(qcontext)
                # Send an account creation confirmation email
                if qcontext.get('token'):
                    user_sudo = request.env['res.users'].sudo().search([('login', '=', qcontext.get('login'))])
                    template = request.env.ref('auth_signup.mail_template_user_signup_account_created',
                                               raise_if_not_found=False)
                    if user_sudo and template:
                        template.sudo().with_context(
                            lang=user_sudo.lang,
                            auth_login=werkzeug.url_encode({'auth_login': user_sudo.email}),
                        ).send_mail(user_sudo.id, force_send=True)
                return self.web_login(*args, **kw)
            except UserError as e:
                _logger.error("-----actual 1 signup error-----%s", e)
                qcontext['error'] = e.name or e.value
            except (SignupError, AssertionError) as e:
                if request.env["res.users"].sudo().search([("login", "=", qcontext.get("login"))]):
                    qcontext["error"] = _("Another user is already registered using this email address.")
                else:
                    _logger.error("-----actual 2 signup error-----%s", e)
                    qcontext['error'] = _("Could not create a new account.")

        response = request.render('auth_signup.signup', qcontext)
        response.headers['X-Frame-Options'] = 'DENY'
        return response

class SignupOtpController(http.Controller):

    @http.route('/web/validate-email-unq', csrf=False, type="http", methods=['POST', 'GET'], auth="public", website=True)
    def check_email_unique(self, **kw):
        """Check If email already Exist"""
        _logger.info("=====ajax called===")
        email = kw.get('email')
        company_id = request.env.user.company_id.id
        website_id = request.website.id
        _logger.info("=====ajax %s-%s-%s===",email,company_id,website_id)
        _logger.info("=====u===")
        domain = [('login', '=', email),('company_id','=',company_id)]
        _logger.info("=====u1===")
        user = request.env['res.users'].sudo().search(domain)
        _logger.info("=====user %s===",user)
        if user:
            res = {'result': 'exist'}
            return json.dumps(res)
        else:
            domain = [('email', '=', email),('company_id','=',company_id),('website_id','=',website_id)]
            signup_obj = request.env['signup.detail.verify'].sudo().search(domain,order='id desc')
            if not signup_obj:
                n_obj = request.env['signup.detail.verify'].sudo().create({
                    'email': email,
                    'company_id': company_id,
                    'website_id': website_id,
                })
                if n_obj:
                    n_obj.send_otp_email()
                    res = {'result': 'generated'}
                    return json.dumps(res)
                else:
                    res = {'result': 'An error occured'}
                    return json.dumps(res)
            elif signup_obj:
                signup_obj.send_otp_email()
                res = {'result': 'generated'}
                return json.dumps(res)
    
    # @http.route('/web/generate-otp', csrf=False, type="http", methods=['POST', 'GET'], auth="public", website=True)
    # def generate_otp(self, **kw):
    #     """Generate Otp"""
    #     email = kw.get('email')
    #     company_id = request.env.user.company_id.id
    #     website_id = request.env.user.website_id.id
    #     try:
    #         domain = [('email', '=', email),('company_id','=',company_id),('website_id','=',website_id)]
    #         signup_obj = request.env['signup.detail.verify'].sudo().search(domain)
    #         if not signup_obj:
    #             n_obj = request.env['signup.detail.verify'].sudo().create({
    #                 'email': email,
    #                 'company_id': company_id,
    #                 'website_id': website_id,
    #             })
    #             if n_obj:
    #                 n_obj.send_otp_email()
    #                 res = {'result': 'generated'}
    #             else:
    #                 res = {'result': 'An error occured'}
    #         elif signup_obj:
    #             signup_obj.send_otp_email()
    #             res = {'result': 'generated'}
    #     except:
    #         res = {'result': 'An error occured'}
    #     return json.dumps(res)
    
    @http.route('/web/validate-otp', csrf=False, type="http", methods=['POST', 'GET'], auth="public", website=True)
    def validate_otp(self, **kw):
        """Validate Otp"""
        _logger.info("=====ajax called===")
        email = kw.get('email')
        vcode = kw.get('vcode')
        company_id = request.env.user.company_id.id
        website_id = request.website.id
        _logger.info("=====ajax %s-%s-%s-%s===",vcode,email,company_id,website_id)
        domain = [('email', '=', email),('company_id','=',company_id),('website_id','=',website_id)]
        signup_obj = request.env['signup.detail.verify'].sudo().search(domain,order='id desc')
        if not signup_obj:
            res = {'result': 'An error occured'}
            return json.dumps(res)
        elif signup_obj:
            _logger.info("=====code-got-is===%s-%s",vcode,signup_obj.vcode)
            if signup_obj.vcode == int(vcode):
                signup_obj.is_done = True
                res = {'result': 'verified'}
                return json.dumps(res)
            else:
                res = {'result': 'notverified'}
                return json.dumps(res)

class CustomerPortalCity(CustomerPortal):

    MANDATORY_BILLING_FIELDS = ["name", "email", "street", "city", "country_id"]
    OPTIONAL_BILLING_FIELDS = ["zipcode","phone", "state_id", "mobile", "vat", "company_name", "city_id"]

    def _prepare_portal_layout_values(self):
        values = super(CustomerPortalCity, self)._prepare_portal_layout_values()
        # cities = request.env['res.city'].sudo().search([])
        # values['cities'] = cities
        
        return values