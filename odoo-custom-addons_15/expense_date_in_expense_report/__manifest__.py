{
    'name': 'Expense Date in Expense Report',
    'version': '1.0',
    'summary': 'This modules add Expense Date in Expense Report.',
    'description' : 'This modules add Expense Date in Expense Report.',
    'category': 'companies',
    'author': 'Nexus',
    'depends': ['hr_expense'],
    'data': ['views/view.xml'],
    'installable': True,
    'auto_install': False,

}
