from odoo import api, fields, models
import logging
_logger = logging.getLogger(__name__)

class ExpenseReportExt(models.Model):
    _inherit = "hr.expense.sheet"

    expense_date = fields.Date("Expense Date")

    @api.onchange('expense_line_ids')
    def expensedate(self): 
        _logger.info("=====onchange===expense===date")
        for rec in self:
            if rec.expense_line_ids:
                for line in rec.expense_line_ids:
                    date = line.date
                    if date:
                        rec.expense_date = date

