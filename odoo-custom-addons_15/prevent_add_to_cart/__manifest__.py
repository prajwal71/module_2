# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Add to Cart prevent',
    'version' : '1.1',
    'summary': '',
    'description': """

====================

    """,
    'author': 'Nexus',
    'depends' : ['base','web','website_event','event'],
    # 'assets': {
    #     'web.assets_frontend': [('website_event_ext/static/src/js/custom.js')]
    # },
    # 'data': [
    #     'views/view.xml',
    #          'views/template.xml'
    #     ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
