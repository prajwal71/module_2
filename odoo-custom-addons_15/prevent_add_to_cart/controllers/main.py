from odoo import http, _
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from dateutil.relativedelta import relativedelta
import base64
import logging
import json
_logger = logging.getLogger(__name__)
# import yaml
import werkzeug
from odoo.http import request

from odoo.addons.website_sale.controllers.main import WebsiteSale

class WebsiteSaleExt(WebsiteSale):
    
    @http.route(['/shop/cart/update'], type='http', auth="public", methods=['GET','POST'], website=True)
    def cart_update(self, product_id, add_qty=1, set_qty=0, **kw):
        res = super(WebsiteSaleExt,self).cart_update(product_id, add_qty=1, set_qty=0, **kw)
        return res