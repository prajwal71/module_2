odoo.define('ehcs_wysiwyg_editor.ehcs_wysiwyg', function(require) {
	"use strict";

	$(document).ready(function() {
		$('.summernote').summernote({
			toolbar: [
				['style', ['style']],
				['font', ['bold', 'italic', 'underline', 'clear']],
				['fontname', ['fontname']],
				['fontsize', ['fontsize']],
				['height', ['height']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['table', ['table']],
				['insert', ['link', 'picture', 'video', 'hr']],
				['view', ['fullscreen']],
				['font', ['strikethrough', 'superscript', 'subscript']],
				['history', ['undo', 'redo']],
				['help', ['help']],
			],
			fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Helvetica Neue', 'Helvetica', 'Impact', 'Lucida Grande', 'Tahoma', 'Times New Roman', 'Verdana'],
			height: 180,
		});
		console.log("hello i m inside");
	});

});
