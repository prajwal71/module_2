# -*- coding: utf-8 -*-
{
    "name": "EHCS WYSIWYG Editor",
    'summary': """ This module provides a WYSIWYG Editor.""",
    'author': "ERP Harbor Consulting Services",
    'website': "http://www.erpharbor.com",
    'version': '13.0.1.0.0',
    "category": "Web",
    'depends': ['website','web'],
    'bootstrap': True,
    'assets': {
      
        'web.assets_backend':
            [
            #           'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.js',
            #  'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-lite.js',
            # 'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.min.css',
            #  'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.min.js',
            #  'ehcs_wysiwyg_editor/static/src/js/**/*',
            # 'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/src/js/summernote.js',
            # 'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/config/webpack.config.dev.js',
            #  'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/config/webpack.config.production.js',
            # 'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.min.css',
            # 'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-lite.js',
            # 'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.js',
            # 'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.css',
            # 'ehcs_wysiwyg_editor/static/src/js/wysiwyg_editor.js',
            # 'ehcs_wysiwyg_editor/static/src/css/editor.css',
            # 'ehcs_wysiwyg_editor/static/src/css/**/*',
             'ehcs_wysiwyg_editor/static/src/css/editor.css',
             'ehcs_wysiwyg_editor/static/src/js/wysiwyg_html.js',
            #   'ehcs_wysiwyg_editor/static/src/js/wysiwyg_editor.js',
        #      'ehcs_wysiwyg_editor/static/src/css/editor.css',
        #   'ehcs_wysiwyg_editor/static/src/js/wysiwyg_editor.js', 
            # 'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/**/*',
            # 'ehcs_wysiwyg_editor/static/src/css/editor.css',
            # 'ehcs_wysiwyg_editor/static/src/js/wysiwyg_editor.js',
            # 'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/**/*',
       
            ],
     
        
        'web.assets_frontend': [
            'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.min.js',
            'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.min.css',
            'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-lite.js',
            'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.js',
            'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.css',
            'ehcs_wysiwyg_editor/static/src/js/wysiwyg_editor.js',
            'ehcs_wysiwyg_editor/static/src/css/editor.css',
            # 'ehcs_wysiwyg_editor/static/src/css/**/*',
            # 'ehcs_wysiwyg_editor/static/src/js/**/*',
            # 'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/**/*',
        #   'ehcs_wysiwyg_editor/static/src/css/editor.css',
        #   'ehcs_wysiwyg_editor/static/src/js/wysiwyg_editor.js',
        #   'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/**/*',
        #    'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.js',
        #    'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-lite.js',
        #    'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.min.css',
        #    'ehcs_wysiwyg_editor/static/src/editor/summernote_mst/dist/summernote-bs4.min.js',
      
           
          
        ],

    },
    "data": [

    ],
    "demo":
        [
            'demo/res_user.xml',
            'demo/website_template_demo.xml',
            
        ],
    'price': 10,
    'currency': 'USD',
}
