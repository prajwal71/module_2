

# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import api,fields,models,_
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning
 
class KnowledgeBaseTags(models.Model):
    """ Tags of Knowledge Base """
    _name = "knowledge.base.tags"
    _inherit = ["knowledge.base.node"]
    _description = "Knowledge Base Tags"
    _order = "name"

    name = fields.Char('Tag Name', required=True)
    complete_name = fields.Char('Complete Name', compute='_compute_complete_name', store=True)
    color = fields.Integer(string='Color Index')
    parent_id = fields.Many2one('knowledge.base.tags', string='Parent Tag', index=True)
    article_ids = fields.Many2many(
            "knowledge.base.article",
            "knowledge_base_tag_table",
            "knowledge_base_atricle_id",
            "knowledge_base_tag_id",
            string="Articles",
        )
    child_ids = fields.One2many(
        "knowledge.base.tags",
        "parent_id",
        string="Child Tags"
    )

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "Tag name already exists!"),
    ]
    
    def name_get(self):
        if not self.env.context.get('hierarchical_naming', True):
            return [(record.id, record.name) for record in self]
        return super(KnowledgeBaseTags, self).name_get()

    @api.depends('name', 'parent_id.complete_name')
    def _compute_complete_name(self):
        for tag in self:
            if tag.parent_id:
                tag.complete_name = '%s / %s' % (tag.parent_id.complete_name, tag.name)
            else:
                tag.complete_name = tag.name
    
    @api.constrains('parent_id')
    def _check_parent_id(self):
        if not self._check_recursion():
            raise ValidationError(_('You cannot create recursive Tags.'))



class KnowledgeBaseSection(models.Model):
    """ Sections of Knowledge Base """
    _name = "knowledge.base.section"
    _inherit = ["knowledge.base.node"]
    _description = "Knowledge Base Sections"
    _order = "name"
    _rec_name = 'complete_name'
    
    name = fields.Char('Section Name', required=True)
    complete_name = fields.Char('Complete Name', compute='_compute_complete_name', store=True)
    parent_id = fields.Many2one('knowledge.base.section', string='Parent Section', index=True)
    note = fields.Text('Note')
    color = fields.Integer('Color Index')

    child_ids = fields.One2many(
        "knowledge.base.section",
        "parent_id",
        string="Sub Sections"
    )

    def name_get(self):
        if not self.env.context.get('hierarchical_naming', True):
            return [(record.id, record.name) for record in self]
        return super(KnowledgeBaseSection, self).name_get()

    @api.depends('name', 'parent_id.complete_name')
    def _compute_complete_name(self):
        for section in self:
            if section.parent_id:
                section.complete_name = '%s / %s' % (section.parent_id.complete_name, section.name)
            else:
                section.complete_name = section.name
    
    @api.constrains('parent_id')
    def _check_parent_id(self):
        if not self._check_recursion():
            raise ValidationError(_('You cannot create recursive Sections.'))
