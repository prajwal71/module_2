# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from . import knowledge_base
from . import knowledge_base_website
from . import website
from . import res_config_settings
from . import knowledge_base_node
from . import knowledge_base_tags_secs
from . import knowledge_base_revision
