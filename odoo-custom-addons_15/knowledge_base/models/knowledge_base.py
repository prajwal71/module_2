# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import api,fields,models,_
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

import logging
from odoo.tools import mail

_logger = logging.getLogger(__name__)
REVISIONCHANGES = {"name", "description", "section_id","short_description", "tag_ids", "attachment_ids"}
SHORTSYMBOLS = 800

class KnowledgeBaseArticle(models.Model):
    _name = "knowledge.base.article"
    _description = "Knowledge Base Articles"
    _inherit = ['mail.thread.cc', 'mail.activity.mixin']
    _order = "sequence, id desc"

    # def _compute_short_desc(self):
    #     for article in self:
    #         if article.description:
    #             article.short_description = article.description[:100]
    @api.depends('like_ids')
    def _compute_likes(self):
        like=0
        for rec in self:
            if rec.like_ids:
                like = len(rec.like_ids)
            rec.likes = like

    @api.depends('dislike_ids')
    def _compute_dislikes(self):
        dislikes = 0
        for rec in self:
            if rec.dislike_ids:
                dislikes = len(rec.dislike_ids)
            rec.dislikes = dislikes

    @api.depends('view_ids')
    def _compute_views(self):
        views = 0
        for rec in self:
            if rec.view_ids:
                views = sum(rec.view_ids.mapped('counter'))
            rec.views = views

    @api.depends('favourite_ids')
    def _compute_favourites(self):
        favourites = 0 
        for rec in self:
            if rec.favourite_ids:
                favourites = len(rec.favourite_ids)
            rec.favourites = favourites

    @api.depends('countributer_ids')
    def _compute_contributions(self):
        contributions = 0
        for rec in self:
            if rec.countributer_ids:
                contributions = len(rec.countributer_ids)
            rec.contributions = contributions

    @api.depends('has_user')
    def _has_user_manager(self):
        res_user = self.env['res.users'].search([('id', '=', self.env.uid)])
        if res_user.has_group('knowledge_base.group_knowledge_base_user') and not res_user.has_group('knowledge_base.group_knowledge_base_manager'):
            self.has_user = True
        else:
            self.has_user = False
    
    @api.depends("revision_ids")
    def _compute_contributor_ids(self):
        """
        Compute method for contributor_ids
        """
        for article in self:
            if article.revision_ids:
                revision_ids = article.revision_ids
                article.write_date = revision_ids[0].change_datetime
                article.write_user_id = revision_ids[0].user_id
                contributors = revision_ids.mapped("user_id.id")
                article.countributer_ids = [(6, 0, contributors)]

    def _inverse_attachment_ids(self):
        """
        Inverse method for attachment_ids to make them available for public and portal

        Methods:
         * generate_access_token - of ir.attachment
        """
        for article in self:
            no_token_attachments = article.attachment_ids
            no_token_attachments.write({"res_id": article.id})
            no_token_attachments.generate_access_token()
    
    def _inverse_description(self):
        """
        The inverse method for description to prepared indexed contents

        Methods:
         * html2plaintext
        """
        for article in self:
            indexed_description = mail.html2plaintext(article.description)
            indexed_description = "\n".join([s for s in indexed_description.splitlines() if s])
            article.indexed_description = indexed_description
            #if not article.kanban_manual_description:
             #   article.kanban_description = len(indexed_description) >= SHORTSYMBOLS \
              #                               and indexed_description[0:SHORTSYMBOLS] \
               #                              or indexed_description


    active = fields.Boolean(default=True)
    name = fields.Char(string='Title', track_visibility='always', required=True, index=True)
    description = fields.Html(string='Description',track_visibility='always', translate=False,
        sanitize=False,inverse=_inverse_description)
    short_description = fields.Text(string='Short Description',track_visibility='always',required=True)
    sequence = fields.Integer(string='Sequence', index=True, default=10,
        help="Gives the sequence order when displaying a list of Articles.")
    
    # tag_ids = fields.Many2many('knowledge.base.tags', string='Tags',track_visibility='always')
    tag_ids = fields.Many2many(
        "knowledge.base.tags",
        "knowledge_base_article_table",
        "knowledge_base_tag_id",
        "knowledge_base_atricle_id",
        string="Tags",
        track_visibility='always',
        copy=True,
    )
    section_id = fields.Many2one('knowledge.base.section', string='Section',track_visibility='always',index=True,required=True)

    create_date = fields.Datetime("Created On", readonly=True, index=True)
    write_date = fields.Datetime("Last Updated On", readonly=True, index=True)
    
    
    user_id = fields.Many2one('res.users',
        string='Author',
        default=lambda self: self.env.uid,
        index=True, tracking=True)
    
    write_user_id = fields.Many2one('res.users',
        string='Last Updated By',
        default=lambda self: self.env.uid,
        index=True, tracking=True)
    
    like_ids = fields.Many2many('res.users', 'article_like_user_rel', 'like_article_id', 'like_user_id',index=True, string="Liked By",
        help="Users who like the article.")
    
    dislike_ids = fields.Many2many('res.users', 'article_dislike_user_rel', 'dislike_article_id', 'dislike_user_id',index=True, string="Disiked By",
        help="Users who dislike the article.")
    
    view_ids = fields.Many2many('knowledge.base.views', 'article_view_user_rel', 'view_article_id', 'view_user_id',index=True,string="Read By",
        help="Users who view the article.")
    favourite_ids = fields.Many2many('res.users', 'article_favourite_user_rel', 'favourite_article_id', 'favourite_user_id',index=True,string="Favourite By",
        help="Users who favourited the article.")
    countributer_ids = fields.Many2many('res.users', 'article_contributor_user_rel', 'contributor_article_id', 'contributor_user_id',index=True,string="Contributions By",
        help="Users who favourited the article.")
    allowed_user_ids = fields.Many2many('res.users', 'article_allowrd_user_rel', 'allowed_article_id', 'allowed_user_id',index=True,string="Allowed Users",
        help="By default all users are allowed.")

    likes = fields.Integer(string='Likes', index=True, compute='_compute_likes')
    dislikes = fields.Integer(string='Dislikes', index=True, compute='_compute_dislikes')
    views = fields.Integer(string='Reads', index=True, compute='_compute_views')
    favourites = fields.Integer(string='Favourites', index=True, compute='_compute_favourites')
    contributions = fields.Integer(string='Contributors', index=True, compute='_compute_contributions')
    has_user = fields.Boolean(string="Has User", compute='_has_user_manager')

    attachment_ids = fields.Many2many(
        'ir.attachment',
        'knowledge_base_article_ir_attachment_rel',
        'knowledge_base_article_id',
        'attachment_id',
        string='Attachments',
        copy=True,
        inverse=_inverse_attachment_ids,
    )
    revision_ids = fields.One2many(
        "knowledge.base.revision",
        "article_id",
        string="Revisions",
    )

    def inc_likes(self):
        for rec in self:
            if self.env.uid not in self.like_ids.ids:
                rec.write({'like_ids': [(4, self.env.uid)]})

    def inc_dislikes(self):
        for rec in self:
            if self.env.uid not in self.dislike_ids.ids:
                rec.write({'dislike_ids': [(4, self.env.uid)]})

    def inc_favourites(self):
        for rec in self:
            if self.env.uid not in self.favourite_ids.ids:
                rec.write({'favourite_ids': [(4, self.env.uid)]})
    
    def action_view_article(self):
        for rec in self:
            view_user_obj = self.env['knowledge.base.views'].search([('user_id','=',self.env.uid)],limit=1)
            if view_user_obj:
                view_user_obj.counter += 1
                if view_user_obj.id not in self.view_ids.ids:
                    rec.write({'view_ids': [(4, view_user_obj.id)]})
            elif not view_user_obj:
                view_obj = view_user_obj.create({
                    'user_id': self.env.uid,
                    'counter': 1
                })
                if view_obj.id not in self.view_ids.ids:
                    rec.write({'view_ids': [(4, view_obj.id)]})
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url_article = base_url + '/knowledgebase/' + str(self.id)
        return {
                'name': _("Knowledgebase Article"),
                'type': 'ir.actions.act_url',
                'url': url_article,
                'target': 'new',
            }
    @api.model
    def create(self, values):
        """
        Re-write to:
         * Save this article version in revisions

        Methods:
         * _prepare_revisions
        """
        res = super(KnowledgeBaseArticle, self).create(values)   
        res._prepare_revisions()
        return res
    
    def write(self, values):
        """
        Re-write to:
         * Save this article version in revisions and notify of those
        1. we need this, since description is always shown as val to write

        Methods:
         * _prepare_revisions
         * _notify_of_revisions
        """
        changed_fields = set(values.keys())
        _logger.info("----------------------changed_field%s",changed_fields)
        need_revision = REVISIONCHANGES & changed_fields
        if need_revision and "description" in need_revision:
            # 1
            for article in self:
                # k =
                # newStr = k.replace('&nbsp;','')
                if  values.get("description") != article.description:
                    _logger.info("----------------------herrrrrre%s",values.get("description"))
                    _logger.info("----------------------herrrrrre%s",article.description)
                    break
            else:
                need_revision.remove("description")
                _logger.info("----------------------here%s",values.get("description"))
                _logger.info("----------------------here%s",article.description)
        res = super(KnowledgeBaseArticle, self).write(values)
        _logger.info("----------------------need_rev%s",need_revision)
        if need_revision:
            # 2
            self._prepare_revisions()
            # self._notify_of_revisions()
        return res
    
    def _prepare_revisions(self):
        """
        The method to save this version of the article before its revisions are saved
        """
        for article in self:
            values = article._prepare_revision_dict()
            revision_id = self.env["knowledge.base.revision"].create(values)

    
            

    def _prepare_revision_dict(self):
        """
        The method to prepare this article revision dict
        """
        article = self
        return {
            "article_id": article.id,
            "name": article.name,
            "description": article.description,
            "short_description": article.short_description,
            "section_id": article.section_id.id,
            "tag_ids": [(6, 0, article.tag_ids.ids)],
            "attachment_ids": [(6, 0, article.attachment_ids.ids)],
        }
    
    def get_revisions(self):
        """
        The method to return js dictionary of revisions

        Methods:
         * _prepare_revision_dict of knowsystem.article.revision

        Return:
         * the list of dict
        """
        js_dict = []
        for revision in self.revision_ids:
            revision_vals = revision._prepare_revision_dict()
            if revision_vals:
                js_dict.append(revision_vals)
        return js_dict
    
    @api.depends('revision_ids')
    def _compute_revisions(self):
        for article in self:
            # rev_obj = self.env['knowledge.base.revision'].search([('article_id','=',self.id)])
            article.revision_count = len(article.revision_ids.ids)


    def action_view_revisions(self):
        action = self.env.ref('knowledge_base.action_knowledge_base_revision_all')
        result = action.read()[0]
        result['domain'] = "[('article_id', '=', " + str(self.id) + ")]"
        return result
    
    revision_count = fields.Integer(compute="_compute_revisions", string='Revisions Count', copy=False, default=0, store=True)
    indexed_description = fields.Text(
        string="Indexed Article",
        translate=False,
        
    )
class KnowledgeBaseViews(models.Model):
    """ Views of Knowledge Base """
    _name = "knowledge.base.views"
    _description = "Knowledge Base Views"
    _rec_name = "user_id"

    user_id = fields.Many2one(
    	"res.users", 
    	"User",
    )
    counter = fields.Integer(string="Number",default=0)

    def name_get(self):
        """
        Overloading the method to construct name as user + number
        """
        result = []
        for stat in self:
            name = u"{} ({})".format(
                stat.user_id.name,
                stat.counter,
            )
            result.append((stat.id, name))
        return result    
