
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

# todo:
# - permission (access restrict)
# - view website (enhancements)
# - Access Rules
# - link sharing options
{
    "name": "Knowledge Base Management",
    "summary": "Knowledge Base Management",
    "category": "Human Resources",
    "author": "Sagar Jayswal",
    "website": "https://github.com/dsk2370/knowledgebase",
    "license": "AGPL-3",
    "version": "13.0.1.0.0",
    "depends": ["base","web","mail","website"],
    'assets': {
        'web_editor.assets_frontend': [
                                    
            ('knowledge_base/static/css/styleer.css'),('knowledge_base/static/js/sections.js')],

    },
    "data": [    
    'security/security.xml',
    # 'data/data.xml',
    'security/ir.model.access.csv',
    'views/knowledge_base.xml',
    'views/knowledge_base_config.xml',
    'views/website_templates.xml',
    'reports/report_knowledge_base.xml',
    'reports/report_template.xml',
    'views/knowledge_base_revisions.xml',
   'views/res_config_settings.xml'

],
    "installable": True,
}
