# -*- coding: utf-8 -*-

import json
import logging
import re

from collections import OrderedDict
from werkzeug.urls import url_encode
from odoo import _, http, SUPERUSER_ID
from odoo.http import request
from odoo.addons.portal.controllers.portal import get_records_pager, CustomerPortal, pager as portal_pager
from odoo.tools import consteq
from odoo.tools.safe_eval import safe_eval

_logger = logging.getLogger(__name__)

class CustomerPortalKnowledgeBase(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(CustomerPortalKnowledgeBase, self)._prepare_portal_layout_values()
        show_portal = request.website.knowledge_base_website_portal
        values.update({"show_portal": show_portal,})
        return values


    # @http.route(['/knowledgebase/<model("knowledge.base.article"):article_id>',], type='http', auth="public", website=True)
    # def website_knowsledge_base_article(self, article_id, **kw):
    #     """
    #     The route to open the article page

    #     """

    #     website_id = request.website
    #     params_str = json.dumps(request.params)
    #     # res = True
    #     # if kw.get("access_token"):
    #     #     # for the case of direct sharing
    #     #     if consteq(article_id.sudo().access_token, kw.get("access_token")):
    #     #         # SUPERUSER_ID is required for check rights because of has_group
    #     #         article_id = article_id.sudo().with_user(SUPERUSER_ID)
    #     #         res = False
    #     # if res:
    #         # res = self._check_rights(redirect_route=redirect_route, redirect_params=params_str, article_id=article_id)
    #     # if not res:
    #     # article_id.with_context(no_sudo_required="True").name_get()
    #     # ICPSudo = request.env['ir.config_parameter'].sudo()
    
    #     if article_id:
    #         values = {
    #             "article_id": article_id,
    #             # "main_object": article_id,
    #             # "page_name": "{}".format(article_id.name),
    #             # "articles_portal": True,
    #             # "edit_website_possible": website_editor,
    #             # "print_portal": print_portal,
    #             # "portal_likes": portal_likes,
    #             # "social_share": social_share,
    #         }
    #         # history = request.session.get('all_articles', [])
    #         # values.update(get_records_pager(history, article_id))
    #         res = request.render("knowledge_base.knowledge_base_article", values)
    #         # article_id.update_number_of_views()
    #     else:
    #         res = request.render("http_routing.404")
    #     return res

    def _return_search_in_articles(self, search_in, search):
        """
        Returns:
         * list - domain to search
        """
        search_domain = []
        if search_in in ('indexed_description'):
            search_domain =  [
                                '|',
                                    ('name', 'ilike', search),
                                    ('indexed_description', 'ilike', search),
                             ]
        if search_in in ('name'):
            search_domain = [('name', 'ilike', search)]
        if search_in in ('section_id'):
            search_domain = [('section_id.name', 'ilike', search)]

        website_id = request.website
        custom_search_ids =  website_id.knowledge_base_custom_search_ids
        for csearch in custom_search_ids:
            field_name = csearch.sudo().custom_field_id.name
            if search_in in (field_name,):
                search_domain = [(field_name, 'ilike', search)]            
        return search_domain

    def _return_searchbar_sortings_articles(self, values):
        """
        Returns:
         * dict
            ** search_by_sortings - {}
            ** searchbar_inputs - {}
        """
        searchbar_sortings = {
            'views': {'label': _('Trending'), 'order': 'views desc, id desc'},
            'name': {'label': _('Title'), 'order': 'name asc, id desc'},
            'section': {'label': _('Section'), 'order': 'section_id asc, name asc, id desc'},
        }
        if request.website.knowledge_base_portal_likes:
            searchbar_sortings.update({
                'likes': {'label': _('Likes'), 'order': 'likes_score desc, id desc'},
            })

        website_id = request.website
        searchbar_inputs = {
            'content': {'input': 'indexed_description', 'label': _('Search in content')},
            'name': {'input': 'name', 'label': _('Search in titles only')},
            'section': {'input': 'section_id', 'label': _('Search by section')},
        }
        custom_search_ids = website_id.sudo().knowledge_base_custom_search_ids
        for csearch in custom_search_ids:
            try:
                searchbar_inputs.update({
                    "{}".format(csearch.sudo().custom_field_id.name): {
                        'input': csearch.sudo().custom_field_id.name, 
                        'label': csearch.name,
                    }
                })
            except:
                # for the case when field was removed
                continue
        return {
            "searchbar_sortings": searchbar_sortings,
            "searchbar_inputs": searchbar_inputs,
        }

    def _prepare_articles_helper(self, page=1, sections=None, tags=None, sortby=None, 
                                 search=None, search_in='content', domain=[], url="/knowledgebase", **kw):
        """
        The helper method for apps list
        """
        values = {}
        article_object = request.env['knowledge.base.article']
        section_object = request.env['knowledge.base.section']
        website_id = request.website
        domain += [("website_id", "in", [False, website_id.id]),]

        if not sortby:
            sortby = 'views'
        
        searches_res = self._return_searchbar_sortings_articles(values)
        searchbar_sortings = searches_res.get("searchbar_sortings")
        searchbar_inputs = searches_res.get("searchbar_inputs")
        sort_order = searchbar_sortings[sortby]['order']

        if search and search_in:
            search_domain = self._return_search_in_articles(search_in, search)
            domain += search_domain
        # count for pager
        # domain_real = [("website_pinned", "=", False)] + domain 
        articles_count_count = article_object.search_count(domain)
        # make pager
        pager = portal_pager(
            url=url,
            url_args={
                'sortby': sortby,
                'search': search,
                'search_in': search_in,
                'sections': sections,
                'tags': tags,
            },
            total=articles_count_count,
            page=page,
            step=website_id.pager_knowledge_base,
        )
        article_ids = article_object.search(
            domain,
            order=sort_order,
            limit=website_id.pager_knowledge_base,
            offset=pager['offset']
        )
        all_article_ids = article_object.search([])
        all_section_ids = section_object.search([])
        show_tooltip = website_id.knowledge_base_portal_tooltip
        section_ids = all_section_ids
        
        section_ids = str(section_ids.return_nodes_with_restriction(show_tooltip))
        tag_ids = request.env["knowledge.base.tags"].search([
        ])
        tag_ids = str(tag_ids.return_nodes_with_restriction(show_tooltip))

        values.update({
            "article_ids": article_ids,
            'section_ids': section_ids,
            'tag_ids': tag_ids,
            'pager': pager,
            'searchbar_sortings': searchbar_sortings,
            'searchbar_inputs': searchbar_inputs,
            'search_in': search_in,
            'sortby': sortby,
        })
        _logger.info("=============section3====%s",section_ids)
        return values

    def _prepare_vals_articles(self, page=1, sections=None, tags=None, sortby=None,  
                               search=None, search_in='content', **kw):
        """
        The method to prepare values for articles
        """
        domain = []
        url="/knowledgebase"
        if sections:
            _logger.info("===========sections2===%s",sections)
            sections_list = sections.split(",")
            sections_int_list = [int(item) for item in sections_list]
            domain += [("section_id", "in", sections_int_list)]
        if tags:
            tags_list = tags.split(",")
            tags_int_list = [int(item) for item in tags_list]
            tags_number = len(tags_int_list) - 1
            itera = 0
            while itera != tags_number:
                domain += ["|"]
                itera += 1
            for tag_u in tags_int_list:
                domain += [("tag_ids", "=", tag_u)]
                    
        values = self._prepare_articles_helper(page=page, sections=sections, tags=tags, sortby=sortby, 
                                                search=search, search_in=search_in, domain=domain, 
                                               url=url, **kw)
        values.update({
            'page_name': _('Knowledge Base'),
            'default_url': '/knowledgebase',
            'sections': sections,
            'tags': tags,
        })
        request.session['all_articles'] = values.get("article_ids").ids[:100]
        return values

    def _check_rights(self, redirect_route="/knowledgebase", redirect_params="{}", article_id=None):
        """
        The method to check whether this user is allowed to observe based on configured options
        1. If portal is turned but not website, and user is not logged in, we redirect to login
        2. If portal and website are turned on, but user is not logged in and doesn't have rights, also redirect to
           login

        Args:
         * redirect_route - in case of redirection after login
         * redirect_params - str representing dumped OrderDict
         * article_id - knowledge.base.article object or none

        Returns:
         * False if no restrictions
         * Redirection otherwise
        """
        website_id = request.website
        show_portal = website_id.knowledge_base_website_portal
        show_website = website_id.knowledge_base_website_public
        res = False
        if not show_portal and not request.env.user.has_group('base.group_user'):
            res = request.render("http_routing.403")
        if show_portal and not request.env.user.has_group('base.group_portal') \
                       and not request.env.user.has_group('base.group_user'):
            redirect_required = False
            # 1
            if not show_website:
                redirect_required = True
            # 2
            elif article_id:
                try:
                    article_id.with_context(no_sudo_required="True").name_get()
                except Exception as error:
                    redirect_required = True
            if redirect_required:
                redirect_path = "/web/login?&redirect={}<knowledgebase_redirect>{}".format(
                    redirect_route, redirect_params,
                )
                res = request.redirect(redirect_path)
        return res
    
    @http.route(['/knowledgebase', '/knowledgebase/page/<int:page>',], type='http', auth="public", website=True)
    def website_knowledgebase(self, page=1, sections=None, tags=None, sortby=None, search=None,
                           search_in='indexed_description', **kw):
        """
        The route to open the knowledgebase website page
        """
        page_str = page != 1 and "/page/{}".format(page) or ""
        redirect_route = u"/knowledgebase{}".format(page_str)
        params_str = json.dumps(request.params)
        _logger.info("===========sections1===%s",sections)
        res = self._check_rights(redirect_route=redirect_route, redirect_params=params_str)
        if not res:
            website_id = request.website
            values = self._prepare_vals_articles(page=page, sections=sections, tags=tags, sortby=sortby, 
                                                search=search, search_in=search_in, **kw)
            portal_likes = website_id.knowledge_base_portal_likes
            values.update({
                "articles_portal": True,
                "portal_likes": portal_likes,
            })
            if search:
                values.update({"done_search": search})
            res = request.render("knowledge_base.knowledgebase", values)
        return res


    @http.route(['/knowledgebase/<model("knowledge.base.article"):article_id>',], type='http', auth="public", website=True)
    def website_knowsledge_base_article(self, article_id=None, **kw):
        """
        The route to open the article page
        Methods:
         * _check_rights
         * update_number_of_views of knowledgebase.article
         * _prepare_portal_layout_values
        """
        redirect_route = "/knowledgebase/{}".format(article_id.sudo().id)
        website_id = request.website
        params_str = json.dumps(request.params)
        res = True
        if kw.get("access_token"):
            # for the case of direct sharing
            if consteq(article_id.sudo().access_token, kw.get("access_token")):
                # SUPERUSER_ID is required for check rights because of has_group
                article_id = article_id.sudo().with_user(SUPERUSER_ID)
                res = False
        if res:
            res = self._check_rights(redirect_route=redirect_route, redirect_params=params_str, article_id=article_id)
        if not res:
            article_id.with_context(no_sudo_required="True").name_get()
            ICPSudo = request.env['ir.config_parameter'].sudo()
            # website_editor = safe_eval(ICPSudo.get_param('knowsystem_website_editor', default='False'))
            print_portal = website_id.knowledge_base_portal_print
            portal_likes = website_id.knowledge_base_portal_likes
            social_share = website_id.knowledge_base_portal_social_share
            if article_id:
                values = {
                    "article_id": article_id,
                    "main_object": article_id,
                    "page_name": "{}".format(article_id.name),
                    "articles_portal": True,
                    # "edit_website_possible": website_editor,
                    "print_portal": print_portal,
                    "portal_likes": portal_likes,
                    "social_share": social_share,
                }
                history = request.session.get('all_articles', [])
                values.update(get_records_pager(history, article_id))
                res = request.render("knowledge_base.knowledge_base_article", values)
                # article_id.update_number_of_views()
            else:
                res = request.render("http_routing.404")
        return res

    @http.route(['/knowledgebase/<model("knowledge.base.article"):article_id>/download/<aname>',], type='http', auth="public",
                website=True)
    def website_knowledge_base_article_print(self, article_id=None, aname=None, **kw):
        """
        The route to make and download printing version of the article

        Methods:
         * _check_rights
         * render_qweb_pdf of report
         * make_response of odoo.request
        """
        res = self._check_rights()
        if not res:
            if article_id:
                lang = request.env.user.lang
                report_id = request.env.ref('knowledge_base.action_report_knowledge_base_article')
                pdf_content, mimetype = report_id.sudo().with_context(lang=lang).render_qweb_pdf(
                    res_ids=article_id.id,
                )
                pdfhttpheaders = [
                    ('Content-Type', 'application/pdf'),
                    ('Content-Length', len(pdf_content)),
                ]
                res = request.make_response(pdf_content, headers=pdfhttpheaders)
            else:
                res = request.render("http_routing.404")
        return res

    @http.route(['/knowledgebase/like/<model("knowledge.base.article"):article_id>'], type='http', auth="user", website=True)
    def like_article(self, article_id, **kw):
        """
        The route like the article

        Methods:
         * inc_likes() of knowledgebase.article
        """
        res = self._check_rights()
        if not res:
            article_id.inc_likes()
            done = u"/knowledgebase/{}?{}".format(article_id.id, url_encode(kw))
            res = request.redirect(done)
        return res

    @http.route(['/knowledgebase/dislike/<model("knowledge.base.article"):article_id>'], type='http', auth="user",
                website=True)
    def dislike_article(self, article_id, **kw):
        """
        The route like the article

        Methods:
         * inc_dislikes() of knowledgebase.article
        """
        res = self._check_rights()
        if not res:
            article_id.inc_dislikes()
            done = u"/knowledgebase/{}?{}".format(article_id.id, url_encode(kw))
            res = request.redirect(done)
        return res
