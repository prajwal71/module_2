# -*- coding: utf-8 -*-
# Copyright (C) Kanak Infosystems LLP.

{
    'name': 'POS Mirror',
    'version': '1.0',
    'summary': 'Customer Pole display function is achieved in this module along with an option to show adverts',
    'description': """
POS Mirror
================================
    """,
    'license': 'OPL-1',
    'author': 'Kanak Infosystems LLP.',
    'category': 'Point of Sale',
    'images': ['static/description/banner.jpg'],
    'depends': ['point_of_sale'],
    'data': [
        'data/data.xml',
        'security/ir.model.access.csv',
        'views/pos_assets.xml',
        'views/pos_mirror_view.xml',
        'views/pos_mirror_template.xml',
    ],
    'qweb': ['static/src/xml/pos.xml'],
    'installable': True,
    'price': 20,
    'currency': 'EUR',
}
