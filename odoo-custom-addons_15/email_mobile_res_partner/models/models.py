# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
import logging
_logger = logging.getLogger(__name__)
from odoo.exceptions import AccessError, UserError


class ResPartnerExt(models.Model):
    _inherit = 'res.partner'

    @api.model
    def create(self,vals):
        _logger.info("====%s      %s   %s===================contact===-%s",vals.get('login'),vals.get('mobile'),vals.get('email'),vals.get('phone'))
        if not vals.get('mobile') and not vals.get('email') and not vals.get('phone') and not vals.get('login'):
            raise UserError(_("You must Enter the value of Either Email or Phone/Mobile Number in Contact"))
        res=super(ResPartnerExt,self).create(vals)
        return res

    def write(self,vals):
        res=super(ResPartnerExt,self).write(vals)
        for rec in self:
            if not rec.mobile and not rec.email and not rec.phone:
                raise UserError(_("You must Enter the value of Either Email or Phone/Mobile Number in Contact"))

        
        return res

    @api.onchange('country_id')
    def CountryCode(self):
        for rec in self:
            if rec.country_id:
                if rec.country_id.id == 167:
                    if rec.mobile:
                        rec.mobile = '+977' + rec.mobile
                    else:
                        rec.mobile ='+977'
                else:
                    if rec.mobile:
                        if '+977' in rec.mobile:
                            s = str(rec.mobile)
                            rec.mobile = s.replace('+977','')
                    else:
                        rec.mobile = False

