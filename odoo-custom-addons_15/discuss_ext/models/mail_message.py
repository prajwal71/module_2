import logging
import re
from collections import defaultdict

from binascii import Error as binascii_error

from odoo import _, api, Command, fields, models, modules, tools
from odoo.exceptions import AccessError
from odoo.osv import expression
from odoo.tools.misc import clean_context

_logger = logging.getLogger(__name__)

class MessageCustom(models.Model):
    _inherit = 'mail.message'

    is_channel_type = fields.Boolean(
        string="Is channel TYpe", default=False,
  
    )
    channel_id = fields.Many2one('mail.channel',string="Channel ID")

    @api.model 
    def create(self,vals):
        _logger.info("===================%s======",vals)
        model = vals.get('model')
        related_id = vals.get('res_id')
        if model and related_id:
            if model == 'mail.channel':
               channel_name = self.env['mail.channel'].search([('id','=',related_id)])
               if channel_name:
                   is_channel = channel_name.channel_type
                   if is_channel:
                       if is_channel == 'channel':
                            _logger.info("===============mail======")
                            test_channel = vals.get('channel_id')
                            if test_channel:
                                _logger.info("=====111==========mail======")
                                pass
                            else:
                                _logger.info("===222============mail======")
                                vals['is_channel_type'] = True
                                vals['channel_id'] = channel_name.id

                           

        res = super(MessageCustom,self).create(vals)
        return res
  

    def set_channel_type(self):
        for rec in self:
            model = rec.model
            related_id = rec.res_id
            if model and related_id:
                if model == 'mail.channel':
                    channel_name = self.env['mail.channel'].search([('id','=',related_id)])
                    if channel_name:
                        is_channel = channel_name.channel_type
                        if is_channel:
                            if is_channel == 'channel':
                                rec.is_channel_type = True
                                rec.channel_id = channel_name
