# -*- coding: utf-8 -*-
{
    'name': "CRM Customization",

    'summary': """
        """,

    'description': """
    """,

    'author': "Subheta Tech",
    'website': "http://shubetatech.com/",
    'category': 'Customizations',
    'version': '15.0.0.1',
    'depends': ['crm','hr'],
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'wizards/wizard.xml',
    ],
}
