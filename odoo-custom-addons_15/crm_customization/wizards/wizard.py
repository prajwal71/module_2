# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class Counselling(models.TransientModel):
    _name = 'counselling.wizard'
    
    asignee_id = fields.Many2one('hr.employee', 'Asignee')
    
    def action_send(self):
        if self.env.context.get('active_model') and self.env.context.get('active_id'):
            record_id = self.env[self.env.context.get('active_model')].browse(
                self.env.context.get('active_id'))
            if record_id:
                if self.asignee_id.user_id:
                    create_vals = {
                        'activity_type_id': '4',
                        'summary': 'Scheduled for Counselling',
                        'automated': True,
                        'date_deadline': fields.Date.today(),
                        'res_model_id': self.env['ir.model'].search([('model', '=', 'crm.lead')]).id,
                        'res_id': record_id.id,
                        'user_id': self.asignee_id.user_id.id
                    }
                    self.env['mail.activity'].create(create_vals)
                    record_id.write({'stage_id': 2, 'assignee_id': self.asignee_id.user_id.id})
                
class Documentation(models.TransientModel):
    _name = 'documentation.wizard'
    
    asignee_id = fields.Many2one('hr.employee', 'Asignee')
    
    def action_send(self):
        if self.env.context.get('active_model') and self.env.context.get('active_id'):
            record_id = self.env[self.env.context.get('active_model')].browse(
                self.env.context.get('active_id'))
            if record_id:
                if self.asignee_id.user_id:
                    create_vals = {
                        'activity_type_id': '4',
                        'summary': 'Scheduled for Documentation',
                        'automated': True,
                        'date_deadline': fields.Date.today(),
                        'res_model_id': self.env['ir.model'].search([('model', '=', 'crm.lead')]).id,
                        'res_id': record_id.id,
                        'user_id': self.asignee_id.user_id.id
                    }
                    self.env['mail.activity'].create(create_vals)
                record_id.write({'stage_id': 3, 'assignee_id': self.asignee_id.user_id.id})
                
class NotConverted(models.TransientModel):
    _name = 'not.converted.wizard'
    
    reason = fields.Text('Reason')
    
    def action_send(self):
        if self.env.context.get('active_model') and self.env.context.get('active_id'):
            record_id = self.env[self.env.context.get('active_model')].browse(
                self.env.context.get('active_id'))
            if record_id:
                if self.reason:
                    record_id.message_post(body=_("%s", self.reason))
                record_id.write({'stage_id': 5})
    