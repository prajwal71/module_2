# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class CRMEXT(models.Model):
    _inherit = 'crm.lead'
    
    @api.depends('partner_id')
    def _compute_name(self):
        for lead in self:
            if lead.partner_id and lead.partner_id.name:
                lead.name = _("%s") % lead.partner_id.name

    # Reception Stage Fields
    
    assignee_id = fields.Many2one('res.users', 'Asignee')
    service_type = fields.Selection([
        ('abroad', 'Study Abroad'),
        ('nepal', 'Study in Nepal'),
    ], track_visibility='onchange')
    abroad_course_type = fields.Selection([
        ('ielts', 'IELTS'),
        ('pte', 'PTE'),
        ('gmat', 'GMAT')
    ], track_visibility='onchange')
    nepal_course_type = fields.Selection([
        ('cmat', 'CMAT'),
        ('kumat', 'KUMAT'),
        ('pumat', 'PUMAT'),
        ('loksewa', 'LOKSEWA')
    ], track_visibility='onchange')
    ielt_taken = fields.Boolean('Already taken IELTS?', default=False, track_visibility='onchange')
    pte_taken = fields.Boolean('Already taken PTE?', default=False, track_visibility='onchange')
    gmat_taken = fields.Boolean('Already taken GMAT?', default=False, track_visibility='onchange')
    ielts_score = fields.Float('IELTS score?', track_visibility='onchange')
    pte_score = fields.Float('PTE score?', track_visibility='onchange')
    gmat_score = fields.Float('GMAT score?', track_visibility='onchange')
    
    # Counselling Stage Fields
    
    offer_letter_state = fields.Selection([
        ('approved', 'Approved'),
        ('not_approve', 'Not Approved'),
        ('receive', 'Receive'),
        ('process', 'Process'),
        ('applied', 'Applied')
    ], track_visibility='onchange')
    level = fields.Selection([
        ('plustwo', '+2'),
        ('bachelor', 'Bachelor'),
        ('master', 'Masters'),
    ], track_visibility='onchange')    
    
    # SLC Attachments
    slc_character_ids = fields.Binary()
    slc_marksheet_ids = fields.Binary()
    slc_provisional_ids = fields.Binary()
    slc_other_ids = fields.Binary()
    
    # +2 Attachments
    plustwo_character_ids = fields.Binary()
    plustwo_marksheet_ids = fields.Binary()
    plustwo_provisional_ids = fields.Binary()
    plustwo_other_ids = fields.Binary()
    
    # Bachelor Attachments
    bachelor_character_ids = fields.Binary()
    bachelor_marksheet_ids = fields.Binary()
    bachelor_provisional_ids = fields.Binary()
    bachelor_other_ids = fields.Binary()
    
    # Master Attachments
    master_character_ids = fields.Binary()
    master_marksheet_ids = fields.Binary()
    master_provisional_ids = fields.Binary()
    master_other_ids = fields.Binary()
    
    # Documentation Stage Fields

    gte_documentation_state = fields.Selection([
        ('approved', 'Approved'),
        ('not_approve', 'Not Approved'),
        ('receive', 'Receive'),
        ('process', 'Process'),
        ('applied', 'Applied')
    ], track_visibility='onchange')
    annual_income = fields.Boolean(default=False, track_visibility='onchange')
    property_valuation = fields.Boolean(default=False, track_visibility='onchange')
    cash_balance = fields.Boolean(default=False, track_visibility='onchange')
    tax_clearance = fields.Boolean(default=False, track_visibility='onchange')
    address_verification = fields.Boolean(default=False, track_visibility='onchange')
    
    college_fee_payment_state = fields.Selection([
        ('processing', 'Processing'),
        ('waiting', 'Waiting'),
        ('not_approve', 'Not Approved'),
        ('approved', 'Approved')
    ], track_visibility='onchange')
    coe_state = fields.Selection([
        ('processing', 'Processing'),
        ('waiting', 'Waiting'),
        ('not_approve', 'Not Approved'),
        ('approved', 'Approved')
    ], track_visibility='onchange')
    visa_lodge_state = fields.Selection([
        ('processing', 'Processing'),
        ('waiting', 'Waiting'),
        ('not_approve', 'Not Approved'),
        ('approved', 'Approved')
    ], track_visibility='onchange')
    medical_state = fields.Selection([
        ('processing', 'Processing'),
        ('waiting', 'Waiting'),
        ('not_approve', 'Not Approved'),
        ('approved', 'Approved')
    ], track_visibility='onchange')
    biometric_state = fields.Selection([
        ('processing', 'Processing'),
        ('waiting', 'Waiting'),
        ('not_approve', 'Not Approved'),
        ('approved', 'Approved')
    ], track_visibility='onchange')
    visa_waiting_state = fields.Selection([
        ('granted', 'Granted'),
        ('notgranted', 'Not Granted'),
    ], track_visibility='onchange')
    
    def action_confirm(self):
        self.activity_feedback(['mail.mail_activity_data_todo'])
        self.write({'stage_id': 4})
    
    
    def send_counselling(self):
        return {
            'name': _('Send For Counselling'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': self.env.ref('crm_customization.wizard_counselling_form_view').id,
            'res_model': 'counselling.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
        } 
        
    def send_documentation(self):
        self.activity_feedback(['mail.mail_activity_data_todo'])
        return {
            'name': _('Send For Documentation'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': self.env.ref('crm_customization.wizard_documentation_form_view').id,
            'res_model': 'documentation.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
        } 
        
    def not_convert(self):
        self.activity_feedback(['mail.mail_activity_data_todo'])
        return {
            'name': _('Not Converted'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': self.env.ref('crm_customization.wizard_not_convert_form_view').id,
            'res_model': 'not.converted.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
        } 