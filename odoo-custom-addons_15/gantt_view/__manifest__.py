# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Gantt View',
    'category': 'Hidden',
    'description': """
Odoo Web Gantt chart view.
=============================

    """,
    'version': '2.0',
    'depends': ['web'],
    'assets': {
        'web.assets_qweb': [
            'gantt_view/static/src/xml/**/*',
        ],
        'web.assets_backend': [
            'gantt_view/static/src/**/*',
        ],
        'web.qunit_suite_tests': [
            'gantt_view/static/tests/**/*',
        ],
    },
    'auto_install': True,
    'license': 'LGPL-3',
}
