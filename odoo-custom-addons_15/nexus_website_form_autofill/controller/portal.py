# ©  2008-2021 Deltatech
#              Dorin Hongu <dhongu(@)gmail(.)com
# See README.rst file on addons root folder for license details


from odoo import fields as odoo_fields, http, tools, _, SUPERUSER_ID
from odoo.http import request
from odoo.http import content_disposition, Controller, request, route
import logging
_logger = logging.getLogger(__name__)

from odoo.addons.portal.controllers.portal import CustomerPortal


class CustomerPortalCity(CustomerPortal):
    MANDATORY_BILLING_FIELDS = ["name", "email", "mobile","country_id","address_state","address_district","address_local","street_name"]
    OPTIONAL_BILLING_FIELDS = ["ward_no"]

    def _prepare_portal_layout_values(self):
        values = super(CustomerPortalCity, self)._prepare_portal_layout_values()
        # cities = request.env['res.city'].sudo().search([])
        # values['cities'] = cities
       
        values["statess"] = request.env['res.nepal.state'].sudo().search([])
        values["districts"] = request.env['res.address.district'].sudo().search([])
        values["localss"] = request.env['res.address.local'].sudo().search([])
        # values["address_state"] = request.env['res.nepal.state'].sudo().search([])
        _logger.info("==========%s=============new======fun",values)
        return values

    # @http.route(
    #     ['/shop/state_infos/<model("res.country.state"):state>'],
    #     type="json",
    #     auth="public",
    #     methods=["POST"],
    #     website=True,
    # )
    # def country_infos(self, state, mode, **kw):
    #     return dict(
    #         cities=[(st.id, st.name, st.zipcode or "") for st in state.get_website_sale_cities(mode=mode)],
    #     )

    @route(['/my/account'], type='http', auth='user', website=True)
    def account(self, redirect=None, **post):
        _logger.info("============i===was====new")
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        values.update({
            'error': {},
            'error_message': [],
        })

        if post and request.httprequest.method == 'POST':
            error, error_message = self.details_form_validate(post)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)
            if not error:
              
                values = {key: post[key] for key in self.MANDATORY_BILLING_FIELDS}
                values.update({key: post[key] for key in self.OPTIONAL_BILLING_FIELDS if key in post})
                for field in set(['country_id','address_state','address_district','address_local']) & set(values.keys()):
                    try:
                        values[field] = int(values[field])
                    except:
                        values[field] = False
                values.update({'zip': values.pop('zipcode', '')})
                # values.update({'mobile': values.pop('mobile', '')})
                # values.update({'street_name': values.pop('street_name', '')})
                # values.update({'ward_no': values.pop('ward_no', '')})
                partner.sudo().write(values)
                if redirect:
                    return request.redirect(redirect)
                return request.redirect('/my/home')

        countries = request.env['res.country'].sudo().search([])
        states = request.env['res.country.state'].sudo().search([])
        statess = request.env['res.nepal.state'].sudo().search([])
        districts = request.env['res.address.district'].sudo().search([])
        localss = request.env['res.address.local'].sudo().search([])

        values.update({
            'partner': partner,
            'countries': countries,
            'address_state': statess,
            'has_check_vat': hasattr(request.env['res.partner'], 'check_vat'),
            'redirect': redirect,
            'page_name': 'my_details',
            'address_district':districts,
            'address_local':localss
        })

        response = request.render("portal.portal_my_details", values)
        response.headers['X-Frame-Options'] = 'DENY'
        return response

    @http.route("/states", type='json', auth="public", methods=['POST'], website=True)
    def stateChange(self, **kw):
        a=kw['statess']
        _logger.info("====================onchange")
        districtss = request.env['res.address.district'].sudo().search([])
        statess = request.env['res.nepal.state'].sudo().search([('id','=',a)])
        districtss = request.env['res.address.district'].sudo().search([('state_id','=',statess.id)])
        return dict(
            districts=[(dist.id, dist.name) for dist in districtss]
        )
    
    @http.route("/district", type='json', auth="public", methods=['POST'], website=True)
    def districtChange(self, **kw):
        a=kw['districts']
        _logger.info("==============disrict==change")
        locals = request.env['res.address.local'].sudo().search([])
        district = request.env['res.address.district'].sudo().search([('id','=',a)])
        locals = request.env['res.address.local'].sudo().search([('district_id','=',district.id)])
        return dict(
            locals=[(loc.id, loc.name) for loc in locals]
        )


    