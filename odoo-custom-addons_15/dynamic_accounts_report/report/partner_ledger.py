from odoo import api, models, _

import logging
_logger = logging.getLogger(__name__)

class PartnerLedgerReport(models.AbstractModel):
    _name = 'report.dynamic_accounts_report.partner_ledger'

    @api.model
    def _get_report_values(self, docids, data=None):
        _logger.info("[===%s===========22222222222222222=============================BIPIN=================================",self)
        if self.env.context.get('partner_ledger_pdf_report'):

            if data.get('report_data'):
                # _logger.info("========reports======%s",data.get('report_data')['fold'])
                # if data.get('report_data')['fold']:
                data.update({'account_data': data.get('report_data')['report_lines'],
                            'Filters': data.get('report_data')['filters'],
                            'company': self.env.company,
                            'fold': data.get('report_data')['fold']
                            })
                
            
        
        return data


    # @api.model
    # def _get_report_values_fold(self, docids, data=None):
    #     _logger.info("[===========================================BIPIN=================================")
    #     if self.env.context.get('partner_ledger_pdf_report_fold'):

    #         if data.get('report_data'):
    #             _logger.info("========reports======%s",data.get('report_data')['filters'])
    #             data.update({'account_data': data.get('report_data')['report_lines'],
    #                          'Filters': data.get('report_data')['filters'],
    #                          'company': self.env.company,
    #                          })
        
    #     return data
