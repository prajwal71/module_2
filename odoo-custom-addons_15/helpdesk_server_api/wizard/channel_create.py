from odoo import models, fields, api, _


class CreateChannel(models.TransientModel):
    _name = 'create.channel'
    _description = 'Create Channel'

    def get_data(self):
        if self.env.context.get('active_model') and self.env.context.get('active_id'):
            client_id = self.env[self.env.context.get('active_model')].browse(
                self.env.context.get('active_id'))
            if client_id:
                for client in client_id:
                    client.channel_create()
        return