from odoo import models, fields, api, _


class TicketAccept(models.TransientModel):
    _name = 'ticket.accept'
    _description = 'Ticket Accept'

    def get_data(self):
        if self.env.context.get('active_model') and self.env.context.get('active_id'):
            ticket_record_id = self.env[self.env.context.get('active_model')].browse(
                self.env.context.get('active_id'))
            if ticket_record_id:
                for ticket in ticket_record_id:
                    ticket.message_post(
                        body=_('Your %s ticket has been registered in our system.') % ticket.origin, message_type='comment')
                    ticket.ticket_accepted()

class TicketClose(models.TransientModel):
    _name = 'ticket.close'
    _description = 'Ticket Close'

    hour_taken = fields.Float('Hours Taken', required=True)
    closing_comment = fields.Text(string="Closing Comment", required=True)
    
    def get_data(self):
        if self.env.context.get('active_model') and self.env.context.get('active_id'):
            ticket_record_id = self.env[self.env.context.get('active_model')].browse(
                self.env.context.get('active_id'))
            if ticket_record_id:
                ticket_record_id.write({'hour_taken': self.hour_taken, 'closing_comment': self.closing_comment})
                ticket_record_id.ticket_closed()
                