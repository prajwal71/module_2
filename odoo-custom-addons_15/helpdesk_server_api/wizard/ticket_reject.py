from odoo import models, fields, api, _


class TicketReject(models.TransientModel):
    _name = 'ticket.reject'
    _description = 'Ticket Reject'

    def get_data(self):
        if self.env.context.get('active_model') and self.env.context.get('active_id'):
            ticket_record_id = self.env[self.env.context.get('active_model')].browse(
                self.env.context.get('active_id'))
            if ticket_record_id:
                if self.env.context.get('from_reject'):
                    ticket_record_id.write({'reject_reason': self.reason})
                for ticket in ticket_record_id:
                    ticket.message_post(
                        body=_('Your %s ticket has been refused.') % ticket.origin, message_type='comment')
                    ticket.ticket_rejected()

    reason = fields.Text('Note', required=True)
