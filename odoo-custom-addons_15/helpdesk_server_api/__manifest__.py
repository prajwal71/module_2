# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Helpdesk Server API',
    'version': '15.0',
    'category': 'Helpdesk',
    'summary': 'Helpdesk Ticket Server API',
    'author': 'Nexus Incorporation',
    'description': """
The aim is to have a complete module to manage all client tickets and helps to communicate with server for ticket for tracking.
====================================================================

The following topics are covered by this module:
------------------------------------------------------
    * Add/remove client Tickets.
    * Track Ticket Stage.
    * Get notified about ticket.
""",
    'depends': ['helpdesk', 'base', 'bus'],
    'data': [
        'data/cron.xml',
        'security/ir.model.access.csv',
        'views/helpdesk_ticket_views.xml',
        'views/update_stack_view.xml',
        'views/api_client_config.xml',
        'wizard/ticket_reject.xml',
        'wizard/ticket_accept.xml',
        'wizard/channel_create.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'LGPL-3',
}
