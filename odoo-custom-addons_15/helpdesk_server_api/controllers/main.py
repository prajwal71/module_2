# -*- coding: utf-8 -*-
import json
import logging

from odoo.http import request
from datetime import datetime, timedelta

from odoo import http

_logger = logging.getLogger(__name__)
header = [('Content-Type', 'application/json'),
          ('Cache-Control', 'no-store')]


class CreateTicket(http.Controller):

    @http.route('/create-ticket/<int:client_id>/', type='json', auth="public", methods=['POST'], csrf=False)
    def create_ticket(self, client_id=None, **kwargs):
        api_key = kwargs.get('api_key') or False
        ticket_id = kwargs.get('client_ticket_id') or False
        _logger.info("===api_key===%s", api_key)
        _logger.info(request.env.user)
        if api_key:
            client_rec = request.env['api.client.config'].sudo().search(
                [('api_key', '=', api_key), ('client_id', '=', client_id)])
            _logger.info("====client_rec===== %s", client_rec)
            if client_rec:
                if client_rec.active:
                    ticket_obj = request.env['helpdesk.ticket'].sudo().search([
                        ('partner_id', '=', client_rec.client_id), ('client_ticket_id', '=', ticket_id)])
                    _logger.info("====ticket_obj===== %s", ticket_obj)
                    if not ticket_obj:

                        support_package = request.env['accounting.support.package'].sudo().search([('partner_id', '=', client_rec.partner_id.id)])                        
                        if support_package.package_code == 'amc':
                            time_difference = datetime.today().date() - support_package.amc_date
                            if time_difference.days > 30 and support_package.bal_amount > 0.0:
                                res = {'result': 'failed',
                                'msg':"Your Amc package has expired for more than 30 days and you have out standing dues with Us. Please Contact Our team.",}
                                _logger.info("====res=====%s", res)
                                return json.dumps(res)
                            else:
                                tkt_rec = request.env['helpdesk.ticket'].sudo().create({
                                'name': kwargs.get('subject'),
                                'description': kwargs.get('description'),
                                'priority': kwargs.get('priority'),
                                'origin': kwargs.get('name'),
                                'partner_id': client_id,
                                'client_ticket_id': kwargs.get('client_ticket_id'),
                                'ticket_sync': kwargs.get('ticket_sync'),
                                })
                                res = {'result': 'success',
                                'ticket_id': tkt_rec.id,
                                'ref_no': tkt_rec.name, }
                                _logger.info("====res=====%s", res)
                                return json.dumps(res)
                        else:
                            tkt_rec = request.env['helpdesk.ticket'].sudo().create({
                                'name': kwargs.get('subject'),
                                'description': kwargs.get('description'),
                                'priority': kwargs.get('priority'),
                                'origin': kwargs.get('name'),
                                'partner_id': client_id,
                                'client_ticket_id': kwargs.get('client_ticket_id'),
                                'ticket_sync': kwargs.get('ticket_sync'),
                            })
                            res = {'result': 'success',
                                'ticket_id': tkt_rec.id,
                                'ref_no': tkt_rec.name, }
                            _logger.info("====res=====%s", res)
                            return json.dumps(res)
                    else:
                        res = {'result': 'Ticket already registered in Server side.'}
                        _logger.info("====res=====%s", res)
                        return json.dumps(res)
                else:
                    res = {'result': 'Client is not Active'}
                    _logger.info("====res=====%s", res)
                    return json.dumps(res)
            else:
                res = {'result': 'Client with given details not found in system'}
                _logger.info("====res=====%s", res)
                return json.dumps(res)
        else:
            res = {'result': 'API Key Not Found'}
            _logger.info("====res=====%s", res)
            return json.dumps(res)
