# -*- coding: utf-8 -*-

from odoo import fields, models,api, _
import requests
import base64

import xmlrpc.client

import logging

_logger = logging.getLogger(__name__)


class UpdateStack(models.Model):
    _name = 'update.stack'

    TICKET_PRIORITY = [
        ('0', 'All'),
        ('1', 'Low priority'),
        ('2', 'High priority'),
        ('3', 'Urgent'),
    ]

    name = fields.Char(string="Update Type")
    state = fields.Selection([
        ('to_update', 'To Update'),
        ('updated', 'Updated')
    ])
    partner_id = fields.Many2one('res.partner', string='Customer')

    # for ticket update
    description = fields.Html('Description')
    priority = fields.Selection(TICKET_PRIORITY, string='Priority', default='0')
    stage_id = fields.Many2one('helpdesk.stage', string='Stage')
    reject_reason = fields.Text('Rejection Reason')
    ticket_sync = fields.Boolean(string='Sync Ticket', default=False)
    origin = fields.Char(string='Source')
    hour_taken = fields.Float('Hours Taken')
    closing_comment = fields.Text(string="Closing Comment")

    #for log note
    log_model = fields.Char('Related Document Model', index=True)
    log_res_id = fields.Many2oneReference('Related Document ID', index=True, model_field='model')
    log_body = fields.Html('Contents', default='', sanitize_style=True)
    log_message_type = fields.Selection([
        ('email', 'Email'),
        ('comment', 'Comment'),
        ('notification', 'System notification'),
        ('user_notification', 'User Specific Notification')],
        'Type'
        )
    log_subtype_id = fields.Many2one('mail.message.subtype', 'Subtype')
    log_attachment_ids = fields.Many2many(
        'ir.attachment', 'log_attachment_rel',
        'message_id', 'attachment_id',
        string='Attachments',
        help='Attachments are linked to a document through model / res_id and to the message '
             'through this field.')
    client_ticket_id = fields.Integer()

    #for channel
    channel_id = fields.Integer('Channel ID')
    ch_body = fields.Html('Contents', default='', sanitize_style=True)
    ch_subject = fields.Char('Subject')

    def update_record(self):
        if not self:
            self = self.env['update.stack'].sudo().search([])
        for rec in self:
            if rec.state == 'to_update':
                client_api_rec = self.env['api.client.config'].search(
                    [('partner_id', '=', rec.partner_id.id)], limit=1)

                _logger.error('-----------------client api-------------------------%s', client_api_rec)
                _logger.error('-----------------client ticket id-------------------------%s', rec.client_ticket_id)
                if client_api_rec:
                    url = client_api_rec.client_url
                    db = client_api_rec.client_database
                    username = client_api_rec.client_username
                    password = client_api_rec.client_password

                    try:
                        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))

                        uid = common.authenticate(db, username, password, {})
                        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

                        if rec.name == 'ticket':
                            vals = {
                            'description': rec.description,
                            'priority': rec.priority,
                            'ticket_sync': rec.ticket_sync,
                            }
                            _logger.error('-----------------Data vals-------------------------%s', vals)
                            
                            update_ticket = models.execute_kw(db, uid, password, 'client.ticket', 'write',
                                                            [[rec.client_ticket_id], vals])
                            _logger.error('-----------------Data updated-------------------------%s', update_ticket)
                            
                            rec.state = 'updated'
                        
                        elif rec.name == 'accept_ticket':
                            rec.state = 'updated'
                            vals = {
                            'stage_id': rec.stage_id.id,
                            }
                            _logger.error('-----------------Data vals-------------------------%s', vals)
                            
                            update_ticket = models.execute_kw(db, uid, password, 'client.ticket', 'write',
                                                            [[rec.client_ticket_id], vals])
                            message = (_('Your %s ticket has been registered in our system.') % rec.origin)
                            notif_type = 'accept'
                            notify = models.execute_kw(db, uid, password, 'client.ticket', 'action_notify',
                                                            [[rec.client_ticket_id], notif_type, message])
                            
                        elif rec.name == 'close_ticket':
                            rec.state = 'updated' 
                            vals = {
                            'stage_id': rec.stage_id.id,
                            'hour_taken': rec.hour_taken,
                            'closing_comment': rec.closing_comment,
                            }
                            _logger.error('-----------------Data vals-------------------------%s', vals)
                            
                            update_ticket = models.execute_kw(db, uid, password, 'client.ticket', 'write',
                                                            [[rec.client_ticket_id], vals])
                            message = (_('Your %s ticket has been closed!! Total hour taken : %.2f with Closing Comment: %s') % (rec.origin, rec.hour_taken, rec.closing_comment))
                            notif_type = 'closed'
                            notify = models.execute_kw(db, uid, password, 'client.ticket', 'action_notify',
                                                            [[rec.client_ticket_id], notif_type, message])

                        elif rec.name == 'reject_ticket':
                            rec.state = 'updated' 
                            vals = {
                            'stage_id': rec.stage_id.id,
                            'reject_reason': rec.reject_reason,
                            }
                            _logger.error('-----------------Data vals-------------------------%s', vals)
                            
                            update_ticket = models.execute_kw(db, uid, password, 'client.ticket', 'write',
                                                            [[rec.client_ticket_id], vals])
                            message = (_('Your %s ticket has been refused!! reason: %s') % (rec.origin, rec.reject_reason))
                            notif_type = 'reject'
                            notify = models.execute_kw(db, uid, password, 'client.ticket', 'action_notify',
                                                            [[rec.client_ticket_id], notif_type, message])
                                                            
                        elif rec.name == 'log/attachment':
                            attachments = []
                            if rec.log_attachment_ids:
                                for attachment in rec.log_attachment_ids:
                                    attachments.append({
                                        'name': attachment.name,
                                        'type': attachment.type,
                                        'datas': attachment.datas,
                                        'res_model': 'client.ticket',
                                        'res_id': rec.client_ticket_id,
                                        'res_name': attachment.res_name,
                                    })
                                create_attachment = models.execute_kw(db, uid, password, 'ir.attachment', 'create',
                                                            [attachments])
                                _logger.info('=================%s', create_attachment)
                                
                                vals = {
                                    'model': 'client.ticket',
                                    'author_id': 41,
                                    'res_id': rec.client_ticket_id,
                                    'body': rec.log_body,
                                    'attachment_ids': create_attachment,
                                    'message_type': rec.log_message_type,
                                    'subtype_id': rec.log_subtype_id.id,
                                }
                            else:
                                vals = {
                                    'model': 'client.ticket',
                                    'author_id': 41,
                                    'res_id': rec.client_ticket_id,
                                    'body': rec.log_body,
                                    'message_type': rec.log_message_type,
                                    'subtype_id': rec.log_subtype_id.id,
                                }
                            update_log = models.execute_kw(db, uid, password, 'mail.message', 'create',
                                                            [vals])
                            _logger.error('-----------------Data updated-------------------------%s', update_log)
                            rec.state = 'updated'

                        elif rec.name == 'channel/attachment':
                            create_attachment = []
                            attachments = []
                            client_channel_id = models.execute_kw(db, uid, password, 'mail.channel', 'search',
                                                                [[['server_channel_id', '=', rec.channel_id]]])
                            
                            if rec.log_attachment_ids:
                                for attachment in rec.log_attachment_ids:
                                    attachments.append({
                                        'name': attachment.name,
                                        'type': attachment.type,
                                        'datas': attachment.datas,
                                        'res_model': 'mail.channel',
                                        'res_id': rec.client_channel_id,
                                        'res_name': rec.name,
                                    })
                                create_attachment = models.execute_kw(db, uid, password, 'ir.attachment', 'create',
                                                            [attachments])

                                notify = models.execute_kw(db, uid, password, 'mail.channel', 'send_message',
                                                            [client_channel_id[0], rec.ch_body, rec.ch_subject, create_attachment])                                
                                _logger.info('=======================================================%s', notify)
                                
                                rec.state = 'updated'
                            else:
                                notify = models.execute_kw(db, uid, password, 'mail.channel', 'send_message',
                                                            [client_channel_id[0], rec.ch_body, rec.ch_subject, create_attachment])                                        
                                _logger.info('=======================================================%s', notify)
                                
                                rec.state = 'updated'
                    except:
                        pass
            else:
                rec.unlink()


