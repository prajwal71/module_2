# -*- coding: utf-8 -*-

from odoo import fields, models,api, _
from odoo.exceptions import UserError, Warning
import requests
import base64

import xmlrpc.client

import logging

_logger = logging.getLogger(__name__)

class HelpdeskTicket(models.Model):
    _inherit = 'helpdesk.ticket'

    origin = fields.Char(string='Source')
    client_ticket_id = fields.Integer(string='Client Ticket ID')
    reject_reason = fields.Text('Rejection Reason')
    ticket_sync = fields.Boolean(string='Sync Ticket')
    hour_taken = fields.Float('Hours Taken', readonly=True)
    closing_comment = fields.Text(string="Closing Comment", readonly=True)

    def open_chat(self):
        for rec in self:
            channel_id = self.env['mail.channel'].search([('client_id', '=', rec.partner_id.id)])
            if channel_id:
                channel_id.sudo().message_post(
                    body=rec.origin,
                    author_id= 2,
                    message_type="comment",
                    subtype_xmlid="mail.mt_comment",
                )
            else:
                raise UserError(_('No Channel Found!!'))

    def write(self, vals):
        result = super(HelpdeskTicket, self).write(vals)
        if self.client_ticket_id:
            for rec in self:
                client_api_rec = self.env['api.client.config'].search(
                    [('partner_id', '=', rec.partner_id.id)], limit=1)

                _logger.error('-----------------client api-------------------------%s', client_api_rec)
                _logger.error('-----------------client ticket id-------------------------%s', rec.client_ticket_id)
                if client_api_rec:
                    url = client_api_rec.client_url
                    db = client_api_rec.client_database
                    username = client_api_rec.client_username
                    password = client_api_rec.client_password

                    try:
                        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))

                        uid = common.authenticate(db, username, password, {})
                        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
                        for ticket in self:
                            vals = {
                                'description': ticket.description,
                                'priority': ticket.priority,
                                'reject_reason': ticket.reject_reason,
                                'ticket_sync': ticket.ticket_sync,
                            }
                            _logger.error('-----------------Data vals-------------------------%s', vals)
                        _logger.error('-----------------ticket-------------------------%s', ticket)
                        
                        update_ticket = models.execute_kw(db, uid, password, 'client.ticket', 'write',
                                                        [[rec.client_ticket_id], vals])
                        _logger.error('-----------------Data vals-------------------------%s', vals)
                        _logger.error('-----------------Data updated-------------------------%s', update_ticket)
                    except:
                        for ticket in self:
                            stack = self.env['update.stack'].sudo().create({
                                    'name': 'ticket',
                                    'state': 'to_update',
                                    'partner_id': rec.partner_id.id,
                                    'description': ticket.description,
                                    'priority': ticket.priority,
                                    'reject_reason': ticket.reject_reason,
                                    'ticket_sync': ticket.ticket_sync,
                                    'client_ticket_id': ticket.client_ticket_id,
                                })
                            _logger.error('-----------------Data Stack-------------------------%s', stack)

        return result

    def ticket_accepted(self):
        if self.client_ticket_id:
            for rec in self:
                rec.write({'stage_id': 2})
                client_api_rec = self.env['api.client.config'].search(
                    [('partner_id', '=', rec.partner_id.id)], limit=1)

                _logger.error('-----------------client api-------------------------%s', client_api_rec)
                _logger.error('-----------------client ticket id-------------------------%s', rec.client_ticket_id)
                if client_api_rec:
                    url = client_api_rec.client_url
                    db = client_api_rec.client_database
                    username = client_api_rec.client_username
                    password = client_api_rec.client_password

                    try:
                        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
                        uid = common.authenticate(db, username, password, {})
                        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

                        for ticket in self:
                            vals = {
                                'stage_id': ticket.stage_id.id,
                            }
                            _logger.error('-----------------Data vals-------------------------%s', vals)
                        _logger.error('-----------------ticket-------------------------%s', ticket)
                        
                        update_ticket = models.execute_kw(db, uid, password, 'client.ticket', 'write',
                                                        [[rec.client_ticket_id], vals])
                        message = (_('Your %s ticket has been registered in our system.') % ticket.origin)
                        notif_type = 'accept'
                        notify = models.execute_kw(db, uid, password, 'client.ticket', 'action_notify',
                                                        [[rec.client_ticket_id], notif_type, message])
                        _logger.error('-----------------Data vals-------------------------%s', vals)
                        _logger.error('-----------------Data updated-------------------------%s', update_ticket)
                        _logger.error('-----------------Data updated-------------------------%s', notify)
                    except:
                        for ticket in self:
                            stack = self.env['update.stack'].sudo().create({
                                    'name': 'accept_ticket',
                                    'state': 'to_update',
                                    'partner_id': rec.partner_id.id,
                                    'stage_id': ticket.stage_id.id,
                                    'origin': ticket.origin,
                                    'client_ticket_id': ticket.client_ticket_id,
                                })
                            _logger.error('-----------------Data Stack-------------------------%s', stack)


    def ticket_closed(self):
        if self.client_ticket_id:
            for rec in self:
                rec.write({'stage_id': 3})
                client_api_rec = self.env['api.client.config'].search(
                    [('partner_id', '=', rec.partner_id.id)], limit=1)

                _logger.error('-----------------client api-------------------------%s', client_api_rec)
                _logger.error('-----------------client ticket id-------------------------%s', rec.client_ticket_id)
                if client_api_rec:
                    url = client_api_rec.client_url
                    db = client_api_rec.client_database
                    username = client_api_rec.client_username
                    password = client_api_rec.client_password

                    try:
                        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
                        uid = common.authenticate(db, username, password, {})
                        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

                        for ticket in self:
                            vals = {
                                'stage_id': ticket.stage_id.id,
                                'hour_taken': ticket.hour_taken,
                                'closing_comment': ticket.closing_comment,
                            }
                            _logger.error('-----------------Data vals-------------------------%s', vals)
                        _logger.error('-----------------ticket-------------------------%s', ticket)
                        
                        update_ticket = models.execute_kw(db, uid, password, 'client.ticket', 'write',
                                                        [[rec.client_ticket_id], vals])
                        message = (_('Your %s ticket has been closed successfully!! Total hour taken : %.2f with Closing Comment: %s') % (ticket.origin, ticket.hour_taken, ticket.closing_comment))
                        notif_type = 'closed'
                        notify = models.execute_kw(db, uid, password, 'client.ticket', 'action_notify',
                                                        [[rec.client_ticket_id], notif_type, message])
                        _logger.error('-----------------Data vals-------------------------%s', vals)
                        _logger.error('-----------------Data updated-------------------------%s', update_ticket)
                        _logger.error('-----------------Data updated-------------------------%s', notify)
                    except:
                        for ticket in self:
                            stack = self.env['update.stack'].sudo().create({
                                    'name': 'close_ticket',
                                    'state': 'to_update',
                                    'partner_id': rec.partner_id.id,
                                    'stage_id': ticket.stage_id.id,
                                    'origin': ticket.origin,
                                    'hour_taken': ticket.hour_taken,
                                    'closing_comment': ticket.closing_comment,
                                    'client_ticket_id': ticket.client_ticket_id,
                                })
                            _logger.error('-----------------Data Stack-------------------------%s', stack)

    def ticket_rejected(self):
        if self.client_ticket_id:
            for rec in self:
                rec.write({'stage_id': 4})
                client_api_rec = self.env['api.client.config'].search(
                    [('partner_id', '=', rec.partner_id.id)], limit=1)

                _logger.error('-----------------client api-------------------------%s', client_api_rec)
                _logger.error('-----------------client ticket id-------------------------%s', rec.client_ticket_id)
                if client_api_rec:
                    url = client_api_rec.client_url
                    db = client_api_rec.client_database
                    username = client_api_rec.client_username
                    password = client_api_rec.client_password

                    try:
                        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
                        uid = common.authenticate(db, username, password, {})
                        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

                        for ticket in self:
                            vals = {
                                'stage_id': ticket.stage_id.id,
                                'reject_reason': ticket.reject_reason,
                            }
                            _logger.error('-----------------Data vals-------------------------%s', vals)
                        _logger.error('-----------------ticket-------------------------%s', ticket)
                        
                        update_ticket = models.execute_kw(db, uid, password, 'client.ticket', 'write',
                                                        [[rec.client_ticket_id], vals])
                        message = (_('Your %s ticket has been refused!! Reason: %s') % (rec.origin, rec.reject_reason))
                        notif_type = 'reject'
                        notify = models.execute_kw(db, uid, password, 'client.ticket', 'action_notify',
                                                        [[rec.client_ticket_id], notif_type, message])
                        _logger.error('-----------------Data vals-------------------------%s', vals)
                        _logger.error('-----------------Data updated-------------------------%s', update_ticket)
                        _logger.error('-----------------Data updated-------------------------%s', notify)
                    except:
                        for ticket in self:
                            stack = self.env['update.stack'].sudo().create({
                                    'name': 'reject_ticket',
                                    'state': 'to_update',
                                    'partner_id': rec.partner_id.id,
                                    'origin': rec.origin,
                                    'stage_id': ticket.stage_id.id,
                                    'reject_reason': ticket.reject_reason,
                                    'client_ticket_id': ticket.client_ticket_id,
                                })
                            _logger.error('-----------------Data Stack-------------------------%s', stack)
    
    
    
    def accept_ticket(self):
        return {
            'name': _('Confirmation'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': self.env.ref('helpdesk_server_api.wizard_ticket_accept_form_view').id,
            'res_model': 'ticket.accept',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'from_accept': True}
        } 

    def close_ticket(self):
        return {
            'name': _('Close Ticket'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': self.env.ref('helpdesk_server_api.wizard_ticket_close_form_view').id,
            'res_model': 'ticket.close',
            'type': 'ir.actions.act_window',
            'target': 'new',
        } 

    def refuse_ticket(self):
        return {
            'name': _('Ticket Reject'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': self.env.ref('helpdesk_server_api.wizard_ticket_reject_form_view').id,
            'res_model': 'ticket.reject',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'from_reject': True}
        }
   
class MailMessageExt(models.Model):
    _inherit = 'mail.message'

    @api.model
    def create(self, vals):
        res = super(MailMessageExt, self).create(vals)
        
        # ===================== for helpdesk log note and attachment ====================== #
        if res.model == 'helpdesk.ticket' and res.author_id.id == self.env.user.partner_id.id:
            ticket_id = self.env['helpdesk.ticket'].search([('id', '=', res.res_id)])
            if ticket_id.client_ticket_id:
                for rec in ticket_id:
                    client_api_rec = ticket_id.env['api.client.config'].search(
                        [('partner_id', '=', rec.partner_id.id)], limit=1)

                    if client_api_rec:
                        url = client_api_rec.client_url
                        db = client_api_rec.client_database
                        username = client_api_rec.client_username
                        password = client_api_rec.client_password

                        if res.attachment_ids:
                            for attach in res.attachment_ids:
                                if (attach.file_size/ 1024.0/ 1024.0) > client_api_rec.max_file_size:
                                    raise UserError(_('Cannot attach file larger than %.2f MB!!' % (client_api_rec.max_file_size)))

                        try:
                            common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))

                            uid = common.authenticate(db, username, password, {})
                            models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
                            attachments = []
                            if res.attachment_ids:
                                for attachment in res.attachment_ids:
                                    attachments.append({
                                        'name': attachment.name,
                                        'type': attachment.type,
                                        'datas': attachment.datas,
                                        'res_model': 'client.ticket',
                                        'res_id': rec.client_ticket_id,
                                        'res_name': rec.origin,
                                    })
                                create_attachment = models.execute_kw(db, uid, password, 'ir.attachment', 'create',
                                                            [attachments])
                                _logger.info('=================%s', create_attachment)
                                
                                vals = {
                                    'model': 'client.ticket',
                                    'author_id': 41,
                                    'res_id': rec.client_ticket_id,
                                    'body': res.body,
                                    'attachment_ids': create_attachment,
                                    'message_type': res.message_type,
                                    'subtype_id': res.subtype_id.id,
                                }
                            else:
                                vals = {
                                    'model': 'client.ticket',
                                    'author_id': 41,
                                    'res_id': rec.client_ticket_id,
                                    'body': res.body,
                                    'message_type': res.message_type,
                                    'subtype_id': res.subtype_id.id,
                                }

                            update_log = models.execute_kw(db, uid, password, 'mail.message', 'create',
                                                            [vals])
                        except:
                            if res.attachment_ids:
                                stack = self.env['update.stack'].sudo().create({
                                        'name': 'log/attachment',
                                        'state': 'to_update',
                                        'partner_id': rec.partner_id.id,
                                        'log_model': res.model,
                                        'log_res_id': rec.client_ticket_id,
                                        'log_body': res.body,
                                        'log_attachment_ids': res.attachment_ids,
                                        'log_message_type': res.message_type,
                                        'log_subtype_id': res.subtype_id.id,
                                    })
                            else:
                                stack = self.env['update.stack'].sudo().create({
                                        'name': 'log/attachment',
                                        'state': 'to_update',
                                        'partner_id': rec.partner_id.id,
                                        'log_model': res.model,
                                        'log_res_id': rec.client_ticket_id,
                                        'log_body': res.body,
                                        'log_message_type': res.message_type,
                                        'log_subtype_id': res.subtype_id.id,
                                    })
                            _logger.error('-----------------Data Stack-------------------------%s', stack)

        # ===================== for communication through channel ====================== #

        if res.model == 'mail.channel':
            channel_id = self.env['mail.channel'].search([('id', '=', res.res_id)])
            _logger.info('=======================================================%s', channel_id)
            if channel_id.client_id and res.author_id.id != channel_id.client_id:
                _logger.info('=======================================================%s', channel_id.client_id)
                for rec in channel_id:
                    client_api_rec = self.env['api.client.config'].search(
                        [('partner_id', '=', rec.client_id)], limit=1)
                    _logger.info('=======================================================%s', client_api_rec)
                    if client_api_rec:
                        url = client_api_rec.client_url
                        db = client_api_rec.client_database
                        username = client_api_rec.client_username
                        password = client_api_rec.client_password

                        if res.attachment_ids:
                            for attach in res.attachment_ids:
                                if (attach.file_size/ 1024.0/ 1024.0) > client_api_rec.max_file_size:
                                    raise UserError(_('Cannot attach file larger than %.2f MB!!' % (client_api_rec.max_file_size)))

                        try:
                            common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))

                            uid = common.authenticate(db, username, password, {})
                            models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

                            client_channel_id = models.execute_kw(db, uid, password, 'mail.channel', 'search',
                                                                [[['server_channel_id', '=', channel_id.id]]])
                            if client_channel_id:
                                create_attachment = []
                                attachments = []
                                
                                if res.attachment_ids:
                                    for attachment in res.attachment_ids:
                                        attachments.append({
                                            'name': attachment.name,
                                            'type': attachment.type,
                                            'datas': attachment.datas,
                                            'res_model': 'mail.channel',
                                            'res_id': client_channel_id[0],
                                            'res_name': rec.name,
                                        })
                                    create_attachment = models.execute_kw(db, uid, password, 'ir.attachment', 'create',
                                                                [attachments])

                                notify = models.execute_kw(db, uid, password, 'mail.channel', 'send_message',
                                                            [client_channel_id[0], res.body, res.subject, create_attachment])
                                _logger.info('=======================================================%s', notify)
                        except:
                            if res.attachment_ids:
                                stack = self.env['update.stack'].sudo().create({
                                        'name': 'channel/attachment',
                                        'state': 'to_update',
                                        'partner_id': rec.client_id,
                                        'ch_body': res.body,
                                        'ch_subject': res.subject,
                                        'channel_id': channel_id.id,
                                        'log_attachment_ids': res.attachment_ids,
                                    })
                            else:
                                stack = self.env['update.stack'].sudo().create({
                                        'name': 'channel/attachment',
                                        'state': 'to_update',
                                        'partner_id': rec.client_id,
                                        'ch_body': res.body,
                                        'ch_subject': res.subject,
                                        'channel_id': channel_id.id,
                                    })
                            _logger.error('-----------------Data Stack-------------------------%s', stack)
        return res   


class MailChannelExt(models.Model):
    _inherit = 'mail.channel'

    client_id = fields.Integer('Client ID')

    @api.model
    def send_message(self, sid, author, body, subject, attachments):
        channel = self.env['mail.channel'].sudo().search([('id', '=', sid)],limit=1)
        if channel:
            # send a message to the related user
            channel.sudo().message_post(
                body=body,
                author_id=author,
                subject=subject,
                attachment_ids=attachments,
                message_type="comment",
                subtype_xmlid="mail.mt_comment",
            )
        return True