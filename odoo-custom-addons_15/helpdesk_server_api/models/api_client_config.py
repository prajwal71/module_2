# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, Warning
import requests
import base64
import xmlrpc.client
import logging

_logger = logging.getLogger(__name__)


class APIKeyConfig(models.Model):
    _name = 'api.client.config'
    _description = 'Client API Config'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char('Client Name', required=True)
    partner_id = fields.Many2one('res.partner', 'Customer', required=True)
    api_key = fields.Char('API KEY', required=True, tracking=True)
    client_id = fields.Integer(related='partner_id.id', string='Client ID', store=True)
    active = fields.Boolean(default=True)
    client_url = fields.Char('API URL', required=True, tracking=True)
    # rpc filelds
    client_database = fields.Char('Database Name', tracking=True)
    client_username = fields.Char('Username', tracking=True)
    client_password = fields.Char('Password', tracking=True)
    channel = fields.Boolean(default=False)
    max_file_size = fields.Float('Max File Size (in MB)', tracking=True)

    def create_channel(self):
        return {
            'name': _('Confirmation'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': self.env.ref('helpdesk_server_api.channel_create_form_view').id,
            'res_model': 'create.channel',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'channel_create': True}
        } 
    
    def channel_create(self):
        for rec in self:
            nam = 'Nexus Incorporation' + ', ' + rec.partner_id.name
            channel = self.env['mail.channel'].sudo().search([
                            ('name','=',nam),
                            ('public', '=', 'groups'),
                            ('client_id', 'in', [rec.client_id])
                        ],
                            limit=1,
                        )
            _logger.info("============hr1===%s",channel)
            if not channel:
                # create a new channel
                channel = self.env['mail.channel'].with_context(mail_create_nosubscribe=True).sudo().create({
                    'channel_partner_ids': [(4, rec.client_id)],
                    'public': 'groups',
                    'group_public_id': 1,
                    'group_ids': [(4, 1)],
                    'channel_type': 'chat',
                    # 'email_send': False,
                    'name': nam,
                    'display_name': nam,
                    'client_id': rec.client_id
                })
                _logger.info("============hr2===%s",channel)

                URL = rec.client_url
                DB = rec.client_database
                USERNAME = rec.client_username
                PASSWORD = rec.client_password
                # try:
                    # create channel on client side
                common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(URL))
                uid = common.authenticate(DB, USERNAME, PASSWORD, {})
                models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(URL))
                search_channel = models.execute_kw(DB, uid, PASSWORD, 'mail.channel', 'search',[[['name', '=', nam], ['public', '=', 'groups']]])
                search_server_id = models.execute_kw(DB, uid, PASSWORD, 'ir.config_parameter', 'search_read',[[['key', '=', 'helpdesk_server_partner_id']]])
                server_id = search_server_id[0]['value']
                category_id = models.execute_kw(DB, uid, PASSWORD, 'ir.module.category', 'search',[[['name', '=', 'Helpdesk_Client']]])
                search_group_id = models.execute_kw(DB, uid, PASSWORD, 'res.groups', 'search',[[['category_id', '=', category_id]]])
                if not search_channel:
                    create_channel = models.execute_kw(DB, uid, PASSWORD, 'mail.channel', 'create',
                                                                [{
                                                                    'channel_partner_ids': [(4, server_id)],
                                                                    'public': 'groups',
                                                                    'group_public_id': 1,
                                                                    'group_ids': [(4, 1)],
                                                                    'channel_type': 'chat',
                                                                    'name': nam,
                                                                    'display_name': nam,
                                                                    'server_channel_id' : channel.id,
                                                                }])
                    _logger.info('=====================hr3===%s', create_channel)
                rec.channel = True
                channel.sudo().message_post(
                                body='Channel Created',
                                author_id= 2,
                                message_type="comment",
                                subtype_xmlid="mail.mt_comment",
                                )
                # except:
                #     if xmlrpc.client.ProtocolError:
                #         raise ValidationError(_('Protocol error occurred'))
                #     elif xmlrpc.client.Fault:
                #         raise Warning(_('A fault occurred'))
            else:
                rec.channel = True
                raise ValidationError(_('Channel already exists for this Client!!'))


    def action_update_on_client(self):
        for rec in self:
            client_api_rec = self.env['api.client.config'].search(
                    [('partner_id', '=', rec.client_id)], limit=1)
            if client_api_rec:
                if client_api_rec.active:
                    url = client_api_rec.client_url
                    db = client_api_rec.client_database
                    username = client_api_rec.client_username
                    password = client_api_rec.client_password

                    common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
                    uid = common.authenticate(db, username, password, {})
                    models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

                    try:
                        max_file_size = models.execute_kw(db, uid, password, 'ir.config_parameter', 'search',
                                                        [[['key', '=', 'max_file_size_in_mb']]])
                        _logger.info('============================%s', max_file_size)
                        if max_file_size:
                            max_file_size = models.execute_kw(db, uid, password, 'ir.config_parameter', 'unlink',
                                                        [[max_file_size[0]]])
                        max_file_size = models.execute_kw(db, uid, password, 'ir.config_parameter', 'set_param',
                                                    ['max_file_size_in_mb', rec.max_file_size])
                        _logger.info('============================%s', max_file_size)
                    except:
                        if xmlrpc.client.ProtocolError:
                            raise ValidationError(_('Protocol error occurred'))
                        elif xmlrpc.client.Fault:
                            raise Warning(_('A fault occurred'))
                else:
                    raise Warning(_('Client API is inactive!'))
            else:
                raise Warning(_('No configuration for client %s found!!', rec.client_id))
