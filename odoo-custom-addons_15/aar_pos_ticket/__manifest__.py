# -*- coding: utf-8 -*-
####################   AARSOL      ####################
#    AARSOL Pvt. Ltd.
#    Copyright (C) 2010-TODAY AARSOL(<http://www.aarsol.com>).
#    Author: Farooq Arif(<http://www.aarsol.com>)
#
#    It is forbidden to distribute, or sell copies of the module.
#
#    License:  OPL-1
####################   AARSOL      ####################
#
#   Send and email at features@aarsol.com  after download, so about latest updates on this module, you will be informed
#
#
{
    'name': 'POS Ticket Logo and Barcode',
    'summary': """POS Receipt with Barcode and Logo""",
    'version': '11.0.1.0',
    'description': """POS Receipt with Barcode and Logo""",
    'author': 'AARSOL (Pvt) Limited.',
    'company': 'AARSOL (Pvt) Limited.',
    'website': 'http://www.aarsol.com',
    'category': 'Point of Sale',
    'depends': ['base', 'point_of_sale'],
    'license': 'OPL-1',
    'data': [
    	'views/import.xml', 
    	'views/pos_order.xml',    	
    ],
    'assets': {
        'point_of_sale.index':[
            'aar_pos_ticket/static/src/libs/js/jquery-barcode-last.min.js'
        ],
        'point_of_sale.assets': [
              'aar_pos_ticket/static/src/libs/js/zxing.js',
            'aar_pos_ticket/static/src/libs/js/JsBarcode.all.min.js',
            'aar_pos_ticket/static/src/js/model.js',
            'aar_pos_ticket/static/src/js/order.js',
            'aar_pos_ticket/static/src/js/screens.js',
           
            'aar_pos_ticket/static/src/css/aarsol.css'],
         'web.assets_qweb': [
             'aar_pos_ticket/static/src/xml/posticket.xml']
    },
    # 'qweb': ['static/src/xml/posticket.xml'],
    'images': ['static/description/banner.png'],
    'demo': [],        
    'installable': True,
    'application': True,
    'auto_install': False,

}
