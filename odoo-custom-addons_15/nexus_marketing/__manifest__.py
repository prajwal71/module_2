# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Marketing Automation",
    'version': "1.0",
    'summary': "Build automated mailing campaigns",
    'website': 'https://www.odoo.com/app/marketing-automation',
    'category': "Marketing/Marketing Automation",
    'sequence': 195,
    'depends': ['mass_mailing'],
    'data': [
        'security/marketing_automation_security.xml',
        'security/ir.model.access.csv',
        'views/ir_actions_views.xml',
        'views/ir_model_views.xml',
        'views/marketing_automation_menus.xml',
        'wizard/marketing_campaign_test_views.xml',
        'views/link_tracker_views.xml',
        'views/mailing_mailing_views.xml',
        'views/mailing_trace_views.xml',
        'views/marketing_activity_views.xml',
        'views/marketing_participant_views.xml',
        'views/marketing_trace_views.xml',
        'views/marketing_campaign_views.xml',
        'data/ir_cron_data.xml',
    ],
    'demo': [
        'data/marketing_automation_demo.xml'
    ],
    'application': True,
    'license': 'LGPL-3',
    'uninstall_hook': 'uninstall_hook',
    'assets': {
        'web._assets_primary_variables': [
            'nexus_marketing/static/src/scss/variables.scss',
        ],
        'web.assets_backend': [
            'nexus_marketing/static/src/js/marketing_automation_graph.js',
            'nexus_marketing/static/src/js/marketing_automation_one2many.js',
            'nexus_marketing/static/src/js/marketing_campaign_view.js',
            'nexus_marketing/static/src/js/marketing_campaign_controller.js',
            'nexus_marketing/static/src/scss/marketing_automation.scss',
        ],
        'web.qunit_suite_tests': [
            'nexus_marketing/static/tests/**/*',
        ],
    }
}
