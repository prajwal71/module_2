{
    'name': 'CRM Action for Dashboard',
    'version': '1.0',
    'summary': 'This modules helps to open different tree view via dashboard.',
    'description' : 'This modules helps to open different tree view via dashboard.',
    'category': 'companies',
    'author': 'Nexus',
    'depends': ['crm','crm_customization'],
    'data': ['views/view.xml'],
    'installable': True,
    'auto_install': False,

}
