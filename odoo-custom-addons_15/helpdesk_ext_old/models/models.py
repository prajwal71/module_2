from datetime import timedelta

from odoo import api, fields, models, _
from odoo.addons.base.models.res_partner import WARNING_MESSAGE, WARNING_HELP
from odoo.tools.float_utils import float_round


class HelpdeskTicketPrint(models.Model):
   
    _inherit = 'helpdesk.ticket'

    # @api.multi
    def print_ticket(self):
        return self.env.ref('helpdesk_ext.report_helpdesk_ticket_print').report_action(self)
       
       