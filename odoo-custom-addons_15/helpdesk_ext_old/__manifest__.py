# -*- coding: utf-8 -*-
{
    'name': "Helpdesk Ticket Print",

    'summary': """
    This Module is used to print the ticket""",

    'description': """
        This Module is used to print the ticket and bring button on left side of ticket form
    """,

    'author': "Nexus Incorporation",
    'website': "nexusgurus.com",


    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['helpdesk'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/report.xml',
    ],

}