# -*- coding: utf-8 -*-
#################################################################################
# Author      : Nexus Incorporation (<http://www.nexusgurus.com>)
# Copyright(c): 2021-Present Nexus Incorporation
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <http://www.nexusgurus.com/license/>
#################################################################################

{
    'name': 'Payroll Email/Mass E-mail',
    'version': '12.0.1.0.0',
    'summary': """Helps to send payroll Slip to Employees through Email.""",
    'description': 'This module helps you to send payslip through Email.',
    'category': 'Generic Modules/Human Resources',
    'author': 'Nexus Incorporation',
    'website': "http://www.nexusgurus.com",
    'depends': ['base', 'hr_payroll_community', 'mail', 'hr'],
    'data': [
        # 'security/ir.model.access.csv',
        'data/mail_template.xml',
        'views/hr_payroll.xml',
        'views/hr_payslip_wizard_view.xml',
        'views/hr_mass_payroll_wizard.xml'
        
    ],
    'demo': [],
    'images': ['static/description/Banner.png'],
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': False,
    'application': False,
}
