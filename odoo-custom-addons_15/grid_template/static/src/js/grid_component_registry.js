odoo.define('grid_template.component_registry', function (require) {
    "use strict";

    const Registry = require('web.Registry');

    return new Registry();
});

odoo.define('grid_template._component_registry', function (require) {
    "use strict";

    const components = require('grid_template.components');
    const registry = require('grid_template.component_registry');

    registry
        .add('float_factor', components.FloatFactorComponent)
        .add('float_time', components.FloatTimeComponent)
        .add('float_toggle', components.FloatToggleComponent);
});
