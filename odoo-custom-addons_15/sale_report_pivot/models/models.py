# -*- coding: utf-8 -*-

from odoo import models, fields, api

class SaleOrderExt(models.Model):
    _inherit = 'sale.order'

    invoice_date_check = fields.Boolean(compute="_compute_invoice_date",store=True)
    invoice_date = fields.Date('Invoice Date')

    def _compute_invoice_date(self):
        for rec in self:
            if rec.invoice_ids:
                for res in rec.invoice_ids:
                    # if res.state == 'posted':
                    rec.invoice_date_check = True
                    rec.invoice_date = res.invoice_date
            else:
                rec.invoice_date_check = False
                rec.invoice_date = False

class SaleReportExt(models.Model):
    _inherit = 'sale.report'

    invoice_date = fields.Date('Invoice Date', readonly=True, store=True)

    def _query(self, with_clause='', fields={}, groupby='', from_clause=''):
        fields['invoice_date'] = ", s.invoice_date as invoice_date"
        groupby += ', s.invoice_date'
        return super(SaleReportExt, self)._query(with_clause, fields, groupby, from_clause)