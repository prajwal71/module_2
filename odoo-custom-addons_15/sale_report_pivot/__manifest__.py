# -*- coding: utf-8 -*-
{
    'name': "sale_report_pivot",

    'summary': """
    """,

    'description': """
    """,

    'author': "Nexus Incorporation",
    'website': "http://nexusgurus.com",
    'category': 'sale',
    'version': '15.0.0.1',
    'depends': ['sale'],
    'data': [
        'views/views.xml',
    ],
}
