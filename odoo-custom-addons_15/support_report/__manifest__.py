
{
    'name': 'Support Extended Report',
    'version': '15.0.1.0.1',
    'summary':'''This module help to print the ticket for repair part RMA''',
    'category': 'Inventory, Logistic, Storage',
    'license': 'AGPL-3',
    'summary': 'Add fields and report',
    'author':  'Nexus Incorporation',
    'website': 'nexusgurus.com',
    'depends': ['repair','base','product','helpdesk'],
    'data': [
        'security/ir.model.access.csv',
        'views/tkt_view.xml',
             
              'wizard/support_rep.xml', ],
    'installable': True,
}
