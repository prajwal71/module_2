# -*- coding: utf-8 -*-
import xlwt
import base64
import calendar
from io import StringIO
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning
from datetime import datetime, timedelta
from odoo.http import request
import re
class SupportReporting(models.TransientModel):
    _name = "support.reporting"
    
    invoice_data = fields.Char('Name',)
    file_name = fields.Binary('Support Excel Report', readonly=True)
    state = fields.Selection([('choose', 'choose'), ('get', 'get')],
                             default='choose')
    team_id = fields.Many2one('helpdesk.team','Category')
    # p_id = fields.Many2one('hr.employee','Employee')
    date_from = fields.Datetime('Start Date')
    date_to = fields.Datetime('End date')

    
    def action_support_report(self):
        mon_name = datetime.strptime(str(self.date_from), '%Y-%m-%d %H:%M:%S')
        mon_name = mon_name.strftime("%B")
        m_date_from = str(self.date_from)
        m_date_to = str(self.date_to)
        workbook = xlwt.Workbook()
        format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour yellow;align: horiz center')
        format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
        # format2 = xlwt.easyxf('font:bold True;align: horiz left')
        format3 = xlwt.easyxf('align: horiz left')
        # format4 = xlwt.easyxf('align: horiz right')
        # format5 = xlwt.easyxf('font:bold True;align: horiz right')
        # format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz right')
        # format7 = xlwt.easyxf('font:bold True;borders:top thick;align: horiz right')
        # format8 = xlwt.easyxf('font:bold True;borders:top thick;pattern: pattern solid, fore_colour gray25;align: horiz left')
##        invoice_number = rec.number.split('/')
        sheet = workbook.add_sheet("Support_report",cell_overwrite_ok=True)
        sheet.col(0).width = int(30*260)
        sheet.col(1).width = int(30*260)    
        sheet.col(2).width = int(18*260)    
        sheet.col(3).width = int(18*260) 
        sheet.col(4).width = int(33*260)   
        sheet.col(5).width = int(15*260)
        sheet.col(6).width = int(18*260)   
        sheet.write_merge(0, 1, 0, 1, 'Support Report',format0)
        sheet.write(2, 0, str("Month" +" : "+ mon_name), format1)
        sheet.write(3, 0, "S.No", format1)
        sheet.write(3, 1, "Call Date", format1)
        sheet.write(3, 2, "Model#", format1)
        sheet.write(3, 3, "Serial#", format1)
        sheet.write(3, 4, "Customer", format1)
        sheet.write(3, 5, "Customer Contact", format1)
        sheet.write(3, 7, "Location", format1)
        sheet.write(3, 6, "Sold By", format1)
        sheet.write(3, 8, "On Site/Workshop", format1)
        sheet.write(3, 9, "Problem Description", format1)
        sheet.write(3, 10, "Warranty", format1)
        sheet.write(3, 11,"Parts Replaced", format1)
        sheet.write(3, 12, "Qty", format1)
        sheet.write(3, 13, "Part Description", format1)
        sheet.write(3, 14, "Part No", format1)
        sheet.write(3, 15, "Description of work Done", format1)
        sheet.write(3, 16, "Technician", format1)
        sheet.write(3, 17, "Date Call Completed", format1)
        
       
        # self.env.cr.execute('select du.name,da.device_datetime,da.device_punch from device_attendances da inner join device_users du on da.device_user_id = du.id where device_datetime>=%s and device_datetime<=%s  order by da.device_user_id', (date_from,date_to))
        # my_data3 = self.env.cr.dictfetchall()
        my_data3 = []
        support_obj = self.env['helpdesk.ticket'].search([('create_date', '>=', m_date_from),('create_date', '<=', m_date_to),('team_id', '=', self.team_id.id)])
        for datas in support_obj:
            my_data3.append(datas)
        row = 4
        sno= 1

        def cleanhtml(raw_html):
            cleanr = re.compile('<.*?>')
            cleantext = re.sub(cleanr, '', raw_html)
            return cleantext
        
        for data in my_data3:
            # d1 = datetime.strptime(data.device_datetime , '%Y-%m-%d %H:%M:%S')
            # data.device_datetime = str(d1 + timedelta(hours=5,minutes=44))
            # if data.device_punch ==  0:
            #     data.punch_state = 'Check In'
            # elif data.device_punch == 1:
            #     data.punch_state = 'Check Out'
            #  sheet.write_merge(0, 1, 0, 4, 'Employee : ' + data['employee'] , format0)
            sheet.write(row, 0, str(sno), format3)
            dt_obj = datetime.strptime(str(data.create_date), '%Y-%m-%d %H:%M:%S.%f')
            dt_obj = dt_obj.date()
            sheet.write(row, 1, str(dt_obj), format3) 
            if data.product_id:
                sheet.write(row, 2, str(data.product_id.default_code), format3) 
           
            if data.lot_id:
                sheet.write(row, 3, str(data.lot_id.name), format3)

            if data.partner_id:
                sheet.write(row, 4, str(data.partner_id.name), format3)
                tmep_cont = str(data.partner_id.phone) + " " + str(data.partner_id.mobile)           
                sheet.write(row, 5, str(tmep_cont), format3)
                tmep_addr =  str(data.partner_id.street) +" "+ str(data.partner_id.city)
                sheet.write(row, 7, " ", format3)
            
            # for lines in data.extra_field_ids:
            #     if lines.name == 'Purchase From':
            #         if lines.value != " ":
            #             sheet.write(row, 6, (lines.value), format3)
            #         else:
            if data.sold_by:
                sheet.write(row, 6, str(data.sold_by.name), format3)
            else:
                sheet.write(row, 6, str("Sagar Distribution Pvt. Ltd."), format3)
            if data.x_studio_support_type:
                sheet.write(row, 8, str(data.x_studio_support_type), format3)
            if data.description:
                f_desc = cleanhtml(data.description)
                sheet.write(row, 9, str(data.name), format3)
            if data.warranty == False:
                sheet.write(row, 10, str("No"), format3)
            else:
               sheet.write(row, 10, str(data.warranty), format3)
            if data.parts_changed == False:
                sheet.write(row, 11, ("No"), format3)
            else:
                sheet.write(row, 11, str(data.parts_changed), format3)
            if data.ref_to_parts:
                aq = []
                an = []
                adc = []
                for lines in data.ref_to_parts:
                    aq.append(lines.product_uom_qty)
                    an.append(lines.product_id.name)
                    adc.append(lines.product_id.default_code)
                saq = ' / '.join(str(v) for v in aq)
                san = ' / '.join(str(an))
                sadc =' / '.join(str(adc))
                sheet.write(row, 12, str(saq), format3)
                sheet.write(row, 13, str(san), format3)
                sheet.write(row, 14, str(sadc), format3)
            if data.close_comment:
                c_desc = cleanhtml(data.close_comment)
                sheet.write(row, 15, str(c_desc), format3)
            if data.user_id:
                sheet.write(row, 16, str(data.user_id.name), format3)
            if data.close_date:
                sheet.write(row, 17, str(data.close_date), format3)
            sno = sno+1
            row = row+1
            
        path = ("/home/odoo/Reports/Monthly_Support_Report.xls")
        # path = ("/home/odoo/reports/Monthly_Support_Report.xls")
        #filename = ('Deadline report'+ '.xls')
        workbook.save(path)
        file = open(path, "rb")
        file_data = file.read()
        out = base64.encodestring(file_data)
        self.write({'state': 'get', 'file_name': out, 'invoice_data':'Monthly_Support_Report.xls'})
        return {
               'type': 'ir.actions.act_window',
               'res_model': 'support.reporting',
               'view_mode': 'form',
               'view_type': 'form',
               'res_id': self.id,
               'target': 'new',
              
            }                      
        
        #else:
         #   raise Warning("Currently No Invoice/Bills For This Data!!")
    
   
    



                  

