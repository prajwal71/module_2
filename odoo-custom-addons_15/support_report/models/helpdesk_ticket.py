from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.tools import config, float_compare
from odoo.exceptions import UserError, ValidationError, Warning
from odoo.addons import decimal_precision as dp
from datetime import datetime,date
class StockQuant(models.Model):
    _inherit = 'helpdesk.ticket'

    parts_changed = fields.Selection(string="Parts Changed",selection=[('yes', 'Yes'),('no', 'No'),])
    warranty = fields.Selection(string="Warranty",selection=[('yes', 'Yes'),('no', 'No')])
    sold_by = fields.Many2one('res.company',string="Sold By")
    # supp_type = fields.Selection(string="Support Type",selection=[('video', 'Video Call'),('pc', 'Phone Call'),('chat', 'Chat'),('email','Email'),('onsite', 'Onsite/Field Visit'),('carry', 'Carryin/Workshop'),('rm', 'Remote access')])
    incentive = fields.Integer("Incentive")
    close_date = fields.Datetime("Close Date")
    close_comment = fields.Html("Close Comment")
    ref_to_parts = fields.One2many('support.parts.line','parts_repair_id', string='rep')
    
    

class SupportPartsLine(models.Model):
    _name = 'support.parts.line'
    _description = 'Support Parts Line'

    name = fields.Char('Part Description')
    parts_repair_id = fields.Many2one(
        'helpdesk.ticket', 'Support Ticket Reference',
        index=True, ondelete='cascade')
    part_no = fields.Char('Part No.')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    
    product_uom_qty = fields.Float(
        'Quantity', default=1.0,
        digits=dp.get_precision('Product Unit of Measure'), required=True)
    
    @api.onchange('product_id')
    def onchange_mysupp_product_id(self):
        """ On change of product it sets desc. """
        if self.product_id:
            self.name = self.product_id.display_name
            self.part_no = self.product_id.default_code
            
        