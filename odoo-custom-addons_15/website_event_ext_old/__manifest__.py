# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Event Ext',
    'version' : '1.1',
    'summary': 'Field added in event and website event module',
    'description': """

====================

    """,
    'author': 'Nexus',
    'depends' : ['base','web','website_event','event'],
    # 'assets': {
    #     'web.assets_frontend': [('website_event_ext/static/src/js/custom.js')]
    # },
    'data': [
        'views/view.xml',
             'views/template.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
