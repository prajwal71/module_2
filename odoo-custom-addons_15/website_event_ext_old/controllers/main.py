from odoo import http, _
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from dateutil.relativedelta import relativedelta
import base64
import logging
import json
_logger = logging.getLogger(__name__)
# import yaml
import werkzeug
from odoo.http import request
from odoo.addons.website_event.controllers.main import WebsiteEventController

class WebsiteEventControllerExt(WebsiteEventController):
    
    @http.route(['''/event/<model("event.event"):event>/registration/confirm'''], type='http', auth="public", methods=['POST'], website=True)
    def registration_confirm(self, event, **post):
        _logger.info("========attendees_sudo======%s============",post)
        if post.get('attachment',False):
                    Attachments = request.env['ir.attachment']
                    name =post.get('attachment').filename      
                    files = post.get('attachment').stream.read()
                    attachment_id = Attachments.sudo().create({
                        'name':name,
                        'type': 'binary',   
                        'res_model': 'event.registration',
                    
                        'datas': base64.encodestring(files),
                    }) 
                    
        
        # if post.get('manager',False):
        #             Attachments = request.env['ir.attachment']
        #             name = post.get('manager').filename      
        #             files = post.get('manager').stream.read()
        #             attachment_id = Attachments.sudo().create({
        #                 'name':name,
        #                 'type': 'binary',   
        #                 'res_model': 'event.registration',
        #                 'res_id':1,
        #                 'datas': base64.encodebytes(files),
        #             }) 
        registrations = self._process_attendees_form(event, post)
        attendees_sudo = self._create_attendees_from_registration_post(event, registrations)
        attach = self.create_attach(attendees_sudo,post)

        return request.redirect(('/event/%s/registration/success?' % event.id) + werkzeug.urls.url_encode({'registration_ids': ",".join([str(id) for id in attendees_sudo.ids])}))
        # return request.render('website_event_ext.attach', {})
   
    # @http.route("/attachment", type='http', auth="public", methods=['POST'], website=True)
    def create_attach(self,attendees_sudo,**post):
        # _logger.info('===========%s======self',manager)
        if attendees_sudo:
            _logger.info("========attendees_sudo======%s============",post)
            if post.get('pl1',False):
                    Attachments = request.env['ir.attachment']
                    name = post.get('pl1').filename      
                    files = post.get('pl1').stream.read()
                    w_attachment_id = Attachments.sudo().create({
                        'name':name,
                        'type': 'binary',   
                        'res_model': 'event.registration',
                        # 'res_id':attendees_sudo.id,
                        'datas': base64.encodestring(files),
                    }) 
            if post.get('pl2',False):
                    Attachments = request.env['ir.attachment']
                    name = post.get('pl2').filename      
                    files = post.get('pl2').stream.read()
                    w_attachment_id = Attachments.sudo().create({
                        'name':name,
                        'type': 'binary',   
                        'res_model': 'event.registration',
                        # 'res_id':attendees_sudo.id,
                        'datas': base64.encodestring(files),
                    }) 

            
            if post.get('pl3',False):
                    Attachments = request.env['ir.attachment']
                    name = post.get('pl3').filename      
                    files = post.get('pl3').stream.read()
                    w_attachment_id = Attachments.sudo().create({
                        'name':name,
                        'type': 'binary',   
                        'res_model': 'event.registration',
                        # 'res_id':attendees_sudo.id,
                        'datas': base64.encodestring(files),
                    }) 
                
            if post.get('pl4',False):
                    Attachments = request.env['ir.attachment']
                    name = post.get('pl4').filename      
                    files = post.get('pl4').stream.read()
                    w_attachment_id = Attachments.sudo().create({
                        'name':name,
                        'type': 'binary',   
                        'res_model': 'event.registration',
                        # 'res_id':attendees_sudo.id,
                        'datas': base64.encodestring(files),
                    }) 

            if post.get('pl5',False):
                    Attachments = request.env['ir.attachment']
                    name = post.get('pl5').filename      
                    files = post.get('pl5').stream.read()
                    w_attachment_id = Attachments.sudo().create({
                        'name':name,
                        'type': 'binary',   
                        'res_model': 'event.registration',
                        # 'res_id':attendees_sudo.id,
                        'datas': base64.encodestring(files),
                    }) 

            if post.get('manager',False):
                    Attachments = request.env['ir.attachment']
                    name =post.get('manager').filename      
                    files = post.get('manager').stream.read()
                    attachment_id = Attachments.sudo().create({
                        'name':name,
                        'type': 'binary',   
                        'res_model': 'event.registration',
                    
                        'datas': base64.encodestring(files),
                    }) 
                    _logger.info("=========================%s=====w_attachment_id=====",attachment_id)
        return True