# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.

from email.policy import default
from odoo import fields, models,api,_
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta
import logging
_logger = logging.getLogger(__name__)


# class EventExt(models.Model):
#     _inherit = 'event.event'

    
    

class EventRegistration(models.Model):
    _inherit = 'event.registration'

    team_name = fields.Char(string='Team Name')
    manager_name = fields.Char(string='Manager Name')
    pl1_name = fields.Char(string='Player1 Name')
    pl2_name = fields.Char(string='Player2 Name')
    pl3_name = fields.Char(string='Player3 Name')
    pl4_name = fields.Char(string='Player4 Name')
    pl5_name = fields.Char(string='Player5 Name')
    manager_contact_no = fields.Char(string='Contact Number')
    manager_email = fields.Char(string='Manager Email')

    manager_image = fields.Binary(string="Citizenship Image")
    pl1_image = fields.Binary(string="Citizenship Image")
    pl2_image = fields.Binary(string="Citizenship Image")
    pl3_image = fields.Binary(string="Citizenship Image")
    pl4_image = fields.Binary(string="Citizenship Image")
    pl5_image = fields.Binary(string="Citizenship Image")

    pl1_g_name = fields.Char(string='Game Name')
    pl2_g_name = fields.Char(string='Game Name')
    pl3_g_name = fields.Char(string='Game Name')
    pl4_g_name = fields.Char(string='Game Name')
    pl5_g_name = fields.Char(string='Game Name')
    

    # visitor_id = fields.Many2one('website.visitor', string='Visitor', ondelete='set null')

    def _get_website_registration_allowed_fields(self):
        return {'name', 'manager_name','pl1_name','pl2_name','pl3_name','pl4_name','pl5_name',
        'manager_contact_no','manager_email',
        'pl1_g_name','pl2_g_name','pl3_g_name','pl4_g_name','pl5_g_name',
        'team_name','phone', 'email', 'mobile', 'event_id', 'partner_id', 'event_ticket_id'}
    
