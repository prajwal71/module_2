odoo.define('nexdocuments.DocumentsListView', function (require) {
"use strict";

/**
 * This file defines the DocumentsListView, a JS extension of the ListView to
 * deal with documents.
 *
 * Warning: there is no groupby menu in this view as it doesn't support the
 * grouped case. Its elements assume that the data isn't grouped.
 */

const DocumentsListController = require('nexdocuments.DocumentsListController');
const DocumentsListModel = require('nexdocuments.DocumentsListModel');
const DocumentsListRenderer = require('nexdocuments.DocumentsListRenderer');
const DocumentsSearchPanel = require('nexdocuments.DocumentsSearchPanel');
const DocumentsView = require('nexdocuments.viewMixin');

const ListView = require('web.ListView');
const viewRegistry = require('web.view_registry');

const DocumentsListView = ListView.extend(DocumentsView, {
    config: Object.assign({}, ListView.prototype.config, {
        Controller: DocumentsListController,
        Model: DocumentsListModel,
        Renderer: DocumentsListRenderer,
        SearchPanel: DocumentsSearchPanel,
    }),
    searchMenuTypes: ['filter', 'favorite'],
});

viewRegistry.add('documents_list', DocumentsListView);

return DocumentsListView;

});
