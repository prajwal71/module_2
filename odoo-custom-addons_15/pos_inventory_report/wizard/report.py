# -*- coding: utf-8 -*-
import xlwt
import base64
import calendar
from io import StringIO
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning
from datetime import datetime

import logging
_logger = logging.getLogger(__name__)



class LandedCostConsignmentReport(models.TransientModel):
    _name = "sale.pos.report"

    start_date = fields.Date(string='Start Date', required=True, default=datetime.today().replace(day=1))
    end_date = fields.Date(string="End Date", required=True, default=datetime.now().replace(day = calendar.monthrange(datetime.now().year, datetime.now().month)[1]))
    file_name = fields.Binary('Sale POS Excel Report', readonly=True)

    sale_data=fields.Char('Name')

    def action_sale_download(self):
        workbook = xlwt.Workbook()
        
        ldata = self.env['sale.order'].search([('date_order', '>=', self.start_date), ('date_order', '<=', self.end_date)])
        # _logger.info('=================%s', ldata)
    
        sheet = workbook.add_sheet('Sale Order',cell_overwrite_ok=True)
        sheet.col(0).width = int(15*260)
        sheet.col(1).width = int(15*260)    
        sheet.col(2).width = int(30*260)    
        sheet.col(3).width = int(15*260)

        format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour gray25;align: horiz center')
        format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
        format11 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour yellow;align: horiz left')

        format2 = xlwt.easyxf('font:bold True;align: horiz left')
        format3 = xlwt.easyxf('align: horiz left')
        format4 = xlwt.easyxf('align: horiz right')
        format5 = xlwt.easyxf('font:bold True;align: horiz right')
        format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz right')
        format7 = xlwt.easyxf('font:bold True;borders:top thick;align: horiz right')
        format8 = xlwt.easyxf('font:bold True;borders:top thick;pattern: pattern solid, fore_colour gray25;align: horiz left')

        sheet.write_merge(0, 2, 0, 9, 'sale order Report' , format0)
      

        record = []
        if ldata:
            for rec in ldata:
                record.append(rec)
              
            file = StringIO()        
            # self.record= record.sort(key=lambda p: (p.date_order, p.name))
            row=5            
            sheet.write(4, 0, 'Order Date', format1)
            sheet.write(4, 1, 'Sale order NAME', format1)
            sheet.write(4, 2, 'Parnter', format1)
            sheet.write(4, 3, 'Total', format1)
            if record:
                for rec in record:
                    sheet.write(row, 0, str(rec.date_order), format3)
                    sheet.write(row, 1, str(rec.name), format3)
                    sheet.write(row, 2, str(rec.partner_id.name), format3)
                    sheet.write(row, 3, str(rec.amount_total), format3)
                    row+=1
            
            path = ("/home/odoo/Reports/sale_pos.xls")
            workbook.save(path)
            file = open(path, "rb")
            file_data = file.read()
            out = base64.encodestring(file_data)
            self.write({'file_name': out, 'sale_data':'sale_pos.xls'})
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'sale.pos.report',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': self.id,
                'target': 'new',
                }                
        else:
            raise Warning("Currently No LandedCost For This Data!!")


