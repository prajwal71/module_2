from odoo import models, api, fields, http, _
import socket 
from odoo.http import request

class new_user(models.Model):
	_name = "user.confirmation"
	_description = 'User Confirmation'

	name = fields.Char(string="Customer Name")
	user_email = fields.Char(string="Email")
	user_ip = fields.Char(string="IP Address")
	user_city = fields.Char(string="City")
	user_country = fields.Many2one('res.country', string="Country")
	user_confirm_state = fields.Selection([('not_confirm','Not Confirm'),('confirm','Confirm')], default="not_confirm")

	def user_confirm(self):
		self.write({'user_confirm_state':'confirm'})
		user_bool = self.env['res.users'].search([('login','=',self.user_email)])

		for user in user_bool:
			user.write({'user_boolean':True})

		ir_model_data = self.env['ir.model.data']
		template_id = ir_model_data._xmlid_to_res_id('approval_user_app.email_approved_mail')

		mail_obj = self.env['mail.mail']
		email_template = self.env['mail.template'].browse(template_id)

		value = email_template.generate_email(self.id,['subject', 'body_html', 'email_from', 'email_to', 'partner_to', 'email_cc', 'reply_to', 'scheduled_date'])
		value['subject'] = "Email Verification"
		
		mail = mail_obj.sudo().create(value)
		if mail:
			mail_obj.sudo().send([mail])
			mail.sudo().send()

	def user_not_confirm(self):

		self.write({'user_confirm_state':'not_confirm'})

		user_bool = self.env['res.users'].search([('login','=',self.user_email)])

		for user in user_bool:
			user.write({'user_boolean':False})

		ir_model_data = self.env['ir.model.data']
		template_id = ir_model_data._xmlid_to_res_id('approval_user_app.email_declined_mail')

		mail_obj = self.env['mail.mail']
		email_template = self.env['mail.template'].browse(template_id)

		value = email_template.generate_email(self.id,['subject', 'body_html', 'email_from', 'email_to', 'partner_to', 'email_cc', 'reply_to', 'scheduled_date'])
		value['subject'] = "Email Verification"
		mail = mail_obj.sudo().create(value)
		if mail:
			mail_obj.sudo().send([mail])
			mail.sudo().send()


class signup(models.Model):

	_inherit = "res.users"

	user_boolean = fields.Boolean(default=False)

	@api.model
	def create(self, vals):
		res = super(signup, self).create(vals)
		user_confirm = self.env['user.confirmation']
		hostname = socket.gethostname()
		IPAddr = socket.gethostbyname(hostname)

		user_confirm.create({'name':res.name,
						   'user_email':res.login,
						   'user_ip':IPAddr,
					  		})
		return res

