# -*- coding: utf-8 -*-

{
	'name' : 'Sign-Up User Approval in Odoo',
	'author': "Edge Technologies",
	'version' : '15.0.1.0',
	'live_test_url':'https://youtu.be/dr8n-Hf3z6A',
	'images':['static/description/main_screenshot.png'],
	'category' : 'Website',
	'summary' : 'User validation approval process user approval validation process user two stage approval website user approval website register approval website sign up approval Approved user signup approval two step users approval website user sign up approval website',
	'description' : """

	This module contains approval of new user after sign up.
Odooo approve user approve
Approved user signup approval register user approval regiter approve user
user two stage apporval website user approval website register approval
website sign up apporval website sign-up approval for website sign up approval for website
website user sign up apporval website user sign-up approval for website user sign up approval for website
website sign up user apporval website sign-up user approval for website sign up user approval for website
website signup user approval for website signup approval for website
two step users approval workflow website user validate website user approval flow
Odoo user approval user sign up validate user approval validate register user
odoo user register approval odoo user sign up approval create user approval 
Odoo user validation approval process user approval validation process


	""",
    "license" : "OPL-1",
	'depends' : ['base','website_sale','website'],
	'data' : [
		'security/ir.model.access.csv',
		'views/user_approval.xml',
		'views/template.xml',
	],
	'qweb' : [],
	'demo' : [],
	'installable' : True,
	'auto_install' : False,
	'price': 10,
	'currency': "EUR",
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
