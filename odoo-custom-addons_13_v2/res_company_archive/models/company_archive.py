from odoo import api, fields, models


class archive_company(models.Model):
    _inherit = "res.company"

    active = fields.Boolean(default=True)
