# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import pprint

from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)


class KhaltiController(http.Controller):

    @http.route(['/payment/khalti'], type='http', auth='public', csrf=False)
    def khalti_capture(self, **kwargs):
        payment_id = kwargs.get('payment_id')
        reference = kwargs.get('productIdentity')
        if payment_id:
            response = request.env['payment.transaction'].sudo()._create_khalti_capture(kwargs)
            if response.get('idx'):
                response['reference'] = reference 
                _logger.info('khalti: entering form_feedback with post data %s', pprint.pformat(response))
                request.env['payment.transaction'].sudo().form_feedback(response, 'khalti')
        return '/payment/process'
