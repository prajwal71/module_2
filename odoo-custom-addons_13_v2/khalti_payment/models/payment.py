# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import requests

from odoo import api, fields, models, _
from odoo.tools.float_utils import float_compare, float_repr, float_round
from odoo.addons.payment.models.payment_acquirer import ValidationError

_logger = logging.getLogger(__name__)


class PaymentAcquirer(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('khalti', 'Khalti')])
    # khalti_key_id = fields.Char(string='Key ID', required_if_provider='khalti', groups='base.group_user')
    khalti_key_public = fields.Char(string='Key Public', required_if_provider='khalti', groups='base.group_user')
    khalti_key_secret = fields.Char(string='Key Secret', required_if_provider='khalti', groups='base.group_user')

    def khalti_form_generate_values(self, values):
        self.ensure_one()
        currency = self.env['res.currency'].sudo().browse(values['currency_id'])
        if currency != self.env.ref('base.NPR'):
            raise ValidationError(_('Currency not supported by Khalti'))
        values.update({
            'publicKey': self.khalti_key_public,
            'amount': float_repr(float_round(values.get('amount'), 2) * 100, 0),
            'productName': values.get('partner_name'),
            'productIdentity': values.get('reference'),
            # 'email': values.get('partner_email'),
            # 'order_id': values.get('reference'),
        })
        return values


class PaymentTransaction(models.Model):
    _inherit = 'payment.transaction'

    @api.model
    def _create_khalti_capture(self, data):
        payment_acquirer = self.env['payment.acquirer'].search([('provider', '=', 'khalti')], limit=1)
        payment_url = "https://khalti.com/api/v2/payment/verify/"

        payload = {
                    "token": data.get('token'),
                    "amount": data.get('amount')
                }
        headers = {
        "Authorization": "Key %s" % (payment_acquirer.khalti_key_secret)
        }
        try:
            payment_response = requests.post(payment_url, payload, headers = headers)
            payment_response = payment_response.json()
        except Exception as e:
            raise e
        
        return payment_response

    @api.model
    def _khalti_form_get_tx_from_data(self, data):
        _logger.info(data)
        reference, txn_id = data.get('reference'), data.get('idx')
        if not txn_id:
            error_msg = _('Khalti: received data with missing reference (%s) or txn_id (%s)') % (reference, txn_id)
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        txs = self.env['payment.transaction'].search([('reference', '=', reference)])
        if not txs or len(txs) > 1:
            error_msg = _('Khalti: received data for reference %s') % (reference)
            if not txs:
                error_msg += '; no order found'
            else:
                error_msg += '; multiple order found'
            _logger.info(error_msg)
            raise ValidationError(error_msg)
        return txs[0]

    def _khalti_form_get_invalid_parameters(self, data):
        invalid_parameters = []
        if float_compare(data.get('amount', '0.0') / 100, self.amount, precision_digits=2) != 0:
            invalid_parameters.append(('amount', data.get('amount'), '%.2f' % self.amount))
        return invalid_parameters

    def _khalti_form_validate(self, data):
        status = data.get('state',{}).get('name')
        if status == "Completed":
            _logger.info('Validated Khalti payment for tx %s: set as done' % (self.reference))
            self.write({'acquirer_reference': data.get('idx')})
            self._set_transaction_done()
            return True
        # if status == 'authorized':
        #     _logger.info('Validated Razorpay payment for tx %s: set as authorized' % (self.reference))
        #     self.write({'acquirer_reference': data.get('id')})
        #     self._set_transaction_authorized()
        #     return True
        else:
            error = 'Received unrecognized status for Khalti payment %s: %s, set as error' % (self.reference, status)
            _logger.info(error)
            self.write({'acquirer_reference': data, 'state_message': data})
            self._set_transaction_cancel()
            return False