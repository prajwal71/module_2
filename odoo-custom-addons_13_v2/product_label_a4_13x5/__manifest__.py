# -*- coding: utf-8 -*-
{
    'name': 'Custom Product Labels',
    'version': '12.0.1.0.1',
    'category': 'Product Management',
    'author': 'Nexus',
    'website': "https://sagar.com",
    'license': 'LGPL-3',
    'summary': """Print custom product labels""",
    'images': ['static/description/banner.png'],
    'description': """Module allows to print custom product labels on different paper formats. Label size: 38x21mm, paperformat: A4 (65 pcs per sheet, 5 pcs x 13 rows).""",
    'depends': ['product', 'stock'],
    'data': [
        'wizard/print_product_label_views.xml',
        'report/product_label_templates.xml',
        'report/product_label_reports.xml',
    ],
    'application': False,
    'installable': True,
    'auto_install': False,
}
