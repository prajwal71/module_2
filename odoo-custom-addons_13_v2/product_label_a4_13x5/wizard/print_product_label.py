# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import Warning

import logging
_logger = logging.getLogger(__name__)

class PrintProductLabel(models.TransientModel):
    _name = "print.product.label"

    @api.model
    def _get_products(self):
        res = []
        if self._context.get('active_model') == 'product.template':
            products = self.env[self._context.get('active_model')].browse(self._context.get('default_product_ids'))
            for product in products:
                label = self.env['product.label'].create({
                    'product_id': product.product_variant_id.id,
                    'qty' : product.qty_available
                })
                res.append(label.id)
        elif self._context.get('active_model') == 'product.product':
            products = self.env[self._context.get('active_model')].browse(self._context.get('default_product_ids'))
            for product in products:
                label = self.env['product.label'].create({
                    'product_id': product.id,
                    'qty' : product.qty_available
                })
                res.append(label.id)
        return res

    label_ids = fields.One2many('product.label', 'wizard_id',
        string='Labels for Products', default=_get_products)
    template = fields.Selection([
            ('product_label_a4_13x5.report_product_label_a4_13x5', 'Label 38x21mm (A4: 65 pcs on sheet, 13x5)')
        ], default='product_label_a4_13x5.report_product_label_a4_13x5', string="Label template")
    qty_per_product = fields.Integer('Label quantity per product', default=1)

    @api.multi
    def action_print(self):
        self.ensure_one()
        labels = self.label_ids.filtered('selected').mapped('id')
        if not labels:
            raise Warning(_('Nothing to print, set the quantity of labels in the table.'))
        return self.env.ref(self.template).report_action(labels, data=None) # ['report'].get_action(labels, self.template)

    @api.multi
    def reopen_form(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Custom Product Labels',
            'res_model': self._name,
            'res_id': self.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
        }

    @api.multi
    def action_set_qty(self):
        self.ensure_one()
        self.label_ids.update({'qty': self.qty_per_product})
        return self.reopen_form()

    @api.multi
    def action_restore_initial_qty(self):
        self.ensure_one()
        for label in self.label_ids:
            if label.qty_initial:
                label.update({'qty': label.qty_initial})
        return self.reopen_form()
