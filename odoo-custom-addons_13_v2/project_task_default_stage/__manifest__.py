{
    "name": "Project Task Default Stage",
    "summary": "Default task stage projects",
    "version": "13.0.1.0.0",
    "category": "Project",
    "author": "Nexus Incorporation Pvt Ltd",
    "license": "AGPL-3",
    "depends": ["project"],
    "data": ["views/project_view.xml", "security/ir.model.access.csv"],
    "installable": True,
}
