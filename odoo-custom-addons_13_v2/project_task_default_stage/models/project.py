# Copyright 2015 Incaser Informatica S.L. - Sergio Teruel
# Copyright 2015 Incaser Informatica S.L. - Carlos Dauden
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
from odoo import fields, models,api
import logging
_logger = logging.getLogger(__name__)

class ProjectProject(models.Model):
    _inherit = "project.project"

    project_type_id = fields.Many2one('project.project.type',string='Project Type')

    @api.onchange('project_type_id')
    def _get_default_type_common(self):
        _logger.info("===================first============%s",self.project_type_id.id)
        
        # projecttypeobj = self.env["project.project.type"].search([('id', "=", self.project_type_id.id)])
        # _logger.info("===============================%s",projecttypeobj)
        #  self.project_type_id.project_task_type_ids
        if self.type_ids:
                self.write({
                            'type_ids' : [(6,0,[])]
                        })
        for dec in self.project_type_id.project_task_type_ids:
            
            self.write({
                            'type_ids' : [(4,dec.id)]
                        })            
    type_ids = fields.Many2many()

    



class ProjectProjectType(models.Model):
    _name = "project.project.type"

    name = fields.Char('Type Name')
    project_task_type_ids = fields.Many2many('project.task.type',string="Type Stages")

