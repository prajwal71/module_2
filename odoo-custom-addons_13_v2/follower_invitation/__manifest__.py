# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
{
  "name"                 :  "Follower invitation correction",
  "summary"              :  """IT helps to remove false in follower invitation.""",
  "category"             :  "Website",
  "version"              :  "1.0.2",
  "sequence"             :  1,
  "author"               :  "Nexus Incoporation",
  "license"              :  "Other proprietary",
  "website"              :  "https://nexusgurus.com",
  "description"          :  """Mobile number validation""",
  "depends"              :   ['base','mail'],
  # "data"                 :  [
                             
  #                            'views/number_validation.xml',
  #                           ],
  "application"          :  False,
  "installable"          :  True,
  "auto_install"         :  False,
}
