
from pyexpat import model
from odoo import api, fields, models,_
import logging
_logger = logging.getLogger(__name__)



# class StockMoveCustom(models.Model):

#     _inherit='stock.move'
    
#     barcode = fields.Char(related='product_id.barcode', string="Product Barcode", store=True)

#     def action_show_details(self):
#         """ Returns an action that will open a form view (in a popup) allowing to work on all the
#         move lines of a particular move. This form view is used when "show operations" is not
#         checked on the picking type.
#         """
#         self.ensure_one()

#         # If "show suggestions" is not checked on the picking type, we have to filter out the
#         # reserved move lines. We do this by displaying `move_line_nosuggest_ids`. We use
#         # different views to display one field or another so that the webclient doesn't have to
#         # fetch both.
#         for record in self:
#             record.partner_id = record.picking_id.partner_id.id

#         if self.picking_id.picking_type_id.show_reserved:
#             view = self.env.ref('stock.view_stock_move_operations')
#         else:
#             view = self.env.ref('stock.view_stock_move_nosuggest_operations')

#         picking_type_id = self.picking_type_id or self.picking_id.picking_type_id
#         return {
#             'name': _('Detailed Operations'),
#             'type': 'ir.actions.act_window',
#             'view_mode': 'form',
#             'res_model': 'stock.move',
#             'views': [(view.id, 'form')],
#             'view_id': view.id,
#             'target': 'new',
#             'res_id': self.id,
#             'context': dict(
#                 self.env.context,
#                 show_owner=self.picking_type_id.code != 'incoming',
#                 show_lots_m2o=self.has_tracking != 'none' and (picking_type_id.use_existing_lots or self.state == 'done' or self.origin_returned_move_id.id),  # able to create lots, whatever the value of ` use_create_lots`.
#                 show_lots_text=self.has_tracking != 'none' and picking_type_id.use_create_lots and not picking_type_id.use_existing_lots and self.state != 'done' and not self.origin_returned_move_id.id,
#                 show_source_location=self.picking_type_id.code != 'incoming',
#                 show_destination_location=self.picking_type_id.code != 'outgoing',
#                 show_package=not self.location_id.usage == 'supplier',
#                 show_reserved_quantity=self.state != 'done' and not self.picking_id.immediate_transfer and self.picking_type_id.code != 'incoming'
#             ),
#         }




class StockMoveCustom(models.Model):

    _inherit='stock.move'
    
    customer_address= fields.Many2one('res.partner', compute='getValue', string='Customer Address', index=True, required=True, ondelete='cascade')

    @api.depends('partner_id')
    def getValue(self):
        for data in self:
            data.customer_address=data.partner_id.id


    def action_show_details(self):
        """ Returns an action that will open a form view (in a popup) allowing to work on all the
        move lines of a particular move. This form view is used when "show operations" is not
        checked on the picking type.
        """
        self.ensure_one()

        # If "show suggestions" is not checked on the picking type, we have to filter out the
        # reserved move lines. We do this by displaying `move_line_nosuggest_ids`. We use
        # different views to display one field or another so that the webclient doesn't have to
        # fetch both.
        for record in self:
            record.partner_id = record.picking_id.partner_id.id
            # record.customer_address=record.partner_id.id

            

        # for record in self:
            # record.customer_address = record.picking_id.partner_id.id
        # customer_address=self.partner_id.id

        if self.picking_id.picking_type_id.show_reserved:
            view = self.env.ref('stock.view_stock_move_operations')
        else:
            view = self.env.ref('stock.view_stock_move_nosuggest_operations')

        picking_type_id = self.picking_type_id or self.picking_id.picking_type_id
        return {
            'name': _('Detailed Operations'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'stock.move',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': self.id,
            'context': dict(
                self.env.context,
                show_owner=self.picking_type_id.code != 'incoming',
                show_lots_m2o=self.has_tracking != 'none' and (picking_type_id.use_existing_lots or self.state == 'done' or self.origin_returned_move_id.id),  # able to create lots, whatever the value of ` use_create_lots`.
                show_lots_text=self.has_tracking != 'none' and picking_type_id.use_create_lots and not picking_type_id.use_existing_lots and self.state != 'done' and not self.origin_returned_move_id.id,
                show_source_location=self.picking_type_id.code != 'incoming',
                show_destination_location=self.picking_type_id.code != 'outgoing',
                show_package=not self.location_id.usage == 'supplier',
                show_reserved_quantity=self.state != 'done' and not self.picking_id.immediate_transfer and self.picking_type_id.code != 'incoming'
            ),
        }




class PackageCustom(models.Model):

    _inherit= 'stock.quant.package'    
    customer_address= fields.Many2one('res.partner',  string='Customer Address')

    # @api.depends('quant_ids.package_id', 'quant_ids.location_id', 'quant_ids.company_id', 'quant_ids.owner_id', 'quant_ids.quantity', 'quant_ids.reserved_quantity', 'quant_ids.customer_address')
    # def compute_package_info(self):
    #     for package in self:
    #         values = {'location_id': False, 'owner_id': False}
    #         if package.quant_ids:
    #             values['location_id'] = package.quant_ids[0].location_id
    #             if all(q.owner_id == package.quant_ids[0].owner_id for q in package.quant_ids):
    #                 values['owner_id'] = package.quant_ids[0].owner_id
    #             if all(q.company_id == package.quant_ids[0].company_id for q in package.quant_ids):
    #                 values['company_id'] = package.quant_ids[0].company_id
    #             # if all(q.customer_address == package.quant_ids[0].customer_address for q in package.quant_ids):
    #             #     values['customer_address'] = package.quant_ids[0].customer_address
    #         package.location_id = values['location_id']
    #         package.company_id = values.get('company_id')
    #         package.owner_id = values['owner_id']
            # package.customer_address = values['customer_address']






class CustomTree(models.Model):
    _inherit = 'stock.move.line'

    customer_address=fields.Many2one('res.partner',  string='Customer Address')

class CustomStockQuant(models.Model):
    _inherit = 'stock.quant'

    customer_address=fields.Many2one('res.partner',  string='Customer Address')
    def create(self, vals):
            """ Override to handle the "inventory mode" and create a quant as
            superuser the conditions are met.
            """
            if self._is_inventory_mode() and 'inventory_quantity' in vals:
                allowed_fields = self._get_inventory_fields_create()
                if any([field for field in vals.keys() if field not in allowed_fields]):
                    raise UserError(_("Quant's creation is restricted, you can't do this operation."))
                inventory_quantity = vals.pop('inventory_quantity')

                # Create an empty quant or write on a similar one.
                product = self.env['product.product'].browse(vals['product_id'])
                location = self.env['stock.location'].browse(vals['location_id'])
                lot_id = self.env['stock.production.lot'].browse(vals.get('lot_id'))
                package_id = self.env['stock.quant.package'].browse(vals.get('package_id'))
                owner_id = self.env['res.partner'].browse(vals.get('owner_id'))
                customer_address = self.env['res.partner'].browse(vals.get('customer_address'))

                quant = self._gather(product, location, lot_id=lot_id, package_id=package_id, owner_id=owner_id, customer_address=customer_address,strict=True)
                if quant:
                    quant = quant[0]
                else:
                    quant = self.sudo().create(vals)
                # Set the `inventory_quantity` field to create the necessary move.
                quant.inventory_quantity = inventory_quantity
                return quant
            res = super(CustomStockQuant, self).create(vals)
            if self._is_inventory_mode():
                res._check_company()
            return res
