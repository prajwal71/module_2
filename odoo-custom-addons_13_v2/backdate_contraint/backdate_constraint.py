# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.

from odoo import fields, models,api,_
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta


class AccountInvoiceDate(models.Model):
    _inherit = 'account.move'

    @api.onchange('invoice_date')
    def invoice_date_validate(self):
        for rec in self:
            if rec.invoice_date:
                today = fields.Date.context_today(self)
                previous_day = today - timedelta(days=3)
                previous_day_str = previous_day.strftime('%Y-%m-%d')
                invoice_date_str = rec.invoice_date.strftime('%Y-%m-%d')
                if invoice_date_str < previous_day_str:
                    if not rec.env.user.has_group('backdate_contraint.backdate_group_manager'):
                        raise ValidationError(_('Please select date not more than 3 days prior'))

    @api.onchange('date')
    def accounting_date_validate(self):
        for rec in self:            
            if rec.date:
                today = fields.Date.context_today(self)
                accounting_date_str = rec.date.strftime('%Y-%m-%d')
                accounting_previous_day = today - timedelta(days=3)
                accounting_previous_day_str = accounting_previous_day.strftime('%Y-%m-%d')
                if accounting_date_str < accounting_previous_day_str:
                    if not rec.env.user.has_group('backdate_contraint.backdate_group_manager'):
                        raise ValidationError(_('Please select date not more than 3 days prior'))
                    

class PaymentDateConstraint(models.Model):
    _inherit = 'account.payment'

    @api.onchange('payment_date')
    def accounting_payment_date_validate(self):
        for rec in self:            
            if rec.payment_date:
                today = fields.Date.context_today(self)
                payment_date_str = rec.payment_date.strftime('%Y-%m-%d')
                payment_previous_day = today - timedelta(days=3)
                payment_previous_day_str = payment_previous_day.strftime('%Y-%m-%d')
                if payment_date_str < payment_previous_day_str:
                    if not rec.env.user.has_group('backdate_contraint.backdate_group_manager'):
                        raise ValidationError(_('Please select date not more than 3 days prior'))

