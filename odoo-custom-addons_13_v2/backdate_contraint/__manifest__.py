# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Backdate Contraint',
    'version' : '1.1',
    'summary': 'Restrict for backdate in Invoice and Journal Entries',
    'description': """
Date Constraint in Invoice and Journal Entries
====================
The user would not be able to date prior 2 days.
    """,
    'author': 'Manoj Khadka',
    'depends' : ['account'],
    'data': [
        'security/backdate_security.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
