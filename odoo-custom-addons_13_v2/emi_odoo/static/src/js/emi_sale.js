// odoo.define('emi_odoo.emi_sale', function (require) {
// "use strict";

// 	$(function(){
// 		$('input[name=add_qty]').change(function(){
// 			var quantity = $('input[name=add_qty]').val();
// 			var news = $('#update_qty').val(quantity)
// 		});
// 	});
// });


$(document).ready(function(){
		
		$(".overlay_v13_preloader").fadeOut("slow");
		console.log("all loaded")
  	

	// $('#emimodal').data('pamt',$('.oe_currency_value').val());
	var ax = parseFloat(($('.product_price span[itemprop="price"]').html()));
	// var jx = ($('.product_price span[itemprop="price"]').text());
	console.log(ax);
	$('#emimodal').attr("data-pamt",ax);




	$('#myModal').on('show.bs.modal', function (event) {
	
	var button = $(event.relatedTarget) // Button that triggered the modal
  	var principalAmt = button.data('pamt')
	var modal = $(this)

	  console.log(principalAmt);
	  modal.find('#prncipleAmount').val(principalAmt);
	  modal.find('#downPayment').val(0);
	})

	$('#bank').on('change', function (e) {
		// var optionSelected = $("option:selected", this);
		// var valueSelected = this.value;

		$("#InterestRate").val($("#bank option:selected").val());
		var kk = $("#bank option:selected");
		$("#min-max").text("Minimum Amt : " + kk.data('min') + "  Maximum Amt : " + kk.data('max'));
		var lst = kk.data('term');
		$('#emiMonth').empty();
		$.each(lst, function(index, value){
            $("#emiMonth").append('<option value="'+ value +'">'+ value + " Months" + '</option>');
        });
		document.getElementById("InterestRate").value =7.0;
	});

	$('#emiMonth').on('change', function (e) {

		const mnth=document.getElementById("emiMonth").value;
		if (mnth==6)
		{
			// console.log("i am here")
			// document.getElementById("InterestRate").readOnly = false; 
			document.getElementById("InterestRate").value =7.0;
			// $("#InterestRate").append("Your EMI = Rs. "+formatNumber(7));
			// document.getElementById("InterestRate").readOnly = true; 
			
		}
		if (mnth==12)
		{
	 
			document.getElementById("InterestRate").value =12.0;

			
		}
		if (mnth==8)
		{
		document.getElementById("InterestRate").value =9.0;
			
		}
	
	});

	$('#downPaymentp').on('change', function (e) {

		const dwn=document.getElementById("downPaymentp").value;
		const amt=document.getElementById("prncipleAmount").value;
		if (dwn=='')
		{
			// console.log("i am here")
			// document.getElementById("InterestRate").readOnly = false; 
			
			document.getElementById("downPayment").value =0.0;
			// $("#InterestRate").append("Your EMI = Rs. "+formatNumber(7));
			// document.getElementById("InterestRate").readOnly = true; 
			
		}
		if (dwn==0)
		{
	 
			document.getElementById("downPayment").value =0.0;

			
		}
		if (dwn==20)
		{
			const dwnamt = amt * 0.2;
		document.getElementById("downPayment").value =dwnamt;
			
		}
		if (dwn==30)
		{
			const dwnamt = amt * 0.3;
		document.getElementById("downPayment").value =dwnamt;
			
		}
		if (dwn==40)
		{
			const dwnamt = amt * 0.4;
		document.getElementById("downPayment").value =dwnamt;
			
		}
		if (dwn==50)
		{
			const dwnamt = amt * 0.5;
		document.getElementById("downPayment").value =dwnamt;
			
		}
	
	});

	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
	}
	
	$("#calc").click(function(){
	
		if (($("#prncipleAmount").val() == "") || ($("#prncipleAmount").val() < 0)){
			alert("Please enter appropriate Principal Amount");
		// $('#downPayment').val() = 0;
		return false;
		}

		else if (($("#InterestRate").val() == "") || ($("#InterestRate").val() < 0)){
			alert("Please enter appropriate Interest Rate");
		// $('#downPayment').val() = 0;
		return false;
		}
		
		else if ($("#emiMonth option:selected").val() == ""){
			alert("Please enter term in Month");
		// $('#downPayment').val() = 0;
		return false;
		}
	
		else{
	
	if(parseFloat($("#downPayment").val()) > parseFloat($("#prncipleAmount").val())){
		console.log($("#downPayment").val())
		console.log($("#prncipleAmount").val())
		alert("Principal Amount should be greater than Downpayment");
		// $('#downPayment').val() = 0;
		return false;
	}
	
	else{
		var month = parseFloat($("#emiMonth option:selected").val());
		var rate = parseFloat($("#InterestRate").val());
		var pamt = parseFloat($("#prncipleAmount").val());
		var downPayment = parseFloat($("#downPayment").val());
	
	  if (downPayment > 0 ){
		  pamt = (pamt - downPayment)
	  }
	if (rate > 0){
	   var monthlyInterestRatio = (rate/100)/12;
	   var monthlyInterest = (monthlyInterestRatio*pamt);
		  var top = Math.pow((1+monthlyInterestRatio),month);
			 var bottom = top -1;
			 var sp = top / bottom;
			 var emi = ((pamt * monthlyInterestRatio) * sp);
	   var result = emi.toFixed(2);
	   var totalAmount = (emi*month).toFixed(2);
	   var totalInterest = (totalAmount-pamt).toFixed(2);
	}
	else{
		var emi = ((pamt /month));
	   var result = emi.toFixed(2);
	   var totalAmount = (emi*month).toFixed(2);
	   var totalInterest = (totalAmount-pamt).toFixed(2);
	}
	const mnth1=document.getElementById("emiMonth").value;
	const rt=document.getElementById("InterestRate").value;
		  //alert(emi);
	   $("#result").empty();
	   $("#result").append("Your monthly installment payment is: Rs. "+formatNumber(result) +" for " +mnth1+ " months including "+ rt +"% bank charge");
	   
	   $("#result2").empty();
	   $("#result2").append("Total Interest = Rs. "+formatNumber(totalInterest));
	   
	   $("#result1").empty();
	   $("#result1").append("Total Payable = Rs. "+formatNumber(totalAmount));
	   
		
	}
}
	});

});