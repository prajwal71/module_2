# -*- coding: utf-8 -*-
{
    'name': "controller_concept3",

    'summary': """
        Learn concept about controller""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['website','stock','helpdesk_sale','helpdesk_stock','helpdesk','website_helpdesk_form','website_helpdesk'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/views.xml',
        'views/templates.xml',
        'views/inside_view.xml',
        'views/view.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
