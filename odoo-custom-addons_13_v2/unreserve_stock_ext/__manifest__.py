# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Unreserve Stock',
    'version' : '1.1',
    'summary': 'Restrict for backdate in Invoice and Journal Entries',
    'description': """
Date Constraint in Invoice and Journal Entries
====================
The user would not be able to date prior 2 days.
    """,
    'author': 'Nexus',
    'depends' : ['stock'],
    'data': [
        # 'security/backdate_security.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
