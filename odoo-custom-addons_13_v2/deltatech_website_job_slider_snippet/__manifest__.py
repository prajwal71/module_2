# ©  2015-2020 Deltatech
#              Dorin Hongu <dhongu(@)gmail(.)com
# See README.rst file on addons root folder for license details
{
    "name": "website job Slider",
    "category": "Website",
    "summary": "eCommerce extension",
    "version": "13.0.1.0.0",
    "author": "Terrabit, Dorin Hongu",
    "license": "AGPL-3",
    "website": "https://www.terrabit.ro",
    "depends": ["website_sale", "deltatech_job_list","website_hr_recruitment"],
    "data": ["views/templates.xml", "views/snippets.xml"],
    "images": ["static/description/main_screenshot.png"],
    "installable": True,
    "qweb": ["static/src/xml/*.xml"],
    "development_status": "Mature",
    "maintainers": ["dhongu"],
}
