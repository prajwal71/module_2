odoo.define("deltatech_website_job_slider_snippet.job_slider_editor", function (require) {
    "use strict";

    var core = require("web.core");

    var wUtils = require("website.utils");
    var options = require("web_editor.snippets.options");

    var _t = core._t;

    options.registry.edit_job_list = options.Class.extend({
        select_job_list: function () {
            var self = this;
            return wUtils
                .prompt({
                    id: "editor_job_list_slider",
                    window_title: _t("Select a Job List"),
                    select: _t("Job List"),
                    init: function () {
                        return self._rpc({
                            model: "job.list",
                            method: "name_search",
                            args: ["", []],
                        });
                    },
                })
                .then(function (result) {
                    self.$target.attr("data-id", result.val);
                    // onclick="hello(result)";
                });
                
        },
        onBuilt: function () {
            var self = this;
            this._super();
            this.select_job_list("click").guardedCatch(function () {
                self.getParent().removeSnippet();
            });
        },
        cleanForSave: function () {
            this.$target.addClass("d-none");
        },
    });
});
