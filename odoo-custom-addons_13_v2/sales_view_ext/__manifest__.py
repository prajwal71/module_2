# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'sales View Ext',
    'version' : '1.1',
    'summary': 'It make Rporting and marketing tab visible to all',
    'description': """

====================
The user would not be able to date prior 2 days.
    """,
    'author': 'Nexus',
    'depends' : ['sale'],
    'data': [
        'views/view.xml',
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
