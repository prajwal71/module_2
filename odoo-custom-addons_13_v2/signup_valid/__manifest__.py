# -*- coding: utf-8 -*-
{
    'name': "Signup Validation",

    'summary': """
        This Module helps to verify email before Signup""",

    'description': """
               This Module helps to verify email before Signup""",


    'author': "Nexus Incorporation",
    'website': "http://www.nexusgurus.com",

    'category': 'Website',
    'version': '13.0.0.1',

    'depends': ['website','auth_signup','base','mail'],

    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        # 'views/views.xml',
        'views/templates.xml',
    ],    
}
