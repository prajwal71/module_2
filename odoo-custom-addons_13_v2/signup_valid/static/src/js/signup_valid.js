odoo.define('signup_valid.simplify_signup', function (require) {
    "use strict";
    var ajax = require('web.ajax');

    $(".alert-warning").alert('close');
    $(".alert-danger").alert('close');
    $("#gen_rate").click(function(){
        var input=$('#login');
        var re =  /(^\w.*@\w+\.\w)/;
        var is_email=re.test(input.val());
        console.log('step1');
        if(is_email){
            console.log('email is formatted');
            $.ajax({
                url: "/web/validate-email-unq",
                method: "POST",
                dataType: "json",
                data: { email: input.val()},
                beforeSend: function() {
                    // setting a timeout
                    // $(placeholder).addClass('loading');
                    $("#gen_rate").attr('disabled', 'disabled');
                    $("#gen_rate").prepend('<i id="refrsh" class="fa fa-refresh fa-spin"/>');
                },
                success: function( data ) {
                        console.log(data);
                        if (data['result'] == 'exist'){
                            $(".alert-success").alert('close');
                            $(".alert-warning").alert('close');
                            $('#gen_rate').before('<div class="alert alert-warning">User with this email address already exists. \n Please <a  href="/web/login" class="btn btn-link btn-sm" role="button">Login</a> or <a  href="/web/reset_password" class="btn btn-link btn-sm" role="button">Reset Your Password</a></div>');
                        }
                        else if  (data['result'] == 'generated'){
                            $(".alert-warning").alert('close');
                            $(".alert-success").alert('close');
                            $(".alert-warning").alert('close');
                            $('#gen_rate').after('<div class="alert alert-success">A verification code has been sent to your email.</div>');
                            
                        

                            $('#f_vcode').show();
                            $('#login').attr('readonly', true);
                            $('#chg_eml').show();
                            $('#gen_rate').hide();
                            $('#ver_ify').show();
                        }
                        else{
                            $(".alert-success").alert('close');
                            $(".alert-warning").alert('close');
                            $('#gen_rate').before('<div class="alert alert-warning">An error occured.</div>');
                        }
                },
                error: function (error) {
                   alert('error: ' + error);
                },
                complete: function() {
                    $('#gen_rate').removeAttr("disabled");
                    $('#refrsh').remove();
                },
                }); 
        }
        
        else{
             $(".alert-success").alert('close');
                  $(".alert-warning").alert('close');
            $('#gen_rate').before('<div class="alert alert-warning">Please enter a valid email address.</div>');
        }
        
    }); 
    // This trigger generate button and verify button respectively when pressed Enter Key
    var enterkey_generateBtn = document.getElementById("login");
    enterkey_generateBtn.addEventListener("keyup", function(event) {    
    if (event.keyCode === 13) {       
        event.preventDefault();        
        document.getElementById("gen_rate").click();
         }
    });

    var enterkey_verifyCode = document.getElementById("vcode");
    enterkey_verifyCode.addEventListener("keyup", function(event) {    
    if (event.keyCode === 13) {       
        event.preventDefault();        
        document.getElementById("ver_ify").click();
         }
    });

    
    $("#ver_ify").click(function(){
        var input_1=$('#vcode').val().split(",").join("");
        console.log(input_1);
        if (input_1 == null || input_1 ==''){
            $(".alert-success").alert('close');
            $(".alert-warning").alert('close');
            $('#ver_ify').before('<div class="alert alert-warning">Verification code does not match.</div>');
        }
        else{
        $.ajax({
            url: "/web/validate-otp",
            method: "POST",
            dataType: "json",
            data: { email: $('#login').val(),
                    vcode : input_1,
            },
            beforeSend: function() {
                // setting a timeout
                // $(placeholder).addClass('loading');
                $("#ver_ify").attr('disabled', 'disabled');
                $("#ver_ify").prepend('<i id="refrsh" class="fa fa-refresh fa-spin"/> ');
            },
            success: function( data ) {
                console.log(data);
                    if (data['result'] == 'verified'){
                    $(".alert-success").alert('close');
                    $(".alert-warning").alert('close');
                    $('#vcode').attr('readonly', true);
                    $('#k1').show();
                    $('#ver_ify').hide();
                    $("#sgn_up").show();
                    }

                    else if  (data['result'] == 'notverified'){
                        $(".alert-success").alert('close');
                        $(".alert-warning").alert('close');
                        $('#ver_ify').before('<div class="alert alert-warning">Verification code does not match.</div>');
                    }
                    else{
                        $(".alert-success").alert('close');
                        $(".alert-warning").alert('close');
                        $('#ver_ify').before('<div class="alert alert-warning">An error occured.</div>');
                    }
            },
            error: function (error) {
               alert('error: ' + error);
            },
            complete: function() {
                $('#ver_ify').removeAttr("disabled");
                $('#refrsh').remove();
            },
            }); 
        }
    }); 

});

