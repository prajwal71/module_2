# -*- coding: utf-8 -*-
{
    "name": "EHCS WYSIWYG Editor",
    'summary': """ This module provides a WYSIWYG Editor.""",
    'author': "ERP Harbor Consulting Services",
    'website': "http://www.erpharbor.com",
    'version': '13.0.1.0.0',
    "category": "Web",
    'depends': ['website'],
    "data": [
        'views/web_asset_template.xml',
    ],
    "demo":
        [
            'demo/res_user.xml',
            # 'demo/website_template_demo.xml'
        ],
    'price': 10,
    'currency': 'USD',
}
