# -*- coding: utf-8 -*-
{
    'name': "stock_move_line_ext",

    'summary': """
        """,

    'description': """
        
    """,

    'author': "Nexus Incorporation",
    'website': "http://nexusgurus.com",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base', 'stock'],
    'data': [
        'views/views.xml',
    ],
}
