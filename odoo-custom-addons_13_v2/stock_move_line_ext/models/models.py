# -*- coding: utf-8 -*-

from odoo import models, fields, api


class StockMoveLineExt(models.Model):
    _inherit = 'stock.move.line'

    # bills = fields.Many2many('account.move', 'stock_move_line_id', 'account_move_id', string="Bills", related='picking_id.purchase_id.invoice_ids')
    # sales = fields.Many2many('account.move', 'stock_move_line_id', 'account_move_id', string="Sales", related='picking_id.sale_id.invoice_ids')
    contact = fields.Many2one('res.partner', related='picking_id.partner_id')
