# -*- coding: utf-8 -*-

import re
from odoo import models, api, fields, _
from odoo.exceptions import AccessDenied, AccessError, UserError, ValidationError
from odoo.http import request


class ResUser(models.Model):
	_inherit = "res.users"

	mobile = fields.Char(store=True, related="partner_id.mobile")

	_sql_constraints = [
		('mobile_key', 'UNIQUE (mobile)',  'You can not have two users with the same mobile number !')
	]

	@api.model_create_multi
	def create(self, vals_list):
		users = super(ResUser, self.with_context(default_customer=False)).create(vals_list)
		for user in users:
			if any('mobile' in d for d in vals_list[0]):
				user.mobile = vals_list[0]['mobile']
				user.partner_id.mobile = vals_list[0]['mobile']
		return users

	def reset_password(self, login):
		""" retrieve the user corresponding to login (login or email),
			and reset their password
		"""
		users = self.search(['|',('login', '=', login),('mobile', '=', login)])
		if not users:
			users = self.search(['|',('email', '=', login),('mobile', '=', login)])
		if len(users) != 1:
			raise Exception(_('Reset password: invalid username or email'))
		return users.action_reset_password()

	@api.model
	def _get_login_domain(self, login):
		return  ['|',('login', '=', login),('mobile', '=', login)]
		

class ResPartner(models.Model):
	
	_inherit = "res.partner"

	@api.model
	def signup_retrieve_info(self, token):
		partner = self._signup_retrieve_partner(token, raise_exception=True)
		res = {'db': self.env.cr.dbname}
		if partner.signup_valid:
			res['token'] = token
			res['name'] = partner.name
		if partner.user_ids:
			res['login'] = partner.user_ids[0].login
			res['mobile'] = partner.user_ids[0].partner_id.mobile
		else:
			res['email'] = res['login'] = partner.email or ''
			res['mobile'] = partner.user_ids[0].partner_id.mobile or ''
		return res



class ResPartner(models.Model):
	_inherit = "res.partner"

	mobile = fields.Char(default="+977")
	# mobile = fields.Char(store=True, related="partner_id.mobile")

	_sql_constraints = [
		('mobile_key', 'UNIQUE (mobile)',  'You can not have two users with the same mobile number !')
	]

	@api.onchange('mobile', 'country_id', 'company_id')
	def _onchange_mobile_validation(self):
			a=123

			# if self.mobile:
			#     self.mobile = self.phone_format(self.mobile)s