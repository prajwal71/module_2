$( document ).ready(function() {
  var input = document.querySelector("#mobile");
  // document.getElementsByClassName("iti__flag-container").classList.remove("iti__flag-container");
  // document.getElementsByClassName("iti__flag-container").classList.add("iti__hide");
 
  
  // initialise plugin
  if (input != null){
  var iti =  window.intlTelInput(input, {
   
    
    geoIpLookup: function(callback) {
      $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        
        var countryCode = (resp && resp.country) ? resp.country : "";
        callback(countryCode);
        // var init1=$("#mobile").val().replace(countryData['dialCode'],'');
      // init.replace('+','');
      });
    },
    
    // hiddenInput: "full_number",
    initialCountry: "np",
    // localizedCountries: { 'de': 'Deutschland' },
    // nationalMode: false,
    onlyCountries: [ 'np'],
    // placeholderNumberType: "MOBILE",
    // preferredCountries: ['cn', 'jp'],
    // separateDialCode: true,
    utilsScript: "/mobile_number_signup_app/static/src/intl-tel-input-master/build/js/utils.js",
  });
  
  // function submit() {
  //   var iti = window.intlTelInputGlobals.getInstance(input);
  // var k = iti.isValidNumber();
  // alert(k);
  // }
  
  
  var errorMsg = document.querySelector("#error-msg"),
  validMsg = document.querySelector("#valid-msg");
  
  // here, the index maps to the error code returned from getValidationError - see readme
  var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
  var countryData = iti.getSelectedCountryData();
  // $("#mobile").val("+" + countryData['dialCode'] );

  // input.addEventListener("countrychange", function() {
  //     countryData = iti.getSelectedCountryData();
  //     var k = countryData['iso2'];
  //   if (k == 'us'){
  //     $("#country_id").val(233);
  //     var init=$("#mobile").val().replace(countryData['dialCode'],'');
      
  //     $("#mobile").val("+" + countryData['dialCode'] + init.replace('+',''));
  //   }
  //   else if(k == 'np'){
  //     $("#country_id").val(167);
  //     var init=$("#mobile").val().replace(countryData['dialCode'],'');      
  //     $("#mobile").val("+" + countryData['dialCode'] + init.replace('+',''));
  //   }
  //   console.log(countryData);
  // });
  
  
  var reset = function() {
  input.classList.remove("error");
  errorMsg.innerHTML = "";
  errorMsg.classList.add("hide");
  validMsg.classList.add("hide");
  };
  
  // on blur: validate
  input.addEventListener('blur', function() {
  reset();
  if (input.value.trim()) {
  if (iti.isValidNumber()) {
    validMsg.classList.remove("hide");
    
    document.getElementById("sgn_up").disabled = false;
  } else {
    input.classList.add("error");
    
    document.getElementById("sgn_up").disabled = true;
    var errorCode = iti.getValidationError();
    console.log('err');
    console.log(errorCode);
    if (errorCode < 0 || errorCode > 4)
    {
      errorCode = 0;
    }
    errorMsg.innerHTML = errorMap[errorCode];
    errorMsg.classList.remove("hide");
   
  
  
  }
  }
  });
  input.addEventListener('mouseout', function() {
    reset();
    if (input.value.trim()) {
    if (iti.isValidNumber()) {
      validMsg.classList.remove("hide");
      
      document.getElementById("sgn_up").disabled = false;

    } else {
      input.classList.add("error");
      
      document.getElementById("sgn_up").disabled = true;
      var errorCode = iti.getValidationError();
      console.log('err');
      console.log(errorCode);
      if (errorCode < 0 || errorCode > 4)
      {
        errorCode = 0;
      }
      errorMsg.innerHTML = errorMap[errorCode];
      errorMsg.classList.remove("hide");
     
    
    
    }
    }
    });
  
  
  // on keyup / change flag: reset
  input.addEventListener('change', reset);
  input.addEventListener('keyup', reset);
  }
  });



 

