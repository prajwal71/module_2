# Copyright 2019 O4SB - Graeme Gellatly
# Copyright 2019 Tecnativa - Ernesto Tejeda
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
import re

from lxml import html as htmltree

from odoo import _, api, models
import logging
_logger = logging.getLogger(__name__)


class MailTemplate(models.Model):
    _inherit = "mail.template"

    @api.model
    def _debrand_body(self, html):
        _logger.info("==========eeeeeeeeeeeeeeeeeeeeeee444444444=====q==q============1====================")
        using_word = _("using")
        odoo_word = _("Odoo")
        _logger.info("========================value==========%s",html)
        html = re.sub(using_word + "(.*)[\r\n]*(.*)>" + odoo_word + r"</a>", "", html)
        _logger.info("============after============value==========%s",html)
        powered_by = _("Powered by")
        if powered_by not in html:
            return html
        root = htmltree.fromstring(html)
        powered_by_elements = root.xpath("//*[text()[contains(.,'%s')]]" % powered_by)
        for elem in powered_by_elements:
            # make sure it isn't a spurious powered by
            if any(
                [
                    "www.odoo.com" in child.get("href", "")
                    for child in elem.getchildren()
                ]
            ):
                for child in elem.getchildren():
                    elem.remove(child)
                elem.text = None
        return htmltree.tostring(root).decode("utf-8")

    @api.model
    def render_post_process(self, html):
        _logger.info("=======33333333333333333333========q==q============1====================")
        html = super().render_post_process(html)
        return self._debrand_body(html)
