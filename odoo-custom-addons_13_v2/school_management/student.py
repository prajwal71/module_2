from odoo import models, fields,api

class SaleOrderOfStudent(models.Model):
    
    _inherit='sale.order'
    student_name2 = fields.Char(string='Student Name')




class SchoolStudent(models.Model):
    _name = 'student'
    _description='Students Record'
    _inherit = ['mail.thread','mail.activity.mixin']
    
    
    name = fields.Char(string='Name',tracking=True, copy=False, required=True)
    student_age = fields.Integer('Age',tracking=True)
    student_father_name = fields.Char('Father Name',tracking=True)
    student_mother_name = fields.Char('Mother Name',tracking=True)
    student_address = fields.Char('Address',tracking=True)
    student_class = fields.Integer('Class',tracking=True)
    student_rank =  fields.Integer('Rank',tracking=True)
    image = fields.Binary('Image',tracking=True)
    phone = fields.Char('Number',tracking=True)
    active= fields.Boolean("Currently Studying", default=True)
    seq_name = fields.Char(string="Student ID", readonly=True, required=True, copy=False, default='New')
    
    @api.model
    def create(self, vals):
        if vals.get('seq_name', 'New') == 'New':
            vals['seq_name'] = self.env['ir.sequence'].next_by_code(
                'student.sequence') or 'New'
        result = super(SchoolStudent, self).create(vals)
        return result
    
    
    
     

    