from odoo import models, fields,api


class SchoolTeacher(models.Model):
    _name = 'teacher'
    _description='Teacher Record'
    _inherit = ['mail.thread','mail.activity.mixin']
    _rec_name='teacher_name'
    
    teacher_name = fields.Char(string='Name' ,tracking=True, copy=False, required=True)
    teacher_age = fields.Integer('Age',tracking=True)
    class_teacher=fields.Integer('Class Teacher',tracking=True)
    teach_class=fields.Selection([('1','1'),('2','2'),('3','3'),('4','4'),('5','5'),('6','6')],tracking=True)
    image = fields.Binary('Image',tracking=True)
    phone = fields.Char('Number',tracking=True)
    high= fields.Integer('Highest Class',tracking=True, required=True)
    active= fields.Boolean("Currently Teaching", default=True)
    seq_name = fields.Char(string="Teacher ID", readonly=True, required=True, copy=False, default='New')
    fav_student=fields.Many2one("student",string="Fav student",domain="[('student_class','=',class_teacher)]")
    #  currency_id = fields.Many2one(
    #     'res.currency', string='Currency')


    salary=fields.Float(compute="calSalary",string="Salary")
    
    
    @api.depends('high')   #to update instantly
    def calSalary(self):   #to calculated on basis of another value
        for rec in self:
            rec.salary=rec.high*10000

    @api.model
    def create(self, vals):
        if vals.get('seq_name', 'New') == 'New':
            vals['seq_name'] = self.env['ir.sequence'].next_by_code(
                'teacher.sequence') or 'New'
        result = super(SchoolTeacher, self).create(vals)
        return result

    def wiz_open(self):
        return {
                'type':'ir.actions.act_window',
                'res_model':'teacher.class.update.wizard',
                'view_mode':'form',
                'target':'new'

               }
        
    # @api.model
    # def name_get(self):
    #     result = []
    #     for rec in self:
    #         name=student.phone 
    #         result.append((rec.id,name))
    #     return result
    
    
     

    # @api.onchange('phone')
    # def phone_validation(self):
    #     if self.phone:
    #         match = re.match('^[0-9]\d{10}$', self.phone)
    #         if match == None:
    #            raise ValidationError('Invalid')
    # def name_get(self):
    #     result = []
    #     for record in self:
    #         _rec_name=record.phone
    #         result.append(_rec_name)
    #     return result
