from odoo import fields, models, api


class Salewizard(models.TransientModel):
    _name="saleperson.update"
    

    sale_person_update=fields.Many2one('res.users',string="Sale Person")

    # def wiz_open(self):
    #     return {
    #             'type':'ir.actions.act_window',
    #             'res_model':'saleperson.update',
    #             'view_mode':'form',
    #             'view_id':self.env.ref('sale_wizard.sale_person_update_view').id,
    #             'target':'new'

    #            }
    
    
    def sale_person_update_fun(self):
        self.env['sale.order'].browse(self._context.get("active_ids")).update({'user_id':self.sale_person_update})
        data={
            'form':self.read()[0],
        }
        return self.env.ref('school_management.studeent_identity_card2').report_action(self, data=data) 
   