from odoo import fields, models, api

class TeacherClassUpdateWizard(models.TransientModel):
    _name="teacher.class.update.wizard"

    class_update=fields.Integer(string="Class Teacher")


    def classValueUpdate(self):
        self.env['teacher'].browse(self._context.get("active_ids")).update({'class_teacher':self.class_update})
        return True
   