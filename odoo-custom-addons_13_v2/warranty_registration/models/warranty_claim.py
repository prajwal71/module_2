# -*- coding: utf-8 -*-

from itertools import groupby
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
# from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
# from odoo.tools.misc import formatLang
# from odoo.tools import html2plaintext
# import odoo.addons.decimal_precision as dp
class HelpdeskTicketInherit(models.Model):
	_inherit = "helpdesk.ticket"

	warranty_claim_id = fields.Many2one('warranty.claim', 'Claim')
	warranty_id = fields.Many2one('product.warranty',related='warranty_claim_id.warranty_id', string='Related Warranty')
	is_war_tkt = fields.Boolean('Is Warranty', default=False)

class crm_claim(models.Model):
	_name = "warranty.claim"
	_description = "Claim"
	_order = "date desc"
	_inherit = ['mail.thread', 'mail.activity.mixin']

	name = fields.Char('Name',required=True, copy=False, readonly=True, states={'draft': [('readonly', False)]}, index=True, default=lambda self: _('New'))
	ticket_id = fields.Many2one('helpdesk.ticket', 'Ticket#')
	description = fields.Text('Description')
	close_date = fields.Datetime('Closed',related='ticket_id.close_date', readonly=True)
	date = fields.Datetime('Claim Date', index=True,default=fields.datetime.now())
	user_id = fields.Many2one('res.users', related='ticket_id.user_id',string='Responsible', track_visibility='always')
	
	team_id = fields.Many2one('helpdesk.team', related='ticket_id.team_id',string='Sales Team',index=True, )
	company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.company.id,required=True)
	partner_id = fields.Many2one('res.partner',related='warranty_id.partner_id',string='Customer')
	
	state = fields.Selection([('draft','Under Approval'),('accept','Claim Accepted'),('reject','Claim Rejected')], default='draft',copy=False,track_visibility='onchange' ,string='State',readonly=True)
	warranty_id = fields.Many2one('product.warranty','Related Warranty',required=True)
	# stage_id = fields.Many2one ('helpdesk.stage', related='ticket_id.stage_id',string='Stage', track_visibility='onchange', index=True)
	product_serial_id = fields.Many2one('stock.production.lot',related='warranty_id.product_serial_id',string="Serial No", domain="[('product_id', '=', product_id)]")
	product_id = fields.Many2one('product.product',related='product_serial_id.product_id', string='Product', domain="[('under_warranty', '=', True)]", required=True)
	cancel_res = fields.Char('Cancel Reason', track_visibility='onchange',readonly=True)

	def create_ticket(self):
		if self.product_id:
			tkt_obj = self.env['helpdesk.ticket'].create({
				'name': 'Warranty issue from ' + self.name,  
				'partner_id': self.partner_id.id,
				'description': self.description,
				'product_id': self.product_id.id,
				'lot_id': self.product_serial_id.id,
				'team_id':1,
				'is_war_tkt': True,
				'warranty_claim_id': self.id,
				'warranty_id':self.warranty_id.id,
			})
			if tkt_obj:
				self.ticket_id = tkt_obj.id
				self.state = 'accept'
				template = self.env.ref('warranty_registration.email_template_warranty_claim_confirm')
				self.env['mail.template'].browse(template.id).send_mail(self.id)
	
	def send_notif_email_claim(self):
		template = self.env.ref('warranty_registration.email_template_warranty_claim_draft')
		if template:
			self.env['mail.template'].browse(template.id).send_mail(self.id)

	def cancel_claim(self):
		for rec in self:
			if not rec.cancel_res:
				raise ValidationError(_('Please Enter cancel Reason before Canceling.'))
			else:
				rec.write({'state':'reject'})
		template = self.env.ref('warranty_registration.email_template_warranty_claim_cancel')
		self.env['mail.template'].browse(template.id).send_mail(self.id)
		

	@api.model
	def create(self, vals):
		vals['name'] = self.env['ir.sequence'].next_by_code('warranty.claim') or 'New'
		result = super(crm_claim, self).create(vals)
		return result

	def unlink(self):
		if self.filtered(lambda x: x.state in ('draft','accept', 'reject')):
			raise UserError(_('You can not delete a confirmed Warrany. Please Cancel and reset to Draft.'))
		return super(crm_claim, self).unlink()