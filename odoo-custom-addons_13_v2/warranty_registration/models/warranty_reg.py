# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID

class ProductionLot(models.Model):
	_inherit = 'stock.production.lot'
	
	@api.depends('warranty_id')
	def _compute_warranty_count(self):
		for res in self:
			count = 0
			warranty = self.env['product.warranty'].search_count([('product_serial_id','=',res.id)])
			res.update({'warranty_count' :warranty})

	warranty_id = fields.Many2one('product.warranty',string='Warranty')
	warranty_count = fields.Integer(string='Warranty Count',compute="_compute_warranty_count")
	warranty_create_date = fields.Date(related='warranty_id.warranty_create_date',string='Warranty Start Date')
	warranty_end_date = fields.Date(related='warranty_id.warranty_end_date',string='Warranty End Date')

#Warranty Registration Master
class WarrantyDetails(models.Model):
	_name = 'product.warranty'
	_order = 'id desc'
	_description = "Product Warranty"
	_inherit = ['mail.thread', 'mail.activity.mixin']

	def calc_warranty_end_date(self):
		if self.product_id.warranty_period:
			months_w = int(self.product_id.warranty_period)
			date_1= (datetime.strptime(self.warranty_create_date, '%Y-%m-%d')+relativedelta(months =+ months_w))
			self.update({'warranty_end_date':date_1})
		else:
			self.update({'warranty_end_date': False})

	name = fields.Char('Name',required=True, copy=False, readonly=True, states={'draft': [('readonly', False)]}, index=True, default=lambda self: _('New'))
	partner_id = fields.Many2one('res.partner', 'Customer', required=True,track_visibility='onchange',states={'draft': [('readonly', False)]})
	phone = fields.Char('Phone',states={'draft': [('readonly', False)]})
	email = fields.Char('Email',states={'draft': [('readonly', False)]})
	comment = fields.Text('Comment')
	accept1 = fields.Boolean("I accept the Terms and Conditions",required=True,track_visibility='onchange',states={'draft': [('readonly', False)]})
	
	# serial_no = fields.Char('Receipt No.')
	vendor_id = fields.Many2one('res.partner', 'Vendor',track_visibility='onchange')
	warranty_create_date = fields.Date('Warranty Start Date', default=fields.date.today(),track_visibility='onchange',states={'draft': [('readonly', False)]})
	warranty_end_date = fields.Date('Warranty End Date',track_visibility='onchange',states={'draft': [('readonly', False)]})
	purchase_date = fields.Date('Purchase Date',track_visibility='onchange')
	product_serial_id = fields.Many2one('stock.production.lot',"Serial No", required=True,track_visibility='onchange',states={'draft': [('readonly', False)]})
	product_id = fields.Many2one('product.product',related='product_serial_id.product_id',string='Product', domain="[('under_warranty', '=', True)]",readonly=True)

	warranty_type = fields.Selection([('free','Free'),('paid','Paid')], string="Warranty Type", default='free',states={'draft': [('readonly', False)]})
	tags_w = fields.Many2many('warranty.tag', string='Tags')
	warranty_claim_ids = fields.One2many('warranty.claim','warranty_id',string='Claims',track_visibility='onchange')
	state = fields.Selection([('draft','Under Approval'),('in_progress','Under Warranty'),('expired','Expired'),('cancel','Canceled')], default='draft',copy=False,track_visibility='onchange' ,string='State',readonly=True)
	company_id = fields.Many2one('res.company', 'Company',default=lambda self: self.env.company.id,required=True,states={'draft': [('readonly', False)]})
	cancel_res = fields.Char('Cancel Reason', track_visibility='onchange',readonly=True)

	@api.onchange('partner_id')
	def customer_details(self):
		if self.partner_id:
			self.update({'phone': self.partner_id.phone, 'email': self.partner_id.email})

	@api.onchange('product_serial_id')
	def check_warranty_serial(self):
		warranty_obj = self.env['product.warranty'].search([('product_serial_id','=',self.product_serial_id.id)])
		if warranty_obj:
			raise ValidationError(_('You Cannot Create more than one Warranty with same serial No.'))

	def action_confirm(self):
		for rec in self:
			if not rec.warranty_create_date or not rec.warranty_end_date:
				raise ValidationError(_('Please Enter the Start and End date Properly.'))
			elif rec.warranty_create_date > rec.warranty_end_date:
				raise ValidationError(_('Start Date Cannot Be greater than End Date.'))
			else:
				rec.write({'state':'in_progress'})
				rec.product_serial_id.warranty_id = self.id
			if rec.warranty_end_date < date.today():
				rec.write({'state':'expired'})
		template = self.env.ref('warranty_registration.email_template_warranty_registration')
		self.env['mail.template'].browse(template.id).send_mail(self.id)

	def action_cancel(self):
		for rec in self:
			if not rec.cancel_res:
				raise ValidationError(_('Please Enter cancel Reason before Canceling.'))
			else:
				rec.write({'state':'cancel'})
		template = self.env.ref('warranty_registration.email_template_warranty_registration_cancel')
		self.env['mail.template'].browse(template.id).send_mail(self.id)
	
	def action_set_to_draft(self):
		for rec in self:
			rec.write({'state':'draft'})
	
	def send_notif_email_warranty(self):
		template = self.env.ref('warranty_registration.email_template_warranty_registration_draft')
		if template:
			self.env['mail.template'].browse(template.id).send_mail(self.id)
			
	@api.model
	def warranty_expiry_scheduler_queue(self):
		warranty_obj = self.env['product.warranty'].search([("state",'=','in_progress')])
		for scheduler in warranty_obj :
			warranty_end = datetime.strptime(str(scheduler.warranty_end_date), DEFAULT_SERVER_DATE_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT)
			if warranty_end < str(datetime.now().date()):
				scheduler.update({"state":'expired'})
				template = self.env.ref('warranty_registration.email_template_warranty_registration_expire')
				self.env['mail.template'].browse(template.id).send_mail(self.id)

	@api.model
	def create(self, vals):
		warranty_obj = self.env['product.warranty'].search([('product_serial_id','=',vals['product_serial_id'])])
		if warranty_obj:
			raise ValidationError(_('You Cannot Create more than one Warranty with same serial No.'))
		vals['name'] = self.env['ir.sequence'].next_by_code('warranty.serial') or 'New'
		result = super(WarrantyDetails, self).create(vals)
		return result

	def unlink(self):
		if self.filtered(lambda x: x.state in ('in_progress', 'cancel')):
			raise UserError(_('You can not delete a confirmed Warrany. Please Cancel and reset to Draft.'))
		return super(WarrantyDetails, self).unlink()

#Warranty Tags
class WarrantyTag(models.Model):
	_name = 'warranty.tag'
	_rec_name = 'tag_name'
	_description = "Warranty Tag"

	tag_name = fields.Char('Tag Name')
	tag_desc = fields.Char('Description')






# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
