# -*- coding: utf-8 -*-

from odoo import fields, models


class JobList(models.Model):
    _name = "job.list"
    _description = "Job List"

    name = fields.Char(string="Name", required=True)
    jobs_domain = fields.Char(string="Jobs", default=[["website_published", "=", True]])
    active = fields.Boolean(default=True)
    limit = fields.Integer(string="Limit", default=80, required=True)
