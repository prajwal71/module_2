# -*- coding: utf-8 -*-
{
    "name": "Job List",
    "summary": "Define jobs lists",
    "version": "13.0.1.0.0",
    "category": "Recruitment",
    "author": "Nexus Incorporation",
    "license": "AGPL-3",
    "website": "https://nexusgurus.com",
    "depends": ["hr_recruitment"],
    "data": ["views/job_list_view.xml",
             "security/ir.model.access.csv"],
}
