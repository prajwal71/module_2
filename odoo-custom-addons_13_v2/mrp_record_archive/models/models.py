# -*- coding: utf-8 -*-

from odoo import models, fields, api

class WorkOrderExtCustomArchieve(models.Model):
    _inherit = "mrp.workorder"
    
    active = fields.Boolean(default=True)


class BomExtArchieve(models.Model):

    _inherit = 'mrp.production'
    
    active = fields.Boolean(default=True)