# -*- coding: utf-8 -*-
{
    'name': "Nexus Calendar MOM",

    'summary': """
        Nexus Calendar MOM""",


    'author': "Manoj Khadka",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','calendar'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
    ],
   
}
