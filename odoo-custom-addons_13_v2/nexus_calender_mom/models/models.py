# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
import json
import logging
_logger = logging.getLogger(__name__)


class AgendaDescription(models.Model):
    _name = 'calendar.agenda'
    
    name = fields.Char(string="Name")


class AgendaDescriptionLine(models.Model):
    _name = 'calendar.agenda.line'
    _rec_name = 'agenda_id'

    agenda_id = fields.Many2one('calendar.agenda',string='Agenda',required=True)
    agenda_event_id = fields.Many2one('calendar.event',stirng="Event Reference")



    
    
    




class MinutesOfMeeting(models.Model):
    _name = 'minutes.meeting.line'

    name = fields.Char('Description',required=True)
    mom_event_id = fields.Many2one('calendar.event',stirng="Event Reference")
    agenda_id = fields.Many2one('calendar.agenda',string="Agenda")
    code = fields.Char('Code')
    action_by = fields.Many2one('res.users',string="Action By")
    responsible = fields.Many2one('res.users',string="Responsible")
   




    

class CalendarEventInherit(models.Model):
    _inherit = 'calendar.event'

    agenda_line = fields.One2many('calendar.agenda.line','agenda_event_id')
    minutes_meeting_line = fields.One2many('minutes.meeting.line','mom_event_id')

   




    
   
    

    

