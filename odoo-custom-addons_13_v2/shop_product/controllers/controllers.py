from odoo import http,tools,_
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale

import logging
_logger = logging.getLogger(__name__)




class WebsiteSaleInherit(WebsiteSale):
    @http.route([
        '''/shop''',
        '''/shop/page/<int:page>''',
        '''/shop/category/<model("product.public.category"):category>''',
        '''/shop/category/<model("product.public.category"):category>/page/<int:page>'''
    ], type='http', auth="public", website=True)
    def shop(self, page=0, category=None, search='', ppg=False, **post):
       res=super(WebsiteSaleInherit,self).shop(page=0, category=None, search='', ppg=False, **post)
       productsc=request.env['product.product'].sudo().search([('website_published','=',True)])
       product_number1=len(productsc)
       res.qcontext.update({'product_number1':product_number1})
       _logger.info("===============a====%s",product_number1)
       return res