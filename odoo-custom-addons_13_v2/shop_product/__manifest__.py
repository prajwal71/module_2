# -*- coding: utf-8 -*-
{
    'name': "shop_product",

    'summary': """
        it is useful to display total number of publish product in website""",

    'description': """
    total of product publish in shop page
    """,

    'author': "Nexus Incoropation",
    'website': "http://nexusgurus.com",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','website_sale'],


    'data': [
    
        'views/templates.xml',
    ],
}
