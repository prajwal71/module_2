# -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2016-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#    See LICENSE file for full copyright and licensing details.
#################################################################################
import werkzeug.utils
import werkzeug.wrappers
from odoo.http import request
import logging
_logger = logging.getLogger(__name__)
from odoo import http, _
from odoo.addons.auth_signup.models.res_users import SignupError
from odoo.addons.web.controllers.main import ensure_db, Home
from odoo.addons.auth_signup.controllers.main import AuthSignupHome
from odoo.addons.website_sale.controllers.main   import WebsiteSale


class ReferEarnSignup(AuthSignupHome):

	@http.route('/web/signup', type='http', auth='public', website=True)
	def web_auth_signup(self, *args, **kw):
		referral = request.httprequest.args.get('referral')
		result = super(ReferEarnSignup, self).web_auth_signup(*args, **kw)
		result.qcontext.update({
			'referral':referral
			})
		if result.qcontext.get('error'):
			return result
		else:
			user = request.env.user
			if request.httprequest.method == 'POST':
				if kw.get('referral'):

					checkparentReferral = user.partner_id._createReferralAccount(
						partner_id=user.partner_id.id,
						parent_referral_code=kw['referral'],
						user_id=user.id
						)
					if referral and referral == kw['referral']:
						user.partner_id.is_direct = False
					else:
						user.partner_id.is_direct = True

					if checkparentReferral['is_parent_referral']:
						request.env['transaction.history'].sudo()._createTransactionSignup(
							partner_id = user.partner_id.id,
							user_id = user.id,
							parent_user_id = checkparentReferral['parent_user_id']
							)
					else:
						request.env['transaction.history'].sudo()._createInvalidTransactionSignup(
							user_id = user.id,
							invalid_referal=kw.get('referral')
							)

				else:
					ReferralCustomer = user.partner_id._createReferralAccount(
						partner_id=user.partner_id.id,
						parent_referral_code="",
						user_id=user.id
						)

			return result


class ReferEarnWebsiteSale(WebsiteSale):

	@http.route(['/shop/confirmation'], type='http', auth="public", website=True)
	def payment_confirmation(self, **post):
		result = super(ReferEarnWebsiteSale, self).payment_confirmation(**post)
		user = request.env.user
		checkComsnType = request.env['res.config.settings'].sudo().get_values().get('verify_commission') == 'afterOrder'
		if checkComsnType and result.qcontext.get('order'):
			order = request.env['sale.order'].sudo().search([('partner_id','=',user.partner_id.id)])
			if len(order) == 1:
				TxnObj = request.env['transaction.history'].sudo()
				if order.state == 'sale':
					TxnObj.addComsnOnFirstSale(user_id=user.id,first_order=order.id)
					# addComsnOnFirstSale() this methods runs when payment gateway is used and
					# sale order is directly converted to state 'sale'
				else:
					TxnObj.addFirstOrderTransaction(user_id=user.id,first_order=order.id)


		return result


	@http.route('/shop/payment/validate', type='http', auth="public", website=True)
	def payment_validate(self, transaction_id=None, sale_order_id=None, **post):
		""" Method that should be called by the server when receiving an update
		for a transaction. State at this point :

		 - UDPATE ME
		"""
		result = super(ReferEarnWebsiteSale,self).payment_validate(transaction_id, sale_order_id, **post)
		user = request.env.user
	
		sale_order_use = request.session.get('sale_last_order_id')
		if sale_order_use:
			no_of_point_id = request.env['res.config.settings'].sudo().browse()
			no_of_point = no_of_point_id .equivalent_amount
			sale_id = request.env['sale.order'].sudo().browse(sale_order_use)
			used = sale_id.use_re_epoints
			amount = sale_id.redeemable_points
			points = amount/no_of_point
			sale_name = sale_id.name
			_logger.info("..................wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww %s<<%s<<<<%s",used,sale_id,amount)
			partner = request.env['res.partner'].sudo().browse(user.partner_id.id)
			balance = partner.referral_earning
			if used:
				
				
				_logger.info("............??????????????????>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>%s",balance)
				if balance>0:
					render_values = {
         							'debit_sale_order_id':sale_id.id,
									'reason':sale_name,
									'user_id':user.id,
									'points':points,
									'point_type':'d',
									'state':'approve',
									'total_amount':amount,
							}
					_logger.info("......THIS**IS**BALANCE**GREATER......??????????????????>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>%s",balance)
					transaction_record=request.env['transaction.history'].sudo().create(render_values)
				# if amount>balance and balance>0:
				# 	amount_change = amount-balance
				# 	amount_final=sale_id.amount_total + amount_change
				# 	sale_id.update({
				# 	'redeemable_points':balance,
				# 	'amount_total': amount_final,
				# 	})
				# 	render_values = {
         		# 					'debit_sale_order_id':sale_id.id,
				# 					'reason':sale_name,
				# 					'user_id':user.id,
				# 					'point_type':'d',
				# 					'state':'approve',
				# 					'total_amount':balance
				# 			}
				# 	_logger.info(".........THIS**IS**BALANCE**LESS...??????????????????>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>%s",balance)
				# 	transaction_record=request.env['transaction.history'].sudo().create(render_values)
				# if balance<=0:
				# 	amount_final=sale_id.amount_total + amount
				# 	sale_id.update({
				# 	'redeemable_points':0,
				# 	'amount_total': amount_final,
				# 	})
				# 	_logger.info(".........THIS**IS**BALANCE**ZERO...??????????????????>>>>>>>>>>>>>>>>>>>%s>>>>>>>>>>>>>>>>>>>>%s",amount_final)
      
		return result
