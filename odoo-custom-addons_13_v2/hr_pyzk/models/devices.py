from odoo import models, fields, api, exceptions, _,SUPERUSER_ID
import datetime
from datetime import timedelta

from odoo_custom_addons.hr_pyzk.controllers import controller as c

import logging
_logger = logging.getLogger(__name__)

class Devices(models.Model):
    _name = 'devices'

    name = fields.Char(string='Device Name')
    ip_address = fields.Char(string='Ip address')
    port = fields.Integer(string='Port', default = 4370)
    sequence = fields.Integer(string='Sequence')
    device_password = fields.Char(string='Device Password')
    state = fields.Selection([('Active', 'Active'), ('Inactive', 'Inactive')], string='Status', default=1)
    difference = fields.Float(string='Time Difference with UTC',
                              help = "Please enter the time difference betwneen local and UTC times in hours", default=0)

    def test_connection(self):
        with c.ConnectToDevice(self.ip_address, self.port, self.device_password) as conn:
            if conn:
                return {
                    "type": "ir.actions.act_window",
                    "res_model": "devices",
                    "views": [[False, "form"]],
                    "res_id": self.id,
                    "target": "main",
                    "context": {'show_message1': True},
                }
    
    def import_users(self): # Import User for fur Import user Wizard
        device_object = self.env['devices']
        devices = device_object.search([('id', '=',self.id)])
        users_object = self.env['device.users']
        odoo_users = users_object.search([])
        odoo_users_id = [user.device_user_id for user in odoo_users]
        unique_data = c.DeviceUsers.get_users(self,devices)
        _logger.info("===========%s",odoo_users_id)
        _logger.info("===========%s",unique_data)
        
        for user in unique_data:
                users_object_a1 = self.env['device.users'].search([('device_user_id','=',int(user.user_id)),('device_id','=',self.id)])
                if not users_object_a1:
                    users_object.create({
                        'device_user_id': int(user.user_id),
                        'device_uid': user.uid,
                        'name': user.name,
                        'device_id':self.id,
                    })

    def import_attendance(self): # Import Attendance Wizard
        all_attendances = []
        all_attendances.clear()
        all_clocks = []
        all_clocks.clear()
        devices_object = self.env['devices']
        devices = devices_object.search([('id', '=', self.id)],limit=1)
        device_user_object = self.env['device.users']
        device_users = device_user_object.search([('device_id','=',self.id)])
        attendance_object = self.env['device.attendances']
       
        
        # user_tz = self.env.user.tz
        # local = pytz.timezone(user_tz)
        # local_time = datetime.datetime.now()
        # difference = (pytz.timezone('UTC').localize(local_time) - local.localize(local_time))
        for device in devices:
            attendances = c.DeviceUsers.get_attendance(self,device)
            _logger.info("========all attendancecs==%s",attendances)
            
            for x in attendances:
                k_a = int(x[0])
                k_b = str(x[1]+ datetime.timedelta(hours=device.difference))
                k_c = int(x[3])
                
                dev_user = self.env['device.users'].search([('device_user_id','=',int(x[0])),('device_id','=',k_c)],limit=1)
                # _logger.info("========all attendancecs==%s",dev_user.name)
                if dev_user:
                    # _logger.info("========all ck attendancecs==%s:%s:%s",k_a,k_b,k_c)
                    attendance_object_exist = self.env['device.attendances'].search([('device_id','=', k_c),('device_user_id', '=', dev_user.id),('device_datetime', '=',k_b)])
                    # _logger.info("========all exist attendancecs==%s",attendance_object_exist)
                    if not attendance_object_exist:
                        emp_id = False
                        shift_id = False
                        if dev_user:
                            emp_id = dev_user.employee_id
                            if emp_id:
                                shift_id = emp_id.resource_calendar_id.id
                        attendance_object.create({
                            'device_user_id': dev_user.id,
                            'device_datetime': x[1]+ datetime.timedelta(hours=device.difference),
                            # 'device_punch': a[2],
                            #'repeat': a[4],
                            'channel': 'auto',
                            'employee_id': emp_id.id,
                            'state': 'app',
                            'approved_id': SUPERUSER_ID,
                            'attendance_state': 'notlogged',
                            'device_id': x[3],
                            'shift_id': shift_id,
                        })

            