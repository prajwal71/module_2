from datetime import datetime
from odoo.exceptions import ValidationError
from odoo import models, fields, api, exceptions, _
import logging
_logger = logging.getLogger(__name__)


class DeviceAttendances(models.Model):
    _name = "device.attendances"
    _description = "Device Attendances"
    #_order = "pyzk_datetime"
    _order = "device_datetime desc"

    # @api.one
    # @api.depends('device_id')
    # def _compute_get_employee_id(self):
    #     _logger.info("==========================emp============%s",self.device_user_id.employee_id)
    #     if self.device_user_id.employee_id:
    #         self.employee_id = self.device_user_id.employee_id.id
    # @api.one        
    # @api.depends('device_id')
    # def _compute_shift_id(self):
    #     if self.device_user_id.employee_id:
    #         self.shift_id = self.device_user_id.employee_id.resource_calendar_id.id

    def _get_employee_id(self):
        if self.employee_id:
            return self.employee_id.user_id.id

    device_user_id = fields.Many2one('device.users','Device ID')
    employee_id = fields.Many2one('hr.employee',related='device_user_id.employee_id', string='Related employee',store=True,readonly=False)
    device_datetime = fields.Datetime(string="Device Datetime")
    device_punch = fields.Selection([('checkin', 'Check In'), ('checkout', 'Check Out')], string='Device Punch')
    attendance_state = fields.Selection([('notlogged', 'Not Logged'), ('logged', 'Logged')], string='Status', default = 0)
    channel = fields.Selection([('man', 'Manual'), ('auto', 'From Device')], string='Channel', default='man')
    device_id = fields.Many2one('devices', related='device_user_id.device_id', string='fingerprint Device', readonly=False,store=True)
    shift_id = fields.Many2one('resource.calendar', 'Shift')
    att_remarks = fields.Char('Remarks')
    state = fields.Selection([('app', 'Approved'), ('nonapp', 'Unapproved')], string='State', default='nonapp')
    approved_id = fields.Many2one('res.users','Approved By')
    
    usr_id = fields.Many2one('res.users', string="User ID", default=_get_employee_id, readonly=True)
    
    def approve_attendance(self):
        for rec in self:
            if rec.channel == 'man':
                if rec.state != 'app':
                    emp_obj = self.env['hr.employee'].search([('user_id','=',self.env.user.id)],limit=1)
                    if emp_obj:
                        if self.env.user.has_group('hr.group_hr_manager') or rec.employee_id.parent_id.id == emp_obj.id:
                            rec.state = 'app'
                            rec.approved_id = self.env.user.id
                        else:
                            raise ValidationError("You should be either manager of employee or HR manager for Approval")
                    else:
                        raise ValidationError("Employee not for for current user")
            else:
                raise ValidationError("You can only approve a manual Attendance")

    @api.onchange('employee_id')
    def _shift_name(self):
        _logger.info("This is the log")
        for rec in self:
            # rec.shift_id = self.env['hr.employee'].rec.employee_id
            if rec.employee_id:
                rec.shift_id = rec.employee_id.resource_calendar_id.id
