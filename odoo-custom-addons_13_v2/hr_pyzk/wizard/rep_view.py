# -*- coding: utf-8 -*-
from os import pathconf
import xlwt
import base64
import calendar
from io import StringIO
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning
from datetime import datetime,date, timedelta
from odoo.http import request
import logging
_logger = logging.getLogger(__name__)
class AttendanceDailyReport(models.TransientModel):
    _name = "atten.reporting"
class AttendanceDailyReportFNL(models.TransientModel):
    _name = "atten.reporting.fnl"
    
    invoice_data = fields.Char('Name',)
    file_name = fields.Binary('Employee Excel Report', readonly=True)
    state = fields.Selection([('choose', 'choose'), ('get', 'get')],
                             default='choose')
    # dev_usr_id = fields.Many2one('device.users','Employee')
    dev_usr_ids = fields.Many2many('device.users')
    # p_id = fields.Many2one('hr.employee','Employee')
    date_from = fields.Datetime('Start Date')
    date_to = fields.Datetime('End date')
    rep_type = fields.Selection([('daily', 'Daily'), ('monthly', 'Monthly'),('individual','Individual')],
                             default='daily')
    # @api.multi
    def action_invoice_report(self):
       
        date_from = str(self.date_from + timedelta(hours=5,minutes=45))
        date_to = str(self.date_to + timedelta(hours=5,minutes=45))
        workbook = xlwt.Workbook()
        format0 = xlwt.easyxf('font:height 500,bold True;pattern: pattern solid, fore_colour yellow;align: horiz center')
        format1 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz left')
        format2 = xlwt.easyxf('font:bold True;align: horiz left')
        format77 = xlwt.easyxf('font:bold False;pattern: pattern solid, fore_colour yellow;align: horiz left')

        format3 = xlwt.easyxf('font:bold False;pattern: pattern solid, fore_colour light_green;align: horiz left')
        format4 = xlwt.easyxf('font:bold False;pattern: pattern solid, fore_colour red;align: horiz left')
        format5 = xlwt.easyxf('font:bold True;align: horiz right')
        format6 = xlwt.easyxf('font:bold True;pattern: pattern solid, fore_colour gray25;align: horiz right')
        format7 = xlwt.easyxf('font:bold True;borders:top thick;align: horiz right')
        format8 = xlwt.easyxf('font:bold True;borders:top thick;pattern: pattern solid, fore_colour gray25;align: horiz left')
        if self.rep_type == 'daily':
            sheet = workbook.add_sheet("Employee_report",cell_overwrite_ok=True)
            sheet.col(0).width = int(30*260)
            sheet.col(1).width = int(30*260)    
            sheet.col(2).width = int(18*260)    
            sheet.col(3).width = int(18*260) 
            sheet.col(4).width = int(33*260)   
            sheet.col(5).width = int(15*260)
            sheet.col(6).width = int(18*260)
            sheet.write_merge(0, 1, 0, 5, 'Daily Attendance Report',format0)   
            sheet.write(2, 0, "Date" +" - "+ date_from [0:10], format1)
            sheet.write(2, 1, "No of Present", format1)
            sheet.write(2, 3, "No of Absent", format1)
            sheet.write(2, 5, " ", format1)
            sheet.write(3, 0, "Employee", format1)
            sheet.write(3, 1, "Check In", format1)
            sheet.write(3, 2, "Check Out", format1)
            sheet.write(3, 3, "Worked Hours", format1)
            sheet.write(3, 4, "State", format1)
            sheet.write(3, 5, "Remarks", format1)
            sheet.write(3, 6, "Device", format1)
            self.env.cr.execute("""select da.device_user_id,da.att_remarks,du.name from device_attendances da inner join device_users du on da.device_user_id = du.id where device_datetime>=%s and device_datetime<=%s and att_remarks IS NOT NULL order by du.name""", (date_from,date_to))
            my_data1 = self.env.cr.dictfetchall()
            self.env.cr.execute("""SELECT distinct(du.id),du.name, device_user_id,device_id
            FROM device_users du where du.id NOT IN(select device_user_id from  device_attendances where device_datetime>=%s and device_datetime<=%s AND device_user_id IS NOT NULL)order by du.name""", (date_from,date_to))
            my_data2 = self.env.cr.dictfetchall()
            self.env.cr.execute("""WITH RECURSIVE minmax AS
                            (
                            SELECT MIN(CAST(device_datetime AS DATE)) AS min, MAX(CAST(device_datetime as DATE)) AS max
                            FROM device_attendances
                            ),
                            dates AS
                            (
                                SELECT m.min as datepart
                                FROM minmax m
                                RIGHT JOIN device_attendances e ON m.min = CAST(e.device_datetime as DATE)
                                UNION ALL
                                SELECT d.datepart + 1 FROM dates d, minmax mm 
                                WHERE d.datepart + 1 <= mm.max
                            )
                            SELECT d.datepart as date, du.name,e.device_user_id, e.device_id, MIN(e.device_datetime) as intime, MAX(e.device_datetime) as outtime FROM dates d
                            LEFT JOIN device_attendances e ON d.datepart = CAST(e.device_datetime as DATE) inner join device_users du on e.device_user_id = du.id 
                            Where e.device_datetime >=%s and e.device_datetime <=%s
                             GROUP BY d.datepart, e.device_user_id,du.name,e.device_id ORDER BY e.device_id, du.name""", (date_from,date_to)) 
            my_data3 = self.env.cr.dictfetchall()
            row = 4
            for datas in my_data3:
                datas['remark'] = " "
            for data in my_data1:
                for datas in my_data3:
                    if data['name'] == datas['name']:
                        datas['remark'] = data['att_remarks']
                        break
            count_present = 0
            count_absent = 0            
            for lines in my_data3:
                    lines['intime'] = lines['intime'] + timedelta(hours=5,minutes=45)
                    lines['outtime'] = lines['outtime'] + timedelta(hours=5,minutes=45)
                    sheet.write(row, 0, (lines['name']), format3)
                    sheet.write(row, 1, str(lines['intime']), format3)
                    if lines['intime'] == lines['outtime']:
                        sheet.write(row, 2, str(" "), format3)
                    else:    
                        sheet.write(row, 2, str(lines['outtime']), format3)
                    sheet.write(row, 3, str(lines['outtime']-lines['intime']), format3)
                    sheet.write(row, 4, ("Present"), format3)
                    if lines['remark']:
                        sheet.write(row, 5, str(lines['remark']), format3)

                    devObj = self.env['devices'].search([('id','=',lines['device_id'])],limit=1)
                    sheet.write(row, 6, str(devObj.name), format3)
                    row = row+1
                    count_present = count_present+1
            sheet.write(2, 2, str(count_present), format1)
            _logger.info("=============mydata22====%s",my_data2)
            new_mylist2 = sorted(my_data2, key=lambda d: d['device_id']) 
            for line in new_mylist2:
                    sheet.write(row, 0, (line['name']), format4)
                    sheet.write(row, 1, (" "), format4)  
                    sheet.write(row, 2, (" "), format4)
                    sheet.write(row, 3, (" "), format4)  
                    sheet.write(row, 4, ("Abcent"), format4)
                    sheet.write(row, 5, (" "), format4)
                    
                    devusrObj = self.env['devices'].search([('id','=',line['device_id'])],limit=1)
                    sheet.write(row, 6, str(devusrObj.name), format4)   
                    row = row+1
                    count_absent = count_absent+1
            sheet.write(2, 4, str(count_absent), format1)
            path = ("/home/Daily_Attendance_Report.xls")
            workbook.save(path)
            file = open(path, "rb")
            file_data = file.read()
            out = base64.encodestring(file_data)
            self.write({'state': 'get', 'file_name': out, 'invoice_data':'Daily_Attendance_Report.xls'})
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'atten.reporting.fnl',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': self.id,
                'target': 'new',
                
                }                
        elif self.rep_type == 'monthly':
            sheet = workbook.add_sheet("Monthly_Employee_report",cell_overwrite_ok=True)
            sheet.col(0).width = int(30*260)
            
            sheet.write_merge(0, 1, 0, 5, 'Monthly Attendance Report',format0)   
            sheet.write(2, 0, "Date From" +" - "+ date_from , format1)
            sheet.write(3, 0, "Date to" +" - "+ date_to , format1)
            dt_list = []
            # def dates_bwn_twodates(start_date, end_date):
            #     for n in range(int ((end_date - start_date).days)):
            #         yield start_date + timedelta(n)
            # dt_list = ([str(d) for d in dates_bwn_twodates(self.date_from.date(),self.date_to.date())])
            col_dt = 1
            delta = self.date_to.date() - self.date_from.date()       
            _logger.info("=============delta==========%s",delta)
            _logger.info("=============delta==========%s",self.date_from.date())
            _logger.info("=============delta==========%s",self.date_to.date())
            for i in range(delta.days + 1):
                day = self.date_from.date() + timedelta(days=i)
                
                dt_list.append(str(day))
            _logger.info("=============day==========%s",dt_list)
            if (len(dt_list) > 0):
                for dts in dt_list:
                    dtk =  fields.Datetime.from_string(dts +' 00:00:00') 
                    dty = dtk.weekday()
                    rc_obj = self.env['resource.calendar'].search([('id','=',1)],limit=1)
                    if dty == 5:
                        _logger.info("=============saturday==========")
                        sheet.col(col_dt).width = int(5*260)
                        sheet.write(4, col_dt, dts[-2:], format4)
                    else:
                        if rc_obj:
                            _logger.info("=============callllllll==========")
                            if (len(rc_obj.global_leave_ids) > 0):
                                for holi in rc_obj.global_leave_ids:
                                    _logger.info("===rc%s",str(holi.date_from+timedelta(hours=5,minutes=45)))
                                    _logger.info("===kc%s",dts +' 00:00:00')
                                    if str(holi.date_from+timedelta(hours=5,minutes=45)) == (dts +' 00:00:00'):
                                        _logger.info("=============holiday==========")
                                        sheet.col(col_dt).width = int(5*260)
                                        sheet.write(4, col_dt, dts[-2:], format4)
                                    else:
                                        _logger.info("=============NOholiday==========")
                                        sheet.col(col_dt).width = int(5*260)
                                        sheet.write(4, col_dt, dts[-2:], format1)
                            else:
                                _logger.info("=============No RC==========")
                                sheet.col(col_dt).width = int(5*260)
                                sheet.write(4, col_dt, dts[-2:], format1)
                        else:
                            _logger.info("=============No RC==========")
                            sheet.col(col_dt).width = int(5*260)
                            sheet.write(4, col_dt, dts[-2:], format1)
                    col_dt +=1
                sheet.write(4, col_dt, "Present Count", format3)
                sheet.write(4, col_dt+1, "Absent Count", format4)
                sheet.write(4, col_dt+2, "Late Count", format77)
                sheet.write(4, col_dt+3, "Holiday Count", format4)
                sheet.write(4, col_dt+4, "Paid Leaves", format3)
                sheet.write(4, col_dt+5, "Unpaid Leaves", format77)
            sheet.write(4, 0, "Employee", format1)
            row = 5
            k = []
            leave_types = self.env['hr.leave.type'].search([])
            for ik in leave_types:
                k.append(ik.id)
            if self.dev_usr_ids:
                for emp in self.dev_usr_ids:
                    p_count = 0
                    a_count = 0
                    lt_count = 0
                    p_time = 0
                    h_count = 0
                    paid_leave = 0
                    unpaid_leave = 0
                    r_obj = emp.employee_id.resource_calendar_id
                    sheet.write(row, 0, emp.name, format1)
                    col_dts = 1
                    for dts in dt_list:
                        da_start = dts +' 00:00:00'
                        da_end = dts + ' 23:59:59'
                        att_obj = self.env['device.attendances'].search([('device_datetime','>=',da_start),('device_datetime','<=',da_end),('device_user_id','=',emp.id)])
                        if att_obj:
                            if r_obj:
                                _logger.info("=======================aaa=========%s",r_obj)
                                if r_obj.attendance_ids:
                                    _logger.info("=======================bbb=========%s",r_obj.attendance_ids)

                                    p_time = r_obj.attendance_ids[0].hour_from
                                    minutes = p_time*60
                                    hours, minutes = divmod(minutes, 60)
                                    seconds = 00
                                    pp_time = "%02d:%02d:%02d"%(hours,minutes,seconds)
                                    p_time =  dts +" " + pp_time
                                    min_stamp = att_obj[0].device_datetime
                                    for stamp in att_obj:
                                        if stamp.device_datetime < min_stamp:
                                            min_stamp = stamp.device_datetime
                                    min_stamp = min_stamp +timedelta(hours=5,minutes=45)
                                    _logger.info("===min==%s",min_stamp)
                                    _logger.info("===max==%s",fields.Datetime.from_string(p_time))
                                    dd = (min_stamp - fields.Datetime.from_string(p_time))
                                    _logger.info("===lt==%s",dd.total_seconds())
                                    if (dd.total_seconds() > 900):
                                        lt_count += 1
                                        sheet.write(row, col_dts, "LP", format77)
                                    else:
                                        sheet.write(row, col_dts, "P", format3)
                                # if r_obj.global_leave_ids:
                                #     for holi in r_obj.global_leave_ids:

                                #         if str(holi.date_from+timedelta(hours=5,minutes=45)) == (dts +' 00:00:00'):
                                #             a_count -= 1
                                #             h_count += 1
                                
                                # else:
                                #     sheet.col(col_dt).width = int(5*260)
                                #     # sheet.write(4, col_dt, dts[-2:], format1)
                            else:
                                sheet.write(row, col_dts, "P", format3)
                            col_dts +=1
                            p_count += 1
                        else:
                            dtkk =  fields.Datetime.from_string(dts +' 00:00:00')
                            dtyy = dtkk.weekday()
                            if dtyy == 5:
                                sheet.write(row, col_dts, "SAT", format4)
                            else:
                                sd1 = datetime.strptime(dts,"%Y-%m-%d").date()
                                all_leaves = self.env['hr.leave'].search([('employee_id','=',emp.employee_id.id),('request_date_from','>=',sd1),('request_date_from','<=',sd1),('holiday_status_id','in',k)],limit=1)
                                if all_leaves and all_leaves.state == 'validate':
                                    _logger.info("===leave===============")
                                    if all_leaves.holiday_status_id.unpaid is True:
                                        unpaid_leave += 1
                                    else:
                                        paid_leave += 1
                                    sheet.col(col_dts).width = int(20*260)
                                    a_count += 1
                                    sheet.write(row, col_dts, 'L-'+str(all_leaves.number_of_hours_display) + ' (' +str(all_leaves.holiday_status_id.name)+')', format4)       
                                else:
                                    if r_obj:
                                        if (len(rc_obj.global_leave_ids) > 0): 
                                            for holi in r_obj.global_leave_ids:
                                                if str(holi.date_from+timedelta(hours=5,minutes=45)) == (dts +' 00:00:00'):                       
                                                    # a_count -= 1
                                                    h_count += 1
                                                    _logger.info("===holiday===============")
                                                    sheet.col(col_dts).width = int(16*260)
                                                    sheet.write(row, col_dts, "H - " +  holi.name, format4)
                                                else:
                                                    sheet.write(row, col_dts, "A", format4)
                                                    a_count += 1
                                        else:
                                                sheet.write(row, col_dts, "A", format4)
                                                a_count += 1
                                    else:
                                        sheet.write(row, col_dts, "A", format4)
                                        a_count += 1
                            # dtk =  fields.Datetime.from_string(dts +' 00:00:00') 
                            # dty = dtk.weekday()
                            # if not dty == 5:
                               
                            col_dts +=1
                    sheet.write(row, col_dt+0, p_count, format3)
                    sheet.write(row, col_dt+1, a_count, format4)
                    sheet.write(row, col_dt+2, lt_count, format77)
                    sheet.write(row, col_dt+3, h_count, format4)
                    sheet.write(row, col_dt+4, paid_leave, format3)
                    sheet.write(row, col_dt+5, unpaid_leave, format77)
                    row +=1
            path = ("/home/Monthly_Attendance_Report.xls")
            workbook.save(path)
            file = open(path, "rb")
            file_data = file.read()
            out = base64.encodestring(file_data)
            self.write({'state': 'get', 'file_name': out, 'invoice_data':'Monthly_Attendance_Report.xls'})
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'atten.reporting.fnl',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': self.id,
                'target': 'new',
                
                }

        elif self.rep_type == 'individual':
            if (len(self.dev_usr_ids) > 1):
                raise ValidationError('Please select 1 Employee')
            else:
                sheet = workbook.add_sheet("Individual_Employee_report",cell_overwrite_ok=True)
                sheet.col(0).width = int(30*260)
                sheet.col(1).width = int(30*260)    
                sheet.col(2).width = int(18*260)    
                sheet.col(3).width = int(18*260) 
                sheet.col(4).width = int(18*260)   
                sheet.col(5).width = int(15*260)
                sheet.col(6).width = int(18*260)
                sheet.col(7).width = int(18*260)
                sheet.col(8).width = int(18*260)
                sheet.write_merge(0, 1, 0, 5, 'Individual Attendance Report',format0)   
                sheet.write(2, 0, "Date From" +" - "+ date_from , format1)
                sheet.write(3, 0, "Date to" +" - "+ date_to , format1)
                sheet.write(4, 0, "Employee" +" - "+  self.dev_usr_ids[0].name, format1)
                def dates_bwn_twodates(start_date, end_date):
                    for n in range(int ((end_date - start_date).days)):
                        yield start_date + timedelta(n)
                dt_list = ([str(d) for d in dates_bwn_twodates(self.date_from.date(),self.date_to.date())])
                # col_dt = 1
                sheet.write(5, 0, "Date", format1)
                sheet.write(5, 1, "Check In", format1)
                sheet.write(5, 2, "Check Out", format1)
                sheet.write(5, 3, "Work Hrs", format1)
                sheet.write(5, 4, "Timesheet Hrs", format1)
                sheet.write(5, 5, "Diff", format1)
                sheet.write(5, 6, "Remarks", format1)
                sheet.write(5, 7, "Channel", format1)
                sheet.write(5, 8, "State", format1)
                sheet.write(5, 9, "Approved By", format1)
                sheet.write(5, 10, "Late Remark", format1)
                row = 6
                rowx = 6
                if (len(dt_list) > 0):
                    for dts in dt_list:
                        dtk =  fields.Datetime.from_string(dts +' 00:00:00') 
                        dty = dtk.weekday()
                        rc_obj = self.env['resource.calendar'].search([('id','=',1)],limit=1)
                        if dty == 5:
                            sheet.write(rowx, 0, dts, format4)
                        else:
                            sheet.write(rowx, 0, dts, format1)
                            if rc_obj:
                                for holi in rc_obj.global_leave_ids:
                                    
                                    if str(holi.date_from+timedelta(hours=5,minutes=45)) == (dts +' 00:00:00'):
                                         sheet.write(rowx, 0, dts, format4)
                                    else:
                                         sheet.write(rowx, 0, dts, format1)
                            else:
                                 sheet.write(rowx, 0, dts, format1)
                        rowx +=1
                       
                if (len(dt_list) > 0):
                    for dts in dt_list:
                        da_start = dts +' 00:00:00'
                        da_end = dts + ' 23:59:59'
                        att_obj = self.env['device.attendances'].search([('device_datetime','>=',da_start),('device_datetime','<=',da_end),('device_user_id','=',self.dev_usr_ids[0].id)])
                        if self.dev_usr_ids[0].employee_id:
                            time_obj = self.env['account.analytic.line'].search([('date','>=',da_start),('date','<=',da_end),('employee_id','=',self.dev_usr_ids[0].employee_id.id)])
                            tot_hr = 0
                            _logger.info("=======Attt Object====%s",time_obj)
                            if time_obj:
                                for tm in time_obj:
                                    tot_hr += tm.unit_amount
                        r_obj = self.dev_usr_ids[0].employee_id.resource_calendar_id
                        lt_count = 0
                        if att_obj:
                            if r_obj:
                                if r_obj.attendance_ids:
                                    p_time = r_obj.attendance_ids[0].hour_from
                                    minutes = p_time*60
                                    hours, minutes = divmod(minutes, 60)
                                    seconds = 00
                                    pp_time = "%02d:%02d:%02d"%(hours,minutes,seconds)
                                    p_time =  dts +" " + pp_time
                                    min_stamp = att_obj[0].device_datetime
                                    for stamp in att_obj:
                                        if stamp.device_datetime < min_stamp:
                                            min_stamp = stamp.device_datetime
                                    min_stamp = min_stamp +timedelta(hours=5,minutes=45)
                
                                    dd = (min_stamp - fields.Datetime.from_string(p_time))
                                    if (dd.total_seconds() > 900):
                                        lt_count += 1
                                        sheet.write(row, 10, "LP" +" "+ str(round(dd.total_seconds()/60, 2)), format77)
                                    else:
                                        sheet.write(row, 10, "", format3)
                            if len(att_obj) > 1:
                                min_stmp = att_obj[0].device_datetime
                                max_stmp = att_obj[0].device_datetime
                                for stmps in att_obj:
                                    if stmps.device_datetime < min_stmp:
                                        min_stmp = stmps.device_datetime
                                    if stmps.device_datetime > max_stmp: 
                                        max_stmp = stmps.device_datetime
                                sheet.write(row, 1, str(min_stmp + timedelta(hours=5,minutes=45))[11:19], format3)
                                sheet.write(row, 2, str(max_stmp + timedelta(hours=5,minutes=45))[11:19], format3)
                                sheet.write(row, 3, str((max_stmp + timedelta(hours=5,minutes=45)) - (min_stmp + timedelta(hours=5,minutes=45))), format3)
                                sheet.write(row, 4, str(tot_hr), format3)
                                diff_a = (max_stmp + timedelta(hours=5,minutes=45)) - (min_stmp + timedelta(hours=5,minutes=45))
                                diff_b = timedelta(hours=tot_hr)
                                if (diff_a > diff_b):
                                    sheet.write(row, 5, str(diff_a - diff_b), format3)
                                else:
                                    sheet.write(row, 5, " - "+str(diff_b - diff_a), format3)
                                sheet.write(row, 6, str(att_obj[0].att_remarks), format3)
                                if att_obj[0].channel == 'auto':
                                    sheet.write(row, 7, str("From Device"), format3)
                                else:
                                    sheet.write(row, 7, str("Manual"), format3)
                                if att_obj[0].state == 'app':
                                    sheet.write(row, 8, str("Approved"), format3)
                                else:
                                    sheet.write(row, 8, str("Unapproved"), format3)
                                sheet.write(row, 9, str(att_obj[0].approved_id.name), format3)
                            else:

                                min_stmp = att_obj[0].device_datetime
                                sheet.write(row, 1, str(min_stmp + timedelta(hours=5,minutes=45))[11:19], format3)
                                # sheet.write(row, 4, str(max_stmp + timedelta(hours=5,minutes=45)), format3)
                                sheet.write(row, 6, str(att_obj[0].att_remarks), format3)
                                if att_obj[0].channel == 'auto':
                                    sheet.write(row, 7, str("From Device"), format3)
                                else:
                                    sheet.write(row, 7, str("Manual"), format3)
                                if att_obj[0].state == 'app':
                                    sheet.write(row, 8, str("Approved"), format3)
                                else:
                                    sheet.write(row, 8, str("Unapproved"), format3)
                                sheet.write(row, 9, str(att_obj[0].approved_id.name), format3)
                            row +=1
                        else:
                            
                            sheet.write(row, 1,"",format4)
                            sheet.write(row, 2,"",format4)
                            sheet.write(row, 3, "",format4)
                            sheet.write(row, 4, "",format4)
                            sheet.write(row, 5, "",format4)
                            sheet.write(row, 6, "",format4)
                            sheet.write(row, 7, "",format4)
                            sheet.write(row, 8, "",format4)
                            sheet.write(row, 9, "",format4)
                            row +=1
                path = ("/home/Indvl_Attendance_Report.xls")
                workbook.save(path)
                file = open(path, "rb")
                file_data = file.read()
                out = base64.encodestring(file_data)
                self.write({'state': 'get', 'file_name': out, 'invoice_data':'Indvl_Attendance_Report.xls'})
                return {
                    'type': 'ir.actions.act_window',
                    'res_model': 'atten.reporting.fnl',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'res_id': self.id,
                    'target': 'new',
                    
                    }




                  

