# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################

#from datetime import datetime
from odoo import models, fields, api, exceptions, _,SUPERUSER_ID
import datetime
from datetime import timedelta
import pytz
from odoo_custom_addons.hr_pyzk.controllers import controller as c
import logging
_logger = logging.getLogger(__name__)

class UserWizard(models.TransientModel):
    _name = 'user.wizard'

    # def import_users(self): # Import User for fur Import user Wizard
    #     device_object = self.env['devices']
    #     devices = device_object.search([('id', '=', 'Active')])
    #     users_object = self.env['device.users']
    #     odoo_users = users_object.search([])
    #     odoo_users_id = [user.device_user_id for user in odoo_users]
    #     unique_data = c.DeviceUsers.get_users(self,devices)

    #     for user in unique_data:
    #         if int(user.user_id) not in odoo_users_id:
    #             users_object.create({
    #                 'device_user_id': int(user.user_id),
    #                 'device_uid': user.uid,
    #                 'name': user.name,
    #             })


    def import_attendance(self): # Import Attendance Wizard
        all_attendances = []
        all_attendances.clear()
        all_clocks = []
        all_clocks.clear()
        device_user_object = self.env['device.users']
        device_users = device_user_object.search([])
        attendance_object = self.env['device.attendances']
        devices_object = self.env['devices']
        devices = devices_object.search([('state', '=', 'Active')])
        # user_tz = self.env.user.tz
        # local = pytz.timezone(user_tz)
        # local_time = datetime.datetime.now()
        # difference = (pytz.timezone('UTC').localize(local_time) - local.localize(local_time))
        for device in devices:
            attendances = c.DeviceUsers.get_attendance(self,device)
            _logger.info("========all recs==%s",attendances)
            latest_rec = attendance_object.search([('device_id', '=', device.id)], limit=1)
            if latest_rec:
                _logger.info("========latest rec==%s",latest_rec.device_user_id)
                _logger.info("========latest rec==%s",latest_rec.device_datetime)
                latest_datetime = str(latest_rec.device_datetime)
                _logger.info("========latest==%s",latest_datetime)
                latest_datetime = datetime.datetime.strptime(latest_datetime, '%Y-%m-%d %H:%M:%S')
                latest_datetime = latest_datetime + datetime.timedelta(hours=device.difference)

                all_attendances = [[y.id, x[1], x[2], x[3]]
                                   for x in attendances for y in device_users if
                                   int(x[0]) == y.device_user_id and x[2] <= 1 and x[1] > latest_datetime]
            else:

                all_attendances = [[y.id, x[1], x[2], x[3]]
                                   for x in attendances for y in device_users if
                                   int(x[0]) == y.device_user_id and x[2] <= 1]
            all_clocks.extend((all_attendances))

        for a in all_clocks:
            k_a = int(a[0])
            k_b = str(a[1]+ datetime.timedelta(hours=device.difference))
            k_c = int(a[3])
            attendance_object_exist = self.env['device.attendances'].search([('device_id','=', k_c),('device_user_id', '=', k_a),('device_datetime', '=',k_b)])
            emp_id = False
            shift_id = False
            if not attendance_object_exist:
                dev_user = self.env['device.users'].search([('id','=',int(a[0]))],limit=1)
                if dev_user:
                    emp_id = dev_user.employee_id
                if emp_id:
                    shift_id = emp_id.resource_calendar_id.id
                attendance_object.create({
                    'device_user_id': int(a[0]),
                    'device_datetime': a[1]+ datetime.timedelta(hours=device.difference),
                    # 'device_punch': a[2],
                    #'repeat': a[4],
                    'channel': 'auto',
                    'employee_id': emp_id.id,
                    'state': 'app',
                    'approved_id': SUPERUSER_ID,
                    'attendance_state': 'notlogged',
                    'device_id': a[3],
                    'shift_id': shift_id,
                })

    def employee_attendance(self): # combining employee attendances
        device_user_object  = self.env['device.users']
        device_attendances_object = self.env['device.attendances']
        odoo_users = device_user_object.search([])

        user_punches2 = []
        user_punches2.clear()
        all_attendance = []
        all_attendance.clear()
        user_clocks = []
        user_clocks.clear()
        attendance = []
        attendance.clear()
        #clock = []
        #clock.clear()

        for user in odoo_users:
            device_attendances = []
            device_attendances.clear()
            device_attendances = device_attendances_object.search(
                [('device_user_id', '=', user.id), ('attendance_state', '=', 'Not Logged')])

            if len(device_attendances)!=0:
                user_punches = [[int(x.device_user_id),datetime.datetime.strptime(str(x.device_datetime),
                                                                                  '%Y-%m-%d %H:%M:%S'),
                                 x.device_punch] for x in device_attendances]
                user_punches.sort()
                attendance = c.DeviceUsers.outputresult(user_punches)
                user_punches2.extend(user_punches)
                all_attendance.extend(attendance)
                #user_clocks.extend(clock)

                for record in device_attendances:
                    if record.attendance_state == 'Not Logged':
                        record.attendance_state = 'Logged'
        return all_attendance

    # @api.multi
    def combine_attendance(self):
        combined_attendances_object= self.env['combined.attendances']
        valid_attendances = []
        valid_attendances.clear()
        valid_attendances = self.employee_attendance()
        for attendance in valid_attendances:
            combined_attendances_object.create({
                'device_user_id': int(attendance[0]),
                'device_date': attendance[1].date(),
                'device_clockin': attendance[1],
                'device_clockout': attendance[2],
            })

    def transfer_attendance(self):
        combined_attendance_object = self.env['combined.attendances']
        hr_attendance_object = self.env['hr.attendance']
        all_data = combined_attendance_object.search([('state', '=', 'Not Transferred'), ('employee_id', '!=', False)])

        for attendance in all_data:
            # if attendance.employee_id:
            hr_attendance_object.create({
                'employee_id': attendance.employee_id.id,
                'check_in': attendance.device_clockin,
                'check_out': attendance.device_clockout,
            })

            attendance.state = 'Transferred'
