from odoo import models, fields, api

class ResPartnerPortalExt(models.Model):
    """This class is defined to enhance ResUsers Class to enhance portal."""

    _inherit = 'res.partner'

    gender = fields.Selection([('male', 'Male'), ('female', 'Female')],
                              string="Gender")
    birthday = fields.Date('Date of Birth')

    marital = fields.Selection(
        [('single', 'Single'),
         ('married', 'Married'),
         ('widower', 'Widower'),
         ('divorced', 'Divorced')],
        'Marital Status')

    
    country_id = fields.Many2one('res.country')
    is_same_address = fields.Boolean('Same as Correspondence Address')
    street_ht = fields.Char('Street')
    street2_ht = fields.Char('Street2')
    city_ht = fields.Char('City')
    zip_ht = fields.Char('Zip')
    state_id_ht = fields.Many2one('res.country.state', string="State")
    