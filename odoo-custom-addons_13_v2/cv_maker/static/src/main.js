
odoo.define('cv_maker.cv_maker', function (require) { 
    "use strict";

    var core = require('web.core');
    var odoo = require('web.ajax');
    var Dialog = require('web.Dialog');
    var rpc = require('web.rpc');
    var _t = core._t;
    var ajax = require('web.ajax');

    // For My profile page Models to create Applicant Details
    $(document).ready(function(){
          
        $(document).on('click', '.experience_btn_cv', function(e){
            e.stopPropagation();
            e.preventDefault();
            $('#myModalExperienceCV').modal();
            $('#myModalExperienceCV').find("h4[class='modal-title']").text("Add Experience Details")
            $('#myModalExperienceCV').find("input[name='operation_type']").val('insert')
            $('#myModalExperienceCV').find('input,select,textarea').val('')
        });

        $(document).on('click', '#ExperienceEditModalCV', function(e){
            e.stopPropagation();
            e.preventDefault();
            var id = $(this).data('experience_id_cv');
            var start_date_val = $(this).parents('tr').find('.exeperience_start_date').text();
            var end_date_val = $(this).parents('tr').find('.exeperience_end_date').text();
            $('#ExperienceEditModalCV'+id).modal();
            $('#ExperienceEditModalCV'+id).find("input[name='start_date']").val(start_date_val);
            $('#ExperienceEditModalCV'+id).find("input[name='end_date']").val(end_date_val);
        });
        $(document).on('click', '#AcademicEditModalCV', function(e){
            e.stopPropagation();
            e.preventDefault();
            var id = $(this).data('academic_id_cv');
            var start_date_val = $(this).parents('tr').find('.academic_start_date').text();
            var end_date_val = $(this).parents('tr').find('.academic_end_date').text();
            $('#AcademicEditModalCV'+id).modal();
            $('#AcademicEditModalCV'+id).find("input[name='start_date']").val(start_date_val);
            $('#AcademicEditModalCV'+id).find("input[name='end_date']").val(end_date_val);
        });
        $(document).on('click', '#CertificationEditModalCV', function(e){
            e.stopPropagation();
            e.preventDefault();
            var id = $(this).data('certification_id_cv');
            var start_date_val = $(this).parents('tr').find('.certification_start_date').text();
            var end_date_val = $(this).parents('tr').find('.certification_end_date').text();
            $('#CertificationEditModalCV'+id).modal();
            $('#CertificationEditModalCV'+id).find("input[name='start_date']").val(start_date_val);
            $('#CertificationEditModalCV'+id).find("input[name='end_date']").val(end_date_val);
        });
    });

    $(document).ready(function(){
      $('#top_menu li').addClass('job_portal_menus')
      $('.job_portal_menus').each(function(){

            if($.trim($(this).text())=='Post Job'){
                 $(this).attr('groups','base.group_erp_manager')
            }
      })
      $(document).on('click', '.certification_btn_cv', function(e){
            e.stopPropagation();
            e.preventDefault();
            $('#myModalCertificationCV').modal();
            $('#myModalCertificationCV').find("h4[class='modal-title']").text("")
            $('#myModalCertificationCV').find("h4[class='modal-title']").text("Add Certification Details")
            $('#myModalCertificationCV').find("input[name='operation_type']").val('insert')
            $('#myModalCertificationCV').find('input,select,textarea').val('')


        });
        $(document).on('click', '.academic_btn_cv', function(e){
            e.stopPropagation();
            e.preventDefault();
            $('#myModalAcademicCV').modal();
            $('#myModalAcademicCV').find("h4[class='modal-title']").text("")
            $('#myModalAcademicCV').find("h4[class='modal-title']").text(" Add Academic Details")
            $('#myModalAcademicCV').find("input[name='operation_type']").val('insert')
            $('#myModalAcademicCV').find('input,select,textarea').val('')
        });


        /* affix the navbar after scroll below header */
        // Click event For Adding Academic of Applicants
        // Validation on the models can/should be improved later on
        $(document).on('click', '#add_academic_cv', function(e){
            var current_val=new Date()
            var target = $(this).parents('#myModalAcademicCV');
            e.stopPropagation();
            e.preventDefault();

             var start_date_val=new Date(target.find("input[name='start_date']").val())
             var end_date_val=new Date(target.find("input[name='end_date']").val())
            if(target.find("input[name='operation_type']").val()=='update'){


            if(start_date_val>end_date_val){
                    alert("start date should not be greater than end date.")
                    return false
                }
                 if((start_date_val>=current_val)){
                    alert("start date  should not greater than or equal to today")
                    return false
                } if((end_date_val>=current_val)){
                    alert("end date should not greater than or equal to today")
                    return false
                }
            if ( $.trim(target.find("input[name='qualification']").val()) &&
                $.trim(target.find("input[name='study_field']").val()) &&
                $.trim(target.find("input[name='organization']").val()) &&
                $.trim(target.find("input[name='location']").val()) &&
                $.trim(target.find("input[name='start_date']").val()) &&
                $.trim(target.find("input[name='grade']").val())){

if($('#myModalAcademicCV').find("input[name='is_still']").is(":checked")){
                var qualification = target.find("input[name='qualification']").val()
                var study_field = target.find("input[name='study_field']").val()
                var organization = target.find("input[name='organization']").val()
                var location = target.find("input[name='location']").val()
                var start_date = target.find("input[name='start_date']").val()
                var is_still =  true
                var grade = target.find("input[name='grade']").val()
                var opr_id = target.find("input[name='opr_id']").val()
                  var tr_no = parseInt(target.find("input[name='tr_no']").val())+1
                   if ($('.academic_desc_table tr').length==2){
                        tr_no=1
                     }
                odoo.jsonRpc("/edit_academic_applicant", 'call', {
                    'qualification' : target.find("input[name='qualification']").val(),
                    'study_field' : target.find("input[name='study_field']").val(),
                    'organization' : target.find("input[name='organization']").val(),
                    'location' : target.find("input[name='location']").val(),
                    'start_date' : target.find("input[name='start_date']").val(),
                    'is_still' : true,
                    'grade' : target.find("input[name='grade']").val(),
                    'id' : target.find("input[name='opr_id']").val()
                }).then(function(rec) {
                     $(".academic_desc_table tr:eq("+$.trim(tr_no)+")").replaceWith("<tr><td>"+$.trim(target.find("input[name='qualification']").val()) +"</td><td>"+$.trim(study_field)+"</td><td>"+$.trim(organization)+"</td><td>"+$.trim(location)+"</td><td>"+$.trim(start_date)+"</td><td>"+$.trim(is_still)+"</td><td>"+$.trim(grade)+"</td><td><button type='button' class='btn edit_academic' data-toggle='modal' data-academic_id="+rec+"><i class='fa fa-pencil-square-o'></i></button><td><button type='button' data-academic_id="+rec+" class='btn delete_academic'><i class='fa fa-trash'></i></button></td></tr>")
                     $('#myModalAcademicCV').modal('hide')
                    //  $( "#academic_desc_table" ).load(window.location.href + " #academic_desc_table" );
//                    location.reload();
                });

}
else{
              var qualification =  target.find("input[name='qualification']").val()
              var study_field = target.find("input[name='study_field']").val()
              var organization = target.find("input[name='organization']").val()
              var location = target.find("input[name='location']").val()
              var start_date = target.find("input[name='start_date']").val()
              var end_date =  target.find("input[name='end_date']").val()
              var grade = target.find("input[name='grade']").val()
              var opr_id = target.find("input[name='opr_id']").val()
              var tr_no = parseInt(target.find("input[name='tr_no']").val())+1
                   if ($('.academic_desc_table tr').length==2){
                        tr_no=1
                     }
             odoo.jsonRpc("/edit_academic_applicant", 'call', {
                    'qualification' : target.find("input[name='qualification']").val(),
                    'study_field' : target.find("input[name='study_field']").val(),
                    'organization' : target.find("input[name='organization']").val(),
                    'location' : target.find("input[name='location']").val(),
                    'start_date' : target.find("input[name='start_date']").val(),
                    'end_date' : target.find("input[name='end_date']").val(),
                    'grade' : target.find("input[name='grade']").val(),
                    'id' : target.find("input[name='opr_id']").val()
                }).then(function(rec) {
                     $(".academic_desc_table tr:eq("+$.trim(tr_no)+")")
                     .replaceWith("<tr><td>"+$.trim(target.find
                     ("input[name='qualification']").val())+"</td><td>"+$
                     .trim(study_field)+"</td><td>"+$.trim(organization)
                     +"</td><td>"+$.trim(location)+"</td><td>"+$.trim(start_date)+"</td><td>"+$.trim(end_date)+"</td><td>"+$.trim(grade)+"</td><td><button type='button' class='btn edit_academic' data-toggle='modal' data-academic_id="+rec+"><i class='fa fa-pencil-square-o'></i></button><td><button type='button' data-academic_id="+rec+" class='btn delete_academic'><i class='fa fa-trash'></i></button></td></tr>")
                     $('#myModalAcademicCV').modal('hide')
//                    location.reload();\
                    // $( "#academic_desc_table" ).load(window.location.href + " #academic_desc_table" );

                });
                }}
               else {
                Dialog.alert(self, _t("Please Give Proper Values to the Input fields !"), {
                    title: _t('Alert!'),
                });
            }
            }
            else if($.trim(target.find("input[name='operation_type']").val()=='insert'))
            {

                if(start_date_val>end_date_val){
                    alert("start date should not be greater than end date.")
                    return false
                }
                 if((start_date_val>=current_val)){
                    alert("start date  should not greater than or equal to today")
                    return false
                } if((end_date_val>=current_val)){
                    alert("end date should not greater than or equal to today")
                    return false
                }
                if ( $.trim(target.find("input[name='qualification']").val()) &&
                $.trim(target.find("input[name='study_field']").val()) &&
                $.trim(target.find("input[name='organization']").val()) &&
                $.trim(target.find("input[name='location']").val()) &&
                $.trim(target.find("input[name='start_date']").val()) &&
                $.trim(target.find("input[name='grade']").val())){


                if($('#myModalAcademicCV').find("input[name='is_still']").is(":checked")){
                    var qualification = target.find("input[name='qualification']").val()
                    var study_field = target.find("input[name='study_field']").val()
                    var organization = target.find("input[name='organization']").val()
                    var location = target.find("input[name='location']").val()
                    var start_date = target.find("input[name='start_date']").val()
                    var is_still =  true
                    var grade = target.find("input[name='grade']").val()
                      odoo.jsonRpc("/add_academic_applicant", 'call', {
                    'qualification' : target.find("input[name='qualification']").val(),
                    'study_field' : target.find("input[name='study_field']").val(),
                    'organization' : target.find("input[name='organization']").val(),
                    'location' : target.find("input[name='location']").val(),
                    'start_date' : target.find("input[name='start_date']").val(),
                    'is_still' : true,
                    'grade' : target.find("input[name='grade']").val(),
                }).then(function(rec) {
                    $('.academic_desc_table tr:last').after("<tr><td>"+$.trim(qualification)+"</td><td>"+$.trim(study_field)+"</td><td>"+$.trim(organization)+"</td><td>"+$.trim(location)+"</td><td>"+$.trim(start_date)+"</td><td>"+$.trim(end_date)+"</td><td>"+$.trim(grade)+"</td><td><button type='button' class='btn edit_academic' data-toggle='modal' data-academic_id="+rec+"><i class='fa fa-pencil-square-o'></i></button><td><button type='button' data-academic_id="+rec+" class='btn delete_academic'><i class='fa fa-trash'></i></button></td></tr>")
                    $('#myModalAcademicCV').modal('hide')
                    // location.reload();
                    // $( "#academic_desc_table" ).load(window.location.href + " #academic_desc_table" );
                });

                 }
                 else
                 {
                    var qualification = target.find("input[name='qualification']").val()
                    var study_field = target.find("input[name='study_field']").val()
                    var organization = target.find("input[name='organization']").val()
                    var location = target.find("input[name='location']").val()
                    var start_date = target.find("input[name='start_date']").val()
                    var end_date =  target.find("input[name='end_date']").val()
                    var grade = target.find("input[name='grade']").val()
                odoo.jsonRpc("/add_academic_applicant", 'call', {
                    'qualification' : target.find("input[name='qualification']").val(),
                    'study_field' : target.find("input[name='study_field']").val(),
                    'organization' : target.find("input[name='organization']").val(),
                    'location' : target.find("input[name='location']").val(),
                    'start_date' : target.find("input[name='start_date']").val(),
                    'end_date' : target.find("input[name='end_date']").val(),
                    'grade' : target.find("input[name='grade']").val(),
                }).then(function(rec) {
                    $('.academic_desc_table tr:last').after("<tr><td>"+$.trim(qualification)+"</td><td>"+$.trim(study_field)+"</td><td>"+$.trim(organization)+"</td><td>"+$.trim(location)+"</td><td>"+$.trim(start_date)+"</td><td>"+$.trim(end_date)+"</td><td>"+$.trim(grade)+"</td><td><button type='button' class='btn edit_academic' data-toggle='modal' data-academic_id="+rec+"><i class='fa fa-pencil-square-o'></i></button><td><button type='button' data-academic_id="+rec+" class='btn delete_academic'><i class='fa fa-trash'></i></button></td></tr>")
                    $('#myModalAcademicCV').modal('hide')
                    //location.reload();
                    // $( "#academic_desc_table" ).load(window.location.href + " #academic_desc_table" );
                });}
            } else {
                Dialog.alert(self, _t("Please Give Proper Values to the Input fields !"), {
                    title: _t('Alert!'),
                });
            }
            }
            else{

            }

        });

        // Click event For Deleting existing Academic Detail of Applicants
        $(document).on('click', '.delete_academic', function(e){
            var ele= $(this)
            e.stopPropagation();
            e.preventDefault();
            var id = parseInt($(this).data('academic_id_cv'));
            odoo.jsonRpc("/delete_academic", 'call', {
              'id' : id,
            }).then(function (rec) {
                ele.closest('tr').remove()
//               location.reload();
            });
        });

     // Get formatted date YYYY-MM-DD
        function getFormattedDate(date) {
            return date.getFullYear()
                + "-"
                + ("0" + (date.getMonth() + 1)).slice(-2)
                + "-"
                + ("0" + date.getDate()).slice(-2);
        }
        
        // Click event For Editing existing Academic Detail of Applicants
        $(document).on('click', '.edit_academic_cv', function(e){

            e.stopPropagation();
            e.preventDefault();
            var id = $(this).data('academic_id_cv');
            $('#myModalAcademicCV').modal();
            $('#myModalAcademicCV').find("h4[class='modal-title']").text("Update Academic Details")
            var target = $(this).parents('#AcademicEditModalCV'+id);
            
            $('#myModalAcademicCV').find("input[name='qualification']").val($.trim($(this).closest("tr").find('td:eq(0)').text()))
            $('#myModalAcademicCV').find("input[name='study_field']").val($.trim($(this).closest("tr").find('td:eq(1)').text()))
            $('#myModalAcademicCV').find("input[name='organization']").val($.trim($(this).closest("tr").find('td:eq(2)').text()))
            $('#myModalAcademicCV').find("input[name='location']").val($.trim($(this).closest("tr").find('td:eq(3)').text()))
            $('#myModalAcademicCV').find("input[name='start_date']").val(getFormattedDate(new Date($(this).closest("tr").find('td:eq(4) span').text())))
            $('#myModalAcademicCV').find("input[name='end_date']").val(getFormattedDate(new Date($(this).closest("tr").find('td:eq(5) span').text())))
            $('#myModalAcademicCV').find("input[name='grade']").val($.trim($(this).closest("tr").find('td:eq(6)').text()))
            $('#myModalAcademicCV').find("input[name='operation_type']").val('update')
            $('#myModalAcademicCV').find("input[name='opr_id']").val(id)
            $('#myModalAcademicCV').find("input[name='tr_no']").val($.trim($(this).closest("tr").index()))

            if($(this).closest("tr").find('td:eq(5) span').text()){

              $('#myModalAcademicCV').find("input[name='is_still']").prop("checked", false);
                 $('#myModalAcademicCV').find('.end_date_div').show();
            }
            else
            {
               $('#myModalAcademicCV').find("input[name='is_still']").prop("checked", true);
               $('#myModalAcademicCV').find('.end_date_div').hide();
            }



        });

        // Click event For Editing existing exeperience Detail of Applicants
        // Validation on the models can/should be improved later on
        $(document).on('click', '#add_experience_cv', function(e){
            e.stopPropagation();
            e.preventDefault();

            var current_val=new Date()
            var target = $(this).parents('#myModalExperienceCV');
            if(target.find("input[name='operation_type']").val()=='update'){
                var start_date_val=new Date(target.find("input[name='start_date']").val())
                var end_date_val=new Date(target.find("input[name='end_date']").val())

                if(start_date_val>end_date_val){
                    alert("start date should not be greater than end date.")
                    return false
                }
                 if((start_date_val>=current_val)){
                    alert("start date  should not greater than or equal to today")
                    return false
                } if((end_date_val>=current_val)){
                    alert("end date should not greater than or equal to today")
                    return false
                }
                if ( $.trim(target.find("input[name='job_position']").val()) &&
                $.trim(target.find("input[name='organization']").val()) &&
                $.trim(target.find("input[name='location']").val()) &&
                $.trim(target.find("input[name='start_date']").val()) &&
                // $.trim(target.find("input[name='contact_referee']").val()) &&
                $.trim(target.find("input[name='description']").val())
                // $.trim(target.find("input[name='position_referee']").val()) &&
                // $.trim(target.find("input[name='name_referee']").val())
                ){
if($('#myModalExperienceCV').find("input[name='is_still']").is(":checked")){
  var job_position = target.find("input[name='job_position']").val()
                     var organization = target.find("input[name='organization']").val()
                     var location =  target.find("input[name='location']").val()
                     var start_date = target.find("input[name='start_date']").val()
                     var is_still = true
                    //  var contact_referee = target.find("input[name='contact_referee']").val()
                     var description = target.find("input[name='description']").val()
                    //  var position_referee = target.find("input[name='position_referee']").val()
                    //  var name_referee = target.find("input[name='name_referee']").val()
                    //  var notice_period = target.find("select.notice_period").val()
                     var type = target.find("select.type").val()
                     var tr_no=parseInt(target.find("input[name='tr_no']").val())+1

//                     alert(tr_no,$('.exp_desc_table tr').length)

                     if ($('.exp_desc_table tr').length==2){
                        tr_no=1
                     }
                    odoo.jsonRpc("/edit_experience_applicant", 'call', {
                        'job_position' : target.find("input[name='job_position']").val(),
                        'organization' : target.find("input[name='organization']").val(),
                        'location' : target.find("input[name='location']").val(),
                        'start_date' : target.find("input[name='start_date']").val(),
                        'is_still':true,
                        // 'contact_referee' : target.find("input[name='contact_referee']").val(),
                        'description' : target.find("input[name='description']").val(),
                        // 'position_referee' : target.find("input[name='position_referee']").val(),
                        // 'name_referee' : target.find("input[name='name_referee']").val(),
                        // 'notice_period' : target.find("select.notice_period").val(),
                        'type' : target.find("select.type").val(),
                        'id' : target.find("input[name='opr_id']").val()
                    }).then(function(rec) {


                    // if(target.find("input[name='contact_referee']").val()==''){
                //  target.find("input[name='contact_referee']").css({'border':'1px solid red'})
                //  alert("Please enter reference contact")
                //  return false

            // }
            //   else if(target.find("input[name='contact_referee']").val().match('^\d+$/')){
            //        target.find("input[name='contact_referee']").css({'border':'1px solid red'})
            //      alert("Please enter reference properly")
            //      return false
            //     }
        //              else if($('.mobile-applicant').val()<0){
        //      target.find("input[name='contact_referee']").css({'border':'1px solid red'})
        //          alert("Please enter reference properly")
        //          return false
        //  }

                         $(".exp_desc_table tr:eq("+$.trim(tr_no)+")").replaceWith("<tr><td>"+$.trim(job_position)+"</td><td>"+$.trim(start_date)+"</td><td>"+$.trim(end_date)+"</td><td>"+$.trim(organization)+"</td><td>"+$.trim(location)+"</td><td>"+$.trim(description)+"</td><td>"+$.trim(type)+"</td><td><button type='button' class='btn edit_experience' data-toggle='modal' data-experience_id="+rec+"><i class='fa fa-pencil-square-o'></i></button><td><button type='button' data-experience_id="+rec+" class='btn delete_experience'><i class='fa fa-trash'></i></button></td></tr>")
                        $('#myModalExperienceCV').modal('hide')
                        // $( "#exp_desc_table" ).load(window.location.href + " #exp_desc_table" );
                    });}
                    else{
                     var job_position = target.find("input[name='job_position']").val()
                     var organization = target.find("input[name='organization']").val()
                     var location =  target.find("input[name='location']").val()
                     var start_date = target.find("input[name='start_date']").val()
                     var end_date = target.find("input[name='end_date']")
                     .val()
                    //  var contact_referee = target.find("input[name='contact_referee']").val()
                     var description = target.find("input[name='description']").val()
                    //  var position_referee = target.find("input[name='position_referee']").val()
                    //  var name_referee = target.find("input[name='name_referee']").val()
                    //  var notice_period = target.find("select.notice_period").val()
                     var type = target.find("select.type").val()
//                     var tr_no=target.find("input[name='tr_no']").val()
                    var tr_no=parseInt(target.find("input[name='tr_no']").val())+1
                     if ($('.exp_desc_table tr').length==2){
                        tr_no=1
                     }
                    odoo.jsonRpc("/edit_experience_applicant", 'call', {
                        'job_position' : target.find("input[name='job_position']").val(),
                        'organization' : target.find("input[name='organization']").val(),
                        'location' : target.find("input[name='location']").val(),
                        'start_date' : target.find("input[name='start_date']").val(),
                        'end_date' : target.find("input[name='end_date']").val(),
                        // 'contact_referee' : target.find("input[name='contact_referee']").val(),
                        'description' : target.find("input[name='description']").val(),
                        // 'position_referee' : target.find("input[name='position_referee']").val(),
                        // 'name_referee' : target.find("input[name='name_referee']").val(),
                        // 'notice_period' : target.find("select.notice_period").val(),
                        'type' : target.find("select.type").val(),
                        'id' : target.find("input[name='opr_id']").val()
                    }).then(function(rec) {


            //         if(target.find("input[name='contact_referee']").val()==''){
            //      target.find("input[name='contact_referee']").css({'border':'1px solid red'})
            //      alert("Please enter reference contact")
            //      return false

            // }
            //   else if(target.find("input[name='contact_referee']").val().match('^\d+$/')){
            //        target.find("input[name='contact_referee']").css({'border':'1px solid red'})
            //      alert("Please enter reference properly")
            //      return false
            //     }
        //              else if($('.mobile-applicant').val()<0){
        //      target.find("input[name='contact_referee']").css({'border':'1px solid red'})
        //          alert("Please enter reference properly")
        //          return false
        //  }
   $(".exp_desc_table tr:eq("+$.trim(tr_no)+")").replaceWith("<tr><td>"+$.trim(job_position)+"</td><td>"+$.trim(start_date)+"</td><td>"+$.trim(end_date)+"</td><td>"+$.trim(organization)+"</td><td>"+$.trim(location)+"</td><td>"+$.trim(description)+"</td><td>"+$.trim(type)+"</td><td><button type='button' class='btn edit_experience' data-toggle='modal' data-experience_id="+rec+"><i class='fa fa-pencil-square-o'></i></button><td><button type='button' data-experience_id="+rec+" class='btn delete_experience'><i class='fa fa-trash'></i></button></td></tr>")

                        $('#myModalExperienceCV').modal('hide')
// $( "#exp_desc_table" ).load(window.location.href + " #exp_desc_table" );
                    });
                    }
                } else {

                    Dialog.alert(self, _t("Please Give Proper Values to the Input fields !"), {
                        title: _t('Alert!'),
                    });
                }
            }
            else{
                var start_date_val=new Date(target.find("input[name='start_date']").val())
                var end_date_val=new Date(target.find("input[name='end_date']").val())

                if(start_date_val>end_date_val){
                    alert("Start date should not be greater than end date.")
                    return false
                }
                if((start_date_val>=current_val)){
                    alert("Start date  should not greater than or equal to today")
                    return false
                } if((end_date_val>=current_val)){
                    alert("End date should not greater than or equal to today")
                    return false
                }


                 if ( $.trim(target.find("input[name='job_position']").val()) &&
                $.trim(target.find("input[name='organization']").val()) &&
                $.trim(target.find("input[name='location']").val()) &&
                $.trim(target.find("input[name='start_date']").val()) &&
                // $.trim(target.find("input[name='contact_referee']").val()) &&
                $.trim(target.find("input[name='description']").val()) &&
                // $.trim(target.find("input[name='position_referee']").val()) &&
                // $.trim(target.find("input[name='name_referee']").val()) &&
                // $.trim(target.find("select.notice_period").val()) &&
                $.trim(target.find("select.type").val())
                ){
                 if($('#myModalExperienceCV').find("input[name='is_still']").is(":checked")){
                     var job_position = target.find("input[name='job_position']").val()
                     var organization = target.find("input[name='organization']").val()
                     var location =  target.find("input[name='location']").val()
                     var start_date = target.find("input[name='start_date']").val()
                     var is_still = true
                    //  var contact_referee = target.find("input[name='contact_referee']").val()
                     var description = target.find("input[name='description']").val()
                    //  var position_referee = target.find("input[name='position_referee']").val()
                    //  var name_referee = target.find("input[name='name_referee']").val()
                    //  var notice_period = target.find("select.notice_period").val()
                     var type = target.find("select.type").val()
                    odoo.jsonRpc("/add_experience", 'call', {
                        'job_position' : target.find("input[name='job_position']").val(),
                        'organization' : target.find("input[name='organization']").val(),
                        'location' : target.find("input[name='location']").val(),
                        'start_date' : target.find("input[name='start_date']").val(),
                        'is_still' : true,
                        // 'contact_referee' : target.find("input[name='contact_referee']").val(),
                        'description' : target.find("input[name='description']").val(),
                        // 'position_referee' : target.find("input[name='position_referee']").val(),
                        // 'name_referee' : target.find("input[name='name_referee']").val(),
                        // 'notice_period' : target.find("select.notice_period").val(),
                        'type' : target.find("select.type").val(),
                    }).then(function(rec) {
            //               if(target.find("input[name='contact_referee']").val()==''){
            //      target.find("input[name='contact_referee']").css({'border':'1px solid red'})
            //      alert("Please enter reference contact")
            //      return false

            // }
            //   else if(target.find("input[name='contact_referee']").val().match('^\d+$/')){
            //        target.find("input[name='contact_referee']").css({'border':'1px solid red'})
            //      alert("Please enter reference properly")
            //      return false
            //     }
        //              else if($('.mobile-applicant').val()<0){
        //      target.find("input[name='contact_referee']").css({'border':'1px solid red'})
        //          alert("Please enter reference properly")
        //          return false
        //  }

                        $('.exp_desc_table tr:last').after("<tr><td>"+$.trim(job_position)+"</td><td>"+$.trim(start_date)+"</td><td>"+$.trim(end_date)+"</td><td>"+$.trim(organization)+"</td><td>"+$.trim(location)+"</td><td>"+$.trim(description)+"</td><td>"+$.trim(type)+"</td><td><button type='button' class='btn edit_experience' data-toggle='modal' data-experience_id="+rec+"><i class='fa fa-pencil-square-o'></i></button><td><button type='button' data-experience_id="+rec+" class='btn delete_experience'><i class='fa fa-trash'></i></button></td></tr>")
                        $('#myModalExperienceCV').modal('hide')
//                        location.reload();
// $( "#exp_desc_table" ).load(window.location.href + " #exp_desc_table" );
                    });}
                    else{
                     var job_position = target.find("input[name='job_position']").val()
                     var organization = target.find("input[name='organization']").val()
                     var location =  target.find("input[name='location']").val()
                     var start_date = target.find("input[name='start_date']").val()
                     var end_date = target.find("input[name='end_date']").val()
                    //  var contact_referee = target.find("input[name='contact_referee']").val()
                     var description = target.find("input[name='description']").val()
                    //  var position_referee = target.find("input[name='position_referee']").val()
                    //  var name_referee = target.find("input[name='name_referee']").val()
                    //  var notice_period = target.find("select.notice_period").val()
                     var type = target.find("select.type").val()
                     odoo.jsonRpc("/add_experience", 'call', {
                        'job_position' : target.find("input[name='job_position']").val(),
                        'organization' : target.find("input[name='organization']").val(),
                        'location' : target.find("input[name='location']").val(),
                        'start_date' : target.find("input[name='start_date']").val(),
                        'end_date' : target.find("input[name='end_date']").val(),
                        // 'contact_referee' : target.find("input[name='contact_referee']").val(),
                        'description' : target.find("input[name='description']").val(),
                        // 'position_referee' : target.find("input[name='position_referee']").val(),
                        // 'name_referee' : target.find("input[name='name_referee']").val(),
                        // 'notice_period' : target.find("select.notice_period").val(),
                        'type' : target.find("select.type").val(),
                    }).then(function(rec) {
            //               if(target.find("input[name='contact_referee']").val()==''){
            //      target.find("input[name='contact_referee']").css({'border':'1px solid red'})
            //      alert("Please enter reference contact")
            //      return false

            // }
            //   else if(target.find("input[name='contact_referee']").val().match('^\d+$/')){
            //        target.find("input[name='contact_referee']").css({'border':'1px solid red'})
            //      alert("Please enter reference properly")
            //      return false
            //     }
            //          else if($('.mobile-applicant').val()<0){
            //  target.find("input[name='contact_referee']").css({'border':'1px solid red'})
            //      alert("Please enter reference properly")
            //      return false
            //     }

                        $('.exp_desc_table tr:last').after("<tr><td>"+$.trim(job_position)+"</td><td>"+$.trim(start_date)+"</td><td>"+$.trim(end_date)+"</td><td>"+$.trim(organization)+"</td><td>"+$.trim(location)+"</td><td>"+$.trim(description)+"</td><td>"+$.trim(type)+"</td><td><button type='button' class='btn edit_experience' data-toggle='modal' data-experience_id="+rec+"><i class='fa fa-pencil-square-o'></i></button><td><button type='button' data-experience_id="+rec+" class='btn delete_experience'><i class='fa fa-trash'></i></button></td></tr>")
                        $('#myModalExperienceCV').modal('hide')
// $( "#exp_desc_table" ).load(window.location.href + " #exp_desc_table" );
                    });
                    }
                } else {
                    Dialog.alert(self, _t("Please Give Proper Values to the Input fields !"), {
                        title: _t('Alert!'),
                    });
                }
                }
        });

        // Click event For Deleting existing exeperience Detail of Applicants
        $(document).on('click', '.delete_experience', function(e){
            e.stopPropagation();
            e.preventDefault();
              var ele= $(this)
            var id = parseInt($(this).data('experience_id_cv'));
            odoo.jsonRpc("/delete_experience", 'call', {
              'id' : id,
            }).then(function () {
                  ele.closest('tr').remove()

            });

        });

        // Click event For Editing existing exeperience Detail of Applicants
        $(document).on('click', '.edit_experience', function(e){
            e.stopPropagation();
            e.preventDefault();
            $('#myModalExperience').modal();
            var id = $(this).data('experience_id_cv');

            var target = $(this).parents('#ExperienceEditModalCV'+id);
            $('#myModalExperienceCV').find("h4[class='modal-title']").text("Update Experience Details")
            $('#myModalExperienceCV').find("input[name='job_position']").val($.trim($(this).closest("tr").find('td:eq(0)').text()))
            $('#myModalExperienceCV').find("input[name='start_date']").val(getFormattedDate(new Date($.trim($(this).closest("tr").find('td:eq(1) span').text()))))
            $('#myModalExperienceCV').find("input[name='end_date']").val(getFormattedDate(new Date($.trim($(this).closest("tr").find('td:eq(2) span').text()))))
            $('#myModalExperienceCV').find("input[name='organization']").val($.trim($(this).closest("tr").find('td:eq(3)').text()))
            $('#myModalExperienceCV').find("input[name='location']").val($.trim($(this).closest("tr").find('td:eq(4)').text()))
            $('#myModalExperienceCV').find("input[name='description']").val($.trim($(this).closest("tr").find('td:eq(5)').text()))
            // $('#myModalExperience').find("input[name='name_referee']").val($.trim($(this).closest("tr").find('td:eq(7)').text()))
            // $('#myModalExperience').find("input[name='position_referee']").val($.trim($(this).closest("tr").find('td:eq(8)').text()))
            // $('#myModalExperience').find("input[name='contact_referee']").val($.trim($(this).closest("tr").find('td:eq(9)').text()))
            $('#myModalExperienceCV').find("input[name='select.type']").val($.trim($(this).closest("tr").find('td:eq(7)').text()))
            $('#myModalExperienceCV').find("input[name='operation_type']").val('update')
            $('#myModalExperienceCV').find("input[name='opr_id']").val(id)
            $('#myModalExperienceCV').find("input[name='tr_no']").val($.trim($(this).closest("tr").index()))
              if($(this).closest("tr").find('td:eq(2) span').text()){

              $('#myModalExperienceCV').find("input[name='is_still']").prop("checked", false);
                 $('#myModalExperienceCV').find('.end_date_div').show();
            }
            else
            {
               $('#myModalExperienceCV').find("input[name='is_still']").prop("checked", true);
               $('#myModalExperienceCV').find('.end_date_div').hide();
            }


        });


        $(document).on('click', '#add_certification_cv', function(e){
            e.stopPropagation();
            e.preventDefault();
            var target = $(this).parents('#myModalCertificationCV');
            var id=target.find("input[name='opr_id']").val()
             var current_val=new Date()
            var start_date_val=new Date(target.find("input[name='start_date']").val())
            var end_date_val=new Date(target.find("input[name='end_date']").val())
            if($('#myModalCertificationCV').find("input[name='operation_type']").val()=='update'){
 if(start_date_val>end_date_val){
                    alert("Start date should not be greater than end date.")
                    return false
                }
                 if((start_date_val>=current_val)){
                    alert("Start date  should not greater than or equal to today")
                    return false
                } if((end_date_val>=current_val)){
                    alert("End date should not greater than or equal to today")
                    return false
                }

                if(target.find("input[name='start_date']").val()==''){
                alert("Please enter start date")
                return false
                }

                if($('#myModalCertificationCV').find("input[name='is_still']").is(":checked")){
                 var name = target.find("input[name='name']").val()
                var certification = target.find("input[name='certification']").val()
                var organization = target.find("input[name='organization']").val()
                var location = target.find("input[name='location']").val()
                var start_date = target.find("input[name='start_date']").val()
                var is_still =  true
                var end_date=target.find("input[name='end_date']").val()
                var grade = target.find("input[name='grade']").val()
                var opr_id = target.find("input[name='opr_id']").val()
                 var tr_no=parseInt(target.find("input[name='tr_no']").val())+1
                     if ($('.certificate_desc_table tr').length==2){
                        tr_no=1
                     }
                   var description = target.find("input[name='description']").val()
                 odoo.jsonRpc("/edit_certification_applicant", 'call', {
                        'name' : target.find("input[name='name']").val(),
                        'organization' : target.find("input[name='organization']").val(),
                        'certification' : target.find("input[name='certification']").val(),
                        'location' : target.find("input[name='location']").val(),
                        'start_date' : target.find("input[name='start_date']").val(),
                        'grade' : target.find("input[name='grade']").val(),
                        'is_still' : true,
                        'description' : target.find("input[name='description']").val(),
                        'id' : id
                    }).then(function(rec) {
                       $(".certificate_desc_table tr:eq("+$.trim(tr_no)+")").replaceWith("<tr><td>"+$.trim(name)+"</td><td>"+$.trim(start_date)+"</td><td>"+$.trim(end_date)+"</td><td>"+$.trim(grade)+"</td><td>"+$.trim(organization)+"</td><td>"+$.trim(certification)+"</td><td>"+$.trim(location)+"</td><td>"+$.trim(description)+"</td><td><button type='button' class='btn edit_certificate' data-toggle='modal' data-certification_id="+rec+"><i class='fa fa-pencil-square-o'></i></button><td><button type='button' data-certification_id="+rec+" class='btn delete_certificate'><i class='fa fa-trash'></i></button></td></tr>")
                     $('#myModalCertificationCV').modal('hide')
                    //  $( "#certificate_desc_table" ).load(window.location.href + " #certificate_desc_table" );
                    });
            }
            else{
             var name = target.find("input[name='name']").val()
                var certification = target.find("input[name='certification']").val()
                var organization = target.find("input[name='organization']").val()
                var location = target.find("input[name='location']").val()
                var start_date = target.find("input[name='start_date']").val()
                var is_still =  true
                var end_date=target.find("input[name='end_date']").val()
                var grade = target.find("input[name='grade']").val()
                var opr_id = target.find("input[name='opr_id']").val()
                var tr_no=parseInt(target.find("input[name='tr_no']").val())+1
                     if ($('.certificate_desc_table tr').length==2){
                        tr_no=1
                     }
if(target.find("input[name='start_date']").val()==''){
                alert("Please enter start date")
                return false
                }
                 var description = target.find("input[name='description']").val()
            odoo.jsonRpc("/edit_certification_applicant", 'call', {
                        'name' : target.find("input[name='name']").val(),
                        'organization' : target.find("input[name='organization']").val(),
                        'certification' : target.find("input[name='certification']").val(),
                        'location' : target.find("input[name='location']").val(),
                        'start_date' : target.find("input[name='start_date']").val(),
                        'grade' : target.find("input[name='grade']").val(),
                        'end_date' : target.find("input[name='end_date']").val(),
                        'description' : target.find("input[name='description']").val(),
                        'id' : id
                    }).then(function(rec) {
                       $(".certificate_desc_table tr:eq("+$.trim(tr_no)+")").replaceWith("<tr><td>"+$.trim(name)+"</td><td>"+$.trim(start_date)+"</td><td>"+$.trim(end_date)+"</td><td>"+$.trim(grade)+"</td><td>"+$.trim(organization)+"</td><td>"+$.trim(certification)+"</td><td>"+$.trim(location)+"</td><td>"+$.trim(description)+"</td><td><button type='button' class='btn edit_certificate' data-toggle='modal' data-certification_id="+rec+"><i class='fa fa-pencil-square-o'></i></button><td><button type='button' data-certification_id="+rec+" class='btn delete_certificate'><i class='fa fa-trash'></i></button></td></tr>")
                     $('#myModalCertificationCV').modal('hide')
                    //  $( "#certificate_desc_table" ).load(window.location.href + " #certificate_desc_table" );
                    });
            }
            }
            else{
             if(start_date_val>end_date_val){
                    alert("Start date should not be greater than end date.")
                    return false
                }
                 if((start_date_val>=current_val)){
                    alert("Start date  should not greater than or equal to today")
                    return false
                } if((end_date_val>=current_val)){
                    alert("End date should not greater than or equal to today")
                    return false
                }
                  if ( $.trim(target.find("input[name='name']").val()) &&
                $.trim(target.find("input[name='organization']").val()) &&
                $.trim(target.find("input[name='certification']").val()) &&
                $.trim(target.find("input[name='location']").val()) &&
                $.trim(target.find("input[name='start_date']").val()) &&
                $.trim(target.find("input[name='description']").val())){

 if($('#myModalCertificationCV').find("input[name='is_still']").is(":checked")){
 var name = target.find("input[name='name']").val()
                var certification = target.find("input[name='certification']").val()
                var organization = target.find("input[name='organization']").val()
                var location = target.find("input[name='location']").val()
                var start_date = target.find("input[name='start_date']").val()
                var is_still =  true
                var end_date=target.find("input[name='end_date']").val()
                var grade = target.find("input[name='grade']").val()
                var opr_id = target.find("input[name='opr_id']").val()
                 var tr_no = target.find("input[name='tr_no']").val()
                   var description = target.find("input[name='description']").val()
  odoo.jsonRpc("/add_certification", 'call', {
                        'name' : target.find("input[name='name']").val(),
                        'organization' : target.find("input[name='organization']").val(),
                        'certification' : target.find("input[name='certification']").val(),
                        'location' : target.find("input[name='location']").val(),
                        'grade' : target.find("input[name='grade']").val(),
                        'start_date' : target.find("input[name='start_date']").val(),
                        'is_still' : true,
                        'description' : target.find("input[name='description']").val(),
                    }).then(function(rec) {
                         $('.certificate_desc_table tr:last').after("<tr><td>"+$.trim(name)+"</td><td>"+$.trim(start_date)+"</td><td>"+$.trim(end_date)+"</td><td>"+$.trim(grade)+"</td><td>"+$.trim(organization)+"</td><td>"+$.trim(certification)+"</td><td>"+$.trim(location)+"</td><td>"+$.trim(description)+"</td><td><button type='button' class='btn edit_certificate' data-toggle='modal' data-certification_id="+rec+"><i class='fa fa-pencil-square-o'></i></button><td><button type='button' data-certification_id="+rec+" class='btn delete_certificate'><i class='fa fa-trash'></i></button></td></tr>")
                    $('#myModalCertificationCV').modal('hide')
                    // $( "#certificate_desc_table" ).load(window.location.href + " #certificate_desc_table" );
                    });
 }else{
   var certification = target.find("input[name='certification']").val()
   var name = target.find("input[name='name']").val()
                var organization = target.find("input[name='organization']").val()
                var location = target.find("input[name='location']").val()
                var start_date = target.find("input[name='start_date']").val()
                var is_still =  true
                var end_date=target.find("input[name='end_date']").val()
                var grade = target.find("input[name='grade']").val()
                var opr_id = target.find("input[name='opr_id']").val()
                var tr_no=parseInt(target.find("input[name='tr_no']").val())+1
                     if ($('.certificate_desc_table tr').length==2){
                        tr_no=1
                     }
                   var description = target.find("input[name='description']").val()
  odoo.jsonRpc("/add_certification", 'call', {
                        'name' : target.find("input[name='name']").val(),
                        'organization' : target.find("input[name='organization']").val(),
                        'certification' : target.find("input[name='certification']").val(),
                        'location' : target.find("input[name='location']").val(),
                        'grade' : target.find("input[name='grade']").val(),
                        'start_date' : target.find("input[name='start_date']").val(),
                        'end_date' : target.find("input[name='end_date']").val(),
                        'description' : target.find("input[name='description']").val(),
                    }).then(function(rec) {
                       $('.certificate_desc_table tr:last').after("<tr><td>"+$.trim(name)+"</td><td>"+$.trim(start_date)+"</td><td>"+$.trim(end_date)+"</td><td>"+$.trim(grade)+"</td><td>"+$.trim(organization)+"</td><td>"+$.trim(certification)+"</td><td>"+$.trim(location)+"</td><td>"+$.trim(description)+"</td><td><button type='button' class='btn edit_certificate' data-toggle='modal' data-certification_id="+rec+"><i class='fa fa-pencil-square-o'></i></button><td><button type='button' data-certification_id="+rec+" class='btn delete_certificate'><i class='fa fa-trash'></i></button></td></tr>")
                    $('#myModalCertificationCV').modal('hide')
                    // $( "#certificate_desc_table" ).load(window.location.href + " #certificate_desc_table" );
                    });

 }

                } else {
                    Dialog.alert(self, _t("Please Give Proper Values to the Input fields !"), {
                        title: _t('Alert!'),
                    });
                }
            }


        });

        // Click event For Deleting Certifications of the Applicants if Needed
        $(document).on('click', '.delete_certificate', function(e){
            e.stopPropagation();
            e.preventDefault();
             var ele= $(this)
            var id = parseInt($(this).data('certification_id_cv'));
            odoo.jsonRpc("/delete_certificate", 'call', {
              'id' : id,
            }).then(function () {
                ele.closest('tr').remove()
            });
        });

        // Click event For Editing Certifications of the Applicants if Needed
        $(document).on('click', '.edit_certificate', function(e){
            e.stopPropagation();
            e.preventDefault();
            $('#myModalCertificationCV').modal();
            var id = $(this).data('certification_id');
            var target = $(this).parents('#CertificationEditModal'+id);
            $('#myModalCertificationCV').find("h4[class='modal-title']").text("")
            $('#myModalCertificationCV').find("h4[class='modal-title']").text("Update Certification Details")

              $('#myModalCertificationCV').find("input[name='name']").val($.trim($(this).closest("tr").find('td:eq(0)').text()))
              $('#myModalCertificationCV').find("input[name='start_date']").val(getFormattedDate(new Date($.trim($(this).closest("tr").find('td:eq(1) span').text()))))
              $('#myModalCertificationCV').find("input[name='end_date']").val(getFormattedDate(new Date($.trim($(this).closest("tr").find('td:eq(2) span').text()))))
              $('#myModalCertificationCV').find("input[name='grade']").val($.trim($(this).closest("tr").find('td:eq(3)').text()))
              $('#myModalCertificationCV').find("input[name='organization']").val($.trim($(this).closest("tr").find('td:eq(4)').text()))
              $('#myModalCertificationCV').find("input[name='certification']").val($.trim($(this).closest("tr").find('td:eq(5)').text()))
              $('#myModalCertificationCV').find("input[name='location']").val($.trim($(this).closest("tr").find('td:eq(6)').text()))
              $('#myModalCertificationCV').find("input[name='description']").val($.trim($(this).closest("tr").find('td:eq(7)').text()))
              $('#myModalCertificationCV').find("input[name='operation_type']").val('update')
              $('#myModalCertificationCV').find("input[name='opr_id']").val(id)
$('#myModalCertificationCV').find("input[name='tr_no']").val($.trim($(this).closest("tr").index()))


                  if($(this).closest("tr").find('td:eq(2) span').text()){

              $('#myModalCertificationCV').find("input[name='is_still']").prop("checked", false);
                 $('#myModalCertificationCV').find('.end_date_div').show();
            }
            else
            {
               $('#myModalCertificationCV').find("input[name='is_still']").prop("checked", true);
               $('#myModalCertificationCV').find('.end_date_div').hide();
            }
        });

        // Click event For Adding Job Benefits  by Employers
        $(document).on('click', '#add_benefits', function(e){
            e.stopPropagation();
            e.preventDefault();
            if (!$("input[name='benefits']").val()){
                Dialog.alert(self, _t("Please add value in the Benefits input !"), {
                    title: _t('Alert!'),
                });
            } else {
//                $(this).hide();
                odoo.jsonRpc("/add_benefits", 'call', {
                    'job_id' : $("input[name='job_id']").val(),
                    'benefits' : $("input[name='benefits']").val(),
                }).then(function(result) {
                    if (result){
                    $("input[name='benefits']").val("")
                        Dialog.alert(self, _t("Job Benefits Has been successfully added for this job posting !"), {
                            confirm_callback: function() {
                            },
                            title: _t('Success!'),
                        });
                    } else {
                        Dialog.alert(self, _t("Job Benefits could not be added. Please try again !"), {
                            confirm_callback: function() {
                                location.reload();
                            },
                            title: _t('Alert'),
                        });
                    }
                });
            }
        });

        // Click event For Adding Job Requirements  by Employers
        $(document).on('click', '#add_job_requirements', function(e){
            e.stopPropagation();
            e.preventDefault();
            if (!$("input[name='job_requirements']").val()){
                Dialog.alert(self, _t("Please add value in the Job Requirements Input !"), {
                    title: _t('Alert!'),
                });
            } else {
//                $(this).hide();
                odoo.jsonRpc("/add_job_requirements", 'call', {
                    'job_id' : $("input[name='job_id']").val(),
                    'job_requirements' : $("input[name='job_requirements']").val(),
                }).then(function(result) {
                    if (result){
                    $("input[name='job_requirements']").val("")
                        Dialog.alert(self, _t("Job Requirement Has been successfully added for this job posting !"), {
                            confirm_callback: function() {

                            },
                            title: _t('Success!'),
                        });
                    } else {
                        Dialog.alert(self, _t("Job Requirement could not be added. Please try again !"), {
                            confirm_callback: function() {
                                location.reload();
                            },
                            title: _t('Alert'),
                        });
                    }
                });
            }
        });

        // Click event For Adding Job Location  by Employers
        $(document).on('click', '#add_job_location', function(e){
            e.stopPropagation();
            e.preventDefault();
            if (!$("input[name='job_location']").val()){
                Dialog.alert(self, _t("Please add value in the Job Location Input !"), {
                    title: _t('Alert!'),
                });
            } else {
//                $(this).hide();
                odoo.jsonRpc("/add_job_location", 'call', {
                    'job_id' : $("input[name='job_id']").val(),
                    'job_location' : $("input[name='job_location']").val(),
                }).then(function(result) {
                    if (result){
                    $("input[name='job_location']").val("")
                        Dialog.alert(self, _t("Job Location Has been successfully added for this job posting !"), {
                            confirm_callback: function() {
//                                location.reload();
                            },
                            title: _t('Success!'),
                        });
                    } else {
                        Dialog.alert(self, _t("Job Location could not be added. Please try again !"), {
                            confirm_callback: function() {
                                location.reload();
                            },
                            title: _t('Alert'),
                        });
                    }
                });
            }
        });

        // For Industry Practices Static Page JS
        $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
    });
});
