import werkzeug
from odoo.http import request
from odoo import http, _
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID, http, _
from dateutil.relativedelta import relativedelta
import base64
import logging
import json
_logger = logging.getLogger(__name__)
# import yaml


class CreateCv(http.Controller):

    @http.route('/cvmaker', type='http', auth='public', website=True)
    def show_custom_webpage(self, **kw):

        countries = request.env["res.country"].sudo().search([])
        states = request.env["res.country.state"].sudo().search([])
        _logger.info("----------------COUNTRY___________%s", states)
        return http.request.render('cv_maker.cv_maker_webPage', {"countries": countries, "states": states})

    @http.route(["/create/cv"], methods=['POST'], type="http", auth="public", website=True)
    def cvMaker(self, **post):
        name = post['name']
        email = post['email']
        mobile = post['mobile']
        street = post['street']
        gender = post['gender']
        marital = post['marital']
        birthday = post["birthday"]
        country_id = int(post["country_id"])
        street = post["street"]
        street2 = post["street2"]
        city = post["city"]
    #     state_id=(post["state_id"])
        zip = post["zip"]

        _logger.info("------------------------------------C---------------")
        _logger.info(
            "------------------------------------NAME---------------%s", name)
        _logger.info(
            "------------------------------------MOBILE---------------%s", mobile)
        _logger.info(
            "------------------------------------GENDER---------------%s", gender)

        s = request.env['res.partner'].sudo().create(
            {"name": name, "gender": gender, "email": email, "mobile": mobile, "marital": marital, "birthday": birthday,
             "country_id": country_id,  "street": street, "street2": street2, "city": city,  "zip": zip,
             })
        
        # env = request.env(user=SUPERUSER_ID)
        # partner = request.env.user.partner_id
        # _logger.info("----------------Partner___________%s",partner)
        # for data in partner:
        #      data.write({
        #    'academic_ids': [(6, 0, data.academic_ids.ids)],
        #    'experience_ids': [(6, 0, data.experience_ids.ids)],
        #    'certification_ids': [(6, 0, data.certification_ids.ids)],
        #  })
       