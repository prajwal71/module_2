# -*- coding: utf-8 -*-
{
    'name': "xlab_form",

    'summary': """
        It is for the registration anf validation of user""",

    'description': """
         It is for the registration anf validation of user
    """,

    'author': "Nexus Incoporation",
    'website': "http://nexusgurus.com",

  
    'category': 'Uncategorized',
    'version': '0.1',

    
    'depends': ['base','website','mail','contacts'],

    'data': [
        'security/ir.model.access.csv',
        'views/email_user.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/page_view_for_login_user.xml'
    ],
   
}
