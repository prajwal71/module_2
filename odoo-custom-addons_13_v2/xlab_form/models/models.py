# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo import SUPERUSER_ID

class xlab_form(models.Model):
    _name = 'xlab.form'
    _inherit = ['mail.thread','mail.activity.mixin']
    

    name = fields.Char(string="Name")
    partner_id =fields.Many2one('res.partner', 'Cusotmer',readonly=1)
    email= fields.Char(string="Email")
    mobile = fields.Char(string="Phone")   
    university= fields.Char(string="University/School")
    grade= fields.Char(string="Grade")
    type_selection = fields.Selection([('student', 'Student'),('dalit', 'Dalit'),('janjaati', 'janjaati')])
    user_product_pricelist = fields.Many2one('product.pricelist', 'Pricelist')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('validate', 'Validate'),
        ('cancel', 'Cancelled'),
    ], string='Status', readonly=True, copy=False, index=True, tracking=3, default='draft')
    

    def action_validate(self):

        for rec in self:
            if not rec.user_product_pricelist:
                raise ValidationError(_('Please Select the Pricelist You want to assign.'))

        if self.user_product_pricelist:
            self.partner_id.update({'property_product_pricelist':self.user_product_pricelist})
            
            template = self.env.ref('xlab_form.email_template_xlab_user_success_notif')
            if template:
                self.env['mail.template'].browse(template.id).send_mail(self.id)
            self.write({'state': 'validate'})

    
    def action_cancel(self):
        self.write({'state': 'cancel'})
        # self.env['mail.mail'].create({
        #     'body_html': 'body',
        #     'subject': 'User Request Unable to Verify',
        #     'email_to': self.email
        # }).send()
        template = self.env.ref('xlab_form.email_template_xlab_user_fail_notif')
        if template:
            self.env['mail.template'].browse(template.id).send_mail(self.id)
        
