# -*- coding: utf-8 -*-
from odoo.http import request
from odoo import http, _
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from dateutil.relativedelta import relativedelta
import base64
import logging
import json
_logger = logging.getLogger(__name__)
# import yaml
import werkzeug

class XlabForm(http.Controller):
    @http.route('/student-registration',  type='http', auth="user", website=True)
    def menucall(self, **kw):
        if request.env.user.partner_id != request.env.ref('base.public_partner'):
            name=request.env.user.partner_id.name
            email=request.env.user.partner_id.email
            mobile=request.env.user.partner_id.mobile
        
        return request.render("xlab_form.form_field", {'email': email,'name':name,'mobile':mobile})


    @http.route(['/submit/user/form'], type='http',methods=['POST'], auth="user", website=True)
    def backendfill(self, **post):
        name=post['name']
        # partner = request.env.user.partner_id
        partner_id=request.env.user.partner_id.id
        email=post['email']
        # mobile=post['mobile']
        # type_selection=post['type_selection']
        grade = post['grade']
        university = post['university']
        customer_obj = request.env['res.partner'].sudo().search([('id','=',partner_id)])
        render_values = {'name':name,
                         'email':email,
                        #  'mobile':mobile,
                        #  'type_selection':type_selection,
                         'grade':grade,
                         'university':university,
                         'partner_id': customer_obj.id,

                         }
        student_exist = request.env['xlab.form'].sudo().search([('partner_id','=', partner_id)])
        if student_exist:
            return request.render("xlab_form.registered_user_page_xlab", {})
        if not student_exist:
            student_regis=request.env['xlab.form'].sudo().create(render_values)
            if student_regis:
                if post.get('attachment',False):
                    Attachments = request.env['ir.attachment']
                    name = post.get('attachment').filename      
                    files = post.get('attachment').stream.read()
                    w_attachment_id = Attachments.sudo().create({
                        'name':name,
                        'type': 'binary',   
                        'res_model': 'xlab.form',
                        'res_id':student_regis.id,
                        'datas': base64.encodestring(files),
                    }) 
            return request.render("xlab_form.sucess_page_xlab", {})