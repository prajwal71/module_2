
from pickle import POP
from odoo import models, fields, api
from odoo.tools import float_compare, float_round, float_is_zero
import logging
from odoo.exceptions import AccessError, UserError
_logger = logging.getLogger(__name__)

class MrpSteelExt(models.Model):

    _inherit = 'mrp.production'
    
    @api.onchange('bom_id', 'product_id','product_uom_id')
    def _onchange_move_raw(self):
        # Clear move raws if we are changing the product. In case of creation (self._origin is empty),
        # we need to avoid keeping incorrect lines, so clearing is necessary too.
        if self.product_id != self._origin.product_id:
            self.move_raw_ids = [(5,)]
        if self.bom_id and self.product_qty > 0:
            # keep manual entries
            list_move_raw = [(4, move.id) for move in self.move_raw_ids.filtered(lambda m: not m.bom_line_id)]
            moves_raw_values = self._get_moves_raw_values()
            move_raw_dict = {move.bom_line_id.id: move for move in self.move_raw_ids.filtered(lambda m: m.bom_line_id)}
            for move_raw_values in moves_raw_values:
                if move_raw_values['bom_line_id'] in move_raw_dict:
                    # update existing entries
                    list_move_raw += [(1, move_raw_dict[move_raw_values['bom_line_id']].id, move_raw_values)]
                else:
                    # add new entries
                    list_move_raw += [(0, 0, move_raw_values)]
            self.move_raw_ids = list_move_raw
        else:
            self.move_raw_ids = [(2, move.id) for move in self.move_raw_ids.filtered(lambda m: m.bom_line_id)]


    def _generate_finished_moves(self):
        if self.product_id in self.bom_id.byproduct_ids.mapped('product_id'):
            raise UserError(_("You cannot have %s  as the finished product and in the Byproducts") % self.product_id.name)
        moves_values = [self._get_finished_move_value(self.product_id.id, self.product_qty, self.product_uom_id.id)]
        for byproduct in self.bom_id.byproduct_ids:
            product_uom_factor = self.product_uom_id._compute_quantity(self.product_qty, self.bom_id.product_uom_id)
            qty = byproduct.product_qty
            move_values = self._get_finished_move_value(byproduct.product_id.id,
                qty, byproduct.product_uom_id.id, byproduct.operation_id.id,
                byproduct.id)
            moves_values.append(move_values)
        moves = self.env['stock.move'].create(moves_values)
        return moves