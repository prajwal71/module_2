# -*- coding: utf-8 -*-
{
    'name': "mrp_steel_ext",

    'summary': """
        It doesnot allow quantity of compoment and by product to be change on pressing produce button""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Nexus Incorporation",
    'website': "nexusgurus.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','mrp'],

    # always loaded
    'data': [
 
    ],
    # only loaded in demonstration mode
    'demo': [
      
    ],
}
