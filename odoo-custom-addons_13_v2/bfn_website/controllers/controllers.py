# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################


from datetime import datetime
from odoo import http, tools, _
from odoo.http import request
from odoo.addons.website.controllers.main import Website


import logging
_logger = logging.getLogger(__name__)

class BfnWebsite(Website):
    @http.route(['/'], type='http', auth="public", website=True)
    def gift_card_page(self, **post):
        values = {'name': "Hello world"}
        products=request.env['hr.job'].sudo().search([])
        return request.render("bfn_website.hero_section", {'products':products,'values':values})
