# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ControllerConcept(models.Model):
    _name = 'controller_concept2'
    _description = 'how to use controller'

    name = fields.Char()
    email=fields.Char()
    number=fields.Char()

    # @api.depends('value')
    # def _value_pc(self):
    #     for record in self:
    #         record.value2 = float(record.value) / 100
