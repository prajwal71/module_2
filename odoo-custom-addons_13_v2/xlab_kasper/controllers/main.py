# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo.http import request
from odoo import http, _
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from dateutil.relativedelta import relativedelta
import base64
import logging
import json
_logger = logging.getLogger(__name__)
# import yaml
import werkzeug

class XlabPromHt(http.Controller):

    # @http.route(['/valid-serial-xlab'], type='json', auth="public", website=True)
    # def check_xlab_serial(self, **kw):
    #     _logger.info('-----------------FYI: - %s - This is happening-------------',kw.get('serial_no_ip'))
    #     cr, context, pool, uid = request.cr, request.context, request.registry, request.uid
    #     serial_no = kw.get('serial_no_ip')
    #     if serial_no:
    #             serial_obj = request.env['stock.production.lot'].sudo().search([('product_id','=',product_id),('name','=', serial_no)])
    #             if serial_obj:
    #                 return {
    #                         'result' : True,
    #                 }
    #             else:
    #                 return {
    #                         'result' : False,
    #                 }
    #     else:
    #         return {
    #                 'result' : False,
    #         }

    @http.route(['/register-product'], type='http', auth="user", website=True)
    def auth_product_xlab(self, page=1, **kwargs):
        _logger.info("--reached to controller--")
        if request.env.user.partner_id:
            name=request.env.user.partner_id.name
            email=request.env.user.partner_id.email
            mobile=request.env.user.partner_id.mobile
        _logger.info('-----------------TEST: - %s - This is happening-------------',email)
        products=request.env['product.product'].sudo().search([('under_warranty','=',True)])
        return request.render("xlab_kasper.xlab_prom", {'email': email,'name':name,'mobile':mobile,'products':products})
    
    # @http.route(['/get-customer-details'], type='http', auth="public", website=True)
    # def serial_guide(self, page=1, **post):
        
    #     name = post['name']
    #     phone = post['phone']
    #     email = post['email']
    #     company_name = post['company_name']
    #     zip1 = post['zip']
    #     city =  post['city']
    #     street = post['street']
    #     state_id = post['state_id']
    #     country_id = post['country_id']
    #     comment = post['comment']
    #     # serial_no = post['product_serial_id']
    #     # merchant = post['merchant']
    #     # product_id = post['product_id']
    #     # w_type = post['type']
        
        
    #     customer_obj = request.env['res.partner'].sudo().search([('customer','=', True),('name','=', name),('email','=',email)])
        
    #     if not customer_obj:
    #        k =  customer_obj.sudo().create({
	# 	    	'name': name,
	# 	    	'street': street,
	# 	    	'city': city,
    #             'phone':phone,
	# 	    	'zip': zip1,
	# 	    	'state_id': state_id,
	# 	    	'country_id': country_id,
	# 	    	# 'company_name': company_name,
	# 	    })
    #         prom_obj  = request.env['kasper.prom']

    #     # customer = []
    #     # customer_warranty_obj = request.env['res.partner'].sudo().search([('name','=', name)])
        


    #     return request.render("web_serial_check.serial_model_guide")
    # @http.route('/serial-model-guide/model-no/fetch', type='http', auth="public", website=True)
    # def serial_model_fetch(self, **kwargs):

    #     values = {}
    #     for field_name, field_value in kwargs.items():
    #         values[field_name] = field_value

    #     models_nos = request.env['product.template'].sudo().search([('product_brand_id','=', int(values['brand_id']) )])

    #     #Only return a dropdown if this category has subcategories
    #     return_string = ""

    #     if models_nos:
    #         return_string += "<div class=\"form-group col-lg-3\">\n"
    #         return_string += "    <label class=\"control-label\" for=\"model_no_id\">Model No.</label>\n"
    #         # return_string += "    <div class=\"col-md-7 col-sm-8\">\n"

    #         return_string += "        <select class=\"form-control js-example-basic-single\" id=\"model_no_id\" name=\"model_no_id\">\n"
    #         for my_models in request.env['product.template'].sudo().search([('product_brand_id','=', int(values['brand_id']) )]):
    #             return_string += "            <option value=\"" + str(my_models.id) + "\">" + my_models.name + "</option>\n"

    #         return_string += "        </select>\n"
    #         # return_string += "    </div>\n"
    #         return_string += "</div>\n"

        # return return_string
    @http.route(['/check-xlab-promo-details'], type='http', auth="user", website=True)
    def check_promo_code_xlab(self, page=1, **post):
        _logger.info("=====posttt===%s",post)
        partner_id=request.env.user.partner_id.id
        # pro_code = post['promo_code_id']
        if not post:
            return request.render('http_routing.404')
        name = post['name']
        phone = post['phone_number']
        email = post['email']
        purchase_date = post['purchase_date']
        # company_name = post['company_name']
        # zip1 = post['zip']
        # city =  post['city']
        product_id = post['product_id']
        serial_no = post['serial_no']
        # res_code = post['res_code']
        # street = post['address']
        attachment = post['attachment']
        accept1 = post['accept1']
        
        # state_id = post['state_id']
        # country_id = post['country_id']
        # comment = post['comment']
        # pro_brand = post['brand_id']
        # pro_brand_obj = request.env['product.brand'].sudo().search([('id','=', pro_brand)])
        # pro_serial = pro_serial.upper()
        # pro_name = yaml.load(pro_name)
        # pro_serial = yaml.load(pro_serial)
        # # _logger.info('-----------------FYI: This is happening-------------')
        # _logger.info(pro_name['product'])
        # _logger.info(pro_serial['reg_serial'])
        # name = 'Warranty'+'/'+pro_serial['reg_serial']
        # support_obj = request.env['website.support.ticket'].sudo().search([('subject','=', name)])
        # product_obj = request.env['product.product'].sudo().search([('name','=', pro_name['product'])]) 
        # prom_obj  = request.env['kasper.prom'].sudo().search([('kasper_pcode','=', pro_code),('state','=','draft')],limit=1)
        # temp_prod = request.env['prod.web.temp']
        # claim_obj = request.env['warranty.claim'].sudo().search([('name','=', name)])
        # rec_id = request.env['product.warranty'].search([('product_serial_id','=',pro_serial['reg_serial'])], limit=1)
        # serial_lot_ws = request.env['stock.production.lot'].sudo().search([('product_id','=',product_obj.id),('name','=', pro_serial['reg_serial'])])
        # warranty_serial_exist = request.env['website.support.ticket'].sudo().search([('product_serial_id','=', serial_obj.id)])
       
        # if prom_obj:
        customer_obj = request.env['res.partner'].sudo().search([('id','=',partner_id)])
        prom_obj = request.env['xlab.kasper.prom']
        ksp = False
        if not customer_obj:
            k =  customer_obj.sudo().create({
            'name': name,
            # 'street': street,
            'email': email,
            'mobile':phone,
            # 'zip': zip1,
            # 'state_id': state_id,
            # 'country_id': country_id,
            # 'company_name': company_name,
            })
            
            
            serial_obj = request.env['stock.production.lot'].sudo().search([('product_id','=',int(product_id)),('name','=', serial_no),('company_id','=', 1)],limit=1,order='create_date desc')
            _logger.info("=====dddd===%s",serial_obj) 
            # if serial_obj:
            ksp_exist = request.env['xlab.kasper.prom'].sudo().search([('lot_id.name','=', serial_no)])
            warranty_obj_exist = request.env['product.warranty'].sudo().search([('product_serial_id','=', serial_obj.id)])
            if warranty_obj_exist:
                return request.render("xlab_kasper.promo_code_failed_xlab_exist")
            if not ksp_exist:
                ksp = prom_obj.sudo().create({
                'lot_id': serial_obj.id,
                'partner_id': customer_obj.id,
                })
                if ksp:
                    if post.get('attachment',False):
                        Attachments = request.env['ir.attachment']
                        name = post.get('attachment').filename      
                        files = post.get('attachment').stream.read()
                        # rec_id = request.env['product.warranty'].search([], limit=1, order='create_date desc')
                        # attachment = files.read() 
                        attachment_id = Attachments.sudo().create({
                            'name':name,
                            # 'datas_fname':name,
                            'type': 'binary',   
                            'res_model': 'xlab.kasper.prom',
                            'res_id':ksp.id,
                            'datas': base64.encodestring(files),
                        }) 
            if not warranty_obj_exist:
                warranty_obj = request.env['product.warranty'].sudo().create({
                'product_serial_id': serial_obj.id,
                'partner_id': customer_obj.id,
                'purchase_date': purchase_date,
                'accept1': True,
                })
                if warranty_obj:
                    if post.get('attachment',False):
                        Attachments = request.env['ir.attachment']
                        name = post.get('attachment').filename      
                        files = post.get('attachment').stream.read()
                        # rec_id = request.env['product.warranty'].search([], limit=1, order='create_date desc')
                        # attachment = files.read() 
                        w_attachment_id = Attachments.sudo().create({
                            'name':name,
                            # 'datas_fname':name,
                            'type': 'binary',   
                            'res_model': 'product.warranty',
                            'res_id':warranty_obj.id,
                            'datas': base64.encodestring(files),
                        }) 
                    warranty_obj.send_notif_email_warranty()
            # else:
            #     return request.render("xlab_kasper.promo_code_failed_xlab")
        else:
            customer_obj.write({
            #    'email': email,
                'mobile':phone,})
            serial_obj = request.env['stock.production.lot'].sudo().search([('product_id','=',int(product_id)),('name','=', serial_no),('company_id','=',2)],limit=1,order='create_date desc')
            _logger.info("=====dddd===%s",serial_obj) 
            if serial_obj:
                ksp_exist = request.env['xlab.kasper.prom'].sudo().search([('lot_id.name','=', serial_no)])
                warranty_obj_exist = request.env['product.warranty'].sudo().search([('product_serial_id','=', serial_obj.id)])
                if warranty_obj_exist:                                       
                    return request.render("xlab_kasper.promo_code_failed_xlab_exist")
                if not ksp_exist:
                    ksp = prom_obj.sudo().create({
                    'lot_id': serial_obj.id,
                    'partner_id': customer_obj.id,
                    })
                    if ksp:
                        if post.get('attachment',False):
                            Attachments = request.env['ir.attachment']
                            name = post.get('attachment').filename      
                            files = post.get('attachment').stream.read()
                            # rec_id = request.env['product.warranty'].search([], limit=1, order='create_date desc')
                            # attachment = files.read() 
                            attachment_id = Attachments.sudo().create({
                                'name':name,
                                # 'datas_fname':name,
                                'type': 'binary',   
                                'res_model': 'xlab.kasper.prom',
                                'res_id':ksp.id,
                                'datas': base64.encodestring(files),
                            }) 
                if not warranty_obj_exist:
                    warranty_obj = request.env['product.warranty'].sudo().create({
                    'product_serial_id': serial_obj.id,
                    'partner_id': customer_obj.id,
                    'purchase_date': purchase_date,
                    'accept1': True,
                    })
                    if warranty_obj:
                        if post.get('attachment',False):
                            Attachments = request.env['ir.attachment']
                            name = post.get('attachment').filename      
                            files = post.get('attachment').stream.read()
                            # rec_id = request.env['product.warranty'].search([], limit=1, order='create_date desc')
                            # attachment = files.read() 
                            w_attachment_id = Attachments.sudo().create({
                                'name':name,
                                # 'datas_fname':name,
                                'type': 'binary',   
                                'res_model': 'product.warranty',
                                'res_id':warranty_obj.id,
                                'datas': base64.encodestring(files),
                            })
                        warranty_obj.send_notif_email_warranty()
            else:
                return request.render("xlab_kasper.promo_code_failed_xlab")
        if ksp:
            ksp.send_notif_email_xlab()
        return request.render("xlab_kasper.promo_code_success_xlab")
        
        
   

