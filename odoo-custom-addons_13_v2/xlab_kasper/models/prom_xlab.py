# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
import requests
import re
import json
from odoo.exceptions import UserError

class XlabKasperskyProm(models.Model):
    _name = "xlab.kasper.prom"
    _description = "Xlab Kaspersky Scheme"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char('Name',required=True, copy=False, readonly=True, states={'draft': [('readonly', False)]}, index=True, default=lambda self: _('New'))
    lot_id = fields.Many2one('stock.production.lot',string='Serial Number',required=True)
    lot_product_id = fields.Many2one('product.product',related='lot_id.product_id',string='Product')
    comment = fields.Char('Customer Feedback')

    partner_id = fields.Many2one('res.partner','Customer')
    product_id = fields.Many2one('product.product','Kaspersky Product')
    windows_product_id = fields.Many2one('product.product','Windows Product')
    kasper_key_used = fields.Char('Kaspersky Key',readonly=True)
    windows_key_used = fields.Char('Windows Key',readonly=True)
    state = fields.Selection([('draft', 'Draft'), ('claim', 'Claimed'),('cancel','Cancelled')], readonly=True, default='draft', copy=False, string="Status",track_visibility='onchange')
    cancel_res = fields.Char('Cancel Reason', track_visibility='onchange',readonly=True)
    

    def send_notif_email_xlab(self):
        template = self.env.ref('xlab_kasper.email_template_xlab_kasper_success_notif')
        if template:
            self.env['mail.template'].browse(template.id).send_mail(self.id)

    def state_update_key_cancel(self,reason):
        # kp = False
        template = self.env.ref('xlab_kasper.email_template_xlab_kasper_failed')
        if template:
            self.env['mail.template'].browse(template.id).send_mail(self.id)
        self.write({'state':'cancel','cancel_res':reason})

    def state_update_key(self,kasper_key_used,windows_key_used):
        if kasper_key_used and windows_key_used:
            self.write({'state':'claim','kasper_key_used':kasper_key_used, 'windows_key_used':windows_key_used})
            template = self.env.ref('xlab_kasper.email_template_xlab_kasper')
            if template:
                self.env['mail.template'].browse(template.id).send_mail(self.id)
        else:
            raise UserError(_('Please check Kaspersky Key/ Windows key'))

        
    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('xlab.promotion.seq') or _('New')

        result = super(XlabKasperskyProm, self).create(vals)
        return result        

    def unlink(self):
        if self.filtered(lambda x: x.state in ('draft','claim', 'cancel')):
            raise UserError(_('You can not delete a Xlab Promotion Form.'))
        return super(XlabKasperskyProm, self).unlink()

class GetKeyDoneExt(models.TransientModel):
    _name = "get.key.done"
    
    password_key = fields.Char('Password',required=True)
    kasper_key_used = fields.Char('Kaspersky Key',required=True)
    windows_key_used = fields.Char('Windows Key',required=True)

    
    def action_set_key(self):
        context = dict(self._context or {})
        active_id = context.get('active_id', False)
        if active_id:
            cur_prom = self.env['xlab.kasper.prom'].browse(active_id)
            if self.password_key == 'xlab_kasper_admin':
                exist_key = self.env['xlab.kasper.prom'].search([('kasper_key_used','=',self.kasper_key_used)])
                exist_key_windows = self.env['xlab.kasper.prom'].search([('windows_key_used','=',self.windows_key_used)])
                if not exist_key and not exist_key_windows:
                    cur_prom.state_update_key(self.kasper_key_used,self.windows_key_used)
                else:
                    raise UserError(_('Kaspersky Key already Used'))
            else:
                raise UserError(_('Please check Password'))

class GetKeyNotDoneExt(models.TransientModel):
    _name = "get.key.not.done"
    
    reason = fields.Char('cancel Reason',required=True)

    
    def action_not_set_key(self):
        context = dict(self._context or {})
        active_id = context.get('active_id', False)
        if active_id:
            cur_prom = self.env['xlab.kasper.prom'].browse(active_id)
            cur_prom.state_update_key_cancel(self.reason)
           

class Website(models.Model):
    _inherit = 'website'

    def get_country_list(self):            
        country_ids=self.env['res.country'].search([])
        return country_ids
        
    def get_state_list(self):            
        state_ids=self.env['res.country.state'].search([])
        return state_ids
        
    