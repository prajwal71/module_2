# -*- coding: utf-8 -*-
from odoo import models, fields, api


class HelpdeskSupportType(models.Model):
    _inherit='helpdesk.ticket'
    
    inquiry_type=fields.Selection([('general_inquiry','General Inquiry'),('product_inquiry','Product Inquiry')])
    serial_number=fields.Selection([('yes','Yes'),('no','No')],string="Have serial number")
    serial_number_input=fields.Char(string="Serial Number")
    
#     _description = 'website_form_valid.website_form_valid'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100


    
    
