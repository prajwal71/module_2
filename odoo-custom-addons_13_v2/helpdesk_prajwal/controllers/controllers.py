  # -*- coding: utf-8 -*-
from odoo import http,tools,_
from odoo.http import request
from odoo.addons.website_helpdesk_form.controller.main import WebsiteForm

import logging
_logger = logging.getLogger(__name__)

# from odoo.addons.website_form.controllers.main import WebsiteForm
class WebsiteFormInherit(WebsiteForm):
  
  
    @http.route('''/helpdesk/<model("helpdesk.team", "[('use_website_helpdesk_form','=',True)]"):team>/submit''', type='http', auth="public", website=True)
    def website_helpdesk_form(self, team, **kwargs):
      res=super(WebsiteFormInherit,self).website_helpdesk_form(team, **kwargs)
      products=request.env['product.product'].sudo().search([('website_published','=',True)])
      track_product=request.env['product.product'].sudo().search(['|',('tracking','=','lot'),('tracking','=','serial')])
      res.qcontext.update({'products':products,'track_product':track_product})
      return res
    
    # @http.route('/website_form/<string:model_name>', type='http', auth="public", methods=['POST'], website=True)
    # def website_form(self, model_name, **kwargs):
    #   res=super(WebsiteFormInherit,self).website_form(model_name, **kwargs)
    #   res.qcontext.update  ({'product.id':product.id})
    #   res.qcontext.update  ({'inquiry_type':inquiry})
    #   return res
        

