from odoo import http,tools,_
from odoo.http import request
from odoo.addons.website_hr_recruitment.controllers.main import WebsiteHrRecruitment

import logging
_logger = logging.getLogger(__name__)

class WebsiteHrRecruitmentInherit(WebsiteHrRecruitment):
    
    @http.route('''/jobs/apply/<model("hr.job", "[('website_id', 'in', (False, current_website_id))]"):job>''', type='http', auth="public", website=True)
    def jobs_apply(self, job, **kwargs):

        res = super(WebsiteHrRecruitmentInherit,self).jobs_apply( job, **kwargs)
        horoscope = request.env['horoscope'].sudo().search([])
        blood_group  = request.env['blood.group'].sudo().search([])
        _logger.info('-----------------TEST: - %s - This is happening-------------',horoscope)
        res.qcontext.update({'horoscope':horoscope,'blood_group':blood_group})
        return res
        