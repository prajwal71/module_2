# -*- coding: utf-8 -*-
{
    'name': "applicant_field_ext",
    'summary': """
        Added Nepali Horoscope and Blood Group in Applicant""",
    'description': """
        Added Nepali Horoscope and Blood Group in Applicant
    """,
    'author': "Nexus Incorporation",
    'website': "http://nexusgurus.com",
    'category': 'Applicant',
    'version': '0.1',
    'depends': ['base', 'hr_recruitment', 'hr','website_hr_recruitment','website'],
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/blood_group.xml',
        'views/horoscope.xml',
        # 'views/template.xml'
    ],
}
