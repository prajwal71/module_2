# -*- coding: utf-8 -*-

from pickle import POP
from odoo import models, fields, api
from odoo.tools import float_compare, float_round, float_is_zero
import logging
from odoo.exceptions import AccessError, UserError
_logger = logging.getLogger(__name__)

class BomExt(models.Model):

    _inherit = 'mrp.production'
  
    byproduct_custom = fields.One2many('mrp.bom.byproduct','bypro', string="By-Product")
    manufacturing_type = fields.Selection([('pre','Billet Cutting'),('core','Steel Manufacturing')],string="Manufacturing Tyoe",default='pre')
   
   

   
    @api.onchange('bom_id')
    def cal(self):

        records = self.bom_id.byproduct_ids
        _logger.info("=Prajwal==during==save====123-=====%s",self)
        _logger.info("=Prajwal===records===123-=====%s",records)
        if records:
            
            for rec in records:
                self.byproduct_custom = rec +self.byproduct_custom
        else:
            self.byproduct_custom = None
            

    def write(self,vals):
        res = super(BomExt,self).write(vals)
        _logger.info("EDIT======CALL")
        a =  self.byproduct_custom
        _logger.info("EDIT==AFTRER===%s===CALL",a)
        records = self.bom_id.byproduct_ids
        if a!=records:
            # self.bom_id.byproduct_ids.unlink()
            for rec in a:
                self.bom_id.byproduct_ids = self.bom_id.byproduct_ids + a 
                # records=records+a
            _logger.info("EDIT======CALL======================%s",self.bom_id.byproduct_ids)
        return res
    @api.model 
    def create(self,vals):
        manu_type = vals.get('manufacturing_type')
        _logger.info("============%s=======BOM_EXT=======",manu_type)
        if manu_type == 'pre':
            if vals.get('name', 'New') == 'New':
                vals['name'] = self.env['ir.sequence'].next_by_code(
                    'pre.sequence') or 'New'
            _logger.info("===================BOM_EXT==22=====")
        if manu_type == 'core':
            if vals.get('name', 'New') == 'New':
                vals['name'] = self.env['ir.sequence'].next_by_code(
                    'core.sequence') or 'New'
        
        res = super(BomExt,self).create(vals)
       
       
   
        _logger.info("EDIT===SAVE===CALL")
        a =  res.byproduct_custom
        _logger.info("EDIT==AFTRER===%s===CALL",a)
        records = res.bom_id.byproduct_ids
        _logger.info("EDIT==AFTRER==BACKEND    =%s===CALL",records)
        if a!=records:
            b = self.env['mrp.bom']
            c = b.byproduct_ids
            for rec in a:
                # b = self.env['mrp.bom']
                # c = b.byproduct_ids
                # _logger.info("EDIT======CALL==========ccc============%s",c)
                c = c + a
               
                # records=records+a
            res.bom_id.byproduct_ids = c 
            _logger.info("EDIT======CALL======================%s",res.bom_id.byproduct_ids)
        return res



    # def open_produce_product(self):
    #     for rec in self:
    #         records = rec.bom_id
    #         _logger.info("EDIT======CALL======================%s",rec.byproduct_custom.product_qty)
    #         # records.product_qty = rec.product_qty
    #         # records.byproduct_ids.product_qty = rec.byproduct_custom.product_qty * rec.product_qty
    #         rec.byproduct_custom.product_qty  = rec.byproduct_custom.product_qty  * (records.product_qty/rec.product_qty)
    #         _logger.info("EDIT======CALL======================%s",rec.byproduct_custom.product_qty)
    #         f = 100/10
    #         _logger.info("=================divide============%s",f)
    #     res = super(BomExt,self).open_produce_product()
            
    #     return res
            


    def post_inventory(self):
            for order in self:
                # In case the routing allows multiple WO running at the same time, it is possible that
                # the quantity produced in one of the workorders is lower than the quantity produced in
                # the MO.
                if order.product_id.tracking != "none" and any(
                    wo.state not in ["done", "cancel"]
                    and float_compare(wo.qty_produced, order.qty_produced, precision_rounding=order.product_uom_id.rounding) == -1
                    for wo in order.workorder_ids
                ):
                    raise UserError(
                        _(
                            "At least one work order has a quantity produced lower than the quantity produced in the manufacturing order. "
                            + "You must complete the work orders before posting the inventory."
                        )
                    )

                if not any(order.move_raw_ids.mapped('quantity_done')):
                    raise UserError(_("You must indicate a non-zero amount consumed for at least one of your components"))

                moves_not_to_do = order.move_raw_ids.filtered(lambda x: x.state == 'done')
                moves_to_do = order.move_raw_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
                for move in moves_to_do.filtered(lambda m: m.product_qty == 0.0 and m.quantity_done > 0):
                    move.product_uom_qty = move.quantity_done
                # MRP do not merge move, catch the result of _action_done in order
                # to get extra moves.
                # moves_to_do = moves_to_do._action_done()
                # moves_to_do = order.move_raw_ids.filtered(lambda x: x.state == 'done') - moves_not_to_do
                # order._cal_price(moves_to_do)
                moves_to_finish = order.move_finished_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
                # moves_to_finish = moves_to_finish._action_done()
                # order.workorder_ids.mapped('raw_workorder_line_ids').unlink()
                # order.workorder_ids.mapped('finished_workorder_line_ids').unlink()
                order.action_assign()
                consume_move_lines = moves_to_do.mapped('move_line_ids')
                for moveline in moves_to_finish.mapped('move_line_ids'):
                    if moveline.move_id.has_tracking != 'none' and moveline.product_id == order.product_id or moveline.lot_id in consume_move_lines.mapped('lot_produced_ids'):
                        if any([not ml.lot_produced_ids for ml in consume_move_lines]):
                            raise UserError(_('You can not consume without telling for which lot you consumed it'))
                        # Link all movelines in the consumed with same lot_produced_ids false or the correct lot_produced_ids
                        filtered_lines = consume_move_lines.filtered(lambda ml: moveline.lot_id in ml.lot_produced_ids)
                        moveline.write({'consume_line_ids': [(6, 0, [x for x in filtered_lines.ids])]})
                    else:
                        # Link with everything
                        moveline.write({'consume_line_ids': [(6, 0, [x for x in consume_move_lines.ids])]})
            return True           
        
class MrpBomByproductExt(models.Model):
    _inherit='mrp.bom.byproduct'
    
    bypro = fields.Many2one("mrp.production",string="By-Product")
    
    
# class WorkOrderExt(models.Model):
#     _inherit='mrp.bom.byproduct'

