# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request


class JobPageCustom(http.Controller):
    @http.route('/css', type='http',methods=['GET','POST'], auth="public", website=True)
    def index(self, **kw):
       return request.render("job_page_custom.test_css", {})

#     @http.route('/job_page_custom/job_page_custom/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('job_page_custom.listing', {
#             'root': '/job_page_custom/job_page_custom',
#             'objects': http.request.env['job_page_custom.job_page_custom'].search([]),
#         })

#     @http.route('/job_page_custom/job_page_custom/objects/<model("job_page_custom.job_page_custom"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('job_page_custom.object', {
#             'object': obj
#         })
