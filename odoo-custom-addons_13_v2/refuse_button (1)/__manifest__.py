# -*- coding: utf-8 -*-
{
    'name': "refuse_button",

    'summary': """
      Make Refuse button visible in time leave approval""",

    'description': """
        Make Refuse button visible in time leave approval
    """,

    'author': "Nexus Incoporation",
    'website': "http://nexusgurus.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','mail'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        # 'views/templates.xml',
    ],
    # only loaded in demonstration mode
   
}
