# See LICENSE file for full copyright and licensing details.
from datetime import *
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class ResPartnerPortalExt(models.Model):
    """This class is defined to enhance ResUsers Class to enhance portal."""

    _inherit = 'res.partner'

    gender = fields.Selection([('male', 'Male'), ('female', 'Female')],
                              string="Gender")
    birthday = fields.Date('Date of Birth')

    marital = fields.Selection(
        [('single', 'Single'),
         ('married', 'Married'),
         ('widower', 'Widower'),
         ('divorced', 'Divorced')],
        'Marital Status')

    
    country_id = fields.Many2one('res.country')
    is_same_address = fields.Boolean('Same as Correspondence Address')
    street_ht = fields.Char('Street')
    street2_ht = fields.Char('Street2')
    city_ht = fields.Char('City')
    zip_ht = fields.Char('Zip')
    state_id_ht = fields.Many2one('res.country.state', string="State")
class HrAcademic(models.Model):
    _name = 'hr.academic'
    _inherit = 'hr.curriculum'
    _description = 'Academic experiences'
    
    study_field = fields.Char(string='Field of study', translate=True, )
    activities = fields.Text(string='Activities and associations',
                             translate=True)

    @api.constrains('start_date', 'end_date')
    def validate_dates(self):
        if not self.env.context.get('website_id'):
            if self.end_date:
                today = date.today()
                for dates in self:
                    if dates.start_date > today:
                        raise ValidationError(_('Start date (%s) should be less than Current Date in Academic Experience') % dates.start_date)
                    if dates.end_date > today:
                        raise ValidationError(_('End date (%s) should be less than Current Date in Academic Experience') % dates.end_date)
                    if dates.start_date > dates.end_date:
                        raise ValidationError(_('End date (%s) should be greater than Start Date (%s) in Academic Experience') % dates.start_date, dates.end_date)

    def write(self, vals):
        if vals.get("is_still"):
            vals.update({'end_date': None})
        result = super(HrAcademic, self).write(vals)
        return result
