# -*- coding: utf-8 -*-
##########################################################################
#
#    Copyright (c) 2019-Present Lekhnath Rijal <mail@lekhnathrijal.com.np>
#
##########################################################################


{
    'name': 'Nepali Date System',
    'summary': 'Nepali datepicker and date conversion tool for Odoo',
    'version': '13.0.6.0',
    'author': 'Lekhnath Rijal<mail@lekhnathrijal.com.np>',
    'category': 'Web',
    'description': '''
Nepali Datepicker
Nepali Date System
Bikram Sambat
Bikram Samvat
Odoo Nepali Date
Odoo datepicker
Nepali Calendar
Nepali Date
BS Calendar
BS Date
Nepali date converter
BS date converter
backend
    ''',
    'depends': [
        'base',
        'web',
    ],
    'data': [
        'data/config.xml',

        'views/res_config.xml',
        'views/assets.xml',
    ],
    'qweb':[
        'static/src/xml/datepicker.xml',
    ],
    'images': [
        'static/description/main_screenshot.png',
    ],
    'price': 199,
    'currency': 'EUR',
    'license': 'OPL-1',
    'auto_install': False,
    'installable': True,
    'application':False,
}
