# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Attachment Export13',
    'version' : '1.1',
    'summary': 'Attachment export import',
    'description': """

====================

    """,
    'author': 'Nexus',
    'depends' : ['base','web'],
    'data': [
            'views/view.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
