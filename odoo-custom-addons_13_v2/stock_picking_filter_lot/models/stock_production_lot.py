# Copyright 2018 Simone Rubino - Agile Business Group
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import api, fields, models
from datetime import date, timedelta
import logging
_logger = logging.getLogger(__name__)


class StockProductionLot(models.Model):
    _inherit = "stock.production.lot"

    location_ids = fields.Many2many(
        comodel_name="stock.location", compute="compute_location_ids_custom_ext", store=True
    )
    # qunt = fields.Float(realted='quant_ids.inventory_quantity')

    @api.depends("quant_ids", "quant_ids.location_id","quant_ids.inventory_quantity")
    def compute_location_ids_custom_ext(self):
        _logger.info("============prajwal-===unquie==============================")
        for lot in self:
            lot.location_ids = lot.quant_ids.filtered(lambda l: l.quantity > 0).mapped("location_id")
        
    # @api.onchange('quant_ids.inventory_quantity')
    # def hellocall(self):
    #    _logger.info("=======hello==========calll")
    
    # def _prajwal(self):
    #     now =  fields.Datetime.now()
    #     _logger.info("hello========%s========bye====",now)