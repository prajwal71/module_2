# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.tools.float_utils import float_round


class HrLeaveChange(models.Model):
    _inherit = 'hr.leave'
    _description = 'time off for minute also'
    
    hr1 = fields.Selection([
        ('1','1'),('2','2'),
        ('3','3'),('4','4'),
        ('5','5'),('6','6'),
        ('7','7'),('8','8'),
        ('9','9'),('10','10'),
        ('11','11'),('12','12')],string="Hour")
    minut1 = fields.Selection([
        ('0','00'),('1','01'),('2','02'),('3','03'),('4','04'),('5','05'),('6','06'),('7','07'),('8','08'),('9','09'),('10','10'),
        ('11','11'),('12','12'),('13','13'),('14','14'),('15','15'),('16','16'),('17','17'),('18','18'),('19','19'),('20','20'),
        ('21','21'),('22','22'),('23','23'),('24','24'),('25','25'),('26','26'),('27','27'),('28','28'),('29','29'),('30','30'),
        ('31','31'),('32','32'),('33','33'),('34','34'),('35','35'),('36','36'),('37','37'),('38','38'),('39','39'),('40','40'),
        ('41','41'),('42','42'),('43','43'),('44','44'),('45','45'),('46','46'),('47','47'),('48','48'),('49','49'),('50','50'),
        ('51','51'),('52','52'),('53','53'),('54','54'),('55','55'),('56','56'),('57','57'),('58','58'),('59','59')],string="Min")
    ampm1 = fields.Selection([('am', 'Am'),('pm','Pm')])
    hr2 =  fields.Selection([
        ('1','1'),('2','2'),
        ('3','3'),('4','4'),
        ('5','5'),('6','6'),
        ('7','7'),('8','8'),
        ('9','9'),('10','10'),
        ('11','11'),('12','12')],string="Hour")
    minut2 = fields.Selection([
        ('0','00'),('1','01'),('2','02'),('3','03'),('4','04'),('5','05'),('6','06'),('7','07'),('8','08'),('9','09'),('10','10'),
        ('11','11'),('12','12'),('13','13'),('14','14'),('15','15'),('16','16'),('17','17'),('18','18'),('19','19'),('20','20'),
        ('21','21'),('22','22'),('23','23'),('24','24'),('25','25'),('26','26'),('27','27'),('28','28'),('29','29'),('30','30'),
        ('31','31'),('32','32'),('33','33'),('34','34'),('35','35'),('36','36'),('37','37'),('38','38'),('39','39'),('40','40'),
        ('41','41'),('42','42'),('43','43'),('44','44'),('45','45'),('46','46'),('47','47'),('48','48'),('49','49'),('50','50'),
        ('51','51'),('52','52'),('53','53'),('54','54'),('55','55'),('56','56'),('57','57'),('58','58'),('59','59')],string="Min")
    ampm2 = fields.Selection([('am', 'Am'),('pm','Pm')])
    request_hour_from = fields.Float(compute="calHourFrom")
    request_hour_to = fields.Float(compute="calHourTo")
    
    @api.depends('hr1','minut1','ampm1')   
    def calHourFrom(self):   
        for rec in self:
            hr1=int(rec.hr1)
            minut1=int(rec.minut1)
            if rec.ampm1=="am":
                if hr1==12:
                    hrc=float((minut1)/60)
                    rec.request_hour_from =str(hrc)
                    
                    
                else:
                    hrc=float((((hr1)*60)+(minut1)))
                    request_hour_from1 =float((hrc/60))
                    rec.request_hour_from =str(request_hour_from1)
                    
            else:
                if hr1==12:
                    hrc=float((((hr1)*60)+(minut1)))
                    request_hour_from1 =float((hrc/60))
                    rec.request_hour_from =str(request_hour_from1)
                    
                else:
                    hrc=float(((((hr1+12))*60)+(minut1)))
                    request_hour_from1 =float((hrc/60))
                    rec.request_hour_from =str(request_hour_from1)
         
    @api.depends('hr2','minut2','ampm2')   
    def calHourTo(self):   
        for rec in self:
            hr2=int(rec.hr2)
            minut2=int(rec.minut2)
            if rec.ampm2=="am":
                if hr2==12:
                    hrc=float((minut2)/60)
                    rec.request_hour_to =str(hrc)
                    
                    
                else:
                    hrc=float((((hr2)*60)+(minut2)))
                    request_hour_to1 =float((hrc/60))
                    rec.request_hour_to =str(request_hour_to1)
                    
            else:
                if hr2==12:
                    hrc=float((((hr2)*60)+(minut2)))
                    request_hour_to1 =float((hrc/60))
                    rec.request_hour_to =str(request_hour_to1)
                    
                else:
                    hrc=float(((((hr2+12))*60)+(minut2)))
                    request_hour_to1 =float((hrc/60))
                    rec.request_hour_to =str(request_hour_to1)
            
                
            
    @api.depends('number_of_hours_display', 'number_of_days_display')
    def _compute_duration_display(self):
        for leave in self:
            leave.duration_display = '%g %s' % (
                (float_round(leave.number_of_hours_display, precision_digits=5)
                if leave.leave_type_request_unit == 'hour'
                else float_round(leave.number_of_days_display, precision_digits=6)),
                _('hours') if leave.leave_type_request_unit == 'hour' else _('days'))
