# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.

from psycopg2 import OperationalError, Error

from odoo import api, fields, models, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from odoo.osv import expression
from odoo.tools.float_utils import float_compare, float_is_zero, float_round
import logging
_logger = logging.getLogger(__name__)


class PickingCustomExt(models.Model):
    _inherit = "stock.picking"
   



    def unreserve_button(self):
            quants = self.env["stock.quant"].search([('company_id','=',1)])
            # _logger.info("====QAUNT========%s=====",quants)
    

            move_line_ids = []

            warning = ""

            for quant in quants:
                move_lines = self.env["stock.move.line"].search(

    
                        [

                            ("product_id", "=", quant.product_id.id),

                            ("location_id", "=", quant.location_id.id),
                            ("reference","=",self.name),

                            ("lot_id", "=", quant.lot_id.id),

                            ("package_id", "=", quant.package_id.id),

                            ("owner_id", "=", quant.owner_id.id),

                            ("product_qty", "!=", 0),

                        ]

                    )
                # _logger.info("============%s====move===",move_lines)

                move_line_ids += move_lines.ids
                # _logger.info("============%s====move_line_ids===",move_line_ids)

                reserved_on_move_lines = sum(move_lines.mapped("product_qty"))

                # _logger.info("============%s====reserved_on_move_lines===",reserved_on_move_lines)

                move_line_str = str.join(

                    ", ", [str(move_line_id) for move_line_id in move_lines.ids]

                )



                if quant.location_id.should_bypass_reservation():

                    # If a quant is in a location that should bypass the reservation, its `reserved_quantity` field

                    # should be 0.
                    # _logger.info("=========if=========case")

                    if quant.reserved_quantity != 0:
                        _logger.info("=========if=========case")

                        quant.write({"reserved_quantity": 0})

                else:
                    _logger.info("=========esle=========case")

                    # If a quant is in a reservable location, its `reserved_quantity` should be exactly the sum

                    # of the `product_qty` of all the partially_available / assigned move lines with the same

                    # characteristics.

                    if quant.reserved_quantity == 0:

                        if move_lines:
                            _logger.info("=========esle==1111=======case")

                            move_lines.with_context(bypass_reservation_update=True).write(

                                {"product_uom_qty": 0}

                            )

                    elif quant.reserved_quantity < 0:
                        _logger.info("=========esle====222222=====case")

                        quant.write({"reserved_quantity": 0})

                        if move_lines:
                            _logger.info("=========esle=333333========case")

                            move_lines.with_context(bypass_reservation_update=True).write(

                                {"product_uom_qty": 0}

                            )

                    else:
                        _logger.info("=========esle=====444444====case")
                        if reserved_on_move_lines != quant.reserved_quantity:
                            _logger.info("=========esle=====555555555555555555555555====case")

                            move_lines.with_context(bypass_reservation_update=True).write(

                                {"product_uom_qty": 0}

                            )

                            quant.write({"reserved_quantity": 0})

                        else:
                            _logger.info("=========esle=====666666666666666666666666666====case")

                            if any(move_line.product_qty < 0 for move_line in move_lines):
                                _logger.info("=========esle==555555=======case") 
                                _logger.info("=========esle=====7777777777777777777777777777777====case")

                                move_lines.with_context(bypass_reservation_update=True).write(

                                    {"product_uom_qty": 0}

                                )

                                quant.write({"reserved_quantity": 0})



            move_lines = self.env["stock.move.line"].search(

                [

                    ("product_id.type", "=", "product"),

                    ("product_qty", "!=", 0),

                    ("id", "in", move_line_ids),

                ]

            )
            _logger.info("=====%s====esle=====666666666666666666666666666====case",move_lines)




            move_lines_to_unreserve = []



            for move_line in move_lines:

                if not move_line.location_id.should_bypass_reservation():

                    move_lines_to_unreserve.append(move_line.id)



            if len(move_lines_to_unreserve) > 1:

                self.env.cr.execute(

                    """ 

                        UPDATE stock_move_line SET product_uom_qty = 0, product_qty = 0 WHERE id in %s ;

                    """

                    % (tuple(move_lines_to_unreserve),)

                )

            elif len(move_lines_to_unreserve) == 1:

                self.env.cr.execute(

                    """ 

                    UPDATE stock_move_line SET product_uom_qty = 0, product_qty = 0 WHERE id = %s ;

                    """

                    % (move_lines_to_unreserve[0])

                )


class StockQuantExtCustom(models.Model):
    _inherit = 'stock.quant'

    @api.model
    def _update_reserved_quantity(self, product_id, location_id, quantity, lot_id=None, package_id=None, owner_id=None, strict=False):
        """ Increase the reserved quantity, i.e. increase `reserved_quantity` for the set of quants
        sharing the combination of `product_id, location_id` if `strict` is set to False or sharing
        the *exact same characteristics* otherwise. Typically, this method is called when reserving
        a move or updating a reserved move line. When reserving a chained move, the strict flag
        should be enabled (to reserve exactly what was brought). When the move is MTS,it could take
        anything from the stock, so we disable the flag. When editing a move line, we naturally
        enable the flag, to reflect the reservation according to the edition.

        :return: a list of tuples (quant, quantity_reserved) showing on which quant the reservation
            was done and how much the system was able to reserve on it
        """
        _logger.info("=========================================new====SB===========")
        self = self.sudo()
        rounding = product_id.uom_id.rounding
        quants = self._gather(product_id, location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=strict)
        reserved_quants = []

        if float_compare(quantity, 0, precision_rounding=rounding) > 0:
            # if we want to reserve
            available_quantity = sum(quants.filtered(lambda q: float_compare(q.quantity, 0, precision_rounding=rounding) > 0).mapped('quantity')) - sum(quants.mapped('reserved_quantity'))
            if float_compare(quantity, available_quantity, precision_rounding=rounding) > 0:
                raise UserError(_('It is not possible to reserve more products of %s than you have in stock.') % product_id.display_name)
        elif float_compare(quantity, 0, precision_rounding=rounding) < 0:
            # if we want to unreserve
            available_quantity = sum(quants.mapped('reserved_quantity'))
            if float_compare(abs(quantity), available_quantity, precision_rounding=rounding) > 0:
                action_fix_unreserve = self.env.ref(
                    'stock.stock_quant_stock_move_line_desynchronization', raise_if_not_found=False)
#                 if action_fix_unreserve and self.user_has_groups('base.group_system'):
#                     raise RedirectWarning(
#                         _("""It is not possible to unreserve more products of %s than you have in stock.
# The correction could unreserve some operations with problematics products.""") % product_id.display_name,
#                         action_fix_unreserve.id,
#                         _('Automated action to fix it'))
#                 else:
#                     raise UserError(_('It is not possible to unreserve more products of %s than you have in stock. Contact an administrator.') % product_id.display_name)
        else:
            return reserved_quants

        for quant in quants:
            if float_compare(quantity, 0, precision_rounding=rounding) > 0:
                max_quantity_on_quant = quant.quantity - quant.reserved_quantity
                if float_compare(max_quantity_on_quant, 0, precision_rounding=rounding) <= 0:
                    continue
                max_quantity_on_quant = min(max_quantity_on_quant, quantity)
                quant.reserved_quantity += max_quantity_on_quant
                reserved_quants.append((quant, max_quantity_on_quant))
                quantity -= max_quantity_on_quant
                available_quantity -= max_quantity_on_quant
            else:
                max_quantity_on_quant = min(quant.reserved_quantity, abs(quantity))
                quant.reserved_quantity -= max_quantity_on_quant
                reserved_quants.append((quant, -max_quantity_on_quant))
                quantity += max_quantity_on_quant
                available_quantity += max_quantity_on_quant

            if float_is_zero(quantity, precision_rounding=rounding) or float_is_zero(available_quantity, precision_rounding=rounding):
                break
        return reserved_quants