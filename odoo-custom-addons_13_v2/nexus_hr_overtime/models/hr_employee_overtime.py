# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
from datetime import date, datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

class HRContractInherit(models.Model):
    _inherit = 'hr.contract'

    ot_rate_normal = fields.Float(string='OT Rate Normal')
    ot_rate_weekend = fields.Float(string='OT Rate Weekend')


class hr_employee_overtime(models.Model):
    _name= "hr.employee.overtime"
    _inherit = ["mail.activity.mixin", "mail.thread"]
    _description = 'HR Employee Overtime'
    _order = 'id desc'
    _rec_name = 'employee_id'


    @api.depends('ot_line_ids.estimated_hr')
    def _compute_total_hour_overtime(self):
        weekend_hour = normal_hour = total_hour = 0.00
        for rec in self:
            for lines in rec.ot_line_ids:
                if lines.estimated_hr:
                    if lines.ot_type == 'normal':
                        normal_hour += lines.estimated_hr
                    else:
                        weekend_hour += lines.estimated_hr
            rec.normal_hour = normal_hour
            rec.weekend_hour = weekend_hour 
            rec.total_hour = normal_hour + weekend_hour 




    name = fields.Char(string="Name")
    employee_id = fields.Many2one('hr.employee', string="Employee")
    date = fields.Date(string="Date", default=date.today())
    # based_on = fields.Selection([('weekday', 'Weekday'),
    #                              ('weekend', 'Weekend')],'Based On')
    state = fields.Selection([('draft', 'Draft'),
                              ('confirm', 'Confirm'),
                              ('under_approval', 'In Approval'),
                              ('approved', 'Approved'),
                              ('paid', 'Paid'),
                              ('cancelled', 'Cancelled')], default='draft', string="State", track_visibility="onchange")
    
    total_hour = fields.Float(string="Total Hour",compute="_compute_total_hour_overtime",store=True,readonly=True)

    
    payslip_id = fields.Many2one('hr.payslip', string="Related Payslip")
    weekend_hour = fields.Float(string="Weekend Hour",compute="_compute_total_hour_overtime",store=True) 
    normal_hour = fields.Float(string="Normal Hour",compute="_compute_total_hour_overtime",store=True)
    ot_line_ids = fields.One2many('hr.employee.overtime.line','hr_employee_ot_id',string="Overtime Lines")



    @api.model
    def create(self, vals):
        ot_name = self.env['ir.sequence'].next_by_code('hr.employee.overtime')
        if ot_name:
            vals.update({'name': ot_name})
        return super(hr_employee_overtime, self).create(vals)

    @api.model
    def create(self, vals):
        if not vals.get('name') or vals['name'] == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('hr.employee.overtime') or _('New')
        return super(hr_employee_overtime, self).create(vals)

    def unlink(self):
        for rec in self:
            if rec.state != 'draft':
                raise Warning(_('You cannot delete record on %s state' % (rec.state)))

    def emp_overtime_submit(self):
        self.ensure_one()

        self.state = 'confirm'
        if self.employee_id.parent_id:
                create_vals = {
                    'activity_type_id': '4',
                    'summary': 'HR Overtime',
                    'automated': True,
                    'date_deadline': fields.Date.today(),
                    'note': "HR Overtime Requested",
                    'res_model_id': self.env['ir.model'].search([('model', '=', 'hr.employee.overtime')]).id,
                    'res_id': self.id,
                    'user_id': self.employee_id.parent_id.id
                }
                self.env['mail.activity'].create(create_vals)

    def emp_overtime_under_approval(self):
        self.ensure_one()
        #check if manager either emloyee manager or hr_manager
        # if self.employee_id.parent_id == self.env.uid :
        self.state = 'under_approval'
        if self.employee_id.sudo().department_id.responsible_hr_overtime:
            for sec in self.employee_id.sudo().department_id.responsible_hr_overtime:
                create_vals = {
                    'activity_type_id': '4',
                    'summary': 'HR Aprroval',
                    'automated': True,
                    'date_deadline': fields.Date.today(),
                    'note': "HR Approved",
                    'res_model_id': self.env['ir.model'].search([('model', '=', 'hr.employee.overtime')]).id,
                    'res_id': self.id,
                    'user_id': sec.id
                }
                self.env['mail.activity'].create(create_vals)

    def emp_overtime_under_approve(self):
        self.ensure_one()
        #check if manager either emloyee manager or hr_manager
        self.state = 'approved'
        if self.employee_id.parent_id:
            create_vals = {
                'activity_type_id': '4',
                'summary': 'Overtime Approval Reminder',
                'automated': True,
                'date_deadline': fields.Date.today(),
                'res_model_id': self.env['ir.model'].search([('model', '=', 'hr.employee.overtime')]).id,
                'res_id': self.id,
                'user_id': self.employee_id.parent_id.user_id
            }
            self.env['mail.activity'].create(create_vals)



    def emp_overtime_make_paid(self):
        self.ensure_one()
        #check if manager either emloyee manager or hr_manager
        self.state = 'paid'

    def emp_overtime_cancel(self):
        self.ensure_one()
        # attendance_ids = self.env['hr.attendance'].search([('employee_ot_id', '=', self.id)])
        # if attendance_ids:
        #     attendance_ids.update({'employee_ot_id': False})
        # self.state = 'cancelled'

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

class hr_employee_overtime_lines(models.Model):
    _name= "hr.employee.overtime.line"

    date = fields.Date(string="Date", default=date.today())
    description = fields.Char(string="Description",required=True)
    estimated_hr = fields.Float(string="Total Hour",required=True)
    ot_type = fields.Selection([ ('normal', 'Normal'),('weekend', 'Weekend'),],'Type', default='normal',required=True)
    hr_employee_ot_id = fields.Many2one('hr.employee.overtime',string="Employee Overtime")


class HRDepOvertimeInherited(models.Model):
    _inherit = "hr.department"

    responsible_hr_overtime = fields.Many2many('res.users','responsible_overtime_hr','responsible_overtime_hr_res_users', string='Hr overtime')

