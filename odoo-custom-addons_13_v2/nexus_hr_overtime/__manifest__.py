# -*- coding: utf-8 -*-
{
    'name': 'Nexus HR OVERTIME',
    'category': 'HR',
    'summary': 'Manage Employee Loan',
    'author': 'Nexus Incorporation Pvt. Ltd.',
    'depends': ['base','mail',
                'hr_payroll_community', 'hr','hr_contract'
                
                ],
    'data': [
        
        #overtime
        'data/overtime_seq.xml',
        'security/ir.model.access.csv',
        'views/hr_employee_contract_views.xml',
        'views/hr_employee_overtime_views.xml',
    ],
   
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
