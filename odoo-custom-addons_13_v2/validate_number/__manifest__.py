# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
{
  "name"                 :  "Number Validation On website",
  "summary"              :  """Mobile number validate.""",
  "category"             :  "Website",
  "version"              :  "1.0.2",
  "sequence"             :  1,
  "author"               :  "Nexus Incoporation",
  "license"              :  "Other proprietary",
  "website"              :  "https://nexusgurus.com",
  "description"          :  """Mobile number validation""",
  "depends"              :  ['auth_signup','website','portal','account_sign_up_details'],
  "data"                 :  [
                             'views/javascript_css_link.xml',
                             'views/number_validation.xml',
                            ],
  "application"          :  False,
  "installable"          :  True,
  "auto_install"         :  False,
}
