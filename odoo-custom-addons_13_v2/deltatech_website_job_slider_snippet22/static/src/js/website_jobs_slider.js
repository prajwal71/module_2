odoo.define("deltatech_website_job_slider_snippet.job_slider", function (require) {
    "use strict";

    var concurrency = require("web.concurrency");
    var config = require("web.config");
    var core = require("web.core");
    var publicWidget = require("web.public.widget");
    var utils = require("web.utils");
    var wSaleUtils = require("website_sale.utils");

    var qweb = core.qweb;

    publicWidget.registry.jobsSliderSnippet = publicWidget.Widget.extend({
        selector: ".s_wsale_jobs_slider",
        xmlDependencies: ["/deltatech_website_job_slider_snippet/static/src/xml/website_jobs_slider.xml"],
        disabledInEditableMode: false,
        read_events: {
            "click .js_add_cart": "_onAddToCart",
        },

        /**
         * @class
         */
        init: function () {
            this._super.apply(this, arguments);
            this._dp = new concurrency.DropPrevious();
            this.uniqueId = _.uniqueId("o_carousel_jobs_slider_");
            this._onResizeChange = _.debounce(this._addCarousel, 100);
        },
        /**
         * @override
         */
        start: function () {
            this._dp.add(this._fetch()).then(this._render.bind(this));
            $(window).resize(() => {
                this._onResizeChange();
            });
            return this._super.apply(this, arguments);
        },
        /**
         * @override
         */
        destroy: function () {
            this._super(...arguments);
            this.$el.addClass("d-none");
            this.$el.find(".slider").html("");
        },

        _fetch: function () {
            return this._rpc({
                route: "/shop/jobs/slider",
                params: {
                    list_id: this.$target.data("id"),
                },
            }).then((res) => {
                var jobs = res.jobs;

                // In edit mode, if the current visitor has no recently viewed
                // jobs, use demo data.
                if (this.editableMode && (!jobs || !jobs.length)) {
                    return {
                        jobs: [
                            {
                                id: 0,
                                website_url: "#",
                                display_name: "job 1",
                                price: '$ <span class="oe_currency_value">750.00</span>',
                            },
                            {
                                id: 0,
                                website_url: "#",
                                display_name: "job 2",
                                price: '$ <span class="oe_currency_value">750.00</span>',
                            },
                            {
                                id: 0,
                                website_url: "#",
                                display_name: "job 3",
                                price: '$ <span class="oe_currency_value">750.00</span>',
                            },
                            {
                                id: 0,
                                website_url: "#",
                                display_name: "job 4",
                                price: '$ <span class="oe_currency_value">750.00</span>',
                            },
                        ],
                    };
                }

                return res;
            });
        },

        _render: function (res) {
            var jobs = res.jobs;
            var mobilejobs = [],
                webjobs = [],
                jobsTemp = [];
            _.each(jobs, function (job) {
                if (jobsTemp.length === 4) {
                    webjobs.push(jobsTemp);
                    jobsTemp = [];
                }
                jobsTemp.push(job);
                mobilejobs.push([job]);
            });
            if (jobsTemp.length) {
                webjobs.push(jobsTemp);
            }

            this.mobileCarousel = $(
                qweb.render("deltatech_website_job_slider_snippet.jobsSlider", {
                    uniqueId: this.uniqueId,
                    jobFrame: 1,
                    jobsGroups: mobilejobs,
                })
            );
            this.webCarousel = $(
                qweb.render("deltatech_website_job_slider_snippet.jobsSlider", {
                    uniqueId: this.uniqueId,
                    jobFrame: 4,
                    jobsGroups: webjobs,
                })
            );
            this._addCarousel();
            this.$el.toggleClass("d-none", !(jobs && jobs.length));
        },
        /**
         * Add the right carousel depending on screen size.
         * @private
         */
        _addCarousel: function () {
            var carousel = config.device.size_class <= config.device.SIZES.SM ? this.mobileCarousel : this.webCarousel;
            this.$(".slider").html(carousel).css("display", "");
            // TODO removing the style is useless in master
        },

        // --------------------------------------------------------------------------
        // Handlers
        // --------------------------------------------------------------------------

        /**
         * Add job to cart and reload the carousel.
         * @private
         * @param {Event} ev
         */
        _onAddToCart: function (ev) {
            var self = this;
            var $card = $(ev.currentTarget).closest(".card");
            this._rpc({
                route: "/shop/cart/update_json",
                params: {
                    job_id: $card.find("input[data-job-id]").data("job-id"),
                    add_qty: 1,
                },
            }).then(function (data) {
                wSaleUtils.updateCartNavBar(data);
                var $navButton = wSaleUtils.getNavBarButton(".o_wsale_my_cart");
                var fetch = self._fetch();
                var animation = wSaleUtils.animateClone(
                    $navButton,
                    $(ev.currentTarget).parents(".o_carousel_job_card"),
                    25,
                    40
                );
                Promise.all([fetch, animation]).then(function (values) {
                    self._render(values[0]);
                });
            });
        },
    });

    publicWidget.registry.jobsSliderUpdate = publicWidget.Widget.extend({
        selector: "#job_detail",
        events: {
            'change input.job_id[name="job_id"]': "_onjobChange",
        },
        debounceValue: 8000,

        /**
         * @class
         */
        init: function () {
            this._super.apply(this, arguments);
            this._onjobChange = _.debounce(this._onjobChange, this.debounceValue);
        },

        // --------------------------------------------------------------------------
        // Private
        // --------------------------------------------------------------------------

        /**
         * Debounced method that wait some time before marking the job as viewed.
         * @private
         * @param {HTMLInputElement} $input
         */
        _updatejobView: function ($input) {
            var jobId = parseInt($input.val(), 10);
            var cookieName = "seen_job_id_" + jobId;
            if (!parseInt(this.el.dataset.viewTrack, 10)) {
                // Is not tracked
                return;
            }
            if (utils.get_cookie(cookieName)) {
                // Already tracked in the last 30min
                return;
            }
            if ($(this.el).find(".js_job.css_not_available").length) {
                // Variant not possible
                return;
            }
            this._rpc({
                route: "/shop/jobs/recently_viewed_update",
                params: {
                    job_id: jobId,
                },
            }).then(function (res) {
                if (res && res.visitor_uuid) {
                    utils.set_cookie("visitor_uuid", res.visitor_uuid);
                }
                utils.set_cookie(cookieName, jobId, 30 * 60);
            });
        },

        // --------------------------------------------------------------------------
        // Handlers
        // --------------------------------------------------------------------------

        /**
         * Call debounced method when job change to reset timer.
         * @private
         * @param {Event} ev
         */
        _onjobChange: function (ev) {
            this._updatejobView($(ev.currentTarget));
        },
    });
});
