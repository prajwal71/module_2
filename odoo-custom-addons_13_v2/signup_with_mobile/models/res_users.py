from odoo import api, fields, models, _


class Users(models.Model):
    _inherit = "res.users"

    mobile = fields.Char(required=True, help="Used to log into the system")