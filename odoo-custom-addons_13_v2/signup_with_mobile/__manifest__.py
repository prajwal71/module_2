{
    'name': 'Signup With Mobile Number',
    'category': 'Website',
    'summary': 'Signup With Mobile (If not Email).',
    'author': 'Nexus Incorporation',
    'website': 'www.sagarnetwork.com',
    'version': '13.0',
    'description': """

    This module can create users from the Odoo main screen as an external user and signup with the same credentials. 
    It mainly provides validation of email_id and mobile number during signup. 
    User id has to be  unique for every account so email id and mobile number cannot be duplicated.

        """,
    'depends': ['auth_signup','base'],
    'data': [
        'views/auth_signup_inherit.xml',
        'views/res_users.xml',
    ],
    'installable': True,
    'application': True,
}
