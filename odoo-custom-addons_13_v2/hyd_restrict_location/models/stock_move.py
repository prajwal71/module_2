# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import ValidationError, Warning
import logging
_logger = logging.getLogger(__name__)

TEMP_MOVE_RESTRICT = _("""Only the users: %s \n """
                       """are allowed to do transfert %s this location""")


class StockMoveExt(models.Model):
    """."""

    _inherit = 'stock.move'

    def _action_done(self, cancel_backorder=False):
        """."""
        res = super(StockMoveExt, self)._action_done(cancel_backorder)
        uid = self.env.user.id
        # _logger.info("========%s===========self====",self)
        for record in self:
            srcl = record.location_id
            _logger.info("========%s===========self====",srcl)
            dstl = record.location_dest_id

            if srcl.allowed_users and uid not in srcl.allowed_users.ids:
                allowed = ','.join(srcl.allowed_users.mapped("name"))
                raise ValidationError(TEMP_MOVE_RESTRICT % (allowed, "from"))

            if dstl.allowed_users and uid not in dstl.allowed_users.ids:
                allowed = ','.join(dstl.allowed_users.mapped("name"))
                raise ValidationError(TEMP_MOVE_RESTRICT % (allowed, "to"))
            
            # res = super(StockMove, record)._action_done(cancel_backorder)
            return res
       
    @api.model
    def create(self, vals):
        res = super(StockMoveExt, self).create(vals)

        a = self.env.user.stock_location
    
     
        _logger.info('====latest==latest====%s=======user===id=========',vals.get('origin'))
        if vals.get('origin'):
            if 'S' in vals.get('origin'):
                _logger.info("=========latest==latest====sale===========order====================")
                if a:
                    res.location_id = a.id
                
        return res
         
        
# class PickingExtCustom(models.Model):
#     _inherit = "stock.picking"
    
#     def button_validate(self):
#         res = super(PickingExtCustom,self).button_validate()
#         return res