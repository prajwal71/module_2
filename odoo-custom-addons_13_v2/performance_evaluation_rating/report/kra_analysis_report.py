# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from odoo import tools
from odoo import api, fields, models

class KRAnalysisReport(models.Model):
    _name = "kra.analysis.report"
    _description = "KRA Analysis Report"
    _auto = False

    week_nu = fields.Integer(string="Week Number", readonly=True)
    month = fields.Integer(string="Month", readonly=True)
    year = fields.Char(string="Year", readonly=True)
    employee_id = fields.Many2one('hr.employee', string="Employee", readonly=True)
    department_id = fields.Many2one('hr.department', string="Department", readonly=True)
    job_id = fields.Many2one('hr.job', string="Job Position", readonly=True)
    reviewer_plan = fields.Selection(string="Reviewer Plan Selection",
                                               selection=[("week", "Weekly"), ("month", "Monthly"), ("year", "Yearly")])
    self_total = fields.Float(string="Employee Rating", readonly=True)
    manager_total = fields.Float(string="Manager Rating", readonly=True)
    hr_total = fields.Float(string="HR Rating", readonly=True)
    final_score = fields.Float(string="Final Score", readonly=True)
    total_score_percent = fields.Float(string="Final Score in %", readonly=True)
    total_score_percent_point = fields.Float(string="Final Score Point", readonly=True)
    validate_date = fields.Date('Validate Date', readonly=True)
    start_date_id = fields.Date(related='employee_id.x_service_start_date', string="Joining Date")
    contract_start_id = fields.Date(related='employee_id.x_studio_new_contract_start_date', string="New Contract Start Date")                     
    total_qsn = fields.Integer(string="Total Number of Question",readonly=True)
    full_mark = fields.Float(string="Full Mark",readonly=True)   

    def _query(self, with_clause='', fields={}, groupby='', from_clause=''):
        with_ = ("WITH %s" % with_clause) if with_clause else ""

        select_ = """
            min(kqr.id) as id,
            CASE WHEN sum(CAST(kqr.self_rating AS INTEGER)) is not null THEN sum(CAST(kqr.self_rating AS INTEGER)) ELSE 0 END as self_total,
            CASE WHEN sum(CAST(kqr.manager_rating AS INTEGER)) is not null THEN sum(CAST(kqr.manager_rating AS INTEGER)) ELSE 0 END as manager_total,
            CASE WHEN sum(CAST(kqr.hr_rating AS INTEGER)) is not null THEN sum(CAST(kqr.hr_rating AS INTEGER)) ELSE 0 END as hr_total,
            sum(CASE WHEN CAST(kqr.self_rating AS INTEGER) is not null THEN CAST(kqr.self_rating AS INTEGER) ELSE 0 END+
                CASE WHEN CAST(kqr.manager_rating AS INTEGER) is not null THEN CAST(kqr.manager_rating AS INTEGER) ELSE 0 END+
                CASE WHEN CAST(kqr.hr_rating AS INTEGER) is not null THEN CAST(kqr.hr_rating AS INTEGER) ELSE 0 END) as final_score,
            ke.department_id as department_id,
            ke.job_id as job_id,
            ke.reviewer_plan,
            ke.employee_id as employee_id,
            ke.month as month,
            ke.year as year,
            ke.validate_date as validate_date,
            ke.week_nu as week_nu,
            ke.total_score_percent as total_score_percent,
            ke.total_score_percent_point as total_score_percent_point,
            ke.total_qsn as total_qsn,
            ke.full_mark as full_mark
        """
        for field in fields.values():
            select_ += field

        from_ = """
                   kra_question_rating kqr
                   join kra_evaluation ke on (kqr.rating_id=ke.id)
                   join hr_employee emp on ke.employee_id = emp.id
                   join hr_department dept on ke.department_id = dept.id
                   join hr_job job on ke.job_id = job.id
                    %s
               """ % from_clause

        groupby_ = """
                    ke.employee_id,
                    ke.month,
                    ke.year,
                    ke.week_nu,
                    ke.validate_date,
                    ke.reviewer_plan,
                    ke.department_id,
                    ke.total_score_percent,
                    ke.total_score_percent_point,
                    ke.total_qsn,
                    ke.full_mark,
                    ke.job_id %s
                """ % (groupby)

        return '%s (SELECT %s FROM %s WHERE ke.employee_id IS NOT NULL GROUP BY %s)' % (with_, select_, from_, groupby_)

    
    def init(self):
        # self._table = sale_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (%s)""" % (self._table, self._query()))
