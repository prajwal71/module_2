# -*- coding: utf-8 -*-
{
    'name': 'Performance Evaluation Rating',
    'category': 'HR',
    'summary': 'Performance Appraisal and Rating',
    'author': 'Nexus Incorporation Pvt Ltd',
    'depends': ['base', 'stock', 'account',
                'hr_payroll_community', 'hr', 'hr_attendance',
                'hr_contract', 'hr_recruitment', 'project', 'hr_timesheet','hr_expense'
                ],
    'data': [
        
        #performance evaluation rating
        'security/security.xml',
        'security/ir.model.access.csv',
        # 'views/res_config_setting.xml',
        'views/kra_template.xml',
        # 'views/kra_schedule_plan.xml',
        'views/kra_evaluation.xml',
        'wizard/kra_wizard.xml',
        'wizard/department_wizard.xml',
        'report/report.xml',
        'report/kra_evaluation_report.xml',
        'report/kra_analysis_report.xml',

    ],
    'external_dependencies': {
        'python': [
            'numpy'
        ],
    },
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
