# -*- coding: utf-8 -*-


from odoo import models,fields,api,_
import logging
_logger = logging.getLogger(__name__)

class kraTemplatewizard(models.TransientModel):
    _name = 'kra.template.wiz'
    _description = 'KRA Template Wizard'

    department_employee_ids = fields.Many2many('hr.employee',string="Department Employees")
    

    # department_id = fields.Many2one('hr.department',string="Choose Department",required=True)

    def create_bulk_appraisal(self):
        for emp in self.department_employee_ids:

            template_id = self.env['kra.template'].search([('job_id', '=', emp.job_id.id),('department_id', '=', emp.department_id.id),('is_active', '=', True)])
            if template_id:
                kra_template_id = template_id
                kra_eval = self.env['kra.evaluation'].search([('employee_id','=',emp.id)])
                if not kra_eval:
                    kra = self.env['kra.evaluation'].create(
                    {
                        'employee_id': emp.id,
                        'kra_template_id': kra_template_id.id
                    })
                    kra._change_kra_template()
                else:
                    for rec in kra_eval:
                        if rec and rec.state == 'done':
                            _logger.info('========================%s',rec)
                            other_kra = self.env['kra.evaluation'].create(
                            {
                                'employee_id': emp.id,
                                'kra_template_id': kra_template_id.id
                            })
                            other_kra._change_kra_template()  


        return {
            'name': _("KRA Evaluation"),
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            # 'view_id': self.env.ref('performance_evaluation_rating.view_kra_evaluation_tree').id,
            'res_model': 'kra.evaluation',
            'type': 'ir.actions.act_window',
            # 'domain': '[]',
            # 'res_id': self.id,
            # 'target': 'new'
        }

    # @api.depends('department_id')
    # def get_all_department_employees(self):
        
    #         # _logger.info('===========a==================')
            
    #         # EmpObj = self.env['hr.employee'].search([('department_id','=',self.department_id.id)])
    #         # for emp in EmpObj:
    #         #     self.meeting_ids = [(4, [emp.id])]
    #         # _logger.info('===========a==================%s',EmpObj)

    #         self.write({
    #                         'department_emp_ids' : [(6,0,[20,3])]
    #                     })


