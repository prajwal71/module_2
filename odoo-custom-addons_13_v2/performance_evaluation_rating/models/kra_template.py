# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, Warning
import logging
_logger = logging.getLogger(__name__)


class kratemplate(models.Model):
    _name = 'kra.template'
    _inherit = ['mail.thread']
    _description = 'KRA Template'

    name = fields.Char(string="Name", required=True)
    department_id = fields.Many2one('hr.department', string="Department", required=True)
    job_id = fields.Many2one('hr.job', string="Job Position", required=True)
    active = fields.Boolean(string='Active', default=True)
    is_active = fields.Boolean(string="Is Active")
    questions_ids = fields.One2many('kra.question','kr_tmpl_id', string="KRA Questions")
    department_temp_employee_ids = fields.Many2many('hr.employee',string="Department Employees")
    
    def action_create_department_appraisal(self):
        # context = dict(self._context or {})
        # _logger.info('===================act=================%s',context)

        # active_id = context.get('active_id', False)
        # _logger.info('===================act=================%s',active_id)
        employee_list = []
        # if active_id:
            # empObj = self.env['kra.template'].browse(active_id)
            # if empObj:
        empjob_obj = self.env['hr.employee'].search([('job_id','=',self.job_id.id),('department_id', '=', self.department_id.id),('active','=',True)])
        if empjob_obj:
            for lst in empjob_obj:
                employee_list.append(lst.id)
            _logger.info('====================================%s',employee_list)
            self.department_temp_employee_ids = [(6,0,employee_list)]
            _logger.info('====================================%s',self.department_temp_employee_ids)
        return {
            'name': _("Department Wizard"),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            # 'view_id': self.env.ref('performance_evaluation_rating.view_action_create_department_appraisal_form').id,
            'res_model': 'kra.template.wiz',
            'type': 'ir.actions.act_window',
            # 'domain': '[]',
            'context': {'default_department_employee_ids': self.department_temp_employee_ids.ids},
            # 'res_id': self.id,
            'target': 'new'
        }
    
    @api.constrains('is_active')
    def _check_is_active(self):
        if self.is_active == True:
            kra_template = self.env['kra.template'].search([('department_id','=',self.department_id.id),
                                                            ('job_id','=',self.job_id.id),
                                                            ('is_active','=',True),('id','!=',self.id)])
            if kra_template:
                raise Warning(_('You already have active template for job position "%s" for "%s" department' %(self.job_id.name, self.department_id.name)))


class kraquestion(models.Model):
    _name = 'kra.question'
    _description = 'KRA Template Questions'
    

    kr_tmpl_id = fields.Many2one('kra.template', string="KRA Questions")
    name = fields.Char(string="Questions")
    # questions = fields.Char(string="Questions")
    description = fields.Char(string="Description")
    display_type = fields.Selection([
        ('line_section', 'Section'),
        ('line_note', 'Note'),
    ], default=False, help="Technical field for UX purpose.")    
    sequence = fields.Integer()