# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, Warning
from lxml import etree
from datetime import datetime, timedelta
import logging
_logger = logging.getLogger(__name__)


class kraevaluation(models.Model):
    _name = 'kra.evaluation'
    _inherit = ['mail.thread']
    _rec_name = 'employee_id'

    week_nu = fields.Integer(string="Week Number")
    month = fields.Integer(string="Month")
    year  = fields.Char(string="Year")
    employee_id = fields.Many2one('hr.employee', string="Employee", required=True)
    reviewer_id = fields.Many2one('hr.employee', string="Reviewer")
    # department_id = fields.Many2one('hr.department', string="Department", required=True)
    # job_id = fields.Many2one('hr.job', string="Job Position", required=True)
    job_id = fields.Many2one(related='employee_id.job_id', string="Job Position",store=True)
    department_id = fields.Many2one(related='employee_id.department_id', string="Department",store=True)
    reviewer_plan = fields.Selection(string="Reviewer Plan", selection=[("week", "Weekly"), ("month", "Monthly"), ("year", "Yearly")], default='week')
    kra_template_id = fields.Many2one('kra.template', string="KRA Template", readonly=False)
    state = fields.Selection([('draft', 'Draft'),('submit','Waiting For Approval'), ('waiting', 'Waiting For HR Approval'),
                             ('done', 'Approved')], string='Status',
                             required=True, readonly=True, copy=False, default='draft',track_visibility='onchange')
    self_review = fields.Boolean(string="Self Review" ,readonly=False, default=True)
    double_validation = fields.Boolean(string="Double Validation" ,readonly=False, default=True)
    internal_msg = fields.Boolean(string="Internal Message" ,readonly=False, default=True)
    que_rating_ids = fields.One2many('kra.question.rating', 'rating_id', string="Questions Rating")
    current_user_id = fields.Boolean(compute="change_reviewer_id")
    hr_manager_user_ids = fields.Many2many('res.users', compute="hr_manager_user_ids_get")
    total_score_percent = fields.Float(string="Total Score Percent",compute="get_total_score_count",store=True)
    total_score_percent_point = fields.Float(string="Total Score Percent Point",compute="get_total_score_count",store=True)
    validate_date = fields.Date('Validate Date', readonly=False)
    total_qsn = fields.Float(string="Total Number of Question",compute="get_total_qsn",store=True)
    full_mark = fields.Float(string="Full Mark",compute="get_full_mark",store=True)
   
    @api.depends('que_rating_ids')
    def get_total_qsn(self):

        for rec in self:
            total_line_count = 0
            total_score_count = 0
            # total_score_count = len(rec.que_rating_ids.ids)
            # _logger.info("========ssss==============%s",total_score_count)
            for line in rec.que_rating_ids:
                
                if not (line.display_type == 'line_section' or line.display_type == 'line_note'):
                    total_line_count += 1
                    # per_line_score_total = (int(line.self_rating) + int(line.manager_rating) + int(line.hr_rating))
                    # total_score_count += per_line_score_total  
                    _logger.info('==========================%s',total_line_count)
                    # total_score_count += line.manager_rating
                    # total_score_count += line.hr_rating
                    # _logger.info("=============================%s",total_score_count)
            rec.total_qsn = total_line_count
    
            _logger.info('=======================sdf=================%s',rec.total_qsn)
    
    
    
    @api.depends('que_rating_ids')
    def get_full_mark(self):

        for rec in self:
            total_line_count = 0
            total_score_count = 0
            # total_score_count = len(rec.que_rating_ids.ids)
            # _logger.info("========ssss==============%s",total_score_count)
            for line in rec.que_rating_ids:
                
                if not (line.display_type == 'line_section' or line.display_type == 'line_note'):
                    total_line_count += 1
                    # per_line_score_total = (int(line.self_rating) + int(line.manager_rating) + int(line.hr_rating))
                    # total_score_count += per_line_score_total  
                    _logger.info('==========================%s',total_line_count)
                    # total_score_count += line.manager_rating
                    # total_score_count += line.hr_rating
                    # _logger.info("=============================%s",total_score_count)
            rec.full_mark = total_line_count * 5 * 3
    
            _logger.info('=======================sdf=================%s',rec.full_mark)
    
    
    
    @api.depends('que_rating_ids')
    def get_total_score_count(self):

        for rec in self:
            total_line_count = 0
            total_score_count = 0
            # total_score_count = len(rec.que_rating_ids.ids)
            # _logger.info("========ssss==============%s",total_score_count)
            for line in rec.que_rating_ids:
                
                if not (line.display_type == 'line_section' or line.display_type == 'line_note'):
                    total_line_count += 1
                    per_line_score_total = (int(line.self_rating) + int(line.manager_rating) + int(line.hr_rating))
                    total_score_count += per_line_score_total  
                    _logger.info('==========================%s',total_score_count)
                    # total_score_count += line.manager_rating
                    # total_score_count += line.hr_rating
                    # _logger.info("=============================%s",total_score_count)
            rec.total_score_percent = (total_score_count/(total_line_count * 3 * 5)) * 100
            if rec.total_score_percent >= 1 and rec.total_score_percent <=20:
                rec.total_score_percent_point = 1
            elif rec.total_score_percent >= 21 and rec.total_score_percent <=40:
                rec.total_score_percent_point = 2
            elif rec.total_score_percent >= 41 and rec.total_score_percent <=60:
                rec.total_score_percent_point = 3
            elif rec.total_score_percent >= 61 and rec.total_score_percent <=80:
                rec.total_score_percent_point = 4 
            elif rec.total_score_percent >= 81 and  rec.total_score_percent <=100:
                    rec.total_score_percent_point = 5
            
            _logger.info('=======================sdf=================%s',rec.total_score_percent)



    # department_manager_user_ids = fields.Many2many('res.users', compute="department_manager_user_ids_get")

    managr_id = fields.Many2one('res.users',compute="department_manager_user_ids_get",store=True)

    # managers_emp_ids = fields.Many2many(default='get_managers_employee_list')

    
    # def get_managers_employee_list(self):
    #     mngr_lst = []
    #     empObj = self.env['hr.employee'].search([('parent_id','=',self.env.user.id)])

    #     for dec in empObj:
    #         mngr_lst.append(dec.id)
    #     self.write({
    #                         'managers_emp_ids' : [(6,0,mngr_lst)]
    #                     })
            
    # @api.onchange('employee_id')
    # def auto_fill_position_and_department(self):
    #     for each in self:
    #         each.job_id = each.employee_id.job_id.id
    #         each.parent_id = each.employee_id.parent_id.id
    def hr_manager_user_ids_get(self):
        self.ensure_one()
        for each in self:
            hr_group_ids = self.env.ref('performance_evaluation_rating.employee_head_department_manager').users.ids
            each.hr_manager_user_ids = [(6, 0, hr_group_ids)]
    @api.depends('employee_id')
    def department_manager_user_ids_get(self):
        for rec in self:
            if rec.employee_id:
                if rec.employee_id.parent_id:
                    usr_obj = self.env['res.users'].search([('id','=',rec.employee_id.parent_id.user_id.id)],limit=1)
                    if usr_obj:
                        rec.managr_id = usr_obj.id

    
    def change_reviewer_id(self):
        self.ensure_one()
        for each in self:
            if each.reviewer_id.user_id.id == self._uid:
                each.current_user_id = True
            elif self.env.user.has_group('performance_evaluation_rating.employee_head_department_manager'):
                each.current_user_id = True
            else:
                each.current_user_id = False

    def unlink(self):
        for rec in self:
            if rec.state != 'draft':
                raise Warning(_('You cannot delete record in this %s' % (rec.state)))
        return super(kraevaluation, self).unlink()
                
    @api.model
    def check_user_group(self, group_name):
        has_group = self.env.user.has_group(group_name)
        return has_group

    @api.onchange('department_id', 'job_id')
    def _onchange_job_id(self):
        if self.department_id and self.job_id:
            template_id = self.env['kra.template'].search([('job_id', '=', self.job_id.id), ('department_id', '=', self.department_id.id),('is_active', '=', True)])
            if template_id:
                self.kra_template_id = template_id
            else:
                raise Warning(_('Please configure KRA tempalte for "%s" job position of "%s" department' % (self.job_id.name, self.department_id.name)))

    @api.model
    def default_get(self, fields_list):
        res = super(kraevaluation, self).default_get(fields_list)
        template_id = self.env['kra.template'].search([])
        if not template_id:
            raise Warning(_('Please configure KRA template'))
        return res

    @api.onchange('kra_template_id')
    def _change_kra_template(self):
        lst = []
        template_id = self.env['kra.template'].search(
            [('job_id', '=', self.job_id.id), ('department_id', '=', self.department_id.id), ('is_active', '=', True)])
        self.kra_template_id = template_id.id
        for item in self.que_rating_ids:
            self.que_rating_ids = [(2, item.id, _)]
        i = []    
        for item in self.kra_template_id.questions_ids:
            i.append((0, 0, {'name': item.name,
                             'description': item.description,
                             'display_type': item.display_type
                     }))
        self.que_rating_ids = i

    def action_first_approval(self):
        self.state = 'waiting'

    def action_second_approval(self):
        self.state = 'done'
        self.validate_date = datetime.now()

    def action_submit_manager(self):
        self.state = 'submit'

    def action_kra_eval_cancel(self):
        self.state = 'draft'

    @api.model
    def find_evaluation(self,rec_id):
        return [self.browse(rec_id[0]).read(), self.browse(rec_id[0]).reviewer_id.user_id.id]


class kraquestionsrating(models.Model):
    _name = 'kra.question.rating'
    _description = 'KRA Questions Rating'

    rating_id = fields.Many2one('kra.evaluation', string="Questions Rating")
    name = fields.Char(string="Questions")
    self_rating = fields.Selection([('0','0'),('1','1'),('2','2'),('3','3'),('4','4'),('5','5')], default='0', string="Self Rating")
    self_comment = fields.Char(string="Self Comment")
    manager_rating = fields.Selection([('0','0'),('1','1'),('2','2'),('3','3'),('4','4'),('5','5')], default='0', string="Manager Rating")
    manager_comment = fields.Char(string="Manager Comment")
    hr_rating = fields.Selection([('0','0'),('1','1'),('2','2'),('3','3'),('4','4'),('5','5')], default='0', string="Management Rating")
    hr_comment = fields.Char(string="Management Comment")
    description = fields.Char(string="Description")
    display_type = fields.Selection([
        ('line_section', 'Section'),
        ('line_note', 'Note'),
    ], default=False, help="Technical field for UX purpose.")
    sequence = fields.Integer()










