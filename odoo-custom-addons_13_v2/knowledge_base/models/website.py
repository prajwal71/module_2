#coding: utf-8

import logging

from odoo import _, api, fields, models
from odoo.tools.safe_eval import safe_eval
from odoo.tools.translate import html_translate

_logger = logging.getLogger(__name__)


class website(models.Model):
    """
    Overwrite to keep configuration for particular website
    """
    _inherit = "website"

    @api.model
    def _default_knowledge_base_website_portal(self):
        ICPSudo = self.env['ir.config_parameter'].sudo()
        return safe_eval(ICPSudo.get_param('knowledge_base_website_portal', default='False'))

    @api.model
    def _default_knowledge_base_website_public(self):
        ICPSudo = self.env['ir.config_parameter'].sudo()
        return safe_eval(ICPSudo.get_param('knowledge_base_website_public', default='False'))

    @api.model
    def _default_knowledge_base_portal_print(self):
        ICPSudo = self.env['ir.config_parameter'].sudo()
        return safe_eval(ICPSudo.get_param('knowledge_base_portal_print', default='False'))

    @api.model
    def _default_knowledge_base_portal_likes(self):
        ICPSudo = self.env['ir.config_parameter'].sudo()
        return safe_eval(ICPSudo.get_param('knowledge_base_portal_likes', default='False'))

    @api.model
    def _default_knowledge_base_portal_social_share(self):
        ICPSudo = self.env['ir.config_parameter'].sudo()
        return safe_eval(ICPSudo.get_param('knowledge_base_portal_social_share', default='False'))

    @api.model
    def _default_knowledge_base_portal_tooltip(self):
        ICPSudo = self.env['ir.config_parameter'].sudo()
        return safe_eval(ICPSudo.get_param('knowledge_base_portal_tooltip', default='False'))

    # @api.model
    # def _default_knowledge_base_portal_filters_ids(self):
    #     ICPSudo = self.env['ir.config_parameter'].sudo()
    #     custom_filters = safe_eval(ICPSudo.get_param('knowledge_base_portal_filters_ids', default='[]'))
    #     custom_filters_ids = self.env["ir.filters"].sudo().search([("id", "in", custom_filters)])    
    #     return custom_filters_ids    

    @api.model
    def _default_knowledge_base_custom_search_ids(self):
        ICPSudo = self.env['ir.config_parameter'].sudo()
        custom_searchs = safe_eval(ICPSudo.get_param('knowledge_base_custom_search_ids', default='[]'))
        custom_search_ids = self.env["ir.filters"].sudo().search([("id", "in", custom_searchs)]) 
        return custom_search_ids

    def _inverse_knowledge_base_website_portal(self):
        """
        Inverse method for knowledge_base_website_portal
        """
        for record in self:
            if not record.knowledge_base_website_portal:
                record.knowledge_base_website_public = False
                record.knowledge_base_portal_print = False
                #record.knowledge_base_portal_filters_ids = False
                record.knowledge_base_custom_search_ids = False
                record.knowledge_base_portal_likes = False
                record.knowledge_base_portal_social_share = False

    knowledge_base_website_portal = fields.Boolean(
        string="Portal Knowledge Base",
        default=_default_knowledge_base_website_portal,
        inverse=_inverse_knowledge_base_website_portal,
    )
    knowledge_base_website_public = fields.Boolean(
        string="Public Knowledge Base",
        default=_default_knowledge_base_website_public,
    )
    knowledge_base_portal_print = fields.Boolean(
        string="Print in Portal",
        default=_default_knowledge_base_portal_print,
    )
    knowledge_base_portal_likes = fields.Boolean(
        string="Portal Likes",
        default=_default_knowledge_base_portal_likes,
    )
    knowledge_base_portal_social_share = fields.Boolean(
        string="Social Sharing",
        default=_default_knowledge_base_portal_social_share,
    )
    knowledge_base_portal_tooltip = fields.Boolean(
        string="Sections and Tags Tooltips",
        default=_default_knowledge_base_portal_tooltip,
    )
    ## knowledge_base_portal_filters_ids = fields.Many2many(
    #     "ir.filters",
    #     "ir_filters_know_website_rel_table",
    #     "ir_filters_id",
    #     "res_config_setting_id",
    #     string="Custom Portal Filters",
    #     default=_default_knowledge_base_portal_filters_ids,
    # )
    knowledge_base_custom_search_ids = fields.Many2many(
        "knowledge.base.custom.search",
        "knowledge_base_custom_know_website_rel_table",
        "knowledge_base_custom_search_id",
        "res_config_settings_id",
        string="Custom Portal Search",
        default=_default_knowledge_base_custom_search_ids,
    )
    left_navigation_hints_header = fields.Char(
        string="Left Navigation Hints Header",
        translate=True,
        default="About Knowledge Base",
    )
    left_navigation_hints = fields.Html(
        string="Left Navigation Hints",
        translate=html_translate,
        sanitize_attributes=False,
    )
    center_knowledge_base_introduction = fields.Html(
        string="Knowledge Base Introduction",
        translate=html_translate,
        sanitize_attributes=False,
    )
    pager_knowledge_base = fields.Integer(
        string="Articles Per Page",
        default=10,
    )
    _sql_constraints = [
        ('pager_knowledge_base_value_check', 'check (pager_knowledge_base>0)', _('Articles number should be positive!')),
    ]

    @api.model
    def create(self, values):
        """
        Overwrite to manage KnowledgeBase menu
        """
        if values.get("knowledge_base_website_public") is not None:
            for record in self:
                record._generate_knowledge_base_menu(values.get("knowledge_base_website_public"), False)            
        return super(website, self).create(values)        

    def write(self, values):
        """
        Re-write to change menus
        """
        if values.get("knowledge_base_website_public") is not None:
            for record in self:
                record._generate_knowledge_base_menu(
                    values.get("knowledge_base_website_public"),
                    record.knowledge_base_website_public,
                )
        return super(website, self).write(values)

    def _generate_knowledge_base_menu(self, shouldbemenu, previouslyexist):
        """
        The method to add Knowledge Base menu or ublink it

        Args:
         * shouldbemenu - bool - whether the menu should present
         * previouslyexist - whether the menu should have already exist (not to recover manually removed menu)
        
        Extra info:
         * Expected singleton
        """
        self.ensure_one()
        exist_ids = self.env["website.menu"].search([
            ("url", "=", "/knowledgebase"), 
            ('website_id', 'in', [False, self.id]),
        ])
        if shouldbemenu:
            if not previouslyexist and not exist_ids:
                try:
                    values = {
                        "name": _("Knowledge Base"),
                        "url": "/knowledgebase",
                        "parent_id": self.menu_id.id,
                        "website_id": self.id,
                        "sequence": 60,
                    }
                    new_menu_id = self.env["website.menu"].create(values)
                except Exception as e:
                    _logger.warning(e)            
        elif exist_ids:
            exist_ids.unlink()
