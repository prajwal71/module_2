# -*- coding: utf-8 -*-

from odoo import fields, models


class res_config_settings(models.TransientModel):
    """
    Overwrite to add website-specific settings
    """
    _inherit = "res.config.settings"

    def _default_knowledge_base_website_id(self):
        """
        Default method for knowledge_base_website_id
        """
        return self.env['website'].search([('company_id', '=', self.env.user.company_id.id)], limit=1)

    knowledge_base_website_id = fields.Many2one(
        "website",
        string='Knowledge Base Website',
        default=_default_knowledge_base_website_id, 
        ondelete='cascade',
    )
    knowledge_base_website_portal = fields.Boolean(
        related="knowledge_base_website_id.knowledge_base_website_portal",
        readonly=False,
    )
    knowledge_base_website_public = fields.Boolean(
        related="knowledge_base_website_id.knowledge_base_website_public",
        readonly=False,
    )
    knowledge_base_portal_print = fields.Boolean(
        related="knowledge_base_website_id.knowledge_base_portal_print",
        readonly=False,
    )
    knowledge_base_portal_likes = fields.Boolean(
        related="knowledge_base_website_id.knowledge_base_portal_likes",
        readonly=False,
    )
    knowledge_base_portal_social_share = fields.Boolean(
        related="knowledge_base_website_id.knowledge_base_portal_social_share",
        readonly=False,
    )
    #knowledge_base_portal_tooltip = fields.Boolean(
     #   related="knowledge_base_website_id.knowledge_base_portal_tooltip",
      #  readonly=False,
    #)
#    knowledge_base_portal_filters_ids = fields.Many2many(
 #       related="knowledge_base_website_id.knowledge_base_portal_filters_ids",
  #      readonly=False,
   # )
    knowledge_base_custom_search_ids = fields.Many2many(
        related="knowledge_base_website_id.knowledge_base_custom_search_ids",
        readonly=False,
    )
    pager_knowledge_base = fields.Integer(
        related="knowledge_base_website_id.pager_knowledge_base",
        readonly=False,
    )

    def set_values(self):
        super(res_config_settings, self).set_values()
