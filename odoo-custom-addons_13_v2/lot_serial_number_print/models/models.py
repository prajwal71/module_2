# -*- coding: utf-8 -*-

# from odoo import models, fields, api


# class lot_serial_number_print(models.Model):
#     _name = 'lot_serial_number_print.lot_serial_number_print'
#     _description = 'lot_serial_number_print.lot_serial_number_print'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100

from odoo import models,fields,api

class PrintCustom(models.Model):
    _inherit='stock.production.lot'
    print_number=fields.Integer(string="Number of Prints:")