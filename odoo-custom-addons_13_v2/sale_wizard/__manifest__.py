{
    'name': "sale wizard",
    'author': 'Nexus',
    'category': 'wizard',
    'summary': "Module for server action ",
    'sequence':'10',
    'website': 'http://www.prajwal.com',
    'license': 'AGPL-3',
    'description': """ """,
    'version': '13.0.1.0',
    'depends': ['base','mail','sale'],
    'data': [
        #  'security/ir.model.access.csv',
        # 'views/student_views.xml',
        #  'views/teacher_view.xml',
        #  'data/school_sequence.xml',
         'wizard/sale_person.xml',
        #  'views/extrend_sale.xml'
        #  'reports/report_view.xml',
        #  'reports/student_identity_card.xml',
        #  'reports/student_identity_card2.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}


