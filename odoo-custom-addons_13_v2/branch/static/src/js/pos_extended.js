
odoo.define('branch.pos_extended', function (require) {
    "use strict";

    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');
    var core = require('web.core');
    var gui = require('point_of_sale.gui');
    var popups = require('point_of_sale.popups');
    

    var QWeb = core.qweb;
	var _t = core._t;

    var _super_posmodel = models.PosModel.prototype;
    models.PosModel = models.PosModel.extend({
        initialize: function (session, attributes) {
            var session_model = _.find(this.models, function(model){ return model.model === 'pos.session'; });
            session_model.fields.push('branch_id');
            return _super_posmodel.initialize.call(this, session, attributes);
        },

    });


	screens.PaymentScreenWidget.include({
		// Include auto_check_invoice boolean condition in watch_order_changes method
		watch_order_changes: function() {
			var self = this;
			var order = this.pos.get_order();
			if(this.pos.config.auto_check_invoice && this.pos.config.module_account) // Condition True/False
			{
				var pos_order=this.pos.get_order();
				pos_order.set_to_invoice(true);
				this.$('.js_invoice').addClass('highlight');
			}
			
			if (!order) {
				return;
			}
			if(this.old_order){
				this.old_order.stop_electronic_payment();
				this.old_order.unbind(null, null, this);
				this.old_order.paymentlines.unbind(null, null, this);
			}
			order.bind('all',function(){
				self.order_changes();
			});
			order.paymentlines.bind('all', self.order_changes.bind(self));
			this.old_order = order;
		}
	
	});
});
