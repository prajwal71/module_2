# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class ProductTemplateIn(models.Model):
    _inherit = 'product.template'

 

    branch_ids = fields.Many2many('res.branch', 'res_branch_product_id', 'branch_id', 'product_id')
