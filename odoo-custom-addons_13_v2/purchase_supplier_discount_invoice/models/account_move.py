# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
import odoo.addons.decimal_precision as dp
from odoo.tools import float_is_zero
import logging
_logger = logging.getLogger(__name__)

class account_move(models.Model):
    _inherit = 'account.move'


    def calc_discount(self):
        for calc in self:
            calc._calculate_discount()

    @api.depends('discount_amount','discount_method')
    def _calculate_discount(self):
        res = discount = 0.0

        for self_obj in self:
            _logger.info("======%s=====testing=========",self_obj.amount_tax)
            if self_obj.discount_method == 'fix':
                res = self_obj.discount_amount
               
                # untax_amt= self_obj.amount_untaxed
                # if untax_amt > self_obj.discount_amt and ch:
                #     tax = (untax_amt - res)*0.13
                #     self_obj.update({
                #         'amount_tax' : tax})
                    # _logger.info("======%s=====TAX=========",self_obj.amount_tax)
            elif self_obj.discount_method == 'per':
                res = self_obj.amount_untaxed * (self_obj.discount_amount/ 100)
                # untax_amt= self_obj.amount_untaxed
                # if untax_amt > self_obj.discount_amt and ch:
                #     tax = (untax_amt - res)*0.13
                #     self_obj.update({
                #         'amount_tax' : tax})
            else:
                res = discount
            return res

    @api.depends(
        'line_ids.matched_debit_ids.debit_move_id.move_id.line_ids.amount_residual',
        'line_ids.matched_debit_ids.debit_move_id.move_id.line_ids.amount_residual_currency',
        'line_ids.matched_credit_ids.credit_move_id.move_id.line_ids.amount_residual',
        'line_ids.matched_credit_ids.credit_move_id.move_id.line_ids.amount_residual_currency',
        'line_ids.debit',
        'line_ids.credit',
        'line_ids.currency_id',
        'line_ids.amount_currency',
        'line_ids.amount_residual',
        'line_ids.amount_residual_currency',
        # 'line_ids.payment_id.state',
        'line_ids.full_reconcile_id')
    def _compute_amount(self):
        for move in self:

            if move.invoice_payment_state == 'invoicing_legacy':
                # invoicing_legacy state is set via SQL when setting setting field
                # invoicing_switch_threshold (defined in account_accountant).
                # The only way of going out of this state is through this setting,
                # so we don't recompute it here.
                move.invoice_payment_state = move.invoice_payment_state
                continue

            total_untaxed = 0.0
            total_untaxed_currency = 0.0
            total_tax = 0.0
            total_tax_currency = 0.0
            total_to_pay = 0.0
            total_residual = 0.0
            total_residual_currency = 0.0
            total = 0.0
            total_currency = 0.0
            currencies = set()
            check = False

            for line in move.line_ids:
                if line.tax_ids:
                    check = True
                else:
                    check = False
                if line.currency_id:
                    currencies.add(line.currency_id)

                if move.is_invoice(include_receipts=True):
                    # === Invoices ===

                    if not line.exclude_from_invoice_tab:
                        # Untaxed amount.
                        total_untaxed += line.balance
                        total_untaxed_currency += line.amount_currency
                        total += line.balance
                        total_currency += line.amount_currency
                    elif line.tax_line_id:
                        # Tax amount.
                        total_tax += line.balance
                        # _logger.info("====================%s==",line.res)
                        total_tax_currency += line.amount_currency
                        total += line.balance
                        total_currency += line.amount_currency
                    elif line.account_id.user_type_id.type in ('receivable', 'payable'):
                        # Residual amount.
                        total_to_pay += line.balance
                        total_residual += line.amount_residual
                        total_residual_currency += line.amount_residual_currency
                else:
                    # === Miscellaneous journal entry ===
                    if line.debit:
                        total += line.balance
                        total_currency += line.amount_currency

            if move.type == 'entry' or move.is_outbound():
                sign = 1
            else:
                sign = -1

            move.amount_untaxed = sign * (total_untaxed_currency if len(currencies) == 1 else total_untaxed)
            
            res = self._calculate_discount()
            _logger.info("======%s=====discount=========",res)
            move.discount_amt = res
            # move.amount_tax = 637
            if check and res>1:
                move.amount_tax = sign * (((total_untaxed_currency-res)*0.13) if len(currencies) == 1 else ((total_untaxed-res)*0.13))  
            else :
                move.amount_tax = sign * (total_tax_currency if len(currencies) == 1 else total_tax)
                      
            _logger.info("======%s=====move_tax=========",move.amount_tax)
            # _logger.info("============%s====amount_by_group=====",move.amount_by_group)           
            if check and res>1:
                move.amount_total = move.amount_untaxed + move.amount_tax  - res
            else:
                move.amount_total = sign * (total_currency if len(currencies) == 1 else total) - res
            
            # move.amount_total = sign * ((total_untaxed_currency + ((total_untaxed_currency-res)*0.13) - res) if len(currencies) == 1 else (total_untaxed -res + ((total_untaxed-res)*0.13) )) - res
            move.amount_residual = -sign * (total_residual_currency if len(currencies) == 1 else total_residual)
            move.amount_untaxed_signed = -total_untaxed
            move.amount_tax_signed = -total_tax
            move.amount_total_signed = -total - res
            move.amount_residual_signed = total_residual - res
            currency = len(currencies) == 1 and currencies.pop() or move.company_id.currency_id
            is_paid = currency and currency.is_zero(move.amount_residual) or not move.amount_residual

            currency = len(currencies) == 1 and currencies.pop() or move.company_id.currency_id

            # Compute 'payment_state'.
            new_pmt_state = 'not_paid' if move.type != 'entry' else False

            if move.is_invoice(include_receipts=True) and move.state == 'posted':

                if currency.is_zero(move.amount_residual):
                    if all(payment.is_matched for payment in move._get_reconciled_payments()):
                        new_pmt_state = 'paid'
                    else:
                        new_pmt_state = move._get_invoice_in_payment_state()
                elif currency.compare_amounts(total_to_pay, total_residual) != 0:
                    new_pmt_state = 'partial'

            if new_pmt_state == 'paid' and move.type in ('in_invoice', 'out_invoice', 'entry'):
                reverse_type = move.type == 'in_invoice' and 'in_refund' or move.type == 'out_invoice' and 'out_refund' or 'entry'
                reverse_moves = self.env['account.move'].search([('reversed_entry_id', '=', move.id), ('state', '=', 'posted'), ('type', '=', reverse_type)])

                # We only set 'reversed' state in cas of 1 to 1 full reconciliation with a reverse entry; otherwise, we use the regular 'paid' state
                reverse_moves_full_recs = reverse_moves.mapped('line_ids.full_reconcile_id')
                if reverse_moves_full_recs.mapped('reconciled_line_ids.move_id').filtered(lambda x: x not in (reverse_moves + reverse_moves_full_recs.mapped('exchange_move_id'))) == move:
                    new_pmt_state = 'reversed'

            move.invoice_payment_state = new_pmt_state
            #Calculate Discount
            
    discount_method = fields.Selection([('fix', 'Fixed'), ('per', 'Percentage')],'Discount Method')
    discount_amount = fields.Float('Discount Amount')

    discount_amt = fields.Float(string='- Discount',  store=True, readonly=True, tracking=True, compute='_compute_amount')

    amount_untaxed = fields.Monetary(string='Untaxed Amount', store=True, readonly=True, tracking=True,
        compute='_compute_amount')

    amount_tax = fields.Monetary(string='Tax', store=True, readonly=True,
        compute='_compute_amount')

    amount_total = fields.Monetary(string='Total', store=True, readonly=True,
        compute='_compute_amount',
        inverse='_inverse_amount_total')
    # test1 = fields.Boolean() 
    @api.onchange('invoice_line_ids','discount_method','discount_amount','discount_amt')
    def _onchange_invoice_lines(self):
        current_invoice_lines = self.line_ids.filtered(lambda line: not line.exclude_from_invoice_tab)
        others_lines = self.line_ids - current_invoice_lines
        discount_lines = self.env['account.move.line']
        count= 0
        for record in self:
            for line in record.invoice_line_ids:
                if line.product_id:
                    account = line.account_id.id
                   
            for line in record.line_ids:
                if line.name == "Discount":
                    count+=1
                else:
                    pass 

            for line in record.line_ids:
                if count == 1:
                    if line.name == "Discount":
                        record._compute_amount()
                        line.with_context(check_move_validity=False).write({'credit':record.discount_amt})   
                else:   
                    if record.discount_amt >0:
                        discount_vals = {
                                'price_unit' : -record.discount_amt,
                                'account_id': account,
                                'quantity': 1,
                                'name': "Discount",
                                'exclude_from_invoice_tab': True,
                                }
                        discount_lines = record.invoice_line_ids.with_context(check_move_validity=False).new(discount_vals)
                                           
            if others_lines and current_invoice_lines - record.invoice_line_ids:
                others_lines[0].recompute_tax_line = True
            record.line_ids = others_lines + record.invoice_line_ids + discount_lines
            record._onchange_recompute_dynamic_lines()

    @api.model
    def create(self, vals_list):
        count=0
        for val in vals_list:     
            if 'flag' in val:
                val.pop('flag')
                res = super(account_move,self).create(vals_list)
                
            else:
                res = super(account_move,self).create(vals_list)
                if vals_list.get('invoice_origin'):
                    purchase = self.env['purchase.order'].search([('name', '=', vals_list["invoice_origin"])],limit=1)
                    res.write({'discount_method':purchase.discount_method,'discount_amount':purchase.discount_amount})
                                 
                if res.discount_method and res.discount_amount:
                    if res.state in 'draft':    
                        for line in res.invoice_line_ids:
                            if line.product_id:
                                account = line.account_id.id
                        for record in res.line_ids:          
                            if record.name == 'Discount':
                                count+=1

                        discount_vals = {
                        'account_id': account, 
                        'quantity': 1,
                        'price_unit': -res.discount_amt,
                        'name': "Discount", 
                        'exclude_from_invoice_tab': True,
                        }
                        if count == 1:
                            pass
                        else:          
                            res.with_context(check_move_validity=False).write({
                                'invoice_line_ids' : [(0,0,discount_vals)]
                                })                                  
            return res 

    def write(self, vals_list):
        count=0
        for move in self:
            res = super(account_move,self).write(vals_list)
            if vals_list.get('invoice_origin'):
                purchase = self.env['purchase.order'].search([('name', '=', vals_list["invoice_origin"])],limit=1)
                move.write({'discount_method':purchase.discount_method,'discount_amount':purchase.discount_amount})
                                 
            if move.discount_method and move.discount_amount:
                if move.state in 'draft':    
                    for line in move.invoice_line_ids:
                        if line.product_id:
                            account = line.account_id.id
                    for record in move.line_ids:          
                        if record.name == 'Discount':
                            count+=1
                    discount_vals = {
                    'account_id': account, 
                    'quantity': 1,
                    'price_unit': -move.discount_amt,
                    'name': "Discount", 
                    'exclude_from_invoice_tab': True,
                    }
                    if count == 1:
                        pass
                    else:          
                        self.with_context(check_move_validity=False).write({
                            'invoice_line_ids' : [(0,0,discount_vals)]
                            })       
            return res                           

    
class account_payment(models.Model):
    _inherit = "account.payment"

    def _prepare_payment_moves(self): 

        res = super(account_payment,self)._prepare_payment_moves()
        
        if self._context.get('default_type') == 'in_invoice' :
            for rec in res:
                rec.update({'flag':True})        
        return res


