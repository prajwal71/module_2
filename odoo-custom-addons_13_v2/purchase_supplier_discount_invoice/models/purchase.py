# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
import odoo.addons.decimal_precision as dp
import logging
_logger = logging.getLogger(__name__)
class purchase_order(models.Model):
    _inherit = 'purchase.order'
       
    @api.depends('discount_amount','discount_method','order_line.taxes_id')
    def _calculate_discount(self):
        # a = self._origin.order_line.taxes_id
        
        
        # for order in self:
            
        #     for line in order.order_line:
        #         # line._compute_amount()
        #         tax_id = line.taxes_id
        #         # amount_tax += line.price_tax
        #         _logger.info("==================tax=====%s",tax_id)
        #         if tax_id:
        #             ch = True
                    
        #         else :
        #             ch = False
        
        
        for self_obj in self:
            if self_obj.discount_method == 'fix':
                discount = self_obj.discount_amount
                self_obj.update({
                    'discount_amt' : discount})
              
                # _logger.info("==================tax=====%s",self_obj.taxes_id)
                
                for order in self:
                
                    for line in order.order_line:
                        # line._compute_amount()
                        tax_id = line.taxes_id
                        # amount_tax += line.price_tax
                        _logger.info("==================tax=====%s",tax_id)
                        if tax_id:
                            ch = True
                            
                        else :
                            ch = False
                        
                        _logger.info("==================CHECK=====%s",ch)
                        untax_amt= self_obj.amount_untaxed
                        if untax_amt > self_obj.discount_amt and ch:
                            tax = (untax_amt - discount)*0.13
                            self_obj.update({
                                'amount_tax' : tax})
                            
                        
            elif self_obj.discount_method == 'per':
                discount = self_obj.amount_untaxed * ((self_obj.discount_amount or 0.0) / 100.0)
                self_obj.update({
                    'discount_amt' : discount})
                
                
                for order in self:
                
                    for line in order.order_line:
                        # line._compute_amount()
                        tax_id = line.taxes_id
                        # amount_tax += line.price_tax
                        _logger.info("==================tax=====%s",tax_id)
                        if tax_id:
                            ch = True
                            
                        else :
                            ch = False
                        
                        _logger.info("==================CHECK=====%s",ch)
                        untax_amt= self_obj.amount_untaxed
                        if untax_amt > self_obj.discount_amt and ch:
                            tax = (untax_amt - discount)*0.13
                            self_obj.update({
                                'amount_tax' : tax})
                            
                
                
                
         
            else:
                discount = 0
                self_obj.update({
                    'discount_amt' : discount})
            self_obj.update({'amount_total': self_obj.amount_untaxed + self_obj.amount_tax - self_obj.discount_amt})

    discount_method = fields.Selection(
            [('fix', 'Fixed'), ('per', 'Percentage')], 'Discount Method')
    discount_amount = fields.Float('Discount Amount')
    discount_amt = fields.Monetary(compute='_calculate_discount',store=True,string='- Discount',digits='Account',readonly=True)
    # test = fields.Boolean()   
class purchase_order_line(models.Model):
    _inherit = 'purchase.order.line'

    discount_method = fields.Selection([('fix', 'Fixed'), ('per', 'Percentage')], 'Discount Method')
    # discounted_amount=  fields.Monetary(string="Discounted Amount")       
    @api.depends('product_qty', 'price_unit', 'taxes_id')
    def _compute_amount(self):
        for line in self:
            taxes = line.taxes_id.compute_all(line.price_unit, line.order_id.currency_id, line.product_qty, product=line.product_id, partner=line.order_id.partner_id)
            
            line.update({
                'price_tax': taxes['total_included'] - taxes['total_excluded'],
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })
            
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:s
