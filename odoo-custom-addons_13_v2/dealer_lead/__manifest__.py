# -*- coding: utf-8 -*-
{
    'name': "dealer_lead",

    'summary': """
        crm form in website""",

    'description': """
        crm form in website
    """,

    'author': "Nexus gurus",
    'website': "http://www.nexusgurus.com",
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['crm','website','website_crm'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
