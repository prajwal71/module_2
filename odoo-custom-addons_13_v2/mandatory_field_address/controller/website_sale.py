# ©  2008-2021 Deltatech
#              Dorin Hongu <dhongu(@)gmail(.)com
# See README.rst file on addons root folder for license details

from odoo.http import request
from odoo import fields, http, SUPERUSER_ID, tools, _
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.exceptions import AccessError, MissingError, ValidationError
from werkzeug.exceptions import Forbidden, NotFound
import logging
_logger = logging.getLogger(__name__)

class WebsiteSaleCity(WebsiteSale):

    # pentru tarile in care nu avem nomencator de localitati nu se poate face acest camp obligatori

    # def _get_mandatory_billing_fields(self):
    #     res = super(WebsiteSaleCity, self)._get_mandatory_billing_fields()
    #     res.remove('street')
    #     res.remove('city')
    #     res.remove('zip')
    #     return res
    # #
    # def _get_mandatory_shipping_fields(self):
    #     res = super(WebsiteSaleCity, self)._get_mandatory_shipping_fields()
    #     res.remove('street')
    #     res.remove('city')
    #     res.remove('zip')
    #     return res


    def _get_mandatory_fields_billing(self, country_id=False):
        req = ["name", "email","street","phone"]
       
        return req

    def _get_mandatory_fields_shipping(self, country_id=False):
        req = ["name", "email","street","phone"]
       
        return req