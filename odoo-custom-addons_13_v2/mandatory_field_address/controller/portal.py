# ©  2008-2021 Deltatech
#              Dorin Hongu <dhongu(@)gmail(.)com
# See README.rst file on addons root folder for license details


from odoo import fields as odoo_fields, http, tools, _, SUPERUSER_ID
from odoo.http import request
from odoo.http import content_disposition, Controller, request, route
import logging
_logger = logging.getLogger(__name__)

from odoo.addons.portal.controllers.portal import CustomerPortal


class CustomerPortalCity(CustomerPortal):
    MANDATORY_BILLING_FIELDS = ["name", "email", "phone","street"]
    OPTIONAL_BILLING_FIELDS = ["zipcode","country_id","state_id","city","company_name","vat"]

    def _prepare_portal_layout_values(self):
        values = super(CustomerPortalCity, self)._prepare_portal_layout_values()
        # cities = request.env['res.city'].sudo().search([])
        # values['cities'] = cities
        
        return values
 

    