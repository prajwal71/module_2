# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Purchase order orgin in bell',
    'version' : '1.1',
    'summary': 'It tell the origin of vendor bill',
    'description': """
.
    """,
    'author': 'Nexus Incoporation',
    'depends' : ['account'],
    'data': [
        'views/view.xml'
        ],
    'demo': [
    ],
    'qweb': [
       ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
