# -*- coding: utf-8 -*-
# Part of Nexus Incorporation. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
import logging
_logger = logging.getLogger(__name__)



class AccountMoveExt(models.Model):
    _inherit = 'account.move'
    
    purchase_source_origin = fields.Char(string="purchase source")
    purchase_source_origin_set = fields.Boolean(string="Source of purchase",default=False)
    
   
    def find_source(self):

        for move in self:
            if move.reversed_entry_id:
                continue
            purchase = move.line_ids.mapped('purchase_line_id.order_id')
            if not purchase:
                continue
            refs = ["%s %s" % tuple(name_get) for name_get in purchase.name_get()]
            vals=""
            c=0
            for name_get in purchase.name_get():
                c = c +1
                a = tuple(name_get)
                b = a[1]
                if c==1:
                    vals = b + vals
                if c>1:
                    vals = vals + ',' +b
                _logger.info("=================%s=====================",b)
                
            
            move.purchase_source_origin = vals 
            move.purchase_source_origin_set = True
            
        
            # move.source = _("%s") % ','.join(refs)
            # refs = ["<a href=# data-oe-model=purchase.order data-oe-id=%s>%s</a>" % tuple(name_get) for name_get in purchase.name_get()]
            # message = _("This vendor bill has been created from: %s") % ','.join(refs)
            # move.source = message
      












