# -*- coding: utf-8 -*-

from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)




class EmployeeWworkManagement(models.Model):
    _name = 'employee.work'
    _description = 'Employee Work Plan'
    _inherit = ['mail.thread','mail.activity.mixin']
    
    name = fields.Many2one('hr.employee',string='Employee Name')
    department = fields.Many2one(related='name.department_id',string='Department')
    month = fields.Selection([('baishakh','Baishakh'),
                              ('jestha','Jestha'),
                              ('ashadh','Ashadh'),
                              ('shrawan','Shrawan'),
                              ('bhadra','Bhadra'),
                              ('ashwin',' Ashwin'),
                              ('kartik','Kartik'),
                              ('mangsir','Mangsir'),
                              ('poush','Poush'),
                              ('magh','Magh'),
                              ('falgun','Falgun'),
                              ('chaitra','Chaitra')],string='Plan Work MOnth')
    year=fields.Char(string="Plan Work Year",default='')
    
    image_1920 = fields.Binary(related='name.image_1920', store=True)
    image_1921 = fields.Binary()
    
    plan_work = fields.One2many('employee.work.plan','employee', string="Planeed Work")

    @api.model
    def create(self,vals):
        _logger.info("===================%s===========",vals)
        if vals.get('image_1920'):
            vals['image_1921'] = vals.get('image_1920')
            _logger.info("============================")
        result = super(EmployeeWworkManagement, self).create(vals)
        return result
        
class EmployeeWworkPlan(models.Model):
    _name = 'employee.work.plan'
    _rec_name = 'week'
    
    employee = fields.Many2one("employee.work",string="Employee" ,readonly=True)
    day = fields.Selection([('sun','Sunday'),
                              ('mon','Monday'),
                              ('tue','Tuesday'),
                              ('wed','Wednesday'),
                              ('thu','Thursday'),
                              ('fri','Friday'),
                              ('sat','Saturday')], string='CComplete Day')
    work = fields.Char(string="Planned Work")
    status=fields.Selection([('comp','complete'),
                             ('uncomp','Not Complete')],string="Status")
    reason = fields.Char(string="If not Why")
    week = fields.Char(string="Week Number")