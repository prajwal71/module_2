# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo.http import request
from odoo import http, _
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from dateutil.relativedelta import relativedelta
import base64
import logging
import json
_logger = logging.getLogger(__name__)
# import yaml
import werkzeug
class AuthProductCheck(http.Controller):

    @http.route(['/serial-no-check'], type='http', auth="public", website=True)
    def auth_product(self, page=1, **kwargs):
        # request.env.cr.execute( """select google_recaptcha_active,google_captcha_secret_key,google_captcha_client_key from website_support_settings""")
        # my_cust_data = request.env.cr.dictfetchall()
        # for data in my_cust_data:
        #     setting_google_recaptcha_active = data['google_recaptcha_active']
        #     setting_google_captcha_client_key = data['google_captcha_client_key']
        #     _logger.info('-----------------FYI: This is happening-------------')
        #     _logger.info(setting_google_recaptcha_active)
        #     _logger.info(setting_google_captcha_client_key)
        # return request.render("web_serial_check.auth_serial",{
        #         'setting_google_recaptcha_active': setting_google_recaptcha_active,
        #         'setting_google_captcha_client_key': setting_google_captcha_client_key})
        return request.render("web_serial_check.auth_serial")
    
    @http.route(['/serial-model-guide'], type='http', auth="public", website=True)
    def serial_guide(self, page=1, **kwargs):

        return request.render("web_serial_check.serial_model_guide")
    @http.route('/serial-model-guide/model-no/fetch', type='http', auth="public", website=True)
    def serial_model_fetch(self, **kwargs):

        values = {}
        for field_name, field_value in kwargs.items():
            values[field_name] = field_value

        models_nos = request.env['product.template'].sudo().search([('product_brand_id','=', int(values['brand_id']) )])

        #Only return a dropdown if this category has subcategories
        return_string = ""

        if models_nos:
            return_string += "<div class=\"form-group col-lg-3\">\n"
            return_string += "    <label class=\"control-label\" for=\"model_no_id\">Model No.</label>\n"
            # return_string += "    <div class=\"col-md-7 col-sm-8\">\n"

            return_string += "        <select class=\"form-control js-example-basic-single\" id=\"model_no_id\" name=\"model_no_id\">\n"
            for my_models in request.env['product.template'].sudo().search([('product_brand_id','=', int(values['brand_id']) )]):
                return_string += "            <option value=\"" + str(my_models.id) + "\">" + my_models.name + "</option>\n"

            return_string += "        </select>\n"
            # return_string += "    </div>\n"
            return_string += "</div>\n"

        return return_string
    @http.route(['/serial-no-check-authorize'], type='http', auth="public", website=True)
    def auth_product_done(self, page=1, **post):
        # values={}
        # request.env.cr.execute( """select google_recaptcha_active,google_captcha_secret_key,google_captcha_client_key from website_support_settings""")
        # my_cust_data = request.env.cr.dictfetchall()
        # for data in my_cust_data:
        #     setting_google_recaptcha_active = data['google_recaptcha_active']
        #     setting_google_captcha_client_key = data['google_captcha_client_key']
        #     setting_google_captcha_secret_key = data['google_captcha_secret_key']
        # # setting_google_recaptcha_active = request.env['website.support.settings'].google_recaptcha_active
        
        # if setting_google_recaptcha_active:

        #     # setting_google_captcha_secret_key = request.env['website.support.settings'].google_captcha_secret_key

        #     #Redirect them back if they didn't answer the captcha
        #     if 'g-recaptcha-response' not in values:
        #         return werkzeug.utils.redirect("/serial-no-check")

        #     payload = {'secret': setting_google_captcha_secret_key, 'response': str(values['g-recaptcha-response'])}
        #     response_json = requests.post("https://www.google.com/recaptcha/api/siteverify", data=payload)

        #     if response_json.json()['success'] is not True:
        #         return werkzeug.utils.redirect("/serial-no-check")
        # cur_id =  request.env.uid
        # partner = request.env['res.users'].search([('id','=',cur_id)])
        # pro_name = post['product_id']
        pro_serial = post['product_serial_id']
        pro_brand = post['brand_id']
        pro_brand_obj = request.env['product.brand'].sudo().search([('id','=', pro_brand)])
        pro_serial = pro_serial.upper()
        # pro_name = yaml.load(pro_name)
        # pro_serial = yaml.load(pro_serial)
        # # _logger.info('-----------------FYI: This is happening-------------')
        # _logger.info(pro_name['product'])
        # _logger.info(pro_serial['reg_serial'])
        # name = 'Warranty'+'/'+pro_serial['reg_serial']
        # support_obj = request.env['website.support.ticket'].sudo().search([('subject','=', name)])
        # product_obj = request.env['product.product'].sudo().search([('name','=', pro_name['product'])]) 
        serial_obj  = request.env['stock.production.lot'].sudo().search([('name','=', pro_serial)],limit=1)
        temp_prod = request.env['prod.web.temp']
        # claim_obj = request.env['warranty.claim'].sudo().search([('name','=', name)])
        # rec_id = request.env['product.warranty'].search([('product_serial_id','=',pro_serial['reg_serial'])], limit=1)
        # serial_lot_ws = request.env['stock.production.lot'].sudo().search([('product_id','=',product_obj.id),('name','=', pro_serial['reg_serial'])])
        # warranty_serial_exist = request.env['website.support.ticket'].sudo().search([('product_serial_id','=', serial_obj.id)])
       
        if serial_obj:
            if serial_obj.sale_order_count > 0.00:
                if serial_obj.sale_order_ids:
                    kc = serial_obj.sale_order_ids[0]
                    sale_status_var = "sold"
                    sales_obj  = request.env['sale.order'].sudo().search([('id','=', kc.id)],limit=1)
                    sale_id_var = sales_obj.name
                    customer_var = sales_obj.partner_id.name
                    sale_date_var =sales_obj.effective_date
            else:
                sale_id_var = "N/A"
                sale_status_var = "not sold"
                customer_var = "N/A"
                sale_date_var = False
        # for serial_existed in warranty_serial_exist:
            #if  serial_exist.product_serial_id == serial_no:
            # k_tmp = str(serial_obj.name)
            # request.env.cr.execute( """select stk.name as st_name, pt.name as p_name,bd.name as brand_name,pt.default_code as d_code from stock_production_lot as stk 
            # inner join product_template as pt on stk.product_id = pt.id inner join product_brand as bd on pt.product_brand_id = bd.id where stk.name =%s"""%(k_tmp))
            # cust_data = request.env.cr.dictfetchall()
            # cust_dataset = []
            # for data in serial_obj:
            prod_name = serial_obj.product_id.product_brand_id.name
            prod_model = serial_obj.product_id.product_tmpl_id.default_code
            prod_serial_num = serial_obj.name
            if prod_name ==  pro_brand_obj.name:
                temp_prod.sudo().create({
                    'name':prod_name,
                    'prod_model':prod_model,
                    'prod_serial_no':prod_serial_num,
                    'sale_order_id':sale_id_var,
                    'sale_status':sale_status_var,
                    'customer': customer_var,
                    'sale_date':sale_date_var,
                })
                return request.render("web_serial_check.auth_reg_thankyou")
            # if temp_prod:
            #     temp_prod.sudo().write({
            #         'name':prod_name,
            #         'prod_model':prod_model,
            #         'prod_serial_no':prod_serial_num
            #     })
            # else:
            else:
                return request.render("web_serial_check.auth_reg_thankyou_wrong")
        else:
            # return request.redirect("/serial-no-check?serial_no_msg=%s" % _("Invalid Serial No Please check once again!"))
            return request.render("web_serial_check.auth_reg_decline")
   
        
   
    
        
