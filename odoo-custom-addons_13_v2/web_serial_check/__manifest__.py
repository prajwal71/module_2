{
    "name" : "Check Product Authorization",
    "version" : "11.0.0.1",
    "category" : "Website",
    "depends" : ['website','portal','website_sale','product_brand','product'],
    "author": "Sagar/Nexus",
    'summary': 'This apps helps users to check authenticity of their product',
    "description": """
    """,
    "website" : "www.sagarcs.com",
    "price": 150,
    "currency": 'EUR',
    "data": [
        'data/data.xml',
        'views/auth_product_serial.xml',
        'views/product_brand_view.xml',
        'views/temp_view.xml',
    ],
    'qweb': [
    ],
    "auto_install": False,
    "installable": True,
    "images":["static/description/Banner.png"],
}

