# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
class ProductBrandTemp(models.Model):
    _inherit = 'product.brand'
    
    web_visibility = fields.Boolean('Web Visibility',default=False)

class ProductTemp(models.Model):
    _name = "prod.web.temp"
    _description = "Temp Product"

    name = fields.Char('Product Name')
    prod_model = fields.Char('Product Model')
    prod_serial_no = fields.Char('Product Serial')
    prod_date = fields.Datetime('Check time',default=datetime.now())
    sale_order_id = fields.Char(string="Sales Orders")
    customer = fields.Char(string="Customer")
    sale_date = fields.Date(string="Sales Date")
    sale_status = fields.Char(string="Sales Status")

    # @api.onchange('sale_status')
    # def onchange_name(self):
    #     if self.sale_status:
    #         if self.sale_status == 'sold':
    #             self.customer = self.sale_order_id.partner_id.id
    #             self.sale_date = self.sale_order_id.date_order 

class ProductProductid(models.Model):
    _inherit = 'product.product'
    _description = "Brand product Relation"

    # def get_brand(self):
    #     self.brand_id = self.product_tmpl_id.product_brand_id

    # brand_id = fields.Many2one('product.brand',default=get_brand)

class Website(models.Model):
    _inherit = 'website'

    # def get_country_list(self):            
    #     country_ids=self.env['res.country'].search([])
    #     return country_ids
        
    # def get_state_list(self):            
    #     state_ids=self.env['res.country.state'].search([])
    #     return state_ids
        
    def get_product_list(self):            
        product_ids=self.env['product.product'].search([('sale_ok','=','True'), ("website_published", "=", True)])
        return product_ids
    def get_brand_list(self):            
        brand_ids=self.env['product.brand'].search([('web_visibility','=','True')])
        return brand_ids
    def get_temp_product(self):          
        self.env.cr.execute( """select * from prod_web_temp ORDER BY ID DESC LIMIT 1""")
        cust_data = self.env.cr.dictfetchall()
        cust_dataset = []
        cust_dataset1 = []
        cust_dataset_s = {}
        for data in cust_data:
            cust_dataset_s = {
                'brand':data['name'],
                'name_model':data['prod_model'],
                'serial_no':data['prod_serial_no'],
                'sale_order_id':data['sale_order_id'],
                'sale_status':data['sale_status'],
                'customer':data['customer'],
                'sale_date':data['sale_date']
                }
            cust_dataset1.append(cust_dataset_s) 
        # print(cust_dataset) 
        return cust_dataset1  
        # temp_prod_ids=self.env['prod.web.temp'].search(['id','=',1])
        # return temp_prod_ids
    # def get_cust_product_list(self):            
    #     cur_id = self.env.uid
    #     partner = self.env['res.users'].search([('id','=',cur_id)])
    #     self.env.cr.execute( """select w.product_id ,w.warranty_create_date as reg_date,
    #     w.state as reg_status,s.name as reg_serial, p.name as product from product_warranty w 
    #     inner join product_template p  on w.product_id=p.id inner join stock_production_lot s 
    #     on w.product_serial_id=s.id where w.partner_id=%s"""%(partner.partner_id.id))
    #     cust_data = self.env.cr.dictfetchall()
    #     cust_dataset = []
    #     cust_dataset1 = []
    #     cust_dataset_s = {}
    #     for data in cust_data:
    #         if data['reg_status'] == 'new':
    #             data['reg_status'] = 'Waiting For Approval'
    #         elif data['reg_status'] == 'in_progress':
    #             data['reg_status'] = 'Under Warranty'
    #         cust_dataset_s = {
    #             'product':data['product'],
    #             'reg_date':data['reg_date'],
    #             'reg_serial':data['reg_serial'],
    #             'reg_status':data['reg_status']
    #             }
    #         cust_dataset1.append(cust_dataset_s) 
    #     print(cust_dataset) 
    #     return cust_dataset1
    # def get_my_product_list(self):
    #     self.env.cr.execute( """select pp.default_code from product_product as pp inner join product_template as tt on pp.id = 
    #     tt.id where tt.sale_ok = True and tt.website_published=True""")
    #     cust_data = self.env.cr.dictfetchall()
    #     cust_dataset = []
    #     cust_dataset1 = []
    #     for data in cust_data:
    #         cust_dataset1.append(data)      
    #     return cust_dataset1
    # def get_serial_list(self):            
    #     serial_ids=self.env['stock.production.lot'].search([])
    #     return serial_ids
    
    # def get_customer_list(self):            
    #     partners_ids=self.env['res.partner'].search([('customer','=','True')])
    #     return partners_ids
