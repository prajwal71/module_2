# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Unbuild Parts',
    'version': '13.0',
    'category': 'Manufacturing/Manufacturing',
    'summary': 'Unbuild parts',
    'author': 'Nexus Incorporation',
    'description': """
The aim is to have a complete module to manage all parts unbuilds.
====================================================================

The following topics are covered by this module:
------------------------------------------------------
    * Add/remove products in the unbuild
    * Impact for stocks
    * Lot line created in Stock lot.
""",
    'depends': ['stock','mrp'],
    'data': [
        'security/ir.model.access.csv',
        'security/unbuild_security.xml',
        'views/unbuild_views.xml',
        'views/stock_production_lot_line_views.xml',
        # 'wizard/stock_warn_insufficient_qty_views.xml',
        'data/ir_sequence.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'LGPL-3',
}
