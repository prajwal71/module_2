
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_compare
from json import dumps

import json
import logging

_logger = logging.getLogger(__name__)


class StockMove(models.Model):
    _inherit = 'stock.move'

    nexus_unbuild_id = fields.Many2one('unbuild.order', string='Unbuild Ref')


class Unbuild(models.Model):
    _name = 'unbuild.order'
    _description = 'Unbuild Order'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'create_date desc'

    @api.model
    def _default_stock_location(self):
        warehouse = self.env['stock.warehouse'].search([], limit=1)
        if warehouse:
            return warehouse.lot_stock_id.id
        return False

    name = fields.Char(
        'Unbuild Reference',
        default='/',
        copy=False, required=True,
        states={'confirmed': [('readonly', True)]})
    origin = fields.Char(
        'Source', copy=False,
        states={'done': [('readonly', True)], 'cancel': [('readonly', True)]},
        help="Reference of the document that generated this production order request.")
    product_id = fields.Many2one(
        'product.product', string='Product to Unbuild',
        domain="[('bom_ids', '!=', False), ('bom_ids.active', '=', True), ('bom_ids.type', '=', 'normal'), ('type', 'in', ['product', 'consu']), '|', ('company_id', '=', False), ('company_id', '=', company_id)]",
        readonly=True, required=True, check_company=True,
        states={'draft': [('readonly', False)]})
    product_qty = fields.Float(
        'Product Quantity',
        default=1.0, digits='Product Unit of Measure',
        required=True, states={'draft': [('readonly', False)]})
    product_uom = fields.Many2one(
        'uom.uom', 'Product Unit of Measure',
        readonly=True, required=True, states={'draft': [('readonly', False)]}, domain="[('category_id', '=', product_uom_category_id)]")
    # bom_line_ids = fields.Many2one(related='mrp.bom')
    product_uom_category_id = fields.Many2one(related='product_id.uom_id.category_id')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('confirmed', 'Confirmed'),
        ('under_unbuild', 'Under Unbuild'),
        ('ready', 'Ready to Unbuild'),
        ('done', 'Unbuilded')], string='Status',
        copy=False, default='draft', readonly=True, tracking=True,
        help="* The \'Draft\' status is used when a user is encoding a new and unconfirmed unbuild order.\n"
             "* The \'Confirmed\' status is used when a user confirms the unbuild order.\n"
             "* The \'Ready to Unbuild\' status is used to start to unbuilding, user can start unbuilding only after unbuild order is confirmed.\n"
             "* The \'Done\' status is set when Unbuilding is completed.\n"
             "* The \'Cancelled\' status is used when user cancel unbuild order.")
    location_id = fields.Many2one(
        'stock.location', 'Source Location',
        index=True, readonly=True, required=True,
        help="This is the location where the product to unbuild is located.",
        states={'draft': [('readonly', False)], 'confirmed': [('readonly', True)]})
    location_dest_id = fields.Many2one(
        'stock.location', 'Dest. Location',
        index=True, readonly=True, required=True,
        help="This is the location where the product to unbuild will be moved.",
        states={'draft': [('readonly', False)], 'confirmed': [('readonly', True)]})
    lot_id = fields.Many2one(
        'stock.production.lot', 'Lot/Serial',
        domain="[('product_id','=', product_id)]",
        help="Products unbuilded are all belonging to this lot")
    operations = fields.One2many(
        'unbuild.line', 'nexus_unbuild_id', 'Parts',
        copy=True, readonly=True, states={'draft': [('readonly', False)]})
    move_id = fields.Many2one(
        'stock.move', 'Move',
        copy=False, readonly=True, tracking=True,
        help="Move created by the unbuild order")
    internal_notes = fields.Text('Internal Notes')
    user_id = fields.Many2one('res.users', string="Responsible", default=lambda self: self.env.user)
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env.company)
    unbuilded = fields.Boolean('unbuilded', copy=False, readonly=True)
    tracking = fields.Selection('Product Tracking', related="product_id.tracking", readonly=False)
    bom_id = fields.Many2one(
        'mrp.bom', 'Bill of Material',
        readonly=True, states={'draft': [('readonly', False)]},
        domain="""[
        '&',
            '|',
                ('company_id', '=', False),
                ('company_id', '=', company_id),
            '&',
                '|',
                    ('product_id','=',product_id),
                    '&',
                        ('product_tmpl_id.product_variant_ids','=',product_id),
                        ('product_id','=',False),
        ('type', '=', 'normal')]""",
        check_company=True,
        help="Bill of Materials allow you to define the list of required components to make a finished product.")

    @api.onchange('product_id')
    def onchange_product_id(self):
        if (self.product_id and self.lot_id and self.lot_id.product_id != self.product_id) or not self.product_id:
            self.lot_id = False
        if self.product_id:
            self.product_uom = self.product_id.uom_id.id

    @api.onchange('product_uom')
    def onchange_product_uom(self):
        res = {}
        if not self.product_id or not self.product_uom:
            return res
        if self.product_uom.category_id != self.product_id.uom_id.category_id:
            res['warning'] = {'title': _('Warning'), 'message': _('The product unit of measure you chose has a different category than the product unit of measure.')}
            self.product_uom = self.product_id.uom_id.id
        return res

    @api.model
    def create(self, vals):
        # To avoid consuming a sequence number when clicking on 'Create', we preprend it if the
        # the name starts with '/'.
        vals['name'] = vals.get('name') or '/'
        if vals['name'].startswith('/'):
            vals['name'] = (self.env['ir.sequence'].next_by_code('unbuild.order') or '/') + vals['name']
            vals['name'] = vals['name'][:-1] if vals['name'].endswith('/') and vals['name'] != '/' else vals['name']
        return super(Unbuild, self).create(vals)

    def unlink(self):
        if self.state == 'done':
            raise UserError(_('The Unbuild Order cannot be deleted in done state.'))
        return super(Unbuild, self).unlink()

    def button_dummy(self):
        # TDE FIXME: this button is very interesting
        return True

    def action_unbuild_cancel_draft(self):
        if self.filtered(lambda unbuild: unbuild.state != 'cancel'):
            raise UserError(_("Unbuild must be canceled in order to reset it to draft."))
        self.mapped('operations').write({'state': 'draft'})
        return self.write({'state': 'draft'})

    # def action_validate(self):
    #     self.ensure_one()
    #     if self.filtered(lambda unbuild: any(op.product_uom_qty < 0 for op in unbuild.operations)):
    #         raise UserError(_("You can not enter negative quantities."))
    #     if self.product_id.type == 'consu':
    #         return self.action_unbuild_confirm()
    #     precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
    #     available_qty_owner = self.env['stock.quant']._get_available_quantity(self.product_id, self.location_id, self.lot_id, strict=True)
    #     available_qty_noown = self.env['stock.quant']._get_available_quantity(self.product_id, self.location_id, self.lot_id, strict=True)
    #     for available_qty in [available_qty_owner, available_qty_noown]:
    #         if float_compare(available_qty, self.product_qty, precision_digits=precision) >= 0:
    #             return self.action_unbuild_confirm()
    #     else:
    #         return {
    #             'name': _('Insufficient Quantity'),
    #             'view_mode': 'form',
    #             'res_model': 'stock.warn.insufficient.qty.unbuild',
    #             'view_id': self.env.ref('unbuild.stock_warn_insufficient_qty_unbuild_form_view').id,
    #             'type': 'ir.actions.act_window',
    #             'context': {
    #                 'default_product_id': self.product_id.id,
    #                 'default_location_id': self.location_id.id,
    #                 'default_nexus_unbuild_id': self.id
    #                 },
    #             'target': 'new'
    #         }

    def action_unbuild_confirm(self):
        if self.filtered(lambda unbuild: unbuild.state != 'draft'):
            raise UserError(_("Only draft Unbuilds can be confirmed."))
        self.mapped('operations').write({'state': 'confirmed'})
        return self.write({'state': 'confirmed'})

    def action_unbuild_cancel(self):
        if self.filtered(lambda unbuild: unbuild.state == 'done'):
            raise UserError(_("Cannot cancel completed unbuilds."))
        self.mapped('operations').write({'state': 'cancel'})
        return self.write({'state': 'cancel'})

    def action_unbuild_ready(self):
        self.mapped('operations').write({'state': 'confirmed'})
        return self.write({'state': 'ready'})

    def action_unbuild_start(self):
        """ Writes unbuild order state to 'Under unbuild'
        @return: True
        """
        if self.filtered(lambda unbuild: unbuild.state not in ['confirmed', 'ready']):
            raise UserError(_("unbuild must be confirmed before starting reparation."))
        self.mapped('operations').write({'state': 'confirmed'})
        return self.write({'state': 'under_unbuild'})

    def action_unbuild_end(self):
        if self.filtered(lambda unbuild: unbuild.state != 'under_unbuild'):
            raise UserError(_("unbuild must be under unbuild in order to end reparation."))
        for unbuild in self:
            unbuild.write({'unbuilded': True})
            vals = {'state': 'done'}
            vals['move_id'] = unbuild.action_unbuild_done().get(unbuild.id)
            unbuild.write(vals)
        return True

    def action_unbuild_done(self):
        """ Creates stock move for operation and stock move for final product of unbuild order.
        @return: Move ids of final products

        """
        if self.filtered(lambda unbuild: not unbuild.unbuilded):
            raise UserError(_("unbuild must be unbuilded in order to make the product moves."))
        res = {}
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        Move = self.env['stock.move']
        for unbuild in self:
            # Try to create move with the appropriate owner
            owner_id = False
            available_qty_owner = self.env['stock.quant']._get_available_quantity(unbuild.product_id, unbuild.location_id, unbuild.lot_id, strict=True)
            moves = self.env['stock.move']
            for operation in unbuild.operations:
                move = Move.create({
                    'name': unbuild.name,
                    'product_id': operation.product_id.id,
                    'product_uom_qty': operation.product_uom_qty,
                    'product_uom': operation.product_uom.id,
                    'location_id': operation.location_id.id,
                    'location_dest_id': operation.location_dest_id.id,
                    'nexus_unbuild_id': unbuild.id,
                    'origin': unbuild.name,
                })

                # self.action.write({
                #     'lot_line_ids': [(0, 0, [action1.id, action2.id, action3.id])],
                # })
                record_ids = self.env['stock.production.lot'].search([('id', '=', unbuild.lot_id.id)])
                for record in record_ids:
                    record.write({
                        'lot_line_ids': [(0, 0, {
                            'product_id': operation.product_id.id,
                            'lot_id': operation.lot_id.id,
                            'lot_line_id': record.id,
                            'type': operation.type,
                            'origin': unbuild.name,
                            'parent_product_id': unbuild.product_id.id,
                            'parent_lot_id': unbuild.lot_id.id,
                            'product_qty': operation.product_uom_qty,
                            'product_uom_id': operation.product_uom.id or operation.product_id.uom_id.id, })],
                    })
                #     _logger.error('----------------------------------------------------%s', record)
                # _logger.error('----------------------------------------------------%s', record)

                # stock_quant_ids = self.env['stock.quant'].search([('location_id', '=', self.location_id.id), ('lot_id', '=', self.lot_id.id)])
                # for quant in stock_quant_ids:
                #     quant.sudo().write({
                #         'location_id': self.location_dest_id.id
                #     })
                # _logger.info('----------------------------------------------------%s', stock_quant_ids)

                # Best effort to reserve the product in a (sub)-location where it is available
                product_qty = move.product_uom._compute_quantity(
                    operation.product_uom_qty, move.product_id.uom_id, rounding_method='HALF-UP')
                available_quantity = self.env['stock.quant']._get_available_quantity(
                    move.product_id,
                    move.location_id,
                    lot_id=operation.lot_id,
                    strict=False,
                )
                move._update_reserved_quantity(
                    product_qty,
                    available_quantity,
                    move.location_id,
                    lot_id=operation.lot_id,
                    strict=False,
                )
                # Then, set the quantity done. If the required quantity was not reserved, negative
                # quant is created in operation.location_id.
                move._set_quantity_done(operation.product_uom_qty)

                if operation.lot_id:
                    move.move_line_ids.lot_id = operation.lot_id

                moves |= move
                operation.write({'move_id': move.id, 'state': 'done'})

            move = Move.create({
                'name': unbuild.name,
                'product_id': unbuild.product_id.id,
                'product_uom': unbuild.product_uom.id or unbuild.product_id.uom_id.id,
                'product_uom_qty': unbuild.product_qty,
                'location_id': unbuild.location_id.id,
                'location_dest_id': unbuild.location_dest_id.id,
                'move_line_ids': [(0, 0, {'product_id': unbuild.product_id.id,
                                           'lot_id': unbuild.lot_id.id,
                                           'product_uom_qty': 0,  # bypass reservation here
                                           'product_uom_id': unbuild.product_uom.id or unbuild.product_id.uom_id.id,
                                           'qty_done': unbuild.product_qty,
                                           'package_id': False,
                                           'result_package_id': False,
                                           'owner_id': owner_id,
                                           'location_id': unbuild.location_id.id, #TODO: owner stuff
                                           'location_dest_id': unbuild.location_dest_id.id,})],
                'nexus_unbuild_id': unbuild.id,
                'origin': unbuild.name,
            })

            consumed_lines = moves.mapped('move_line_ids')
            produced_lines = move.move_line_ids
            moves |= move
            moves._action_done()
            produced_lines.write({'consume_line_ids': [(6, 0, consumed_lines.ids)]})
            res[unbuild.id] = move.id
        return res

    # @api.onchange('bom_id')
    # def onchange_product_id(self):
    #     if self.bom_id:
    #         product_lists = []
    #         for bom_line in self.bom_id.bom_line_ids:
    #             product_list = {
    #                 self.operations.product_id : bom_line.product_id.id,
    #             }
    #             product_lists.append(product_list)
    #         return product_lists


    # @api.onchange('product_id')
    # def onchange_product_id(self):
    #     product_ids_list = []
    #     if self._context.get('bom_id'):
    #         for bom_line in self.bom_id.bom_line_ids:
    #             product_ids_list.append(bom_line.product_id)
    #             return {'domain': {'product_id': [('product_id', 'in', product_ids_list)]}}

    # @api.onchange('product_id')
    # def onchange_product_id(self):
    #     if self.product_id:
    #         for bom_line in self.bom_id.bom_line_ids:
    #
    #             return {
    #                 'domain': {'product': [('product_id', '=', self.product_id)]}}


    # @api.onchange('product_id', 'company_id')
    # def onchange_product_id(self):
    #     """ Finds UoM of changed product. """
    #     if not self.product_id:
    #         self.bom_id = False
    #     else:
    #         bom = self.env['mrp.bom']._bom_find(product=self.product_id, company_id=self.company_id.id, bom_type='normal')
    #         if bom:
    #             self.bom_id = bom.id
    #             self.product_qty = self.bom_id.product_qty
    #             self.product_uom = self.bom_id.product_uom_id.id
    #         else:
    #             self.bom_id = False
    #             self.product_uom = self.product_id.uom.id
    #         return {'domain': {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}}
    #


class UnbuildLine(models.Model):
    _name = 'unbuild.line'
    _description = 'Unbuild Line (parts)'

    name = fields.Text('Description', required=True)
    nexus_unbuild_id = fields.Many2one(
        'unbuild.order', 'Unbuild Order Reference',
        index=True, ondelete='cascade')
    type = fields.Selection([
        ('add', 'Add'),
        ('remove', 'Remove')], 'Type', default='add', required=True)
    product_id = fields.Many2one('product.product', 'Product', required=True)
    product_uom_qty = fields.Float(
        'Quantity', default=1.0,
        digits='Product Unit of Measure', required=True)
    product_uom = fields.Many2one(
        'uom.uom', 'Product Unit of Measure',
        required=True, domain="[('category_id', '=', product_uom_category_id)]")
    product_uom_category_id = fields.Many2one(related='product_id.uom_id.category_id')
    location_id = fields.Many2one(
        'stock.location', 'Source Location',
        index=True, required=True)
    location_dest_id = fields.Many2one(
        'stock.location', 'Dest. Location',
        index=True, required=True)
    move_id = fields.Many2one(
        'stock.move', 'Inventory Move',
        copy=False, readonly=True)
    lot_id = fields.Many2one('stock.production.lot', 'Lot/Serial')
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env.company)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')], 'Status', default='draft',
        copy=False, readonly=True, required=True,
        help='The status of a unbuild line is set automatically to the one of the linked unbuild order.')
    product_id_domain = fields.Char(
        compute="_compute_product_id_domain",
        readonly=True,
        store=False,
    )

    @api.constrains('lot_id', 'product_id')
    def constrain_lot_id(self):
        for line in self.filtered(lambda x: x.product_id.tracking != 'none' and not x.lot_id):
            raise ValidationError(_("Serial number is required for operation line with product '%s'") % (line.product_id.name))

    @api.onchange('type', 'nexus_unbuild_id')
    def onchange_operation_type(self):
        """ On change of operation type it sets source location, destination location.
        @param product: Changed operation type.
        @return: Dictionary of values.
        """
        args = self.nexus_unbuild_id.company_id and [('company_id', '=', self.nexus_unbuild_id.company_id.id)] or []
        warehouse = self.env['stock.warehouse'].search(args, limit=1)
        if not self.type:
            self.location_id = False
            self.location_dest_id = False
        elif self.type == 'add':
            self.onchange_product_id()
            self.location_id = warehouse.lot_stock_id
            self.location_dest_id = self.env['stock.location'].search([('usage', '=', 'production')], limit=1).id
        else:
            self.location_id = self.env['stock.location'].search([('usage', '=', 'production')], limit=1).id
            self.location_dest_id = warehouse.lot_stock_id

    @api.onchange('nexus_unbuild_id', 'product_id', 'product_uom_qty', 'nexus_unbuild_id.product_id')
    def onchange_product_id(self):
        self.ensure_one()
        product_ids_list = []
        """ On change of product it sets product quantity, name,
        uom of product. """
        if not self.product_id or not self.product_uom_qty:
            return
        if self.product_id:
            self.name = self.product_id.display_name
            if self.product_id.description_sale:
                self.name += '\n' + self.product_id.description_sale
            self.product_uom = self.product_id.uom_id.id

        # if self.nexus_unbuild_id.product_id:
        #     for bom_line in self.nexus_unbuild_id.bom_id.bom_line_ids:
        #         product_ids_list.append(bom_line.product_id.id)
        #         return {'domain': {'product_id': [('product_id', 'in', product_ids_list)]}}
        #         _logger.error('----------------------------------------------------%s', bom_line)
        #         _logger.error('----------------------------------------------------%s', product_ids_list)
        #     _logger.error('----------------------------------------------------%s', product_ids_list)

    # @api.depends('nexus_unbuild_id.product_id')
    # def _compute_product_id_domain(self):
    #     for rec in self:
    #         # rec.product_id_domain = json.dumps([('product_id', '=', rec.nexus_unbuild_id.product_id.id)])
    #         return True

    # @api.onchange('nexus_unbuild_id.product_id')
    # def onchange_product_id(self):
    #     product_ids_list = []
    #     if self._context.get('bom_id'):
    #         for bom_line in self.nexus_unbuild_id.bom_id.bom_line_ids:
    #             product_ids_list.append(bom_line.product_id.id)
    #             return {'domain': {'product_id': [('product_id', 'in', product_ids_list)]}}
    #             _logger.error('----------------------------------------------------%s', bom_line)
    #             _logger.error('----------------------------------------------------%s', product_ids_list)
    #         _logger.error('----------------------------------------------------%s', product_ids_list)
