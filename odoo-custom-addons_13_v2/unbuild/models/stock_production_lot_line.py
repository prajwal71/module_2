# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.osv import expression


class ProductionLot(models.Model):
    _inherit = 'stock.production.lot'

    lot_line_ids = fields.One2many(
        'stock.production.lot.line', 'lot_line_id', 'Parts',
        copy=True, readonly=True)


class ProductionLotLine(models.Model):
    _name = 'stock.production.lot.line'
    _inherit = ['mail.thread','mail.activity.mixin']
    _description = 'Lot/Serial Line'
    _check_company_auto = True

    origin = fields.Char('Source')
    lot_line_id = fields.Many2one('stock.production.lot', 'Serial/Lots', index=True, ondelete='cascade')
    lot_id = fields.Many2one(
        'stock.production.lot', 'Lot/Serial',
        domain="[('product_id','=', product_id)]",
        help="Products unbuilded are all belonging to this lot")
    product_id = fields.Many2one(
        'product.product', 'Product', check_company=True)
    parent_product_id = fields.Many2one(
        'product.product', 'Parent Product',)
    parent_lot_id = fields.Many2one(
        'stock.production.lot', 'Parent Lot',)
    product_uom_id = fields.Many2one(
        'uom.uom', 'Unit of Measure',
        related='product_id.uom_id', store=True, readonly=False)
    product_qty = fields.Float('Quantity')
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env.company)
    type = fields.Selection([
        ('add', 'Added'),
        ('remove', 'Removed')], string='Type')


