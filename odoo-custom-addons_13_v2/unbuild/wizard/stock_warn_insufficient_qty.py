# # -*- coding: utf-8 -*-
# # Part of Odoo. See LICENSE file for full copyright and licensing details.
#
# from odoo import fields, models
#
# class StockWarnInsufficientQtyUnbuild(models.TransientModel):
#     _name = 'stock.warn.insufficient.qty.unbuild'
#     _inherit = 'stock.warn.insufficient.qty'
#     _description = 'Warn Insufficient Unbuild Quantity'
#
#     nexus_unbuild_id = fields.Many2one('unbuild.order', string='Unbuild Ref')
#
#     def _get_reference_document_company_id(self):
#         return self.nexus_unbuild_id.company_id
#
#     def action_done(self):
#         self.ensure_one()
#         return self.nexus_unbuild_id.action_unbuild_confirm()
