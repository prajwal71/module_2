# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ResCompanyInherited(models.Model):
    _inherit = "res.company"

    sparrow_sms_active  = fields.Boolean(string='Activate Sparrow sms api Key', help="Activate Sparrow SMS API Key")
    sparrow_sms_api_key  = fields.Char(string='Sparrow sms api Key', help="Sparrow SMS API Key")
    sparrow_sms_from  = fields.Char(string='Sparrow sms Sender Name', help="Sparrow SMS Sender Name")

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    sparrow_sms_active  = fields.Boolean(string='Activate Sparrow sms api Key',related='company_id.sparrow_sms_active', help="Activate Sparrow SMS API Key",readonly=False)
    sparrow_sms_api_key  = fields.Char(string='Sparrow sms api Key',related='company_id.sparrow_sms_api_key',help="Sparrow SMS API Key",readonly=False)
    sparrow_sms_from  = fields.Char(string='Sparrow sms Sender Name',related='company_id.sparrow_sms_from', help="Sparrow SMS Sender Name",readonly=False)
    

    # def set_values(self):
    #     res = super(ResConfigSettings, self).set_values()
    #     self.env['ir.config_parameter'].sudo().set_param('sms_automation.sparrow_sms_active', self.sparrow_sms_active)
    #     self.env['ir.config_parameter'].sudo().set_param('sms_automation.sparrow_sms_api_key', self.sparrow_sms_api_key)
    #     self.env['ir.config_parameter'].sudo().set_param('sms_automation.sparrow_sms_from', self.sparrow_sms_from)
    #     return res