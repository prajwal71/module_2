# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import api,fields,models,_
from odoo.exceptions import UserError, ValidationError
#from odoo.tools.pycompat import izip
from odoo.tools.float_utils import float_round, float_compare, float_is_zero
import logging
_logger = logging.getLogger(__name__)
import json
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from collections import defaultdict
from math import floor

class MrpSteel(models.Model):
    _inherit = "mrp.production"

    @api.depends('move_raw_ids.product_id')
    def _compute_duration(self):
        duration_consumed = self.env['mrp.workorder'].search([('production_id', 'in', self.ids)],limit=1)
        for order in self:
            duration = 0.0 
            if duration_consumed:
                for k in duration_consumed.time_ids:
                    duration += k.duration
            order.total_production_duration = duration

    @api.depends('move_raw_ids.product_id')
    def _compute_duration_min(self):
        duration_consumed = self.env['mrp.workorder'].search([('production_id', 'in', self.ids)],limit=1)
        for order in self:
            duration = 0.0 
            if duration_consumed:
                for k in duration_consumed.time_ids:
                    duration += k.duration
            order.total_production_duration_min = duration/60
    
    @api.depends('move_raw_ids.product_id')
    def _compute_duration_loss_min(self):
        duration_consumed = self.env['mrp.workorder'].search([('production_id', 'in', self.ids)],limit=1)
        for order in self:
            duration_loss = 0.0 
            if duration_consumed:
                for k in duration_consumed.time_ids:
                    duration_loss += k.loss_duration
            order.total_loss_duration_min = duration_loss/60

    @api.depends('move_raw_ids.product_id')
    def _compute_duration_loss(self):
        duration_consumed = self.env['mrp.workorder'].search([('production_id', 'in', self.ids)],limit=1)
        for order in self:
            duration_loss = 0.0 
            if duration_consumed:
                for k in duration_consumed.time_ids:
                    duration_loss += k.loss_duration
            order.total_loss_duration = duration_loss


    @api.depends('total_production','total_production_duration')
    def _production_ton_per_hr(self):
        """
        Compute the amounts
        """
        for order in self:
            product_tons_hr = 0.0
            if order.total_production and order.total_production_duration:
                product_tons_hr = order.total_production / (order.total_production_duration/60)
            order.update({
                    'product_tons_hr': product_tons_hr,
                })
    
    @api.depends('product_tons','missroll')
    def _production_ton_load(self):
        """
        Compute the amounts
        """
        for order in self:
            product_tons_load = 0.0
            if order.product_tons and order.missroll:
                product_tons_load = (order.product_tons + order.missroll)
            order.update({
                    'product_tons_load': product_tons_load,
                })

    @api.depends('move_raw_ids.quantity_done')
    def _production_ton(self):
        """
        Compute the amounts
        """
        for order in self:
            product_tons = 0.0
            # product_tons = self.env['stock.move'].search([('product_id.is_type', '=', 'billet'),('raw_material_production_id','=',self.id)])
            product_tons = self.move_raw_ids.filtered(lambda l: l.product_id.is_type  == 'billet')
            _logger.info("======Product tons=====%s",product_tons)
            if product_tons:
                summed_weight = 0
                for bil in product_tons:
                    summed_weight += ((bil.quantity_done*bil.product_id.weight)/1000)
                order.update({
                    'product_tons': summed_weight - order.missroll,
                })
                


    @api.depends('move_raw_ids.quantity_done')
    def _production_litre(self):
        """
        Compute the amounts
        """
        for order in self:
            consumption = 0.0
            # consumption = self.env['stock.move'].search([('product_id.is_type', '=', 'oil'),('raw_material_production_id','=',self.id)],limit=1)
            consumption = self.move_raw_ids.filtered(lambda l: l.product_id.is_type  == 'oil')
            if order.total_production:
                summed_weight = 0
                for oil in consumption:
                    summed_weight += oil.quantity_done
                order.update({
                    'consumption': summed_weight / order.total_production,
                })

    @api.depends('move_raw_ids.quantity_done')
    def _production_power(self):
        """
        Compute the amounts
        """
        for order in self:
            power_consumption = 0.0
            # power_consumption = self.env['stock.move'].search([('product_id.is_type', '=', 'power'),('raw_material_production_id','=',self.id)],limit=1)
            power_consumption = self.move_raw_ids.filtered(lambda l: l.product_id.is_type  == 'power')
            if order.total_production:
                summed_weight = 0
                for power in power_consumption:
                    summed_weight += power.quantity_done
                order.update({
                    'power_consumption': summed_weight / order.total_production,
                })

    @api.depends('move_finished_ids.quantity_done')
    def _production_misroll(self):
        """
        Compute the amounts
        """
        for order in self:
            product_misroll = 0.0
            # product_misroll = self.env['stock.move'].search([('product_id.is_type', '=', 'missroll'),('production_id','=',self.id)],limit=1)
            
            product_misroll = self.move_finished_ids.filtered(lambda l: l.product_id.is_type  == 'missroll')
            if product_misroll:
                summed_weight = 0
                for misroll in product_misroll:
                    summed_weight += (misroll.quantity_done)          
                order.update({
                        'missroll': (summed_weight/1000),
                    })

    @api.depends('move_finished_ids.quantity_done')
    def _production_endouting(self):
        """
        Compute the amounts
        """
        for order in self:
            product_endcut = 0.0
            # product_endcut = self.env['stock.move'].search([('product_id.is_type', '=', 'endcut'),('production_id','=',order.id)],limit=1)
            product_endcut = self.move_finished_ids.filtered(lambda l: l.product_id.is_type  == 'endcut')
            if product_endcut:
                summed_weight = 0
                for endcut in product_endcut:
                    summed_weight += (endcut.quantity_done)
                order.update({
                    'end_outing':(summed_weight/1000),
                })

    @api.depends('move_finished_ids.quantity_done')
    def _production_dust(self):
        """
        Compute the amounts
        """
        for order in self:
            product_dust = 0.0
            # product_dust = self.env['stock.move'].search([('product_id.is_type', '=', 'dust'),('production_id','=',order.id)],limit=1)
            product_dust = self.move_finished_ids.filtered(lambda l: l.product_id.is_type  == 'dust')
            if product_dust:
                summed_weight = 0
                for dust in product_dust:
                    summed_weight += (dust.quantity_done)
                order.update({
                   'burning_lossed':(summed_weight/1000)
                })


    @api.depends('missroll','burning_lossed','end_outing')
    def _production_loss(self):
        """
        Compute the amounts
        """
        for order in self:
            total_loss = 0.0
            total_loss = order.missroll + order.burning_lossed + order.end_outing
            if total_loss:
                order.update({
                        'total_loss': total_loss
                    })

    @api.depends('product_tons','total_loss')
    def _total_production(self):
        """
        Compute the amounts
        """
        for order in self:
            total_production = 0.0
            total_production = order.product_tons_load - order.total_loss
            if total_production:
                order.update({
                        'total_production': total_production
                    })

    

        
    shift = fields.Selection(selection=[
            ('day', 'Day'),
            ('night', 'Night'),
            ('daynight', 'Day/Night'),
        ], string='Shift', default='day')

    # grade = fields.Char("Grade")
    grade_id = fields.Many2one("mrp.production.grade", string="Grade")
    water_flow = fields.Char("Water Flow Rate")
    water_pr = fields.Char("Water Pressure")
    # attachment = fields.Binary(string="Attachment")
    attachment_ids = fields.Many2many('ir.attachment', string='Attachments') 
  

    product_tons = fields.Float("Billets Used in Production", store=True, compute="_production_ton")
    product_tons_load = fields.Float("Total Loaded(Tons)", store=True, compute="_production_ton_load")
    product_tons_hr = fields.Float("Productivity Tons/ hour",store=True, compute="_production_ton_per_hr")
    consumption = fields.Float("F.O Consumption Ltr/Ton", store=True, compute="_production_litre")
    power_consumption = fields.Float("Power Consumption Kwh/Ton", store=True, compute="_production_power")
    missroll = fields.Float("Missroll (Tons)", store=True, compute="_production_misroll")
    burning_lossed = fields.Float("Burning Lossed", store=True, compute="_production_dust")
    end_outing = fields.Float("End Outing", store=True, compute="_production_endouting")
    total_loss = fields.Float("Total Loss",store=True, compute="_production_loss")
    total_production = fields.Float("Total Production", store=True, compute="_total_production")
    total_production_duration= fields.Float("Total Production Duraton", store=True, compute="_compute_duration")
    total_production_duration_min= fields.Float("Total Production Duraton Min", store=True, compute="_compute_duration_min")
    total_loss_duration= fields.Float("Total Loss Duraton", store=True, compute="_compute_duration_loss")
    total_loss_duration_min= fields.Float("Total Loss Duraton Min", store=True, compute="_compute_duration_loss_min")
    lab_responsible = fields.Many2one('res.users',"Lab Responsible")
    
    def schedule_lab(self):
        for rec in self:
            if not rec.lab_responsible:
                raise UserError(_('Please select a Lab Responsible'))
            # active_model = self._context.get('active_model')
            _logger.info("dfdf%s",self._name)
            create_vals = {
                'activity_type_id': 1,
                'summary': "Please upload the Lab Report for" + rec.name,
                'automated': True,
                'note': rec.origin,
                'date_deadline': rec.date_deadline,
                'res_model_id': self.env['ir.model'].search([('model', '=', self._name)]).id,
                'res_id': rec.id,
                'user_id': rec.lab_responsible.id,
            }
            self.env['mail.activity'].create(create_vals)
        


    def button_mark_done(self):
        a = self.manufacturing_type
        _logger.info("==========my==call=====%s",a)
        if self.attachment_ids:
            self.ensure_one()
            self._check_company()
            for wo in self.workorder_ids:
                if wo.time_ids.filtered(lambda x: (not x.date_end) and (x.loss_type in ('productive', 'performance'))):
                    raise UserError(_('Work order %s is still running') % wo.name)
            self._check_lots()

            self.post_inventory()
            # Moves without quantity done are not posted => set them as done instead of canceling. In
            # case the user edits the MO later on and sets some consumed quantity on those, we do not
            # want the move lines to be canceled.
            (self.move_raw_ids | self.move_finished_ids).filtered(lambda x: x.state not in ('done', 'cancel')).write({
                'state': 'done',
                'product_uom_qty': 0.0,
            })
            return self.write({'date_finished': fields.Datetime.now()})
        
        elif self.manufacturing_type=='pre':
            self.ensure_one()
            self._check_company()
            for wo in self.workorder_ids:
                if wo.time_ids.filtered(lambda x: (not x.date_end) and (x.loss_type in ('productive', 'performance'))):
                    raise UserError(_('Work order %s is still running') % wo.name)
            self._check_lots()

            self.post_inventory()
            # Moves without quantity done are not posted => set them as done instead of canceling. In
            # case the user edits the MO later on and sets some consumed quantity on those, we do not
            # want the move lines to be canceled.
            (self.move_raw_ids | self.move_finished_ids).filtered(lambda x: x.state not in ('done', 'cancel')).write({
                'state': 'done',
                'product_uom_qty': 0.0,
            })
            return self.write({'date_finished': fields.Datetime.now()})
        else:
            raise UserError('Please attach Lab report before Mark as Done')

    def calculate_values(self):
        self._total_production()
        self._production_loss()
        self._production_dust()
        self._production_endouting()
        self._production_misroll()
        self._production_power()
        self._production_litre()
        self._production_ton()
        self._production_ton_load()
        self._production_ton_per_hr()
        self._compute_duration_loss()
        self._compute_duration_loss_min()
        self._compute_duration_min()
        self._compute_duration()


class MrpProductionGrade(models.Model):
    _name = "mrp.production.grade"

    name = fields.Char(string="Grade")

class MrpProductTemplate(models.Model):
    _inherit = "product.template"

    is_type = fields.Selection(selection=[
            ('oil', 'Furnace oil'),
            ('power', 'Power'),
            ('billet', 'Billet'),
            ('missroll', 'Miss roll'),
            ('endcut', 'End Cutting'),
            ('dust', 'Dust'),
            ('none', 'None'),
        ], string='Material Type')
    
    # @api.model
    # def create(self,vals):
    #     a = vals.get('is_type')
    #     if a != 'none' and a != False:
    #         check = self.env['product.template'].search([('is_type','=',a)])
    #         if check:
    #            raise UserError(_("Sorry,%s Material Type is Already Used")
    #                            % a)
    #     res = super(MrpProductTemplate,self).create(vals)


    #     _logger.info("========create=======%s=",a)
    #     return res
    
class MrpWorkCentreProductivity(models.Model):
    _inherit = "mrp.workcenter.productivity"

    loss_duration = fields.Float('Duration Loss', compute='_compute_duration_loss', store=True)

    @api.depends('date_end', 'date_start')
    def _compute_duration(self):
        for blocktime in self:
            if blocktime.date_end:
                d1 = fields.Datetime.from_string(blocktime.date_start)
                d2 = fields.Datetime.from_string(blocktime.date_end)
                diff = d2 - d1
                if (blocktime.loss_type in ('productive', 'performance')):
                    blocktime.duration = round(diff.total_seconds() / 60.0, 2)
                   
            else:
                blocktime.duration = 0.0


    @api.depends('date_end', 'date_start')
    def _compute_duration_loss(self):
        for blocktime in self:
            if blocktime.date_end:
                d1 = fields.Datetime.from_string(blocktime.date_start)
                d2 = fields.Datetime.from_string(blocktime.date_end)
                diff = d2 - d1
                if (blocktime.loss_type in ('quality', 'availability')):
                    blocktime.loss_duration = round(diff.total_seconds() / 60.0, 2)
                    
            else:
                blocktime.loss_duration = 0.0

class MrpWorkOrder(models.Model):
    _inherit = "mrp.workorder"


    @api.onchange('raw_workorder_line_ids','finished_workorder_line_ids')
    def production_burning_wo(self):
        """
        Compute the amounts
        """
        for rec in self:
            _logger.info("=================43534534=====%s",self.id)
            _logger.info("=================herer=====%s",rec.id)
            product_endcut = rec.raw_workorder_line_ids.filtered(lambda l: l.product_id.is_type  == 'billet')
            _logger.info("=================herer=====%s",product_endcut)
            product_missroll = rec.finished_workorder_line_ids.filtered(lambda l: l.product_id.is_type  == 'missroll').qty_done
            summed_weight = 0.0
            for bil in product_endcut:
                summed_weight += (bil.qty_done * bil.product_id.weight)
            if product_endcut:
                for lines in rec.finished_workorder_line_ids:
                    if lines.product_id.is_type == 'endcut':
                        lines.qty_done = ((summed_weight - product_missroll) * 0.015)
                    elif lines.product_id.is_type == 'dust':
                        lines.qty_done = ((summed_weight - product_missroll) * 0.02)

    @api.onchange('raw_workorder_line_ids','finished_workorder_line_ids')
    def calc_qty_producing(self):
        """
        Compute the amounts
        """
        _logger.info('===========prajwal=============log===info')
        _logger.info('=====%s======prajwal=============log===info')
        
        for rec in self:
            product_billet = rec.raw_workorder_line_ids.filtered(lambda l: l.product_id.is_type  == 'billet')
            product_missroll = rec.finished_workorder_line_ids.filtered(lambda l: l.product_id.is_type  == 'missroll').qty_done
            product_endcut = rec.finished_workorder_line_ids.filtered(lambda l: l.product_id.is_type  == 'endcut').qty_done
            product_dust = rec.finished_workorder_line_ids.filtered(lambda l: l.product_id.is_type  == 'dust').qty_done
            summed_weight_billet = 0.0
            for bil in product_billet:
                summed_weight_billet += (bil.qty_done * bil.product_id.weight)
                _logger.info('=====%s======prajwal=============log===info',bil.qty_done)
                _logger.info("====summed billet %s=====",summed_weight_billet)
            rec.update({
                'qty_producing': summed_weight_billet - product_missroll - product_endcut - product_dust
            })

    def end_previous(self, doall=False):
        """
        @param: doall:  This will close all open time lines on the open work orders when doall = True, otherwise
        only the one of the current user
        """
        # TDE CLEANME
        timeline_obj = self.env['mrp.workcenter.productivity']
        domain = [('workorder_id', 'in', self.ids),('loss_type','not in',('quality', 'availability')),('date_end', '=', False)]
        if not doall:
            domain.append(('user_id', '=', self.env.user.id))
        not_productive_timelines = timeline_obj.browse()
        for timeline in timeline_obj.search(domain, limit=None if doall else 1):
            wo = timeline.workorder_id
            if wo.duration_expected <= wo.duration:
     
                if timeline.loss_type == 'productive':
                    not_productive_timelines += timeline
                timeline.write({'date_end': fields.Datetime.now()})
            else:
                maxdate = fields.Datetime.from_string(timeline.date_start) + relativedelta(minutes=wo.duration_expected - wo.duration)
                enddate = datetime.now()
                if maxdate > enddate:
                    timeline.write({'date_end': enddate})
                else:
                    timeline.write({'date_end': maxdate})
                    not_productive_timelines += timeline.copy({'date_start': maxdate, 'date_end': enddate})
        if not_productive_timelines:
            loss_id = self.env['mrp.workcenter.productivity.loss'].search([('loss_type', '=', 'performance')], limit=1)
            if not len(loss_id):
                raise UserError(_("You need to define at least one unactive productivity loss in the category 'Performance'. Create one from the Manufacturing app, menu: Configuration / Productivity Losses."))
            not_productive_timelines.write({'loss_id': loss_id.id})
        return True


class MrpWorkcenterProductivityLossExt(models.Model):

    _inherit = "mrp.workcenter.productivity.loss"

    active = fields.Boolean(default=True)

# class MrpWorkcenterProductivityLossExt(models.Model):

#     _inherit = "mrp.workcenter.productivity.loss.type"

#     loss_type = fields.Selection([
#             ('availability', 'Loss'),
#             ('productive', 'Productive')], string='Category', default='availability', required=True)

