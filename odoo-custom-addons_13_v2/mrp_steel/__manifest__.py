
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Mrp Steel",
    "summary": "Mrp Steel",
    # "category": "Manufacture",
    "author": "Nexus",
    # "website": "https://github.com/dsk2370/fleet",
    "license": "AGPL-3",
    "version": "13.0.1.0.0",
    "depends": ["base","mrp","bom_ext"],
    "data": [    

    # 'security/security.xml',
    # 'data/data.xml',
    # 'security/ir.model.access.csv',
    'views/mrp_steel.xml',
    # 'views/fleet_report.xml'

],
    "installable": True,
    'application':True
}
